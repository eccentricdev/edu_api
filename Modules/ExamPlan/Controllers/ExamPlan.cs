using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using edu_api.Modules.ExamPlan.Databases.Models;
using edu_api.Modules.ExamPlan.Models;
using edu_api.Modules.Study.Databases.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SeventyOneDev.Utilities;

namespace edu_api.Modules.ExamPlan.Controllers
{
    [ApiController]
    [Route("api/exam_plan/exam_plan", Name = "exam_plan")]
    public class ExamPlanController:ControllerBase
    {
        private readonly Db _db;

        public ExamPlanController(Db db)
        {
            _db = db;
        }

        [HttpGet("year_subject/{academic_semester_uid}/{page:int}/{size:int}")]
        public async Task<ActionResult<PagedResult<v_year_subject_exam_summary>>> GetYearSubject(
            [FromRoute] Guid academic_semester_uid,
            [FromQuery] DateTime? from_midterm_lecture_exam_date,
            [FromQuery] DateTime? to_midterm_lecture_exam_date,
            [FromQuery] DateTime? from_midterm_lab_exam_date,
            [FromQuery] DateTime? to_midterm_lab_exam_date,
            [FromQuery] DateTime? from_final_lecture_exam_date,
            [FromQuery] DateTime? to_final_lecture_exam_date,
            [FromQuery] DateTime? from_final_lab_exam_date,
            [FromQuery] DateTime? to_final_lab_exam_date,
            [FromQuery] v_year_subject_exam_summary yearSubject = null,
            [FromRoute] int page = 1,
            [FromRoute] int size = 10)
        {
            var yearSubjects = _db.v_year_subject_exam_summary.AsNoTracking()
                .Where(w => w.academic_semester_uid == academic_semester_uid);
            if (from_midterm_lecture_exam_date != null && to_midterm_lecture_exam_date != null)
            {
                yearSubjects = yearSubjects.Where(w =>
                    w.midterm_lecture_exam_date.Value.Date >= from_midterm_lecture_exam_date.Value.Date 
                    && w.midterm_lecture_exam_date.Value.Date <= to_midterm_lecture_exam_date.Value.Date);
            }
            if (from_midterm_lab_exam_date != null && to_midterm_lab_exam_date != null)
            {
                yearSubjects = yearSubjects.Where(w =>
                    w.midterm_lab_exam_date.Value.Date >= from_midterm_lab_exam_date.Value.Date 
                    && w.midterm_lab_exam_date.Value.Date <= to_midterm_lab_exam_date.Value.Date);
            }
            if (from_final_lecture_exam_date != null && to_final_lecture_exam_date != null)
            {
                yearSubjects = yearSubjects.Where(w =>
                    w.final_lecture_exam_date.Value.Date >= from_final_lecture_exam_date.Value.Date 
                    && w.final_lecture_exam_date.Value.Date <= to_final_lecture_exam_date.Value.Date);
            }
            if (from_final_lab_exam_date != null && to_final_lab_exam_date != null)
            {
                yearSubjects = yearSubjects.Where(w =>
                    w.final_lab_exam_date.Value.Date >= from_final_lab_exam_date.Value.Date 
                    && w.final_lab_exam_date.Value.Date <= to_final_lab_exam_date.Value.Date);
            }

            if (EFExtension.IsNullOrEmpty(yearSubject))
            {
                var result=await yearSubjects.DefaultOrderBy().GetPaged(page, size);
                return result;
            }
            else
            {
                yearSubjects = yearSubjects.Search(yearSubject);
                var result=await yearSubjects.DefaultOrderBy().GetPaged(page, size);
                return result;
            }
        }
        [HttpGet("building")]
        public async Task<ActionResult<List<v_building>>> GetBuilding()
        {
            var buildings = await _db.v_building.AsNoTracking().ToListAsync();
            return Ok(buildings);
        }

        [HttpGet("floor/{building_uid}")]
        public async Task<ActionResult<List<v_floor>>> GetBuildingFloor([FromRoute] Guid building_uid)
        {
            var floors = await _db.v_floor.AsNoTracking()
                .Include(r => r.rooms)
                .Where(f => f.building_uid == building_uid && f.total_exam_seats > 0).ToListAsync();
            return Ok(floors);
        }

        [HttpGet("instructor")]
        public async Task<ActionResult<List<v_instructor>>> GetInstructor()
        {
            var instructors = await _db.v_instructor.AsNoTracking().ToListAsync();
            return Ok(instructors);
        }
        
        [HttpGet("instructor/{no_of_random}")]
        public async Task<ActionResult<List<v_instructor>>> GetRandomInstructor([FromRoute]int no_of_random)
        {
            var instructors = await _db.v_instructor.AsNoTracking().OrderBy(a=>Guid.NewGuid()).Take(no_of_random).ToListAsync();
            return Ok(instructors);
        }

        [HttpGet("room")]
        public async Task<ActionResult<List<year_subject_exam_room>>> GetExamRoomStudent()
        {
            var yearSubjectExamRooms = await _db.v_year_subject_exam_room.AsNoTracking().Take(100).ToListAsync();
            return Ok(yearSubjectExamRooms);
        }

        [HttpGet("room/{year_subject_exam_room_uid}")]
        public async Task<ActionResult<exam_room_detail>> GetExamRoomStudent(Guid year_subject_exam_room_uid)
        {
            var yearSubjectExamRoomStudents = await _db.v_year_subject_exam_room_student.AsNoTracking()
                .Where(s => s.year_subject_exam_room_uid == year_subject_exam_room_uid)
                .ProjectTo<v_year_subject_exam_room_student, exam_room_student>()
                .ToListAsync();
            var yearSubjectExamRoom = await _db.v_year_subject_exam_room.AsNoTracking()
                .FirstOrDefaultAsync(r => r.year_subject_exam_room_uid == year_subject_exam_room_uid);
            var yearSubjectExamRoomProctors = await _db.v_year_subject_exam_room_proctor.AsNoTracking()
                .Where(r => r.year_subject_exam_room_uid == year_subject_exam_room_uid).ToListAsync();
            var examRoomDetail = new exam_room_detail()
            {
                year_subject_exam_room_uid = yearSubjectExamRoom.year_subject_exam_room_uid,
                year_subject_code = yearSubjectExamRoom.year_subject_code,
                year_subject_name_th = yearSubjectExamRoom.year_subject_name_th,
                year_subject_name_en = yearSubjectExamRoom.year_subject_name_en,
                // public string exam_date { get; set; }
                // public string exam_time { get; set; }
                exam_type_id = yearSubjectExamRoom.exam_type_id,
                building_name_th = yearSubjectExamRoom.building_name_th,
                //building_name_en =yearSubjectExamRoom.building_name_th
                floor_no = yearSubjectExamRoom.floor_no,
                room_no = yearSubjectExamRoom.room_no
                //proctor_name { get; set; }
            };
            if (yearSubjectExamRoom.exam_type_id == 1)
            {
                examRoomDetail.exam_date = yearSubjectExamRoom.midterm_lecture_exam_date;
                if (yearSubjectExamRoom.midterm_lecture_exam_from_time.HasValue &&
                    yearSubjectExamRoom.midterm_lecture_exam_to_time.HasValue)
                {
                    examRoomDetail.exam_time =
                        yearSubjectExamRoom.midterm_lecture_exam_from_time.Value.ToString(@"\hh\:mm") + "-" +
                        yearSubjectExamRoom.midterm_lecture_exam_to_time.Value.ToString(@"\hh\:mm");
                }
            }
            else
            {
                examRoomDetail.exam_date = yearSubjectExamRoom.final_lecture_exam_date;
                if (yearSubjectExamRoom.final_lecture_exam_from_time.HasValue &&
                    yearSubjectExamRoom.final_lecture_exam_to_time.HasValue)
                {
                    examRoomDetail.exam_time =
                        yearSubjectExamRoom.final_lecture_exam_from_time.Value.ToString(@"\hh\:mm") + "-" +
                        yearSubjectExamRoom.final_lecture_exam_to_time.Value.ToString(@"\hh\:mm");
                }
            }

            if (yearSubjectExamRoomProctors.Any())
            {
                examRoomDetail.proctor_name =
                    string.Join(",", yearSubjectExamRoomProctors.Select(p => p.display_name_th));

            }

            if (yearSubjectExamRoomStudents.Any())
            {
                examRoomDetail.students = yearSubjectExamRoomStudents.OrderBy(s => s.seat_no).ToList();
            }

            return Ok(examRoomDetail);
        }
    
        [HttpPost]
        public async Task<ActionResult<List<t_year_subject_exam_room>>> AssignExamRoom([FromBody]List<t_year_subject_exam_room> yearSubjectExamRooms)
        {
            
            foreach (var yearSubjectExamRoom in yearSubjectExamRooms)
            {
                yearSubjectExamRoom.year_subject_exam_room_uid = Guid.NewGuid();
                var yearSubjectExam = await _db.t_year_subject_exam.AsNoTracking().FirstOrDefaultAsync(y =>
                    y.year_subject_exam_uid == yearSubjectExamRoom.year_subject_exam_uid);
                yearSubjectExamRoom.exam_type_id ??= 2;
                if (yearSubjectExamRoom.exam_type_id==2)
                {
                    if (yearSubjectExam.final_lecture_exam_date.HasValue && yearSubjectExam.final_lecture_exam_from_time.HasValue && yearSubjectExam.final_lecture_exam_to_time.HasValue)
                    {
                        yearSubjectExamRoom.start_datetime =
                            yearSubjectExam.final_lecture_exam_date.Value.Add(yearSubjectExam.final_lecture_exam_from_time.Value);
                        yearSubjectExamRoom.start_datetime =
                            yearSubjectExam.final_lecture_exam_date.Value.Add(yearSubjectExam.final_lecture_exam_to_time.Value);
                    }
                   
                }
                else
                {
                    yearSubjectExamRoom.exam_type_id = 1;
                    if (yearSubjectExam.midterm_lecture_exam_date.HasValue &&
                        yearSubjectExam.midterm_lecture_exam_from_time.HasValue &&
                        yearSubjectExam.midterm_lecture_exam_to_time.HasValue)
                    {
                        yearSubjectExamRoom.start_datetime =
                            yearSubjectExam.midterm_lecture_exam_date.Value.Add(yearSubjectExam
                                .midterm_lecture_exam_from_time.Value);
                        yearSubjectExamRoom.start_datetime =
                            yearSubjectExam.midterm_lecture_exam_date.Value.Add(yearSubjectExam
                                .midterm_lecture_exam_to_time.Value);
                    }
                }
                if (yearSubjectExamRoom.no_of_students.HasValue)
                {
                    var finalRegisterSubjects = await _db.v_final_register_subject.AsNoTracking()
                        .Where(f => f.year_subject_uid == yearSubjectExam.year_subject_uid)
                        .Take(yearSubjectExamRoom.no_of_students.Value).ToListAsync();
                    yearSubjectExamRoom.year_subject_exam_room_students = finalRegisterSubjects
                        .Select((f,index) => new t_year_subject_exam_room_student()
                        {
                            student_uid = f.student_uid,
                            final_register_subject_uid = f.final_register_subject_uid,
                            seat_no = index +1
                        }).ToList();

                }
                else if (yearSubjectExamRoom.exam_seats.HasValue)
                {
                    var finalRegisterSubjects = await _db.v_final_register_subject.AsNoTracking()
                        .Where(f => f.year_subject_uid == yearSubjectExam.year_subject_uid)
                        .Take(yearSubjectExamRoom.exam_seats.Value).ToListAsync();
                    yearSubjectExamRoom.year_subject_exam_room_students = finalRegisterSubjects
                        .Select((f,index) => new t_year_subject_exam_room_student()
                        {
                            student_uid = f.student_uid,
                            final_register_subject_uid = f.final_register_subject_uid,
                            seat_no = index +1
                        }).ToList();
                }

                if (yearSubjectExamRoom.year_subject_exam_room_proctors != null)
                {
                    foreach (var yearSubjectExamRoomProctor in yearSubjectExamRoom.year_subject_exam_room_proctors)
                    {
                        if (yearSubjectExamRoom.start_datetime.HasValue && yearSubjectExamRoom.end_datetime.HasValue)
                        {
                            var _ = await AddInstructorAvailability(
                                yearSubjectExamRoomProctor.instructor_uid!.Value,
                                yearSubjectExamRoom.start_datetime!.Value,
                                yearSubjectExamRoom.end_datetime!.Value,
                                yearSubjectExamRoom.year_subject_exam_room_uid!.Value);
                        }
                        
                    }
                }
            }
            await _db.t_year_subject_exam_room.AddRangeAsync(yearSubjectExamRooms);
            await _db.SaveChangesAsync();
            return Ok(yearSubjectExamRooms);
        }
        [HttpGet]
        public async Task<ActionResult<v_year_subject_exam_room>> GetYearSubjectExamRoom([FromQuery]v_year_subject_exam_room yearSubjectExamRoom)
        {
            if (EFExtension.IsNullOrEmpty(yearSubjectExamRoom))
            {
                var result = await _db.v_year_subject_exam_room.AsNoTracking().ToListAsync();
                return Ok(result);
            }
            else
            {
                var result = await _db.v_year_subject_exam_room.AsNoTracking().Search(yearSubjectExamRoom).ToListAsync();
                return Ok(result);
            }
        }
        
        [HttpGet("{year_subject_exam_room_uid}")]
        public async Task<ActionResult<v_year_subject_exam_room>> GetYearSubjectExamRoom([FromRoute]Guid year_subject_exam_room_uid)
        {
            var yearSubjectExamRoom = _db.v_year_subject_exam_room.AsNoTracking()
                .Include(y => y.year_subject_exam_room_proctors)
                .FirstOrDefaultAsync(y => y.year_subject_exam_room_uid == year_subject_exam_room_uid);
            return Ok(yearSubjectExamRoom);
        }
        [HttpPut]
        public async Task<ActionResult> UpdateYearSubjectExamRoom([FromBody]t_year_subject_exam_room yearSubjectExamRoom)
        {
            _db.t_year_subject_exam_room.Update(yearSubjectExamRoom);
            await _db.SaveChangesAsync();
            return Ok();
        }

        private async Task<bool> AddInstructorAvailability(Guid instructorUid,DateTime startDatetime,DateTime endDatetime,Guid yearSubjectExamRoomUid)
        {
            if (!_db.t_instructor_availability.Any(i=>i.instructor_uid==instructorUid))
                {
                    var defaultInstructorAvailability = new t_instructor_availability()
                    {
                        instructor_availability_uid = Guid.NewGuid(),
                        instructor_uid = instructorUid,
                        //start_datetime = instructorBusy.start_datetime,
                        end_datetime =  DateTime.Parse("2018-01-01"),
                        next_available_datetime = DateTime.Today.AddYears(10)
                    };
                    await _db.t_instructor_availability.AddAsync(defaultInstructorAvailability);
                    await _db.SaveChangesAsync();
                }
                
                var currentAvailability = await _db.t_instructor_availability.FirstOrDefaultAsync(a => 
                    a.instructor_uid == instructorUid &&
                    a.end_datetime < startDatetime &&
                    a.next_available_datetime > endDatetime);
                
                if (currentAvailability is not null)
                {
                    var newAvailability = new t_instructor_availability()
                    {
                        instructor_availability_uid = Guid.NewGuid(),
                        instructor_uid = instructorUid,
                        start_datetime = startDatetime,
                        end_datetime = endDatetime,
                        next_available_datetime = currentAvailability.next_available_datetime,
                        previous_instructor_availability_uid=currentAvailability.instructor_availability_uid,
                        year_subject_exam_room_uid = yearSubjectExamRoomUid
                    };
                    currentAvailability.next_available_datetime = startDatetime.AddSeconds(-1);

                    var nextAvailability = await _db.t_instructor_availability.FirstOrDefaultAsync(a =>
                        a.previous_instructor_availability_uid == currentAvailability.instructor_availability_uid);
                    if (nextAvailability!=null)
                    {
                        nextAvailability.previous_instructor_availability_uid =
                            newAvailability.instructor_availability_uid;
                    }
                    await _db.t_instructor_availability.AddAsync(newAvailability);

                }

                await _db.SaveChangesAsync();

                return true;
        }
    }
}