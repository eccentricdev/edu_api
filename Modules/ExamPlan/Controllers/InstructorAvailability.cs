using System;
using System.Threading.Tasks;
using edu_api.Modules.ExamPlan.Databases.Models;
using Microsoft.AspNetCore.Mvc;
using SeventyOneDev.Utilities;
using System.Collections.Generic;
using System.Linq;
using edu_api.Modules.ExamPlan.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.EntityFrameworkCore;


namespace edu_api.Modules.ExamPlan.Controllers
{
    [ApiController]
    [Route("api/exam_plan/instructor_availability", Name = "instructor_availability")]
    public class InstructorAvailability:ControllerBase
    {
        private readonly Db _db;
        public InstructorAvailability(Db db) 
        {
            _db = db;
        }


        [Authorize]
        [HttpGet("schedule/{instructor_uid}")]
        public async Task<ActionResult<List<instructor_busy>>> GetInstructorSchedule([FromRoute] Guid instructor_uid)
        {
            var instructorBusies = await _db.v_instructor_availability.AsNoTracking()
                .Where(i => i.instructor_uid == instructor_uid && i.is_busy == null)
                .ProjectTo<v_instructor_availability,instructor_busy>()
                .ToListAsync();
            return Ok(instructorBusies);

        }

        [Authorize]
        [HttpGet("busy/{instructor_uid}")]
        public async Task<ActionResult<List<instructor_busy>>> GetInstructorBusy([FromRoute] Guid instructor_uid)
        {
            var instructorBusies = await _db.v_instructor_availability.AsNoTracking()
                .Where(i => i.instructor_uid == instructor_uid && i.is_busy == true)
.ProjectTo<v_instructor_availability,instructor_busy>()
.ToListAsync();
            return Ok(instructorBusies);

        }

        [Authorize]
        [HttpPost("busy")]
        public async Task<ActionResult> AddInstructorBusy([FromBody]List<instructor_busy> instructorBusies)
        {
            foreach (var instructorBusy in instructorBusies)
            {
                if (!_db.t_instructor_availability.Any(i=>i.instructor_uid==instructorBusy.instructor_uid))
                {
                    var defaultInstructorAvailability = new t_instructor_availability()
                    {
                        instructor_availability_uid = Guid.NewGuid(),
                        instructor_uid = instructorBusy.instructor_uid,
                        //start_datetime = instructorBusy.start_datetime,
                        end_datetime = DateTime.Parse("2018-01-01"),
                        next_available_datetime = DateTime.Today.AddYears(10)
                    };
                    await _db.t_instructor_availability.AddAsync(defaultInstructorAvailability);
                    await _db.SaveChangesAsync();
                }
                
                var currentAvailability = await _db.t_instructor_availability.FirstOrDefaultAsync(a => 
                    a.instructor_uid == instructorBusy.instructor_uid &&
                    a.end_datetime < instructorBusy.start_datetime &&
                    a.next_available_datetime > instructorBusy.end_datetime);
                
                if (currentAvailability is not null)
                {
                    var newAvailability = new t_instructor_availability()
                    {
                        instructor_availability_uid = Guid.NewGuid(),
                        instructor_uid = instructorBusy.instructor_uid,
                        start_datetime = instructorBusy.start_datetime,
                        end_datetime = instructorBusy.end_datetime,
                        next_available_datetime = currentAvailability.next_available_datetime,
                        remark = instructorBusy.remark,
                        previous_instructor_availability_uid=currentAvailability.instructor_availability_uid,
                        is_busy = true
                      
                    };
                    currentAvailability.next_available_datetime = instructorBusy.start_datetime.Value.AddSeconds(-1);
                    
                    var nextAvailability = await _db.t_instructor_availability.FirstOrDefaultAsync(a =>
                        a.previous_instructor_availability_uid == currentAvailability.instructor_availability_uid);
                    if (nextAvailability!=null)
                    {
                        nextAvailability.previous_instructor_availability_uid =
                            newAvailability.instructor_availability_uid;
                    }
                    await _db.t_instructor_availability.AddAsync(newAvailability);

                }

                

                await _db.SaveChangesAsync();
            }

            return Ok();
        }
        [Authorize]
        [HttpDelete("busy")]
        public async Task<ActionResult> RemoveInstructorBusy([FromBody]List<Guid> instructorBusyUids)
        {
            foreach (var instructorBusyUid in instructorBusyUids)
            {
                var currentInstructorAvailability =
                    await _db.t_instructor_availability.FirstOrDefaultAsync(a =>
                        a.instructor_availability_uid == instructorBusyUid);

                var previousInstructorAvailability = await _db.t_instructor_availability.FirstOrDefaultAsync(a =>
                    a.instructor_availability_uid ==
                    currentInstructorAvailability.previous_instructor_availability_uid);
                if (previousInstructorAvailability is not null)
                {
                    previousInstructorAvailability.next_available_datetime =
                        currentInstructorAvailability.next_available_datetime;
                }
                var nextInstructorAvailability = await _db.t_instructor_availability.FirstOrDefaultAsync(a =>
                    a.previous_instructor_availability_uid ==
                    currentInstructorAvailability.instructor_availability_uid);
                if (nextInstructorAvailability is not null)
                {
                    if (previousInstructorAvailability is not null)
                    {
                        nextInstructorAvailability.previous_instructor_availability_uid =
                            previousInstructorAvailability.instructor_availability_uid;
                    }
                    else
                    {

                        nextInstructorAvailability.previous_instructor_availability_uid = null;
                    }
                   
                }

                _db.t_instructor_availability.Remove(currentInstructorAvailability);

            }

            await _db.SaveChangesAsync();
            return Ok();
        }
        
    }
}