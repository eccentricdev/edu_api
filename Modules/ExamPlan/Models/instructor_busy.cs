using System;
namespace edu_api.Modules.ExamPlan.Models
{
    public class instructor_busy
    {
        public Guid? instructor_uid { get; set; }
        public DateTime? start_datetime { get; set; }
        public DateTime? end_datetime { get; set; }
        public string remark { get; set; }
    }
}