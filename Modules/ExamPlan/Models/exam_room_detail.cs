using System;
using System.Collections.Generic;

namespace edu_api.Modules.ExamPlan.Models
{
    public class exam_room_detail
    {
public Guid? year_subject_exam_room_uid { get; set; }
        public string year_subject_code { get; set; }
        public string year_subject_name_th { get; set; }
        public string year_subject_name_en { get; set; }
        public DateTime? exam_date { get; set; }
        public string exam_time { get; set; }
        public int? exam_type_id { get; set; }
        public string building_name_th { get; set; }
        //public string building_name_en { get; set; }
        public string floor_no { get; set; }
        public string room_no { get; set; }
        public string proctor_name { get; set; }
public List<exam_room_student> students { get; set; }
    }

    public class exam_room_student
    {
        public string student_code { get; set; }
        public string display_name_th { get; set; }
        public string display_name_en { get; set; }
        public string college_faculty_name_th { get; set; }
        public string college_faculty_name_en { get; set; }
        public string faculty_curriculum_name_th { get; set; }
        public string faculty_curriculum_name_en { get; set; }
        public string section_code { get; set; }
        public int? seat_no { get; set; }

    }
}