
using edu_api.Modules.ExamPlan.Databases.Models;
using Microsoft.EntityFrameworkCore;

namespace SeventyOneDev.Utilities
{
    public partial class Db : DbContext
    {
        public DbSet<t_year_subject_exam_room>  t_year_subject_exam_room { get; set; }
        public DbSet<v_year_subject_exam_room>  v_year_subject_exam_room { get; set; }
        public DbSet<t_year_subject_exam_room_proctor>  t_year_subject_exam_room_proctor { get; set; }
        public DbSet<v_year_subject_exam_room_proctor>  v_year_subject_exam_room_proctor { get; set; }
        public DbSet<t_year_subject_exam_room_student>  t_year_subject_exam_room_student { get; set; }
        public DbSet<v_year_subject_exam_room_student>  v_year_subject_exam_room_student { get; set; }
        public DbSet<v_year_subject_exam_summary>  v_year_subject_exam_summary { get; set; }
        
        public DbSet<t_instructor_availability>  t_instructor_availability { get; set; }
        public DbSet<v_instructor_availability>  v_instructor_availability { get; set; }

        private void OnExamPlanModelCreating(ModelBuilder builder, string schema)
        {
            builder.Entity<t_year_subject_exam_room>().ToTable("year_subject_exam_room", schema);
            builder.Entity<v_year_subject_exam_room>().ToView("v_year_subject_exam_room", schema);
            
            builder.Entity<t_year_subject_exam_room_proctor>().ToTable("year_subject_exam_room_proctor", schema);
            builder.Entity<t_year_subject_exam_room_proctor>().HasOne(y => y.year_subject_exam_room)
                .WithMany(y => y.year_subject_exam_room_proctors)
                .HasForeignKey(y => y.year_subject_exam_room_uid).OnDelete(DeleteBehavior.Cascade);
            builder.Entity<v_year_subject_exam_room_proctor>().ToView("v_year_subject_exam_room_proctor", schema);
            builder.Entity<v_year_subject_exam_room_proctor>().HasOne(y => y.year_subject_exam_room)
                .WithMany(y => y.year_subject_exam_room_proctors)
                .HasForeignKey(y => y.year_subject_exam_room_uid).OnDelete(DeleteBehavior.Cascade);
            
            builder.Entity<t_year_subject_exam_room_student>().ToTable("year_subject_exam_room_student", schema);
            builder.Entity<t_year_subject_exam_room_student>().HasOne(y => y.year_subject_exam_room)
                .WithMany(y => y.year_subject_exam_room_students)
                .HasForeignKey(y => y.year_subject_exam_room_uid).OnDelete(DeleteBehavior.Cascade);
            builder.Entity<v_year_subject_exam_room_student>().ToView("v_year_subject_exam_room_student", schema);
            builder.Entity<v_year_subject_exam_room_student>().HasOne(y => y.year_subject_exam_room)
                .WithMany(y => y.year_subject_exam_room_students)
                .HasForeignKey(y => y.year_subject_exam_room_uid).OnDelete(DeleteBehavior.Cascade);
            
            builder.Entity<v_year_subject_exam_summary>().ToView("v_year_subject_exam_summary", schema);
            
            builder.Entity<t_instructor_availability>().ToTable("instructor_availability", schema);
            builder.Entity<v_instructor_availability>().ToView("v_instructor_availability", schema);
            
        }
    }
}