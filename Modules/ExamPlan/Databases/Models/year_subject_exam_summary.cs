using System;
using System.ComponentModel.DataAnnotations;

namespace edu_api.Modules.ExamPlan.Databases.Models
{
        public class v_year_subject_exam_summary
        {
                [Key]
                public Guid? year_subject_exam_uid { get; set; }
                public Guid? year_subject_uid { get; set; }
                public Guid? academic_year_uid { get; set; }
                public string academic_year_code { get; set; }
                public Guid? academic_semester_uid { get; set; }
                public string academic_semester_code { get; set; }
                public string year_subject_code { get; set; }
                public string year_subject_name_th { get; set; }
                public string year_subject_name_en { get; set; }
                public DateTime? midterm_lecture_exam_date { get; set; }
                public TimeSpan? midterm_lecture_exam_from_time { get; set; }
                public TimeSpan? midterm_lecture_exam_to_time { get; set; }
                public DateTime? midterm_lab_exam_date { get; set; }
                public TimeSpan? midterm_lab_exam_from_time { get; set; }
                public TimeSpan? midterm_lab_exam_to_time { get; set; }
                public DateTime? final_lecture_exam_date { get; set; }
                public TimeSpan? final_lecture_exam_from_time { get; set; }
                public TimeSpan? final_lecture_exam_to_time { get; set; }
                public DateTime? final_lab_exam_date { get; set; }
                public TimeSpan? final_lab_exam_from_time { get; set; }
                public TimeSpan? final_lab_exam_to_time { get; set; }
                public int? total_sections { get; set; }
                public int? total_registers { get; set; }
        }
}