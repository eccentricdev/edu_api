using System;
using System.ComponentModel.DataAnnotations;
using SeventyOneDev.Utilities;
using SeventyOneDev.Utilities.Attributes;

namespace edu_api.Modules.ExamPlan.Databases.Models
{
    public class instructor_availability:base_table
    {
        [Key]
        public Guid? instructor_availability_uid { get; set; }
        public Guid? instructor_uid { get; set; }
        public DateTime? start_datetime { get; set; }
        public DateTime? end_datetime { get; set; }
        public DateTime? next_available_datetime { get; set; }
        public Guid? previous_instructor_availability_uid { get; set; }
        public Guid? year_subject_exam_room_uid { get; set; }
        public bool? is_busy { get; set; }
        public string remark { get; set; }
    }
    //[GeneratedUidController("api/exam_plan/instructor_availability")]
    public class t_instructor_availability : instructor_availability
    {
        
    }

    public class v_instructor_availability : instructor_availability
    {
        
    }
}