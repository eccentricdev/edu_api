using System;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;
using SeventyOneDev.Utilities;

namespace edu_api.Modules.ExamPlan.Databases.Models
{
    public class year_subject_exam_room_student:base_table
    {
        [Key]
        public Guid? year_subject_exam_room_student_uid { get; set; }
        public Guid? year_subject_exam_room_uid { get; set; }
        public Guid? student_uid { get; set; }
        public Guid? final_register_subject_uid { get; set; }
        public int? seat_no { get; set; }
    }

    public class t_year_subject_exam_room_student : year_subject_exam_room_student
    {
        [JsonIgnore]public t_year_subject_exam_room year_subject_exam_room { get; set; }
    }

    public class v_year_subject_exam_room_student : year_subject_exam_room_student
    {
        public string student_code { get; set; }
        public string display_name_th { get; set; }
        public string display_name_en { get; set; }
        public string college_faculty_name_th { get; set; }
        public string college_faculty_name_en { get; set; }
        public string faculty_curriculum_name_th { get; set; }
        public string faculty_curriculum_name_en { get; set; }
        public string section_code { get; set; }
        
        
        [JsonIgnore]public v_year_subject_exam_room year_subject_exam_room { get; set; }
    }
}