using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using SeventyOneDev.Utilities;

namespace edu_api.Modules.ExamPlan.Databases.Models
{
    public class year_subject_exam_room : base_table
    {
        [Key] public Guid? year_subject_exam_room_uid { get; set; }
        public Guid? year_subject_exam_uid { get; set; }
        public Guid? room_uid { get; set; }
        public int? exam_seats { get; set; }
        public int? no_of_students { get; set; }
        public int? exam_type_id { get; set; }
        public int? exam_section_type_id { get; set; }
        public DateTime? start_datetime { get; set; }
        public DateTime? end_datetime { get; set; }
    }

    public class t_year_subject_exam_room : year_subject_exam_room
    {
        public List<t_year_subject_exam_room_proctor> year_subject_exam_room_proctors { get; set; }
        public List<t_year_subject_exam_room_student> year_subject_exam_room_students { get; set; }
    }

    public class v_year_subject_exam_room : year_subject_exam_room
    {
        public Guid? year_subject_uid { get; set; }
        public string academic_year_code { get; set; }
        public string academic_semester_code { get; set; }
        public string year_subject_code { get; set; }
        public string year_subject_name_th { get; set; }
        public string year_subject_name_en { get; set; }
        public DateTime? midterm_lecture_exam_date { get; set; }
        public TimeSpan? midterm_lecture_exam_from_time { get; set; }
        public TimeSpan? midterm_lecture_exam_to_time { get; set; }
        public DateTime? midterm_lab_exam_date { get; set; }
        public TimeSpan? midterm_lab_exam_from_time { get; set; }
        public TimeSpan? midterm_lab_exam_to_time { get; set; }
        public DateTime? final_lecture_exam_date { get; set; }
        public TimeSpan? final_lecture_exam_from_time { get; set; }
        public TimeSpan? final_lecture_exam_to_time { get; set; }
        public DateTime? final_lab_exam_date { get; set; }
        public TimeSpan? final_lab_exam_from_time { get; set; }
        public TimeSpan? final_lab_exam_to_time { get; set; }
        public string room_no {get;set;}
        public string floor_no {get;set;}
        public string building_name_th  {get;set;}
        public List<v_year_subject_exam_room_proctor> year_subject_exam_room_proctors { get; set; }
        public List<v_year_subject_exam_room_student> year_subject_exam_room_students { get; set; }
    }
}