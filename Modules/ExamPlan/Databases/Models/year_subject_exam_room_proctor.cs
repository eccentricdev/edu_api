using System;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;
using SeventyOneDev.Utilities;

namespace edu_api.Modules.ExamPlan.Databases.Models
{
    public class year_subject_exam_room_proctor:base_table
    {
        [Key]
        public Guid? year_subject_exam_room_proctor_uid { get; set; }
        public Guid? year_subject_exam_room_uid { get; set; }
        public Guid? instructor_uid { get; set; }
    }

    public class t_year_subject_exam_room_proctor : year_subject_exam_room_proctor
    {
        [JsonIgnore]public t_year_subject_exam_room year_subject_exam_room { get; set; }
    }

    public class v_year_subject_exam_room_proctor : year_subject_exam_room_proctor
    {
        public string instructor_code {get;set;}
        public string display_name_th {get;set;}
        public string display_name_en {get;set;}
        [JsonIgnore]public v_year_subject_exam_room year_subject_exam_room { get; set; }
    }
    
}