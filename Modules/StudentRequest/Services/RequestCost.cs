using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using edu_api.Modules.StudentRequest.Databases.Models;
using Microsoft.EntityFrameworkCore;
using SeventyOneDev.Core.Document;
using SeventyOneDev.Core.Document.Services;
using SeventyOneDev.Utilities;
using System.Linq;

namespace edu_api.Modules.StudentRequest.Services
{
    public class RequestCostService:EntityUidService<t_request_cost,v_request_cost>
    {
        private readonly Db _context;
    
        public RequestCostService(Db context) : base(context)
        {
            _context = context;
        }


        public override async Task<t_request_cost> NewEntity(t_request_cost entity, ClaimsIdentity claimsIdentity)
        {
            var currCost = _context.t_request_cost.FirstOrDefault(w => w.status_id == 1 && w.request_type_uid == entity.request_type_uid);
            if(currCost != null)
            {
                currCost.status_id = 2;
                _context.t_request_cost.Update(currCost);
                await _context.SaveChangesAsync(claimsIdentity);
            }
            
            entity.status_id = 1;
            return await base.NewEntity(entity, claimsIdentity);
        }


    }
}