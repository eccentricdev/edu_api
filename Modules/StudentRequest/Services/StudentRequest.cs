using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using edu_api.Modules.StudentRequest.Databases.Models;
using Microsoft.EntityFrameworkCore;
using SeventyOneDev.Core.Document;
using SeventyOneDev.Core.Document.Services;
using SeventyOneDev.Utilities;
using System.Linq;

namespace edu_api.Modules.StudentRequest.Services
{
    public class StudentRequestService : EntityUidService<t_student_request, v_student_request>
    {
        private readonly Db _context;
        private readonly DocumentNoService _documentNoService;

        public StudentRequestService(Db context, DocumentNoService documentNoService) : base(context)
        {
            _context = context;
            _documentNoService = documentNoService;
        }

        public override async Task<v_student_request> GetEntity(Guid uid, List<Guid?> agencies = null,
            string userName = null)
        {
            var result = await _context.v_student_request.AsNoTracking()
                .Include(w => w.request_type)
                .Include(w => w.transfer_all_requests)
                .Include(w => w.transfer_partial_requests)
                .ThenInclude(w => w.transfer_partial_subjects)
                .Include(w => w.transfer_new_requests)
                .ThenInclude(w => w.transfer_new_froms)
                .ThenInclude(w => w.transfer_new_subjects)
                .Include(w => w.transfer_institute_requests)
                .ThenInclude(w => w.transfer_institute_subjects)
                .Include(w => w.transfer_institute_requests)
                .ThenInclude(w => w.transfer_institute_documents)
                .Include(w => w.transfer_degree_requests)
                .ThenInclude(w => w.transfer_degree_subjects)
                .Include(w => w.transfer_degree_requests)
                .ThenInclude(w => w.transfer_degree_documents)
                .Include(w => w.transfer_leisure_requests)
                .ThenInclude(w => w.transfer_leisure_subjects)
                .Include(w => w.transfer_leisure_requests)
                .ThenInclude(w => w.transfer_leisure_documents)
                .Include(w => w.transfer_leisure_requests)
                .ThenInclude(w => w.transfer_leisure_consideration_documents)
                .Include(w => w.leave_status_requests)
                .ThenInclude(w => w.leave_status_documents)
                .Include(w => w.student_card_requests)
                .ThenInclude(w => w.student_card_documents)
                .Include(w => w.student_request_name_and_addresses)
                .FirstOrDefaultAsync(w => w.student_request_uid == uid);
            return result;
        }


        public override async Task<t_student_request> NewEntity(t_student_request entity, ClaimsIdentity claimsIdentity)
        {
            var documentNoData = new document_no_data()
            {
                year_code = DateTime.Now.Year.ToString()
            };

            var code = await _documentNoService.GetDocumentNo(null, "STU_REQ", documentNoData);
            entity.student_request_code = code.Substring(4);
            return await base.NewEntity(entity, claimsIdentity);
        }

        public async Task<List<v_student_request>> ListStudentRequestByDate(v_student_request entity,
            DateTime? from_date, DateTime? to_date, List<Guid?> agencies = null, string userName = null)
        {
            var entities = await base.ListEntityId(entity, agencies, userName);

            if (from_date != null)
            {
                entities = entities.Where(w => w.request_date >= from_date).ToList();
            }
            if (to_date != null)
            {
                entities = entities.Where(w => w.request_date <= to_date).ToList();
            }

            return entities;
        }
    }
}