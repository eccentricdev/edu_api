using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using edu_api.Modules.StudentRequest.Databases.Models;
using Microsoft.EntityFrameworkCore;
using SeventyOneDev.Core.Document;
using SeventyOneDev.Core.Document.Services;
using SeventyOneDev.Utilities;
using System.Linq;

namespace edu_api.Modules.StudentRequest.Services
{
    public class RequestTypeService:EntityUidService<t_request_type,v_request_type>
    {
        private readonly Db _context;
        public RequestTypeService(Db context) : base(context)
        {
            _context = context;
        }

        public override async Task<List<v_request_type>> ListEntityId(v_request_type entity, List<Guid?> agencies = null, string userName = null)
        {
             var requestCost = await _context.v_request_cost.AsNoTracking().Where(w => w.status_id == 1).ToListAsync();
        
            var results = await base.ListEntityId(entity, agencies, userName);
            foreach(var result in results)
            {
                result.request_cost_amount = requestCost.FirstOrDefault(w => w.request_type_uid == result.request_type_uid)?.request_cost_amount;
                result.request_cost_type_name_th = requestCost.FirstOrDefault(w => w.request_type_uid == result.request_type_uid)?.request_cost_type_name_th;
                result.request_cost_updated_by = requestCost.FirstOrDefault(w => w.request_type_uid == result.request_type_uid)?.updated_by;
                result.request_cost_updated_datetime = requestCost.FirstOrDefault(w => w.request_type_uid == result.request_type_uid)?.updated_datetime;
            }
            return results;
        }

        public override async Task<v_request_type> GetEntity(Guid uid, List<Guid?> agencies = null, string userName = null)
        {
            var requestCost = await _context.v_request_cost.AsNoTracking().Where(w => w.status_id == 1).ToListAsync();
            var result = await base.GetEntity(uid, agencies, userName);

            if(result != null)
            {
                result.request_cost_amount = requestCost.FirstOrDefault(w => w.request_type_uid == result.request_type_uid)?.request_cost_amount;
                result.request_cost_type_name_th = requestCost.FirstOrDefault(w => w.request_type_uid == result.request_type_uid)?.request_cost_type_name_th;
                result.request_cost_updated_by = requestCost.FirstOrDefault(w => w.request_type_uid == result.request_type_uid)?.updated_by;
                result.request_cost_updated_datetime = requestCost.FirstOrDefault(w => w.request_type_uid == result.request_type_uid)?.updated_datetime;
            }
           return result;
        }

    }
}