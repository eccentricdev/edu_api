using edu_api.Modules.StudentRequest.Services;
using Microsoft.Extensions.DependencyInjection;

namespace edu_api.Modules.Register.Configures
{
  public static class StudentRequestCollectionExtension
  {
    public static IServiceCollection AddStudentRequestServices(this IServiceCollection services)
    {
      services.AddScoped<StudentRequestService>();
      services.AddScoped<RequestCostService>();
      services.AddScoped<RequestTypeService>();
      
      return services;
    }
  }
}
