using System;

namespace edu_api.Modules.StudentRequest.Models
{
  public class current_subject_info
  {
    public Guid? final_register_subject_uid { get; set; }
    public string year_subject_code { get; set; }
    public string year_subject_name_th { get; set; }
    public string year_subject_name_en { get; set; }
    public short? credit { get; set; }
    public Guid? grade_uid { get; set; }
    public string grade_name_th { get; set; }
    public string grade_name_en { get; set; }
  }
}
