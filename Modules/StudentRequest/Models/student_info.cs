using System;

namespace edu_api.Modules.StudentRequest.Models
{
  public class student_info
  {
    public Guid? student_uid { get; set; }
    public string student_code { get; set; }
    public string display_name_th { get; set; }
    public string display_name_en { get; set; }
    public Guid? college_faculty_uid { get; set; }
    public Guid? faculty_curriculum_uid { get; set; }
  }
}
