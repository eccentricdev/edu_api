using System;
using System.Collections.Generic;

namespace edu_api.Modules.StudentRequest.Models
{
  public class student_subject_info
  {
    public Guid? student_uid { get; set; }
    public string student_code { get; set; }
    public List<current_subject_info> subjects { get; set; }
  }
}
