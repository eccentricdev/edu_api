using System;

namespace edu_api.Modules.StudentRequest.Models
{
  public class academic_year_info
  {
    public Guid? academic_year_uid { get; set; }
    public string academic_year_code { get; set; }
    public string academic_year_name_th { get; set; }
    public string academic_year_name_en { get; set; }
  }
}
