using System;

namespace edu_api.Modules.StudentRequest.Models
{
  public class education_type_info
  {
    public Guid? education_type_uid { get; set; }
    public string education_type_code { get; set; }
    public string education_type_name_th { get; set; }
    public string education_type_name_en { get; set; }
  }
}
