using System;

namespace edu_api.Modules.StudentRequest.Models
{
  public class academic_semester_info
  {
    public Guid? academic_semester_uid { get; set; }
    public string academic_semester_code { get; set; }
    public string semester_name_th { get; set; }
    public string semester_name_en { get; set; }
  }
}
