using System;

namespace edu_api.Modules.StudentRequest.Models
{
  public class faculty_curriculum_info
  {
    public Guid? faculty_curriculum_uid { get; set; }
    public string faculty_curriculum_code { get; set; }
    public string faculty_curriculum_name_th { get; set; }
    public string faculty_curriculum_name_en { get; set; }
  }
}
