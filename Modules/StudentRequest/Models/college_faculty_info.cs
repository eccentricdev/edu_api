using System;

namespace edu_api.Modules.StudentRequest.Models
{
  public class college_faculty_info
  {
    public Guid? college_faculty_uid { get; set; }
    public string college_faculty_code { get; set; }
    public string college_faculty_name_th { get; set; }
    public string college_faculty_name_en { get; set; }
  }
}
