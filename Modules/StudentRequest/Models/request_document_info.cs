using System;

namespace edu_api.Modules.StudentRequest.Models
{
  public class request_document_info
  {
    public Guid? request_document_type_uid { get; set; }
    public string request_document_type_code { get; set; }
    public string request_document_type_name_th { get; set; }
    public string request_document_type_name_en { get; set; }
    public int? row_order { get; set; }
  }
}
