using System;
using edu_api.Modules.StudentRequest.Databases.Models;
using edu_api.Modules.StudentRequest.Services;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SeventyOneDev.Utilities;
using System.Collections.Generic;

namespace edu_api.Modules.StudentRequest.Controllers
{
    [Route("api/student_request/request_cost", Name = "request_cost")]
    public class RequestCostController:BaseUidController<t_request_cost,v_request_cost>
    {
        private readonly RequestCostService _entityUidService;
        public RequestCostController(RequestCostService entityUidService) : base(entityUidService)
        {
            _entityUidService = entityUidService;
        }
    
    }
}