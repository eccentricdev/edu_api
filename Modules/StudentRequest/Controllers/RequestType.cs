using System;
using edu_api.Modules.StudentRequest.Databases.Models;
using edu_api.Modules.StudentRequest.Services;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SeventyOneDev.Utilities;
using System.Collections.Generic;

namespace edu_api.Modules.StudentRequest.Controllers
{
     [Route("api/student_request/request_type", Name = "request_type")]
    public class RequestTypeController:BaseUidController<t_request_type,v_request_type>
    {
        private readonly RequestTypeService _entityUidService;
        public RequestTypeController(RequestTypeService entityUidService) : base(entityUidService)
        {
            _entityUidService = entityUidService;
        }
    
    }
}