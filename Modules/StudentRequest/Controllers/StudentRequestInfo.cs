using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using edu_api.Modules.Calendars.Databases.Models;
using edu_api.Modules.Organizations.Databases.Models;
using edu_api.Modules.Register.Databases;
using edu_api.Modules.StudentRequest.Databases.Models;
using edu_api.Modules.StudentRequest.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using rsu_common_api.models;
using SeventyOneDev.Utilities;

namespace edu_api.Modules.StudentRequest.Controllers
{
  [Route("api/student_request/data", Name = "data")]
  public class StudentRequestDataController:Controller
  {
    private readonly Db _db;
    public StudentRequestDataController(Db db)
    {
      _db = db;
    }


    [Authorize]
    [HttpGet("student_info")]
    public async Task<ActionResult<student_info>> GetStudentInfo()
    {
      var claimsIdentity = User.Identity as ClaimsIdentity;
      if (claimsIdentity == null) throw new Exception("NOT_AUTHORIZED");
      var userUid = Token.GetUserUid(claimsIdentity);
      var studentInfo = await _db.t_student.AsNoTracking().Where(s => s.student_uid == userUid)
        .ProjectTo<t_student, student_info>().FirstOrDefaultAsync();
      return studentInfo == null ? Ok() : Ok(studentInfo);
    }
    [Authorize]
    [HttpGet("education_type")]
    public async Task<ActionResult<student_info>> GetEducationType([FromQuery]education_type_info educationTypeFilter)
    {

      var educationType = new t_education_type();
      educationTypeFilter.CloneTo(educationType);
      var educationTypes = await _db.t_education_type.AsNoTracking().Search(educationType)
        .ProjectTo<t_education_type, education_type_info>().ToListAsync();
      return Ok(educationTypes);
    }

    [Authorize]
    [HttpGet("college_faculty")]
    public async Task<ActionResult<student_info>> GetCollegeFaculty([FromQuery]college_faculty_info collegeFacultyFilter)
    {
      var collegeFaculty = new t_college_faculty();
      collegeFacultyFilter.CloneTo(collegeFaculty);
      var collegeFaculties = await _db.t_college_faculty.AsNoTracking().Search(collegeFaculty)
          .OrderBy(c=>c.college_faculty_code)
          .ProjectTo<t_college_faculty, college_faculty_info>().ToListAsync();
        return Ok(collegeFaculties);


    }

    [Authorize]
    [HttpGet("faculty_curriculum")]
    public async Task<ActionResult<student_info>> GetFacultyCurriculum([FromQuery]faculty_curriculum_info facultyCurriculumFilter)
    {
      var facultyCurriculum = new t_faculty_curriculum();
      facultyCurriculumFilter.CloneTo(facultyCurriculum);
      var facultyCurriculums = await _db.t_faculty_curriculum.AsNoTracking().Search(facultyCurriculum)
        .OrderBy(f=>f.faculty_curriculum_code)
        .ProjectTo<t_faculty_curriculum, faculty_curriculum_info>().ToListAsync();
      return Ok(facultyCurriculums);
    }

    [Authorize]
    [HttpGet("academic_year")]
    public async Task<ActionResult<student_info>> GetAcademicYear([FromQuery]academic_year_info academicYearFilter)
    {
      // var claimsIdentity = User.Identity as ClaimsIdentity;
      // if (claimsIdentity == null) throw new Exception("NOT_AUTHORIZED");
      var academicYear = new t_academic_year();
      academicYearFilter.CloneTo(academicYear);
      var academicYears = await _db.t_academic_year.AsNoTracking().Search(academicYear)
        .OrderByDescending(a=>a.academic_year_code)
        .ProjectTo<t_academic_year, academic_year_info>().ToListAsync();
      return Ok(academicYears);
    }
    [Authorize]
    [HttpGet("academic_semester")]
    public async Task<ActionResult<student_info>> GetAcademicSemester([FromQuery]academic_semester_info academicSemesterFilter)
    {
      // var claimsIdentity = User.Identity as ClaimsIdentity;
      // if (claimsIdentity == null) throw new Exception("NOT_AUTHORIZED");
      var academicSemester = new t_academic_semester();
      academicSemesterFilter.CloneTo(academicSemester);
      var academicSemesters = await _db.t_academic_semester.AsNoTracking().Search(academicSemester)
        .OrderBy(a=>a.academic_semester_code)
        .ProjectTo<t_academic_semester, academic_semester_info>().ToListAsync();
      return Ok(academicSemesters);
    }
    [Authorize]
    [HttpGet("current_subject")]
    public async Task<ActionResult<student_info>> GetCurrentSubject()
    {
      var claimsIdentity = User.Identity as ClaimsIdentity;
      if (claimsIdentity == null) throw new Exception("NOT_AUTHORIZED");
      var userUid = Token.GetUserUid(claimsIdentity);
      var student = await _db.t_student.AsNoTracking().FirstOrDefaultAsync(s => s.student_uid == userUid);
      var finalRegisterSubjects =
        await _db.v_final_register_subject.AsNoTracking().Where(f => f.student_uid == userUid)
        .OrderBy(a=>a.year_subject_code)
        .ProjectTo<v_final_register_subject, current_subject_info>().ToListAsync();
      return Ok(finalRegisterSubjects);
    }

    [Authorize]
    [HttpGet("all_subject")]
    public async Task<ActionResult<student_info>> GetAllSubject()
    {
      var claimsIdentity = User.Identity as ClaimsIdentity;
      if (claimsIdentity == null) throw new Exception("NOT_AUTHORIZED");
      var userUid = Token.GetUserUid(claimsIdentity);
      var student = await _db.t_student.AsNoTracking().FirstOrDefaultAsync(s => s.student_uid == userUid);
      var previousStudents = await _db.t_student.AsNoTracking().Where(s => s.citizen_id == student.citizen_id)
        .Select(s => new { s.student_uid, s.student_code }).ToListAsync();
      var studentSubjects = new List<student_subject_info>();
      if (previousStudents.Any())
      {
        foreach (var previousStudent in previousStudents)
        {
          var finalRegisterSubjects =
            await _db.v_final_register_subject.AsNoTracking().Where(f => f.student_uid == userUid)
              .OrderBy(a=>a.year_subject_code)
              .ProjectTo<v_final_register_subject, current_subject_info>().ToListAsync();

          var registerSubjects = new List<current_subject_info>();
          foreach (var finalRegisterSubject in finalRegisterSubjects)
          {
            if (!registerSubjects.Any(r => r.year_subject_code == finalRegisterSubject.year_subject_code))
            {
              registerSubjects.Add(finalRegisterSubject);
            }
          }
          studentSubjects.Add(new student_subject_info()
          {
            student_uid = previousStudent.student_uid,
            student_code = previousStudent.student_code,
            subjects = registerSubjects
          });
        }

      }

      return Ok(studentSubjects);
    }
    [Authorize]
    [HttpGet("request_document/{request_type_uid}")]
    public async Task<ActionResult<List<request_document_info>>> GetRequestDocumentType([FromRoute]Guid request_type_uid)
    {
      var requestDocuments = _db.v_student_request_document_type.AsNoTracking()
        .Where(s => s.request_type_uid == request_type_uid)
        .OrderBy(s => s.row_order)
        .ProjectTo<v_student_request_document_type, request_document_info>().ToListAsync();
      return Ok(requestDocuments);

    }

  }
}
