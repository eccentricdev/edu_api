using System;
using edu_api.Modules.StudentRequest.Databases.Models;
using edu_api.Modules.StudentRequest.Services;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SeventyOneDev.Utilities;
using System.Collections.Generic;
using edu_api.Modules.Payment.Services;
using payment_core.Models;

namespace edu_api.Modules.StudentRequest.Controllers
{
    [Route("api/student_request/student_request", Name = "student_request")]
    public class StudentRequestController:BaseUidController<t_student_request,v_student_request>
    {
        private readonly StudentRequestService _entityUidService;
        private readonly PaymentService _paymentService;
        private readonly Db _db;
        public StudentRequestController(StudentRequestService entityUidService ,PaymentService paymentService,Db db) : base(entityUidService)
        {
            _db = db;
            _entityUidService = entityUidService;
            _paymentService = paymentService;
        }

        [HttpGet("get_by_date")]
        public async Task<ActionResult<List<v_student_request>>> StudentRequestByDate(
           [FromQuery] v_student_request request,
           [FromQuery] DateTime? from_date,
           [FromQuery] DateTime? to_date
        )
        {
            var studentLogResult= await _entityUidService.ListStudentRequestByDate(request,from_date,to_date);
            return Ok(studentLogResult);
        }


        [HttpPost("bill/{student_request_uid}")]
        public async Task<ActionResult<t_student_request>> NewBill([FromRoute] Guid student_request_uid)
        {
            try
            {
                var studentRequest =  await _db.t_student_request.AsNoTracking().FirstOrDefaultAsync(w => w.student_request_uid == student_request_uid);
                if(studentRequest != null){
                    var requestCost = await _db.v_request_cost.AsNoTracking()
                    .FirstOrDefaultAsync(w => w.status_id == 1 && w.request_type_uid == studentRequest.request_type_uid);

                    //bill_uid,bill_no
                    var result=await _paymentService.CreateBill(studentRequest.student_uid ?? Guid.Empty, studentRequest.total_amout ?? 0, new List<bill_request_item>()
                    {
                        new bill_request_item(){
                            item_code = studentRequest.student_request_code,
                            unit_name = "รายการ",
                            item_name_th = requestCost.request_type_name_th,
                            item_name_en = requestCost.request_type_name_en,
                            unit_price = studentRequest.total_amout,
                            quantity = 1,
                            amount = studentRequest.total_amout,
                            }
                    });


                    if(result != null){
                        studentRequest.bill_uid = result.bill_uid;
                        studentRequest.bill_no = result.bill_no;

                        _db.t_student_request.Update(studentRequest); 
                        await _db.SaveChangesAsync();
                    }
                }
                return Ok(studentRequest);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return BadRequest(ErrorMessage.Get(ex.Message));
            }
        }
    }
}