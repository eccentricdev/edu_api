using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;
using SeventyOneDev.Utilities;

namespace edu_api.Modules.StudentRequest.Databases.Models
{
  public class transfer_institute_subject:base_table
  {
    [Key]
    public Guid? transfer_institute_subject_uid { get; set; }
    public Guid? transfer_institute_request_uid { get; set; }
    public Guid? transfer_subject_category_uid { get; set; }
    public string subject_code { get; set; }
    public string subject_name { get; set; }
    [Column(TypeName = "decimal(5,2)")]public decimal? credits { get; set; }
    [MaxLength(50)]public string receive_grade { get; set; }
    public Guid? to_year_subject_uid { get; set; }
    public Guid? to_grade_uid { get; set; }
    public string remark { get; set; }
  }

  public class t_transfer_institute_subject : transfer_institute_subject
  {
    [JsonIgnore]public t_transfer_institute_request transfer_institute_request { get; set; }
  }

  public class v_transfer_institute_subject : transfer_institute_subject
  {
    public string to_year_subject_code { get; set; }
    public string to_year_subject_name_th { get; set; }
    public string to_grade_code { get; set; }
    public string to_grade_name_th { get; set; }
    public string to_grade_point { get; set; }
    [JsonIgnore]public v_transfer_institute_request transfer_institute_request { get; set; }
  }
}
