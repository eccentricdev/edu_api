using System;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;
using SeventyOneDev.Utilities;
using SeventyOneDev.Utilities.Attributes;

namespace edu_api.Modules.StudentRequest.Databases.Models
{
  public class transfer_all_request:base_table
  {
    [Key]

    public Guid? transfer_all_request_uid { get; set; }
    public Guid? student_request_uid { get; set; }
    public string mobile_no { get; set; }
    public string email { get; set; }
    public Guid? education_type_uid { get; set; }
    public Guid? college_faculty_uid { get; set; }
    public Guid? faculty_curriculum_uid { get; set; }
    public Guid? academic_year_uid { get; set; }
    public Guid? academic_semester_uid { get; set; }
  }
  [GeneratedUidController("api/student_request/transfer_all_request")]
  public class t_transfer_all_request : transfer_all_request
  {
    [JsonIgnore]public t_student_request student_request { get; set; }
  }

  public class v_transfer_all_request : transfer_all_request
  {
    [JsonIgnore]public v_student_request student_request { get; set; }
  }
}
