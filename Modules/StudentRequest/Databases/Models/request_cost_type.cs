using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using SeventyOneDev.Utilities;
using SeventyOneDev.Utilities.Attributes;

namespace edu_api.Modules.StudentRequest.Databases.Models
{
       public class request_cost_type:base_table
    {
        [Key]
        public Guid? request_cost_type_uid {get;set;}
        public string request_cost_type_code {get;set;}
        [Search]
        public string request_cost_type_name_th {get;set;}
        [Search]
        public string request_cost_type_name_en {get;set;}
    }
    
    [GeneratedUidController("api/student_request/request_cost_type")]
    public class t_request_cost_type : request_cost_type
    {

    }
    public class v_request_cost_type : request_cost_type
    {

    }
}