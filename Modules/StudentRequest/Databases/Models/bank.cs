using System;
using System.ComponentModel.DataAnnotations;
using SeventyOneDev.Utilities;
using SeventyOneDev.Utilities.Attributes;

namespace edu_api.Modules.StudentRequest.Databases.Models
{
    public class bank:base_table
    {
        [Key]
        public Guid? bank_id {get;set;}
        public string bank_code {get;set;}
        [Search]
        public string bank_name_th {get;set;}
        [Search]
        public string bank_name_en {get;set;}
    }
    [GeneratedUidController("api/student_request/bank")]
    public class t_bank : bank
    {

    }
    public class v_bank : bank
    {

    }
}