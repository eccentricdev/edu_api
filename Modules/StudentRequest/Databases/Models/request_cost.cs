using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using SeventyOneDev.Utilities;
using SeventyOneDev.Utilities.Attributes;

namespace edu_api.Modules.StudentRequest.Databases.Models
{
    public class request_cost:base_table
    {
        [Key]
        public Guid? request_cost_uid {get;set;}
        public Guid? request_type_uid { get; set; }

        [Column(TypeName = "decimal(18,2)")]
        public decimal? request_cost_amount {get;set;}
        public Guid? request_cost_type_uid {get;set;}

    }
    // [GeneratedUidController("api/student_request/request_cost")]
    public class t_request_cost : request_cost
    {

    }
    public class v_request_cost : request_cost
    {
        public string request_type_code { get; set; }
        public string request_type_name_th { get; set; }
        public string request_type_name_en { get; set; }

        public string request_cost_type_code {get;set;}
        public string request_cost_type_name_th {get;set;}
    }
}