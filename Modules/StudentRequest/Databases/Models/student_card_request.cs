using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;
using SeventyOneDev.Utilities;
using SeventyOneDev.Utilities.Attributes;

namespace edu_api.Modules.StudentRequest.Databases.Models
{
  public class student_card_request:base_table
  {
    [Key]
    public Guid? student_card_request_uid { get; set; }
    public Guid? student_request_uid { get; set; }
    public bool? is_lost { get; set; }
    public bool? is_expire { get; set; }
    public string graduate_semester { get; set; }
    public string graduate_year { get; set; }
    public bool? is_faculty_change { get; set; }
    public Guid? from_college_faculty_uid { get; set; }
    public Guid? to_college_faculty_uid { get; set; }
    public bool? is_name_change { get; set; }
    public int? from_title_id { get; set; }
    public string from_first_name_th { get; set; }
    public string from_middle_name_th { get; set; }
    public string from_last_name_th { get; set; }
    public string from_first_name_en { get; set; }
    public string from_middle_name_en { get; set; }
    public string from_last_name_en { get; set; }

    public int? to_title_id { get; set; }
    public string to_first_name_th { get; set; }
    public string to_middle_name_th { get; set; }
    public string to_last_name_th { get; set; }
    public string to_first_name_en { get; set; }
    public string to_middle_name_en { get; set; }
    public string to_last_name_en { get; set; }
  }
  [GeneratedUidController("api/student_request/student_card_request")]
  public class t_student_card_request : student_card_request
  {
    [Include]public List<t_student_card_document> student_card_documents { get; set; }
    [JsonIgnore]public t_student_request student_request { get; set; }
  }

  public class v_student_card_request : student_card_request
  {
    [Include]public List<v_student_card_document> student_card_documents { get; set; }
    [JsonIgnore]public v_student_request student_request { get; set; }
  }
}
