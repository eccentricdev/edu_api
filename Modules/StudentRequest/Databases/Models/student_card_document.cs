using System;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;
using SeventyOneDev.Utilities;

namespace edu_api.Modules.StudentRequest.Databases.Models
{
  public class student_card_document:base_table
  {
    [Key]
    public Guid? student_card_document_uid { get; set; }
    public Guid? student_card_request_uid { get; set; }
    public Guid? request_document_type_uid {get;set;}
    public string request_document_type_name  {get;set;}
    public string file_name {get;set;}
    public string document_name  {get;set;}
    public string document_url  {get;set;}
    public string admin_file_name {get;set;}
    public string admin_document_url  {get;set;}
    [MaxLength(50)]public string mime_type {get;set;}
    public int? document_status_id {get;set;}
  }

  public class t_student_card_document : student_card_document
  {
    [JsonIgnore]public t_student_card_request student_card_request { get; set; }
  }

  public class v_student_card_document : student_card_document
  {
    [JsonIgnore]public v_student_card_request student_card_request { get; set; }
  }
}
