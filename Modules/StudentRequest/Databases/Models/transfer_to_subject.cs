using System;
using SeventyOneDev.Utilities;

namespace edu_api.Modules.StudentRequest.Databases.Models
{
    public class transfer_to_subject:base_table
    {
       public Guid? transfer_to_subject_uid { get; set; }
       public Guid? year_subject_uid { get; set; }
       public Guid? grade_uid { get; set; }
       public string remark { get; set; }
    }
}