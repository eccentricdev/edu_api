// using System;
// using System.Collections.Generic;
// using System.ComponentModel.DataAnnotations;
// using System.Text.Json.Serialization;
// using SeventyOneDev.Utilities;
// using SeventyOneDev.Utilities.Attributes;
//
// namespace edu_api.Modules.StudentRequest.Databases.Models
// {
//   public class transfer_type:base_table
//   {
//     [Key]
//     public Guid? transfer_type_uid { get; set; }
//     public string transfer_type_code { get; set; }
//     public string transfer_type_name_th { get; set; }
//     public string transfer_type_name_en { get; set; }
//   }
//   [GeneratedUidController("api/student_request/transfer_type")]
//   public class t_transfer_type : transfer_type
//   {
//       [JsonIgnore]public List<t_transfer_partial_subject> transfer_partial_subjects { get; set; }
//   }
//
//   public class v_transfer_type : transfer_type
//   {
//     [JsonIgnore]public List<v_transfer_partial_subject> transfer_partial_subjects { get; set; }
//   }
// }
