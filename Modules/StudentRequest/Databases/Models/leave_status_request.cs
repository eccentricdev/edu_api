using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;
using SeventyOneDev.Utilities;
using SeventyOneDev.Utilities.Attributes;

namespace edu_api.Modules.StudentRequest.Databases.Models
{
  public class leave_status_request:base_table
  {
    [Key]
    public Guid? leave_status_request_uid { get; set; }
    public Guid? student_request_uid { get; set; }
    public bool? is_intermission_leave { get; set; }
    public string intermission_leave_semester { get; set; }
    public string intermission_leave_reason { get; set; }

    public bool? is_leave { get; set; }
    public string leave_semester { get; set; }
    public string leave_year { get; set; }
    public Guid? leave_reason_uid { get; set; }
    public string leave_reason_other_remark { get; set; }

    public bool? is_receive_insurance { get; set; }
    public decimal? insurance_amount { get; set; }

    public bool? is_leave_receive_advance_payment { get; set; }
    public decimal? leave_advanced_payment_amount { get; set; }
    public bool? is_leave_receive_other { get; set; }
    public string leave_receive_other_remark { get; set; }
    public string leave_other_amount { get; set; }

    public bool? is_return { get; set; }

    public bool? is_receive_loan { get; set; }
    public decimal? loan_amount { get; set; }
    public bool? is_other_receive { get; set; }
    public string other_receive_reason { get; set; }
    public decimal? other_receive_amount { get; set; }

    public string receive_account_no { get; set; }
    public Guid? receive_bank_uid { get; set; }
    public string receive_bank_branch { get; set; }
    public string receive_account_owner { get; set; }
    public string receive_phone_no { get; set; }
  }
  [GeneratedUidController("api/student_request/leave_status_request")]
  public class t_leave_status_request : leave_status_request
  {
    [Include]public List<t_leave_status_document> leave_status_documents { get; set; }
    [JsonIgnore]public t_student_request student_request { get; set; }
  }

  public class v_leave_status_request : leave_status_request
  {
    [Include]public List<v_leave_status_document> leave_status_documents { get; set; }
    [JsonIgnore]public v_student_request student_request { get; set; }
  }
}
