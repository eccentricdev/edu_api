using System;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;
using SeventyOneDev.Utilities;
using SeventyOneDev.Utilities.Attributes;

namespace edu_api.Modules.StudentRequest.Databases.Models
{
  public class student_request_name_and_address : base_table
  {
    [Key] public Guid? student_request_name_and_address_uid { get; set; }
    public Guid? student_request_uid { get; set; }
    public bool? is_name { get; set; }
    public string url_name { get; set; }
    public string name_url { get; set; }
    public int? from_title_id { get; set; }
    public string from_first_name_th { get; set; }
    public string from_middle_name_th { get; set; }
    public string from_last_name_th { get; set; }
    public string from_first_name_en { get; set; }
    public string from_middle_name_en { get; set; }
    public string from_last_name_en { get; set; }
    public int? to_title_id { get; set; }
    public string to_first_name_th { get; set; }
    public string to_middle_name_th { get; set; }
    public string to_last_name_th { get; set; }
    public string to_first_name_en { get; set; }
    public string to_middle_name_en { get; set; }
    public string to_last_name_en { get; set; }


    public bool? is_address { get; set; }
    public string url_address { get; set; }
    public string from_house_no { get; set; }
    public string from_room_no { get; set; }
    public string from_floor_no { get; set; }
    public string from_building_name { get; set; }
    public string from_village_name { get; set; }
    public string from_village_no { get; set; }
    public string from_alley_name { get; set; }
    public string from_street_name { get; set; }
    public int? form_sub_district_id { get; set; }
    public string from_other_sub_district_name_th { get; set; }
    public string from_other_sub_district_name_en { get; set; }
    public string from_other_district_name_th { get; set; }
    public string from_other_district_name_en { get; set; }
    public string from_other_province_name_th { get; set; }
    public string from_other_province_name_en { get; set; }
    public string form_postal_code { get; set; }
    public int? form_country_id { get; set; }
    public string form_telephone_no { get; set; }
    public string to_house_no { get; set; }
    public string to_room_no { get; set; }
    public string to_floor_no { get; set; }
    public string to_building_name { get; set; }
    public string to_village_name { get; set; }
    public string to_village_no { get; set; }
    public string to_alley_name { get; set; }
    public string to_street_name { get; set; }
    public int? to_sub_district_id { get; set; }
    public string to_other_sub_district_name_th { get; set; }
    public string to_other_sub_district_name_en { get; set; }
    public string to_other_district_name_th { get; set; }
    public string to_other_district_name_en { get; set; }
    public string to_other_province_name_th { get; set; }
    public string to_other_province_name_en { get; set; }
    public string to_postal_code { get; set; }
    public int? to_country_id { get; set; }
    public string to_telephone_no { get; set; }


    public bool? is_name_dad { get; set; }
    public string url_name_dad { get; set; }
    public int? form_dad_title_id { get; set; }
    public string form_dad_first_name_th { get; set; }
    public string form_dad_middle_name_th { get; set; }
    public string form_dad_last_name_th { get; set; }
    public string form_dad_first_name_en { get; set; }
    public string form_dad_middle_name_en { get; set; }
    public string form_dad_last_name_en { get; set; }
    public int? to_dad_title_id { get; set; }
    public string to_dad_first_name_th { get; set; }
    public string to_dad_middle_name_th { get; set; }
    public string to_dad_last_name_th { get; set; }
    public string to_dad_first_name_en { get; set; }
    public string to_dad_middle_name_en { get; set; }
    public string to_dad_last_name_en { get; set; }


    public bool? is_name_mom { get; set; }
    public string url_mom { get; set; }
    public int? form_mom_title_id { get; set; }
    public string form_mom_first_name_th { get; set; }
    public string form_mom_middle_name_th { get; set; }
    public string form_mom_last_name_th { get; set; }
    public string form_mom_first_name_en { get; set; }
    public string form_mom_middle_name_en { get; set; }
    public string form_mom_last_name_en { get; set; }
    public int? to_mom_title_id { get; set; }
    public string to_mom_first_name_th { get; set; }
    public string to_mom_middle_name_th { get; set; }
    public string to_mom_last_name_th { get; set; }
    public string to_mom_first_name_en { get; set; }
    public string to_mom_middle_name_en { get; set; }
    public string to_mom_last_name_en { get; set; }


    public bool? is_address_dad { get; set; }
    public string url_address_dad { get; set; }
    public string from_add_house_no { get; set; }
    public string from_add_room_no { get; set; }
    public string from_add_floor_no { get; set; }
    public string from_add_building_name { get; set; }
    public string from_add_village_name { get; set; }
    public string from_add_village_no { get; set; }
    public string from_add_alley_name { get; set; }
    public string from_add_street_name { get; set; }
    public int? form_add_sub_district_id { get; set; }
    public string from_add_other_sub_district_name_th { get; set; }
    public string from_add_other_sub_district_name_en { get; set; }
    public string from_add_other_district_name_th { get; set; }
    public string from_add_other_district_name_en { get; set; }
    public string from_add_other_province_name_th { get; set; }
    public string from_add_other_province_name_en { get; set; }
    public string form_add_postal_code { get; set; }
    public int? form_add_country_id { get; set; }
    public string form_add_telephone_no { get; set; }
    public string to_add_house_no { get; set; }
    public string to_add_room_no { get; set; }
    public string to_add_floor_no { get; set; }
    public string to_add_building_name { get; set; }
    public string to_add_village_name { get; set; }
    public string to_add_village_no { get; set; }
    public string to_add_alley_name { get; set; }
    public string to_add_street_name { get; set; }
    public int? to_add_sub_district_id { get; set; }
    public string to_add_other_sub_district_name_th { get; set; }
    public string to_add_other_sub_district_name_en { get; set; }
    public string to_add_other_district_name_th { get; set; }
    public string to_add_other_district_name_en { get; set; }
    public string to_add_other_province_name_th { get; set; }
    public string to_add_other_province_name_en { get; set; }
    public string to_add_postal_code { get; set; }
    public int? to_add_country_id { get; set; }
    public string to_add_telephone_no { get; set; }

    public bool? is_address_mom { get; set; }
    public string url_address_mom { get; set; }
    public string from_adm_house_no { get; set; }
    public string from_adm_room_no { get; set; }
    public string from_adm_floor_no { get; set; }
    public string from_adm_building_name { get; set; }
    public string from_adm_village_name { get; set; }
    public string from_adm_village_no { get; set; }
    public string from_adm_alley_name { get; set; }
    public string from_adm_street_name { get; set; }
    public int? form_adm_sub_district_id { get; set; }
    public string from_adm_other_sub_district_name_th { get; set; }
    public string from_adm_other_sub_district_name_en { get; set; }
    public string from_adm_other_district_name_th { get; set; }
    public string from_adm_other_district_name_en { get; set; }
    public string from_adm_other_province_name_th { get; set; }
    public string from_adm_other_province_name_en { get; set; }
    public string form_adm_postal_code { get; set; }
    public int? form_adm_country_id { get; set; }
    public string form_adm_telephone_no { get; set; }
    public string to_adm_house_no { get; set; }
    public string to_adm_room_no { get; set; }
    public string to_adm_floor_no { get; set; }
    public string to_adm_building_name { get; set; }
    public string to_adm_village_name { get; set; }
    public string to_adm_village_no { get; set; }
    public string to_adm_alley_name { get; set; }
    public string to_adm_street_name { get; set; }
    public int? to_adm_sub_district_id { get; set; }
    public string to_adm_other_sub_district_name_th { get; set; }
    public string to_adm_other_sub_district_name_en { get; set; }
    public string to_adm_other_district_name_th { get; set; }
    public string to_adm_other_district_name_en { get; set; }
    public string to_adm_other_province_name_th { get; set; }
    public string to_adm_other_province_name_en { get; set; }
    public string to_adm_postal_code { get; set; }
    public int? to_adm_country_id { get; set; }
    public string to_adm_telephone_no { get; set; }

    public bool? is_guardian { get; set; }
    public string url_guardian { get; set; }
    public int? form_guardian_title_id { get; set; }
    public string form_guardian_first_name_th { get; set; }
    public string form_guardian_middle_name_th { get; set; }
    public string form_guardian_last_name_th { get; set; }
    public string form_guardian_first_name_en { get; set; }
    public string form_guardian_middle_name_en { get; set; }
    public string form_guardian_last_name_en { get; set; }
    public int? to_guardian_title_id { get; set; }
    public string to_guardian_first_name_th { get; set; }
    public string to_guardian_middle_name_th { get; set; }
    public string to_guardian_last_name_th { get; set; }
    public string to_guardian_first_name_en { get; set; }
    public string to_guardian_middle_name_en { get; set; }
    public string to_guardian_last_name_en { get; set; }

    public bool? is_address_guardian { get; set; }
    public string url_address_guardian { get; set; }
    public string from_adg_house_no { get; set; }
    public string from_adg_room_no { get; set; }
    public string from_adg_floor_no { get; set; }
    public string from_adg_building_name { get; set; }
    public string from_adg_village_name { get; set; }
    public string from_adg_village_no { get; set; }
    public string from_adg_alley_name { get; set; }
    public string from_adg_street_name { get; set; }
    public int? form_adg_sub_district_id { get; set; }
    public string from_adg_other_sub_district_name_th { get; set; }
    public string from_adg_other_sub_district_name_en { get; set; }
    public string from_adg_other_district_name_th { get; set; }
    public string from_adg_other_district_name_en { get; set; }
    public string from_adg_other_province_name_th { get; set; }
    public string from_adg_other_province_name_en { get; set; }
    public string form_adg_postal_code { get; set; }
    public int? form_adg_country_id { get; set; }
    public string form_adg_telephone_no { get; set; }
    public string to_adg_house_no { get; set; }
    public string to_adg_room_no { get; set; }
    public string to_adg_floor_no { get; set; }
    public string to_adg_building_name { get; set; }
    public string to_adg_village_name { get; set; }
    public string to_adg_village_no { get; set; }
    public string to_adg_alley_name { get; set; }
    public string to_adg_street_name { get; set; }
    public int? to_adg_sub_district_id { get; set; }
    public string to_adg_other_sub_district_name_th { get; set; }
    public string to_adg_other_sub_district_name_en { get; set; }
    public string to_adg_other_district_name_th { get; set; }
    public string to_adg_other_district_name_en { get; set; }
    public string to_adg_other_province_name_th { get; set; }
    public string to_adg_other_province_name_en { get; set; }
    public string to_adg_postal_code { get; set; }
    public int? to_adg_country_id { get; set; }
    public string to_adg_telephone_no { get; set; }

    public string name_url_address { get; set; }
    public string name_url_name_dad { get; set; }
    public string name_url_mom { get; set; }
    public string name_url_address_dad { get; set; }
    public string name_url_address_mom { get; set; }
    public string name_url_guardian { get; set; }
    public string name_url_address_guardian { get; set; }

    public string from_no { get; set; }
    public string to_no { get; set; }
    public string from_add_no { get; set; }
    public string to_add_no { get; set; }
    public string from_adm_no { get; set; }
    public string to_adm_no { get; set; }
    public string from_adg_no { get; set; }
    public string to_adg_no { get; set; }
  }

  [GeneratedUidController("api/student_request/student_request_name_and_address")]
  public class t_student_request_name_and_address : student_request_name_and_address
  {
    [JsonIgnore]public t_student_request student_request { get; set; }
  }

  public class v_student_request_name_and_address : student_request_name_and_address
  {
    [JsonIgnore]public v_student_request student_request { get; set; }
  }
}
