using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;
using SeventyOneDev.Utilities;
using SeventyOneDev.Utilities.Attributes;

namespace edu_api.Modules.StudentRequest.Databases.Models
{
  public class student_request:base_table
  {
    [Key]

    public Guid? student_request_uid { get; set; }
    public Guid? student_uid { get; set; }
    public string student_request_code{ get; set; }
    public Guid? request_type_uid { get; set; }
    public bool? is_consent { get; set; }
    public DateTime? request_date { get; set; }
    public int? student_request_status { get; set; }
    public decimal? total_amout {get;set;}
    public Guid? bill_uid { get; set; }
    public string bill_no {get;set;}
    
  }
  // [GeneratedUidController("api/student_request/student_request")]
  public class t_student_request : student_request
  {
    [JsonIgnore]public t_request_type request_type { get; set; }
    [JsonIgnore]public List<t_transfer_all_request> transfer_all_requests { get; set; }
    [JsonIgnore]public List<t_transfer_partial_request> transfer_partial_requests { get; set; }
    [JsonIgnore]public List<t_transfer_new_request> transfer_new_requests { get; set; }
    [JsonIgnore]public List<t_transfer_institute_request> transfer_institute_requests { get; set; }
    [JsonIgnore]public List<t_transfer_degree_request> transfer_degree_requests { get; set; }
    [JsonIgnore]public List<t_transfer_leisure_request> transfer_leisure_requests { get; set; }
    [JsonIgnore]public List<t_leave_status_request> leave_status_requests { get; set; }
    [JsonIgnore]public List<t_student_card_request> student_card_requests { get; set; }
    
    [JsonIgnore]public List<t_student_request_name_and_address> student_request_name_and_addresses { get; set; }
  }

  public class v_student_request : student_request
  {
    public string request_type_code { get; set; }
    public string request_type_name_th { get; set; }
    public string request_type_name_en { get; set; }
    
    public string student_code { get; set; }
    public string display_name_th { get; set; }
    public decimal? cumulative_gpa { get; set; }
    
    public string faculty_curriculum_name_th { get; set; }
    public string faculty_curriculum_name_en { get; set; }
    public string college_faculty_name_th { get; set; }
    public string college_faculty_name_en { get; set; }

    public Guid? receipt_uid { get; set; }
    [Include]public v_request_type request_type { get; set; }
    [Include]public List<v_transfer_all_request> transfer_all_requests { get; set; }
    [Include]public List<v_transfer_partial_request> transfer_partial_requests { get; set; }
    [Include]public List<v_transfer_new_request> transfer_new_requests { get; set; }
    [Include]public List<v_transfer_institute_request> transfer_institute_requests { get; set; }
    [Include]public List<v_transfer_degree_request> transfer_degree_requests { get; set; }
    [Include]public List<v_transfer_leisure_request> transfer_leisure_requests { get; set; }
    [Include]public List<v_leave_status_request> leave_status_requests { get; set; }
    [Include]public List<v_student_card_request> student_card_requests { get; set; }
    [Include]public List<v_student_request_name_and_address> student_request_name_and_addresses { get; set; }
  }

}
