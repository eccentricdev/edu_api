namespace edu_api.Modules.StudentRequest.Databases.Models
{
  public class name_change_request
  {
   public bool? is_student_name_change { get; set; }
   public bool? is_address_change { get; set; }
   public bool? is_father_name_change { get; set; }
   public bool? is_mother_name_change { get; set; }
   public bool? is_father_address_change { get; set; }
   public bool? is_mother_address_change { get; set; }
   public bool? is_parent_name_change { get; set; }
   public bool? is_parent_address_change { get; set; }
  }
}
