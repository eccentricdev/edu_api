using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;
using SeventyOneDev.Utilities;
using SeventyOneDev.Utilities.Attributes;

namespace edu_api.Modules.StudentRequest.Databases.Models
{
  public class transfer_institute_request:base_table
  {
    [Key]
    public Guid? transfer_institute_request_uid { get; set; }
    public Guid? student_request_uid { get; set; }
    public string mobile_no { get; set; }
    public string email { get; set; }
    public string highest_education_level_name { get; set; }
    public string institute_name { get; set; }
    public string faculty_name { get; set; }
    public string major_name { get; set; }
    [Column(TypeName = "decimal(5,2)")]public decimal? gpa { get; set; }
    [MaxLength(4)]public string start_year { get; set; }
    [MaxLength(4)]public string last_year { get; set; }
    public Guid? leave_reason_uid { get; set; }
    public string other_reason_name { get; set; }
    public Guid? academic_year_uid { get; set; }
    public Guid? academic_semester_uid { get; set; }
  }
  [GeneratedUidController("api/student_request/transfer_institute_request")]
  public class t_transfer_institute_request : transfer_institute_request
  {
    [JsonIgnore]public t_student_request student_request { get; set; }
    [Include]public List<t_transfer_institute_subject> transfer_institute_subjects { get; set; }
    [Include]public List<t_transfer_institute_document> transfer_institute_documents { get; set; }
  }

  public class v_transfer_institute_request : transfer_institute_request
  {
    [JsonIgnore]public v_student_request student_request { get; set; }
    [Include]public List<v_transfer_institute_subject> transfer_institute_subjects { get; set; }
    [Include]public List<v_transfer_institute_document> transfer_institute_documents { get; set; }
  }

}
