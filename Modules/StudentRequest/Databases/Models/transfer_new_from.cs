using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;
using SeventyOneDev.Utilities;
using SeventyOneDev.Utilities.Attributes;

namespace edu_api.Modules.StudentRequest.Databases.Models
{
  public class transfer_new_from:base_table
  {
    [Key]
    public Guid? transfer_new_from_uid { get; set; }
    public Guid? transfer_new_request_uid { get; set; }
    public Guid? student_uid { get; set; }
    public string student_code { get; set; }
  }

  public class t_transfer_new_from : transfer_new_from
  {
    [JsonIgnore]public t_transfer_new_request transfer_new_request { get; set; }
    [Include]public List<t_transfer_new_subject> transfer_new_subjects { get; set; }
  }

  public class v_transfer_new_from : transfer_new_from
  {
    [JsonIgnore]public v_transfer_new_request transfer_new_request { get; set; }
    [Include]public List<v_transfer_new_subject> transfer_new_subjects { get; set; }
  }
}
