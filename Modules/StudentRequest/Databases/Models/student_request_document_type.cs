using System;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;
using SeventyOneDev.Utilities;
using SeventyOneDev.Utilities.Attributes;

namespace edu_api.Modules.StudentRequest.Databases.Models
{
  public class student_request_document_type:base_table
  {
    [Key]
    public Guid? student_request_document_type_uid { get; set; }
    public Guid? request_type_uid { get; set; }
    public Guid? request_document_type_uid { get; set; }
    public bool? is_required { get; set; }
    public int? row_order { get; set; }
  }
  [GeneratedUidController("api/student_request/student_request_document_type")]
  public class t_student_request_document_type : student_request_document_type
  {
    [JsonIgnore]public t_request_document_type request_document_type { get; set; }
    [JsonIgnore]public t_request_type request_type { get; set; }
  }

  public class v_student_request_document_type : student_request_document_type
  {
    public string request_document_type_code { get; set; }
    public string request_document_type_name_th { get; set; }
    public string request_document_type_name_en { get; set; }
    [JsonIgnore]public v_request_document_type request_document_type { get; set; }
    [JsonIgnore]public v_request_type request_type { get; set; }
  }

}
