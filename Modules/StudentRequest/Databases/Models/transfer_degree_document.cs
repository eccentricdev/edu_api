using System;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;
using SeventyOneDev.Utilities;

namespace edu_api.Modules.StudentRequest.Databases.Models
{
  public class transfer_degree_document:base_table
  {
    [Key]
    public Guid? transfer_degree_document_uid { get; set; }
    public Guid? transfer_degree_request_uid { get; set; }
    public Guid? request_document_type_uid {get;set;}
    public string request_document_type_name  {get;set;}
    public string file_name {get;set;}
    public string document_name  {get;set;}
    public string document_url  {get;set;}
    public string admin_file_name {get;set;}
    public string admin_document_url  {get;set;}
    [MaxLength(50)]public string mime_type {get;set;}
    public int? document_status_id {get;set;}
  }
  public class t_transfer_degree_document : transfer_degree_document
  {
    [JsonIgnore]public t_transfer_degree_request transfer_degree_request { get; set; }
    [JsonIgnore]public t_request_document_type request_document_type { get; set; }
  }

  public class v_transfer_degree_document : transfer_degree_document
  {
    [JsonIgnore]public v_transfer_degree_request transfer_degree_request { get; set; }
    [JsonIgnore]public v_request_document_type request_document_type { get; set; }
  }
}
