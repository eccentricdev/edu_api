using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;
using SeventyOneDev.Utilities;
using SeventyOneDev.Utilities.Attributes;

namespace edu_api.Modules.StudentRequest.Databases.Models
{
  public class request_type:base_table
  {
    [Key]
    public Guid? request_type_uid { get; set; }
    public string request_type_code { get; set; }
    public string request_type_name_th { get; set; }
    public string request_type_name_en { get; set; }
  }
  // [GeneratedUidController("api/student_request/request_type")]
  public class t_request_type : request_type
  {
    [JsonIgnore]public List<t_student_request> student_requests { get; set; }
    [JsonIgnore]public List<t_student_request_document_type> student_request_document_types { get; set; }
  }

  public class v_request_type : request_type
  {
    [JsonIgnore]public List<v_student_request> student_requests { get; set; }
    [JsonIgnore]public List<v_student_request_document_type> student_request_document_types { get; set; }

    [NotMapped]
     public decimal? request_cost_amount {get;set;}
    [NotMapped]
     public string request_cost_type_name_th {get;set;}
     [NotMapped]
     public string request_cost_updated_by { get; set; }
     [NotMapped]
     public DateTime? request_cost_updated_datetime { get; set; }
  }
}
