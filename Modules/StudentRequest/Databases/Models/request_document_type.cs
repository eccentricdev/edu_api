using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;
using SeventyOneDev.Utilities;
using SeventyOneDev.Utilities.Attributes;

namespace edu_api.Modules.StudentRequest.Databases.Models
{
  public class request_document_type:base_table
  {
    [Key]
    public Guid? request_document_type_uid { get; set; }
    public string request_document_type_code { get; set; }
    public string request_document_type_name_th { get; set; }
    public string request_document_type_name_en { get; set; }
    public bool? allow_multiple { get; set; }
  }
  [GeneratedUidController("api/student_request/request_document_type")]
  public class t_request_document_type : request_document_type
  {
    [JsonIgnore]public List<t_student_request_document_type> student_request_document_types { get; set; }
    [JsonIgnore]public List<t_transfer_leisure_document> transfer_leisure_documents { get; set; }
    [JsonIgnore]public List<t_transfer_degree_document> transfer_degree_documents { get; set; }
    [JsonIgnore]public List<t_transfer_institute_document> transfer_institute_documents { get; set; }
    [JsonIgnore]public List<t_transfer_leisure_consideration_document> transfer_leisure_consideration_documents { get; set; }
  }

  public class v_request_document_type : request_document_type
  {
    [JsonIgnore]public List<v_student_request_document_type> student_request_document_types { get; set; }
    [JsonIgnore]public List<v_transfer_leisure_document> transfer_leisure_documents { get; set; }
    [JsonIgnore]public List<v_transfer_degree_document> transfer_degree_documents { get; set; }
    [JsonIgnore]public List<v_transfer_institute_document> transfer_institute_documents { get; set; }
    [JsonIgnore]public List<v_transfer_leisure_consideration_document> transfer_leisure_consideration_documents { get; set; }
  }
}
