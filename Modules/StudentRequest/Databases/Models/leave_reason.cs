using System;
using System.ComponentModel.DataAnnotations;
using SeventyOneDev.Utilities;
using SeventyOneDev.Utilities.Attributes;

namespace edu_api.Modules.StudentRequest.Databases.Models
{
    public class leave_reason:base_table
    {
        [Key]
        public Guid? leave_reason_id {get;set;}
        public string leave_reason_code {get;set;}
        [Search]
        public string leave_reason_name_th {get;set;}
        [Search]
        public string leave_reason_name_en {get;set;}
    }
    [GeneratedUidController("api/student_request/leave_reason")]
    public class t_leave_reason : leave_reason
    {

    }
    public class v_leave_reason : leave_reason
    {

    }
}