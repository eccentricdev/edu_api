using System;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;
using SeventyOneDev.Utilities;

namespace edu_api.Modules.StudentRequest.Databases.Models
{
  public class transfer_new_subject:base_table
  {
    [Key]
    public Guid? transfer_new_subject_uid { get; set; }
    public Guid? transfer_new_from_uid { get; set; }
    public Guid? final_register_subject_uid { get; set; }
    public Guid? to_year_subject_uid { get; set; }
    public Guid? to_grade_uid { get; set; }
    public string remark { get; set; }
  }

  public class t_transfer_new_subject : transfer_new_subject
  {
    [JsonIgnore]public t_transfer_new_from transfer_new_from { get; set; }
  }

  public class v_transfer_new_subject : transfer_new_subject
  {
    public Guid? grade_uid { get; set; }
    public decimal? grade_point { get; set; }
    public Guid? subject_uid { get; set; }
    public string subject_code { get; set; }
    public string subject_name_th {get;set;}
    public short? credit { get; set; }
    public string to_year_subject_code { get; set; }
    public string to_year_subject_name_th { get; set; }
    public string to_grade_code { get; set; }
    public string to_grade_name_th { get; set; }
    public string to_grade_point { get; set; }
    [JsonIgnore]public v_transfer_new_from transfer_new_from { get; set; }
  }
}
