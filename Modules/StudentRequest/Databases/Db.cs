using edu_api.Modules.Grade.Databases.Models;
using edu_api.Modules.Profiles.Databases.Models;
using edu_api.Modules.StudentRequest.Databases.Models;
using Microsoft.EntityFrameworkCore;
using rsu_common_api.models;

namespace SeventyOneDev.Utilities
{
  public partial class Db : DbContext
  {
    public DbSet<t_request_type> t_request_type { get; set; }
    public DbSet<v_request_type> v_request_type { get; set; }

    public DbSet<t_request_document_type> t_request_document_type { get; set; }
    public DbSet<v_request_document_type> v_request_document_type { get; set; }

    public DbSet<t_student_request_document_type> t_student_request_document_type { get; set; }
    public DbSet<v_student_request_document_type> v_student_request_document_type { get; set; }
    public DbSet<t_student_request> t_student_request { get; set; }
    public DbSet<v_student_request> v_student_request { get; set; }
    public DbSet<t_student_request_name_and_address> t_student_request_name_and_address { get; set; }
    public DbSet<v_student_request_name_and_address> v_student_request_name_and_address { get; set; }
    public DbSet<t_transfer_all_request> t_transfer_all_request { get; set; }
    public DbSet<v_transfer_all_request> v_transfer_all_request { get; set; }

    public DbSet<t_transfer_partial_request> t_transfer_partial_request { get; set; }
    public DbSet<v_transfer_partial_request> v_transfer_partial_request { get; set; }
    public DbSet<t_transfer_partial_subject> t_transfer_partial_subject { get; set; }
    public DbSet<v_transfer_partial_subject> v_transfer_partial_subject { get; set; }

    public DbSet<t_transfer_new_request> t_transfer_new_request { get; set; }
    public DbSet<v_transfer_new_request> v_transfer_new_request { get; set; }
    public DbSet<t_transfer_new_from> t_transfer_new_from { get; set; }
    public DbSet<v_transfer_new_from> v_transfer_new_from { get; set; }
    public DbSet<t_transfer_new_subject> t_transfer_new_subject { get; set; }
    public DbSet<v_transfer_new_subject> v_transfer_new_subject { get; set; }

    public DbSet<t_transfer_degree_request> t_transfer_degree_request { get; set; }
    public DbSet<v_transfer_degree_request> v_transfer_degree_request { get; set; }
    public DbSet<t_transfer_degree_subject> t_transfer_degree_subject { get; set; }
    public DbSet<v_transfer_degree_subject> v_transfer_degree_subject { get; set; }
    public DbSet<t_transfer_degree_document> t_transfer_degree_document { get; set; }
    public DbSet<v_transfer_degree_document> v_transfer_degree_document { get; set; }

    public DbSet<t_transfer_institute_request> t_transfer_institute_request { get; set; }
    public DbSet<v_transfer_institute_request> v_transfer_institute_request { get; set; }
    public DbSet<t_transfer_institute_subject> t_transfer_institute_subject { get; set; }
    public DbSet<v_transfer_institute_subject> v_transfer_institute_subject { get; set; }
    public DbSet<t_transfer_institute_document> t_transfer_institute_document { get; set; }
    public DbSet<v_transfer_institute_document> v_transfer_institute_document { get; set; }

    public DbSet<t_transfer_leisure_request> t_transfer_leisure_request { get; set; }
    public DbSet<v_transfer_leisure_request> v_transfer_leisure_request { get; set; }
    public DbSet<t_transfer_leisure_subject> t_transfer_leisure_subject { get; set; }
    public DbSet<v_transfer_leisure_subject> v_transfer_leisure_subject { get; set; }
    public DbSet<t_transfer_leisure_document> t_transfer_leisure_document { get; set; }
    public DbSet<v_transfer_leisure_document> v_transfer_leisure_document { get; set; } 
    public DbSet<t_transfer_leisure_consideration_document> t_transfer_leisut_transfer_leisure_consideration_documentre_document { get; set; }
    public DbSet<v_transfer_leisure_consideration_document> v_transfer_leisure_consideration_document { get; set; }

    public DbSet<t_leave_status_request> t_leave_status_request { get; set; }
    public DbSet<v_leave_status_request> v_leave_status_request { get; set; }
    public DbSet<t_leave_status_document> t_leave_status_document { get; set; }
    public DbSet<v_leave_status_document> v_leave_status_document { get; set; }
    public DbSet<t_student_card_request> t_student_card_request { get; set; }
    public DbSet<v_student_card_request> v_student_card_request { get; set; }
    public DbSet<t_student_card_document> t_student_card_document { get; set; }
    public DbSet<v_student_card_document> v_student_card_document { get; set; }
    
    public DbSet<t_bank> t_bank { get; set; }
    public DbSet<v_bank> v_bank { get; set; }
    
    public DbSet<t_leave_reason> t_leave_reason { get; set; }
    public DbSet<v_leave_reason> v_leave_reason { get; set; }

    public DbSet<t_request_cost_type> t_request_cost_type { get; set; }
    public DbSet<v_request_cost_type> v_request_cost_type { get; set; }

    public DbSet<t_request_cost> t_request_cost { get; set; }
    public DbSet<v_request_cost> v_request_cost { get; set; }

    private void OnStudentRequestModelCreating(ModelBuilder builder, string schema)
    {

      builder.Entity<t_request_type>().ToTable("request_type", schema);
      builder.Entity<v_request_type>().ToView("v_request_type", schema);

      builder.Entity<t_request_document_type>().ToTable("request_document_type", schema);
      builder.Entity<v_request_document_type>().ToView("v_request_document_type", schema);

      builder.Entity<t_student_request_document_type>().ToTable("student_request_document_type", schema);
      builder.Entity<t_student_request_document_type>().HasOne(s => s.request_type)
        .WithMany(s => s.student_request_document_types).HasForeignKey(s => s.request_type_uid)
        .OnDelete(DeleteBehavior.Restrict);
      builder.Entity<t_student_request_document_type>().HasOne(s => s.request_document_type)
        .WithMany(s => s.student_request_document_types).HasForeignKey(s => s.request_document_type_uid)
        .OnDelete(DeleteBehavior.Restrict);
      builder.Entity<v_student_request_document_type>().ToView("v_student_request_document_type", schema);
      builder.Entity<v_student_request_document_type>().HasOne(s => s.request_type)
        .WithMany(s => s.student_request_document_types).HasForeignKey(s => s.request_type_uid)
        .OnDelete(DeleteBehavior.Restrict);
      builder.Entity<v_student_request_document_type>().HasOne(s => s.request_document_type)
        .WithMany(s => s.student_request_document_types).HasForeignKey(s => s.request_document_type_uid)
        .OnDelete(DeleteBehavior.Restrict);


      builder.Entity<t_student_request>().ToTable("student_request", schema);
      builder.Entity<t_student_request>().HasOne(s => s.request_type).WithMany(s => s.student_requests)
        .HasForeignKey(s => s.request_type_uid).OnDelete(DeleteBehavior.Restrict);
      builder.Entity<v_student_request>().ToView("v_student_request", schema);
      builder.Entity<v_student_request>().HasOne(s => s.request_type).WithMany(s => s.student_requests)
        .HasForeignKey(s => s.request_type_uid).OnDelete(DeleteBehavior.Restrict);
      builder.Entity<t_transfer_all_request>().ToTable("transfer_all_request", schema);
      builder.Entity<t_transfer_all_request>().HasOne(t => t.student_request).WithMany(t => t.transfer_all_requests)
        .HasForeignKey(t => t.student_request_uid).OnDelete(DeleteBehavior.Cascade);
      builder.Entity<v_transfer_all_request>().ToView("v_transfer_all_request", schema);
      builder.Entity<v_transfer_all_request>().HasOne(t => t.student_request).WithMany(t => t.transfer_all_requests)
        .HasForeignKey(t => t.student_request_uid).OnDelete(DeleteBehavior.Cascade);
      
      builder.Entity<t_student_request_name_and_address>().ToTable("student_request_name_and_address", schema);
      builder.Entity<t_student_request_name_and_address>().HasOne(t => t.student_request).WithMany(t => t.student_request_name_and_addresses)
        .HasForeignKey(t => t.student_request_uid).OnDelete(DeleteBehavior.Cascade);
      
      builder.Entity<v_student_request_name_and_address>().ToView("v_student_request_name_and_address", schema);
      builder.Entity<v_student_request_name_and_address>().HasOne(t => t.student_request).WithMany(t => t.student_request_name_and_addresses)
        .HasForeignKey(t => t.student_request_uid).OnDelete(DeleteBehavior.Cascade);


      builder.Entity<t_transfer_partial_request>().ToTable("transfer_partial_request", schema);
      builder.Entity<t_transfer_partial_request>().HasOne(t => t.student_request).WithMany(t => t.transfer_partial_requests)
        .HasForeignKey(t => t.student_request_uid).OnDelete(DeleteBehavior.Cascade);
      builder.Entity<v_transfer_partial_request>().ToView("v_transfer_partial_request", schema);
      builder.Entity<v_transfer_partial_request>().HasOne(t => t.student_request).WithMany(t => t.transfer_partial_requests)
        .HasForeignKey(t => t.student_request_uid).OnDelete(DeleteBehavior.Cascade);

      builder.Entity<t_transfer_partial_subject>().ToTable("transfer_partial_subject", schema);
      builder.Entity<t_transfer_partial_subject>().HasOne(t => t.transfer_partial_request)
        .WithMany(t => t.transfer_partial_subjects).HasForeignKey(t => t.transfer_partial_request_uid).OnDelete(DeleteBehavior.Cascade);

      builder.Entity<v_transfer_partial_subject>().ToView("v_transfer_partial_subject", schema);
      builder.Entity<v_transfer_partial_subject>().HasOne(t => t.transfer_partial_request)
        .WithMany(t => t.transfer_partial_subjects).HasForeignKey(t => t.transfer_partial_request_uid).OnDelete(DeleteBehavior.Cascade);

      builder.Entity<t_transfer_new_request>().ToTable("transfer_new_request", schema);
      builder.Entity<t_transfer_new_request>().HasOne(t => t.student_request).WithMany(t => t.transfer_new_requests)
        .HasForeignKey(t => t.student_request_uid).OnDelete(DeleteBehavior.Cascade);
      builder.Entity<v_transfer_new_request>().ToView("v_transfer_new_request", schema);
      builder.Entity<v_transfer_new_request>().HasOne(t => t.student_request).WithMany(t => t.transfer_new_requests)
        .HasForeignKey(t => t.student_request_uid).OnDelete(DeleteBehavior.Cascade);

      builder.Entity<t_transfer_new_from>().ToTable("transfer_new_from", schema);
      builder.Entity<t_transfer_new_from>().HasOne(t => t.transfer_new_request)
        .WithMany(t => t.transfer_new_froms).HasForeignKey(t => t.transfer_new_request_uid).OnDelete(DeleteBehavior.Cascade);

      builder.Entity<v_transfer_new_from>().ToView("v_transfer_new_from", schema);
      builder.Entity<v_transfer_new_from>().HasOne(t => t.transfer_new_request)
        .WithMany(t => t.transfer_new_froms).HasForeignKey(t => t.transfer_new_request_uid).OnDelete(DeleteBehavior.Cascade);


      builder.Entity<t_transfer_new_subject>().ToTable("transfer_new_subject", schema);
      builder.Entity<t_transfer_new_subject>().HasOne(t => t.transfer_new_from)
        .WithMany(t => t.transfer_new_subjects).HasForeignKey(t => t.transfer_new_from_uid).OnDelete(DeleteBehavior.Cascade);

      builder.Entity<v_transfer_new_subject>().ToView("v_transfer_new_subject", schema);
      builder.Entity<v_transfer_new_subject>().HasOne(t => t.transfer_new_from)
        .WithMany(t => t.transfer_new_subjects).HasForeignKey(t => t.transfer_new_from_uid).OnDelete(DeleteBehavior.Cascade);


      builder.Entity<t_transfer_degree_request>().ToTable("transfer_degree_request", schema);
      builder.Entity<t_transfer_degree_request>().HasOne(t => t.student_request).WithMany(t => t.transfer_degree_requests)
        .HasForeignKey(t => t.student_request_uid).OnDelete(DeleteBehavior.Cascade);
      builder.Entity<v_transfer_degree_request>().ToView("v_transfer_degree_request", schema);
      builder.Entity<v_transfer_degree_request>().HasOne(t => t.student_request).WithMany(t => t.transfer_degree_requests)
        .HasForeignKey(t => t.student_request_uid).OnDelete(DeleteBehavior.Cascade);

      builder.Entity<t_transfer_degree_subject>().ToTable("transfer_degree_subject", schema);
      builder.Entity<t_transfer_degree_subject>().HasOne(t => t.transfer_degree_request).WithMany(t => t.transfer_degree_subjects)
        .HasForeignKey(t => t.transfer_degree_request_uid).OnDelete(DeleteBehavior.Cascade);
      builder.Entity<v_transfer_degree_subject>().ToView("v_transfer_degree_subject", schema);
      builder.Entity<v_transfer_degree_subject>().HasOne(t => t.transfer_degree_request).WithMany(t => t.transfer_degree_subjects)
        .HasForeignKey(t => t.transfer_degree_request_uid).OnDelete(DeleteBehavior.Cascade);

      builder.Entity<t_transfer_degree_document>().ToTable("transfer_degree_document", schema);
      builder.Entity<t_transfer_degree_document>().HasOne(t => t.transfer_degree_request).WithMany(t => t.transfer_degree_documents)
        .HasForeignKey(t => t.transfer_degree_request_uid).OnDelete(DeleteBehavior.Cascade);
      builder.Entity<t_transfer_degree_document>().HasOne(t => t.request_document_type)
        .WithMany(t => t.transfer_degree_documents).HasForeignKey(t => t.request_document_type_uid)
        .OnDelete(DeleteBehavior.Restrict);
      builder.Entity<v_transfer_degree_document>().ToView("v_transfer_degree_document", schema);
      builder.Entity<v_transfer_degree_document>().HasOne(t => t.transfer_degree_request).WithMany(t => t.transfer_degree_documents)
        .HasForeignKey(t => t.transfer_degree_request_uid).OnDelete(DeleteBehavior.Cascade);
      builder.Entity<v_transfer_degree_document>().HasOne(t => t.request_document_type)
        .WithMany(t => t.transfer_degree_documents).HasForeignKey(t => t.request_document_type_uid)
        .OnDelete(DeleteBehavior.Restrict);

      builder.Entity<t_transfer_institute_request>().ToTable("transfer_institute_request", schema);
      builder.Entity<t_transfer_institute_request>().HasOne(t => t.student_request).WithMany(t => t.transfer_institute_requests)
        .HasForeignKey(t => t.student_request_uid).OnDelete(DeleteBehavior.Cascade);
      builder.Entity<v_transfer_institute_request>().ToView("v_transfer_institute_request", schema);
      builder.Entity<v_transfer_institute_request>().HasOne(t => t.student_request).WithMany(t => t.transfer_institute_requests)
        .HasForeignKey(t => t.student_request_uid).OnDelete(DeleteBehavior.Cascade);

      builder.Entity<t_transfer_institute_subject>().ToTable("transfer_institute_subject", schema);
      builder.Entity<t_transfer_institute_subject>().HasOne(t => t.transfer_institute_request).WithMany(t => t.transfer_institute_subjects)
        .HasForeignKey(t => t.transfer_institute_request_uid).OnDelete(DeleteBehavior.Cascade);
      builder.Entity<v_transfer_institute_subject>().ToView("v_transfer_institute_subject", schema);
      builder.Entity<v_transfer_institute_subject>().HasOne(t => t.transfer_institute_request).WithMany(t => t.transfer_institute_subjects)
        .HasForeignKey(t => t.transfer_institute_request_uid).OnDelete(DeleteBehavior.Cascade);

      builder.Entity<t_transfer_institute_document>().ToTable("transfer_institute_document", schema);
      builder.Entity<t_transfer_institute_document>().HasOne(t => t.transfer_institute_request).WithMany(t => t.transfer_institute_documents)
        .HasForeignKey(t => t.transfer_institute_request_uid).OnDelete(DeleteBehavior.Cascade);
      builder.Entity<t_transfer_institute_document>().HasOne(t => t.request_document_type)
        .WithMany(t => t.transfer_institute_documents).HasForeignKey(t => t.request_document_type_uid)
        .OnDelete(DeleteBehavior.Restrict);
      builder.Entity<v_transfer_institute_document>().ToView("v_transfer_institute_document", schema);
      builder.Entity<v_transfer_institute_document>().HasOne(t => t.transfer_institute_request).WithMany(t => t.transfer_institute_documents)
        .HasForeignKey(t => t.transfer_institute_request_uid).OnDelete(DeleteBehavior.Cascade);
      builder.Entity<v_transfer_institute_document>().HasOne(t => t.request_document_type)
        .WithMany(t => t.transfer_institute_documents).HasForeignKey(t => t.request_document_type_uid)
        .OnDelete(DeleteBehavior.Restrict);


      builder.Entity<t_transfer_leisure_request>().ToTable("transfer_leisure_request", schema);
      builder.Entity<t_transfer_leisure_request>().HasOne(t => t.student_request).WithMany(t => t.transfer_leisure_requests)
        .HasForeignKey(t => t.student_request_uid).OnDelete(DeleteBehavior.Cascade);
      builder.Entity<v_transfer_leisure_request>().ToView("v_transfer_leisure_request", schema);
      builder.Entity<v_transfer_leisure_request>().HasOne(t => t.student_request).WithMany(t => t.transfer_leisure_requests)
        .HasForeignKey(t => t.student_request_uid).OnDelete(DeleteBehavior.Cascade);

      builder.Entity<t_transfer_leisure_subject>().ToTable("transfer_leisure_subject", schema);
      builder.Entity<t_transfer_leisure_subject>().HasOne(t => t.transfer_leisure_request).WithMany(t => t.transfer_leisure_subjects)
        .HasForeignKey(t => t.transfer_leisure_request_uid).OnDelete(DeleteBehavior.Cascade);
      builder.Entity<v_transfer_leisure_subject>().ToView("v_transfer_leisure_subject", schema);
      builder.Entity<v_transfer_leisure_subject>().HasOne(t => t.transfer_leisure_request).WithMany(t => t.transfer_leisure_subjects)
        .HasForeignKey(t => t.transfer_leisure_request_uid).OnDelete(DeleteBehavior.Cascade);

      builder.Entity<t_transfer_leisure_document>().ToTable("transfer_leisure_document", schema);
      builder.Entity<t_transfer_leisure_document>().HasOne(t => t.transfer_leisure_request).WithMany(t => t.transfer_leisure_documents)
        .HasForeignKey(t => t.transfer_leisure_request_uid).OnDelete(DeleteBehavior.Cascade);
      builder.Entity<t_transfer_leisure_document>().HasOne(t => t.request_document_type)
        .WithMany(t => t.transfer_leisure_documents).HasForeignKey(t => t.request_document_type_uid)
        .OnDelete(DeleteBehavior.Restrict);
      builder.Entity<v_transfer_leisure_document>().ToView("v_transfer_leisure_document", schema);
      builder.Entity<v_transfer_leisure_document>().HasOne(t => t.transfer_leisure_request).WithMany(t => t.transfer_leisure_documents)
        .HasForeignKey(t => t.transfer_leisure_request_uid).OnDelete(DeleteBehavior.Cascade);
      builder.Entity<v_transfer_leisure_document>().HasOne(t => t.request_document_type)
        .WithMany(t => t.transfer_leisure_documents).HasForeignKey(t => t.request_document_type_uid)
        .OnDelete(DeleteBehavior.Restrict);
      
      builder.Entity<t_transfer_leisure_consideration_document>().ToTable("transfer_leisure_consideration_document", schema);
      builder.Entity<t_transfer_leisure_consideration_document>().HasOne(t => t.transfer_leisure_request).WithMany(t => t.transfer_leisure_consideration_documents)
        .HasForeignKey(t => t.transfer_leisure_request_uid).OnDelete(DeleteBehavior.Cascade);
      builder.Entity<t_transfer_leisure_consideration_document>().HasOne(t => t.request_document_type)
        .WithMany(t => t.transfer_leisure_consideration_documents).HasForeignKey(t => t.request_document_type_uid)
        .OnDelete(DeleteBehavior.Restrict);
      builder.Entity<v_transfer_leisure_consideration_document>().ToView("v_transfer_leisure_consideration_document", schema);
      builder.Entity<v_transfer_leisure_consideration_document>().HasOne(t => t.transfer_leisure_request).WithMany(t => t.transfer_leisure_consideration_documents)
        .HasForeignKey(t => t.transfer_leisure_request_uid).OnDelete(DeleteBehavior.Cascade);
      builder.Entity<v_transfer_leisure_consideration_document>().HasOne(t => t.request_document_type)
        .WithMany(t => t.transfer_leisure_consideration_documents).HasForeignKey(t => t.request_document_type_uid)
        .OnDelete(DeleteBehavior.Restrict);


      builder.Entity<t_leave_status_request>().ToTable("leave_status_request", schema);
      builder.Entity<t_leave_status_request>().HasOne(t => t.student_request).WithMany(t => t.leave_status_requests)
        .HasForeignKey(t => t.student_request_uid).OnDelete(DeleteBehavior.Cascade);
      builder.Entity<v_leave_status_request>().ToView("v_leave_status_request", schema);
      builder.Entity<v_leave_status_request>().HasOne(t => t.student_request).WithMany(t => t.leave_status_requests)
        .HasForeignKey(t => t.student_request_uid).OnDelete(DeleteBehavior.Cascade);

      builder.Entity<t_leave_status_document>().ToTable("leave_status_document", schema);
      builder.Entity<t_leave_status_document>().HasOne(t => t.leave_status_request).WithMany(t => t.leave_status_documents)
        .HasForeignKey(t => t.leave_status_request_uid).OnDelete(DeleteBehavior.Cascade);
      builder.Entity<v_leave_status_document>().ToView("v_leave_status_document", schema);
      builder.Entity<v_leave_status_document>().HasOne(t => t.leave_status_request).WithMany(t => t.leave_status_documents)
        .HasForeignKey(t => t.leave_status_request_uid).OnDelete(DeleteBehavior.Cascade);


      builder.Entity<t_student_card_request>().ToTable("student_card_request", schema);
      builder.Entity<t_student_card_request>().HasOne(t => t.student_request).WithMany(t => t.student_card_requests)
        .HasForeignKey(t => t.student_request_uid).OnDelete(DeleteBehavior.Cascade);
      builder.Entity<v_student_card_request>().ToView("v_student_card_request", schema);
      builder.Entity<v_student_card_request>().HasOne(t => t.student_request).WithMany(t => t.student_card_requests)
        .HasForeignKey(t => t.student_request_uid).OnDelete(DeleteBehavior.Cascade);

      builder.Entity<t_student_card_document>().ToTable("student_card_document", schema);
      builder.Entity<t_student_card_document>().HasOne(t => t.student_card_request).WithMany(t => t.student_card_documents)
        .HasForeignKey(t => t.student_card_request_uid).OnDelete(DeleteBehavior.Cascade);
      builder.Entity<v_student_card_document>().ToView("v_student_card_document", schema);
      builder.Entity<v_student_card_document>().HasOne(t => t.student_card_request).WithMany(t => t.student_card_documents)
        .HasForeignKey(t => t.student_card_request_uid).OnDelete(DeleteBehavior.Cascade);
      
      
      builder.Entity<t_bank>().ToTable("bank", schema);
      builder.Entity<v_bank>().ToView("v_bank", schema); 
      
      builder.Entity<t_leave_reason>().ToTable("leave_reason", schema);
      builder.Entity<v_leave_reason>().ToView("v_leave_reason", schema);

      builder.Entity<t_request_cost_type>().ToTable("request_cost_type", schema);
      builder.Entity<v_request_cost_type>().ToView("v_request_cost_type", schema);

      builder.Entity<t_request_cost>().ToTable("request_cost", schema);
      builder.Entity<v_request_cost>().ToView("v_request_cost", schema);

    }
  }
}
