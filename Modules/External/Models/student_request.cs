namespace edu_api.Modules.External.Models
{
  public class student_request
  {
    public string student_code { get; set; }
    public string citizen_id { get; set; }
    public string tax_id { get; set; }
    public string passport_no { get; set; }
    public string uid_no { get; set; }
  }
}
