using System;
using edu_api.Modules.Organizations.Databases.Models;

namespace edu_api.Modules.External.Controllers
{
  public class major
  {
    public Guid? major_uid { get; set; }
    public Guid? education_type_uid { get; set; }
    public Guid? faculty_uid { get; set; }
    //public int? education_type_id {get;set;}
    //public int? faculty_id {get;set;}
    public string major_code {get;set;}
    public string major_name_th {get;set;}
    public string major_name_en {get;set;}
    public string remark {get;set;}
    public string education_type_code {get;set;}
    public string faculty_code { get; set; }

    public major()
    {

    }

    public major(v_faculty_curriculum facultyCurriculum)
    {
      major_uid = facultyCurriculum.faculty_curriculum_uid;
      education_type_uid = facultyCurriculum.education_type_uid;
      faculty_uid = facultyCurriculum.college_faculty_uid;
      major_code = facultyCurriculum.faculty_curriculum_code;
      major_name_th = facultyCurriculum.faculty_curriculum_name_th;
      major_name_en = facultyCurriculum.faculty_curriculum_name_en;
      remark = facultyCurriculum.remark;
      education_type_code = facultyCurriculum.education_type_code;
      faculty_code = facultyCurriculum.faculty_code;
    }
  }
}
