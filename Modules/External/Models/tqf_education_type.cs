namespace edu_api.Modules.External.Models
{
  public class tqf_education_type
  {
    public string education_type_code { get; set; }
    public string education_type_name_th { get; set; }
    public string education_type_name_en { get; set; }
    public string education_type_level_code { get; set; }
    public string education_type_level_name_th { get; set; }
    public string education_type_level_name_en { get; set; }
  }
}
