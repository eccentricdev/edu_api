using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.Json.Serialization;

namespace edu_api.Modules.External.Models
{

  public class t_subject_master
  {
    [Key]
    public Guid? subject_master_uid { get; set; }
    public string subject_code { get; set; }
    public string subject_name_th { get; set; }
    public string subject_name_en { get; set; }
    public string subject_type_code { get; set; }
    public string subject_type { get; set; }
    public bool? subject_co_operative_edu { get; set; }
    public DateTime? created_datetime { get; set; }
    public List<t_subject_master_owner> subject_master_owners { get; set; }
    public List<t_subject_master_year> subject_master_years { get; set; }


    public t_subject_master()
    {
    }

    public t_subject_master(subject_master subjectMaster)
    {
      this.subject_code = subjectMaster.subject_code;
      this.subject_name_th = subjectMaster.subject_name_th;
      this.subject_name_en = subjectMaster.subject_name_en;
      this.subject_type = subjectMaster.subject_type_code;
      this.subject_type = subjectMaster.subject_type;
      this.subject_co_operative_edu = subjectMaster.subject_co_operative_edu;
      
      this.created_datetime = DateTime.Now;
      if (subjectMaster.subject_master_owners != null)
      {
        subject_master_owners = subjectMaster.subject_master_owners.Select(s => new t_subject_master_owner(s)).ToList();
      }
      if (subjectMaster.subject_master_years != null)
      {
        subject_master_years = subjectMaster.subject_master_years.Select(s => new t_subject_master_year(s)).ToList();
      }
    }
  }


  public class t_subject_master_owner
  {
    [Key]
    public Guid? subject_master_owner_uid { get; set; }
    public Guid? subject_master_uid { get; set; }
    public string register_year { get; set; }
    public string faculty_code { get; set; }
    public string curriculum_code { get; set; }
    public decimal? owner_faculty_percent { get; set; }
    public decimal? student_faculty_percent { get; set; }
    public string education_type_code { get; set; }
    public DateTime? created_datetime { get; set; }
    public string semester_code { get; set; }
    public string subject_master_seq_code { get; set; }

    [JsonIgnore]
    public t_subject_master subject_master { get; set; }
    public t_subject_master_owner(){}

    public t_subject_master_owner(subject_master_owner subjectMasterOwner)
    {
      this.register_year = subjectMasterOwner.register_year;
      this.faculty_code = subjectMasterOwner.faculty_code;
      this.curriculum_code = subjectMasterOwner.curriculum_code;
      
      this.owner_faculty_percent = subjectMasterOwner.owner_faculty_percent;
      this.student_faculty_percent = subjectMasterOwner.student_faculty_percent;
      this.education_type_code = subjectMasterOwner.education_type_code;
      this.created_datetime = DateTime.Now;
      this.semester_code = subjectMasterOwner.semester_code;
      this.subject_master_seq_code = subjectMasterOwner.subject_master_seq_code;
    }
  }

  public class t_subject_master_year
  {
    [Key]
    public Guid? subject_master_year_uid { get; set; }
    public Guid? subject_master_uid { get; set; }
    public string subject_master_seq_code { get; set; }
    public string year_subject_name_th { get; set; }
    public string year_subject_name_en { get; set; }
    public string register_year { get; set; }
    public string credit_text { get; set; }
    public string remark { get; set; }
    public short? lecture_hour { get; set; }
    public short? lab_hour { get; set; }
    public short? practice_hour { get; set; }
    public string min_grade { get; set; }
    public short? credit { get; set; }
    public bool? is_gpa_include { get; set; }
    public string pre_subject_codes { get; set; }
    public string co_subject_codes { get; set; }
    public bool? allow_scholarship { get; set; }
    public string subject_type_code { get; set; }
    public DateTime? created_datetime { get; set; }

    public t_subject_master_year() {}

    public t_subject_master_year(subject_master_year subjectMasterYear)
    {
      register_year = subjectMasterYear.register_year;
    credit_text = subjectMasterYear.credit_text;
    remark = subjectMasterYear.remark;
    lecture_hour = subjectMasterYear.lecture_hour;
    lab_hour = subjectMasterYear.lab_hour;
    practice_hour = subjectMasterYear.practice_hour;
    min_grade = subjectMasterYear.min_grade;
    credit = subjectMasterYear.credit;
    is_gpa_include = subjectMasterYear.is_gpa_include;
    subject_master_seq_code = subjectMasterYear.subject_master_seq_code;
    year_subject_name_th = subjectMasterYear.year_subject_name_th;
    year_subject_name_en = subjectMasterYear.year_subject_name_en;
    created_datetime = DateTime.Now;
    if (subjectMasterYear.pre_subject_codes != null)
    {
      pre_subject_codes = string.Join(",", subjectMasterYear.pre_subject_codes);
    }
    if (subjectMasterYear.pre_subject_codes != null)
    {
      co_subject_codes = string.Join(",", subjectMasterYear.co_subject_codes);
    }
    allow_scholarship = subjectMasterYear.allow_scholarship;
    subject_type_code = subjectMasterYear.subject_type_code;
    if (subjectMasterYear.subject_master_year_costs != null)
    {
      subject_master_year_costs = subjectMasterYear.subject_master_year_costs
        .Select(s => new t_subject_master_year_cost(s)).ToList();
    }
    if (subjectMasterYear.subject_master_year_topics != null)
    {
      subject_master_year_topics = subjectMasterYear.subject_master_year_topics
        .Select(s => new t_subject_master_year_topic(s)).ToList();
    }
    }

    [JsonIgnore]
    public t_subject_master subject_master { get; set; }
    //public short? study_type_id { get; set; }
    public List<t_subject_master_year_cost> subject_master_year_costs { get; set; }
    public List<t_subject_master_year_topic> subject_master_year_topics { get; set; }
  }

  public class t_subject_master_year_cost
  {
    [Key]
    public Guid? subject_master_year_cost_uid { get; set; }
    public Guid? subject_master_year_uid { get; set; }
    public string education_type_code { get; set; }
    public decimal? lecture_amount { get; set; }
    public decimal? lab_amount { get; set; }
    public decimal? practice_amount { get; set; }
    public DateTime? created_datetime { get; set; }


    public t_subject_master_year_cost()
    {

    }

    public t_subject_master_year_cost(subject_master_year_cost subjectMasterYearCost)
    {
      education_type_code = subjectMasterYearCost.education_type_code;
      lecture_amount = subjectMasterYearCost.lecture_amount;
      lab_amount = subjectMasterYearCost.lab_amount;
      practice_amount = subjectMasterYearCost.practice_amount;
      created_datetime = DateTime.Now;
    }
    [JsonIgnore]
    public t_subject_master_year subject_master_year { get; set; }
  }
  public class t_subject_master_year_topic
  {
    [Key]
    public Guid? subject_master_year_topic_uid { get; set; }
    public Guid? subject_master_year_uid { get; set; }
    public string topic_code { get; set; }
    public string topic_name_th { get; set; }
    public string topic_name_en { get; set; }
    public short? topic_credit { get; set; }
    public DateTime? created_datetime { get; set; }

    public t_subject_master_year_topic(){}

    public t_subject_master_year_topic(subject_master_year_topic subjectMasterYearTopic)
    {
      topic_code = subjectMasterYearTopic.topic_code;
      topic_name_th = subjectMasterYearTopic.topic_name_th;
      topic_name_en = subjectMasterYearTopic.topic_name_en;
      topic_credit = subjectMasterYearTopic.topic_credit;
      created_datetime = DateTime.Now;
    }
    [JsonIgnore]
    public t_subject_master_year subject_master_year { get; set; }

  }


  public class subject_master
  {
    public string subject_code { get; set; }
    public string subject_name_th { get; set; }
    public string subject_name_en { get; set; }
    public string subject_type_code { get; set; }
    public string subject_type { get; set; }
    public bool? subject_co_operative_edu { get; set; }

    public List<subject_master_owner> subject_master_owners { get; set; }
    public List<subject_master_year> subject_master_years { get; set; }

  }


  public class subject_master_year_topic
  {

    public string topic_code { get; set; }
    public string topic_name_th { get; set; }
    public string topic_name_en { get; set; }
    public short? topic_credit { get; set; }
  }

  public class subject_master_owner
  {
    public string register_year { get; set; }
    public string faculty_code { get; set; }
    public string curriculum_code { get; set; }
    public decimal? owner_faculty_percent { get; set; }
    public decimal? student_faculty_percent { get; set; }
    public string education_type_code { get; set; }
    public string semester_code { get; set; }
    public string subject_master_seq_code { get; set; }

  }

  public class subject_master_year
  {
    public string register_year { get; set; }
    public string subject_master_seq_code { get; set; }
    public string year_subject_name_th { get; set; }
    public string year_subject_name_en { get; set; }
    public string credit_text { get; set; }
    public string remark { get; set; }
    public short? lecture_hour { get; set; }
    public short? lab_hour { get; set; }
    public short? practice_hour { get; set; }
    public string min_grade { get; set; }
    public short? credit { get; set; }
    public bool? is_gpa_include { get; set; }
    public List<string> pre_subject_codes { get; set; }
    public List<string> co_subject_codes { get; set; }
    public bool? allow_scholarship { get; set; }
    public string subject_type_code { get; set; }
    //public short? study_type_id { get; set; }
    public List<subject_master_year_cost> subject_master_year_costs { get; set; }
    public List<subject_master_year_topic> subject_master_year_topics { get; set; }
  }

  public class subject_master_year_cost
  {
    public string education_type_code { get; set; }
    public decimal? lecture_amount { get; set; }
    public decimal? lab_amount { get; set; }
    public decimal? practice_amount { get; set; }
  }

}
