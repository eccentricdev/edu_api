using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Intrinsics.Arm;
using System.Text.Json.Serialization;

namespace edu_api.Modules.External.Models
{
	public class t_curriculum_master
	{
		[Key]
		public Guid? curriculum_uid { get; set; }
		public string curriculum_code { get; set; }
		public string education_type_code { get; set; }
		public string faculty_code { get; set; }
		public short? total_semester { get; set; }
		public short? max_year_study { get; set; }
		public short? minimum_credits { get; set; }
		public short? normal_year_study { get; set; }
		public string curriculum_name_th { get; set; }
		public string curriculum_name_en { get; set; }
		public string full_degree_name_th { get; set; }
		public string full_degree_name_en { get; set; }
		public string short_degree_name_th { get; set; }
		public string short_degree_name_en { get; set; }
		public string curriculum_faculty_name_th { get; set; }
		public string curriculum_faculty_name_en { get; set; }
		public string curriculum_major_name_th { get; set; }
		public string curriculum_major_name_en { get; set; }
		public short? curriculum_type_id { get; set; }
		public string full_degree_doc_name_th { get; set; }
		public string full_degree_doc_name_en { get; set; }
		public string curriculum_faculty_doc_name_th { get; set; }
		public string curriculum_faculty_doc_name_en { get; set; }
		public string curriculum_major_doc_name_th { get; set; }
		public string curriculum_major_doc_name_en { get; set; }
		public string curriculum_year { get; set; }
		public string academic_year { get; set; }
		//public string entry_year { get; set; }
		public bool? is_package { get; set; }
		public bool? is_group_semester { get; set; }
		public decimal? semester_1_amount { get; set; }
		public decimal? semester_2_amount { get; set; }
		public decimal? semester_3_amount { get; set; }
		public List<t_curriculum_subject_category_master> curriculum_subject_categories { get; set; }
		public List<t_curriculum_instructor_master> curriculum_instructors { get; set; }
		public List<t_education_plan_master> education_plans { get; set; }
		public List<t_curriculum_package> curriculum_packages { get; set; }
	}

	public class t_curriculum_package
	{
		[Key]
		public Guid? curriculum_package_uid { get; set; }
		public Guid? curriculum_uid { get; set; }
		public string register_year { get; set; }
		public decimal? semester_1_amount { get; set; }
		public decimal? semester_2_amount { get; set; }
		public decimal? semester_3_amount { get; set; } 
		[JsonIgnore]public t_curriculum_master curriculum_master { get; set; }
	}

	public class t_curriculum_subject_category_master
	{
		[Key]
		public Guid? curriculum_subject_category_uid { get; set; }
		public Guid? curriculum_uid { get; set; }
		public string curriculum_subject_category_code { get; set; }
		public string curriculum_subject_category_name_th { get; set; }
		public string curriculum_subject_category_name_en { get; set; }
		public short? total_credit { get; set; }
		public short? total_subject { get; set; }
		public short? min_credit { get; set; }
		public short? max_credit { get; set; }
		public List<t_curriculum_subject_group_master> curriculum_subject_groups { get; set; }
		[JsonIgnore]public t_curriculum_master curriculum_master { get; set; }
	}

	public class t_curriculum_subject_group_master
	{
		[Key]
		public Guid? curriculum_subject_group_uid { get; set; }
		public Guid? curriculum_subject_category_uid { get; set; }
		public string curriculum_subject_group_code { get; set; }
		public string curriculum_subject_group_name_th { get; set; }
		public string curriculum_subject_group_name_en { get; set; }
		public short? total_credit { get; set; }
		public short? total_subject { get; set; }
		public short? min_credit { get; set; }
		public short? max_credit { get; set; }
		public Guid? parent_curriculum_subject_group_uid { get; set; }
		public List<t_curriculum_subject_master> curriculum_subjects { get; set; }
		public List<t_curriculum_subject_group_master> curriculum_subject_groups { get; set; }
		[JsonIgnore]public t_curriculum_subject_category_master curriculum_subject_category_master { get; set; }
		[JsonIgnore]public t_curriculum_subject_group_master parent_curriculum_subject_group { get; set; }

	}

	public class t_curriculum_subject_master
	{
		[Key]
		public Guid? curriculum_subject_uid { get;set; }
		public Guid? curriculum_subject_group_uid { get; set; }
		public string subject_code { get; set; }
		public string subject_master_seq_code { get; set; }
		public string curriculum_subject_group_code { get; set; }
		[JsonIgnore]public t_curriculum_subject_group_master curriculum_subject_group_master { get; set; }
	}

	public class t_curriculum_instructor_master
	{
		[Key]
		public Guid? curriculum_instructor_uid { get; set; }
		public Guid? curriculum_uid { get; set; }
		public string personnel_no { get; set; }
		public string firstname_th { get; set; }
		public string lastname_th { get; set; }
		public string firstname_en { get; set; }
		public string lastname_en { get; set; }
		public string id_card { get; set; }
		public string academic_position_name_th { get; set; }
		public string academic_position_name_en { get; set; }
		public string graduate { get; set; }
		[JsonIgnore]public t_curriculum_master curriculum_master { get; set; }
	}

	public class t_education_plan_master
	{
		[Key]
		public Guid? education_plan_uid { get; set; }
		public Guid? curriculum_uid { get; set; }
		public string education_plan_name { get; set; }
		public List<t_class_unit_head_master> class_units { get; set; }
		[JsonIgnore]public t_curriculum_master curriculum_master { get; set; }



	}

	public class t_class_unit_head_master
	{
		[Key]
		public Guid? class_unit_uid { get; set; }
		public Guid? education_plan_uid { get; set; }
		[MaxLength(200)]public string class_unit { get; set; }
		public List<t_course_semester_head_master> course_semesters { get; set; }
		[JsonIgnore]public t_education_plan_master education_plan_master { get; set; }
	}

	public class t_course_semester_head_master
	{
		[Key]
		public Guid? course_semester_uid { get; set; }
		public Guid? class_unit_uid { get; set; }
		[MaxLength(200)]public string course_semester { get; set; }
		public List<t_education_plan_detail_master> education_plan_details { get; set; }
		[JsonIgnore]public t_class_unit_head_master class_unit_head_master { get; set; }
	}

	public class t_education_plan_detail_master
	{
		[Key]
		public Guid? education_plan_detail { get; set; }
		public Guid? course_semester_uid { get; set; }
		public string education_plan_code { get; set; }
		[MaxLength(200)]public string education_plan_subject { get; set; }
		[MaxLength(200)]public string education_plan_credit { get; set; }
		[JsonIgnore]public t_course_semester_head_master course_semester_head_master { get; set; }
	}


	// public class curriculum_master
	// {
	//   public string course_year { get; set; }
	//   public string faculty_code { get; set; }
	//   public string faculty_name_th { get; set; }
	//   public string university_name { get; set; }
	//   public string course_level { get; set; }
	//   public string course_of_type { get; set; }
	//   public string course_code_structure { get; set; }
	//   public string main_course_code { get; set; }
	//   public string course_name_th { get; set; }
	//   public string branch_name_th { get; set; }
	//   public string course_name_en { get; set; }
	//   public string degree_name_th { get; set; }
	//   public string degree_name_en { get; set; }
	//   public string initial_degree_name_th { get; set; }
	//   public string initial_degree_name_en { get; set; }
	//   public string major { get; set; }
	//   public int?  credit_course { get; set; }
	//   public string course_duration { get; set; }
	//   public string course_type { get; set; }
	//   public string course_language { get; set; }
	//   public string course_entrance { get; set; }
	//   public string course_other { get; set; }
	//   public string give_graduate { get; set; }
	//   public bool? is_package { get; set; }
	//   public bool? is_s_plus_1 { get; set; }
	//   public decimal? semester_1 { get; set; }
	//   public decimal? semester_2 { get; set; }
	//   public decimal? semester_s { get; set; }
	//   public List<instructor> instructors { get; set; }
	//   public List<subject_category> subject_categories { get; set; }
	//   public List<education_plan> education_plans { get; set; }
	//
	// }
	//
	// public class subject_category
	// {
	//   public string subjectcate_code { get; set; }
	//   public string subjectcate_name_th { get; set; }
	//   public string subjectcate_name_en { get; set; }
	//   public short? subjectcate_credit { get; set; }
	//   public List<subject_category_group> subject_category_groups { get; set; }
	// }
	//
	// public class subject_category_group
	// {
	//   public string subjectcate_group_code { get; set; }
	//   public string subjectcate_group_name_th { get; set; }
	//   public string subjectcate_group_name_en { get; set; }
	//   public short? subjectcate_group_credit { get; set; }
	//   public List<subject_category_sub_group> subject_category_sub_groups { get; set; }
	// }
	//
	// public class subject_category_sub_group
	// {
	//   public string subjectcate_sub_code { get; set; }
	//   public string subjectcate_sub_name_th { get; set; }
	//   public string subjectcate_sub_name_en { get; set; }
	//   public short? subjectcate_sub_credit { get; set; }
	//   public List<subject_category_sub_sub_group> subject_category_sub_sub_groups { get; set; }
	// }
	//
	// public class subject_category_sub_sub_group
	// {
	//   public string subjectcate_sub_sub_code { get; set; }
	//   public string subjectcate_sub_sub_name_th { get; set; }
	//   public string subjectcate_sub_sub_name_en { get; set; }
	//   public short? subjectcate_sub_sub_credit { get; set; }
	//   public List<subject> subjects { get; set; }
	// }
	//
	// public class instructor
	// {
	//   public string firstname_th { get; set; }
	//   public string lastname_th { get; set; }
	//   public string firstname_en { get; set; }
	//   public string lastname_en { get; set; }
	//   public string id_card { get; set; }
	//   public string position_name_th { get; set; }
	//   public string position_name_en { get; set; }
	//   public string graduate { get; set; }
	// }
	//
	// public class subject
	// {
	//   public string subject_code { get; set; }
	//   public string subject_name_th { get; set; }
	//   public string subject_name_en { get; set; }
	//   public string subject_credit { get; set; }
	//   public string subject_detail_th { get; set; }
	//   public string subject_detail_en { get; set; }
	//   public string subject_condition { get; set; }
	//   public string subject_requisite { get; set; }
	//
	// }
	//
	// public class education_plan
	// {
	//   public string education_plan_name { get; set; }
	//  public List<class_unit_head> class_units { get; set; }
	//
	//
	//
	// }
	//
	// public class class_unit_head
	// {
	//   public string class_unit { get; set; }
	//   public List<course_semester_head> course_semesters { get; set; }
	// }
	//
	// public class course_semester_head
	// {
	//   public string course_semester { get; set; }
	//   public List<education_plan_detail> education_plan_details { get; set; }
	// }
	//
	// public class education_plan_detail
	// {
	//   public string education_plan_code { get; set; }
	//   public string education_plan_subject { get; set; }
	//   public string education_plan_credit { get; set; }
	// }
}
