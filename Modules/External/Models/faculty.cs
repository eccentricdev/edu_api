using System;
using edu_api.Modules.Organizations.Databases.Models;
using SeventyOneDev.Utilities.Models;

namespace edu_api.Modules.External.Models
{
  public class faculty
  {
    //public Guid? faculty_uid { get; set; }
    //public int? faculty_id {get;set;}
    //public int? education_type_id {get;set;}
    public string faculty_code {get;set;}
    public string faculty_name_th {get;set;}
    public string faculty_name_en {get;set;}


    public string education_type_level_code {get;set;}
    public string education_type_level_name_th {get;set;}
    public string education_type_level_name_en {get;set;}

    public faculty()
    {

    }

    public faculty(v_faculty_education_level facultyEducationLevel)
    {
      //this.faculty_uid = collegeFaculty.college_faculty_uid;
      this.faculty_code = facultyEducationLevel.faculty_code;
      this.faculty_name_th = facultyEducationLevel.college_faculty_name_th;
      this.faculty_name_en = facultyEducationLevel.college_faculty_name_en;
      this.education_type_level_code = facultyEducationLevel.education_level_code;
      this.education_type_level_name_th = facultyEducationLevel.education_level_name_th;
      this.education_type_level_name_en = facultyEducationLevel.education_level_name_en;
    }
  }
}
