using System.Linq;
using System.Threading.Tasks;
using edu_api.Modules.External.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using rsu_common_api.models;
using SeventyOneDev.Utilities;

namespace edu_api.Modules.External.Controllers
{
  [Route("/api/external/student",Name="external_student")]
  public class PersonnelController:Controller
  {
    private readonly Db _db;
    public PersonnelController(Db db)
    {
      _db = db;
    }

    [Authorize(Policy = "STUDENT")]
    [HttpGet()]
    public async Task<ActionResult<v_student>> GetAgency([FromQuery] student_request studentRequest)
    {
      if (EFExtension.IsNullOrEmpty(studentRequest))
      {
        return BadRequest("NO_FILTER_FOUND");
      }

      var students = _db.v_student.AsNoTracking();

      if (string.IsNullOrEmpty(studentRequest.student_code))
      {
        students = students.Where(p => p.student_code == studentRequest.student_code);
      }

      if (string.IsNullOrEmpty(studentRequest.citizen_id))
      {
        students = students.Where(p => p.citizen_id == studentRequest.citizen_id);
      }

      if (string.IsNullOrEmpty(studentRequest.tax_id))
      {
        students = students.Where(p => p.tax_id == studentRequest.tax_id);
      }

      if (string.IsNullOrEmpty(studentRequest.passport_no))
      {
        students = students.Where(p => p.passport_no == studentRequest.passport_no);
      }

      if (string.IsNullOrEmpty(studentRequest.uid_no))
      {
        students = students.Where(p => p.uid_no == studentRequest.uid_no);
      }

      students = students
        .Include(s => s.student_addresses)//.ThenInclude(s => s.address)
        .Include(s => s.student_parents)//.ThenInclude(s => s.address)
        .Include(s => s.student_parents)//.ThenInclude(s => s.company)
        .Include(s => s.student_documents);
      var student = await students.FirstOrDefaultAsync();
      return student;
    }


  }
}
