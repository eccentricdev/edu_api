using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using edu_api.Modules.External.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using rsu_common_api.models;
using SeventyOneDev.Utilities;

namespace edu_api.Modules.External.Controllers
{
  [Route("/api/external/subject_master",Name="subject_master")]
  public class SubjectMasterController: Controller
  {
    private readonly Db _context;
    public SubjectMasterController(Db context)
    {
      _context = context;
    }
    //[Authorize(Policy = "curriculum")]
    [HttpPost()]
    public async Task<ActionResult> GetAgency([FromBody] subject_master subjectMaster)
    {
      if (subjectMaster != null)
      {
        if (_context.t_subject_master.Any(s=>s.subject_code==subjectMaster.subject_code))
        {
          return BadRequest(new Error(400,"รหัสวิชาซ้ำ"));
        }

        if (subjectMaster.subject_master_owners.GroupBy(s=>new {s.education_type_code,s.register_year,s.semester_code}).Any(s=>s.Count()>1))
        {
          return BadRequest(new Error(400,"ประเภทการศึกษาของเจ้าของซ้ำ"));
        }
        if (subjectMaster.subject_master_years.GroupBy(s=>new {s.subject_master_seq_code,s.register_year}).Any(s=>s.Count()>1))
        {
          return BadRequest(new Error(400,"subject_seq ซ้ำ"));
        }

        foreach (var subjectMasterYear in subjectMaster.subject_master_years)
        {
          if (subjectMasterYear.subject_master_year_costs.GroupBy(s => s.education_type_code).Any(s => s.Count() > 1))
          {
            return BadRequest(new Error(400,"ประเภทการศึกษาของ year ซ้ำ"));
          }
        }
        var newSubjectMaster = new t_subject_master(subjectMaster);
        await _context.t_subject_master.AddAsync(newSubjectMaster);
        await _context.SaveChangesAsync();
        return NoContent();
      }
      else
      {
        return BadRequest();
      }

    }
    [HttpPost("s")]
    public async Task<ActionResult> NewSubjectMasters([FromBody] List<subject_master> subjectMasters)
    {
      foreach (var subjectMaster in subjectMasters)
      {
        if (_context.t_subject_master.Any(s=>s.subject_code==subjectMaster.subject_code))
        {
          return BadRequest(new Error(400,"รหัสวิชาซ้ำ"));
        }

        if (subjectMaster.subject_master_owners.GroupBy(s=>new {s.education_type_code,s.register_year,s.semester_code}).Any(s=>s.Count()>1))
        {
          return BadRequest(new Error(400,"ประเภทการศึกษาของเจ้าของซ้ำ"));
        }
        if (subjectMaster.subject_master_years.GroupBy(s=>new {s.subject_master_seq_code,s.register_year}).Any(s=>s.Count()>1))
        {
          return BadRequest(new Error(400,"subject_seq ซ้ำ"));
        }

        foreach (var subjectMasterYear in subjectMaster.subject_master_years)
        {
          if (subjectMasterYear.subject_master_year_costs.GroupBy(s => s.education_type_code).Any(s => s.Count() > 1))
          {
            return BadRequest(new Error(400,"ประเภทการศึกษาของ year ซ้ำ"));
          }
        }
        
        var newSubjectMaster = new t_subject_master(subjectMaster);
        await _context.t_subject_master.AddAsync(newSubjectMaster);
      }
     
      
        await _context.SaveChangesAsync();
        return NoContent();
      

    }
    [HttpPut("owner/{subject_code}")]
    public async Task<ActionResult> UpdateSubjectOwner([FromRoute]string subject_code, [FromBody] subject_master_owner subjectMasterOwner)
    {
      var subjectMasterUid = _context.t_subject_master.AsNoTracking()
        .FirstOrDefault(s => s.subject_code == subject_code)?.subject_master_uid;
      if (subjectMasterUid is null) return BadRequest();
      var _subjectMasterOwner = await 
        _context.t_subject_master_owner.FirstOrDefaultAsync(s => s.subject_master_uid == subjectMasterUid && 
                                                                 s.register_year==subjectMasterOwner.register_year &&
                                                                 s.education_type_code==subjectMasterOwner.education_type_code);
      if (_subjectMasterOwner == null)
      {
        _subjectMasterOwner = new t_subject_master_owner(subjectMasterOwner);
        _subjectMasterOwner.subject_master_uid = subjectMasterUid;
        await _context.t_subject_master_owner.AddAsync(_subjectMasterOwner);
        await _context.SaveChangesAsync();
        return Ok();
      }
      else
      {
        _subjectMasterOwner.faculty_code = subjectMasterOwner.faculty_code;
        _subjectMasterOwner.curriculum_code = subjectMasterOwner.curriculum_code;
        await _context.SaveChangesAsync();
        return Ok();
      }
      
      

    }
    [HttpPut("year/{subject_code}")]
    public async Task<ActionResult> UpdateSubjectYear([FromRoute]string subject_code, [FromBody] subject_master_year subjectMasterYear)
    {
      var subjectMasterUid = _context.t_subject_master.AsNoTracking()
        .FirstOrDefault(s => s.subject_code == subject_code)?.subject_master_uid;
      if (subjectMasterUid is null) return BadRequest();
      var _subjectMasterYear = await 
        _context.t_subject_master_year.FirstOrDefaultAsync(s => s.subject_master_uid == subjectMasterUid && 
                                                                 s.register_year==subjectMasterYear.register_year &&
                                                                 s.subject_master_seq_code==subjectMasterYear.subject_master_seq_code);
      if (_subjectMasterYear != null)
      {
        _context.t_subject_master_year.Remove(_subjectMasterYear);
        await _context.SaveChangesAsync();
      }
      _subjectMasterYear = new t_subject_master_year(subjectMasterYear);
      _subjectMasterYear.subject_master_uid = subjectMasterUid;
      await _context.t_subject_master_year.AddAsync(_subjectMasterYear);
      await _context.SaveChangesAsync();
      return Ok();
      
      

    }
    //[Authorize(Policy = "curriculum")]
    [HttpGet("{subject_code}")]
    public async Task<ActionResult<t_subject_master>> GetAgency([FromRoute] string subject_code)
    {
      if (string.IsNullOrEmpty(subject_code)) return BadRequest();
      var subjectMaster = await _context.t_subject_master.AsNoTracking()
        .Include(s => s.subject_master_owners)
        .Include(s => s.subject_master_years).ThenInclude(s=>s.subject_master_year_costs)
        .Include(s => s.subject_master_years).ThenInclude(s=>s.subject_master_year_topics)
        .FirstOrDefaultAsync(s => s.subject_code == subject_code);
      if (subjectMaster!=null)
      {
        return Ok(subjectMaster);
      }
      else
      {
        return Ok();
      }
     

    }
  }
}
