using System.Linq;
using System.Threading.Tasks;
using edu_api.Modules.External.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SeventyOneDev.Utilities;

namespace edu_api.Modules.External.Controllers
{
  [Route("/api/external/curriculum_master",Name="curriculum_master")]
  public class CurriculumMasterController: Controller
  {
    private readonly Db _context;
    public CurriculumMasterController(Db context)
    {
      _context = context;
    }
    //[Authorize(Policy = "curriculum")]
    [HttpPost()]
    public async Task<ActionResult> NewCurriculum([FromBody] t_curriculum_master curriculumMaster)
    {
      if (curriculumMaster != null)
      {
        var result=await _context.t_curriculum_master.AddAsync(curriculumMaster);
        await _context.SaveChangesAsync();
        return NoContent();
      }
      else
      {
        return BadRequest();
      }

    }

    [HttpGet("{curriculum_code}/{year_code}")]
    public async Task<ActionResult<t_curriculum_master>> GetCurriculum([FromRoute] string curriculum_code,[FromRoute]string year_code)
    {
      if (!string.IsNullOrEmpty(curriculum_code))
      {
        var result = await _context.t_curriculum_master.AsNoTracking()
          .Include(c => c.curriculum_subject_categories).ThenInclude(c => c.curriculum_subject_groups)
          .ThenInclude(c => c.curriculum_subjects)
          .Include(c => c.curriculum_instructors)
          .Include(c => c.education_plans).ThenInclude(c => c.class_units).ThenInclude(c => c.course_semesters)
          .ThenInclude(c => c.education_plan_details)
          .FirstOrDefaultAsync(c => c.curriculum_code == curriculum_code && c.curriculum_year==year_code);
        foreach (var curriculumSubjectCategory in result.curriculum_subject_categories)
        {
          foreach (var curriculumSubjectGroup in curriculumSubjectCategory.curriculum_subject_groups)
          {
            GetCurriculumSubjectGroup(curriculumSubjectGroup);
            // var subjectGroups = await _context.t_curriculum_subject_group_master.AsNoTracking()
            //   .Include(c=>c.curriculum_subjects)
            //   .Where(c =>
            //     c.parent_curriculum_subject_group_uid == curriculumSubjectGroup.curriculum_subject_group_uid)
            //   .ToListAsync();
            // if (subjectGroups.Any())
            // {
            //   curriculumSubjectGroup.curriculum_subject_groups = subjectGroups;
            // }
            
          }
        }
        return Ok(result);
      }
      else
      {
        return BadRequest();
      }

    }
    private void  GetCurriculumSubjectGroup(t_curriculum_subject_group_master curriculumSubjectGroupMaster)
    {
      var childs = _context.t_curriculum_subject_group_master.AsNoTracking()
        .Include(o=>o.curriculum_subjects)
        .Where(o =>
        o.parent_curriculum_subject_group_uid == curriculumSubjectGroupMaster.curriculum_subject_group_uid).ToList();
      if (childs.Any())
      {
        curriculumSubjectGroupMaster.curriculum_subject_groups = childs;
        foreach (var child in curriculumSubjectGroupMaster.curriculum_subject_groups)
        {
          GetCurriculumSubjectGroup(child);
        }
      }

    }
  }
}
