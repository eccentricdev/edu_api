using System.Threading.Tasks;
using edu_api.Modules.External.Models;
using Microsoft.AspNetCore.Mvc;
using SeventyOneDev.Utilities;

namespace edu_api.Modules.External.Controllers
{
  [Route("/api/external/education_type",Name="education_type")]
  public class ExternalEducationTypeController:Controller
  {
    private readonly Db _db;
    public ExternalEducationTypeController(Db db)
    {
      _db = db;
    }
    [HttpPost]
    public async Task<ActionResult> NewEducationType([FromBody] tqf_education_type tqfEducationType)
    {
      return NoContent();
    }
  }
}
