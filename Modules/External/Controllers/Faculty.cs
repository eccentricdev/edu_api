using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using edu_api.Modules.External.Models;
using edu_api.Modules.Organizations.Databases.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SeventyOneDev.Utilities;

namespace edu_api.Modules.External.Controllers
{
  [Route("/api/external/faculty",Name="faculty")]
  public class FacultyController:Controller
  {
    private readonly Db _db;
    public FacultyController(Db db)
    {
      _db = db;
    }
    [HttpGet]
    public async Task<ActionResult<List<faculty>>> GetAllFaculty()
    {
      var collegeFaculties = await _db.v_faculty_education_level.AsNoTracking().ToListAsync();
      var faculties = collegeFaculties.Select(c => new faculty(c));
      return Ok(faculties);
    }
  }
}
