using System.Collections.Generic;
using System.Threading.Tasks;
using edu_api.Modules.Fee.Databases.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.EntityFrameworkCore;
using SeventyOneDev.Utilities;

namespace edu_api.Modules.External.Controllers
{
  [Route("/api/external/fee",Name="fee")]
  public class FeeController:Controller
  {
    private readonly Db _db;
    public FeeController(Db db)
    {
      _db = db;
    }
    [HttpGet]
    public async Task<ActionResult<List<v_fee>>> GetAllFee()
    {
      var fees = await _db.t_fee.AsNoTracking().ToListAsync();
      return Ok(fees);
    }
  }
}
