using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using edu_api.Modules.External.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SeventyOneDev.Utilities;

namespace edu_api.Modules.External.Controllers
{
  [Route("/api/external/major",Name="major")]
  public class MajorController:Controller
  {
    private readonly Db _db;
    public MajorController(Db db)
    {
      _db = db;
    }
    [HttpGet]
    public async Task<ActionResult<List<major>>> GetAllFaculty()
    {
      var facultyCurriculums = await _db.v_faculty_curriculum.AsNoTracking().ToListAsync();
      var majors = facultyCurriculums.Select(c => new major(c));
      return Ok(majors);
    }
  }
}
