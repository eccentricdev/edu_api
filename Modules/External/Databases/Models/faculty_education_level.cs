namespace SeventyOneDev.Utilities.Models
{
  public class v_faculty_education_level
  {
    public string faculty_code {get;set;}
    public string college_faculty_name_th {get;set;}
    public string college_faculty_name_en {get;set;}
    public string education_level_code {get;set;}
    public string education_level_name_th {get;set;}
    public string education_level_name_en {get;set;}
  }
}
