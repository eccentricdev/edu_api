using edu_api.Modules.External.Models;
using edu_api.Modules.Register.Databases;
using Microsoft.EntityFrameworkCore;
using SeventyOneDev.Utilities.Models;

namespace SeventyOneDev.Utilities
{
  public partial class Db : DbContext
  {
    public DbSet<t_subject_master> t_subject_master {get;set;}
    public DbSet<t_subject_master_owner> t_subject_master_owner {get;set;}
    public DbSet<t_subject_master_year> t_subject_master_year {get;set;}
    public DbSet<t_subject_master_year_cost> t_subject_master_year_cost {get;set;}
    public DbSet<t_subject_master_year_topic> t_subject_master_year_topic {get;set;}

    public DbSet<v_faculty_education_level> v_faculty_education_level {get;set;}
    
    
    public DbSet<t_curriculum_master> t_curriculum_master { get; set; }
    public DbSet<t_curriculum_subject_category_master> t_curriculum_subject_category_master { get; set; }
    public DbSet<t_curriculum_subject_group_master> t_curriculum_subject_group_master { get; set; }
    public DbSet<t_curriculum_subject_master> t_curriculum_subject_master { get; set; }
    public DbSet<t_curriculum_instructor_master> t_curriculum_instructor_master { get; set; }
    public DbSet<t_curriculum_package> t_curriculum_package { get; set; }
    
    public DbSet<t_education_plan_master> t_education_plan_master { get; set; }
    public DbSet<t_class_unit_head_master> t_class_unit_head_master { get; set; }
    public DbSet<t_course_semester_head_master> t_course_semester_head_master { get; set; }
    public DbSet<t_education_plan_detail_master> t_education_plan_detail_master { get; set; }
    
    //public DbSet<t_pro>

    private void OnExternalModelCreating(ModelBuilder builder, string schema)
    {
      
      builder.Entity<t_curriculum_master>().ToTable("curriculum_master", schema);
      builder.Entity<t_curriculum_subject_category_master>().ToTable("curriculum_subject_category_master", schema);
      builder.Entity<t_curriculum_subject_category_master>().HasOne(c => c.curriculum_master)
        .WithMany(c => c.curriculum_subject_categories).HasForeignKey(c => c.curriculum_uid)
        .OnDelete(DeleteBehavior.Cascade);
      builder.Entity<t_curriculum_subject_group_master>().ToTable("curriculum_subject_group_master", schema);
      builder.Entity<t_curriculum_subject_group_master>().HasOne(c => c.curriculum_subject_category_master)
        .WithMany(c => c.curriculum_subject_groups).HasForeignKey(c => c.curriculum_subject_category_uid)
        .OnDelete(DeleteBehavior.Cascade);
      builder.Entity<t_curriculum_subject_group_master>().HasOne(c => c.parent_curriculum_subject_group)
        .WithMany(c => c.curriculum_subject_groups).HasForeignKey(c => c.parent_curriculum_subject_group_uid)
        .OnDelete(DeleteBehavior.Cascade);
      
      builder.Entity<t_curriculum_subject_master>().ToTable("curriculum_subject_master", schema);
      builder.Entity<t_curriculum_subject_master>().HasOne(c => c.curriculum_subject_group_master)
        .WithMany(c => c.curriculum_subjects)
        .HasForeignKey(c => c.curriculum_subject_group_uid).OnDelete(DeleteBehavior.Cascade);
      builder.Entity<t_curriculum_instructor_master>().ToTable("curriculum_instructor_master", schema);
      builder.Entity<t_curriculum_instructor_master>().HasOne(c => c.curriculum_master)
        .WithMany(c => c.curriculum_instructors)
        .HasForeignKey(c => c.curriculum_uid).OnDelete(DeleteBehavior.Cascade);
      
      builder.Entity<t_curriculum_package>().ToTable("curriculum_package", schema);
      builder.Entity<t_curriculum_package>().HasOne(c => c.curriculum_master)
        .WithMany(c => c.curriculum_packages)
        .HasForeignKey(c => c.curriculum_uid).OnDelete(DeleteBehavior.Cascade);
      
      
      builder.Entity<t_education_plan_master>().ToTable("education_plan_master", schema);
      builder.Entity<t_education_plan_master>().HasOne(c => c.curriculum_master).WithMany(c => c.education_plans)
        .HasForeignKey(c => c.curriculum_uid).OnDelete(DeleteBehavior.Cascade);
      builder.Entity<t_class_unit_head_master>().ToTable("class_unit_head_master", schema);
      builder.Entity<t_class_unit_head_master>().HasOne(c => c.education_plan_master).WithMany(c => c.class_units)
        .HasForeignKey(c => c.education_plan_uid).OnDelete(DeleteBehavior.Cascade);
      builder.Entity<t_course_semester_head_master>().ToTable("course_semester_head_master", schema);
      builder.Entity<t_course_semester_head_master>().HasOne(c => c.class_unit_head_master)
        .WithMany(c => c.course_semesters)
        .HasForeignKey(c => c.class_unit_uid).OnDelete(DeleteBehavior.Cascade);
      builder.Entity<t_education_plan_detail_master>().ToTable("education_plan_detail_master", schema);
      builder.Entity<t_education_plan_detail_master>().HasOne(c => c.course_semester_head_master)
        .WithMany(c => c.education_plan_details)
        .HasForeignKey(c => c.course_semester_uid).OnDelete(DeleteBehavior.Cascade);
      
      
      
      builder.Entity<t_subject_master>().ToTable("subject_master", schema);
      builder.Entity<t_subject_master_owner>().ToTable("subject_master_owner", schema);
      builder.Entity<t_subject_master_owner>().HasOne(s => s.subject_master).WithMany(s => s.subject_master_owners)
        .HasForeignKey(s => s.subject_master_uid).OnDelete(DeleteBehavior.Cascade);;
      builder.Entity<t_subject_master_year>().ToTable("subject_master_year", schema);
      builder.Entity<t_subject_master_year>().HasOne(s => s.subject_master).WithMany(s => s.subject_master_years)
        .HasForeignKey(s => s.subject_master_uid).OnDelete(DeleteBehavior.Cascade);

      builder.Entity<t_subject_master_year_cost>().ToTable("subject_master_year_cost", schema);
      builder.Entity<t_subject_master_year_cost>().HasOne(s => s.subject_master_year).WithMany(s => s.subject_master_year_costs)
        .HasForeignKey(s => s.subject_master_year_uid).OnDelete(DeleteBehavior.Cascade);;
      builder.Entity<t_subject_master_year_topic>().ToTable("subject_master_year_topic", schema);
      builder.Entity<t_subject_master_year_topic>().HasOne(s => s.subject_master_year).WithMany(s => s.subject_master_year_topics)
        .HasForeignKey(s => s.subject_master_year_uid).OnDelete(DeleteBehavior.Cascade);;

      builder.Entity<v_faculty_education_level>().ToView("v_faculty_education_level", schema).HasNoKey();
    }
  }
}
