using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using edu_api.Modules.EducationChecks.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using rsu_common_api.Models.EducationChecks;
using SeventyOneDev.Utilities;

namespace edu_api.Modules.EducationChecks.Controllers
{
    [Route("api/education_check/student_education_check_waitings", Name = "student_education_check_waitings")]
    public class StudentEducationCheckWaitingController:Controller
    {
        readonly StudentEducationCheckWaitingService _studentEducationCheckWaitingService;
        
        public StudentEducationCheckWaitingController(StudentEducationCheckWaitingService studentEducationCheckWaitingService){
            _studentEducationCheckWaitingService = studentEducationCheckWaitingService;
        }
        
        [HttpGet("getlist")]
        public async Task<ActionResult<List<v_student_education_check_waiting>>> GetStudentEducationCheckWaitings([FromQuery]v_student_education_check_waiting _student_education_check_waiting=null)
        {
            if (EFExtension.IsNullOrEmpty(_student_education_check_waiting))
            {
                var listResult = await _studentEducationCheckWaitingService.ListStudentEducationCheckWaiting(null);
                return Ok(listResult);
            }
            else
            {
                var getResult = await _studentEducationCheckWaitingService.ListStudentEducationCheckWaiting(_student_education_check_waiting);
                if (getResult.Count()==0)
                {
                    return Ok(new List<v_student_education_check_waiting>());
                }
                else
                {
                    return getResult.ToList();
                }

            }

        }

        [HttpGet("get/{school_code}")]
        public async Task<ActionResult<v_student_education_check_waiting>> Getstudent_education_check_waiting([FromRoute]string school_code)
        {
            var getResult = await _studentEducationCheckWaitingService.GetStudentEducationCheckWaiting(school_code);
            if (getResult==null)
            {
                return NotFound();
            }
            else
            {
                return Ok(getResult);
            }
        }

    }
}