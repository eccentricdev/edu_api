using edu_api.Modules.EducationChecks.Services;
using Microsoft.AspNetCore.Mvc;
using rsu_common_api.models.EducationChecks;
using SeventyOneDev.Utilities;

namespace edu_api.Modules.EducationChecks.Controllers
{
    [Route("api/education_check/education_check_document", Name = "education_check_document")]
    public class EducationCheckDocumentController:BaseUidController<t_education_check_document,v_education_check_document>
    {
        public EducationCheckDocumentController(EducationCheckDocumentService entityUidService) : base(entityUidService)
        {

        }
    }
}