using System.Collections.Generic;
using System.Threading.Tasks;
using edu_api.Modules.EducationChecks.Services;
using Microsoft.AspNetCore.Mvc;
using rsu_common_api.models.EducationChecks;
using SeventyOneDev.Utilities;

namespace edu_api.Modules.EducationChecks.Controllers
{
    [Route("api/education_check/student_education_check", Name = "student_education_check")]
    public class StudentEducationCheckController:BaseUidController<t_student_education_check,v_student_education_check>
    {
        private readonly StudentEducationCheckService _entityUidService;
        public StudentEducationCheckController(StudentEducationCheckService entityUidService) : base(entityUidService)
        {
            _entityUidService = entityUidService;
        }
        
        [HttpGet("new")]
        public async Task<ActionResult<List<v_student_education_check>>> ListNewStudentEducationCheck([FromQuery] v_student_education_check entity)
        {
            var studentLogResult= await _entityUidService.ListNewStudentEducationCheck(entity);
            return Ok(studentLogResult);
        }
    }
}