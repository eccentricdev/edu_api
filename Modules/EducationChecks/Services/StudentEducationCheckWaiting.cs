using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using rsu_common_api.Models.EducationChecks;
using SeventyOneDev.Utilities;

namespace edu_api.Modules.EducationChecks.Services
{
    public class StudentEducationCheckWaitingService
    {
        readonly Db _context;

        public StudentEducationCheckWaitingService(Db context)
        {
            _context = context;
        }
        
        public async Task<List<v_student_education_check_waiting>> ListStudentEducationCheckWaiting(v_student_education_check_waiting _student_education_check_waiting)
        {
            IQueryable<v_student_education_check_waiting> _student_education_check_waitings=_context.v_student_education_check_waiting.AsNoTracking();

            if (_student_education_check_waiting != null)
            {
                _student_education_check_waitings = _student_education_check_waiting.search != null ? _student_education_check_waitings.Search(_student_education_check_waiting.search) : _student_education_check_waitings.Search(_student_education_check_waiting);
            }
            _student_education_check_waitings = _student_education_check_waitings.OrderByDescending(c => c.updated_datetime);
            return await _student_education_check_waitings.ToListAsync();
        }
        
        public async Task<v_student_education_check_waiting> GetStudentEducationCheckWaiting(string code)
        {
            return await _context.v_student_education_check_waiting
                .FirstOrDefaultAsync(a=>a.school_code==code);
        }
    }
}