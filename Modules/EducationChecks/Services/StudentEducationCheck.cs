using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using rsu_common_api.models.EducationChecks;
using SeventyOneDev.Utilities;

namespace edu_api.Modules.EducationChecks.Services
{
    public class StudentEducationCheckService:EntityUidService<t_student_education_check,v_student_education_check>
    {
        private readonly Db _context;
        public StudentEducationCheckService(Db context) : base(context)
        {
            _context = context;
        }
        
        public async Task<List<v_student_education_check>> ListNewStudentEducationCheck(v_student_education_check entity,List<Guid?> agencies=null,string userName=null)
        {

            var entities = _context.v_student_education_check.AsNoTracking().Where(e=>e.education_check_document_uid==null);
            if (agencies != null)
            {
                if (agencies.Any())
                {
                    entities = entities.Where(e => agencies.Any(a => a == e.owner_agency_uid));
                }
            }
            else if (!string.IsNullOrEmpty(userName) )
            {
                entities = entities.Where(e => e.created_by == userName);
            }
            var p =entity?.GetType().GetProperties().FirstOrDefault(a => a.Name == "search");
            if (p == null) return await entities.OrderByDescending(c => c.updated_datetime).ToListAsync();
            var con = p.GetValue(entity);
            entities = con != null ? entities.Search(con.ToString()) : entities.Search(entity);
            return await entities.OrderByDescending(c => c.updated_datetime).ToListAsync();
            
        }

        public async override Task<t_student_education_check> NewEntity(t_student_education_check entity, ClaimsIdentity claimsIdentity)
        {
            var studentEducationCheck = await _context.t_student_education_check.AsNoTracking()
                .FirstOrDefaultAsync(w => w.student_code == entity.student_code);
            if (studentEducationCheck != null)
            {
                studentEducationCheck.school_id = entity.school_id;
                studentEducationCheck.education_level_id = entity.education_level_id;
                studentEducationCheck.gpa = entity.gpa;
                studentEducationCheck.remark = entity.remark;
                studentEducationCheck.graduate_document_no = entity.graduate_document_no;
                await base.UpdateEntity(studentEducationCheck, claimsIdentity);
            }
            else
            { 
                studentEducationCheck =  await base.NewEntity(entity, claimsIdentity);
            }
            //update to student
            var student =await _context.t_student.AsNoTracking()
                .FirstOrDefaultAsync(w => w.student_code == studentEducationCheck.student_code);
            if (student != null)
            {
                student.school_id = studentEducationCheck.school_id;
                student.entry_education_level_id = studentEducationCheck.education_level_id;
                student.entry_gpa = studentEducationCheck.gpa;
                student.graduate_document_no = studentEducationCheck.graduate_document_no;
                student.is_education_checked =
                    studentEducationCheck.student_education_check_status_uid != null ? true : false;
                
                _context.t_student.Update(student);
                await _context.SaveChangesAsync(claimsIdentity);
            }

            return studentEducationCheck;
        }
    }
}