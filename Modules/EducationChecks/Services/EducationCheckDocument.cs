using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using rsu_common_api.models.EducationChecks;
using SeventyOneDev.Core.Document.Services;
using SeventyOneDev.Utilities;

namespace edu_api.Modules.EducationChecks.Services
{
    public class EducationCheckDocumentService:EntityUidService<t_education_check_document,v_education_check_document>
    {
        private readonly Db _context;
        private readonly DocumentNoService _documentNoService;
        public EducationCheckDocumentService(Db context ,DocumentNoService documentNoService) : base(context)
        {
            _context = context;
            _documentNoService = documentNoService;
        }

        public override async Task<v_education_check_document> GetEntity(Guid uid, List<Guid?> agencies = null, string userName = null)
        {
            var rs =  await _context.v_education_check_document.FirstOrDefaultAsync(a=>a.education_check_document_uid==uid);
            rs.student_education_checks = await _context.v_student_education_check.Where(s =>
                s.education_check_document_uid == rs.education_check_document_uid).ToListAsync();
            return rs;
        }

        public override async Task<t_education_check_document> NewEntity(t_education_check_document entity, ClaimsIdentity claimsIdentity)
        {
            var documentNo = await _documentNoService.GetDocumentNo(null,"DOC",null);
            entity.document_no = documentNo;
            
            var rs =  await base.NewEntity(entity, claimsIdentity);
            
            foreach (var studentEducationCheck in entity.student_education_checks)
            {
                var _studentEducationCheck = await _context.t_student_education_check
                    .FirstOrDefaultAsync(s => s.student_education_check_uid == studentEducationCheck.student_education_check_uid);
                _studentEducationCheck.education_check_document_uid = entity.education_check_document_uid;
                
                _context.t_student_education_check.Update(_studentEducationCheck);
            }
            await _context.SaveChangesAsync(claimsIdentity);

            return rs;
        }

        public override async Task<int> UpdateEntity(t_education_check_document entity, ClaimsIdentity claimsIdentity)
        {
            var rs = await base.UpdateEntity(entity, claimsIdentity);
            
            foreach (var studentEducationCheck in entity.student_education_checks)
            {
                var _studentEducationCheck = await _context.t_student_education_check
                    .FirstOrDefaultAsync(s => s.student_education_check_uid == studentEducationCheck.student_education_check_uid);
                _studentEducationCheck.education_check_document_uid = entity.education_check_document_uid;
                _studentEducationCheck.remark = studentEducationCheck.remark;
                _studentEducationCheck.is_correct = studentEducationCheck.is_correct;
                _studentEducationCheck.cause_failed_id = studentEducationCheck.cause_failed_id;
                _studentEducationCheck.student_education_check_status_uid = studentEducationCheck.student_education_check_status_uid;
                _context.t_student_education_check.Update(_studentEducationCheck);
            }
            await _context.SaveChangesAsync(claimsIdentity);

            return rs;
        }
    }
}