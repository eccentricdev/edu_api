using Microsoft.EntityFrameworkCore;
using rsu_common_api.models;
using rsu_common_api.models.EducationChecks;
using rsu_common_api.Models.EducationChecks;

namespace SeventyOneDev.Utilities
{
  public partial class Db : DbContext
  {
    public DbSet<t_student_education_check> t_student_education_check { get; set; }
    public DbSet<v_student_education_check> v_student_education_check { get; set; }
    public DbSet<t_education_check_document> t_education_check_document { get; set; }
    public DbSet<v_education_check_document> v_education_check_document { get; set; }
    public DbSet<t_education_check_document_status> t_education_check_document_status { get; set; }
    public DbSet<v_education_check_document_status> v_education_check_document_status { get; set; }
    public DbSet<t_student_education_check_status> t_student_education_check_status { get; set; }
    public DbSet<v_student_education_check_status> v_student_education_check_status { get; set; }
    public DbSet<v_student_education_check_waiting> v_student_education_check_waiting { get; set; }

    private void OnEducationCheckModelCreating(ModelBuilder builder, string schema)
    {
      
     
      builder.Entity<t_education_check_document>().ToTable("education_check_document", schema);
      builder.Entity<v_education_check_document>().ToView("v_education_check_document", schema);
      
      builder.Entity<t_education_check_document_status>().ToTable("education_check_document_status", schema);
      builder.Entity<v_education_check_document_status>().ToView("v_education_check_document_status", schema);
      
      builder.Entity<t_student_education_check>().ToTable("student_education_check", schema);
      // builder.Entity<t_student_education_check>().HasOne(d => d.education_check_document).WithMany(d => d.student_education_checks)
      //   .HasForeignKey(d => d.education_check_document_uid).OnDelete(DeleteBehavior.Cascade);
      builder.Entity<v_student_education_check>().ToView("v_student_education_check", schema);
      // builder.Entity<v_student_education_check>().HasOne(d => d.education_check_document).WithMany(d => d.student_education_checks)
      //   .HasForeignKey(d => d.education_check_document_uid).OnDelete(DeleteBehavior.Cascade);

      builder.Entity<t_student_education_check_status>().ToTable("student_education_check_status", schema);
      builder.Entity<v_student_education_check_status>().ToView("v_student_education_check_status", schema);
      
      
      builder.Entity<v_student_education_check_waiting>().ToView("v_student_education_check_waiting",schema).HasNoKey();
    }
  }
}
