using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using SeventyOneDev.Utilities;
using SeventyOneDev.Utilities.Attributes;

namespace rsu_common_api.models
{
    public class education_check_document_status : base_table
    {
        [Key]
        public Guid? education_check_document_status_uid {get;set;}
        public string education_check_document_status_code {get;set;}
        public string education_check_document_status_name_th {get;set;}
        public string education_check_document_status_name_en {get;set;}
        public string education_check_document_status_short_name_th {get;set;}
        public string education_check_document_status_short_name_en {get;set;}
    }

    [GeneratedUidController("api/education_check/education_check_document_status")]
    public class t_education_check_document_status : education_check_document_status
    {
          
    }
    public class v_education_check_document_status : education_check_document_status
    {

    }
}
