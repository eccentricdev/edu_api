using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;
using SeventyOneDev.Utilities;
using SeventyOneDev.Utilities.Attributes;

namespace rsu_common_api.models.EducationChecks
{
  public class student_education_check : base_table
  {
    [Key]
    public Guid? student_education_check_uid { get; set; }
    public string student_code { get; set; }
    public Guid? academic_year_uid { get; set; }
    public Guid? academic_semester_uid { get; set; }
    public int? major_id { get; set; }
    public int? school_id { get; set; }
    public int? education_level_id {get;set;}
    public int? program_type_id { get; set; }
    [MaxLength(4)]
    public string graduate_year { get; set; }
    [MaxLength(10)]
    public string gpa { get; set; }
    public Guid? student_education_check_status_uid { get; set; }
    public bool? is_correct { get; set; }
    public Guid? education_check_document_uid { get; set; }
    [MaxLength(50)]
    public string update_program {get;set;}
    [MaxLength(500)]
    public string remark {get;set;}
    [MaxLength(200)]
    public string graduate_document_no { get; set; }
    public int? cause_failed_id { get; set; }
  }
  
  public class t_student_education_check : student_education_check
  {
    // [JsonIgnore]
    // public t_education_check_document education_check_document { get; set; }
  }
  public class v_student_education_check : student_education_check
  {
    public string display_name_th {get;set;}
    public string display_name_en {get;set;}
    // public string school_code {get;set;}
    // public string school_name_th {get;set;}
    // public string school_name_en {get;set;}
    // public string academic_year_code {get;set;}
    // public string academic_year_name_th {get;set;}
    // public string academic_year_name_en {get;set;}
    // public string academic_semester_code {get;set;}
    // public string academic_semester_name_th {get;set;}
    // public string academic_semester_name_en {get;set;}
    // public string program_type_code {get;set;}
    // public string program_type_name_th {get;set;}
    // public string program_type_name_en {get;set;}
    // public string education_level_code {get;set;}
    // public string education_level_name_th {get;set;}
    // public string education_level_name_en {get;set;}
    // public string student_education_check_status_code {get;set;}
    // public string student_education_check_status_name_th {get;set;}
    // public string student_education_check_status_name_en  {get;set;}
    
    // [JsonIgnore]
    // public v_education_check_document education_check_document { get; set; }
    
  }

}
