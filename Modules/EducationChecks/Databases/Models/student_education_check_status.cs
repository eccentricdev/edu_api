using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using SeventyOneDev.Utilities;
using SeventyOneDev.Utilities.Attributes;

namespace rsu_common_api.models
{
    public class student_education_check_status : base_table
    {
        [Key]
        public Guid? student_education_check_status_uid {get;set;}
        public string student_education_check_status_code {get;set;}
        public string student_education_check_status_name_th {get;set;}
        public string student_education_check_status_name_en {get;set;}
        public string student_education_check_status_short_name_th {get;set;}
        public string student_education_check_status_short_name_en {get;set;}
        public bool? is_checked { get; set; }
    }

    [GeneratedUidController("api/education_check/student_education_check_status")]
    public class t_student_education_check_status : student_education_check_status
    {
          
    }
    public class v_student_education_check_status : student_education_check_status
    {
          
    }
}
