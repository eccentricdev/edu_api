using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using SeventyOneDev.Utilities;
using SeventyOneDev.Utilities.Attributes;

namespace rsu_common_api.models.EducationChecks
{
  public class education_check_document : base_table
  {
    [Key]
    public Guid? education_check_document_uid { get; set; }
    public Guid? academic_year_uid { get; set; }
    public Guid? academic_semester_uid { get; set; }
    public int? school_id { get; set; }
    [Column(TypeName = "date")]
    public DateTime? document_date { get; set; }
    [MaxLength(50)]
    public string document_no { get; set; }
    public Guid? education_check_document_status_uid { get; set; }
    [MaxLength(500)]
    public string remark {get;set;}
    [MaxLength(50)]
    public string update_program {get;set;}
    
    public int? no_of_students { get; set; }
    public int? no_of_students_correct { get; set; }
    public int? no_of_students_not_correct{ get; set; }
  }

  public class t_education_check_document : education_check_document
  {
    [NotMapped]
    public List<t_student_education_check> student_education_checks { get; set; }
    
  }
  public class v_education_check_document : education_check_document
  {
    public string school_code {get;set;}
    public string school_name_th {get;set;}
    // public string address1_th {get;set;}
    // public string address2_th {get;set;}
    // public string postal_code {get;set;}
    // public string address1_en {get;set;}
    // public string address2_en {get;set;}
    // public string province_id {get;set;}
    // public string province_code {get;set;}
    // public string province_name_th {get;set;}
    // public string province_name_en {get;set;}
    // public string district_id {get;set;}
    // public string district_code {get;set;}
    // public string district_name_th {get;set;}
    // public string district_name_en {get;set;}
    // public string sub_district_id {get;set;}
    // public string sub_district_code {get;set;}
    // public string sub_district_name_th {get;set;}
    // public string sub_district_name_en {get;set;}
    // public string school_name_en {get;set;}
    // public string academic_year_code {get;set;}
    // public string academic_year_name_th {get;set;}
    // public string academic_year_name_en {get;set;}
    // public string academic_semester_code {get;set;}
    // public string academic_semester_name_th {get;set;}
    // public string academic_semester_name_en { get; set; }
    // public string education_check_document_status_code {get;set;}
    // public string education_check_document_status_name_th {get;set;}
    // public string education_check_document_status_name_en {get;set;}
    
    [NotMapped]
    public List<v_student_education_check> student_education_checks { get; set; }
  }
}
