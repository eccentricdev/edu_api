using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace rsu_common_api.Models.EducationChecks
{
  public class v_student_education_check_waiting
  {
    //[Key]
    public Guid? academic_year_uid { get; set; }
    public Guid? academic_semester_uid { get; set; }
    public string school_code { get; set; }
    public string school_name_th { get; set; }
    public string school_name_en { get; set; }
    public int? total_students { get; set; }
    public DateTime? updated_datetime { get; set; }
    [NotMapped]
    public string search { get; set; }
  }
}
