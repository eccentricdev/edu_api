using edu_api.Modules.EducationChecks.Services;
using Microsoft.Extensions.DependencyInjection;

namespace edu_api.Modules.EducationChecks.Configures
{
    public static class EducationCheckCollectionExtension
    {
        public static IServiceCollection AddEducationCheckServices(this IServiceCollection services)
        {
            services.AddScoped<EducationCheckDocumentService>();
            services.AddScoped<StudentEducationCheckWaitingService>();
            services.AddScoped<StudentEducationCheckService>();

            return services;
        }
    }
}