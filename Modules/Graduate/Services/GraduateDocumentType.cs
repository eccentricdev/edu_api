
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using edu_api.Modules.Graduate.Databases.Models;
using Microsoft.EntityFrameworkCore;
using SeventyOneDev.Utilities;

namespace edu_api.Modules.Graduate.Services
{
    public class GraduateDocumentTypeService:EntityUidService<t_graduate_document_type,v_graduate_document_type>
    {
        private readonly Db _context;
        public GraduateDocumentTypeService(Db context) : base(context)
        {
            _context = context;
        }

         public override async Task<List<v_graduate_document_type>> ListEntityId(v_graduate_document_type entity, List<Guid?> agencies = null, string userName = null)
        {
             var graduateDocumentCost = await _context.v_graduate_document_cost.AsNoTracking().Where(w => w.status_id == 1).ToListAsync();
        
            var results = await base.ListEntityId(entity, agencies, userName);
            foreach(var result in results)
            {
                result.graduate_document_cost_amount = graduateDocumentCost.FirstOrDefault(w => w.graduate_document_type_uid == result.graduate_document_type_uid)?.graduate_document_cost_amount;
                result.graduate_document_cost_updated_by = graduateDocumentCost.FirstOrDefault(w => w.graduate_document_type_uid == result.graduate_document_type_uid)?.updated_by;
                result.graduate_document_cost_updated_datetime = graduateDocumentCost.FirstOrDefault(w => w.graduate_document_type_uid == result.graduate_document_type_uid)?.updated_datetime;
            }
            return results;
        }

        public override async Task<v_graduate_document_type> GetEntity(Guid uid, List<Guid?> agencies = null, string userName = null)
        {
            var graduateDocumentCost = await _context.v_graduate_document_cost.AsNoTracking().FirstOrDefaultAsync(w => w.status_id == 1 && w.graduate_document_type_uid == uid);
            var result = await base.GetEntity(uid, agencies, userName);
            if(result != null)
            {
                result.graduate_document_cost_amount = graduateDocumentCost?.graduate_document_cost_amount;
                result.graduate_document_cost_updated_by = graduateDocumentCost?.updated_by;
                result.graduate_document_cost_updated_datetime = graduateDocumentCost?.updated_datetime;
            }
            return result;
        }
    }
}