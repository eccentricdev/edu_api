using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using edu_api.Modules.Graduate.Databases.Models;
using Microsoft.EntityFrameworkCore;
using SeventyOneDev.Utilities;

namespace edu_api.Modules.Graduate.Services
{
    public class GraduateRequestService:EntityUidService<t_graduate_request,v_graduate_request>
    {
        private readonly Db _context;
        public GraduateRequestService(Db context) : base(context)
        {
            _context = context;
        }


        public override async Task<v_graduate_request> GetEntity(Guid uid, List<Guid?> agencies = null, string userName = null)
        {
            var result = await _context.v_graduate_request.AsNoTracking()
                .Include(w => w.graduate_request_profiles)
                .ThenInclude(w => w.graduate_request_profile_documents)
                .Include(w => w.graduate_request_profiles)
                .ThenInclude(w => w.graduate_request_profile_addresses)
                .Include(w => w.graduate_request_payments)
                .ThenInclude(w => w.graduate_request_payment_details)
                .FirstOrDefaultAsync(w => w.graduate_request_uid == uid);
            return result;
        }
    }
}