using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using edu_api.Modules.Graduate.Databases.Models;
using Microsoft.EntityFrameworkCore;
using SeventyOneDev.Utilities;

namespace edu_api.Modules.Graduate.Services
{
    public class GraduateFacultyCurriculumService:EntityUidService<t_graduate_faculty_curriculum,v_graduate_faculty_curriculum>
    {
        private readonly Db _context;
        public GraduateFacultyCurriculumService(Db context) : base(context)
        {
            _context = context;
        }

        public override async Task<t_graduate_faculty_curriculum> NewEntity(t_graduate_faculty_curriculum entity, ClaimsIdentity claimsIdentity)
        {
            var graduateStudents = await _context.v_graduate_register.AsNoTracking().Where(w => w.is_get_degree == true)
                .ToListAsync();
            foreach (var detail in entity.graduate_faculty_curriculum_details.OrderBy(w => w.row_order))
            {
                var studentResults = new List<t_graduate_student_final>();
                var row = 1;
                var students = graduateStudents.Where(w => w.faculty_curriculum_uid == detail.faculty_curriculum_uid);
                foreach (var student in students.Where(w => w.honor_status_code == "1" && w.honor_status_code == "2")
                             .OrderBy(w => w.gpax).ThenBy(w => w.first_name_th).ThenBy(w => w.last_name_th))
                    
                {
                    studentResults.Add(new t_graduate_student_final()
                    {
                        graduate_student_final_uid = Guid.NewGuid(),
                        student_uid = student.student_uid,
                        row_order = row,
                    });
                    row++;
                }
                
                foreach (var student in students.Where(w => w.honor_status_code != "1" && w.honor_status_code != "2")
                             .OrderBy(w => w.first_name_th).ThenBy(w => w.last_name_th))
                    
                {
                    studentResults.Add(new t_graduate_student_final()
                    {
                        graduate_student_final_uid = Guid.NewGuid(),
                        student_uid = student.student_uid,
                        row_order = row,
                    });
                    row++;
                }
                detail.graduate_student_finals = studentResults;
            }
            return await base.NewEntity(entity, claimsIdentity);
        }
        
        
        public override async Task<int> UpdateEntity(t_graduate_faculty_curriculum entity, ClaimsIdentity claimsIdentity)
        {
            var graduateStudents = await _context.v_graduate_register.AsNoTracking().Where(w => w.is_get_degree == true)
                .ToListAsync();
            foreach (var detail in entity.graduate_faculty_curriculum_details.Where(w => w.graduate_faculty_curriculum_detail_uid == null)
                         .OrderBy(w => w.row_order))
            {
                var studentResults = new List<t_graduate_student_final>();
                var row = 1;
                var students = graduateStudents.Where(w => w.faculty_curriculum_uid == detail.faculty_curriculum_uid);
                foreach (var student in students.Where(w => w.honor_status_code == "1" && w.honor_status_code == "2")
                             .OrderBy(w => w.gpax).ThenBy(w => w.first_name_th).ThenBy(w => w.last_name_th))
                    
                {
                    studentResults.Add(new t_graduate_student_final()
                    {
                        graduate_student_final_uid = Guid.NewGuid(),
                        student_uid = student.student_uid,
                        row_order = row,
                    });
                    row++;
                }
                
                foreach (var student in students.Where(w => w.honor_status_code != "1" && w.honor_status_code != "2")
                             .OrderBy(w => w.first_name_th).ThenBy(w => w.last_name_th))
                    
                {
                    studentResults.Add(new t_graduate_student_final()
                    {
                        graduate_student_final_uid = Guid.NewGuid(),
                        student_uid = student.student_uid,
                        row_order = row,
                    });
                    row++;
                }
                detail.graduate_student_finals = studentResults;
            }
            return await base.UpdateEntity(entity, claimsIdentity);
        }
    }
}