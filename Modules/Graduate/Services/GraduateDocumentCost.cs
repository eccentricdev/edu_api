using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using edu_api.Modules.Graduate.Databases.Models;
using SeventyOneDev.Utilities;

namespace edu_api.Modules.Graduate.Services
{
    public class GraduateDocumentCostService:EntityUidService<t_graduate_document_cost,v_graduate_document_cost>
    {
        private readonly Db _context;
        public GraduateDocumentCostService(Db context) : base(context)
        {
            _context = context;
        }

        public override async Task<t_graduate_document_cost> NewEntity(t_graduate_document_cost entity, ClaimsIdentity claimsIdentity)
        {
            var currCost = _context.t_graduate_document_cost.FirstOrDefault(w => w.status_id == 1 && w.graduate_document_type_uid == entity.graduate_document_type_uid);
            if(currCost != null)
            {
                currCost.status_id = 2;
                _context.t_graduate_document_cost.Update(currCost);
                await _context.SaveChangesAsync(claimsIdentity);
            }
            
            entity.status_id = 1;
            return await base.NewEntity(entity, claimsIdentity);
        }

    }
}