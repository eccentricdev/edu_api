using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using edu_api.Modules.Graduate.Databases.Models;
using Microsoft.EntityFrameworkCore;
using SeventyOneDev.Utilities;

namespace edu_api.Modules.Graduate.Services
{
    public class GraduateStudentFinalService:EntityUidService<t_graduate_student_final,v_graduate_student_final>
    {
        private readonly Db _context;
        public GraduateStudentFinalService(Db context) : base(context)
        {
            _context = context;
        }
        
        public override async Task<List<v_graduate_student_final>> ListEntityId(v_graduate_student_final entity, List<Guid?> agencies = null, string userName = null)
        {
            var rs = await base.ListEntityId(entity, agencies, userName);
            return rs.OrderBy(w => w.faculty_curriculum_row_order).ThenBy(w => w.row_order).ToList();
        }

        public override async Task<PagedResult<v_graduate_student_final>> PageEntityId(v_graduate_student_final entity, int page, int size, List<Guid?> agencies = null, string userName = null)
        {
            var entities = _context.v_graduate_student_final.AsNoTracking();
            if (agencies != null)
            {
                if (agencies.Any())
                {
                    entities = entities.Where(e => agencies.Any(a => a == e.owner_agency_uid));
                }
            }
            else if (!string.IsNullOrEmpty(userName) )
            {
                entities = entities.Where(e => e.created_by == userName);
            }
            var p =entity?.GetType().GetProperties().FirstOrDefault(a => a.Name == "search");
            if (p == null) return await entities.OrderBy(w => w.faculty_curriculum_row_order).ThenBy(w => w.row_order).GetPaged(page, size);
            var con = p.GetValue(entity);
            entities = con != null ? entities.Search(con.ToString()) : entities.Search(entity);
            return await  entities.OrderBy(w => w.faculty_curriculum_row_order).ThenBy(w => w.row_order).GetPaged(page, size);
        }
    }
}