
using edu_api.Modules.Graduate.Databases.Models;
using Microsoft.EntityFrameworkCore;

namespace SeventyOneDev.Utilities
{
    public partial class Db : DbContext
    {
        public DbSet<v_student_curriculum_subject>  v_student_curriculum_subject { get; set; }
        public DbSet<v_student_curriculum_subject_all>  v_student_curriculum_subject_all { get; set; }
        public DbSet<t_graduate_request>  t_graduate_request { get; set; }
        public DbSet<v_graduate_request>  v_graduate_request { get; set; }

        public DbSet<t_graduate_document_type>  t_graduate_document_type { get; set; }
        public DbSet<v_graduate_document_type>  v_graduate_document_type { get; set; }

        public DbSet<t_graduate_document_cost>  t_graduate_document_cost { get; set; }
        public DbSet<v_graduate_document_cost>  v_graduate_document_cost { get; set; }

        public DbSet<t_graduate_request_profile>  t_graduate_request_profile { get; set; }
        public DbSet<v_graduate_request_profile>  v_graduate_request_profile { get; set; }

        public DbSet<t_graduate_request_profile_document>  t_graduate_request_profile_document { get; set; }
        public DbSet<v_graduate_request_profile_document>  v_graduate_request_profile_document { get; set; }

        public DbSet<t_graduate_request_profile_address>  t_graduate_request_profile_address { get; set; }
        public DbSet<v_graduate_request_profile_address>  v_graduate_request_profile_address { get; set; }

        public DbSet<t_graduate_request_payment>  t_graduate_request_payment { get; set; }
        public DbSet<v_graduate_request_payment>  v_graduate_request_payment { get; set; }

        public DbSet<t_graduate_request_payment_detail>  t_graduate_request_payment_detail { get; set; }
        public DbSet<v_graduate_request_payment_detail>  v_graduate_request_payment_detail { get; set; }

        public DbSet<t_graduate_register>  t_graduate_register { get; set; }
        public DbSet<v_graduate_register>  v_graduate_register { get; set; }

        public DbSet<t_graduate_register_payment>  t_graduate_register_payment { get; set; }
        public DbSet<v_graduate_register_payment>  v_graduate_register_payment { get; set; }

        public DbSet<t_graduate_register_question>  t_graduate_register_question { get; set; }
        public DbSet<v_graduate_register_question>  v_graduate_register_question { get; set; }

        public DbSet<t_graduate_student>  t_graduate_student { get; set; }
        public DbSet<v_graduate_student>  v_graduate_student { get; set; }
        
        public DbSet<t_graduate_faculty_curriculum>  t_graduate_faculty_curriculum { get; set; }
        public DbSet<v_graduate_faculty_curriculum>  v_graduate_faculty_curriculum { get; set; }
        
        public DbSet<t_graduate_faculty_curriculum_detail>  t_graduate_faculty_curriculum_detail { get; set; }
        public DbSet<v_graduate_faculty_curriculum_detail>  v_graduate_faculty_curriculum_detail { get; set; }
        
        public DbSet<t_graduate_student_final>  t_graduate_student_final { get; set; }
        public DbSet<v_graduate_student_final>  v_graduate_student_final { get; set; }

        private void OnGraduateModelCreating(ModelBuilder builder, string schema)
        {
            builder.Entity<v_student_curriculum_subject>().ToView("v_student_curriculum_subject", schema).HasNoKey();
            builder.Entity<v_student_curriculum_subject_all>().ToView("v_student_curriculum_subject_all", schema).HasNoKey();

            builder.Entity<t_graduate_request>().ToTable("graduate_request", schema);
            builder.Entity<v_graduate_request>().ToView("v_graduate_request", schema);

            builder.Entity<t_graduate_document_type>().ToTable("graduate_document_type", schema);
            builder.Entity<v_graduate_document_type>().ToView("v_graduate_document_type", schema);

            builder.Entity<t_graduate_document_cost>().ToTable("graduate_document_cost", schema);
            builder.Entity<v_graduate_document_cost>().ToView("v_graduate_document_cost", schema);

            builder.Entity<t_graduate_request_profile>().ToTable("graduate_request_profile", schema);
            builder.Entity<t_graduate_request_profile>().HasOne(t => t.graduate_request).WithMany(t => t.graduate_request_profiles)
                .HasForeignKey(t => t.graduate_request_uid).OnDelete(DeleteBehavior.Cascade);
            builder.Entity<v_graduate_request_profile>().ToView("v_graduate_request_profile", schema);
            builder.Entity<v_graduate_request_profile>().HasOne(t => t.graduate_request).WithMany(t => t.graduate_request_profiles)
                .HasForeignKey(t => t.graduate_request_uid).OnDelete(DeleteBehavior.Cascade);

            builder.Entity<t_graduate_request_profile_document>().ToTable("graduate_request_profile_document", schema);
            builder.Entity<t_graduate_request_profile_document>().HasOne(t => t.graduate_request_profile).WithMany(t => t.graduate_request_profile_documents)
                .HasForeignKey(t => t.graduate_request_profile_uid).OnDelete(DeleteBehavior.Cascade);
            builder.Entity<v_graduate_request_profile_document>().ToView("v_graduate_request_profile_document", schema);
            builder.Entity<v_graduate_request_profile_document>().HasOne(t => t.graduate_request_profile).WithMany(t => t.graduate_request_profile_documents)
                .HasForeignKey(t => t.graduate_request_profile_uid).OnDelete(DeleteBehavior.Cascade);

            builder.Entity<t_graduate_request_profile_address>().ToTable("graduate_request_profile_address", schema);
            builder.Entity<t_graduate_request_profile_address>().HasOne(t => t.graduate_request_profile).WithMany(t => t.graduate_request_profile_addresses)
                .HasForeignKey(t => t.graduate_request_profile_uid).OnDelete(DeleteBehavior.Cascade);
            builder.Entity<v_graduate_request_profile_address>().ToView("v_graduate_request_profile_address", schema);
            builder.Entity<v_graduate_request_profile_address>().HasOne(t => t.graduate_request_profile).WithMany(t => t.graduate_request_profile_addresses)
                .HasForeignKey(t => t.graduate_request_profile_uid).OnDelete(DeleteBehavior.Cascade);

            builder.Entity<t_graduate_request_payment>().ToTable("graduate_request_payment", schema);
            builder.Entity<t_graduate_request_payment>().HasOne(t => t.graduate_request).WithMany(t => t.graduate_request_payments)
                .HasForeignKey(t => t.graduate_request_uid).OnDelete(DeleteBehavior.Cascade);
            builder.Entity<v_graduate_request_payment>().ToView("v_graduate_request_payment", schema);
            builder.Entity<v_graduate_request_payment>().HasOne(t => t.graduate_request).WithMany(t => t.graduate_request_payments)
                .HasForeignKey(t => t.graduate_request_uid).OnDelete(DeleteBehavior.Cascade);

            builder.Entity<t_graduate_request_payment_detail>().ToTable("graduate_request_payment_detail", schema);
            builder.Entity<t_graduate_request_payment_detail>().HasOne(t => t.graduate_request_payment).WithMany(t => t.graduate_request_payment_details)
                .HasForeignKey(t => t.graduate_request_payment_uid).OnDelete(DeleteBehavior.Cascade);
            builder.Entity<v_graduate_request_payment_detail>().ToView("v_graduate_request_payment_detail", schema);
            builder.Entity<v_graduate_request_payment_detail>().HasOne(t => t.graduate_request_payment).WithMany(t => t.graduate_request_payment_details)
                .HasForeignKey(t => t.graduate_request_payment_uid).OnDelete(DeleteBehavior.Cascade);

            builder.Entity<t_graduate_register>().ToTable("graduate_register", schema);
            builder.Entity<v_graduate_register>().ToView("v_graduate_register", schema);

            builder.Entity<t_graduate_register_payment>().ToTable("graduate_register_payment", schema);
            builder.Entity<t_graduate_register_payment>().HasOne(t => t.graduate_register).WithMany(t => t.graduate_register_payments)
                .HasForeignKey(t => t.graduate_register_uid).OnDelete(DeleteBehavior.Cascade);
            builder.Entity<v_graduate_register_payment>().ToView("v_graduate_register_payment", schema);
            builder.Entity<v_graduate_register_payment>().HasOne(t => t.graduate_register).WithMany(t => t.graduate_register_payments)
                .HasForeignKey(t => t.graduate_register_uid).OnDelete(DeleteBehavior.Cascade);

            builder.Entity<t_graduate_register_question>().ToTable("graduate_register_question", schema);
            builder.Entity<t_graduate_register_question>().HasOne(t => t.graduate_register).WithMany(t => t.graduate_register_questions)
                .HasForeignKey(t => t.graduate_register_uid).OnDelete(DeleteBehavior.Cascade);
            builder.Entity<v_graduate_register_question>().ToView("v_graduate_register_question", schema);
            builder.Entity<v_graduate_register_question>().HasOne(t => t.graduate_register).WithMany(t => t.graduate_register_questions)
                .HasForeignKey(t => t.graduate_register_uid).OnDelete(DeleteBehavior.Cascade);

            builder.Entity<t_graduate_student>().ToTable("graduate_student", schema);
            builder.Entity<v_graduate_student>().ToView("v_graduate_student", schema);
            
            builder.Entity<t_graduate_faculty_curriculum>().ToTable("graduate_faculty_curriculum", schema);
            builder.Entity<v_graduate_faculty_curriculum>().ToView("v_graduate_faculty_curriculum", schema);
            
            builder.Entity<t_graduate_faculty_curriculum_detail>().ToTable("graduate_faculty_curriculum_detail", schema);
            builder.Entity<t_graduate_faculty_curriculum_detail>().HasOne(t => t.graduate_faculty_curriculum).WithMany(t => t.graduate_faculty_curriculum_details)
                .HasForeignKey(t => t.graduate_faculty_curriculum_uid).OnDelete(DeleteBehavior.Cascade);
            builder.Entity<v_graduate_faculty_curriculum_detail>().ToView("v_graduate_faculty_curriculum_detail", schema);
            builder.Entity<v_graduate_faculty_curriculum_detail>().HasOne(t => t.graduate_faculty_curriculum).WithMany(t => t.graduate_faculty_curriculum_details)
                .HasForeignKey(t => t.graduate_faculty_curriculum_uid).OnDelete(DeleteBehavior.Cascade);
            
            builder.Entity<t_graduate_student_final>().ToTable("graduate_student_final", schema);
            builder.Entity<t_graduate_student_final>().HasOne(t => t.graduate_faculty_curriculum_detail).WithMany(t => t.graduate_student_finals)
                .HasForeignKey(t => t.graduate_faculty_curriculum_detail_uid).OnDelete(DeleteBehavior.Cascade);
            builder.Entity<v_graduate_student_final>().ToView("v_graduate_student_final", schema);
            builder.Entity<v_graduate_student_final>().HasOne(t => t.graduate_faculty_curriculum_detail).WithMany(t => t.graduate_student_finals)
                .HasForeignKey(t => t.graduate_faculty_curriculum_detail_uid).OnDelete(DeleteBehavior.Cascade);
        }

    }
}
