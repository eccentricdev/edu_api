using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using SeventyOneDev.Utilities;

namespace edu_api.Modules.Graduate.Databases.Models
{
  public class graduate_student:base_table
  {
    [Key]
    public Guid? graduate_student_uid { get; set; }
    public Guid? student_uid { get; set; }
    public Guid? graduate_request_uid { get; set; }
    [Column(TypeName = "decimal(5,2)")]
    public decimal? attended_credit { get; set; }
    [Column(TypeName = "decimal(5,2)")]
    public decimal? earned_credit { get; set; }
    [Column(TypeName = "decimal(5,2)")]
    public decimal? total_gpa { get; set; }
    [Column(TypeName = "decimal(3,2)")]
    public decimal? gpa { get; set; }
    public DateTime? entry_date { get; set; }
    public DateTime? graduate_date { get; set; }
    public int? year_study { get; set; }
    public int? semester_study { get; set; }
    public int? maximum_study_year { get; set; }
    public int? curriculum_study_year { get; set; }
    public bool? is_study_period_pass { get; set; }
    public string study_period_remark { get; set; }

    [Column(TypeName = "decimal(5,2)")]
    public decimal? curriculum_credit { get; set; }
    [Column(TypeName = "decimal(5,2)")]
    public decimal? curriculum_earned_credit { get; set; }
    [Column(TypeName = "decimal(5,2)")]
    public decimal? curriculum_attended_credit { get; set; }
    public bool? is_curriculum_credit_pass { get; set; }
    public string curriculum_credit_remark { get; set; }

    [Column(TypeName = "decimal(3,2)")]
    public decimal? curriculum_gpa { get; set; }
    [Column(TypeName = "decimal(5,2)")]
    public decimal? total_passed_gp { get; set; }
    [Column(TypeName = "decimal(3,2)")]
    public decimal? passed_gpa { get; set; }
    public bool? is_curriculum_gpa_pass { get; set; }
    public string curriculum_gpa_remark { get; set; }

    [Column(TypeName = "decimal(3,2)")]
    public decimal? facultry_gpa { get; set; }
    public bool? is_facultry_gpa_pass { get; set; }
    public string facultry_gpa_remark { get; set; }

    public bool? is_subject_score_pass { get; set; }
    public string subject_score_remark { get; set; }
    public bool? is_gradudate_check_pass { get; set; }

    public bool? is_honor { get; set; }
    public Guid? honor_level_uid { get; set; }
    public bool? is_honor_pass { get; set; }
    public string honor_remark { get; set; }

    public bool? has_honor_re_grade { get; set; }
    public bool? is_honor_re_grade_pass { get; set; }
    public string honor_re_grade_remark { get; set; }
    public bool? has_honor_education_leave { get; set; }
    public bool? is_honor_education_leave_pass { get; set; }
    public string honor_education_leave_remark { get; set; }

    public bool? is_honor_credit_check_pass { get; set; }
    public string honor_credit_check_remark { get; set; }

    public bool? is_honor_subject_score_pass { get; set; }
    public string honor_subject_score_remark { get; set; }
    public bool? is_honor_gradudate_pass { get; set; }

    public bool? is_honor_check_pass { get; set; }
    
    public int? honor_status_id { get; set; }
    
    //บันทึกการจบการศึกษา
    public bool? is_record_graduate{ get; set; }
    public Guid? graduate_academic_year_uid { get; set; }
    public Guid? graduate_academic_semester_uid { get; set; }
    public int? generation { get; set; }
    public int? round { get; set; }
    
  }

  public class t_graduate_student : graduate_student
  {

  }

  public class v_graduate_student : graduate_student
  {
    public string student_code { get; set; }
    public string display_name_th { get; set; }
    public string display_name_en { get; set; }
    public Guid? college_faculty_uid { get; set; }
    public string college_faculty_name_th { get; set; }
    public string college_faculty_name_en { get; set; }
    public Guid? faculty_curriculum_uid { get; set; }
    public string faculty_curriculum_name_th { get; set; }
    public string faculty_curriculum_name_en { get; set; }
    public Guid? education_type_uid { get; set; }
    public string education_type_name_th {get;set;}
    public string education_type_name_en {get;set;}
    
    public string graduate_academic_year_name_th {get;set;}
    public string graduate_academic_semester_name_th {get;set;}
    
  }
}
