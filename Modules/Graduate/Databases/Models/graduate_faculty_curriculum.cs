using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using SeventyOneDev.Utilities;
using SeventyOneDev.Utilities.Attributes;

namespace edu_api.Modules.Graduate.Databases.Models
{
    public class graduate_faculty_curriculum : base_table
    {
        [Key]
        public Guid? graduate_faculty_curriculum_uid { get; set; }
        [OrderBy(false)]
        public Guid? graduate_academic_year_uid { get; set; } 
        [ThenOrderBy(false)]
        public int? round { get; set; }
        public DateTime? graduation_date { get; set; }
    }
    
    public class t_graduate_faculty_curriculum : graduate_faculty_curriculum
    {
        [Include]
        public List<t_graduate_faculty_curriculum_detail> graduate_faculty_curriculum_details { get; set; }
    }
    
    public class v_graduate_faculty_curriculum : graduate_faculty_curriculum
    {
        [Include]
        public List<v_graduate_faculty_curriculum_detail> graduate_faculty_curriculum_details { get; set; }
        
        public string graduate_academic_year_name_th {get;set;}
    }
}