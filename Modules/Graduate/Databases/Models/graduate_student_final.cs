using System;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using SeventyOneDev.Utilities;
using SeventyOneDev.Utilities.Attributes;

namespace edu_api.Modules.Graduate.Databases.Models
{
    public class graduate_student_final : base_table
    {
        [Key]
        public Guid? graduate_student_final_uid { get; set; }
        public Guid? student_uid { get; set; }
        [OrderBy(false)]
        public int? row_order { get; set; }
        public Guid? graduate_faculty_curriculum_detail_uid { get; set; }
        public bool? is_rehearsal { get; set; }
        public int? remark_no_rehearsal { get; set; }
    }

    public class t_graduate_student_final : graduate_student_final
    {
        [JsonIgnore]
        public t_graduate_faculty_curriculum_detail graduate_faculty_curriculum_detail { get; set; }
    }
    
    
    public class v_graduate_student_final : graduate_student_final
    {
        [JsonIgnore]
        public v_graduate_faculty_curriculum_detail graduate_faculty_curriculum_detail { get; set; }
        
        public Guid? graduate_faculty_curriculum_uid { get; set; }
        public int? faculty_curriculum_row_order { get; set; }
        public string student_code { get; set; }
        public string display_name_th { get; set; }
        public string display_name_en { get; set; }
        public Guid? college_faculty_uid { get; set; }
        public string college_faculty_name_th { get; set; }
        public string college_faculty_name_en { get; set; }
        public Guid? faculty_curriculum_uid { get; set; }
        public string faculty_curriculum_name_th { get; set; }
        public string faculty_curriculum_name_en { get; set; }
        public string honor_status_code { get; set; }
        public string honor_status_name_th { get; set; }
    }
}