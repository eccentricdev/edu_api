using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;
using SeventyOneDev.Utilities;
using SeventyOneDev.Utilities.Attributes;

namespace edu_api.Modules.Graduate.Databases.Models
{
    public class graduate_request:base_table
    {
        [Key]
        public Guid? graduate_request_uid { get; set; }
        public Guid? student_uid { get; set; }
        [OrderBy(true)]
        public DateTime? request_date { get; set; }
        public bool? is_check { get; set; }
        public bool? is_approve { get; set; }
        public int? step { get; set; }
        public Guid? bill_uid { get; set;}
        public string bill_no {get;set;}
        public string graduate_request_code {get;set;}
        
    }

    public class t_graduate_request : graduate_request
    {
        [Include]
        public List<t_graduate_request_profile> graduate_request_profiles { get; set; }
        [Include]
        public List<t_graduate_request_payment> graduate_request_payments { get; set; }
    }

    public class v_graduate_request : graduate_request
    {
        public string student_code { get; set; }
        public string display_name_th { get; set; }
        public string display_name_en { get; set; }
        public string college_faculty_name_th { get; set; }
        public string college_faculty_name_en { get; set; }
        public string faculty_curriculum_name_th { get; set; }
        public string faculty_curriculum_name_en { get; set; }
        
        public Guid? receipt_uid {get;set;}

        [Include]
        public List<v_graduate_request_profile> graduate_request_profiles { get; set; }
        [Include]
        public List<v_graduate_request_payment> graduate_request_payments { get; set; }
    }
}