using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;
using SeventyOneDev.Utilities;
using SeventyOneDev.Utilities.Attributes;

namespace edu_api.Modules.Graduate.Databases.Models
{
    public class graduate_request_payment_detail: base_table
    {
        [Key]
        public Guid? graduate_request_payment_detail_uid { get; set; }
        public Guid? graduate_request_payment_uid { get; set; }

        public Guid?  graduate_document_type_uid {get; set;}
        public decimal? amout {get;set;}
    }

     public class t_graduate_request_payment_detail : graduate_request_payment_detail
    {
        [JsonIgnore]
        public t_graduate_request_payment graduate_request_payment { get; set; }
    }

    public class v_graduate_request_payment_detail : graduate_request_payment_detail
    {
        public string graduate_document_type_code { get; set; }
        public string graduate_document_type_name_th { get; set; }
        public string graduate_document_type_name_en { get; set; }
        [JsonIgnore]
        public v_graduate_request_payment graduate_request_payment { get; set; }
    }
}