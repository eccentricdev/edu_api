using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;
using SeventyOneDev.Utilities;
using SeventyOneDev.Utilities.Attributes;

namespace edu_api.Modules.Graduate.Databases.Models
{
    public class graduate_request_payment : base_table
    {
        [Key]
        public Guid? graduate_request_payment_uid { get; set; }
        public Guid? graduate_request_uid { get; set; }
        public bool? is_pick_up_by_yourself { get; set; }
        public decimal? total_amout {get;set;}
    }
    
    [GeneratedUidController("api/graduate/graduate_request_payment")]
    public class t_graduate_request_payment : graduate_request_payment
    {
        [JsonIgnore]
        public t_graduate_request graduate_request { get; set; }
        [Include]
        public List<t_graduate_request_payment_detail> graduate_request_payment_details { get; set; }
    }

    public class v_graduate_request_payment : graduate_request_payment
    {
        [JsonIgnore]
        public v_graduate_request graduate_request { get; set; }
        [Include]
        public List<v_graduate_request_payment_detail> graduate_request_payment_details { get; set; }
    }
}