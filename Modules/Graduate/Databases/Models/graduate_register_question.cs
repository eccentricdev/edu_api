using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;
using SeventyOneDev.Utilities;
using SeventyOneDev.Utilities.Attributes;

namespace edu_api.Modules.Graduate.Databases.Models
{
    public class graduate_register_question : base_table
    {
        [Key]
        public Guid? graduate_register_question_uid { get; set;}
        public Guid? graduate_register_uid { get; set;}
        public int? question_id { get; set;}
        public string question_title { get; set;}
        public int? answer_type { get; set;}
        public string answer { get; set;}
    }

    [GeneratedUidController("api/graduate/graduate_register_question")]
    public class t_graduate_register_question : graduate_register_question
    {
        [JsonIgnore]
        public t_graduate_register graduate_register { get; set; }
    }
    
    public class v_graduate_register_question : graduate_register_question
    {
        [JsonIgnore]
        public v_graduate_register graduate_register { get; set; }
    }
}