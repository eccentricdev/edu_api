using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;
using SeventyOneDev.Utilities;
using SeventyOneDev.Utilities.Attributes;

namespace edu_api.Modules.Graduate.Databases.Models
{
    public class graduate_request_profile_address  : base_table
    {
        [Key]
        public Guid? graduate_request_profile_address_uid { get; set; }
        public Guid? graduate_request_profile_uid { get; set; }
        public int? address_type_id { get; set; }
        // public string address_name { get; set; }
        public string house_no { get; set; }
        // public string room_no { get; set; }
        // public string floor_no { get; set; }
        // public string building_name { get; set; }
        // public string village_name { get; set; }
        // public string village_no { get; set; }
        // public string alley_name { get; set; }
        // public string street_name { get; set; }

        public int? sub_district_id { get; set; }
        public int? district_id { get; set; }
        public int? province_id { get; set; }

        // public string other_sub_district_name_th { get; set; }
        // public string other_sub_district_name_en { get; set; }
        // public string other_district_name_th { get; set; }
        // public string other_district_name_en { get; set; }
        // public string other_province_name_th { get; set; }
        // public string other_province_name_en { get; set; }

        public string postal_code { get; set; }
        public string telephone_no { get; set; }
        public string email { get; set; }

    }
    public class t_graduate_request_profile_address :graduate_request_profile_address
    {
        [JsonIgnore]
        public t_graduate_request_profile graduate_request_profile { get; set; }
    }
    public class v_graduate_request_profile_address :graduate_request_profile_address
    {
        public string sub_district_name_th { get; set; }
        public string province_name_th { get; set; }
        public string district_name_th { get; set; }
        [JsonIgnore]
        public v_graduate_request_profile graduate_request_profile { get; set; }
    }
}