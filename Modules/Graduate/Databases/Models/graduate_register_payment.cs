using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;
using SeventyOneDev.Utilities;
using SeventyOneDev.Utilities.Attributes;

namespace edu_api.Modules.Graduate.Databases.Models
{
    public class graduate_register_payment: base_table
    {
        [Key]
        public Guid? graduate_register_payment_uid { get; set; }
        public Guid? graduate_register_uid { get; set;}

        public Guid?  graduate_document_type_uid {get; set;}
        public decimal? amout {get;set;}
    }

    [GeneratedUidController("api/graduate/graduate_register_payment")]
     public class t_graduate_register_payment : graduate_register_payment
    {
        [JsonIgnore]
        public t_graduate_register graduate_register { get; set; }
    }

    public class v_graduate_register_payment : graduate_register_payment
    {
        public string graduate_document_type_code { get; set; }
        public string graduate_document_type_name_th { get; set; }
        public string graduate_document_type_name_en { get; set; }
        [JsonIgnore]
        public v_graduate_register graduate_register { get; set; }
    }
}