using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;
using SeventyOneDev.Utilities;
using SeventyOneDev.Utilities.Attributes;

namespace edu_api.Modules.Graduate.Databases.Models
{
    public class graduate_request_profile:base_table
    {
        [Key]
        public Guid? graduate_request_profile_uid { get; set; }
        public Guid? graduate_request_uid { get; set; }

        public bool? is_wrong_name_th { get; set; }
        public string correct_name_th { get; set; }

        public bool? is_wrong_last_name_th { get; set; }
        public string correct_last_name_th { get; set; }

        public bool? is_wrong_name_en { get; set; }
        public string correct_name_en { get; set; }

        public bool? is_wrong_last_name_en { get; set; }
        public string correct_last_name_en { get; set; }

        public bool? is_wrong_date_of_birth { get; set; }
        public DateTime? correct_date_of_birth  { get; set; }

        public bool? is_wrong_citizen_id { get; set; }
        public string correct_citizen_id { get; set; }


    }

    [GeneratedUidController("api/graduate/graduate_request_profile")]
    public class t_graduate_request_profile :graduate_request_profile
    {
        [JsonIgnore]
        public t_graduate_request graduate_request { get; set; }
        [Include]
        public List<t_graduate_request_profile_document> graduate_request_profile_documents { get; set; }
        [Include] 
        public List<t_graduate_request_profile_address> graduate_request_profile_addresses { get; set; }

    }

    public class v_graduate_request_profile :graduate_request_profile
    {
        [JsonIgnore]
        public v_graduate_request graduate_request { get; set; }
        [Include]
        public List<v_graduate_request_profile_document> graduate_request_profile_documents { get; set; }
        [Include] 
        public List<v_graduate_request_profile_address> graduate_request_profile_addresses { get; set; }
        
    }
}