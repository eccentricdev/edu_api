using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using SeventyOneDev.Utilities;
using SeventyOneDev.Utilities.Attributes;

namespace edu_api.Modules.Graduate.Databases.Models
{
    public class graduate_register : base_table
    {
        [Key]
        public Guid? graduate_register_uid { get; set;}
        public string graduate_register_code {get;set;}
        public Guid? student_uid { get; set;}
        [OrderBy(true)]
        public DateTime? request_date { get; set; }
        public bool? is_check { get; set; }
        public bool? is_approve { get; set; }
        public Guid? bill_uid { get; set;}
        public string bill_no {get;set;}
        
        public bool? is_get_degree { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public decimal? total_amout {get;set;}
    }
    
    public class t_graduate_register : graduate_register
    {
        [Include]
        public List<t_graduate_register_question> graduate_register_questions { get; set; }
        [Include]
        public List<t_graduate_register_payment> graduate_register_payments { get; set; }
    }

    public class v_graduate_register : graduate_register
    {
        public string student_code { get; set; }
        public string display_name_th { get; set; }
        public string display_name_en { get; set; }
        public Guid? college_faculty_uid { get; set; }
        public string college_faculty_name_th { get; set; }
        public string college_faculty_name_en { get; set; }
        public Guid? faculty_curriculum_uid { get; set; }
        public string faculty_curriculum_name_th { get; set; }
        public string faculty_curriculum_name_en { get; set; }
        public Guid? education_type_uid { get; set; }
        public string education_type_name_th {get;set;}
        public string education_type_name_en {get;set;}

        public Guid? receipt_uid {get;set;}
        
        public string honors {get;set;}
        public decimal? gpax { get; set; }
        public string first_name_th { get; set; }
        public string last_name_th { get; set; }
        public int? honor_status_id { get; set; }
        public string honor_status_code { get; set; }
        public string honor_status_name_th { get; set; }
        
        [Include]
        public List<v_graduate_register_question> graduate_register_questions { get; set; }
        [Include]
        public List<v_graduate_register_payment> graduate_register_payments { get; set; }
    }
}