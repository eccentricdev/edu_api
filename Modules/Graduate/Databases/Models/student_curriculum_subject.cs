using System;

namespace edu_api.Modules.Graduate.Databases.Models
{
    public class v_student_curriculum_subject
    {
        public Guid? curriculum_subject_category_uid {get;set;}
        public string curriculum_subject_category_code {get;set;}
        public string curriculum_subject_category_name_th {get;set;}
        public string curriculum_subject_category_name_en {get;set;}
        public Guid? curriculum_subject_group_uid {get;set;}
        public string curriculum_subject_group_code {get;set;}
        public string curriculum_subject_group_name_th {get;set;}
        public string curriculum_subject_group_name_en {get;set;}
        public string year_subject_code {get;set;}
        public string year_subject_name_th {get;set;}
        public string year_subject_name_en {get;set;}
        public Guid? student_uid {get;set;}
        public Guid? academic_year_uid {get;set;}
        public Guid? academic_semester_uid {get;set;}
        public int? credit {get;set;}
        public Guid? grade_uid {get;set;}
        public string grade_name_th {get;set;}
        public decimal? grade_point {get;set;}
public string academic_year_code { get; set; }
public string academic_semester_code { get; set; }

        public v_student_curriculum_subject()
        {
            
        }

        public v_student_curriculum_subject(v_student_curriculum_subject_all studentCurriculumSubjectAll)
        {
        curriculum_subject_category_uid = Guid.Empty;
        curriculum_subject_category_code = "3";
        curriculum_subject_category_name_th = "หมวดวิชาเลือกเสรี";
        curriculum_subject_group_uid = Guid.Empty;
        curriculum_subject_group_code = "3";
        curriculum_subject_group_name_th = "กลุ่มวิชาเลือกเสรี";

        curriculum_subject_category_name_en = null;
            year_subject_code = studentCurriculumSubjectAll.year_subject_code;
            year_subject_name_th = studentCurriculumSubjectAll.year_subject_name_th;
            year_subject_name_en = studentCurriculumSubjectAll.year_subject_name_en;
            student_uid = studentCurriculumSubjectAll.student_uid;
            academic_year_uid = studentCurriculumSubjectAll.academic_year_uid;
            academic_semester_uid = studentCurriculumSubjectAll.academic_semester_uid;
            credit = studentCurriculumSubjectAll.credit;
            grade_uid = studentCurriculumSubjectAll.grade_uid;
            grade_name_th = studentCurriculumSubjectAll.grade_name_th;
            grade_point = studentCurriculumSubjectAll.grade_point;
            academic_year_code = studentCurriculumSubjectAll.academic_year_code;
            academic_semester_code = studentCurriculumSubjectAll.academic_semester_code;
            
        

        }
    }

    public class v_student_curriculum_subject_all
    {
        public string year_subject_code {get;set;}
        public string year_subject_name_th {get;set;}
        public string year_subject_name_en {get;set;}
        public Guid? student_uid {get;set;}
        public Guid? academic_year_uid {get;set;}
        public Guid? academic_semester_uid {get;set;}
        public int? credit {get;set;}
        public Guid? grade_uid {get;set;}
        public string grade_name_th {get;set;}
        public decimal? grade_point {get;set;}
        
        public string academic_year_code { get; set; }
        public string academic_semester_code { get; set; }
    }
}