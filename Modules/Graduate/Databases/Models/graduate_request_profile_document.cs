using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;
using SeventyOneDev.Utilities;
using SeventyOneDev.Utilities.Attributes;

namespace edu_api.Modules.Graduate.Databases.Models
{
    public class graduate_request_profile_document:base_table
  {
    [Key]
    public Guid? graduate_request_profile_document_uid { get; set; }
    public Guid? graduate_request_profile_uid { get; set; }
    public Guid? request_document_type_uid {get;set;}
    public string request_document_type_name  {get;set;}
    public string file_name {get;set;}
    public string document_name  {get;set;}
    public string document_url  {get;set;}
    [MaxLength(50)]public string mime_type {get;set;}
    public int? document_status_id {get;set;}
  }
  public class t_graduate_request_profile_document : graduate_request_profile_document
  {
    [JsonIgnore]
    public t_graduate_request_profile graduate_request_profile { get; set; }
  }

  public class v_graduate_request_profile_document : graduate_request_profile_document
  {
    [JsonIgnore]
    public v_graduate_request_profile graduate_request_profile { get; set; }
  }
}