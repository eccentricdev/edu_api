using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using SeventyOneDev.Utilities;

namespace edu_api.Modules.Graduate.Databases.Models
{
    public class graduate_document_cost : base_table
    { 
        [Key]
        public Guid? graduate_document_cost_uid {get;set;}
        public Guid?  graduate_document_type_uid {get; set;}

        [Column(TypeName = "decimal(18,2)")]
        public decimal? graduate_document_cost_amount {get;set;}
    }


    public class t_graduate_document_cost : graduate_document_cost
    {

    }

    public class v_graduate_document_cost : graduate_document_cost
    {
        public string graduate_document_type_code { get; set; }
        public string graduate_document_type_name_th { get; set; }
        public string graduate_document_type_name_en { get; set; }
    }
}