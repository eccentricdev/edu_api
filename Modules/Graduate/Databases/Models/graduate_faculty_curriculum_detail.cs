using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;
using SeventyOneDev.Utilities;
using SeventyOneDev.Utilities.Attributes;

namespace edu_api.Modules.Graduate.Databases.Models
{
    public class graduate_faculty_curriculum_detail : base_table
    {
        [Key]
        public Guid? graduate_faculty_curriculum_detail_uid { get; set; }
        public Guid? graduate_faculty_curriculum_uid { get; set; }
        public Guid? faculty_curriculum_uid { get; set; }
        [OrderBy(false)]
        public int? row_order { get; set; }
    }

    public class t_graduate_faculty_curriculum_detail : graduate_faculty_curriculum_detail
    {
        [JsonIgnore]
        public t_graduate_faculty_curriculum graduate_faculty_curriculum { get; set; }
        [Include]
        public List<t_graduate_student_final> graduate_student_finals { get; set; }
    }
    
    public class v_graduate_faculty_curriculum_detail : graduate_faculty_curriculum_detail
    {
        [JsonIgnore]
        public v_graduate_faculty_curriculum graduate_faculty_curriculum { get; set; }
        
        [Include]
        public List<v_graduate_student_final> graduate_student_finals { get; set; }
        
        public string faculty_curriculum_code {get;set;}
        public string faculty_curriculum_name_th {get;set;}
        public string faculty_curriculum_name_en {get;set;}
    }
}