using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using SeventyOneDev.Utilities;

namespace edu_api.Modules.Graduate.Databases.Models
{
    public class graduate_document_type : base_table
    {
        [Key]
        public Guid?  graduate_document_type_uid {get; set;}
        public string graduate_document_type_code { get; set; }
        public string graduate_document_type_name_th { get; set; }
        public string graduate_document_type_name_en { get; set; }
        public bool? is_used { get; set; }
    }

    public class t_graduate_document_type : graduate_document_type
    {

    }

    public class v_graduate_document_type : graduate_document_type
    {
        [NotMapped]
        public decimal? graduate_document_cost_amount {get;set;}
        [NotMapped]
        public string graduate_document_cost_updated_by { get; set; }
        [NotMapped]
        public DateTime? graduate_document_cost_updated_datetime { get; set; }
    }
}