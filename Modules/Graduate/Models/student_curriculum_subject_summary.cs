using System.Collections.Generic;
using edu_api.Modules.Grade.Databases.Models;

namespace edu_api.Modules.Graduate.Models
{
    public class student_curriculum_subject_summary
    {
        public student_grade student_grade { get; set; }
        public List<student_subject_category> student_subject_categories { get; set; }
    }

    public class student_subject_category
    {
        public string curriculum_subject_category_code {get;set;}
        public string curriculum_subject_category_name_th {get;set;}
        public string curriculum_subject_category_name_en {get;set;} 
        public List<student_subject_group> student_subject_groups { get; set; }
    }
    public class student_subject_group
    {
        public string curriculum_subject_group_code {get;set;}
        public string curriculum_subject_group_name_th {get;set;}
        public string curriculum_subject_group_name_en {get;set;}
        public List<student_subject> student_subjects { get; set; }
    }
    public class student_subject
    {
        public string year_subject_code {get;set;}
        public string year_subject_name_th {get;set;}
        public string year_subject_name_en {get;set;}
        public int? credit {get;set;}
        public string grade_name_th {get;set;}
        public string academic_year_code { get; set; }
        public string academic_semester_code { get; set; }


    }
    
}