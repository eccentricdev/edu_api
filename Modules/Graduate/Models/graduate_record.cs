using System;

namespace edu_api.Modules.Graduate.Databases.Models
{
    public class graduate_record
    {
        public Guid? graduate_faculty_curriculum_uid { get; set; }
        public Guid? graduate_academic_year_uid { get; set; }
        public string graduate_academic_semester_name_th { get; set; }
        public int? round { get; set; }
        public bool? is_sort { get; set; }
    }
}