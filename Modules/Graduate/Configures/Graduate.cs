using edu_api.Modules.Graduate.Services;
using Microsoft.Extensions.DependencyInjection;

namespace edu_api.Modules.Graduate.Configures
{
    public static class GraduateCollectionExtension
    {
        public static IServiceCollection AddGraduateServices(this IServiceCollection services)
        {
            services.AddScoped<GraduateDocumentTypeService>();
            services.AddScoped<GraduateDocumentCostService>();
            services.AddScoped<GraduateRequestService>();
            services.AddScoped<GraduateFacultyCurriculumService>();
            services.AddScoped<GraduateStudentFinalService>();
      
            return services;
        }
    }
}