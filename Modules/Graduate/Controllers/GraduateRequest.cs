using System.Collections.Generic;
using System;
using System.Security.Claims;
using System.Threading.Tasks;
using edu_api.Modules.Graduate.Databases.Models;
using edu_api.Modules.Graduate.Services;
using edu_api.Modules.Payment.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using payment_core.Models;
using SeventyOneDev.Core.Document;
using SeventyOneDev.Core.Document.Services;
using SeventyOneDev.Utilities;

namespace edu_api.Modules.Graduate.Controllers
{
    [ApiController]
    [Route("api/graduate/graduate_request", Name = "graduate_request")]
    public class GraduateRequestController : BaseUidController<t_graduate_request, v_graduate_request>
    {
        private readonly Db _db;
        private readonly PaymentService _paymentService;
        private readonly DocumentNoService _documentNoService;
        private readonly GraduateRequestService _entityUidService;

        public GraduateRequestController(GraduateRequestService entityUidService,
            
            PaymentService paymentService, Db db, DocumentNoService documentNoService) : base(entityUidService)
        {
            _db = db;
            _paymentService = paymentService;
            _documentNoService = documentNoService;
            _entityUidService = entityUidService;
        }


        [HttpPost("new")]
        public async Task<ActionResult<t_graduate_request>> NewEntityUid()
        {
            var claimsIdentity = User.Identity as ClaimsIdentity;
            var userUidString = claimsIdentity.FindFirst(c => c.Type == "user_uid").Value;
            var userUid = Guid.Parse(userUidString);
            var graduateRequest = new t_graduate_request()
            {
                student_uid = userUid,
                request_date = DateTime.Today,
                is_approve = false,
                is_check = false,
                step = 2
            };
            
            var documentNoData = new document_no_data()
            {
                year_code = DateTime.Now.Year.ToString()
            };
            var code = await _documentNoService.GetDocumentNo(null, "GRADUATE_REQ", documentNoData);
            graduateRequest.graduate_request_code = code.Substring(4);

            await _db.t_graduate_request.AddAsync(graduateRequest);
            await _db.SaveChangesAsync(claimsIdentity);
            return Ok();
            // return await base.NewEntityUid(graduateRequest);
        }

        [HttpPost("bill/{graduate_request_uid}")]
        public async Task<ActionResult<t_graduate_request>> NewBill([FromRoute] Guid graduate_request_uid)
        {
            try
            {
                var studentGraduate = await _db.t_graduate_request.AsNoTracking()
                    .FirstOrDefaultAsync(w => w.graduate_request_uid == graduate_request_uid);
                if (studentGraduate != null)
                {

                    var graduateRequestPayment = await _db.v_graduate_request_payment.AsNoTracking()
                        .FirstOrDefaultAsync(w => w.graduate_request_uid == graduate_request_uid);
                    //bill_uid,bill_no
                    var result = await _paymentService.CreateBill(studentGraduate.student_uid ?? Guid.Empty,
                        graduateRequestPayment.total_amout ?? 0, new List<bill_request_item>()
                        {
                            new bill_request_item()
                            {
                                item_code = studentGraduate.graduate_request_code,
                                unit_name = "รายการ",
                                item_name_th = "เอกสารขอสำเร็จการศึกษา",
                                // item_name_en = requestCost.request_type_name_en,
                                unit_price = graduateRequestPayment.total_amout,
                                quantity = 1,
                                amount = graduateRequestPayment.total_amout,
                            }
                        });


                    if (result != null)
                    {
                        studentGraduate.bill_uid = result.bill_uid;
                        studentGraduate.bill_no = result.bill_no;

                        _db.t_graduate_request.Update(studentGraduate);
                        await _db.SaveChangesAsync();
                    }
                }

                return Ok(studentGraduate);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return BadRequest(ErrorMessage.Get(ex.Message));
            }
        }
    }
}