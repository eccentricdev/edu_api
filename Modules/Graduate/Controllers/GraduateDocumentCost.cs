using edu_api.Modules.Graduate.Databases.Models;
using edu_api.Modules.Graduate.Services;
using Microsoft.AspNetCore.Mvc;
using SeventyOneDev.Utilities;

namespace edu_api.Modules.Graduate.Controllers
{
    [Route("api/graduate/graduate_document_cost", Name = "graduate_document_cost")]
    public class GraduateDocumentCostController:BaseUidController<t_graduate_document_cost,v_graduate_document_cost>
    {
        private readonly GraduateDocumentCostService _entityUidService;
        public GraduateDocumentCostController(GraduateDocumentCostService entityUidService) : base(entityUidService)
        {
            _entityUidService = entityUidService;
        }
    }
}