using edu_api.Modules.Graduate.Databases.Models;
using edu_api.Modules.Graduate.Services;
using Microsoft.AspNetCore.Mvc;
using SeventyOneDev.Utilities;

namespace edu_api.Modules.Graduate.Controllers
{
    [ApiController]
    [Route("api/graduate/graduate_student_final", Name = "graduate_student_final")]
    public class GraduateStudentFinalController :BaseUidController<t_graduate_student_final,v_graduate_student_final>
    {
    private readonly GraduateStudentFinalService _entityUidService;
    public GraduateStudentFinalController(GraduateStudentFinalService entityUidService) : base(entityUidService)
    {
        _entityUidService = entityUidService;
    }
    }
}