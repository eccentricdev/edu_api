using System;
using System.Linq;
using System.Threading.Tasks;
using edu_api.Modules.Grade.Databases.Models;
using edu_api.Modules.Grade.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SeventyOneDev.Utilities;

namespace edu_api.Modules.Graduate.Controllers
{
  [ApiController]
  [Route("api/graduate/graduate_check", Name = "graduate_check")]
  public class GradudateCheckController:ControllerBase
  {
    private readonly Db _db;
    public GradudateCheckController(Db db)
    {
      _db = db;
    }

    [HttpPut("request_year/{academic_year_code:guid}")]
    public async Task<ActionResult> CalcuateByRequestYear()
    {
      return Ok();
    }
    [HttpPut("request_date/{request_date:datetime}")]
    public async Task<ActionResult> CalcuateByRegisterYear()
    {
      return Ok();
    }

    [HttpPut("student/{student_code}")]
    public async Task<ActionResult> CalcuateByStudent()
    {
      return Ok();
    }
    [HttpPut("faculty_curriculum/{college_faculty_uid}/{faculty_curriculum_uid}")]
    public async Task<ActionResult> CalcuateByFacultyCurriculum()
    {
      return Ok();
    }

  }
}
