using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using edu_api.Modules.Graduate.Databases.Models;
using edu_api.Modules.Graduate.Services;
using edu_api.Modules.Payment.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using payment_core.Models;
using SeventyOneDev.Core.Document;
using SeventyOneDev.Core.Document.Services;
using SeventyOneDev.Utilities;

namespace edu_api.Modules.Graduate.Controllers
{
    [ApiController]
    [Route("api/graduate/graduate_register", Name = "graduate_register")]
    public class GraduateRegisterController : BaseUidController<t_graduate_register, v_graduate_register >
    {
        private readonly Db _db;
        private readonly PaymentService _paymentService;
        private readonly DocumentNoService _documentNoService;

        public GraduateRegisterController(EntityUidService<t_graduate_register, v_graduate_register> entityUidService,
            PaymentService paymentService, Db db, DocumentNoService documentNoService) : base(entityUidService)
        {
            _db = db;
            _paymentService = paymentService;
            _documentNoService = documentNoService;
        }

        [HttpPost("new")]
        public async Task<ActionResult<t_graduate_register>> NewEntityUid([FromBody] t_graduate_register entity)
        {
            var claimsIdentity = User.Identity as ClaimsIdentity;
            var userUidString = claimsIdentity.FindFirst(c => c.Type == "user_uid").Value;
            var userUid = Guid.Parse(userUidString);

            entity.student_uid = userUid;
            entity.request_date = DateTime.Today;
            entity.is_approve = false;
            entity.is_check = false;

            var documentNoData = new document_no_data()
            {
                year_code = DateTime.Now.Year.ToString()
            };
            var code = await _documentNoService.GetDocumentNo(null, "GRADUATE_REGIS", documentNoData);
            entity.graduate_register_code = code.Substring(4);

            await _db.t_graduate_register.AddAsync(entity);
            await _db.SaveChangesAsync(claimsIdentity);
            return Ok(entity);
        }

        [HttpPost("bill/{graduate_register_uid}")]
        public async Task<ActionResult<t_graduate_register>> NewBill([FromRoute] Guid graduate_register_uid)
        {
            try
            {
                var graduateRegister = await _db.t_graduate_register.AsNoTracking()
                    .FirstOrDefaultAsync(w => w.graduate_register_uid == graduate_register_uid);
                if (graduateRegister != null)
                {
                    var result = await _paymentService.CreateBill(graduateRegister.student_uid ?? Guid.Empty,
                        graduateRegister.total_amout ?? 0, new List<bill_request_item>()
                        {
                            new bill_request_item()
                            {
                                item_code = graduateRegister.graduate_register_code,
                                unit_name = "รายการ",
                                item_name_th = "ค่าธรรมเนียมขึ้นทะเบียนบัณทิต",
                                // item_name_en = requestCost.request_type_name_en,
                                unit_price = graduateRegister.total_amout,
                                quantity = 1,
                                amount = graduateRegister.total_amout,
                            }
                        });
                    
                    if (result != null)
                    {
                        graduateRegister.bill_uid = result.bill_uid;
                        graduateRegister.bill_no = result.bill_no;

                        _db.t_graduate_register.Update(graduateRegister);
                        await _db.SaveChangesAsync();
                    }
                }

                return Ok(graduateRegister);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return BadRequest(ErrorMessage.Get(ex.Message));
            }
        }
    }
}