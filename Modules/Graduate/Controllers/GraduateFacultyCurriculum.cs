using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using edu_api.Modules.Graduate.Databases.Models;
using edu_api.Modules.Graduate.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SeventyOneDev.Utilities;

namespace edu_api.Modules.Graduate.Controllers
{
    [ApiController]
    [Route("api/graduate/graduate_faculty_curriculum", Name = "graduate_faculty_curriculum")]
    public class GraduateFacultyCurriculumController : BaseUidController<t_graduate_faculty_curriculum, v_graduate_faculty_curriculum>
    {
        private readonly Db _db;
        private readonly GraduateFacultyCurriculumService _entityUidService;

        public GraduateFacultyCurriculumController(GraduateFacultyCurriculumService entityUidService, Db db) :
            base(entityUidService)
        {
            _db = db;
            _entityUidService = entityUidService;
        }

        [HttpGet("get")]
        public async Task<ActionResult<List<graduate_record>>> GetEntityIds(
            [FromQuery] Guid? graduate_academic_year_uid,
            [FromQuery] int? round )
        {
            var graduateFacultyCurriculums = _db.v_graduate_faculty_curriculum.AsNoTracking();

            var graduateStudent = _db.v_graduate_student.AsNoTracking();
            if (graduate_academic_year_uid != null)
            {
                graduateStudent =
                    graduateStudent.Where(w => w.graduate_academic_year_uid == graduate_academic_year_uid);
            }
            if (round != null)
            {
                graduateStudent = graduateStudent.Where(w => w.round == round);
            }
            var graduateRecord = new List<graduate_record>();
            if (graduateStudent.ToList().Count > 0)
            {
                var gsGroups = graduateStudent.GroupBy(s => new
                    {
                        s.graduate_academic_year_uid,
                        s.graduate_academic_semester_name_th,
                        s.round
                    }).OrderBy(w => w.Key.graduate_academic_year_uid)
                    .ThenBy(w => w.Key.round);

           
                foreach (var gs in gsGroups)
                {

                    var gfc = graduateFacultyCurriculums.FirstOrDefault(w =>
                        w.graduate_academic_year_uid == gs.Key.graduate_academic_year_uid && w.round == gs.Key.round);
                    var gr = new graduate_record()
                    {
                        graduate_academic_year_uid = gs.Key.graduate_academic_year_uid,
                        graduate_academic_semester_name_th = gs.Key.graduate_academic_semester_name_th,
                        round = gs.Key.round,
                        is_sort = gfc != null ? true : false,
                        graduate_faculty_curriculum_uid = gfc?.graduate_faculty_curriculum_uid ?? null
                    };
                    graduateRecord.Add(gr);
                }
            }
            return Ok(graduateRecord);
        }
    }
}