using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using edu_api.Modules.Graduate.Databases.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SeventyOneDev.Utilities;

namespace edu_api.Modules.Graduate.Controllers
{
  [ApiController]
  [Route("api/graduate/graduate_student", Name = "graduate_student")]
  public class GraduateStudentController : BaseUidController<t_graduate_student, v_graduate_student>
  {
    private readonly Db _db;

    public GraduateStudentController(EntityUidService<t_graduate_student, v_graduate_student> entityUidService, Db db) :
      base(entityUidService)
    {
      _db = db;
    }

    [HttpPost("graduate_record/save/{graduate_academic_year_uid}/{graduate_academic_semester_uid}/{graduate_date}/{generation}/{round}")]
    public async Task<ActionResult<List<t_graduate_student>>> UpdateEntityUids(
      [FromBody] List<Guid> uids,
      [FromRoute] Guid graduate_academic_year_uid,
      [FromRoute] Guid graduate_academic_semester_uid,
      [FromRoute] DateTime graduate_date,
      [FromRoute] int generation,
      [FromRoute] int round)
    {
      var graduateStudents = new List<t_graduate_student>();
      foreach (var uid in uids)
      {
        var graduateStudent = await _db.t_graduate_student.AsNoTracking()
          .FirstOrDefaultAsync(w => w.graduate_student_uid == uid);

        graduateStudent.is_record_graduate = true;
        graduateStudent.graduate_academic_year_uid = graduate_academic_year_uid;
        graduateStudent.graduate_academic_semester_uid = graduate_academic_semester_uid;
        graduateStudent.graduate_date = graduate_date.Date;
        graduateStudent.generation = generation;
        graduateStudent.round = round;

        graduateStudents.Add(graduateStudent);
      }

      return await base.UpdateEntityId(graduateStudents);
    }

    [HttpPost("graduate_record/cancel")]
    public async Task<ActionResult<List<t_graduate_student>>> UpdateEntityUid(
      [FromBody] List<Guid> uids)
    {
      var graduateStudents = new List<t_graduate_student>();
      foreach (var uid in uids)
      {
        var graduateStudent = await _db.t_graduate_student.AsNoTracking()
          .FirstOrDefaultAsync(w => w.graduate_student_uid == uid);

        graduateStudent.is_record_graduate = null;
        graduateStudent.graduate_academic_year_uid = null;
        graduateStudent.graduate_academic_semester_uid = null;
        graduateStudent.generation = null;
        graduateStudent.round = null;

        graduateStudents.Add(graduateStudent);
      }

      return await base.UpdateEntityId(graduateStudents);
    }


  }
}
