using edu_api.Modules.Graduate.Databases.Models;
using edu_api.Modules.Graduate.Services;
using Microsoft.AspNetCore.Mvc;
using SeventyOneDev.Utilities;

namespace edu_api.Modules.Graduate.Controllers
{
     [Route("api/graduate/graduate_document_type", Name = "graduate_document_type")]
    public class GraduateDocumentTypeController:BaseUidController<t_graduate_document_type,v_graduate_document_type>
    {
        private readonly GraduateDocumentTypeService _entityUidService;
        public GraduateDocumentTypeController(GraduateDocumentTypeService entityUidService) : base(entityUidService)
        {
            _entityUidService = entityUidService;
        }
    }
}