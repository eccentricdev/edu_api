using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using edu_api.Modules.Graduate.Databases.Models;
using edu_api.Modules.Graduate.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SeventyOneDev.Utilities;

namespace edu_api.Modules.Graduate.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/graduate/graduate", Name = "graduate")]
    public class GraduateController : ControllerBase
    {
        private readonly Db _db;

        public GraduateController(Db db)
        {
            _db = db;
        }

        [HttpGet("check")]
        public async Task<ActionResult> CheckCompleteRegister()
        {
            return Ok(new graduate_check_response() { result = true });
        }

        [HttpGet("summary")]
        public async Task<ActionResult> GetStudentCurriculumSubject()
        {
            var claimsIdentity = User.Identity as ClaimsIdentity;
            var userUidString = claimsIdentity.FindFirst(c => c.Type == "user_uid").Value;
            var userUid = Guid.Parse(userUidString);
            var tempCurriculumSubjects = new List<v_student_curriculum_subject>();
            var studentCurriculumSubjects = await _db.v_student_curriculum_subject.AsNoTracking()
                .Where(s => s.student_uid == userUid).ToListAsync();
            foreach (var studentCurriculumSubject in studentCurriculumSubjects)
            {
                if (!tempCurriculumSubjects.Any(s => s.year_subject_code == studentCurriculumSubject.year_subject_code))
                {
                    tempCurriculumSubjects.Add(studentCurriculumSubject); 
                }
            }

            var tempCurriculumSubjectAlls = new List<v_student_curriculum_subject_all>();
            var studentCurriculumSubjectAlls = await _db.v_student_curriculum_subject_all.AsNoTracking()
                .Where(s => s.student_uid == userUid).ToListAsync();
            foreach (var studentCurriculumSubjectAll in studentCurriculumSubjectAlls)
            {
                if (!tempCurriculumSubjectAlls.Any(s=>s.year_subject_code==studentCurriculumSubjectAll.year_subject_code))
                {
                    tempCurriculumSubjectAlls.Add(studentCurriculumSubjectAll);
                }
            }

            var electiveSubjects = tempCurriculumSubjectAlls
                .Where(a => tempCurriculumSubjects.Any(c => c.year_subject_code != a.year_subject_code)).ToList();
            tempCurriculumSubjects.AddRange(electiveSubjects.Select(e=>new v_student_curriculum_subject(e)
            {
                
                
            }));
            var studentSubjectCategories = tempCurriculumSubjects.GroupBy(s => s.curriculum_subject_category_uid)
                .Select(s =>
                {
                    var firstStudentCurriculumSubject = s.FirstOrDefault();
                    var studentSubjectCategory = new student_subject_category()
                    {
                        curriculum_subject_category_code =
                            firstStudentCurriculumSubject.curriculum_subject_category_code,
                        curriculum_subject_category_name_th =
                            firstStudentCurriculumSubject.curriculum_subject_category_name_th,
                        curriculum_subject_category_name_en =
                            firstStudentCurriculumSubject.curriculum_subject_category_name_en,
                        student_subject_groups = s.GroupBy(g => g.curriculum_subject_group_uid).Select(g =>
                        {
                            var firstStudentSubjectGroup = g.FirstOrDefault();
                            var studentSubjectGroup = new student_subject_group()
                            {
                                curriculum_subject_group_code = firstStudentSubjectGroup.curriculum_subject_group_code,
                                curriculum_subject_group_name_th =
                                    firstStudentSubjectGroup.curriculum_subject_group_name_th,
                                curriculum_subject_group_name_en =
                                    firstStudentSubjectGroup.curriculum_subject_group_name_en,
                                student_subjects = g.Select(ss => new student_subject()
                                {
                                    year_subject_code = ss.year_subject_code,
                                    year_subject_name_th = ss.year_subject_name_th,
                                    year_subject_name_en = ss.year_subject_name_en,
                                    credit = ss.credit,
                                    grade_name_th = ss.grade_name_th,
                                    academic_year_code = ss.academic_year_code,
                                    academic_semester_code = ss.academic_semester_code
                                }).ToList()
                            };
                            return studentSubjectGroup;
                        }).ToList()
                    };
                    return studentSubjectCategory;
                }).ToList();
            var studentGrade = await _db.t_student_grade.AsNoTracking()
                .FirstOrDefaultAsync(s => s.student_uid == userUid && s.academic_semester_uid == null);

            var result = new student_curriculum_subject_summary()
            {
                student_grade = studentGrade,
                student_subject_categories = studentSubjectCategories
            };

            return Ok(result);
        }

        [HttpGet("summary/{student_uid}")]
        public async Task<ActionResult> GetStudentCurriculumSubject([FromRoute]Guid student_uid)
        {
            // var claimsIdentity = User.Identity as ClaimsIdentity;
            // var userUidString = claimsIdentity.FindFirst(c => c.Type == "user_uid").Value;
            // var userUid = Guid.Parse(userUidString);
             var tempCurriculumSubjects = new List<v_student_curriculum_subject>();
            var studentCurriculumSubjects = await _db.v_student_curriculum_subject.AsNoTracking()
                .Where(s => s.student_uid == student_uid).ToListAsync();
            foreach (var studentCurriculumSubject in studentCurriculumSubjects)
            {
                if (!tempCurriculumSubjects.Any(s => s.year_subject_code == studentCurriculumSubject.year_subject_code))
                {
                    tempCurriculumSubjects.Add(studentCurriculumSubject); 
                }
            }

            var tempCurriculumSubjectAlls = new List<v_student_curriculum_subject_all>();
            var studentCurriculumSubjectAlls = await _db.v_student_curriculum_subject_all.AsNoTracking()
                .Where(s => s.student_uid == student_uid).ToListAsync();
            foreach (var studentCurriculumSubjectAll in studentCurriculumSubjectAlls)
            {
                if (!tempCurriculumSubjectAlls.Any(s=>s.year_subject_code==studentCurriculumSubjectAll.year_subject_code))
                {
                    tempCurriculumSubjectAlls.Add(studentCurriculumSubjectAll);
                }
            }

            var electiveSubjects = tempCurriculumSubjectAlls
                .Where(a => tempCurriculumSubjects.Any(c => c.year_subject_code != a.year_subject_code)).ToList();
            tempCurriculumSubjects.AddRange(electiveSubjects.Select(e=>new v_student_curriculum_subject(e)
            {
                
                
            }));
            var studentSubjectCategories = tempCurriculumSubjects.GroupBy(s => s.curriculum_subject_category_uid)
                .Select(s =>
                {
                    var firstStudentCurriculumSubject = s.FirstOrDefault();
                    var studentSubjectCategory = new student_subject_category()
                    {
                        curriculum_subject_category_code =
                            firstStudentCurriculumSubject.curriculum_subject_category_code,
                        curriculum_subject_category_name_th =
                            firstStudentCurriculumSubject.curriculum_subject_category_name_th,
                        curriculum_subject_category_name_en =
                            firstStudentCurriculumSubject.curriculum_subject_category_name_en,
                        student_subject_groups = s.GroupBy(g => g.curriculum_subject_group_uid).Select(g =>
                        {
                            var firstStudentSubjectGroup = g.FirstOrDefault();
                            var studentSubjectGroup = new student_subject_group()
                            {
                                curriculum_subject_group_code = firstStudentSubjectGroup.curriculum_subject_group_code,
                                curriculum_subject_group_name_th =
                                    firstStudentSubjectGroup.curriculum_subject_group_name_th,
                                curriculum_subject_group_name_en =
                                    firstStudentSubjectGroup.curriculum_subject_group_name_en,
                                student_subjects = g.Select(ss => new student_subject()
                                {
                                    year_subject_code = ss.year_subject_code,
                                    year_subject_name_th = ss.year_subject_name_th,
                                    year_subject_name_en = ss.year_subject_name_en,
                                    credit = ss.credit,
                                    grade_name_th = ss.grade_name_th,
                                    academic_year_code = ss.academic_year_code,
                                    academic_semester_code = ss.academic_semester_code
                                }).ToList()
                            };
                            return studentSubjectGroup;
                        }).ToList()
                    };
                    return studentSubjectCategory;
                }).ToList();
            var studentGrade = await _db.t_student_grade.AsNoTracking()
                .FirstOrDefaultAsync(s => s.student_uid == student_uid && s.academic_semester_uid == null);

            var result = new student_curriculum_subject_summary()
            {
                student_grade = studentGrade,
                student_subject_categories = studentSubjectCategories
            };

            return Ok(result);
        }



    }
}