// using System;
// using System.Threading.Tasks;
// using Microsoft.AspNetCore.Http;
// using Microsoft.AspNetCore.Mvc;
// using rsu_common_api.Modules.Profiles.Databases.Models;
// using rsu_common_api.Modules.Profiles.Models;
// using rsu_common_api.Modules.Profiles.Services;
//
// namespace rsu_common_api.Modules.Profiles.Controllers
// {
//   public class StudentCardController: Controller
//   {
//     readonly StudentCardService _studentCardService;
//     //private readonly IMapper _mapper;
//
//     public StudentCardController(StudentCardService studentCardService)
//     {
//       this._studentCardService = studentCardService;
//       //_mapper = mapper;
//     }
//
//     [HttpPost, Route("v1/profile/student_card")]
//     [ApiExplorerSettings(GroupName = "profile")]
//     [ProducesResponseType(StatusCodes.Status200OK)]
//     [ProducesResponseType(StatusCodes.Status404NotFound)]
//     public async Task<ActionResult<student_card_response>> PageStudentStatuss([FromBody]student_card_request studentCardRequest)
//     {
//       try
//       {
//         var result=_studentCardService.GetStudentCard(studentCardRequest);
//         return Ok(result);
//       }
//       catch (Exception e)
//       {
//         Console.WriteLine(e);
//         return BadRequest();
//       }
//
//     }
//
//
//
//   }
// }
