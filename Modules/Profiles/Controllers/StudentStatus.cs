// using System;
// using System.Collections.Generic;
// using System.Linq;
// using System.Security.Claims;
// using System.Threading.Tasks;
// using Microsoft.AspNetCore.Http;
// using Microsoft.AspNetCore.Mvc;
// using rsu_common_api.Modules.Profiles.Databases.Models;
// using rsu_common_api.Modules.Profiles.Models;
// using rsu_common_api.Modules.Profiles.Services;
// using SeventyOneDev.Utilities;
//
// namespace rsu_common_api.Modules.Profiles.Controllers
// {
//   public class StudentStatusController : Controller
//   {
//     readonly StudentStatusService _studentStatusService;
//     //private readonly IMapper _mapper;
//
//     public StudentStatusController(StudentStatusService incomeTypeService)
//     {
//       this._studentStatusService = incomeTypeService;
//       //_mapper = mapper;
//     }
//
//     [HttpGet, Route("v1/profile/student_statuses/{page}/{size}")]
//     [ApiExplorerSettings(GroupName = "profile")]
//     [ProducesResponseType(StatusCodes.Status200OK)]
//     [ProducesResponseType(StatusCodes.Status404NotFound)]
//     public async Task<ActionResult<PagedResult<v_student_status>>> PageStudentStatuss([FromRoute]int page=1,[FromRoute]int size=10,
//       [FromQuery] v_student_status _student_status = null)
//     {
//       if (EFExtension.IsNullOrEmpty(_student_status))
//       {
//         var listResult = await _studentStatusService.PageStudentStatus(null,page,size);
//         return Ok(listResult);
//       }
//       else
//       {
//         var getResult = await _studentStatusService.PageStudentStatus(_student_status,page,size);
//         return Ok(getResult);
//       }
//     }
//
//     [HttpGet, Route("v1/profile/student_statuses")]
//     [ApiExplorerSettings(GroupName = "profile")]
//     [ProducesResponseType(StatusCodes.Status200OK)]
//     [ProducesResponseType(StatusCodes.Status404NotFound)]
//     public async Task<ActionResult<List<v_student_status>>> GetStudentStatuss(
//       [FromQuery] v_student_status _student_status = null)
//     {
//       if (EFExtension.IsNullOrEmpty(_student_status))
//       {
//         var listResult = await _studentStatusService.ListStudentStatus(null);
//         return Ok(listResult);
//       }
//       else
//       {
//         var getResult = await _studentStatusService.ListStudentStatus(_student_status);
//         if (getResult.Count() == 0)
//         {
//           return Ok(new List<v_student_status>());
//         }
//         else
//         {
//           return getResult.ToList();
//         }
//
//       }
//
//     }
//     [HttpGet, Route("v1/profile/student_statuses/short")]
//     [ApiExplorerSettings(GroupName = "profile")]
//     [ProducesResponseType(StatusCodes.Status200OK)]
//     [ProducesResponseType(StatusCodes.Status404NotFound)]
//     public async Task<ActionResult<List<v_student_status>>> GetStudentStatussShort()
//     {
//
//         var getResult = await _studentStatusService.ListStudentStatus<student_status_short>(null);
//         return Ok(getResult);
//
//     }
//     [HttpGet, Route("v1/profile/student_statuses/long")]
//     [ApiExplorerSettings(GroupName = "profile")]
//     [ProducesResponseType(StatusCodes.Status200OK)]
//     [ProducesResponseType(StatusCodes.Status404NotFound)]
//     public async Task<ActionResult<List<v_student_status>>> GetStudentStatussLong()
//     {
//
//       var getResult = await _studentStatusService.ListStudentStatus<student_status_long>(null);
//       return Ok(getResult);
//
//     }
//
//
//     [HttpGet, Route("v1/profile/student_status/{student_status_uid}")]
//     [ApiExplorerSettings(GroupName = "profile")]
//     [ProducesResponseType(StatusCodes.Status200OK)]
//     [ProducesResponseType(StatusCodes.Status404NotFound)]
//     public async Task<ActionResult<v_student_status>> GetStudentStatus([FromRoute] Guid student_status_uid)
//     {
//
//       var getResult = await _studentStatusService.GetStudentStatus(student_status_uid);
//       if (getResult == null)
//       {
//         return NotFound();
//       }
//       else
//       {
//         return Ok(getResult);
//       }
//     }
//
//     [HttpPost, Route("v1/profile/student_status")]
//     [ApiExplorerSettings(GroupName = "profile")]
//     [ProducesResponseType(StatusCodes.Status201Created)]
//     [ProducesResponseType(StatusCodes.Status400BadRequest)]
//     public async Task<ActionResult<t_student_status>> NewStudentStatus(
//       [FromBody] t_student_status _student_status)
//     {
//       try
//       {
//         var newResult =
//           await _studentStatusService.NewStudentStatus(_student_status, User.Identity as ClaimsIdentity);
//         return Created("/v1/profile/student_status/" + newResult.student_status_uid.ToString(), newResult);
//       }
//       // catch(DuplicateException ex)
//       // {
//       //     response = ApiHelper.ApiError<v_student_status>(ex.ErrorId,ex.Message);
//       // }
//       catch (Exception ex)
//       {
//         Console.WriteLine("Create student_status failed: " + ex.Message);
//         return BadRequest();
//       }
//
//     }
//
//     [HttpPut, Route("v1/profile/student_status")]
//     [ApiExplorerSettings(GroupName = "profile")]
//     [ProducesResponseType(StatusCodes.Status204NoContent)]
//     [ProducesResponseType(StatusCodes.Status404NotFound)]
//     [ProducesResponseType(StatusCodes.Status400BadRequest)]
//     public async Task<ActionResult> UpdateStudentStatus([FromBody] t_student_status _student_status)
//     {
//       try
//       {
//         var result =
//           await _studentStatusService.UpdateStudentStatus(_student_status, User.Identity as ClaimsIdentity);
//         return NoContent();
//       }
//       // catch(DuplicateException ex)
//       // {
//       //     response = ApiHelper.ApiError<v_student_status>(ex.ErrorId,ex.Message);
//       // }
//       catch (Exception ex)
//       {
//         Console.WriteLine("Update student_status failed: " + ex.Message);
//         return BadRequest();
//       }
//
//     }
//
//     [HttpDelete, Route("v1/profile/student_status/{student_status_uid}")]
//     [ApiExplorerSettings(GroupName = "profile")]
//     [ProducesResponseType(StatusCodes.Status204NoContent)]
//     [ProducesResponseType(StatusCodes.Status404NotFound)]
//     [ProducesResponseType(StatusCodes.Status400BadRequest)]
//     public async Task<ActionResult> DeleteStudentStatus([FromRoute] Guid student_status_uid)
//     {
//       try
//       {
//         var _deleteCount =
//           await _studentStatusService.DeleteStudentStatus(student_status_uid, User.Identity as ClaimsIdentity);
//         if (_deleteCount == 1)
//         {
//           return NoContent();
//         }
//         else
//         {
//           return NotFound();
//         }
//
//       }
//       // catch(DuplicateException ex)
//       // {
//       //     response = ApiHelper.ApiError<v_student_status>(ex.ErrorId,ex.Message);
//       // }
//       catch (Exception ex)
//       {
//         Console.WriteLine("Delete student_status failed: " + ex.Message);
//         return BadRequest();
//       }
//     }
//   }
// }
