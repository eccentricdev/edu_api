using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using rsu_common_api.Modules.Profiles.Services;
using SeventyOneDev.Utilities;

namespace edu_api.Modules.Profiles.Controllers
{
  [Route("api/profile/iam", Name = "iam")]
  public class IamController:Controller
  {
    private readonly IamService _iamService;
    private readonly Db _db;
    public IamController(IamService iamService,Db db)
    {
      _iamService = iamService;
      _db = db;
    }
    [HttpGet("sync/{student_prefix}")]
    public async Task<ActionResult> SyncIamStudent([FromRoute]string student_prefix)
    {
      //var diAgency = _db.t_agency.AsNoTracking().ToDictionary(a => a.agency_uid, a => a);

      var students = await _db.t_student.AsNoTracking().Where(s=>s.student_code.StartsWith(student_prefix)).ToListAsync();
      _iamService.AddIamUsers(students);
      return Ok();
    }
    [HttpPost("sync")]
    public async Task<ActionResult> SyncIamStudent()
    {
      //var diAgency = _db.t_agency.AsNoTracking().ToDictionary(a => a.agency_uid, a => a);

      var student60s = await _db.t_student.AsNoTracking().Where(s=>s.student_code.StartsWith("60")).ToListAsync();
      // foreach (var student in students)
      // {
      //   student.agency_name = diAgency[student.agency_uid].agency_name_th;
      // }
      _iamService.AddIamUsers(student60s);
      var student61s = await _db.t_student.AsNoTracking().Where(s=>s.student_code.StartsWith("61")).ToListAsync();
      // foreach (var student in students)
      // {
      //   student.agency_name = diAgency[student.agency_uid].agency_name_th;
      // }
      _iamService.AddIamUsers(student61s);
      var student62s = await _db.t_student.AsNoTracking().Where(s=>s.student_code.StartsWith("62")).ToListAsync();
      // foreach (var student in students)
      // {
      //   student.agency_name = diAgency[student.agency_uid].agency_name_th;
      // }
      _iamService.AddIamUsers(student62s);
      var student63s = await _db.t_student.AsNoTracking().Where(s=>s.student_code.StartsWith("63")).ToListAsync();
      // foreach (var student in students)
      // {
      //   student.agency_name = diAgency[student.agency_uid].agency_name_th;
      // }
      _iamService.AddIamUsers(student63s);
      var student64s = await _db.t_student.AsNoTracking().Where(s=>s.student_code.StartsWith("64")).ToListAsync();
      // foreach (var student in students)
      // {
      //   student.agency_name = diAgency[student.agency_uid].agency_name_th;
      // }
      _iamService.AddIamUsers(student64s);
      return Ok();
    }
  }
}
