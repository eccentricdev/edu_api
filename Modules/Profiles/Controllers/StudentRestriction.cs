// using System;
// using System.Collections.Generic;
// using System.Linq;
// using System.Security.Claims;
// using System.Threading.Tasks;
// using Microsoft.AspNetCore.Http;
// using Microsoft.AspNetCore.Mvc;
// using rsu_common_api.Modules.Profiles.Databases.Models;
// using rsu_common_api.Modules.Profiles.Services;
// using SeventyOneDev.Utilities;
//
// namespace rsu_common_api.Modules.Profiles.Controllers
// {
//   public class StudentRestrictionController : Controller
//   {
//     readonly StudentRestrictionService _studentRestrictionService;
//
//     public StudentRestrictionController(StudentRestrictionService incomeTypeService)
//     {
//       this._studentRestrictionService = incomeTypeService;
//     }
//
//     [HttpGet, Route("v1/profile/student_restrictions")]
//     [ApiExplorerSettings(GroupName = "profile")]
//     [ProducesResponseType(StatusCodes.Status200OK)]
//     [ProducesResponseType(StatusCodes.Status404NotFound)]
//     public async Task<ActionResult<List<v_student_restriction>>> GetStudentRestrictions(
//       [FromQuery] v_student_restriction _student_restriction = null)
//     {
//       if (EFExtension.IsNullOrEmpty(_student_restriction))
//       {
//         var listResult = await _studentRestrictionService.ListStudentRestriction(null);
//         return Ok(listResult);
//       }
//       else
//       {
//         var getResult = await _studentRestrictionService.ListStudentRestriction(_student_restriction);
//         if (getResult.Count() == 0)
//         {
//           return Ok(new List<v_student_restriction>());
//         }
//         else
//         {
//           return getResult.ToList();
//         }
//
//       }
//
//     }
//
//     [HttpGet, Route("v1/profile/student_restriction/{student_restriction_uid}")]
//     [ApiExplorerSettings(GroupName = "profile")]
//     [ProducesResponseType(StatusCodes.Status200OK)]
//     [ProducesResponseType(StatusCodes.Status404NotFound)]
//     public async Task<ActionResult<v_student_restriction>> GetStudentRestriction([FromRoute] Guid student_restriction_uid)
//     {
//
//       var getResult = await _studentRestrictionService.GetStudentRestriction(student_restriction_uid);
//       if (getResult == null)
//       {
//         return NotFound();
//       }
//       else
//       {
//         return Ok(getResult);
//       }
//     }
//
//     [HttpPost, Route("v1/profile/student_restriction")]
//     [ApiExplorerSettings(GroupName = "profile")]
//     [ProducesResponseType(StatusCodes.Status201Created)]
//     [ProducesResponseType(StatusCodes.Status400BadRequest)]
//     public async Task<ActionResult<t_student_restriction>> NewStudentRestriction(
//       [FromBody] t_student_restriction _student_restriction)
//     {
//       try
//       {
//         var newResult =
//           await _studentRestrictionService.NewStudentRestriction(_student_restriction, User.Identity as ClaimsIdentity);
//         return Created("/v1/profile/student_restriction/" + newResult.student_restriction_uid.ToString(), newResult);
//       }
//       // catch(DuplicateException ex)
//       // {
//       //     response = ApiHelper.ApiError<v_student_restriction>(ex.ErrorId,ex.Message);
//       // }
//       catch (Exception ex)
//       {
//         Console.WriteLine("Create student_restriction failed: " + ex.Message);
//         return BadRequest();
//       }
//
//     }
//
//     [HttpPut, Route("v1/profile/student_restriction")]
//     [ApiExplorerSettings(GroupName = "profile")]
//     [ProducesResponseType(StatusCodes.Status204NoContent)]
//     [ProducesResponseType(StatusCodes.Status404NotFound)]
//     [ProducesResponseType(StatusCodes.Status400BadRequest)]
//     public async Task<ActionResult> UpdateStudentRestriction([FromBody] t_student_restriction _student_restriction)
//     {
//       try
//       {
//         var result =
//           await _studentRestrictionService.UpdateStudentRestriction(_student_restriction, User.Identity as ClaimsIdentity);
//         return NoContent();
//       }
//       // catch(DuplicateException ex)
//       // {
//       //     response = ApiHelper.ApiError<v_student_restriction>(ex.ErrorId,ex.Message);
//       // }
//       catch (Exception ex)
//       {
//         Console.WriteLine("Update student_restriction failed: " + ex.Message);
//         return BadRequest();
//       }
//
//     }
//
//     [HttpDelete, Route("v1/profile/student_restriction/{student_restriction_uid}")]
//     [ApiExplorerSettings(GroupName = "profile")]
//     [ProducesResponseType(StatusCodes.Status204NoContent)]
//     [ProducesResponseType(StatusCodes.Status404NotFound)]
//     [ProducesResponseType(StatusCodes.Status400BadRequest)]
//     public async Task<ActionResult> DeleteStudentRestriction([FromRoute] Guid student_restriction_uid)
//     {
//       try
//       {
//         var _deleteCount =
//           await _studentRestrictionService.DeleteStudentRestriction(student_restriction_uid, User.Identity as ClaimsIdentity);
//         if (_deleteCount == 1)
//         {
//           return NoContent();
//         }
//         else
//         {
//           return NotFound();
//         }
//
//       }
//       // catch(DuplicateException ex)
//       // {
//       //     response = ApiHelper.ApiError<v_student_restriction>(ex.ErrorId,ex.Message);
//       // }
//       catch (Exception ex)
//       {
//         Console.WriteLine("Delete student_restriction failed: " + ex.Message);
//         return BadRequest();
//       }
//     }
//   }
// }
