using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;
using edu_api.Modules.Register.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using SeventyOneDev.Utilities;

namespace edu_api.Modules.Profiles.Controllers
{
  [Route("api/profile/advisor", Name = "advisor")]
  public class AdvisorController:Controller
  {
    private readonly Db _db;
    private readonly IHttpClientFactory _httpClientFactory;
    private readonly open_id_setting _openIdSetting;

    public AdvisorController(Db db,IHttpClientFactory httpClientFactory,IOptions<open_id_setting> openIdSetting)
    {
      _db = db;
      _httpClientFactory = httpClientFactory;
      _openIdSetting = openIdSetting.Value;
    }
    [Authorize]
    [HttpGet("{student_code}/{instructor_code}")]
    public async Task<ActionResult> SetAdvisor([FromRoute] string student_code, [FromRoute] string instructor_code)
    {
      var claimsIdentity = User.Identity as ClaimsIdentity;
      var student = await _db.t_student.FirstOrDefaultAsync(s => s.student_code == student_code);
      if (student == null) return BadRequest(new Error(400, "NOT_FOUND_STUDENT"));
      var personnel = await _db.v_instructor.AsNoTracking()
        .FirstOrDefaultAsync(t => t.instructor_code == instructor_code);
      if (personnel==null) return BadRequest(new Error(400, "NOT_FOUND_INSTRUCTOR"));
      student.advisor_uid = personnel.personnel_uid;
      await _db.SaveChangesAsync(claimsIdentity);
      return Ok();
    }
  }
}
