using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using edu_api.Modules.Profiles.Databases.Models;
using edu_api.Modules.Register.Databases;
using edu_api.Modules.Study.Databases.Models;
using edu_api.Modules.Study.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using rsu_common_api.models;
using rsu_common_api.Modules.Profiles.Models;
using rsu_common_api.Modules.Profiles.Services;
using SeventyOneDev.Utilities;
using SeventyOneDev.Utilities.Attributes;

namespace edu_api.Modules.Profiles.Controllers
{
  [ApiController]
  [Route("api/profile/student", Name = "student")]
  public class StudentController:BaseUidController<t_student,v_student>
  {
    private readonly Db _db;
    private readonly StudentService _entityUidService;
    public StudentController(StudentService entityUidService,Db db) : base(entityUidService)
    {
      _db = db;
      _entityUidService = entityUidService;
    }
    

    [HttpGet("register_subject/{academic_year_uid}/{academic_semester_uid}/{student_uid}")]
    public async Task<ActionResult<List<student_register_subject>>> GetStudentRegisterSubject([FromRoute] Guid academic_year_uid, Guid academic_semester_uid,
      Guid student_uid)
    {
      var finalRegisterSubjects =await _db.v_final_register_subject.AsNoTracking().Where(f =>
          f.academic_year_uid == academic_year_uid &&
          f.academic_semester_uid == academic_semester_uid &&
          f.student_uid == student_uid)
        .ProjectTo<v_final_register_subject, student_register_subject>()
        .OrderBy(f => f.year_subject_code)
        .ToListAsync();
      return Ok(finalRegisterSubjects);
    }
    [Authorize]
    [HttpPut("register_subject_section")]
    public async Task<ActionResult> MoveSection([FromBody]move_section_request moveSectionRequest)
    {
      if (moveSectionRequest.final_register_subject_uids == null) return BadRequest();
      if (!moveSectionRequest.final_register_subject_uids.Any()) return BadRequest();
      if (moveSectionRequest.lecture_year_subject_section_uid == null &&
          moveSectionRequest.lab_year_subject_section_uid == null) return BadRequest();
      foreach (var finalRegisterSubjectUid in moveSectionRequest.final_register_subject_uids)
      {
        var finalRegisterSubject =
          await _db.t_final_register_subject.FirstOrDefaultAsync(f =>
            f.final_register_subject_uid == finalRegisterSubjectUid);
        if (finalRegisterSubject!=null)
        {
          finalRegisterSubject.lecture_year_subject_section_uid = moveSectionRequest.lecture_year_subject_section_uid;
          finalRegisterSubject.lab_year_subject_section_uid = moveSectionRequest.lab_year_subject_section_uid;
        }
      }

      await _db.SaveChangesAsync(User.Identity as ClaimsIdentity);

      return Ok();
    }
    // [HttpGet("external")]
    // public async Task<ActionResult<List<v_student>>> GetStudent([FromQuery] v_student student)
    // {
    //   await _entityUidService.GetEntity()
    // }
    [HttpPut("change")]
     public async Task<ActionResult<List<t_student_log>>> ChangeUpdateEntityId([FromBody]t_student entity)
     {
       try
       {
         var currentEntity = await _db.v_student.AsNoTracking().FirstOrDefaultAsync(s => s.student_uid == entity.student_uid);
         var result= await _entityUidService.UpdateEntity(entity, User.Identity as ClaimsIdentity);
           var updateEntity = await _db.v_student.AsNoTracking().FirstOrDefaultAsync(s => s.student_uid == entity.student_uid);
           var changes =GetChanges(currentEntity, updateEntity);
           var addStudentLog = await _entityUidService.NewStudentLog(changes,updateEntity.student_uid, User.Identity as ClaimsIdentity);

           return Ok(addStudentLog);
       }
       catch (Exception e)
       {
         Console.WriteLine(e);
         return BadRequest();
       }
       // var currentEntity = await _db.v_student.AsNoTracking().FirstOrDefaultAsync(s => s.student_uid == entity.student_uid);
       // var result= await _entityUidService.UpdateEntity(entity, User.Identity as ClaimsIdentity);
       // if (result == 2)
       // {
       //   var updateEntity = await _db.v_student.AsNoTracking().FirstOrDefaultAsync(s => s.student_uid == entity.student_uid);
       //   var changes =GetChanges(currentEntity, updateEntity);
       //   var addStudentLog = await _entityUidService.NewStudentLog(changes,updateEntity.student_uid, User.Identity as ClaimsIdentity);
       //
       //   return Ok(addStudentLog);
       // }
       // else
       // {
       //   return BadRequest();
       // }
     }
     private  List<ChangeTracking> GetChanges(object oldEntry, object newEntry)
     {
       List<ChangeTracking> logs = new List<ChangeTracking>();

       var oldType = oldEntry.GetType();
       var newType = newEntry.GetType();
       if(oldType != newType)
       {
         return logs; //Types don't match, cannot log changes
       }

       var oldProperties = oldType.GetProperties();
       var newProperties = newType.GetProperties();

       var dateChanged = DateTime.Now;
       var primaryKey = (Guid)oldProperties.Where(x => Attribute.IsDefined(x, typeof(System.ComponentModel.DataAnnotations.KeyAttribute))).First().GetValue(oldEntry);
       var className = oldEntry.GetType().Name;

       foreach(var oldProperty in oldProperties)
       {
         var matchingProperty = newProperties.Where(x => !Attribute.IsDefined(x, typeof(IgnoreLoggingAttribute))
                                                         && x.Name == oldProperty.Name
                                                         && x.PropertyType == oldProperty.PropertyType)
           .FirstOrDefault();
         if(matchingProperty == null)
         {
           continue;
         }

         var oldVal = oldProperty.GetValue(oldEntry);
         string oldValue = "";
         string newValue = "";
         if (oldVal != null)
         {
           oldValue = oldVal.ToString();
         }
         var newVal = matchingProperty.GetValue(newEntry);
         if (newVal != null)
         {
           newValue = newVal.ToString();
         }

         if(matchingProperty != null && oldValue != newValue)
         {
           logs.Add(new ChangeTracking()
           {
             key = primaryKey,
             date_changed = dateChanged,
             class_name = className,
             property_name = matchingProperty.Name,
             old_value = oldValue,//oldProperty.GetValue(oldEntry).ToString(),
             new_value = newValue,//matchingProperty.GetValue(newEntry).ToString()
           });
         }
       }

       return logs;
     }

     [HttpGet("student_log")]
     public async Task<ActionResult<List<v_student_log>>> StudentLogResults([FromQuery] v_student_log entity)
     {
       var studentLogResult= await _entityUidService.ListStudentLogResults(entity);
       return Ok(studentLogResult);
     }

  }
  // [Route("api/profile/student", Name = "student")]
  // public class StudentController:BaseUidController<t_student,v_student>
  // {
  //   private readonly Db _db;
  //   public StudentController(StudentService entityUidService,Db db) : base(entityUidService)
  //   {
  //     _db = db;
  //   }
  //   //[HttpPut("change")]
  //   // public async Task<ActionResult<List<ChangeLog>>> ChangeUpdateEntityId(t_student entity)
  //   // {
  //   //   var currentEntity = _db.v_student.AsNoTracking().FirstOrDefaultAsync(s => s.student_uid == entity.student_uid);
  //   //   var result= await base.UpdateEntityId(entity);
  //   //   var updateEntity = _db.v_student.AsNoTracking().FirstOrDefaultAsync(s => s.student_uid == entity.student_uid);
  //   //   var changes =SeventyOneDev.Utilities.ObjectChange.GetChanges(currentEntity, updateEntity);
  //   //   return Ok(changes);
  //   // }
  // }
}
