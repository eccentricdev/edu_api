using System;
using System.Collections.Generic;
using rsu_common_api.models;
using rsu_common_api.Modules.Profiles.Databases.Models;

namespace rsu_common_api.constants
{
  internal class StudentStatus
  {
    public static IEnumerable<t_student_status> Get()
    {
      return new[]{
        new t_student_status(Guid.Parse("10000000-0000-0000-0000-000000000001"),"1","แจ้งจบเทอม 1","แจ้งจบเทอม 1"),
        new t_student_status(Guid.Parse("10000000-0000-0000-0000-000000000002"),"2","แจ้งจบเทอม 2","แจ้งจบเทอม 2"),
        new t_student_status(Guid.Parse("10000000-0000-0000-0000-000000000003"),"3","แจ้งจบเทอม 3","แจ้งจบเทอม 3"),
        new t_student_status(Guid.Parse("10000000-0000-0000-0000-000000000004"),"A","ให้ออก","ให้ออก"),
        new t_student_status(Guid.Parse("10000000-0000-0000-0000-000000000005"),"C","ยกเลิกรหัสนักศึกษา","ยกเลิกรหัสนักศึกษา"),
        new t_student_status(Guid.Parse("10000000-0000-0000-0000-000000000006"),"D","เสียชีวิต","เสียชีวิต"),
        new t_student_status(Guid.Parse("10000000-0000-0000-0000-000000000007"),"E","วุฒิตรวจสอบไม่ผ่าน","วุฒิตรวจสอบไม่ผ่าน"),
        new t_student_status(Guid.Parse("10000000-0000-0000-0000-000000000008"),"F","ทัณฑ์บน","ทัณฑ์บน"),
        new t_student_status(Guid.Parse("10000000-0000-0000-0000-000000000009"),"G","สำเร็จการศึกษา","สำเร็จการศึกษา"),
        new t_student_status(Guid.Parse("10000000-0000-0000-0000-00000000000A"),"I","ไม่ส่งเอกสารรายงานตัว","ไม่ส่งเอกสารรายงานตัว"),
        new t_student_status(Guid.Parse("10000000-0000-0000-0000-00000000000B"),"L","รักษาสถานภาพการศึกษา","รักษาสถานภาพการศึกษา"),
        new t_student_status(Guid.Parse("10000000-0000-0000-0000-00000000000C"),"N","ปกติ","ปกติ"),
        new t_student_status(Guid.Parse("10000000-0000-0000-0000-00000000000D"),"O","สั่งพักการศึกษา","สั่งพักการศึกษา"),
        new t_student_status(Guid.Parse("10000000-0000-0000-0000-00000000000E"),"P","Probation","Probation"),
        new t_student_status(Guid.Parse("10000000-0000-0000-0000-00000000000F"),"Q","คัดชื่อออก","คัดชื่อออก"),
        new t_student_status(Guid.Parse("10000000-0000-0000-0000-000000000010"),"R","ลาออก","ลาออก"),
        new t_student_status(Guid.Parse("10000000-0000-0000-0000-000000000011"),"T","พ้นสภาพ","พ้นสภาพ"),
        new t_student_status(Guid.Parse("10000000-0000-0000-0000-000000000012"),"V","เรียนเกินหลักสูตร","เรียนเกินหลักสูตร"),
        new t_student_status(Guid.Parse("10000000-0000-0000-0000-000000000013"),"X","ออกแรกเข้า","ออกแรกเข้า"),
        new t_student_status(Guid.Parse("10000000-0000-0000-0000-000000000014"),"Z","นศ.ทุน กรุณาติดต่อสำนักงานทุน","นศ.ทุน กรุณาติดต่อสำนักงานทุน"),
      };

    }
  }
}
