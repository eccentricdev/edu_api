using System.Collections.Generic;
using rsu_common_api.models;
using rsu_common_api.Modules.Profiles.Databases.Models;

namespace rsu_common_api.constants
{
  internal class AddressType
  {
    public static IEnumerable<t_address_type> Get()
    {
      return new[]{
        new t_address_type(1,"C","Contact","ติดต่อสะดวก"),
        new t_address_type(2,"R","Registered","ทะเบียนบ้าน"),
      };
    }
  }
}
