using System.Collections.Generic;
using rsu_common_api.models;
using rsu_common_api.Modules.Profiles.Databases.Models;

namespace rsu_common_api.constants
{
  internal class ParentType
  {
    public static IEnumerable<t_parent_type> Get()
    {
      return new[]{
        new t_parent_type(1,"F","Father","พ่อ"),
        new t_parent_type(2,"M","Mother","แม่"),
        new t_parent_type(3,"P","Parent","ผู้ปกครอง")
      };
    }
  }
}
