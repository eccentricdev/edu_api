using System.Linq;
using SeventyOneDev.Utilities;


namespace rsu_common_api.constants
{
  public static class ProfileInit
  {
    public static void Run(Db context)
    {
      if (!context.t_student_status.Any())
      {
        context.AddRange(StudentStatus.Get());
        context.SaveChanges();
      }
      if (!context.t_address_type.Any())
      {
        context.AddRange(AddressType.Get());
        context.SaveChanges();
      }
      if (!context.t_parent_type.Any())
      {
        context.AddRange(ParentType.Get());
        context.SaveChanges();
      }
    }
  }
}
