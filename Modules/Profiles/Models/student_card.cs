using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace rsu_common_api.Modules.Profiles.Models
{
  public class student_card_request
  {
    [JsonPropertyName("params")]
    public List<student_card_request_param> student_params { get; set; }
  }

  public class student_card_request_param
  {
    public string uid { get; set; }
    public string personal_id { get; set; }
  }

  public class student_card_response
  {
    public bool success { get; set; }
    public List<student_card_response_result> result { get; set; }

  }

  public class student_card_response_result
  {
    public string uid { get; set; }
    public string personal_id { get; set; }
    public string first_name { get; set; }
    public string last_name { get; set; }
    public string first_name_eng { get; set; }
    public string last_name_eng { get; set; }
  }
}
