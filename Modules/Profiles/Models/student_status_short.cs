using System;

namespace rsu_common_api.Modules.Profiles.Models
{
  public class student_status_short
  {
    public Guid? student_status_uid { get; set; }
    public string student_status_code { get; set; }
    public string student_status_name_th { get; set; }
  }
  public class student_status_long
  {
    public Guid? student_status_uid { get; set; }
    public string student_status_code { get; set; }
    public string student_status_name_th { get; set; }
    public string student_status_name_en { get; set; }
  }
}
