using System;

namespace rsu_common_api.Modules.Profiles.Models
{
    public class student_register_subject
    {
        public Guid? final_register_subject_uid { get; set; }
        public Guid? year_subject_uid { get; set; }
        public string year_subject_code { get; set; }
        public string year_subject_name_th { get; set; }
        public string year_subject_name_en { get; set; }
        public Guid? lecture_year_subject_section_uid { get; set; }
        public string lecture_section_code { get; set; }
        public string lecture_study_time_text { get; set; }
        public Guid? lab_year_subject_section_uid { get; set; }
        public string lab_section_code { get; set; }
        public string lab_study_time_text { get; set; }
    }
}