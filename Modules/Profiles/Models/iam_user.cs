using System;
using Microsoft.VisualBasic;
using rsu_common_api.models;

namespace rsu_common_api.Modules.Profiles.Models
{
  public class iam_user
  {
    public Guid? user_uid { get; set; }
    public string login_name { get; set; }
    public string display_name { get; set; }
    public Guid? agency_uid { get; set; }
    public string image_url { get; set; }
    public bool? is_active { get; set; }
    public string group_name { get; set; }
    public string agency_name { get; set; }
    public string password { get; set; }
    public iam_user()
    {

    }
    public iam_user(t_student student)
    {
      user_uid = student.student_uid;
      login_name = "u" + student.student_code;
      display_name = student.display_name_th;
      agency_uid = Guid.Empty; //student.agency_uid;
      is_active = true;
      group_name = "student";
      //agency_name = student.agency_name;
      password = "123456";//Strings.Right(student.citizen_id, 4);
    }
  }
}
