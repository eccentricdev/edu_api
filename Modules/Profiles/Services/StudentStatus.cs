using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using System.Security.Claims;
using System.Threading.Tasks;
//using AutoMapper;
//using AutoMapper.QueryableExtensions;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Conventions;
using rsu_common_api.Modules.Profiles.Databases.Models;
//using rsu_common_api.Modules.Commons;
using rsu_common_api.Modules.Profiles.Models;
using SeventyOneDev.Utilities;
using SeventyOneDev.Utilities.Attributes;

namespace rsu_common_api.Modules.Profiles.Services
{

  public class StudentStatusService
  {
    readonly Db _context;

    public StudentStatusService(Db context)
    {
      _context = context;
    }
    public async Task<PagedResult<v_student_status>> PageStudentStatus(v_student_status student_status,int page,int size)
    {
      IQueryable < v_student_status > student_statuss = _context.v_student_status.AsNoTracking();


      if (student_status != null)
      {
        student_statuss = student_status.search != null ? student_statuss.Search(student_status.search) : student_statuss.Search(student_status);
      }
      student_statuss = student_statuss.OrderBy(c => c.created_by);
      var paged = await student_statuss.GetPaged<v_student_status>(page, size);

      return paged;
    }

    public async Task<List<v_student_status>> ListStudentStatus(v_student_status student_status)
    {

      IQueryable < v_student_status > student_statuss = _context.v_student_status.AsNoTracking();


      if (student_status != null)
      {
        student_statuss = student_status.search != null ? student_statuss.Search(student_status.search) : student_statuss.Search(student_status);
      }
      student_statuss = student_statuss.OrderBy(c => c.created_by);
      return await student_statuss.ToListAsync();
    }
    public async Task<List<T>> ListStudentStatus<T>(v_student_status student_status)
    {

      IQueryable < v_student_status > student_statuss = _context.v_student_status.AsNoTracking();
      var y = _context.v_student_status.AsNoTracking().Select(x => new student_status_short());
      if (student_status != null)
      {
        student_statuss = student_status.search != null ? student_statuss.Search(student_status.search) : student_statuss.Search(student_status);
      }
      var x = student_statuss.OrderBy(c => c.created_by).ProjectTo<v_student_status,T>();
      var result = await x.ToListAsync();
      return result;
    }

    public async Task<v_student_status> GetStudentStatus(Guid uid)
    {
      return await _context.v_student_status.AsNoTracking()
        .LoadRelated()
        .FirstOrDefaultAsync(a => a.student_status_uid == uid);
    }

    public async Task<t_student_status> NewStudentStatus(t_student_status entity, ClaimsIdentity claimsIdentity)
    {
      entity.student_status_uid = Guid.NewGuid();
      foreach (var prop in typeof(t_student_status).GetProperties()
        .Where(p => Attribute.IsDefined(p, typeof(IncludeAttribute))))
      {
        var propertyType = prop.PropertyType;
        if (prop.PropertyType.GetGenericTypeDefinition() == typeof(List<>))
        {
          var includeType = propertyType.GetGenericArguments()[0];
          var keyProperty = includeType.GetProperties().FirstOrDefault(x => Attribute.IsDefined(x, typeof(KeyAttribute)));
          if (keyProperty.GetType() == typeof(Guid?))
          {
            foreach(var item in (IEnumerable)prop.GetValue(entity, null))
            {
              keyProperty.SetValue(item,Guid.NewGuid());
            }
          }
        }
      }
      await _context.t_student_status.AddAsync(entity);
      await _context.SaveChangesAsync(claimsIdentity);
      return entity;
    }

    public async Task<int> UpdateStudentStatus(t_student_status entity, ClaimsIdentity claimsIdentity)
    {
      // var currentData=await _context.t_student_status.AsNoTracking()
      //   .LoadRelated()
      //   .FirstOrDefaultAsync(a => a.student_status_uid == entity.student_status_uid);
      var currentData = await GetStudentStatus(entity.student_status_uid.Value);
      foreach (var prop in typeof(t_student_status).GetProperties()
        .Where(p => Attribute.IsDefined(p, typeof(IncludeAttribute))))
      {
        var propertyType = prop.PropertyType;
        if (prop.PropertyType.GetGenericTypeDefinition() == typeof(List<>))
        {
          var includeType = propertyType.GetGenericArguments()[0];
          var keyProperty = includeType.GetProperties().FirstOrDefault(x => Attribute.IsDefined(x, typeof(KeyAttribute)));
          if (keyProperty.GetType() == typeof(Int32?))
          {
            var includeUpdates = new List<Int32?>();
            foreach(var item in (IEnumerable)prop.GetValue(entity, null))
            {
             Type ty = item.GetType();
              var pp = ty.GetProperties();
              includeUpdates.Add((Int32?)keyProperty.GetValue(item));
            }
            foreach(var item in (IEnumerable)prop.GetValue(currentData, null))
            {
              var key = (Int32?) keyProperty.GetValue(item);
              if (!includeUpdates.Contains(key))
              {
                _context.Remove(item);
                _context.SaveChanges();
              }

            }
          }

          else if (keyProperty.GetType() == typeof(Guid?))
          {
            var includeUpdates = new List<Guid?>();

            foreach(var item in (IEnumerable)prop.GetValue(entity, null))
            {
              //Type ty = item.GetType();
              //ll2.Add(item);
              Type ty = item.GetType();
              var pp = ty.GetProperties();
              includeUpdates.Add((Guid?)keyProperty.GetValue(item));
            }
            foreach(var item in (IEnumerable)prop.GetValue(currentData, null))
            {
              var key = (Guid?) keyProperty.GetValue(item);
              if (!includeUpdates.Contains(key))
              {
                _context.Remove(item);
                _context.SaveChanges();
              }
            }
          }
        }
      }
      _context.t_student_status.Update(entity);
      return await _context.SaveChangesAsync(claimsIdentity);
    }

    // private void RemoveChild(object entity )
    // {
    //   foreach (var prop in typeof(t_student_status).GetProperties()
    //     .Where(p => Attribute.IsDefined(p, typeof(IncludeAttribute))))
    //   {
    //     var propertyType = prop.PropertyType;
    //     if (prop.PropertyType.GetGenericTypeDefinition() == typeof(List<>))
    //     {
    //       var includeType = propertyType.GetGenericArguments()[0];
    //       var keyProperty = includeType.GetProperties().FirstOrDefault(x => x.Name == "student_restriction_uid"); //pp.FirstOrDefault(x => Attribute.IsDefined(x, typeof(KeyAttribute)));
    //       if (keyProperty.GetType() == typeof(Int32?))
    //       {
    //         var includeUpdates = new List<Int32?>();
    //         foreach(var item in (IEnumerable)prop.GetValue(student_status, null))
    //         {
    //          Type ty = item.GetType();
    //           var pp = ty.GetProperties();
    //           includeUpdates.Add((Int32?)keyProperty.GetValue(item));
    //         }
    //         foreach(var item in (IEnumerable)prop.GetValue(currentData, null))
    //         {
    //           var key = (Int32?) keyProperty.GetValue(item);
    //           if (!includeUpdates.Contains(key))
    //           {
    //             _context.Remove(item);
    //             _context.SaveChanges();
    //           }
    //
    //         }
    //       }
    //
    //       else if (keyProperty.GetType() == typeof(Guid?))
    //       {
    //         var includeUpdates = new List<Guid?>();
    //
    //         foreach(var item in (IEnumerable)prop.GetValue(student_status, null))
    //         {
    //           //Type ty = item.GetType();
    //           //ll2.Add(item);
    //           Type ty = item.GetType();
    //           var pp = ty.GetProperties();
    //           includeUpdates.Add((Guid?)keyProperty.GetValue(item));
    //         }
    //         foreach(var item in (IEnumerable)prop.GetValue(currentData, null))
    //         {
    //           var key = (Guid?) keyProperty.GetValue(item);
    //           if (!includeUpdates.Contains(key))
    //           {
    //             _context.Remove(item);
    //             _context.SaveChanges();
    //           }
    //         }
    //       }
    //     }
    //   }
    // }

    public async Task<int> DeleteStudentStatus(Guid uid, ClaimsIdentity claimsIdentity)
    {
      var student_status = await _context.t_student_status.FirstOrDefaultAsync(c => c.student_status_uid == uid);
      _context.Remove(student_status);
      return await _context.SaveChangesAsync(claimsIdentity);

    }
  }


}
