using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;
using Hangfire;
using rsu_common_api.models;
using rsu_common_api.Modules.Profiles.Models;
using SeventyOneDev.Utilities;

namespace rsu_common_api.Modules.Profiles.Services
{
  public class IamService
  {
    private readonly IHttpClientFactory _httpClientFactory;
    public IamService(IHttpClientFactory httpClientFactory)
    {
      _httpClientFactory = httpClientFactory;
    }

    // public void AddIamUser(t_student student)
    // {
    //
    // }
    public void AddIamUsers(List<t_student> students)
    {
      var iamUsers = students.Select(student => new iam_user(student)).ToList();
      var iamUserBatches=Helper.SplitList<iam_user>(iamUsers,500);
      foreach(var iamUserBatch in iamUserBatches) {
        BackgroundJob.Enqueue(() => AddOrUpdateIamUsers(iamUserBatch));
      }

    }
    public async Task<bool> AddOrUpdateIamUser(t_student student)
    {

      var httpClient = _httpClientFactory.CreateClient("iam_api");
      var iamUser = new iam_user(student);
      //Console.WriteLine(JsonSerializer.Serialize(iamUser));
      //var url = setting.tb320_url + "/api/payment_item_demo_bank";
      try
      {
        var content = new ByteArrayContent(JsonSerializer.SerializeToUtf8Bytes(iamUser));
        content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");
        var res = await httpClient.PostAsync("openid/user", content);
        if (res.StatusCode != HttpStatusCode.NoContent)
        {
          Console.WriteLine("Error status code:" + res.StatusCode.ToString());
          return false;
        }

        return true;
      }
      catch (Exception ex)
      {
        Console.WriteLine(ex);
      }
      return false;
      


    }
    public void AddOrUpdateIamUsers(List<iam_user> iamUsers)
    {

      var httpClient = _httpClientFactory.CreateClient("iam_api");
      try
      {
        var content = new ByteArrayContent(JsonSerializer.SerializeToUtf8Bytes(iamUsers));
        content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");
        var res = httpClient.PostAsync("openid/user/batch", content).Result;
        if (res.StatusCode != HttpStatusCode.NoContent)
        {
          Console.WriteLine("Error status code:" + res.StatusCode.ToString());
        }

      }
      catch (Exception ex)
      {
        Console.WriteLine(ex);
      }


    }
  }
}
