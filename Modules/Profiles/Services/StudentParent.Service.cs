// using System.Collections.Generic;
// using System.Linq;
// using System.Security.Claims;
// using System.Threading.Tasks;
// using Microsoft.EntityFrameworkCore;
//
// using rsu_common_api.models;
//
// using SeventyOneDev.Utilities;
//
// namespace rsu_common_api.services.fees
// {
//   public class StudentParentService
//   {
//     readonly Db _context;
//         public StudentParentService(Db context)
//         {
//             this._context = context;
//         }
//         public async Task<List<v_student_parent>> ListStudentParent(v_student_parent _student_parent)
//         {
//
//             IQueryable<v_student_parent> _student_parents=_context.v_student_parent.AsNoTracking();
//
//             if (_student_parent!=null){
//               if (_student_parent.search != null)
//               {
//                 _student_parents = _student_parents.Search(_student_parent.search);
//               }
//               else
//               {
//                 _student_parents = _student_parents.Search(_student_parent);
//               }
//             }
//             _student_parents=_student_parents.OrderBy(c=>c.student_parent_id);
//             return await _student_parents.ToListAsync();
//         }
//         public async Task<v_student_parent> GetStudentParent(int id)
//         {
//             return await _context.v_student_parent.FirstOrDefaultAsync(a=>a.student_parent_id==id);
//             // documentTypes=documentTypes.FirstOrDefault().OrderBy(c=>c._student_parent_id);
//             // return documentTypes;
//         }
//         public async Task<student_parent> NewStudentParent(student_parent _student_parent,ClaimsIdentity claimsIdentity)
//         {
//             // if (await _context.student_parent.AnyAsync(c=>
//             //     c.student_parent_code==_student_parent.student_parent_code ||
//             //     c.student_parent_name_th==_student_parent.student_parent_name_th ||
//             //     c.student_parent_name_en==_student_parent.student_parent_name_en))
//             // {
//             //     throw new Exception("DATA_EXISTS");
//             // }
//             await _context.student_parent.AddAsync(_student_parent);
//             await _context.SaveChangesAsync();
//             return _student_parent;
//         }
//
//         public async Task<int> UpdateStudentParent(student_parent _student_parent,ClaimsIdentity claimsIdentity)
//         {
//             // if (await _context.student_parent.AnyAsync(c=>c.student_parent_id!=_student_parent.student_parent_id && (
//             //     c.student_parent_code==_student_parent.student_parent_code ||
//             //     c.student_parent_name_th==_student_parent.student_parent_name_th ||
//             //     c.student_parent_name_en==_student_parent.student_parent_name_en)))
//             // {
//             //     throw new Exception("DATA_EXISTS");
//             // }
//             _context.student_parent.Update(_student_parent);
//             return await _context.SaveChangesAsync();
//         }
//
//         public async Task<int> DeleteStudentParent(int id,ClaimsIdentity claimsIdentity)
//         {
//             var _student_parent = await _context.student_parent.FirstOrDefaultAsync(c=>c.student_parent_id==id);
//             _context.Remove(_student_parent);
//             return await _context.SaveChangesAsync();
//
//         }
//   }
// }
