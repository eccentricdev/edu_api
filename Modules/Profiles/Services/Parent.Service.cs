// using System.Collections.Generic;
// using System.Linq;
// using System.Security.Claims;
// using System.Threading.Tasks;
// using Microsoft.EntityFrameworkCore;
//
// using rsu_common_api.models;
//
// using SeventyOneDev.Utilities;
//
// namespace rsu_common_api.services.fees
// {
//   public class ParentService
//   {
//     readonly Db _context;
//         public ParentService(Db context)
//         {
//             this._context = context;
//         }
//         public async Task<List<v_parent>> ListParent(v_parent _parent)
//         {
//
//             IQueryable<v_parent> _parents=_context.v_parent.AsNoTracking();
//
//             if (_parent!=null){
//               if (_parent.search != null)
//               {
//                 _parents = _parents.Search(_parent.search);
//               }
//               else
//               {
//                 _parents = _parents.Search(_parent);
//               }
//             }
//             _parents=_parents.OrderBy(c=>c.parent_id);
//             return await _parents.ToListAsync();
//         }
//         public async Task<v_parent> GetParent(int id)
//         {
//             return await _context.v_parent.FirstOrDefaultAsync(a=>a.parent_id==id);
//             // documentTypes=documentTypes.FirstOrDefault().OrderBy(c=>c._parent_id);
//             // return documentTypes;
//         }
//         public async Task<parent> NewParent(parent _parent,ClaimsIdentity claimsIdentity)
//         {
//             // if (await _context.parent.AnyAsync(c=>
//             //     c.parent_code==_parent.parent_code ||
//             //     c.parent_name_th==_parent.parent_name_th ||
//             //     c.parent_name_en==_parent.parent_name_en))
//             // {
//             //     throw new Exception("DATA_EXISTS");
//             // }
//             await _context.parent.AddAsync(_parent);
//             await _context.SaveChangesAsync();
//             return _parent;
//         }
//
//         public async Task<int> UpdateParent(parent _parent,ClaimsIdentity claimsIdentity)
//         {
//             // if (await _context.parent.AnyAsync(c=>c.parent_id!=_parent.parent_id && (
//             //     c.parent_code==_parent.parent_code ||
//             //     c.parent_name_th==_parent.parent_name_th ||
//             //     c.parent_name_en==_parent.parent_name_en)))
//             // {
//             //     throw new Exception("DATA_EXISTS");
//             // }
//             _context.parent.Update(_parent);
//             return await _context.SaveChangesAsync();
//         }
//
//         public async Task<int> DeleteParent(int id,ClaimsIdentity claimsIdentity)
//         {
//             var _parent = await _context.parent.FirstOrDefaultAsync(c=>c.parent_id==id);
//             _context.Remove(_parent);
//             return await _context.SaveChangesAsync();
//
//         }
//   }
// }
