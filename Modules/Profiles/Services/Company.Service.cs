// using System.Collections.Generic;
// using System.Linq;
// using System.Security.Claims;
// using System.Threading.Tasks;
// using Microsoft.EntityFrameworkCore;
//
// using rsu_common_api.models;
//
// using SeventyOneDev.Utilities;
//
// namespace rsu_common_api.services.fees
// {
//   public class CompanyService
//   {
//     readonly Db _context;
//         public CompanyService(Db context)
//         {
//             this._context = context;
//         }
//         public async Task<List<v_company>> ListCompany(v_company _company)
//         {
//
//             IQueryable<v_company> _companys=_context.v_company.AsNoTracking();
//
//             if (_company!=null){
//               if (_company.search != null)
//               {
//                 _companys = _companys.Search(_company.search);
//               }
//               else
//               {
//                 _companys = _companys.Search(_company);
//               }
//             }
//             _companys=_companys.OrderBy(c=>c.company_id);
//             return await _companys.ToListAsync();
//         }
//         public async Task<v_company> GetCompany(int id)
//         {
//             return await _context.v_company.FirstOrDefaultAsync(a=>a.company_id==id);
//             // documentTypes=documentTypes.FirstOrDefault().OrderBy(c=>c._company_id);
//             // return documentTypes;
//         }
//         public async Task<company> NewCompany(company _company,ClaimsIdentity claimsIdentity)
//         {
//             // if (await _context.company.AnyAsync(c=>
//             //     c.company_code==_company.company_code ||
//             //     c.company_name_th==_company.company_name_th ||
//             //     c.company_name_en==_company.company_name_en))
//             // {
//             //     throw new Exception("DATA_EXISTS");
//             // }
//             await _context.company.AddAsync(_company);
//             await _context.SaveChangesAsync();
//             return _company;
//         }
//
//         public async Task<int> UpdateCompany(company _company,ClaimsIdentity claimsIdentity)
//         {
//             // if (await _context.company.AnyAsync(c=>c.company_id!=_company.company_id && (
//             //     c.company_code==_company.company_code ||
//             //     c.company_name_th==_company.company_name_th ||
//             //     c.company_name_en==_company.company_name_en)))
//             // {
//             //     throw new Exception("DATA_EXISTS");
//             // }
//             _context.company.Update(_company);
//             return await _context.SaveChangesAsync();
//         }
//
//         public async Task<int> DeleteCompany(int id,ClaimsIdentity claimsIdentity)
//         {
//             var _company = await _context.company.FirstOrDefaultAsync(c=>c.company_id==id);
//             _context.Remove(_company);
//             return await _context.SaveChangesAsync();
//
//         }
//   }
// }
