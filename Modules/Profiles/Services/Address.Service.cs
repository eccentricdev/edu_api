// using System.Collections.Generic;
// using System.Linq;
// using System.Security.Claims;
// using System.Threading.Tasks;
// using Microsoft.EntityFrameworkCore;
//
// using rsu_common_api.models;
//
// using SeventyOneDev.Utilities;
//
// namespace rsu_common_api.services.fees
// {
//   public class AddressService
//   {
//     readonly Db _context;
//         public AddressService(Db context)
//         {
//             this._context = context;
//         }
//         public async Task<List<v_address>> ListAddress(v_address _address)
//         {
//
//             IQueryable<v_address> _addresss=_context.v_address.AsNoTracking();
//
//             if (_address!=null){
//               if (_address.search != null)
//               {
//                 _addresss = _addresss.Search(_address.search);
//               }
//               else
//               {
//                 _addresss = _addresss.Search(_address);
//               }
//             }
//             _addresss=_addresss.OrderBy(c=>c.address_id);
//             return await _addresss.ToListAsync();
//         }
//         public async Task<v_address> GetAddress(int id)
//         {
//             return await _context.v_address.FirstOrDefaultAsync(a=>a.address_id==id);
//             // documentTypes=documentTypes.FirstOrDefault().OrderBy(c=>c._address_id);
//             // return documentTypes;
//         }
//         public async Task<address> NewAddress(address _address,ClaimsIdentity claimsIdentity)
//         {
//             // if (await _context.address.AnyAsync(c=>
//             //     c.address_code==_address.address_code ||
//             //     c.address_name_th==_address.address_name_th ||
//             //     c.address_name_en==_address.address_name_en))
//             // {
//             //     throw new Exception("DATA_EXISTS");
//             // }
//             await _context.address.AddAsync(_address);
//             await _context.SaveChangesAsync();
//             return _address;
//         }
//
//         public async Task<int> UpdateAddress(address _address,ClaimsIdentity claimsIdentity)
//         {
//             // if (await _context.address.AnyAsync(c=>c.address_id!=_address.address_id && (
//             //     c.address_code==_address.address_code ||
//             //     c.address_name_th==_address.address_name_th ||
//             //     c.address_name_en==_address.address_name_en)))
//             // {
//             //     throw new Exception("DATA_EXISTS");
//             // }
//             _context.address.Update(_address);
//             return await _context.SaveChangesAsync();
//         }
//
//         public async Task<int> DeleteAddress(int id,ClaimsIdentity claimsIdentity)
//         {
//             var _address = await _context.address.FirstOrDefaultAsync(c=>c.address_id==id);
//             _context.Remove(_address);
//             return await _context.SaveChangesAsync();
//
//         }
//   }
// }
