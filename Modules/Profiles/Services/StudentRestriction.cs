using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

using rsu_common_api.Modules.Profiles.Databases.Models;
using SeventyOneDev.Utilities;

namespace rsu_common_api.Modules.Profiles.Services
{

  public class StudentRestrictionService
  {
    readonly Db _context;

    public StudentRestrictionService(Db context)
    {
      _context = context;
    }

    public async Task<List<v_student_restriction>> ListStudentRestriction(v_student_restriction student_restriction)
    {

      IQueryable < v_student_restriction > student_restrictions = _context.v_student_restriction.AsNoTracking();


      if (student_restriction != null)
      {
        student_restrictions = student_restriction.search != null ? student_restrictions.Search(student_restriction.search) : student_restrictions.Search(student_restriction);
      }
      student_restrictions = student_restrictions.OrderBy(c => c.created_by);
      return await student_restrictions.ToListAsync();
    }

    public async Task<v_student_restriction> GetStudentRestriction(Guid uid)
    {
      return await _context.v_student_restriction.AsNoTracking().FirstOrDefaultAsync(a => a.student_restriction_uid == uid);
    }

    public async Task<t_student_restriction> NewStudentRestriction(t_student_restriction student_restriction, ClaimsIdentity claimsIdentity)
    {
      student_restriction.student_restriction_uid = Guid.NewGuid();
      await _context.t_student_restriction.AddAsync(student_restriction);
      await _context.SaveChangesAsync(claimsIdentity);
      return student_restriction;
    }

    public async Task<int> UpdateStudentRestriction(t_student_restriction student_restriction, ClaimsIdentity claimsIdentity)
    {

      _context.t_student_restriction.Update(student_restriction);
      return await _context.SaveChangesAsync(claimsIdentity);
    }

    public async Task<int> DeleteStudentRestriction(Guid uid, ClaimsIdentity claimsIdentity)
    {
      var student_restriction = await _context.t_student_restriction.FirstOrDefaultAsync(c => c.student_restriction_uid == uid);
      _context.Remove(student_restriction);
      return await _context.SaveChangesAsync(claimsIdentity);

    }
  }


}
