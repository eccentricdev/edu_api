// using System.Runtime.Serialization;
// using System.Linq;
// using System.Threading.Tasks;
//
// using rsu_common_api.models;
// using Microsoft.EntityFrameworkCore;
// using System;
// using System.Security.Claims;
// using System.Collections.Generic;
//
// using System.Net.Http;
// using System.Text.Json;
// using System.Net;
// using SeventyOneDev.Utilities;
//
// namespace rsu_common_api.services
// {
//     public class StudentService
//     {
//         readonly Db _context;
//         readonly IHttpClientFactory _httpClientFactory;
//         readonly application_setting _setting;
//         public StudentService(Db context,IHttpClientFactory httpClientFactory,application_setting setting)
//         {
//             this._context = context;
//             this._httpClientFactory = httpClientFactory;
//             this._setting = setting;
//         }
//         public async Task<student_profile_mobile> GetStudentProfileModile(string studentCode)
//         {
//             var studentProfile =  await _context.student
//                 .Join(_context.education_type,s=>s.education_type_id,et=>et.education_type_id,(s,et)=>new {s,et})
//                 .Join(_context.faculty, j=>j.s.faculty_id, f=>f.faculty_id, (j,f)=>new {j.s,j.et,f})
//                 .Join(_context.major, jj=>jj.s.major_id,m=>m.major_id,(jj,m)=>new {jj.s,jj.et,jj.f,m})
//                 .Where(s=>s.s.student_code==studentCode)
//                 .Select(r=>new student_profile_mobile(r.s.student_code,r.s.display_name_th,r.s.display_name_en,
//                     r.f.faculty_name_th,r.f.faculty_name_en,r.m.major_name_th,r.m.major_name_en,1,r.s.total_credit,r.s.cumulative_gpa,"นาย ไม่มี ข้อมูล","Mr No Data","01234567890","unknown@gmail.com",
//                     r.et.education_type_name_th,r.et.education_type_name_en
//                 ))
//                 .FirstOrDefaultAsync();
//             var studentImage = await _context.v_student_image.FirstOrDefaultAsync(s=>s.student_id==studentCode);
//             if (studentImage != null)
//             {
//                 studentProfile.image = Convert.ToBase64String(studentImage.image_file);
//             }
//             return studentProfile;//s.FirstOrDefault();
//
//
//         }
//
//         // public async Task<List<dynamic>> ListStudentInclude(v_student student)
//         // {
//         //
//         //   IQueryable<v_student> students=_context.v_student.AsNoTracking();
//         //
//         //   if (student!=null){
//         //     if (student.search!=null)
//         //     {
//         //       students = students.Where(m=>m.student_code.Contains(student.search) ||
//         //                                    m.first_name_th.Contains(student.search) ||
//         //                                    m.last_name_th.Contains(student.search));
//         //     }
//         //     else
//         //     {
//         //       students = students.Search(student);
//         //     }
//         //   }
//         //   students=students.OrderBy(c=>c.student_code).Take(200);
//         //   if (student==null)
//         //   {
//         //     return await students.ToDynamicListAsync();
//         //   }
//         //   else if (student.o!=null)
//         //   {
//         //     return await students.Select("new(" + string.Join(",",student.o) +")").ToDynamicListAsync();
//         //   }
//         //   else
//         //   {
//         //     return await students.ToDynamicListAsync();
//         //   }
//         // }
//         public async Task<List<v_student>> ListStudent(v_student student)
//         {
//
//             IQueryable<v_student> students=_context.v_student.AsNoTracking();
//
//             if (student!=null){
//                 if (student.search!=null)
//                 {
//                     students = students.Where(m=>m.student_code.Contains(student.search) ||
//                     m.first_name_th.Contains(student.search) ||
//                     m.last_name_th.Contains(student.search));
//                 }
//                 else
//                 {
//                     students = students.Search(student);
//                 }
//             }
//             students=students.OrderBy(c=>c.student_code).Take(200);
//             return await students.ToListAsync();
//         }
//         public async Task<SeventyOneDev.Utilities.PagedResult<v_student>> PagedStudent(v_student student,int page,int size)
//         {
//
//           IQueryable<v_student> students=_context.v_student.AsNoTracking();
//
//           if (student!=null){
//             if (student.search!=null)
//             {
//               students = students.Where(m=>m.student_code.Contains(student.search) ||
//                                            m.first_name_th.Contains(student.search) ||
//                                            m.last_name_th.Contains(student.search));
//             }
//             else
//             {
//               students = students.Search(student);
//             }
//           }
//           var results= await students.OrderBy(c=>c.student_code).GetPaged(page,size);
//           return results;
//         }
//         public async Task<v_student> GetStudent(string code)
//         {
//           var _student = await _context.v_student.AsNoTracking().FirstOrDefaultAsync(a => a.student_code == code);
//           if (_student != null)
//           {
//             _student.student_addresses = await _context.v_student_address.AsNoTracking().Include(s => s.address).Where(s=>s.student_code==code).ToListAsync();
//             _student.student_parents = await _context.v_student_parent.AsNoTracking().Include(s => s.parent).ThenInclude(s=>s.address)
//               .Include(s=>s.parent).ThenInclude(s=>s.company)
//               .Where(s=>s.student_code==code).ToListAsync();
//             _student.school =
//               await _context.v_school.AsNoTracking().FirstOrDefaultAsync(s => s.school_id == _student.school_id);
//             _student.student_documents = await _context.v_student_document.AsNoTracking()
//               .Where(d => d.student_code == code).ToListAsync();
//
//           }
//
//             return _student;
//         }
//         public async Task<student> NewStudent(student student,ClaimsIdentity claimsIdentity)
//         {
//
//             Console.WriteLine("Create new student:" + JsonSerializer.Serialize(student));
//             var _code=await _context.generate_code.FirstOrDefaultAsync(g=>g.code=="STD_CODE");
//             var _next = _code.current_number.Value+ 1;
//             var _studentCode = _code.data1 + _next.ToString("D" + _code.digit);
//             student.student_code = _studentCode;
//             if (await _context.student.AnyAsync(c=>
//                 c.student_code==student.student_code))
//             {
//                 Console.WriteLine("Create student error: Duplicate student code");
//                 throw new Exception("DATA_EXISTS");
//             }
//             try
//             {
//             await CreateEmail(_studentCode,student.first_name_th,student.last_name_th,"0123456789");
//             }
//             catch(Exception ex)
//             {
//                 Console.WriteLine("Create email error:" + ex.Message);
//             }
//             student.rsu_email = _studentCode + "@rsqapi.mailmaster.mobi";
//             _context.student.Add(student);
//             _code.current_number = _next;
//             await _context.SaveChangesAsync();
//             return student;
//         }
//
//         public async Task<int> UpdateStudent(student student,ClaimsIdentity claimsIdentity)
//         {
//             // if (await _context.student.AnyAsync(c=>c.student_code!=student.student_code &&
//             //     (c.first_name_th==student.first_name_th ||
//             //     c.last_name_th==student.last_name_th)))
//             // {
//             //     throw new Exception("DATA_EXISTS");
//             // }
//             _context.student.Update(student);
//             return await _context.SaveChangesAsync();
//         }
//
//         public async Task<int> DeleteStudent(string code,ClaimsIdentity claimsIdentity)
//         {
//             var _student = await _context.student.FirstOrDefaultAsync(c=>c.student_code==code);
//             _context.Remove(_student);
//             return await _context.SaveChangesAsync();
//
//         }
//         private async Task<bool> CreateEmail(string student_code,string first_name,string last_name,string password)
//         {
//             var httpClient = _httpClientFactory.CreateClient("gsuite");
//             var _mailRequest = new a_mail_request(){
//                 username= student_code + "@rsqapi.mailmaster.mobi",
//                 first_name = first_name,
//                 last_name = last_name,
//                 password = password,
//                 orgUnitPath = "/Year2020"
//             };
//             var content = new ByteArrayContent(JsonSerializer.SerializeToUtf8Bytes(_mailRequest));
//             content.Headers.ContentType = new  System.Net.Http.Headers.MediaTypeHeaderValue("application/json");
//             var res = await httpClient.PostAsync(_setting.gsuite_url + "/users/user_insert.php",content);
//             if (res.StatusCode==HttpStatusCode.OK)
//             {
//                 var stream = await res.Content.ReadAsStreamAsync();
//                 var response = await JsonSerializer.DeserializeAsync<a_mail_response>(stream);
//                 if (response.data.status == "success")
//                 {
//                     return true;
//                 }
//             }
//             return false;
//
//
//         }
//
//
//     }
// }
