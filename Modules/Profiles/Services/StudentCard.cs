// using System.Linq;
// using Microsoft.EntityFrameworkCore;
//
// using rsu_common_api.Modules.Profiles.Models;
// using SeventyOneDev.Utilities;
//
// namespace rsu_common_api.Modules.Profiles.Services
// {
//   public class StudentCardService
//   {
//     readonly Db _context;
//
//     public StudentCardService(Db context)
//     {
//       _context = context;
//     }
//
//     public student_card_response GetStudentCard(student_card_request studentCardRequest)
//     {
//       var studentCodes = studentCardRequest.student_params.Select(s => s.personal_id).ToList();
//       var result=_context.student.AsNoTracking().Where(s => studentCodes.Contains(s.student_code))
//         .Select(s => new student_card_response_result()
//         {
//           uid=s.card_uid,
//           personal_id = s.student_code,
//           first_name = s.first_name_th,
//           last_name = s.last_name_th,
//           first_name_eng = s.first_name_en,
//           last_name_eng = s.last_name_en
//         }).ToList();
//       if (result.Any())
//       {
//         return new student_card_response()
//         {
//           success = true,
//           result = result
//         };
//       }
//       else
//       {
//         return new student_card_response()
//         {
//           success = false
//         };
//       }
//
//     }
//
//   }
// }
