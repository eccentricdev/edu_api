using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using edu_api.Modules.Discipline.Databases.Models;
using edu_api.Modules.Profiles.Databases.Models;
using Microsoft.EntityFrameworkCore;
using rsu_common_api.models;
using SeventyOneDev.Utilities;

namespace rsu_common_api.Modules.Profiles.Services
{
  public class StudentService:EntityUidService<t_student,v_student>
  {
    private readonly Db _context;
    public StudentService(Db context) : base(context)
    {
      _context = context;
    }

    public override async Task<v_student> GetEntity(Guid uid, List<Guid?> agencies = null, string userName = null)
    {
      var rs =  await _context.v_student.AsNoTracking()
        .Include(a => a.student_parents)
        .Include(a => a.student_documents)
        .Include(a => a.student_addresses)
        .FirstOrDefaultAsync(w => w.student_uid == uid);

      foreach (var studentAddress in rs.student_addresses)
      {
        var address = await _context.v_address.AsNoTracking()
          .FirstOrDefaultAsync(w => w.address_uid == studentAddress.address_uid);
        studentAddress.address = address;
      }

      foreach (var studentParent in rs.student_parents)
      {
        var address = await _context.v_address.AsNoTracking()
          .FirstOrDefaultAsync(w => w.address_uid == studentParent.address_uid);
        studentParent.address = address;
      }

      rs.student_disciplines = new List<v_student_discipline>();
      var disciplines = await _context.v_student_discipline.AsNoTracking()
        .Include(a =>a.student_discipline_details)
        .Include(a =>a.student_discipline_scores)
        .Include(a =>a.student_discipline_exams)
        .Include(a =>a.student_service_activities)
        .Where(w => w.student_uid == rs.student_uid).ToListAsync();

      foreach (var discipline in disciplines)
      {
        rs.student_disciplines.Add(discipline);
      }

      rs.student_logs = new List<v_student_log>();
      var logs = await _context.v_student_log.AsNoTracking()
        .Where(w => w.student_uid == rs.student_uid).OrderByDescending(a =>a.date_changed).ToListAsync();

      foreach (var log in logs)
      {
        rs.student_logs.Add(log);
      }

      return rs;
    }

    public async Task<List<t_student_log>> NewStudentLog(List<ChangeTracking> entity,Guid? student_uid, ClaimsIdentity claimsIdentity)
    {
      var listResult = new List<t_student_log>();
      var bases = new base_table();
      foreach (var log in entity)
      {
        if (log.property_name != "status_id" && log.property_name != "created_by" && log.property_name != "created_datetime"
            && log.property_name != "updated_by" && log.property_name != "updated_datetime" &&
            log.property_name != "owner_agency_uid")
        {
          var rs = new t_student_log()
          {
            student_log_uid = Guid.NewGuid(),
            student_uid = student_uid,
            key = log.key,
            date_changed = log.date_changed,
            class_name = log.class_name,
            property_name = log.property_name,
            old_value = log.old_value,
            new_value = log.new_value,
            changed_by = claimsIdentity?.FindFirst("login_name")?.Value
          };
          listResult.Add(rs);

          await _context.t_student_log.AddAsync(rs);
          await _context.SaveChangesAsync(claimsIdentity);
        }
      }

      return listResult;
    }

    public async Task<List<v_student_log>> ListStudentLogResults(v_student_log entity, List<Guid?> agencies = null, string userName = null)
    {
      var entities = _context.v_student_log.AsNoTracking();
      var p =entity?.GetType().GetProperties().FirstOrDefault(a => a.Name == "search");
      if (p == null)
        return await entities.OrderByDescending(c => c.date_changed).ToListAsync();
      var con = p.GetValue(entity);
      entities = con != null ? entities.Search(con.ToString()) : entities.Search(entity);
      return await entities.OrderByDescending(c => c.date_changed).ToListAsync();
    }
    public override async Task<int> UpdateEntity(t_student entity, ClaimsIdentity claimsIdentity)
    {
      foreach (var studentAddress in entity.student_addresses)
      {
        if (!studentAddress.address.address_uid.HasValue)
        {
          studentAddress.address.address_uid = Guid.NewGuid();
          _context.t_address.Add(studentAddress.address);
          await _context.SaveChangesAsync();
          studentAddress.address_uid = studentAddress.address.address_uid;
        }
        if (studentAddress.address.address_uid.HasValue)
        {
          _context.t_address.Update(studentAddress.address);
          await _context.SaveChangesAsync();
        }
      }

      foreach (var studentParent in entity.student_parents)
      {
        if (!studentParent.address.address_uid.HasValue)
        {
          studentParent.address.address_uid = Guid.NewGuid();
          _context.t_address.Add(studentParent.address);
          await _context.SaveChangesAsync();
        }
        if (studentParent.address.address_uid.HasValue)
        {
          _context.t_address.Update(studentParent.address);
          await _context.SaveChangesAsync();
        }
      }
      return await base.UpdateEntity(entity, claimsIdentity);
    }



  }
}
