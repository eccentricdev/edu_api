using Microsoft.Extensions.DependencyInjection;
using rsu_common_api.Modules.Profiles.Services;

namespace rsu_common_api.Modules.Profiles.Configures
{
  public static class ProfileCollectionExtension
  {
    public static IServiceCollection AddProfileServices(this IServiceCollection services)
    {
      //services.AddAutoMapper(typeof(ProfileMapping));
      services.AddScoped<StudentStatusService>();
      services.AddScoped<StudentRestrictionService>();
      services.AddScoped<IamService>();
      services.AddScoped<StudentService>();
      //services.AddScoped<StudentCardService>();

      return services;
    }
  }
}
