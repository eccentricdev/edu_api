using System;
using System.ComponentModel.DataAnnotations;
using SeventyOneDev.Utilities;
using SeventyOneDev.Utilities.Attributes;

namespace rsu_common_api.Modules.Profiles.Databases.Models
{
  public class address_type:base_table
  {
    [Key]
    public int? address_type_id {get;set;}
    public string address_type_code {get;set;}
    public string address_type_name_th {get;set;}
    public string address_type_name_en {get;set;}
  }
  [GeneratedIdController("api/profile/address_type")]
  public class t_address_type : address_type
  {
    public t_address_type()
    {
    }

    public t_address_type(short? addressTypeId, string addressTypeCode, string addressTypeNameEn,
      string addressTypeNameTh)
    {
      this.address_type_id = addressTypeId;
      this.address_type_code = addressTypeCode;
      this.address_type_name_en = addressTypeNameEn;
      this.address_type_name_th = addressTypeNameTh;
      this.status_id = 1;
      this.created_by = "SYSTEM";
      this.created_datetime = DateTime.Now;
      this.updated_by = "SYSTEM";
      this.updated_datetime = DateTime.Now;
    }
  }
  public class v_address_type : address_type
  {
  }
}
