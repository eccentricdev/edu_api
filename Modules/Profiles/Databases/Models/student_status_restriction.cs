using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;
using rsu_common_api.models;
using SeventyOneDev.Core;
using SeventyOneDev.Utilities;

namespace rsu_common_api.Modules.Profiles.Databases.Models
{
  public class student_status_restriction:base_table
  {
    [Key]
    public Guid? student_status_restriction_uid { get; set; }
    public Guid? student_status_uid { get; set; }
    public Guid? student_restriction_uid { get; set; }

  }

  public class t_student_status_restriction : student_status_restriction
  {

    [JsonIgnore]
    public t_student_status student_status { get; set; }
    [JsonIgnore]
    public t_student_restriction student_restriction { get; set; }
  }

  public class v_student_status_restriction : student_status_restriction
  {
    public string student_restriction_code { get; set; }
    public string student_restriction_name_th { get; set; }
    public string student_restriction_name_en { get; set; }
    [JsonIgnore]
    public v_student_status student_status { get; set; }
    [JsonIgnore]
    public v_student_restriction student_restriction { get; set; }
  }
}
