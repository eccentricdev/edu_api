using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;
using SeventyOneDev.Utilities;
using SeventyOneDev.Utilities.Attributes;

namespace edu_api.Modules.Profiles.Databases.Models
{
    public class student_flag_type : base_table
    {
        [Key] public Guid? student_flag_type_uid { get; set; }
        public string student_flag_code { get; set; }
        public string student_flag_name_th { get; set; }
        public string student_flag_name_en { get; set; }
    }
    [GeneratedUidController("api/profile/student_flag_type")]
    public class t_student_flag_type : student_flag_type
    {
        [JsonIgnore]public List<t_student_flag> student_flags { get; set; }
    }


    public class v_student_flag_type : student_flag_type
    {
        [JsonIgnore]public List<v_student_flag> student_flags { get; set; }
    }
}