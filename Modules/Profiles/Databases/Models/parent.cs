// using System;
// using System.Collections.Generic;
// using System.ComponentModel.DataAnnotations;
// using System.ComponentModel.DataAnnotations.Schema;
// using System.Text.Json.Serialization;
// using SeventyOneDev.Utilities;
//
// namespace rsu_common_api.models
// {
//   //[Table("parent",Schema="EDU")]
//   public class parent:base_table
//   {
//     //[Key,DatabaseGenerated(DatabaseGeneratedOption.Identity)]
//     public Guid? parent_uid {get;set;}
//     //public short? parent_type_id { get; set; }
//     public short? prefix_id { get; set; }
//     public string first_name_th {get;set;}
//     public string last_name_th {get;set;}
//     public string middle_name_th {get;set;}
//     public string first_name_en {get;set;}
//     public string last_name_en {get;set;}
//     public string middle_name_en {get;set;}
//     public string display_name_th {get;set;}
//     public string display_name_en {get;set;}
//
//     public short? marital_status_id { get; set; }
//     public bool? is_deformed { get; set; }
//     public short? income_level_id { get; set; }
//     public string id_card_no { get; set; }
//     //
//     // public string id_card_issue_by { get; set; }
//     public DateTime? id_card_issue_date { get; set; }
//     public DateTime? id_card_expire_date { get; set; }
//     public Guid? address_uid { get; set; }
//
//     // public int? country_id { get; set; }
//     //
//     // public string postal_code { get; set; }
//     // [MaxLength(1000)]
//     // public string address { get; set; }
//     //
//     // public string moo_th {get;set;}
//     //
//     // public string road_th {get;set;}
//     //
//     // public string soi_th {get;set;}
//     // public int? sub_district_id { get; set; }
//     public string personal_email { get; set; }
//     public short? occupation_id { get; set; }
//     public string home_phone { get; set; }
//     public string mobile_phone { get; set; }
//
//     public Guid? company_uid { get; set; }
//
//     // public int? company_country_id { get; set; }
//     //
//     // public string company_postal_code { get; set; }
//     // [MaxLength(1000)]
//     // public string company_address { get; set; }
//     // public string company_sub_district_id { get; set; }
//     //[MaxLength(200)]
//     public string company_contact_email { get; set; }
//     //
//     public string company_contact_phone { get; set; }
//
//   }
//
//   public class t_parent : parent
//   {
//     //[JsonIgnore]
//     public t_address address { get; set; }
//     public t_company company { get; set; }
//     [JsonIgnore]
//     public t_student_parent student_parent { get; set; }
//   }
//   public class v_parent:parent
//   {
//     public v_address address { get; set; }
//     public v_company company { get; set; }
//     [JsonIgnore]
//     public v_student_parent student_parent { get; set; }
//   }
// }
