using System;
using System.ComponentModel.DataAnnotations;
using SeventyOneDev.Utilities;
using SeventyOneDev.Utilities.Attributes;

namespace edu_api.Modules.Profiles.Databases.Models
{
    public class student_card_record:base_table
    {
        [Key] 
        public Guid? student_card_record_uid { get; set; }
        public string student_code { get; set; }
        public string digits_code { get; set; }
        public int? times { get; set; }
        public DateTime? student_card_issue_date { get; set; }
        public DateTime? student_card_expire_date { get; set; }
        public bool? is_long_name { get; set; }
        public bool? is_english  { get; set; }
        public bool? is_display  { get; set; }
        public string student_image_url { get; set; }
    }
    
    [GeneratedUidController("api/profile/student_card_record")]
    public class t_student_card_record : student_card_record
    {
        
    }


    public class v_student_card_record : student_card_record
    {
        public string display_name_th { get; set; }
        public string faculty_curriculum_name_th { get; set; }
        public string major_name_th { get; set; }
    }
}