using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;
using SeventyOneDev.Utilities;

namespace rsu_common_api.models
{
  //[Table("company",Schema="EDU")]
  public class company:base_table
  {
      [Key]
      public Guid? company_id { get; set; }
      public string company_name { get; set; }
      public Guid? company_address_uid { get; set; }
      public string company_email { get; set; }
      public string company_phone { get; set; }


  }

  public class t_company : company
  {
    [JsonIgnore]
    public t_student_parent student_parent { get; set; }
    public t_address address { get; set; }
  }


  public class v_company : company
  {
    [JsonIgnore]
    public v_student_parent student_parent { get; set; }
    public v_address address { get; set; }
  }
  // {
  //   [Key]
  //   public int? company_id { get; set; }
  //   [MaxLength(200)]
  //   public string company_name { get; set; }
  //   public int? company_address_id { get; set; }
  //   [MaxLength(200)]
  //   public string company_email { get; set; }
  //
  //   public string company_phone { get; set; }
  //
  //   [JsonIgnore]
  //   public List<v_parent> parents { get; set; }
  //   public v_address company_address { get; set; }
  //   [NotMapped]
  //   public string search { get; set; }
  //
  // }
}
