using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;
using SeventyOneDev.Utilities;
using SeventyOneDev.Utilities.Attributes;

namespace rsu_common_api.models
{
  //[Table("student_document",Schema="EDU")]
  public class student_document:base_table
  {
    [Key]
    public Guid? student_document_uid { get; set; }
    public Guid? student_uid { get; set; }
    public int? attached_document_id { get; set; }
    public int? required_document_id { get; set; }
    [MaxLength(500)]
    public string original_file_name { get; set; }
    [MaxLength(500)]
    public string file_id { get; set; }
    [MaxLength(500)]
    public string file_name { get; set; }
    [MaxLength(500)]
    public string file_path { get; set; }
    
    public bool? is_approve { get; set; }
    public bool? is_request_edit { get; set; }
    public string remark { get; set; }

  }
  public class t_student_document : student_document
  {
    [JsonIgnore]
    public t_student student { get; set; }
  }
  public class v_student_document:student_document
  {
    // [Key]
    // public int? student_document_id { get; set; }
    // public string student_code { get; set; }
    // public short? attached_document_id { get; set; }
    // public short? required_document_id { get; set; }
    // public string original_file_name { get; set; }
    // public string file_id { get; set; }
    // public string file_name { get; set; }
    // public string file_path { get; set; }
    [JsonIgnore]
    public v_student student { get; set; }
  }
}
