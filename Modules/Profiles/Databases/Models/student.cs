using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;
using edu_api.Modules.Discipline.Databases.Models;
using edu_api.Modules.Profiles.Databases.Models;
using SeventyOneDev.Utilities;
using SeventyOneDev.Utilities.Attributes;

namespace rsu_common_api.models
{
  public class student : base_table
  {
    [Key] public Guid? student_uid { get; set; }
    [OrderBy(true)]
    public string student_code { get; set; }
    public int? prefix_id { get; set; }
    public string first_name_th { get; set; }
    public string last_name_th { get; set; }
    public string middle_name_th { get; set; }
    public string first_name_en { get; set; }
    public string last_name_en { get; set; }
    public string middle_name_en { get; set; }
    public string display_name_th { get; set; }
    public string display_name_en { get; set; }

    public int? gender_id { get; set; }
    public int? marital_status_id { get; set; }
    public int? military_status_id { get; set; }
    public int? religion_id { get; set; }
    public int? nationality_id { get; set; }
    public int? blood_group_id { get; set; }
    public int? family_status_id { get; set; }
    public int? relatives_child { get; set; }
    public int? relatives_child_study { get; set; }
    public int? support_by_id { get; set; }
    public int? support_per_month_id { get; set; }
    public int? student_type_id { get; set; }
    public bool? is_dorm_student { get; set; }
    // Remove may be not used
    public string citizen_id { get; set; }
    public DateTime? citizen_id_card_issue_date { get; set; }
    public DateTime? citizen_id_card_expire_date { get; set; }

    public string passport_no { get; set; }
    public DateTime? passport_issue_date { get; set; }
    public DateTime? passport_expire_date { get; set; }
    public int? passport_country_id { get; set; }

    public string uid_no { get; set; }
    [MaxLength(50)]
    public string tax_id { get; set; }
    public string phone_no { get; set; }
    public string mobile_no { get; set; }
    public string fax_no { get; set; }
    public string personal_email { get; set; }
    public string rsu_email { get; set; }
    public string extraction { get; set; }


    [Column(TypeName="decimal(5,2)")]
    public decimal? weight { get; set; }
    [Column(TypeName="decimal(5,2)")]
    public decimal? height { get; set; }
    public DateTime? birth_date { get; set; }
    public int? born_province_id { get; set; }

    //public string id_card_issue_by {get;set;}


    public Guid? entry_academic_year_uid { get; set; }
    public Guid? entry_semester_uid { get; set; }
    public Guid? education_type_uid { get; set; }
    public Guid? college_faculty_uid { get; set; }
    public Guid? faculty_curriculum_uid { get; set; }
    public Guid? student_status_uid { get; set; }

    public int? major_id { get; set; }
    //
    // public string graduate_code {get;set;}





    public bool? is_rsu_student { get; set; }

    // public string foreign_student {get;set;}
    public string student_image_url { get; set; }


    //public bool? is_education_validated {get;set;}

    //Apply

    public int? entry_type_id { get; set; }
    public int? entry_program_id { get; set; }
    public long? application_id { get; set; }
    public int? entry_education_level_id { get; set; }
    public int? entry_program_type_id { get; set; }
    public int? school_id { get; set; }
    public string entry_graduate_certificate_no { get; set; }
    public DateTime? entry_graduate_date { get; set; }
    [MaxLength(1)] public string approve_certificate { get; set; }
    public bool? is_education_checked { get; set; }

    [MaxLength(50)] public string entry_graduate_year { get; set; }
    [MaxLength(50)] public string entry_gpa { get; set; }
    public int? honor_status_id { get; set; }
    public Guid? graduate_academic_year_uid { get; set; }
    public Guid? graduate_semester_uid { get; set; }
    public DateTime? graduate_date { get; set; }
    public string graduate_document_no { get; set; }
    public int? graduate_country_id { get; set; }
    public int? graduate_province_id { get; set; }

    // [MaxLength(1)]
    // public string honor_status {get;set;}


    // public DateTime? date_in {get;set;}
    [Column(TypeName="decimal(5,2)")]
    public decimal? cumulative_gpa { get; set; }
    [Column(TypeName="decimal(5,1)")]
    public decimal? total_credit { get; set; }
    public Guid? advisor_uid { get; set; }


    public bool? is_deformed { get; set; }
    public string deformed_no { get; set; }
    public DateTime? deformed_issue_date { get; set; }
    public DateTime? deformed_expire_date { get; set; }
    public string deformed_detail { get; set; }


    public string visa_no { get; set; }
    public DateTime? visa_issue_date { get; set; }
    public DateTime? visa_expire_date { get; set; }

    public string old_student_code { get; set; }
    public string remark { get; set; }
    [MaxLength(200)]
    public string line_oa_id { get; set; }
    //public bool? is_package { get; set; }

    //[MaxLength(50)] public string card_uid { get; set; }

    // public int? register_address_id { get; set; }
    // public int? contact_address_id { get; set; }
    // public address register_address { get; set; }
    // public address contact_address { get; set; }




    // [MaxLength(1000)]
    // public string register_address_th {get;set;}
    //
    // public string register_moo_th {get;set;}
    //
    // public string register_road_th {get;set;}
    //
    // public string register_soi_th {get;set;}
    // [MaxLength(1000)]
    // public string register_address_en {get;set;}
    //
    // public string register_moo_en {get;set;}
    //
    // public string register_road_en {get;set;}
    //
    // public string register_soi_en {get;set;}
    // public int? register_sub_district_id {get;set;}
    // // public int? register_district_id {get;set;}
    // // public int? register_province_id {get;set;}
    // public int? register_country_id {get;set;}
    //
    // public string register_postal_code {get;set;}
    //
    // public bool? same_as_register_address {get;set;}





  }

  // [GeneratedUidController("api/profile/student")]
  public class t_student : student
  {
    [Include] public List<t_student_parent> student_parents { get; set; }
    [Include] public List<t_student_address> student_addresses { get; set; }
    [Include] public List<t_student_document> student_documents { get; set; }

    // public List<t_student_discipline> student_disciplines { get; set; }

    // [JsonIgnore] public blood_group blood_group { get; set; }
    // [JsonIgnore] public entry_type entry_type { get; set; }
    // [JsonIgnore] public family_status family_status { get; set; }
    // [JsonIgnore] public honor_status honor_status { get; set; }
    // [JsonIgnore] public marital_status marital_status { get; set; }
    // [JsonIgnore] public military_status military_status { get; set; }
    //
    // [JsonIgnore] public religion religion { get; set; }
    //
    // // [JsonIgnore]
    // // public student_status student_status { get; set; }
    // [JsonIgnore] public student_type student_type { get; set; }
    // [JsonIgnore] public gender gender { get; set; }
  }

  public class v_student:student
  {
    // [Key] public Guid? student_uid { get; set; }
    // public string student_code { get; set; }
    // public int? prefix_id { get; set; }
    // public string first_name_th { get; set; }
    // public string last_name_th { get; set; }
    // public string middle_name_th { get; set; }
    // public string first_name_en { get; set; }
    // public string last_name_en { get; set; }
    // public string middle_name_en { get; set; }
    // public string display_name_th { get; set; }
    // public string display_name_en { get; set; }
    //
    // public short? gender_id { get; set; }
    //
    // // [MaxLength(1)]
    // // public string marital_status {get;set;}
    // public short? marital_status_id { get; set; }
    //
    // //
    // // public string military_status {get;set;}
    // public short? military_status_id { get; set; }
    // public string phone_no { get; set; }
    // public string mobile_no { get; set; }
    // public string fax_no { get; set; }
    // public string personal_email { get; set; }
    // public string rsu_email { get; set; }
    // public short? religion_id { get; set; }
    // public int? nationality_id { get; set; }
    //  public string extraction { get; set; }
    // public short? blood_group_id { get; set; }
    // public bool? is_deformed { get; set; }
    // public decimal? weight { get; set; }
    // public decimal? height { get; set; }
    // public DateTime? date_of_birth { get; set; }
    // public int? born_province_id { get; set; }
    //
    // public string id_card_no { get; set; }
    //
    // //public string id_card_issue_by {get;set;}
    // public DateTime? id_card_issue_date { get; set; }
    // public DateTime? id_card_expire_date { get; set; }
    // public string passport_no { get; set; }
    // public DateTime? passport_issue_date { get; set; }
    // public DateTime? passport_expire_date { get; set; }
    // public int? passport_country_id { get; set; }
    // public short? student_type_id { get; set; }
    // public int? education_type_id { get; set; }
    // public int? faculty_id { get; set; }
    // public int? major_id { get; set; }
    // public Guid? student_status_uid { get; set; }
    // public short? entry_type_id { get; set; }
    // public int? entry_program_id { get; set; }
    // public long? application_id { get; set; }
    // public int? entry_academic_year_id { get; set; }
    //
    // public int? entry_academic_semester_id { get; set; }
    //
    // //
    // // public string graduate_code {get;set;}
    // public int? entry_education_level_id { get; set; }
    // public int? entry_program_type_id { get; set; }
    // public int? school_id { get; set; }
    // public string entry_graduate_certificate_no { get; set; }
    // public DateTime? entry_graduate_date { get; set; }
    // [MaxLength(50)] public string entry_graduate_year { get; set; }
    // [MaxLength(50)] public string entry_gpa { get; set; }
    // public int? relatives_child { get; set; }
    // public int? relatives_child_study { get; set; }
    //
    // public short? family_status_id { get; set; }
    //
    // // Remove may be not used
    // public short? support_by_id { get; set; }
    // public short? support_per_month_id { get; set; }
    //
    // public bool? is_rsu_student { get; set; }
    //
    // // public string foreign_student {get;set;}
    // public string student_image_url { get; set; }
    //
    // [MaxLength(1)] public string approve_certificate { get; set; }
    //
    // public bool? is_education_checked { get; set; }
    // //public bool? is_education_validated {get;set;}
    //
    //
    // public int? graduate_academic_year_id { get; set; }
    // public int? graduate_academic_semester_id { get; set; }
    // public DateTime? graduate_date { get; set; }
    //
    // public string graduate_document_no { get; set; }
    //
    // // [MaxLength(1)]
    // // public string honor_status {get;set;}
    // public short? honor_status_id { get; set; }
    //
    //
    // public bool? is_dorm_student { get; set; }
    //
    // // public DateTime? date_in {get;set;}
    // public decimal? cumulative_gpa { get; set; }
    // public short? total_credit { get; set; }
    // public string advisor_code { get; set; }
    //
    // [MaxLength(50)] public string card_uid { get; set; }


    // public string prefix_code { get; set; }
    // public string prefix_name_en { get; set; }
    // public string prefix_name_th { get; set; }
    //
    // public string gender_code { get; set; }
    // public string gender_name_en { get; set; }
    // public string gender_name_th { get; set; }
    //
    // public string major_code { get; set; }
    // public string major_name_en { get; set; }
    public string major_name_th { get; set; }
    public string education_type_name_th { get; set; }
    public string education_type_name_en { get; set; }
    public string student_status_name_th { get; set; }
    //
    // public string faculty_code { get; set; }
    // public string faculty_name_en { get; set; }
    // public string faculty_name_th { get; set; }
    //
    // public string education_type_code { get; set; }
    // public string education_type_name_en { get; set; }
    // public string education_type_name_th { get; set; }
    //
    // public string entry_academic_year_code { get; set; }
    // public string entry_academic_year_name_en { get; set; }
    //
    public string entry_academic_year_name_th { get; set; }
    // public string entry_academic_semester_code { get; set; }
    // public string entry_academic_semester_name_en { get; set; }
    public string entry_academic_semester_name_th { get; set; }
    //
    // public string marital_status_code { get; set; }
    // public string marital_status_name_en { get; set; }
    // public string marital_status_name_th { get; set; }
    //
    // public string student_status_name_th { get; set; }
    //
    // public string student_status_name_en { get; set; }

    public string faculty_curriculum_name_th { get; set; }
    public string faculty_curriculum_name_en { get; set; }
    public string college_faculty_name_th { get; set; }
    public string college_faculty_name_en { get; set; }

    public string graduate_country_name_th { get; set; }
    public string graduate_province_name_th { get; set; }
   
    public Guid? education_type_level_uid { get; set; }
    public string education_type_level_code { get; set; }
    public string education_type_level_name_en { get; set; }
    public string education_type_level_name_th { get; set; }

    // [NotMapped]
    // public v_school school { get; set; }
    [Include] public List<v_student_parent> student_parents { get; set; }
    [Include]public List<v_student_address> student_addresses { get; set; }
    [Include]public List<v_student_document> student_documents { get; set; }

    [NotMapped]
    public List<v_student_discipline> student_disciplines { get; set; }
    [NotMapped]
    public List<v_student_log> student_logs { get; set; }





  }
}

