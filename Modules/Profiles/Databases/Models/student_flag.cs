using System;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;
using SeventyOneDev.Utilities;
using SeventyOneDev.Utilities.Attributes;

namespace edu_api.Modules.Profiles.Databases.Models
{
    public class student_flag:base_table
    {
        [Key]
        public Guid? student_flag_uid {get; set; }
        public Guid? student_uid { get; set; }
        public Guid? academic_year_uid { get; set; }
        public Guid? academic_semester_uid { get; set; }
        public DateTime? flag_date { get; set; }
        public Guid? student_flag_type_uid { get; set; }
        public Guid? agency_uid { get; set; }
        [MaxLength(500)]
        public string description_th { get; set; }
        [MaxLength(500)]
        public string description_en { get; set; }
        public bool? is_active { get; set; }
    }
    [GeneratedUidController("api/profile/student_flag")]
    public class t_student_flag : student_flag
    {
        [JsonIgnore]public t_student_flag_type student_flag_type { get; set; }
    }

    public class v_student_flag : student_flag
    {
        public string student_flag_code {get; set; }
        public string student_flag_name_th { get; set; }
        public string student_flag_name_en { get; set; }
        public string student_code { get; set; }
        public string display_name_en { get; set; }
        public string display_name_th { get; set; }
        public string academic_year_code { get; set; }
        public string academic_year_name_th { get; set; }
        public string academic_year_name_en { get; set; }
        public string academic_semester_code { get; set; }
        public string semester_name_th { get; set; }
        public string semester_name_en { get; set; }

        [JsonIgnore] public v_student_flag_type student_flag_type { get; set; }
    }
}