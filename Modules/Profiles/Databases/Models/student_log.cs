using System;
using System.ComponentModel.DataAnnotations;

namespace edu_api.Modules.Profiles.Databases.Models
{
    public class student_log
    {
        [Key]
        public Guid? student_log_uid {get;set;}
        public Guid? student_uid {get;set;}
        public string class_name { get; set; }
        public string property_name { get; set; }
        public Guid key { get; set; }
        public string old_value { get; set; }
        public string new_value { get; set; }
        public DateTime date_changed { get; set; }
        public string changed_by { get; set; }
    }

    public class t_student_log : student_log
    {
        
    }
    
    public class v_student_log : student_log
    {
        
    }
}