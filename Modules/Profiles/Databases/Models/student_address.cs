using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;
using SeventyOneDev.Utilities;
using SeventyOneDev.Utilities.Attributes;

namespace rsu_common_api.models
{
  public class student_address:base_table
  {
    [Key]
    public Guid? student_address_uid { get; set; }
    public Guid? student_uid { get; set; }
    //public int? address_id { get; set; }
    public Guid? address_uid { get; set; }
    public int? address_type_id { get; set; }
  }

  public class t_student_address : student_address
  {
    [JsonIgnore]
    public t_student student { get; set; }

    [NotMapped]
    public t_address address { get; set; }
  }
  public class v_student_address : student_address
  {
    [JsonIgnore]
    public v_student student { get; set; }
    [NotMapped]
    public v_address address { get; set; }
  }
  // [Table("student_address",Schema="EDU")]
  // public class student_address
  // {
  //   [Key,DatabaseGenerated(DatabaseGeneratedOption.Identity)]
  //   public int? student_address_id {get;set;}
  //   [MaxLength(50)]
  //   public string student_code { get; set; }
  //   public int? address_id { get; set; }
  //   public short? address_type_id { get; set; }
  //   public address address { get; set; }
  //   [JsonIgnore]
  //   public student student { get; set; }
  // }
  // public class v_student_address
  // {
  //   [Key]
  //   public int? student_address_id {get;set;}
  //   [MaxLength(50)]
  //   public string student_code { get; set; }
  //   public int? address_id { get; set; }
  //   public short? address_type_id { get; set; }
  //
  //   public string address_type_code { get; set; }
  //   public string address_type_name_th { get; set; }
  //   public string address_type_name_en { get; set; }
  //
  //   public v_address address { get; set; }
  //   [JsonIgnore]
  //   public v_student student { get; set; }
  //   [NotMapped]
  //   public string search {get;set;}
  // }
}
