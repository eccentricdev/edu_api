using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;
using SeventyOneDev.Utilities;

namespace rsu_common_api.models
{
  public class address:base_table
  {
    [Key]
    public Guid? address_uid { get; set; }
    //public int? address_id { get; set; }
    public string address_name { get; set; }
    public string house_no { get; set; }
    public string room_no { get; set; }
    public string floor_no { get; set; }
    public string building_name { get; set; }
    public string village_name { get; set; }
    public string village_no { get; set; }
    public string alley_name { get; set; }
    public string street_name { get; set; }
    public int? sub_district_id { get; set; }
    public string other_sub_district_name_th { get; set; }
    public string other_sub_district_name_en { get; set; }
    public string other_district_name_th { get; set; }
    public string other_district_name_en { get; set; }
    public string other_province_name_th { get; set; }
    public string other_province_name_en { get; set; }
    public string postal_code { get; set; }
    public int? country_id { get; set; }
    public string telephone_no { get; set; }
  }

  public class t_address : address
  {
    // [JsonIgnore]
    // public List<t_applicant_address> applicant_addresses { get; set; }
    // [JsonIgnore]
    // public List<t_student_address> student_addresses { get; set; }
    // [JsonIgnore]
    // public t_student_parent student_parent { get; set; }
    // [JsonIgnore]
    // public t_company company { get; set; }
    // [JsonIgnore]
    // public List<t_contact> contacts { get; set; }
    // [JsonIgnore]
    // public List<t_employment_contract> employment_contract { get; set; }
  }
  public class v_address : address
  {
    // [JsonIgnore]
    // public List<v_applicant_address> applicant_addresses { get; set; }
    // [JsonIgnore]
    // public List<v_student_address> student_addresses { get; set; }
    // [JsonIgnore]
    // public v_student_parent student_parent { get; set; }
    // [JsonIgnore]
    // public v_company company { get; set; }
    // [JsonIgnore]
    // public List<v_contact> contacts { get; set; }
    // [JsonIgnore]
    // public List<v_employment_contract> employment_contract { get; set; }
  }
  // [Table("address",Schema="EDU")]
  // public class address
  // {
  //   [Key,DatabaseGenerated(DatabaseGeneratedOption.Identity)]
  //   public int? address_id { get; set; }
  //   [MaxLength(1000)]
  //   public string address_th {get;set;}
  //   [MaxLength(200)]
  //   public string moo_ban { get; set; }
  //
  //   public string moo_th {get;set;}
  //
  //   public string road_th {get;set;}
  //
  //   public string soi_th {get;set;}
  //   [MaxLength(1000)]
  //   public string address_en {get;set;}
  //
  //   public string moo_en {get;set;}
  //
  //   public string road_en {get;set;}
  //
  //   public string soi_en {get;set;}
  //   public int? sub_district_id {get;set;}
  //   // public int? district_id {get;set;}
  //   // public int? province_id {get;set;}
  //   public int? country_id {get;set;}
  //
  //   public string postal_code {get;set;}
  //
  //
  //   [JsonIgnore]
  //   public List<student_address> student_addresses { get; set; }
  //   [JsonIgnore]
  //   public List<parent> parents { get; set; }
  //   [JsonIgnore]
  //   public List<company> companies { get; set; }
  // }
  // public class v_address
  // {
  //   [Key]
  //   public int? address_id { get; set; }
  //   [MaxLength(1000)]
  //   public string address_th {get;set;}
  //   public string moo_ban { get; set; }
  //
  //   public string moo_th {get;set;}
  //
  //   public string road_th {get;set;}
  //
  //   public string soi_th {get;set;}
  //   [MaxLength(1000)]
  //   public string address_en {get;set;}
  //
  //   public string moo_en {get;set;}
  //
  //   public string road_en {get;set;}
  //
  //   public string soi_en {get;set;}
  //   public int? sub_district_id {get;set;}
  //   // public int? district_id {get;set;}
  //   // public int? province_id {get;set;}
  //   public int? country_id {get;set;}
  //
  //   public string postal_code {get;set;}
  //   public string sub_district_name_th { get; set; }
  //   public string sub_district_name_en { get; set; }
  //   public int? district_id { get; set; }
  //   public string district_name_th { get; set; }
  //   public string district_name_en { get; set; }
  //   public int? province_id { get; set; }
  //   public string province_name_th { get; set; }
  //   public string province_name_en { get; set; }
  //   public string country_name_th { get; set; }
  //   public string country_name_en { get; set; }
  //
  //   [JsonIgnore]
  //   public List<v_student_address> student_addresses { get; set; }
  //   [JsonIgnore]
  //   public List<v_parent> parents { get; set; }
  //   [JsonIgnore]
  //   public List<v_company> companies { get; set; }
  //
  //   [NotMapped]
  //   public string search {get;set;}
  //   // [JsonIgnore]
  //   // public List<student> students { get; set; }
  //   // [JsonIgnore]
  //   // public List<parent> parents { get; set; }
  // }
}
