using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace rsu_common_api.models
{
    [Table("student_grade",Schema="EDU")]
    public class student_grade
    {
        [Key,DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int? student_grade_id {get;set;}
        public Guid? academic_year_uid {get;set;}
        public int? academic_semester_id {get;set;}
        public string academic_year {get;set;}
        public string academic_semester {get;set;}

        public string student_code {get;set;}
        [MaxLength(2)]
        public string credit {get;set;}
        [MaxLength(300)]
        public string subject_name_th {get;set;}
        [MaxLength(300)]
        public string subject_name_en {get;set;}
        [MaxLength(5)]
        public string grade {get;set;}

        public string subject_seq {get;set;}

        public string subject_code {get;set;}

        public string education_type_code {get;set;}

        public string lecture_section {get;set;}

        public string lab_section {get;set;}

        public string cancel_status {get;set;}

        public string seq {get;set;}

        public string topic_code {get;set;}
        //
        // public string topic_name_th {get;set;}
        //
        // public string topic_name_en {get;set;}
    }
    // ACADEMIC_YEAR,
    // SEMESTER,
    // STUDENT_CODE, CRE_BY, CRE_DATE, UPD_BY, UPD_DATE, UPD_PGM,
    // CREDIT,
    // SUBJ_NAME,
    // GRADE_F_REASON,
    // GRADE,
    // SUBJ_SEQ,
    // EDUCATION_TYPE_CODE,
    // SUBJ_CODE,
    // TF_DOC_NO, T
    // RANS_CAL_GPA_FLAG,
    // REGRADE_CAL_GPA_FLAG, LEC_SEC, LAB_SEC, CANCEL_STATUS, TF_CODE, TOPIC_CODE, SEQ, SUBJ_NAME_ENG
    // FROM RSU.GD_GRADE_TRX;

}
