using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using rsu_common_api.models;
using SeventyOneDev.Core;
using SeventyOneDev.Utilities;
using SeventyOneDev.Utilities.Attributes;

namespace rsu_common_api.Modules.Profiles.Databases.Models
{
  public class student_restriction:base_table
  {
    [Key]
    public Guid? student_restriction_uid { get; set; }
    public string student_restriction_code { get; set; }
    public string student_restriction_name_th { get; set; }
    public string student_restriction_name_en { get; set; }
  }
  [GeneratedUidController("api/profile/student_restriction")]
  public class t_student_restriction : student_restriction
  {
    [Include]
    public List<t_student_status_restriction> student_status_restrictions { get; set; }
  }

  public class v_student_restriction : student_restriction
  {
    [Include]
    public List<v_student_status_restriction> student_status_restrictions { get; set; }
  }
}
