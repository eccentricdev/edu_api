using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using SeventyOneDev.Core;
using SeventyOneDev.Utilities;
using SeventyOneDev.Utilities.Attributes;

namespace rsu_common_api.Modules.Profiles.Databases.Models
{
  public class student_status:base_table
  {
    [Key]
    public Guid? student_status_uid { get; set; }
    public string student_status_code { get; set; }
    public string student_status_name_th { get; set; }
    public string student_status_name_en { get; set; }

  }

  [GeneratedUidController("api/profile/student_status")]
  public class t_student_status : student_status
  {
    public t_student_status()
    {

    }

    public t_student_status(Guid studentStatusUid, string studentStatusCode, string studentStatusNameTh,
      string studentStatusNameEn)
    {
      this.student_status_uid = studentStatusUid;
      this.student_status_code = studentStatusCode;
      this.student_status_name_th = studentStatusNameTh;
      this.student_status_name_en = studentStatusNameEn;
      this.created_by = "SYSTEM";
      this.created_datetime = DateTime.Now;
      this.updated_by = "SYSTEM";
      this.updated_datetime = DateTime.Now;
    }
    [Include]
    public List<t_student_status_restriction> student_status_restrictions { get; set; }
  }

  public class v_student_status : student_status
  {
    [Include]
    public List<v_student_status_restriction> student_status_restrictions { get; set; }
  }

}
