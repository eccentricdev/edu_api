// namespace rsu_common_api.models
// {
//     public class student_profile_mobile
//     {
//         public string student_code {get;set;}
//         public string student_name_th {get;set;}
//         public string student_name_en {get;set;}
//         public string education_type_name_th {get;set;}
//         public string education_type_name_en {get;set;}
//         public string faculty_name_th {get;set;}
//         public string faculty_name_en {get;set;}
//         public string major_name_th {get;set;}
//         public string major_name_en {get;set;}
//         public int? education_year {get;set;}
//         public int? total_credit {get;set;}
//         public decimal? cumulative_gpa {get;set;}
//         public string advisor_name_th {get;set;}
//         public string advisor_name_en {get;set;}
//         public string advisor_phone_no {get;set;}
//         public string advisor_email {get;set;}
//         public string image {get;set;}
//         public student_profile_mobile() {}
//         public student_profile_mobile(
//             string student_code,
//             string student_name_th,
//             string student_name_en,
//             string faculty_name_th,
//             string faculty_name_en,
//             string major_name_th,
//             string major_name_en,
//             int? education_year,
//             int? total_credit,
//             decimal? cumulative_gpa,
//             string advisor_name_th,
//             string advisor_name_en,
//             string advisor_phone_no,
//             string advisor_email,
//             string education_type_name_th,
//             string education_type_name_en)
//         {
//             this.student_code = student_code;
//             this.student_name_th = student_name_th;
//             this.student_name_en = student_name_en;
//             this.faculty_name_th = faculty_name_th;
//             this.faculty_name_en = faculty_name_en;
//             this.major_name_th = major_name_th;
//             this.major_name_en = major_name_en;
//             this.education_year = education_year;
//             this.total_credit = total_credit;
//             this.cumulative_gpa = cumulative_gpa;
//             this.advisor_name_th = advisor_name_th;
//             this.advisor_name_en = advisor_name_en;
//             this.advisor_phone_no = advisor_phone_no;
//             this.advisor_email = advisor_email;
//             this.education_type_name_th = education_type_name_th;
//             this.education_type_name_en = education_type_name_en;
//         }
//
//     }
// }
