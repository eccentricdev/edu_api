using System;
using System.ComponentModel.DataAnnotations;
using SeventyOneDev.Utilities;
using SeventyOneDev.Utilities.Attributes;

namespace rsu_common_api.Modules.Profiles.Databases.Models
{
      public class parent_type:base_table
      {
          [Key]
          public int? parent_type_id {get;set;}
          public string parent_type_code {get;set;}
          public string parent_type_name_th {get;set;}
          public string parent_type_name_en {get;set;}


      }
      [GeneratedIdController("api/profile/parent_type")]
      public class t_parent_type : parent_type
      {
        public t_parent_type()
        {
        }

        public t_parent_type(short? parentTypeId, string parentTypeCode, string parentTypeNameEn,
          string parentTypeNameTh)
        {
          this.parent_type_id = parentTypeId;
          this.parent_type_code = parentTypeCode;
          this.parent_type_name_en = parentTypeNameEn;
          this.parent_type_name_th = parentTypeNameTh;
          this.status_id = 1;
          this.created_by = "SYSTEM";
          this.updated_by = "SYSTEM";
          this.created_datetime = DateTime.Now;
          this.updated_datetime = DateTime.Now;
        }
      }
      public class v_parent_type :parent_type
      {


      }
}
