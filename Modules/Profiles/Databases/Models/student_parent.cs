using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;
using SeventyOneDev.Utilities;
using SeventyOneDev.Utilities.Attributes;

namespace rsu_common_api.models
{
  public class student_parent : base_table
  {
    [Key]
    public Guid? student_parent_uid { get; set; }
    public Guid? student_uid { get; set; }
    //public Guid? parent_uid { get; set; }
    public int? parent_type_id { get; set; }
    //public Guid? parent_uid {get;set;}
    //public short? parent_type_id { get; set; }
    public int? prefix_id { get; set; }
    public string first_name_th {get;set;}
    public string last_name_th {get;set;}
    public string middle_name_th {get;set;}
    public string first_name_en {get;set;}
    public string last_name_en {get;set;}
    public string middle_name_en {get;set;}
    public string display_name_th {get;set;}
    public string display_name_en {get;set;}
    public int? marital_status_id { get; set; }
    public int? income_level_id { get; set; }
    public string id_card_no { get; set; }
    //
    // public string id_card_issue_by { get; set; }
    public DateTime? id_card_issue_date { get; set; }
    public DateTime? id_card_expire_date { get; set; }
    public Guid? address_uid { get; set; }

    // public int? country_id { get; set; }
    //
    // public string postal_code { get; set; }
    // [MaxLength(1000)]
    // public string address { get; set; }
    //
    // public string moo_th {get;set;}
    //
    // public string road_th {get;set;}
    //
    // public string soi_th {get;set;}
    // public int? sub_district_id { get; set; }
    public string personal_email { get; set; }
    public int? occupation_id { get; set; }
    public string home_phone { get; set; }
    public string mobile_phone { get; set; }

    public Guid? company_uid { get; set; }

    // public int? company_country_id { get; set; }
    //
    // public string company_postal_code { get; set; }
    // [MaxLength(1000)]
    // public string company_address { get; set; }
    // public string company_sub_district_id { get; set; }
    //[MaxLength(200)]
    public string company_contact_email { get; set; }
    //
    public string company_contact_phone { get; set; }

    public bool? is_deformed { get; set; }
    public string deformed_no { get; set; }
    public DateTime? deformed_issue_date { get; set; }
    public DateTime? deformed_expire_date { get; set; }
    public string deformed_detail { get; set; }

    public string company_name { get; set; }
    public string company_province_id { get; set; }
    
    public string relationship_with_student {get;set;}

  }

  public class t_student_parent : student_parent
  {
    [JsonIgnore]
    public t_student student { get; set; }
    [NotMapped]
    public t_address address { get; set; }
    // [Include]
    // public t_company company { get; set; }
    //public t_parent parent { get; set; }
  }
  public class v_student_parent : student_parent
  {
    [JsonIgnore]
    public v_student student { get; set; }
    //public v_parent parent { get; set; }
    [NotMapped]
    public v_address address { get; set; }
    // [Include]
    // public v_company company { get; set; }
  }
  //[Table("student_parent",Schema="EDU")]
  // public class student_parent
  // {
  //   [Key,DatabaseGenerated(DatabaseGeneratedOption.Identity)]
  //   public int? student_parent_id {get;set;}
  //   [MaxLength(50)]
  //   public string student_code { get; set; }
  //   public int? parent_id { get; set; }
  //   public short? parent_type_id { get; set; }
  //   public parent parent { get; set; }
  //   [JsonIgnore]
  //   public student student { get; set; }
  // }
  // public class v_student_parent
  // {
  //   [Key]
  //   public int? student_parent_id {get;set;}
  //   [MaxLength(50)]
  //   public string student_code { get; set; }
  //   public int? parent_id { get; set; }
  //   public short? parent_type_id { get; set; }
  //
  //   public string parent_type_code { get; set; }
  //   public string parent_type_name_th { get; set; }
  //   public string parent_type_name_en { get; set; }
  //
  //   public v_parent parent { get; set; }
  //   [JsonIgnore]
  //   public v_student student { get; set; }
  //   [NotMapped]
  //   public string search {get;set;}
  // }
}
