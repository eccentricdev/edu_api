using edu_api.Modules.Profiles.Databases.Models;
using Microsoft.EntityFrameworkCore;
using rsu_common_api.models;
using rsu_common_api.Modules.Profiles.Databases.Models;

namespace SeventyOneDev.Utilities
{
  public partial class Db : DbContext
  {

    public DbSet<t_student> t_student { get; set; }
    public DbSet<v_student> v_student { get; set; }

    public DbSet<t_address> t_address { get; set; }
    public DbSet<v_address> v_address { get; set; }
    public DbSet<t_student_status> t_student_status { get; set; }
    public DbSet<v_student_status> v_student_status { get; set; }
    public DbSet<t_student_restriction> t_student_restriction { get; set; }
    public DbSet<v_student_restriction> v_student_restriction { get; set; }
    public DbSet<t_student_status_restriction> t_student_status_restriction { get; set; }
    public DbSet<v_student_status_restriction> v_student_status_restriction { get; set; }

    public DbSet<t_company> t_company { get; set; }
    public DbSet<v_company> v_company { get; set; }

    public DbSet<t_address_type> t_address_type { get; set; }
    public DbSet<v_address_type> v_address_type { get; set; }
    public DbSet<t_parent_type> t_parent_type { get; set; }
    public DbSet<v_parent_type> v_parent_type { get; set; }
    public DbSet<t_student_parent> t_student_parent { get; set; }
    public DbSet<v_student_parent> v_student_parent { get; set; }
    // public DbSet<t_parent> t_parent { get; set; }
    // public DbSet<v_parent> v_parent { get; set; }

    public DbSet<t_student_address> t_student_address { get; set; }
    public DbSet<v_student_address> v_student_address { get; set; }

    public DbSet<t_student_document> t_student_document { get; set; }
    public DbSet<v_student_document> v_student_document { get; set; }

    public DbSet<t_student_log> t_student_log { get; set; }
    public DbSet<v_student_log> v_student_log { get; set; }
    //public DbSet<student_grade> student_grade { get; set; }
    //public DbSet<v_student_image> v_student_image { get; set; }
    //public DbSet<student_semester_grade> student_semester_grade { get; set; }
    
    public DbSet<t_student_card_record> t_student_card_record { get; set; }
    public DbSet<v_student_card_record> v_student_card_record { get; set; }

    public DbSet<t_student_flag> t_student_flag { get; set; }
    public DbSet<v_student_flag> v_student_flag { get; set; }

    public DbSet<t_student_flag_type> t_student_flag_type { get; set; }
    public DbSet<v_student_flag_type> v_student_flag_type { get; set; }


    private void OnProfileModelCreating(ModelBuilder builder,string schema)
    {

      builder.Entity<t_student>().ToTable("student", schema);
      builder.Entity<v_student_log>().ToView("v_student_log", schema);

      builder.Entity<t_student_log>().ToTable("student_log", schema);
      builder.Entity<v_student>().ToView("v_student", schema);

      builder.Entity<t_address>().ToTable("address", schema);
      // builder.Entity<t_address>().HasOne(a => a.company).WithOne(a => a.address)
      //   .HasForeignKey<t_address>(a => a.address_uid).OnDelete(DeleteBehavior.Restrict);
      // builder.Entity<t_address>().HasOne(a => a.student_parent).WithOne(a => a.address)
      //   .HasForeignKey<t_address>(a => a.address_uid).OnDelete(DeleteBehavior.Restrict);

      // builder.Entity<t_address>().HasMany(a => a.student_addresses).WithOne(a => a.address)
      //   .HasForeignKey(a => a.address_uid).OnDelete(DeleteBehavior.Restrict);

      builder.Entity<v_address>().ToView("v_address", schema);
      // builder.Entity<v_address>().HasOne(a => a.company).WithOne(a => a.address)
      //   .HasForeignKey<v_address>(a => a.address_uid).OnDelete(DeleteBehavior.Restrict);
      // builder.Entity<v_address>().HasOne(a => a.student_parent).WithOne(a => a.address)
      //   .HasForeignKey<v_address>(a => a.address_uid).OnDelete(DeleteBehavior.Restrict);
      // builder.Entity<v_address>().HasMany(a => a.student_addresses).WithOne(a => a.address)
      //   .HasForeignKey(a => a.address_uid).OnDelete(DeleteBehavior.Restrict);

      builder.Entity<t_student_address>().ToTable("student_address", schema);
      builder.Entity<t_student_address>().HasOne(s => s.student).WithMany(s => s.student_addresses)
        .HasForeignKey(s => s.student_uid).OnDelete(DeleteBehavior.Cascade);
      builder.Entity<v_student_address>().ToView("v_student_address", schema);
      builder.Entity<v_student_address>().HasOne(s => s.student).WithMany(s => s.student_addresses)
        .HasForeignKey(s => s.student_uid).OnDelete(DeleteBehavior.Cascade);

      // builder.Entity<t_parent>().ToTable("parent", schema);
      // builder.Entity<t_parent>().HasOne(p => p.student_parent).WithOne(p => p.parent)
      //   .HasForeignKey<t_parent>(p => p.parent_uid).OnDelete(DeleteBehavior.Cascade);
      // builder.Entity<v_parent>().ToView("v_parent", schema);
      builder.Entity<t_student_parent>().ToTable("student_parent", schema);
      // builder.Entity<t_student_parent>().HasOne(s => s.company).WithOne(s => s.student_parent)
      //   .HasForeignKey<t_company>(s => s.company_id).OnDelete(DeleteBehavior.Restrict);
      builder.Entity<t_student_parent>().HasOne(s => s.student).WithMany(s => s.student_parents)
        .HasForeignKey(s => s.student_uid).OnDelete(DeleteBehavior.Cascade);

      builder.Entity<v_student_parent>().ToView("v_student_parent", schema);
      // builder.Entity<v_student_parent>().HasOne(s => s.company).WithOne(s => s.student_parent)
      //   .HasForeignKey<v_company>(s => s.company_id).OnDelete(DeleteBehavior.Restrict);
      builder.Entity<v_student_parent>().HasOne(s => s.student).WithMany(s => s.student_parents)
        .HasForeignKey(s => s.student_uid).OnDelete(DeleteBehavior.Cascade);


      builder.Entity<t_student_status>().ToTable("student_status", schema);
      builder.Entity<v_student_status>().ToView("v_student_status", schema);

      builder.Entity<t_company>().ToTable("company", schema);
      builder.Entity<v_company>().ToView("v_company", schema);
      builder.Entity<t_student_restriction>().ToTable("student_restriction", schema);
      builder.Entity<v_student_restriction>().ToView("v_student_restriction", schema);
      builder.Entity<t_student_status_restriction>().ToTable("student_status_restriction", schema);
      //builder.Entity<t_student_status_restriction>().HasKey(s => new {s.student_status_uid, s.student_restriction_uid});
      builder.Entity<t_student_status_restriction>().HasOne(s => s.student_restriction)
        .WithMany(s => s.student_status_restrictions).HasForeignKey(s => s.student_restriction_uid)
        .OnDelete(DeleteBehavior.SetNull);
      builder.Entity<t_student_status_restriction>().HasOne(s => s.student_status)
        .WithMany(s => s.student_status_restrictions).HasForeignKey(s => s.student_status_uid)
        .OnDelete(DeleteBehavior.SetNull);
      builder.Entity<v_student_status_restriction>().ToView("v_student_status_restriction", schema);
      //builder.Entity<v_student_status_restriction>().HasKey(s => new {s.student_status_uid, s.student_restriction_uid});
      builder.Entity<v_student_status_restriction>().HasOne(s => s.student_restriction)
        .WithMany(s => s.student_status_restrictions).HasForeignKey(s => s.student_restriction_uid)
        .OnDelete(DeleteBehavior.SetNull);
      builder.Entity<v_student_status_restriction>().HasOne(s => s.student_status)
        .WithMany(s => s.student_status_restrictions).HasForeignKey(s => s.student_status_uid)
        .OnDelete(DeleteBehavior.SetNull);


      //builder.Entity<v_student_image>().ToView("v_student_image", schema);

      //builder.Entity<v_student_status>().ToView("v_student_status",schema);
      builder.Entity<t_student_document>().ToTable("student_document", schema);
      builder.Entity<t_student_document>().HasOne(s => s.student).WithMany(s => s.student_documents)
        .HasForeignKey(s => s.student_uid).OnDelete(DeleteBehavior.Cascade);
      builder.Entity<v_student_document>().ToView("v_student_document", schema);
      builder.Entity<v_student_document>().HasOne(s => s.student).WithMany(s => s.student_documents)
        .HasForeignKey(s => s.student_uid).OnDelete(DeleteBehavior.Cascade);
      builder.Entity<t_address_type>().ToTable("address_type", schema);
      builder.Entity<v_address_type>().ToView("v_address_type", schema);

      builder.Entity<t_parent_type>().ToTable("parent_type", schema);
      builder.Entity<v_parent_type>().ToView("v_parent_type", schema);
      
      builder.Entity<t_student_card_record>().ToTable("student_card_record", schema);
      builder.Entity<v_student_card_record>().ToView("v_student_card_record", schema);
      
      builder.Entity<t_student_flag>().ToTable("student_flag", schema);
      builder.Entity<t_student_flag>().HasOne(s => s.student_flag_type).WithMany(s => s.student_flags)
        .HasForeignKey(s => s.student_flag_type_uid).OnDelete(DeleteBehavior.Restrict);
      builder.Entity<v_student_flag>().ToView("v_student_flag", schema);
      builder.Entity<v_student_flag>().HasOne(s => s.student_flag_type).WithMany(s => s.student_flags)
        .HasForeignKey(s => s.student_flag_type_uid).OnDelete(DeleteBehavior.Restrict);
      
      builder.Entity<t_student_flag_type>().ToTable("student_flag_type", schema);
      builder.Entity<v_student_flag_type>().ToView("v_student_flag_type", schema);
    }
  }
}
