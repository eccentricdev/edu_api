using edu_api.Modules.Calendars.Databases.Models;
using Microsoft.EntityFrameworkCore;
using rsu_common_api.models;

namespace SeventyOneDev.Utilities
{
  public partial class Db : DbContext
  {
    public DbSet<t_academic_calendar> t_academic_calendar {get;set;}
    public DbSet<v_academic_calendar> v_academic_calendar {get;set;}
    // public DbSet<t_academic_calendar_agency> t_academic_calendar_agency {get;set;}
    // public DbSet<v_academic_calendar_agency> v_academic_calendar_agency {get;set;}
    public DbSet<t_academic_register_calendar> t_academic_register_calendar {get;set;}
    public DbSet<v_academic_register_calendar> v_academic_register_calendar {get;set;}
    public DbSet<t_academic_register_type> t_academic_register_type {get;set;}
    public DbSet<v_academic_register_type> v_academic_register_type {get;set;}
    public DbSet<t_academic_calendar_type> t_academic_calendar_type {get;set;}
    public DbSet<v_academic_calendar_type> v_academic_calendar_type {get;set;}
    public DbSet<t_academic_year> t_academic_year {get;set;}
    public DbSet<v_academic_year> v_academic_year {get;set;}
    public DbSet<t_academic_semester> t_academic_semester {get;set;}
    public DbSet<v_academic_semester> v_academic_semester {get;set;}
    public DbSet<t_semester> t_semester {get;set;}
    public DbSet<v_semester> v_semester {get;set;}
    public DbSet<t_semester_type> t_semester_type {get;set;}
    public DbSet<v_semester_type> v_semester_type {get;set;}
    public DbSet<t_calendar> t_calendar {get;set;}
    public DbSet<v_calendar> v_calendar {get;set;}

    public DbSet<t_academic_year_filter> t_academic_year_filter {get;set;}
    public DbSet<v_academic_year_filter> v_academic_year_filter {get;set;}

    public DbSet<t_academic_year_filter_education_type> t_academic_year_filter_education_type {get;set;}
    public DbSet<v_academic_year_filter_education_type> v_academic_year_filter_education_type {get;set;}

    public DbSet<t_academic_year_filter_faculty> t_academic_year_filter_faculty {get;set;}
    public DbSet<v_academic_year_filter_faculty> v_academic_year_filter_faculty {get;set;}

    public DbSet<t_academic_year_filter_curriculum> t_academic_year_filter_curriculum {get;set;}
    public DbSet<v_academic_year_filter_curriculum> v_academic_year_filter_curriculum {get;set;}


    public DbSet<t_academic_semester_filter> t_academic_semester_filter {get;set;}
    public DbSet<v_academic_semester_filter> v_academic_semester_filter {get;set;}

    public DbSet<t_academic_semester_filter_education_type> t_academic_semester_filter_education_type {get;set;}
    public DbSet<v_academic_semester_filter_education_type> v_academic_semester_filter_education_type {get;set;}

    public DbSet<t_academic_semester_filter_faculty> t_academic_semester_filter_faculty {get;set;}
    public DbSet<v_academic_semester_filter_faculty> v_academic_semester_filter_faculty {get;set;}

    public DbSet<t_academic_semester_filter_curriculum> t_academic_semester_filter_curriculum {get;set;}
    public DbSet<v_academic_semester_filter_curriculum> v_academic_semester_filter_curriculum {get;set;}
    
    public DbSet<t_academic_year_document> t_academic_year_document {get;set;}
    public DbSet<v_academic_year_document> v_academic_year_document {get;set;}
    
    public DbSet<t_academic_year_document_type> t_academic_year_document_type {get;set;}
    public DbSet<v_academic_year_document_type> v_academic_year_document_type {get;set;}

    // public DbSet<t_academic_semester_date> t_academic_semester_date {get;set;}
    // public DbSet<v_academic_semester_date> v_academic_semester_date {get;set;}
    private void OnCalendarModelCreating(ModelBuilder builder, string schema)
    {
      // builder.Entity<t_academic_calendar_agency>().ToTable("academic_calendar_agency", schema);
      // builder.Entity<t_academic_calendar_agency>().HasOne(a => a.academic_calendar)
      //   .WithMany(a => a.academic_calendar_agencies).HasForeignKey(a => a.academic_calendar_uid)
      //   .OnDelete(DeleteBehavior.Cascade);
      // builder.Entity<v_academic_calendar_agency>().ToView("v_academic_calendar_agency", schema);
      // builder.Entity<v_academic_calendar_agency>().HasOne(a => a.academic_calendar)
        // .WithMany(a => a.academic_calendar_agencies).HasForeignKey(a => a.academic_calendar_uid)
        // .OnDelete(DeleteBehavior.Cascade);
      builder.Entity<t_academic_calendar>().ToTable("academic_calendar", schema);
      // builder.Entity<t_academic_calendar>().HasOne(a => a.academic_year)
      //   .WithMany(a => a.academic_calendars).HasForeignKey(a => a.academic_year_uid)
      //   .OnDelete(DeleteBehavior.Restrict);
      builder.Entity<t_academic_calendar>().HasOne(a => a.academic_semester)
        .WithMany(a => a.academic_calendars).HasForeignKey(a => a.academic_semester_uid)
        .OnDelete(DeleteBehavior.Restrict);
      builder.Entity<t_academic_calendar>().HasOne(a => a.academic_semester_filter)
        .WithMany(a => a.academic_calendars).HasForeignKey(a => a.academic_semester_filter_uid)
        .OnDelete(DeleteBehavior.Restrict);

      builder.Entity<v_academic_calendar>().ToView("v_academic_calendar", schema);
      // builder.Entity<v_academic_calendar>().HasOne(a => a.academic_year)
      //   .WithMany(a => a.academic_calendars).HasForeignKey(a => a.academic_year_uid)
      //   .OnDelete(DeleteBehavior.Restrict);
      builder.Entity<v_academic_calendar>().HasOne(a => a.academic_semester)
        .WithMany(a => a.academic_calendars).HasForeignKey(a => a.academic_semester_uid)
        .OnDelete(DeleteBehavior.Restrict);
      builder.Entity<v_academic_calendar>().HasOne(a => a.academic_semester_filter)
        .WithMany(a => a.academic_calendars).HasForeignKey(a => a.academic_semester_filter_uid)
        .OnDelete(DeleteBehavior.Restrict);


      builder.Entity<t_academic_year>().ToTable("academic_year", schema);
      builder.Entity<v_academic_year>().ToView("v_academic_year", schema);

      builder.Entity<t_academic_semester>().ToTable("academic_semester", schema);
      builder.Entity<t_academic_semester>().HasOne(a => a.semester).WithMany(a => a.academic_semesters)
        .HasForeignKey(a => a.semester_uid).OnDelete(DeleteBehavior.Restrict);
      builder.Entity<t_academic_semester>().HasOne(a => a.academic_year).WithMany(a => a.academic_semesters)
        .HasForeignKey(a => a.academic_year_uid).OnDelete(DeleteBehavior.Cascade);
      builder.Entity<t_academic_semester>().HasOne(a => a.academic_year_filter).WithMany(a => a.academic_semesters)
        .HasForeignKey(a => a.academic_year_filter_uid).OnDelete(DeleteBehavior.Cascade);

      builder.Entity<v_academic_semester>().ToView("v_academic_semester", schema);
      builder.Entity<v_academic_semester>().HasOne(a => a.semester).WithMany(a => a.academic_semesters)
        .HasForeignKey(a => a.semester_uid).OnDelete(DeleteBehavior.Restrict);
      builder.Entity<v_academic_semester>().HasOne(a => a.academic_year).WithMany(a => a.academic_semesters)
        .HasForeignKey(a => a.academic_year_uid).OnDelete(DeleteBehavior.Cascade);
      builder.Entity<v_academic_semester>().HasOne(a => a.academic_year_filter).WithMany(a => a.academic_semesters)
        .HasForeignKey(a => a.academic_year_filter_uid).OnDelete(DeleteBehavior.Cascade);

      builder.Entity<t_semester_type>().ToTable("semester_type", schema);
      builder.Entity<v_semester_type>().ToView("v_semester_type", schema);
      builder.Entity<t_semester>().ToTable("semester", schema);
      builder.Entity<t_semester>().HasOne(s => s.semester_type).WithMany(s => s.semesters)
        .HasForeignKey(s => s.semester_type_uid).OnDelete(DeleteBehavior.Cascade);
      builder.Entity<v_semester>().ToView("v_semester", schema);
      builder.Entity<v_semester>().HasOne(s => s.semester_type).WithMany(s => s.semesters)
        .HasForeignKey(s => s.semester_type_uid).OnDelete(DeleteBehavior.Cascade);
      builder.Entity<t_academic_register_type>().ToTable("academic_register_type", schema);
      builder.Entity<v_academic_register_type>().ToView("v_academic_register_type", schema);
      builder.Entity<t_academic_calendar_type>().ToTable("academic_calendar_type", schema);
      builder.Entity<v_academic_calendar_type>().ToView("v_academic_calendar_type", schema);
      builder.Entity<t_academic_register_calendar>().ToTable("academic_register_calendar", schema);
      // builder.Entity<t_academic_register_calendar>().HasOne(a => a.academic_calendar)
      //   .WithOne(a => a.academic_register_calendar).HasForeignKey<v_academic_calendar>(a => a.academic_register_calendar_uid)
      //   .OnDelete(DeleteBehavior.Cascade);
      // builder.Entity<t_academic_register_calendar>().HasOne(a => a.academic_year)
      //   .WithMany(a => a.academic_register_calendars).HasForeignKey(a => a.academic_year_uid)
      //   .OnDelete(DeleteBehavior.Restrict);
      builder.Entity<t_academic_register_calendar>().HasOne(a => a.academic_semester)
        .WithMany(a => a.academic_register_calendars).HasForeignKey(a => a.academic_semester_uid)
        .OnDelete(DeleteBehavior.Restrict);
      builder.Entity<t_academic_register_calendar>().HasOne(a => a.academic_semester_filter)
        .WithMany(a => a.academic_register_calendars).HasForeignKey(a => a.academic_semester_filter_uid)
        .OnDelete(DeleteBehavior.Restrict);
      builder.Entity<t_academic_register_calendar>().HasOne(a => a.academic_register_type)
        .WithMany(a => a.academic_register_calendars).HasForeignKey(a => a.academic_register_type_uid)
        .OnDelete(DeleteBehavior.Restrict);

      builder.Entity<v_academic_register_calendar>().ToView("v_academic_register_calendar", schema);
      // builder.Entity<v_academic_register_calendar>().HasOne(a => a.academic_calendar)
      //   .WithOne(a => a.academic_register_calendar).HasForeignKey<v_academic_calendar>(a => a.academic_register_calendar_uid)
      //   .OnDelete(DeleteBehavior.Cascade);
      // builder.Entity<v_academic_register_calendar>().HasOne(a => a.academic_year)
      //   .WithMany(a => a.academic_register_calendars).HasForeignKey(a => a.academic_year_uid)
      //   .OnDelete(DeleteBehavior.Restrict);
      builder.Entity<v_academic_register_calendar>().HasOne(a => a.academic_semester)
        .WithMany(a => a.academic_register_calendars).HasForeignKey(a => a.academic_semester_uid)
        .OnDelete(DeleteBehavior.Restrict);
      builder.Entity<v_academic_register_calendar>().HasOne(a => a.academic_semester_filter)
        .WithMany(a => a.academic_register_calendars).HasForeignKey(a => a.academic_semester_filter_uid)
        .OnDelete(DeleteBehavior.Restrict);
      builder.Entity<v_academic_register_calendar>().HasOne(a => a.academic_register_type)
        .WithMany(a => a.academic_register_calendars).HasForeignKey(a => a.academic_register_type_uid)
        .OnDelete(DeleteBehavior.Restrict);

      builder.Entity<t_calendar>().ToTable("calendar", schema);
      builder.Entity<v_calendar>().ToView("v_calendar", schema);

      builder.Entity<t_academic_year_filter>().ToTable("academic_year_filter", schema);
      builder.Entity<t_academic_year_filter>().HasOne(a => a.academic_year).WithMany(a => a.academic_year_filters)
        .HasForeignKey(a => a.academic_year_uid).OnDelete(DeleteBehavior.Cascade);
      builder.Entity<v_academic_year_filter>().ToView("v_academic_year_filter", schema);
      builder.Entity<v_academic_year_filter>().HasOne(a => a.academic_year).WithMany(a => a.academic_year_filters)
        .HasForeignKey(a => a.academic_year_uid).OnDelete(DeleteBehavior.Cascade);

      builder.Entity<t_academic_year_filter_education_type>().ToTable("academic_year_filter_education_type", schema);
      builder.Entity<t_academic_year_filter_education_type>().HasOne(a => a.academic_year_filter)
        .WithMany(a => a.academic_year_filter_education_types).HasForeignKey(a => a.academic_year_filter_uid)
        .OnDelete(DeleteBehavior.Cascade);
      builder.Entity<v_academic_year_filter_education_type>().ToView("v_academic_year_filter_education_type", schema);
      builder.Entity<v_academic_year_filter_education_type>().HasOne(a => a.academic_year_filter)
        .WithMany(a => a.academic_year_filter_education_types).HasForeignKey(a => a.academic_year_filter_uid)
        .OnDelete(DeleteBehavior.Cascade);

      builder.Entity<t_academic_year_filter_faculty>().ToTable("academic_year_filter_faculty", schema);
      builder.Entity<t_academic_year_filter_faculty>().HasOne(a => a.academic_year_filter)
        .WithMany(a => a.academic_year_filter_faculties).HasForeignKey(a => a.academic_year_filter_uid)
        .OnDelete(DeleteBehavior.Cascade);
      builder.Entity<v_academic_year_filter_faculty>().ToView("v_academic_year_filter_faculty", schema);
      builder.Entity<v_academic_year_filter_faculty>().HasOne(a => a.academic_year_filter)
        .WithMany(a => a.academic_year_filter_faculties).HasForeignKey(a => a.academic_year_filter_uid)
        .OnDelete(DeleteBehavior.Cascade);

      builder.Entity<t_academic_year_filter_curriculum>().ToTable("academic_year_filter_curriculum", schema);
      builder.Entity<t_academic_year_filter_curriculum>().HasOne(a => a.academic_year_filter)
        .WithMany(a => a.academic_year_filter_curriculums).HasForeignKey(a => a.academic_year_filter_uid)
        .OnDelete(DeleteBehavior.Cascade);
      builder.Entity<v_academic_year_filter_curriculum>().ToView("v_academic_year_filter_curriculum", schema);
      builder.Entity<v_academic_year_filter_curriculum>().HasOne(a => a.academic_year_filter)
        .WithMany(a => a.academic_year_filter_curriculums).HasForeignKey(a => a.academic_year_filter_uid)
        .OnDelete(DeleteBehavior.Cascade);


      builder.Entity<t_academic_semester_filter>().ToTable("academic_semester_filter", schema);
      builder.Entity<t_academic_semester_filter>().HasOne(a => a.academic_semester)
        .WithMany(a => a.academic_semester_filters).HasForeignKey(a => a.academic_semester_uid)
        .OnDelete(DeleteBehavior.Cascade);
      builder.Entity<v_academic_semester_filter>().ToView("v_academic_semester_filter", schema);
      builder.Entity<v_academic_semester_filter>().HasOne(a => a.academic_semester)
        .WithMany(a => a.academic_semester_filters).HasForeignKey(a => a.academic_semester_uid)
        .OnDelete(DeleteBehavior.Cascade);


      builder.Entity<t_academic_semester_filter_education_type>().ToTable("academic_semester_filter_education_type", schema);
      builder.Entity<t_academic_semester_filter_education_type>().HasOne(a => a.academic_semester_filter)
        .WithMany(a => a.academic_semester_filter_education_types).HasForeignKey(a => a.academic_semester_filter_uid)
        .OnDelete(DeleteBehavior.Cascade);
      builder.Entity<v_academic_semester_filter_education_type>().ToView("v_academic_semester_filter_education_type", schema);
      builder.Entity<v_academic_semester_filter_education_type>().HasOne(a => a.academic_semester_filter)
        .WithMany(a => a.academic_semester_filter_education_types).HasForeignKey(a => a.academic_semester_filter_uid)
        .OnDelete(DeleteBehavior.Cascade);

      builder.Entity<t_academic_semester_filter_faculty>().ToTable("academic_semester_filter_faculty", schema);
      builder.Entity<t_academic_semester_filter_faculty>().HasOne(a => a.academic_semester_filter)
        .WithMany(a => a.academic_semester_filter_faculties).HasForeignKey(a => a.academic_semester_filter_uid)
        .OnDelete(DeleteBehavior.Cascade);
      builder.Entity<v_academic_semester_filter_faculty>().ToView("v_academic_semester_filter_faculty", schema);
      builder.Entity<v_academic_semester_filter_faculty>().HasOne(a => a.academic_semester_filter)
        .WithMany(a => a.academic_semester_filter_faculties).HasForeignKey(a => a.academic_semester_filter_uid)
        .OnDelete(DeleteBehavior.Cascade);

      builder.Entity<t_academic_semester_filter_curriculum>().ToTable("academic_semester_filter_curriculum", schema);
      builder.Entity<t_academic_semester_filter_curriculum>().HasOne(a => a.academic_semester_filter)
        .WithMany(a => a.academic_semester_filter_curriculums).HasForeignKey(a => a.academic_semester_filter_uid)
        .OnDelete(DeleteBehavior.Cascade);
      builder.Entity<v_academic_semester_filter_curriculum>().ToView("v_academic_semester_filter_curriculum", schema);
      builder.Entity<v_academic_semester_filter_curriculum>().HasOne(a => a.academic_semester_filter)
        .WithMany(a => a.academic_semester_filter_curriculums).HasForeignKey(a => a.academic_semester_filter_uid)
        .OnDelete(DeleteBehavior.Cascade);
      
      
      builder.Entity<t_academic_year_document>().ToTable("academic_year_document", schema);
      builder.Entity<v_academic_year_document>().ToView("v_academic_year_document", schema);
      
      builder.Entity<t_academic_year_document_type>().ToTable("academic_year_document_type", schema);
      builder.Entity<t_academic_year_document_type>().HasOne(t => t.academic_year_document).WithMany(t => t.academic_year_document_types)
        .HasForeignKey(t => t.academic_year_document_uid).OnDelete(DeleteBehavior.Cascade);
      builder.Entity<v_academic_year_document_type>().ToView("v_academic_year_document_type", schema);
      builder.Entity<v_academic_year_document_type>().HasOne(t => t.academic_year_document).WithMany(t => t.academic_year_document_types)
        .HasForeignKey(t => t.academic_year_document_uid).OnDelete(DeleteBehavior.Cascade);

    }
  }
}
