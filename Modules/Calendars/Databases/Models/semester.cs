using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;
using SeventyOneDev.Utilities;
using SeventyOneDev.Utilities.Attributes;

namespace edu_api.Modules.Calendars.Databases.Models
{
  public class semester : base_table
  {
    [Key] public Guid? semester_uid { get; set; }
    public string semester_code { get; set; }
    public string semester_name_th { get; set; }
    public string semester_name_en { get; set; }
    public Guid? semester_type_uid { get; set; }
    //public Guid? curriculum_uid { get; set; }
  }

  [GeneratedUidController("api/calendar/semester")]
  public class t_semester : semester
  {
    [JsonIgnore]
    public List<t_academic_semester> academic_semesters { get; set; }

    [JsonIgnore]
    public t_semester_type semester_type { get; set; }
  }

  public class v_semester : semester
  {
    [JsonIgnore]
    public List<v_academic_semester> academic_semesters { get; set; }

    [JsonIgnore]
    public v_semester_type semester_type { get; set; }
  }
}
