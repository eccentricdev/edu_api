using System;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;
using SeventyOneDev.Utilities;

namespace edu_api.Modules.Calendars.Databases.Models
{
  public class academic_year_filter_faculty:base_table
  {
    [Key]
    public Guid? academic_year_filter_faculty_uid { get; set; }
    public Guid? academic_year_filter_uid { get; set; }
    public Guid? faculty_uid { get; set; }
  }

  public class t_academic_year_filter_faculty : academic_year_filter_faculty
  {
    [JsonIgnore]
    public t_academic_year_filter academic_year_filter { get; set; }
  }

  public class v_academic_year_filter_faculty : academic_year_filter_faculty
  {
    [JsonIgnore]
    public v_academic_year_filter academic_year_filter { get; set; }
  }

}
