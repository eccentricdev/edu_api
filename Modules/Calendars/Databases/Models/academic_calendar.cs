using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;
using rsu_common_api.models;
using SeventyOneDev.Utilities;
using SeventyOneDev.Utilities.Attributes;

namespace edu_api.Modules.Calendars.Databases.Models
{
	//[Table("academic_calendar",Schema="EDU")]
      public class academic_calendar:base_table
      {
        [Key]
        public Guid? academic_calendar_uid {get;set;}
		    public Guid? academic_year_uid {get;set;}
		    public Guid? academic_semester_uid {get;set;}
		    public DateTime? from_date {get;set;}
		    public DateTime? to_date {get;set;}
		    public string description {get;set;}
		    public string remark {get;set;}
        public Guid? academic_calendar_type_uid { get; set; }
        public Guid? academic_semester_filter_uid { get; set; }
        public Guid? education_type_uid { get; set; }


	}
      [GeneratedUidController("api/calendar/academic_calendar")]
      public class t_academic_calendar : academic_calendar
      {
        // [Include]
        // public List<t_academic_calendar_agency> academic_calendar_agencies { get; set; }

        // [JsonIgnore]
        // public t_academic_register_calendar academic_register_calendar {get;set;}
        //[JsonIgnore] public t_academic_year academic_year { get; set; }
        [JsonIgnore] public t_academic_semester academic_semester { get; set; }
        [JsonIgnore] public t_academic_semester_filter academic_semester_filter { get; set; }

      }
    public class v_academic_calendar: academic_calendar
    {
	    public string academic_calendar_type_name_th {get;set;}
	    public string academic_calendar_type_name_en {get;set;}
	    public string education_type_name_th {get;set;}
	    public string education_type_name_en {get;set;}
        // [Include]
        // public List<v_academic_calendar_agency> academic_calendar_agencies { get; set; }
        // [JsonIgnore]
        // public v_academic_register_calendar academic_register_calendar {get;set;}
        //[JsonIgnore] public v_academic_year academic_year { get; set; }
        [JsonIgnore] public v_academic_semester academic_semester { get; set; }
        [JsonIgnore] public v_academic_semester_filter academic_semester_filter { get; set; }


	}

}
// CREATE TABLE "EDU"."EN_REGISTER_CALENDAR"
//    (	"CALENDER_YEAR" NUMBER(5,0) NOT NULL ENABLE,
// 	"CALENDER_SEMESTER" NUMBER(2,0) NOT NULL ENABLE,
// 	"EDUCATION_TYPE_CODE" VARCHAR2(10 CHAR) NOT NULL ENABLE,
// 	"STD_STATUS" VARCHAR2(1 CHAR) NOT NULL ENABLE,
// 	"SEMESTER_TYPE" VARCHAR2(1 CHAR) NOT NULL ENABLE,
// 	"WEB_REGISTER_DATE_FROM" TIMESTAMP (6),
// 	"WEB_REGISTER_DATE_TO" TIMESTAMP (6),
// 	"MONEY_TRANSFER_DATE_FROM" TIMESTAMP (6),
// 	"MONEY_TRANSFER_DATE_TO" TIMESTAMP (6),
// 	"COUNTER_REGISTER_DATE_FROM" TIMESTAMP (6),
// 	"COUNTER_REGISTER_DATE_TO" TIMESTAMP (6),
// 	"ADD_REGISTER_DATE_FROM" TIMESTAMP (6),
// 	"ADD_REGISTER_DATE_TO" TIMESTAMP (6),
// 	"FINE_RATE" NUMBER(6,0),
// 	"SEMESTER_START_DATE" TIMESTAMP (6),
// 	"SEMESTER_END_DATE" TIMESTAMP (6),
// 	"REC_GRADE_START_DATE" TIMESTAMP (6),
// 	"REC_GRADE_END_DATE" TIMESTAMP (6),
// 	"UPD_BY" VARCHAR2(30 CHAR),
// 	"UPD_DATE" TIMESTAMP (6),
// 	"UPD_PGM" VARCHAR2(30 CHAR),
// 	"CRE_BY" VARCHAR2(30 CHAR),
// 	"CRE_DATE" TIMESTAMP (6),
// 	"MIN_CREDIT" NUMBER(4,0),
// 	"MAX_CREDIT" NUMBER(4,0),
// 	"REC_GRADE_START_TIME" VARCHAR2(50 CHAR),
// 	"REC_GRADE_END_TIME" VARCHAR2(50 CHAR),
// 	"MAX_FINE" NUMBER(20,2),
// 	"LATE_TRANSFER_DATE_FROM" TIMESTAMP (6),
// 	"LATE_TRANSFER_DATE_TO" TIMESTAMP (6),
// 	"ADD_TRANSFER_DATE_FROM" TIMESTAMP (6),
// 	"ADD_TRANSFER_DATE_TO" TIMESTAMP (6)
