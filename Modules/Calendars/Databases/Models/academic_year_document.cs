using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using SeventyOneDev.Utilities;
using SeventyOneDev.Utilities.Attributes;

namespace edu_api.Modules.Calendars.Databases.Models
{
    public class academic_year_document : base_table
    {
        [Key] public Guid? academic_year_document_uid { get; set; }
        public Guid? from_academic_year_uid { get; set; }
        public Guid? to_academic_year_uid { get; set; }
        public bool? is_not_specified_to_academic_year { get; set; }
    }

    // [GeneratedUidController("api/calendar/academic_year_document")]
    public class t_academic_year_document : academic_year_document
    {
        [Include]
        public List<t_academic_year_document_type> academic_year_document_types { get; set; }

    }
    
    public class v_academic_year_document : academic_year_document
    {
        public string from_academic_year_code { get; set; }
        public string from_academic_year_name_th { get; set; }
        public string to_academic_year_code { get; set; }
        public string to_academic_year_name_th { get; set; }
        public int? count_doc { get; set; }
        public int? from_academic_year { get; set; }
        public int? to_academic_year { get; set; }
        
        [Include]
        public List<v_academic_year_document_type> academic_year_document_types { get; set; }
    }
}