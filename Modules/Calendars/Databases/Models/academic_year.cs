using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;
using SeventyOneDev.Utilities;
using SeventyOneDev.Utilities.Attributes;

namespace edu_api.Modules.Calendars.Databases.Models
{
  //[Table("academic_year",Schema="EDU")]
  public class academic_year : base_table
  {
    [Key] public Guid? academic_year_uid { get; set; }
    [OrderBy(true)]
    public string academic_year_code { get; set; }
    public string academic_year_name_en { get; set; }
    [Unique]
    public string academic_year_name_th { get; set; }
    public DateTime? start_date { get; set; }
    public DateTime? end_date { get; set; }
    public string remark { get; set; }
    // public Guid? college_uid { get; set; }
    // public Guid? faculty_uid { get; set; }
    // public Guid? department_uid { get; set; }
    // public Guid? curriculum_uid { get; set; }
    // public Guid? agency_uid { get; set; }
    public Guid? semester_type_uid { get; set; }


  }

  //[GeneratedUidController("api/calendar/academic_year")]
  public class t_academic_year : academic_year
  {
    [Include]
    public List<t_academic_semester> academic_semesters { get; set; }

    [Include]
    public List<t_academic_year_filter> academic_year_filters { get; set; }
  }

  public class v_academic_year : academic_year
  {
    [Include]
    public List<v_academic_semester> academic_semesters { get; set; }

    [Include]
    public List<v_academic_year_filter> academic_year_filters { get; set; }

  }
}
