using System;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;
using SeventyOneDev.Utilities;

namespace edu_api.Modules.Calendars.Databases.Models
{
  public class academic_year_filter_curriculum:base_table
  {
    [Key]
    public Guid? academic_year_filter_curriculum_uid { get; set; }
    public Guid? academic_year_filter_uid { get; set; }
    public Guid? curriculum_uid { get; set; }
  }

  public class t_academic_year_filter_curriculum : academic_year_filter_curriculum
  {
    [JsonIgnore]
    public t_academic_year_filter academic_year_filter { get; set; }
  }

  public class v_academic_year_filter_curriculum : academic_year_filter_curriculum
  {
    public string curriculum_name_th { get; set; }
    [JsonIgnore]
    public v_academic_year_filter academic_year_filter { get; set; }
  }
}
