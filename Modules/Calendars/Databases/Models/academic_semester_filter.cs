using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;
using SeventyOneDev.Utilities;

namespace edu_api.Modules.Calendars.Databases.Models
{
  public class academic_semester_filter:base_table
  {
    [Key]
    public Guid? academic_semester_filter_uid { get; set; }
    public Guid? academic_semester_uid { get; set; }
    public string academic_semester_filter_code { get; set; }
    public string academic_semester_filter_name_th { get; set; }
    public string academic_semester_filter_name_en { get; set; }
    public Guid? academic_calendar_type_uid { get; set; }
  }

  public class t_academic_semester_filter : academic_semester_filter
  {
    [JsonIgnore]
    public t_academic_semester academic_semester { get; set; }
    public List<t_academic_calendar> academic_calendars { get; set; }
    public List<t_academic_register_calendar> academic_register_calendars { get; set; }

    public List<t_academic_semester_filter_education_type> academic_semester_filter_education_types { get; set; }
    public List<t_academic_semester_filter_faculty> academic_semester_filter_faculties { get; set; }
    public List<t_academic_semester_filter_curriculum> academic_semester_filter_curriculums { get; set; }
  }

  public class v_academic_semester_filter : academic_semester_filter
  {
    [JsonIgnore]
    public v_academic_semester academic_semester { get; set; }
    public List<v_academic_calendar> academic_calendars { get; set; }
    public List<v_academic_register_calendar> academic_register_calendars { get; set; }
    public List<v_academic_semester_filter_education_type> academic_semester_filter_education_types { get; set; }
    public List<v_academic_semester_filter_faculty> academic_semester_filter_faculties { get; set; }
    public List<v_academic_semester_filter_curriculum> academic_semester_filter_curriculums { get; set; }
  }
}
