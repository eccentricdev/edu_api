using System;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;
using SeventyOneDev.Utilities;

namespace edu_api.Modules.Calendars.Databases.Models
{
  public class academic_semester_filter_faculty:base_table
  {
    [Key]
    public Guid? academic_semester_filter_faculty_uid { get; set; }
    public Guid? academic_semester_filter_uid { get; set; }
    public Guid? faculty_uid { get; set; }
  }

  public class t_academic_semester_filter_faculty : academic_semester_filter_faculty
  {
    [JsonIgnore]
    public t_academic_semester_filter academic_semester_filter { get; set; }
  }

  public class v_academic_semester_filter_faculty : academic_semester_filter_faculty
  {
    [JsonIgnore]
    public v_academic_semester_filter academic_semester_filter { get; set; }
  }
}
