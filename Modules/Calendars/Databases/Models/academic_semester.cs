using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;
using SeventyOneDev.Utilities;
using SeventyOneDev.Utilities.Attributes;

namespace edu_api.Modules.Calendars.Databases.Models
{
  public class academic_semester : base_table
  {
    [Key] public Guid? academic_semester_uid { get; set; }
    public Guid? academic_year_uid { get; set; }
    public Guid? semester_uid { get; set; }
    public string semester_name_th { get; set; }
    public string semester_name_en { get; set; }
    public string academic_semester_code { get; set; }
    public DateTime? semester_start_date { get; set; }
    public DateTime? semester_end_date { get; set; }
    public bool? is_student_status_update { get; set; }
    public int? semester_order { get; set; }
    public Guid? academic_year_filter_uid { get; set; }
  }

  //[GeneratedUidController("api/calendar/academic_semester")]
  public class t_academic_semester : academic_semester
  {
    [JsonIgnore] public t_semester semester { get; set; }
    [JsonIgnore] public t_academic_year academic_year { get; set; }
    [JsonIgnore] public t_academic_year_filter academic_year_filter { get; set; }

    public List<t_academic_register_calendar> academic_register_calendars { get; set; }
    public List<t_academic_calendar> academic_calendars { get; set; }
    public List<t_academic_semester_filter> academic_semester_filters { get; set; }
  }

  public class v_academic_semester : academic_semester
  {
    [JsonIgnore] public v_semester semester { get; set; }
    [JsonIgnore] public v_academic_year academic_year { get; set; }
    public List<v_academic_register_calendar> academic_register_calendars { get; set; }
    public List<v_academic_calendar> academic_calendars { get; set; }

    [JsonIgnore] public v_academic_year_filter academic_year_filter { get; set; }
    public List<v_academic_semester_filter> academic_semester_filters { get; set; }
  }
}
