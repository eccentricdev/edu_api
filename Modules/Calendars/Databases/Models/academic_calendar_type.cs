using System;
using System.ComponentModel.DataAnnotations;
using SeventyOneDev.Utilities;
using SeventyOneDev.Utilities.Attributes;

namespace edu_api.Modules.Calendars.Databases.Models
{
  public class academic_calendar_type:base_table
  {
    [Key] public Guid? academic_calendar_type_uid { get; set; }
    public string academic_calendar_type_code { get; set; }
    public string academic_calendar_type_name_th { get; set; }
    public string academic_calendar_type_name_en { get; set; }
    public string remark { get; set; }
    //public int? display_order { get; set; }
  }
  [GeneratedUidController("api/calendar/calendar_type")]
  public class t_academic_calendar_type : academic_calendar_type
  {

  }

  public class v_academic_calendar_type : academic_calendar_type
  {

  }
}
