using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;
using SeventyOneDev.Utilities;
using SeventyOneDev.Utilities.Attributes;

namespace edu_api.Modules.Calendars.Databases.Models
{
  public class semester_type : base_table
  {
    [Key] public Guid? semester_type_uid { get; set; }
    public string semester_type_code { get; set; }
    public string semester_type_name_th { get; set; }
    public string semester_type_name_en { get; set; }
    //public Guid? curriculum_uid { get; set; }
  }

  [GeneratedUidController("api/calendar/semester_type")]
  public class t_semester_type : semester_type
  {
    public List<t_semester> semesters { get; set; }
  }

  public class v_semester_type : semester_type
  {
    public List<v_semester> semesters { get; set; }
  }
}
