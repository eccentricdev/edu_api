using System;
using System.ComponentModel.DataAnnotations;
using SeventyOneDev.Utilities;
using SeventyOneDev.Utilities.Attributes;

namespace edu_api.Modules.Calendars.Databases.Models
{
  public class calendar:base_table
  {
    [Key]
    public Guid? calendar_uid { get; set; }
    public string calendar_code { get; set; }
    public string calendar_name_th { get; set; }
    public string calendar_name_en { get; set; }
    public DateTime? from_date { get; set; }
    public DateTime? to_date { get; set; }
  }
  [GeneratedUidController("api/calendar/calendar")]
  public class t_calendar : calendar
  {

  }
 
  public class v_calendar : calendar
  {

  }
}
