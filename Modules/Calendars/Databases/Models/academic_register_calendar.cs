using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;
using SeventyOneDev.Utilities;
using SeventyOneDev.Utilities.Attributes;

namespace edu_api.Modules.Calendars.Databases.Models
{
  //[Table("academic_register_calendar",Schema="EDU")]
  public class academic_register_calendar : base_table
  {
    [Key] public Guid? academic_register_calendar_uid { get; set; }
    public Guid? academic_year_uid { get; set; }
    public Guid? academic_semester_uid { get; set; }
    public Guid? education_type_uid { get; set; }
    //public Guid? academic_calendar_uid { get; set; }
    public DateTime? from_date { get; set; }
    public DateTime? to_date { get; set; }

    //public int? education_type_id { get; set; }

    // public int? faculty_id {get;set;}
    // public int? major_id {get;set;}
    public Guid? student_status_uid { get; set; }

    public DateTime? last_payment_date { get; set; }
    public string description { get; set; }
    public decimal? fine_rate_amount { get; set; }
    public decimal? max_fine_amount { get; set; }
    [Column(TypeName = "decimal(5,2)")]
    public decimal? min_credit { get; set; }
    [Column(TypeName = "decimal(5,2)")]
    public decimal? max_credit { get; set; }
    public Guid? academic_register_type_uid { get; set; }
    public string remark { get; set; }
    public Guid? academic_semester_filter_uid { get; set; }
    [Column(TypeName = "decimal(5,2)")]
    public decimal? cash_back { get; set; }
    public bool? after_withdraw_status { get; set; }
    public Guid? semester_uid { get; set; }
  }
  [GeneratedUidController("api/calendar/academic_register_calendar")]
  public class t_academic_register_calendar : academic_register_calendar
  {
    //[JsonIgnore] public t_academic_calendar academic_calendar { get; set; }
    //[JsonIgnore] public t_academic_year academic_year { get; set; }
    [JsonIgnore] public t_academic_semester academic_semester { get; set; }
    [JsonIgnore] public t_academic_register_type academic_register_type { get; set; }
    [JsonIgnore] public t_academic_semester_filter academic_semester_filter { get; set; }

  }

  public class v_academic_register_calendar : academic_register_calendar
  {
    //[JsonIgnore] public v_academic_calendar academic_calendar { get; set; }
    //[JsonIgnore] public v_academic_year academic_year { get; set; }
    public string academic_year_code { get; set; }
    public string semester_code { get; set; }
    
    public string education_type_name_th {get;set;}
    public string education_type_name_en {get;set;}
    public string student_status_name_th {get;set;}
    public string student_status_name_en {get;set;}
    public string academic_register_type_name_th {get;set;}
    public string academic_register_type_name_en {get;set;}
    [JsonIgnore] public v_academic_semester academic_semester { get; set; }
    [JsonIgnore] public v_academic_register_type academic_register_type { get; set; }

    [JsonIgnore] public v_academic_semester_filter academic_semester_filter { get; set; }


    // public string academic_year_code {get;set;}
    // public string academic_year_name_th {get;set;}
    // public string academic_year_name_en {get;set;}
    // public string academic_year_short_name_th {get;set;}
    // public string academic_year_short_name_en {get;set;}
    //
    // public string academic_semester_code {get;set;}
    // public string academic_semester_name_th {get;set;}
    // public string academic_semester_name_en {get;set;}
    // public string academic_semester_short_name_th {get;set;}
    // public string academic_semester_short_name_en {get;set;}
    // public string education_type_code {get;set;}
    // public string education_type_name_th {get;set;}
    // public string education_type_name_en {get;set;}
    //   public string student_status_code {get;set;}
    // public string student_status_name_th {get;set;}
    // public string student_status_name_en {get;set;}
    // public string academic_register_type_code {get;set;}
    // public string academic_register_type_name_th {get;set;}
    // public string academic_register_type_name_en {get;set;}
    // public string academic_register_type_short_name_th {get;set;}
    // public string academic_register_type_short_name_en {get;set;}
    // [NotMapped]
    // public string search {get;set;}
    //public academic_calendar academicCalendar {get;set;}
  }
}
// CREATE TABLE "EDU"."EN_REGISTER_CALENDAR"
//    (	"CALENDER_YEAR" NUMBER(5,0) NOT NULL ENABLE,
// 	"CALENDER_SEMESTER" NUMBER(2,0) NOT NULL ENABLE,
// 	"EDUCATION_TYPE_CODE" VARCHAR2(10 CHAR) NOT NULL ENABLE,
// 	"STD_STATUS" VARCHAR2(1 CHAR) NOT NULL ENABLE,
// 	"SEMESTER_TYPE" VARCHAR2(1 CHAR) NOT NULL ENABLE,
// 	"WEB_REGISTER_DATE_FROM" TIMESTAMP (6),
// 	"WEB_REGISTER_DATE_TO" TIMESTAMP (6),
// 	"MONEY_TRANSFER_DATE_FROM" TIMESTAMP (6),
// 	"MONEY_TRANSFER_DATE_TO" TIMESTAMP (6),
// 	"COUNTER_REGISTER_DATE_FROM" TIMESTAMP (6),
// 	"COUNTER_REGISTER_DATE_TO" TIMESTAMP (6),
// 	"ADD_REGISTER_DATE_FROM" TIMESTAMP (6),
// 	"ADD_REGISTER_DATE_TO" TIMESTAMP (6),
// 	"FINE_RATE" NUMBER(6,0),
// 	"SEMESTER_START_DATE" TIMESTAMP (6),
// 	"SEMESTER_END_DATE" TIMESTAMP (6),
// 	"REC_GRADE_START_DATE" TIMESTAMP (6),
// 	"REC_GRADE_END_DATE" TIMESTAMP (6),
// 	"UPD_BY" VARCHAR2(30 CHAR),
// 	"UPD_DATE" TIMESTAMP (6),
// 	"UPD_PGM" VARCHAR2(30 CHAR),
// 	"CRE_BY" VARCHAR2(30 CHAR),
// 	"CRE_DATE" TIMESTAMP (6),
// 	"MIN_CREDIT" NUMBER(4,0),
// 	"MAX_CREDIT" NUMBER(4,0),
// 	"REC_GRADE_START_TIME" VARCHAR2(50 CHAR),
// 	"REC_GRADE_END_TIME" VARCHAR2(50 CHAR),
// 	"MAX_FINE" NUMBER(20,2),
// 	"LATE_TRANSFER_DATE_FROM" TIMESTAMP (6),
// 	"LATE_TRANSFER_DATE_TO" TIMESTAMP (6),
// 	"ADD_TRANSFER_DATE_FROM" TIMESTAMP (6),
// 	"ADD_TRANSFER_DATE_TO" TIMESTAMP (6)
