using System;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;
using SeventyOneDev.Utilities;

namespace edu_api.Modules.Calendars.Databases.Models
{
  public class academic_semester_filter_curriculum:base_table
  {
    [Key]
    public Guid? academic_semester_filter_curriculum_uid { get; set; }
    public Guid? academic_semester_filter_uid { get; set; }
    public Guid? curriculum_uid { get; set; }
  }

  public class t_academic_semester_filter_curriculum : academic_semester_filter_curriculum
  {
    [JsonIgnore]
    public t_academic_semester_filter academic_semester_filter { get; set; }
  }

  public class v_academic_semester_filter_curriculum : academic_semester_filter_curriculum
  {
    public string curriculum_code {get;set;}
    public string curriculum_name_th {get;set;}
    public string curriculum_name_en {get;set;}
    [JsonIgnore]
    public v_academic_semester_filter academic_semester_filter { get; set; }
  }
}
