using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;
using rsu_common_api.models;
using SeventyOneDev.Utilities;
using SeventyOneDev.Utilities.Attributes;

namespace edu_api.Modules.Calendars.Databases.Models
{
  public class academic_register_type : base_table
  {
    [Key] public Guid? academic_register_type_uid { get; set; }

    public string academic_register_type_code { get; set; }
    public string academic_register_type_name_th { get; set; }
    public string academic_register_type_name_en { get; set; }
    public string remark { get; set; }
    public bool? has_fine { get; set; }
    public int? display_order { get; set; }

  }
  [GeneratedUidController("api/calendar/academic_register_type")]
  public class t_academic_register_type : academic_register_type
  {

    [JsonIgnore] public List<t_academic_register_calendar> academic_register_calendars { get; set; }
    // public t_academic_register_type() {}
    // public t_academic_register_type(
    //   int academic_register_type_id,
    //   string academic_register_type_code,
    //   string academic_register_type_short_name_th,
    //   string academic_register_type_short_name_en,
    //   string academic_register_type_name_th,
    //   string academic_register_type_name_en,
    //   string remark)
    // {
    //   this.academic_register_type_id = academic_register_type_id;
    //   this.academic_register_type_code = academic_register_type_code;
    //   this.academic_register_type_short_name_th = academic_register_type_short_name_th;
    //   this.academic_register_type_short_name_en = academic_register_type_short_name_en;
    //   this.academic_register_type_name_th = academic_register_type_name_th;
    //   this.academic_register_type_name_en = academic_register_type_name_en;
    //   this.remark = remark;
    //   // this.status = "A";
    //   // this.created_by = "SYSTEM";
    //   // this.update_by = "SYSTEM";
    //   // this.create_datetime = DateTime.Now;
    //   // this.update_datetime = DateTime.Now;
    // }
  }

  public class v_academic_register_type : academic_register_type
  {
    [JsonIgnore] public List<v_academic_register_calendar> academic_register_calendars { get; set; }

  }
}
