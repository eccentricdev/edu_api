using System;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;
using SeventyOneDev.Utilities;

namespace edu_api.Modules.Calendars.Databases.Models
{
  public class academic_semester_filter_education_type:base_table
  {
    [Key]
    public Guid? academic_semester_filter_education_type_uid { get; set; }
    public Guid? academic_semester_filter_uid { get; set; }
    public Guid? education_type_uid { get; set; }
  }

  public class t_academic_semester_filter_education_type : academic_semester_filter_education_type
  {
    [JsonIgnore]
    public t_academic_semester_filter academic_semester_filter { get; set; }
  }

  public class v_academic_semester_filter_education_type : academic_semester_filter_education_type
  {
    [JsonIgnore]
    public v_academic_semester_filter academic_semester_filter { get; set; }
  }
}
