using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;
using SeventyOneDev.Utilities;

namespace edu_api.Modules.Calendars.Databases.Models
{
  public class academic_year_filter:base_table
  {
    [Key] public Guid? academic_year_filter_uid { get; set; }
    public Guid? academic_year_uid { get; set; }
    public string academic_year_filter_code { get; set; }
    public string academic_year_filter_name_th { get; set; }
    public string academic_year_filter_name_en { get; set; }
    public DateTime? start_date { get; set; }
    public DateTime? end_date { get; set; }
    public string remark { get; set; }
    public Guid? semester_type_uid { get; set; }
  }

  public class t_academic_year_filter : academic_year_filter
  {
    [JsonIgnore]
    public t_academic_year academic_year { get; set; }
    public List<t_academic_year_filter_education_type> academic_year_filter_education_types { get; set; }
    public List<t_academic_year_filter_faculty> academic_year_filter_faculties { get; set; }
    public List<t_academic_year_filter_curriculum> academic_year_filter_curriculums { get; set; }
    public List<t_academic_semester> academic_semesters { get; set; }
  }

  public class v_academic_year_filter : academic_year_filter
  {
    [JsonIgnore]
    public v_academic_year academic_year { get; set; }
    public List<v_academic_year_filter_education_type> academic_year_filter_education_types { get; set; }
    public List<v_academic_year_filter_faculty> academic_year_filter_faculties { get; set; }
    public List<v_academic_year_filter_curriculum> academic_year_filter_curriculums { get; set; }
    public List<v_academic_semester> academic_semesters { get; set; }
  }
}
