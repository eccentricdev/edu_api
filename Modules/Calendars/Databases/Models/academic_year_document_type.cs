using System;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;
using SeventyOneDev.Utilities;

namespace edu_api.Modules.Calendars.Databases.Models
{
    public class academic_year_document_type : base_table
    {
        [Key] public Guid? academic_year_document_type_uid { get; set; }
        public Guid? academic_year_document_uid { get; set; }
        public int? document_type_id {get;set;}
    }
    
    public class t_academic_year_document_type : academic_year_document_type 
    {
        [JsonIgnore]
        public t_academic_year_document academic_year_document { get; set; }
    }
    
    public class v_academic_year_document_type : academic_year_document_type 
    {
        public string document_type_name  {get;set;}
        [JsonIgnore]
        public v_academic_year_document academic_year_document { get; set; }
    }
}