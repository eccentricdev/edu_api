//using edu_api.Modules.Calendars.Services;

using edu_api.Modules.Calendars.Services;
using Microsoft.Extensions.DependencyInjection;

namespace edu_api.Modules.Calendars.Configures
{
  public static class CalendarCollectionExtension
  {
    public static IServiceCollection AddCalendarServices(this IServiceCollection services)
    {
      services.AddScoped<AcademicYearService>();
      services.AddScoped<AcademicSemesterService>();
      return services;
    }
  }
}
