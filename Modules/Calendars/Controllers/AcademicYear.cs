using System;
using System.ComponentModel.DataAnnotations;
using System.Security.Claims;
using System.Threading.Tasks;
using edu_api.Modules.Calendars.Databases.Models;
using edu_api.Modules.Calendars.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SeventyOneDev.Utilities;

namespace edu_api.Modules.Calendars.Controllers
{
  [Route("api/calendar/academic_year", Name = "academic_year")]
  public class AcademicYearController:BaseUidController<t_academic_year,v_academic_year>
  {
    private readonly AcademicYearService _academicYearService;
    public AcademicYearController(AcademicYearService entityUidService) : base(entityUidService)
    {
      _academicYearService = entityUidService;
    }

    [Authorize]
    [HttpPost("academic_year_copy/{_uid}/{add_year}")]
    public async Task<ActionResult<v_academic_year>> GetAcademicSemesterNoFilters([FromRoute, Required] Guid _uid,
      [FromRoute, Required] int add_year)
    {
      try
      {
        var result = await _academicYearService.Copy(_uid, add_year, User.Identity as ClaimsIdentity);
        return result == null ? NotFound() : Ok(result);
      }
      catch (Exception ex)
      {
        Console.WriteLine(ex);
        return BadRequest(ErrorMessage.Get(ex.Message));
      }
    }
  }
}
