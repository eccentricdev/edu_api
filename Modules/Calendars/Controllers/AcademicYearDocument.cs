using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using edu_api.Modules.Calendars.Databases.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SeventyOneDev.Utilities;

namespace edu_api.Modules.Calendars.Controllers
{
    [ApiController]
    [Route("api/calendar/academic_year_document", Name = "academic_year_document")]
    public class AcademicYearDocumentController : BaseUidController<t_academic_year_document, v_academic_year_document >
    {
        private readonly Db _db;

        public AcademicYearDocumentController(EntityUidService<t_academic_year_document, v_academic_year_document> entityUidService,Db db) : base(entityUidService)
        {
            _db = db;
        }

        [HttpGet("search/{year}")]
        public async Task<ActionResult<List<v_academic_year_document>>> NewEntityUid([FromRoute] string year)
        {
            var academicYearDocuments = await _db.v_academic_year_document.AsNoTracking()
                .Include(w => w.academic_year_document_types)
                .Where(w => w.to_academic_year_name_th == null && w.from_academic_year_name_th == year).ToListAsync();
            
            if (academicYearDocuments.Count == 0)
            {
                academicYearDocuments = await _db.v_academic_year_document.AsNoTracking()
                    .Include(w => w.academic_year_document_types)
                    .Where(w => w.to_academic_year_name_th != null 
                            && w.from_academic_year <= Int64.Parse(year)
                            && w.to_academic_year >= Int64.Parse(year)).ToListAsync();
            }
            return Ok(academicYearDocuments);
        }
    }
}