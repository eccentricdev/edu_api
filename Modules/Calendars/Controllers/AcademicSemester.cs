using System;
using System.ComponentModel.DataAnnotations;
using System.Security.Claims;
using System.Threading.Tasks;
using edu_api.Modules.Calendars.Databases.Models;
using edu_api.Modules.Calendars.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SeventyOneDev.Utilities;

namespace edu_api.Modules.Calendars.Controllers
{
  [Route("api/calendar/academic_semester", Name = "academic_semester")]
  public class AcademicSemesterController:BaseUidController<t_academic_semester,v_academic_semester>
  {
    private readonly AcademicSemesterService _academicSemesterService;
    public AcademicSemesterController(AcademicSemesterService entityUidService) : base(entityUidService)
    {
      _academicSemesterService = entityUidService;
    }
    [HttpGet("academic_semester_no_filters/{_uid}")]
    public async Task<ActionResult<v_academic_semester>> GetAcademicSemesterNoFilters([FromRoute, Required] Guid _uid)
    {
      var result = await _academicSemesterService.GetAcademicSemesterNoFilters(_uid);
      return result == null ? NotFound() : Ok(result);
    }

    [HttpGet("academic_semester_filters/{_uid}")]
    public async Task<ActionResult<v_academic_semester>> GetAcademicSemesterFilters([FromRoute, Required] Guid _uid)
    {
      var result = await _academicSemesterService.GetAcademicSemesterFilters(_uid);
      return result == null ? NotFound() : Ok(result);
    }

    [Authorize]
    [HttpPut("academic_semester_filters")]
    public async Task<ActionResult<v_academic_semester>> updateAcademicSemesterFilters([FromBody] t_academic_semester entity)
    {
      try
      {
        await _academicSemesterService.UpdateAcademicSemesterFilters(entity, User.Identity as ClaimsIdentity);
        return NoContent();
      }
      catch (Exception ex)
      {
        Console.WriteLine("Update failed: " + ex.Message);
        return BadRequest(ErrorMessage.Get(ex.Message));
      }
    }

    [Authorize]
    [HttpPut("academic_semester_no_filters")]
    public async Task<ActionResult<v_academic_semester>> updateAcademicSemesterNoFilters([FromBody] t_academic_semester entity)
    {
      try
      {
        await _academicSemesterService.UpdateAcademicSemesterNoFilters(entity, User.Identity as ClaimsIdentity);
        return NoContent();
      }
      catch (Exception ex)
      {
        Console.WriteLine("Update failed: " + ex.Message);
        return BadRequest(ErrorMessage.Get(ex.Message));
      }
    }
  }
}
