using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using edu_api.Modules.Calendars.Databases.Models;
using Microsoft.EntityFrameworkCore;
using SeventyOneDev.Utilities;

namespace edu_api.Modules.Calendars.Services
{
  public class AcademicYearService:EntityUidService<t_academic_year,v_academic_year>
  {
    private readonly Db _context;
    public AcademicYearService(Db context) : base(context)
    {
      _context = context;
    }

    public override async Task<v_academic_year> GetEntity(Guid uid, List<Guid?> agencies = null, string userName = null)
    {
      //return base.GetEntity(uid, agencies, userName);
      return await _context.v_academic_year.AsNoTracking()
        .Include(a => a.academic_semesters)
        .Include(a => a.academic_year_filters).ThenInclude(a=>a.academic_semesters)
        .Include(a => a.academic_year_filters).ThenInclude(a=>a.academic_year_filter_curriculums)
        .Include(a => a.academic_year_filters).ThenInclude(a=>a.academic_year_filter_education_types)
        .Include(a => a.academic_year_filters).ThenInclude(a=>a.academic_year_filter_faculties)
        .FirstOrDefaultAsync(a => a.academic_year_uid == uid);
    }
    
    public  async Task<t_academic_year> Copy(Guid uid,int add_year,  ClaimsIdentity claimsIdentity)
    {
      //return base.GetEntity(uid, agencies, userName);
      var current_academic_year = await _context.t_academic_year.AsNoTracking()
        .Include(a => a.academic_semesters)
        .Include(a => a.academic_year_filters).ThenInclude(a=>a.academic_semesters)
        .Include(a => a.academic_year_filters).ThenInclude(a=>a.academic_year_filter_curriculums)
        .Include(a => a.academic_year_filters).ThenInclude(a=>a.academic_year_filter_education_types)
        .Include(a => a.academic_year_filters).ThenInclude(a=>a.academic_year_filter_faculties)
        .FirstOrDefaultAsync(a => a.academic_year_uid == uid);
     
      current_academic_year.academic_year_uid = null;
      current_academic_year.academic_year_code = (Convert.ToInt32(current_academic_year.academic_year_code)+ add_year).ToString();
      current_academic_year.academic_year_name_th = (Convert.ToInt32(current_academic_year.academic_year_name_th)+ add_year).ToString();
      current_academic_year.academic_year_name_en = (Convert.ToInt32(current_academic_year.academic_year_name_en)+ add_year).ToString();
      current_academic_year.start_date=current_academic_year?.start_date?.AddYears(add_year);
      current_academic_year.end_date=current_academic_year?.end_date?.AddYears(add_year);
      foreach (var academicYearFilter in current_academic_year.academic_year_filters)
      {
        academicYearFilter.academic_year_uid = null;
        academicYearFilter.academic_year_filter_uid = null;
        foreach (var academicYearFilterCurriculum in academicYearFilter.academic_year_filter_curriculums)
        {
          academicYearFilterCurriculum.academic_year_filter_uid = null;
          academicYearFilterCurriculum.academic_year_filter_curriculum_uid = null;

        }
        foreach (var academicYearFilterEducationType in academicYearFilter.academic_year_filter_education_types)
        {
          academicYearFilterEducationType.academic_year_filter_uid = null;
          academicYearFilterEducationType.academic_year_filter_education_type_uid = null;
       
        }
        foreach (var academicYearFilterFaculties in academicYearFilter.academic_year_filter_faculties)
        {
          academicYearFilterFaculties.academic_year_filter_uid = null;
          academicYearFilterFaculties.academic_year_filter_faculty_uid = null;

        }
        foreach (var academicSemester in academicYearFilter.academic_semesters)
      {
        var academic_semester = await _context.t_academic_semester
        .AsNoTracking()
        .Include(a => a.academic_calendars)
        .Include(a => a.academic_register_calendars)
        .Include(a => a.academic_semester_filters).ThenInclude(a => a.academic_calendars)
        .Include(a => a.academic_semester_filters).ThenInclude(a => a.academic_register_calendars)
        .Include(a => a.academic_semester_filters).ThenInclude(a => a.academic_semester_filter_curriculums)
        .Include(a => a.academic_semester_filters).ThenInclude(a => a.academic_semester_filter_education_types)
        .Include(a => a.academic_semester_filters).ThenInclude(a => a.academic_semester_filter_faculties)
        .FirstOrDefaultAsync(a => a.academic_semester_uid == academicSemester.academic_semester_uid);

        academicSemester.academic_semester_uid = null;
        academicSemester.academic_year_uid = null;
        academicSemester.academic_year_filter_uid = null;
        academicSemester.semester_start_date=academicSemester.semester_start_date?.AddYears(add_year);
        academicSemester.semester_end_date=academicSemester.semester_end_date?.AddYears(add_year);
        if (academic_semester.academic_calendars.Count>0)
        {
          foreach (var academicCalendar in academic_semester.academic_calendars)
          {
            academicCalendar.academic_year_uid = null;
            academicCalendar.academic_semester_uid = null;
            academicCalendar.academic_semester_filter_uid = null;
            academicCalendar.academic_calendar_uid = null;
            academicCalendar.from_date = academicCalendar.from_date?.AddYears(add_year);
            academicCalendar.to_date = academicCalendar.to_date?.AddYears(add_year);
            // academicCalendar.from_date = 
          }
        }
        if (academic_semester.academic_register_calendars.Count>0)
        {
          foreach (var academicRegisterCalendar in academic_semester.academic_register_calendars)
          {
            academicRegisterCalendar.academic_year_uid = null;
            academicRegisterCalendar.academic_semester_uid = null;
            academicRegisterCalendar.academic_semester_filter_uid = null;
            academicRegisterCalendar.academic_register_calendar_uid = null;
            academicRegisterCalendar.from_date = academicRegisterCalendar.from_date?.AddYears(add_year);
            academicRegisterCalendar.to_date = academicRegisterCalendar.to_date?.AddYears(add_year);
          }
        }
        if (academic_semester.academic_semester_filters.Count>0)
        {
           foreach (var academicSemesterFilter in academic_semester.academic_semester_filters)
        {
          academicSemesterFilter.academic_semester_filter_uid = null;
          academicSemesterFilter.academic_semester_uid = null;

          if (academicSemesterFilter.academic_calendars.Count>0)
          {
            foreach (var academicCalendar in academicSemesterFilter.academic_calendars)
            {
              academicCalendar.academic_semester_filter_uid = null;
              academicCalendar.academic_semester_uid = null;
              academicCalendar.academic_year_uid = null;
              academicCalendar.academic_semester_filter_uid = null;
              academicCalendar.academic_calendar_uid = null;
              academicCalendar.from_date = academicCalendar.from_date?.AddYears(add_year);
              academicCalendar.to_date = academicCalendar.to_date?.AddYears(add_year);
            }
          }

          if (academicSemesterFilter.academic_register_calendars.Count>0)
          {
            foreach (var academicRegisterCalendar in academicSemesterFilter.academic_register_calendars)
            {
              academicRegisterCalendar.academic_register_calendar_uid = null;
              academicRegisterCalendar.academic_semester_uid = null;
              academicRegisterCalendar.academic_year_uid = null;
              academicRegisterCalendar.academic_semester_filter_uid = null;
              academicRegisterCalendar.from_date = academicRegisterCalendar.from_date?.AddYears(add_year);
              academicRegisterCalendar.to_date = academicRegisterCalendar.to_date?.AddYears(add_year);
            }
          }
        
          if (academicSemesterFilter.academic_semester_filter_curriculums.Count>0)
          {
            foreach (var academicSemesterFilterCurriculum in academicSemesterFilter.academic_semester_filter_curriculums)
            {
              academicSemesterFilterCurriculum.academic_semester_filter_curriculum_uid = null;
              academicSemesterFilterCurriculum.academic_semester_filter_uid = null;
            }
          }
        
          if (academicSemesterFilter.academic_semester_filter_education_types.Count>0)
          {
            foreach (var academicSemesterFilterEducationType in academicSemesterFilter.academic_semester_filter_education_types)
            {
              academicSemesterFilterEducationType.academic_semester_filter_education_type_uid = null;
              academicSemesterFilterEducationType.academic_semester_filter_uid = null;
            }
          }
        
          if (academicSemesterFilter.academic_semester_filter_faculties.Count>0)
          {
            foreach (var academicSemesterFilterFaculty in academicSemesterFilter.academic_semester_filter_faculties)
            {
              academicSemesterFilterFaculty.academic_semester_filter_faculty_uid = null;
              academicSemesterFilterFaculty.academic_semester_filter_uid = null;
              
            }
          }
        }
        }
        // foreach (var academicCalendar in academic_semester.academic_calendars)
        // {
        //   academicCalendar.academic_year_uid = null;
        //   academicCalendar.academic_semester_uid = null;
        // }
        // foreach (var academicSemesterFilter in academic_semester.academic_semester_filters)
        // {
        //   academicSemesterFilter.academic_semester_filter_uid = null;
        //   academicSemesterFilter.academic_semester_uid = null;
        //
        //   foreach (var academicCalendar in academicSemesterFilter.academic_calendars)
        //   {
        //     academicCalendar.academic_semester_filter_uid = null;
        //     academicCalendar.academic_semester_uid = null;
        //     academicCalendar.academic_year_uid = null;
        //     academicCalendar.academic_semester_filter_uid = null;
        //   }
        //
        //   foreach (var academicRegisterCalendar in academicSemesterFilter.academic_register_calendars)
        //   {
        //     academicRegisterCalendar.academic_year_uid = null;
        //     academicRegisterCalendar.academic_semester_uid = null;
        //     academicRegisterCalendar.academic_semester_filter_uid = null;
        //     academicRegisterCalendar.academic_register_calendar_uid = null;
        //   }
        //   foreach (var academicSemesterFilterCurriculum in academicSemesterFilter.academic_semester_filter_curriculums)
        //   {
        //     academicSemesterFilterCurriculum.academic_semester_filter_curriculum_uid = null;
        //     academicSemesterFilterCurriculum.academic_semester_filter_uid = null;
        //
        //   }
        //   foreach (var academicSemesterFilterEducationType in academicSemesterFilter.academic_semester_filter_education_types)
        //   {
        //     academicSemesterFilterEducationType.academic_semester_filter_education_type_uid = null;
        //     academicSemesterFilterEducationType.academic_semester_filter_uid = null;
        //   }
        //   foreach (var academicSemesterFilterFaculty in academicSemesterFilter.academic_semester_filter_faculties)
        //   {
        //     academicSemesterFilterFaculty.academic_semester_filter_faculty_uid = null;
        //     academicSemesterFilterFaculty.academic_semester_filter_uid = null;
        //   }
        // }
      }
      }

      var new_academic_semester = new List<t_academic_semester>();
      
      foreach (var academicSemester in current_academic_year.academic_semesters)
      {
        var academic_semester = await _context.t_academic_semester
        .AsNoTracking()
        .Include(a => a.academic_calendars)
        .Include(a => a.academic_register_calendars)
        .Include(a => a.academic_semester_filters).ThenInclude(a => a.academic_calendars)
        .Include(a => a.academic_semester_filters).ThenInclude(a => a.academic_register_calendars)
        .Include(a => a.academic_semester_filters).ThenInclude(a => a.academic_semester_filter_curriculums)
        .Include(a => a.academic_semester_filters).ThenInclude(a => a.academic_semester_filter_education_types)
        .Include(a => a.academic_semester_filters).ThenInclude(a => a.academic_semester_filter_faculties)
        .FirstOrDefaultAsync(a => a.academic_semester_uid == academicSemester.academic_semester_uid);

        academic_semester.academic_semester_uid = null;
        academic_semester.academic_year_uid = null;
        academic_semester.academic_year_filter_uid = null;
        academicSemester.semester_start_date=academicSemester?.semester_start_date?.AddYears(add_year);
        academicSemester.semester_end_date=academicSemester?.semester_end_date?.AddYears(add_year);
        if (academic_semester.academic_calendars.Count>0)
        {
          foreach (var academicCalendar in academic_semester.academic_calendars)
          {
            academicCalendar.academic_year_uid = null;
            academicCalendar.academic_semester_uid = null;
            academicCalendar.academic_semester_filter_uid = null;
            academicCalendar.academic_calendar_uid = null;
            academicCalendar.from_date = academicCalendar?.from_date?.AddYears(add_year);
            academicCalendar.to_date = academicCalendar?.to_date?.AddYears(add_year);
            // academicCalendar.from_date = 
          }
        }
        if (academic_semester.academic_register_calendars.Count>0)
        {
          foreach (var academicRegisterCalendar in academic_semester.academic_register_calendars)
          {
            academicRegisterCalendar.academic_year_uid = null;
            academicRegisterCalendar.academic_semester_uid = null;
            academicRegisterCalendar.academic_semester_filter_uid = null;
            academicRegisterCalendar.academic_register_calendar_uid = null;
            academicRegisterCalendar.from_date = academicRegisterCalendar.from_date?.AddYears(add_year);
            academicRegisterCalendar.to_date = academicRegisterCalendar.to_date?.AddYears(add_year);
          }
        }
        if (academic_semester.academic_semester_filters.Count>0)
        {
           foreach (var academicSemesterFilter in academic_semester.academic_semester_filters)
        {
          academicSemesterFilter.academic_semester_filter_uid = null;
          academicSemesterFilter.academic_semester_uid = null;

          if (academicSemesterFilter.academic_calendars.Count>0)
          {
            foreach (var academicCalendar in academicSemesterFilter.academic_calendars)
            {
              academicCalendar.academic_semester_filter_uid = null;
              academicCalendar.academic_semester_uid = null;
              academicCalendar.academic_year_uid = null;
              academicCalendar.academic_semester_filter_uid = null;
              academicCalendar.academic_calendar_uid = null;
              academicCalendar.from_date = academicCalendar.from_date?.AddYears(add_year);
              academicCalendar.to_date = academicCalendar.to_date?.AddYears(add_year);
            }
          }
        
          if (academicSemesterFilter.academic_register_calendars.Count>0)
          {
            foreach (var academicRegisterCalendar in academicSemesterFilter.academic_register_calendars)
            {
              academicRegisterCalendar.academic_register_calendar_uid = null;
              academicRegisterCalendar.academic_semester_uid = null;
              academicRegisterCalendar.academic_year_uid = null;
              academicRegisterCalendar.academic_semester_filter_uid = null;
              academicRegisterCalendar.from_date = academicRegisterCalendar.from_date?.AddYears(add_year);
              academicRegisterCalendar.to_date = academicRegisterCalendar.to_date?.AddYears(add_year);
            }
          }
        
          if (academicSemesterFilter.academic_semester_filter_curriculums.Count>0)
          {
            foreach (var academicSemesterFilterCurriculum in academicSemesterFilter.academic_semester_filter_curriculums)
            {
              academicSemesterFilterCurriculum.academic_semester_filter_curriculum_uid = null;
              academicSemesterFilterCurriculum.academic_semester_filter_uid = null;
            }
          }
        
          if (academicSemesterFilter.academic_semester_filter_education_types.Count>0)
          {
            foreach (var academicSemesterFilterEducationType in academicSemesterFilter.academic_semester_filter_education_types)
            {
              academicSemesterFilterEducationType.academic_semester_filter_education_type_uid = null;
              academicSemesterFilterEducationType.academic_semester_filter_uid = null;
            }
          }
        
          if (academicSemesterFilter.academic_semester_filter_faculties.Count>0)
          {
            foreach (var academicSemesterFilterFaculty in academicSemesterFilter.academic_semester_filter_faculties)
            {
              academicSemesterFilterFaculty.academic_semester_filter_faculty_uid = null;
              academicSemesterFilterFaculty.academic_semester_filter_uid = null;
              
            }
          }
        }
        }
        // if (academic_semester.academic_calendars.Count>0)
        // {
        //   foreach (var academicCalendar in academic_semester.academic_calendars)
        //   {
        //     academicCalendar.academic_year_uid = null;
        //     academicCalendar.academic_semester_uid = null;
        //     academicCalendar.academic_semester_filter_uid = null;
        //     academicCalendar.academic_calendar_uid = null;
        //     
        //   }
        // }
        // if (academic_semester.academic_register_calendars.Count>0)
        // {
        //   foreach (var academicRegisterCalendar in academic_semester.academic_register_calendars)
        //   {
        //     academicRegisterCalendar.academic_year_uid = null;
        //     academicRegisterCalendar.academic_semester_uid = null;
        //     academicRegisterCalendar.academic_semester_filter_uid = null;
        //     academicRegisterCalendar.academic_register_calendar_uid = null;
        //
        //   }
        // }
        // if (academic_semester.academic_semester_filters.Count>0)
        // {
        //    foreach (var academicSemesterFilter in academic_semester.academic_semester_filters)
        // {
        //   academicSemesterFilter.academic_semester_filter_uid = null;
        //   academicSemesterFilter.academic_semester_uid = null;
        //  
        //   if (academicSemesterFilter.academic_calendars.Count>0)
        //   {
        //     foreach (var academicCalendar in academicSemesterFilter.academic_calendars)
        //     {
        //       academicCalendar.academic_semester_filter_uid = null;
        //       academicCalendar.academic_semester_uid = null;
        //       academicCalendar.academic_year_uid = null;
        //       academicCalendar.academic_semester_filter_uid = null;
        //       academicCalendar.academic_calendar_uid = null;
        //       
        //     }
        //   }
        //
        //   if (academicSemesterFilter.academic_register_calendars.Count>0)
        //   {
        //     foreach (var academicRegisterCalendar in academicSemesterFilter.academic_register_calendars)
        //     {
        //       academicRegisterCalendar.academic_register_calendar_uid = null;
        //       academicRegisterCalendar.academic_semester_uid = null;
        //       academicRegisterCalendar.academic_year_uid = null;
        //       academicRegisterCalendar.academic_semester_filter_uid = null;
        //      
        //     }
        //   }
        //
        //   if (academicSemesterFilter.academic_semester_filter_curriculums.Count>0)
        //   {
        //     foreach (var academicSemesterFilterCurriculum in academicSemesterFilter.academic_semester_filter_curriculums)
        //     {
        //       academicSemesterFilterCurriculum.academic_semester_filter_curriculum_uid = null;
        //       academicSemesterFilterCurriculum.academic_semester_filter_uid = null;
        //       
        //     }
        //   }
        //
        //   if (academicSemesterFilter.academic_semester_filter_education_types.Count>0)
        //   {
        //     foreach (var academicSemesterFilterEducationType in academicSemesterFilter.academic_semester_filter_education_types)
        //     {
        //       academicSemesterFilterEducationType.academic_semester_filter_education_type_uid = null;
        //       academicSemesterFilterEducationType.academic_semester_filter_uid = null;
        //     }
        //   }
        //
        //   if (academicSemesterFilter.academic_semester_filter_faculties.Count>0)
        //   {
        //     foreach (var academicSemesterFilterFaculty in academicSemesterFilter.academic_semester_filter_faculties)
        //     {
        //       academicSemesterFilterFaculty.academic_semester_filter_faculty_uid = null;
        //       academicSemesterFilterFaculty.academic_semester_filter_uid = null;
        //       
        //     }
        //   }
        // }
        // }

        new_academic_semester.Add(academic_semester);
      }

      current_academic_year.academic_semesters = null;
      
      
      _context.t_academic_year.Add(current_academic_year);
      await _context.SaveChangesAsync(claimsIdentity);
      _context.ChangeTracker.Clear();
      current_academic_year.academic_semesters = new_academic_semester;
   
      _context.t_academic_semester.AddRange(current_academic_year.academic_semesters);
      await _context.SaveChangesAsync(claimsIdentity);
      // return await _context.v_academic_semester
      //   .AsNoTracking()
      //   .Include(a => a.academic_calendars)
      //   .Include(a => a.academic_register_calendars)
      //   .Include(a => a.academic_semester_filters).ThenInclude(a => a.academic_calendars)
      //   .Include(a => a.academic_semester_filters).ThenInclude(a => a.academic_register_calendars)
      //   .Include(a => a.academic_semester_filters).ThenInclude(a => a.academic_semester_filter_curriculums)
      //   .FirstOrDefaultAsync(a => a.academic_semester_uid == uid);
      
      return current_academic_year;
    }

    public override async Task<int> DeleteById(Guid uid, ClaimsIdentity claimsIdentity)
    {
      var disciplineYear = await _context.v_discipline_year.AsNoTracking()
        .Where(w => w.academic_year_uid == uid).ToListAsync();
      if(disciplineYear.Count > 0 ) throw new Exception("INUSE");
      
      return await base.DeleteById(uid, claimsIdentity);
    }
  }
  
}
