using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using edu_api.Modules.Calendars.Databases.Models;
using Microsoft.EntityFrameworkCore;
using SeventyOneDev.Utilities;

namespace edu_api.Modules.Calendars.Services
{
  public class AcademicSemesterService:EntityUidService<t_academic_semester,v_academic_semester>
  {
    private readonly Db _context;
    public AcademicSemesterService(Db context) : base(context)
    {
      _context = context;
    }

    public override async Task<v_academic_semester> GetEntity(Guid uid, List<Guid?> agencies = null, string userName = null)
    {
      return await _context.v_academic_semester
        .AsNoTracking()
        .Include(a => a.academic_calendars)
        .Include(a => a.academic_register_calendars)
        .Include(a => a.academic_semester_filters).ThenInclude(a => a.academic_calendars)
        .Include(a => a.academic_semester_filters).ThenInclude(a => a.academic_register_calendars)
        .Include(a => a.academic_semester_filters).ThenInclude(a => a.academic_semester_filter_curriculums)
        .FirstOrDefaultAsync(a => a.academic_semester_uid == uid);

    }
    
    public  async Task<v_academic_semester> GetAcademicSemesterNoFilters(Guid uid, List<Guid?> agencies = null, string userName = null)
    {
      return await _context.v_academic_semester
        .AsNoTracking()
        .Include(a => a.academic_calendars)
        .Include(a => a.academic_register_calendars)
        .FirstOrDefaultAsync(a => a.academic_semester_uid == uid);

    }
    
    public  async Task<v_academic_semester> GetAcademicSemesterFilters(Guid uid, List<Guid?> agencies = null, string userName = null)
    {
      return await _context.v_academic_semester
        .AsNoTracking()
        .Include(a => a.academic_semester_filters).ThenInclude(a => a.academic_calendars)
        .Include(a => a.academic_semester_filters).ThenInclude(a => a.academic_register_calendars)
        .Include(a => a.academic_semester_filters).ThenInclude(a => a.academic_semester_filter_curriculums)
        .FirstOrDefaultAsync(a => a.academic_semester_uid == uid);

    }

    public override async Task<int> UpdateEntity(t_academic_semester entity, ClaimsIdentity claimsIdentity)
    {
      if (entity.academic_calendars != null)
      {
        var currentAcademicCalendars = await _context.t_academic_calendar.AsNoTracking()
          .Where(a => a.academic_semester_uid == entity.academic_semester_uid).ToListAsync();
        var removeAcademicCalendars = currentAcademicCalendars.Where(c =>
          entity.academic_calendars.All((a => a.academic_calendar_uid != c.academic_calendar_uid))).ToList();
        if (removeAcademicCalendars.Any())
        {
          _context.t_academic_calendar.RemoveRange(removeAcademicCalendars);
        }
      }

      if (entity.academic_register_calendars != null)
      {
        var currentAcademicRegisterCalendars = await _context.t_academic_register_calendar.AsNoTracking()
          .Where(a => a.academic_semester_uid == entity.academic_semester_uid).ToListAsync();
        var removeAcademicRegisterCalendars = currentAcademicRegisterCalendars.Where(c =>
          entity.academic_register_calendars.All((a => a.academic_register_calendar_uid != c.academic_register_calendar_uid))).ToList();
        if (removeAcademicRegisterCalendars.Any())
        {
          _context.t_academic_register_calendar.RemoveRange(removeAcademicRegisterCalendars);
        }
      }
      
      if (entity.academic_semester_filters != null)
      {
        var currentAcademicSemesterFilters = await _context.t_academic_semester_filter.AsNoTracking()
          .Where(a => a.academic_semester_uid == entity.academic_semester_uid).ToListAsync();
        var removeAcademicSemesterFilters = currentAcademicSemesterFilters.Where(c =>
          entity.academic_semester_filters.All((a => a.academic_semester_filter_uid != c.academic_semester_filter_uid))).ToList();
        if (removeAcademicSemesterFilters.Any())
        {
          _context.t_academic_semester_filter.RemoveRange(removeAcademicSemesterFilters);
        }
      }
     
      if (entity.academic_semester_filters != null)
      {
        var currentAcademicSemesterFilters = await _context.t_academic_semester_filter.AsNoTracking().Include(c=>c.academic_calendars).Include(c=>c.academic_register_calendars)
          .Where(a => a.academic_semester_uid == entity.academic_semester_uid).ToListAsync();
        // var removeAcademicSemesterFilters = currentAcademicSemesterFilters.Where(c =>
        //   entity.academic_semester_filters.All((a => a.academic_semester_filter_uid != c.academic_semester_filter_uid))).ToList();
        // if (removeAcademicSemesterFilters.Any())
        // {
        //   _context.t_academic_semester_filter.RemoveRange(removeAcademicSemesterFilters);
        // }
        foreach (var currentAcademicSemesterFilter in currentAcademicSemesterFilters)
        {
          foreach (var academicSemesterFilter in entity.academic_semester_filters)
          {
            var remove = currentAcademicSemesterFilter.academic_calendars.Where(c =>
                academicSemesterFilter.academic_calendars.All(a => a.academic_calendar_uid != c.academic_calendar_uid)).ToList();
            if (remove.Count>0)
            {
              _context.t_academic_calendar.RemoveRange(remove);
            }
            var remove2 = currentAcademicSemesterFilter.academic_register_calendars.Where(c =>
                academicSemesterFilter.academic_register_calendars.All(a => a.academic_register_calendar_uid != c.academic_register_calendar_uid)).ToList();
            if (remove2.Count>0)
            {
              _context.t_academic_register_calendar.RemoveRange(remove2);
              
            }
          }
        }
      }

      await _context.SaveChangesAsync();
      _context.ChangeTracker.Clear();
      return await base.UpdateEntity(entity, claimsIdentity);
    }
    
    
    public  async Task<int> UpdateAcademicSemesterNoFilters(t_academic_semester entity, ClaimsIdentity claimsIdentity)
    {
      if (entity.academic_calendars != null)
      {
        var currentAcademicCalendars = await _context.t_academic_calendar.AsNoTracking()
          .Where(a => a.academic_semester_uid == entity.academic_semester_uid).ToListAsync();
        var removeAcademicCalendars = currentAcademicCalendars.Where(c =>
          entity.academic_calendars.All((a => a.academic_calendar_uid != c.academic_calendar_uid))).ToList();
        if (removeAcademicCalendars.Any())
        {
          _context.t_academic_calendar.RemoveRange(removeAcademicCalendars);
        }
      }

      if (entity.academic_register_calendars != null)
      {
        var currentAcademicRegisterCalendars = await _context.t_academic_register_calendar.AsNoTracking()
          .Where(a => a.academic_semester_uid == entity.academic_semester_uid).ToListAsync();
        var removeAcademicRegisterCalendars = currentAcademicRegisterCalendars.Where(c =>
          entity.academic_register_calendars.All((a => a.academic_register_calendar_uid != c.academic_register_calendar_uid))).ToList();
        if (removeAcademicRegisterCalendars.Any())
        {
          _context.t_academic_register_calendar.RemoveRange(removeAcademicRegisterCalendars);
        }
      }

      await _context.SaveChangesAsync();
      _context.ChangeTracker.Clear();
      return await base.UpdateEntity(entity, claimsIdentity);
    }
    
    
       public  async Task<int> UpdateAcademicSemesterFilters(t_academic_semester entity, ClaimsIdentity claimsIdentity)
    {
        if (entity.academic_semester_filters != null)
      {
        var currentAcademicSemesterFilters = await _context.t_academic_semester_filter.AsNoTracking()
          .Where(a => a.academic_semester_uid == entity.academic_semester_uid).ToListAsync();
        var removeAcademicSemesterFilters = currentAcademicSemesterFilters.Where(c =>
          entity.academic_semester_filters.All((a => a.academic_semester_filter_uid != c.academic_semester_filter_uid))).ToList();
        if (removeAcademicSemesterFilters.Any())
        {
          _context.t_academic_semester_filter.RemoveRange(removeAcademicSemesterFilters);
        }
      }
     
      if (entity.academic_semester_filters != null)
      {
        var currentAcademicSemesterFilters = await _context.t_academic_semester_filter.AsNoTracking().Include(c=>c.academic_calendars).Include(c=>c.academic_register_calendars)
          .Where(a => a.academic_semester_uid == entity.academic_semester_uid).ToListAsync();
        // var removeAcademicSemesterFilters = currentAcademicSemesterFilters.Where(c =>
        //   entity.academic_semester_filters.All((a => a.academic_semester_filter_uid != c.academic_semester_filter_uid))).ToList();
        // if (removeAcademicSemesterFilters.Any())
        // {
        //   _context.t_academic_semester_filter.RemoveRange(removeAcademicSemesterFilters);
        // }
        foreach (var currentAcademicSemesterFilter in currentAcademicSemesterFilters)
        {
          foreach (var academicSemesterFilter in entity.academic_semester_filters)
          {
            var remove = currentAcademicSemesterFilter.academic_calendars.Where(c =>
                academicSemesterFilter.academic_calendars.All(a => a.academic_calendar_uid != c.academic_calendar_uid)).ToList();
            if (remove.Count>0)
            {
              _context.t_academic_calendar.RemoveRange(remove);
            }
            var remove2 = currentAcademicSemesterFilter.academic_register_calendars.Where(c =>
                academicSemesterFilter.academic_register_calendars.All(a => a.academic_register_calendar_uid != c.academic_register_calendar_uid)).ToList();
            if (remove2.Count>0)
            {
              _context.t_academic_register_calendar.RemoveRange(remove2);
              
            }
          }
        }
      }

      await _context.SaveChangesAsync();
      _context.ChangeTracker.Clear();
      return await base.UpdateEntity(entity, claimsIdentity);
    }
    
  }
}
