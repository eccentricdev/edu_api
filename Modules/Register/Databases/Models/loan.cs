using System;
using System.ComponentModel.DataAnnotations;
using SeventyOneDev.Utilities;

namespace edu_api.Modules.Register.Databases
{
  public class loan:base_table
  {
    [Key]
    public Guid? loan_uid { get; set; }
    public Guid? student_uid { get; set; }
    public Guid? academic_year_uid { get; set; }
    public string contract_no { get; set; }
  }

  public class t_loan : loan
  {

  }

  public class v_loan : loan
  {

  }
}
