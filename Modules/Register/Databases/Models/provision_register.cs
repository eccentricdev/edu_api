using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using edu_api.Modules.EducationPlan.Databases;
using SeventyOneDev.Utilities;

namespace edu_api.Modules.Register.Databases
{
  public class provision_register:base_table
    {
        [Key]
        public Guid? provision_register_uid {get;set;}
        public Guid? student_uid {get;set;}
        public Guid? academic_year_uid {get;set;}
        public Guid? academic_semester_uid {get;set;}
        public string bill_no {get;set;}
        public Guid? bill_uid { get; set; }
        public DateTime? register_date {get;set;}
        public bool? is_package {get;set;}
        public decimal? package_amount {get;set;}
        public decimal? credit_amount {get;set;}
        public decimal? fee_amount {get;set;}
        public decimal? scholarship_amount {get;set;}
        public decimal? loan_amount {get;set;}
        public decimal? advanced_amount {get;set;}
        public short? total_subject {get;set;}
        public short? total_credit {get;set;}
        public decimal? payment_amount {get;set;}

        public string register_type_code {get;set;}
        public short? register_status_id {get;set;}
        public bool? is_paid {get;set;}
        public DateTime? paid_datetime {get;set;}

        public string invoice_no { get; set; }
        public DateTime? invoice_due_date { get; set; }
        public decimal? paid_amount { get; set; }
        public string receipt_no { get; set; }
        // public List<provision_register_subject> provision_register_subjects {get;set;}
        // public List<provision_register_fee> provision_register_fees {get;set;}
        // public provision_register() {}
        // public provision_register(v_education_plan plan)
        // {
        //     //this.is_student = true;
        //     this.academic_semester_id = plan.academic_semester_id;
        //     this.register_date = DateTime.Now;
        //     this.is_package = false;
        //     this.package_amount = 0;
        //     this.credit_amount = plan.credit_amount;
        //     this.fee_amount = plan.fee_amount;
        //     this.scholarship_amount = 0;
        //     this.loan_amount = 0;
        //     this.advanced_amount = 0;
        //     this.total_subject = plan.total_subject;
        //     this.total_credit = plan.total_credit;
        //     this.payment_amount = plan.total_amount;
        //     this.provision_register_subjects = plan.education_plan_subjects.Select(p=>new provision_register_subject(p)).ToList();
        //     this.provision_register_fees = plan.education_plan_fees.Select(f=>new provision_register_fee(f)).ToList();
        // }


    }

  public class t_provision_register : provision_register
  {
    public t_provision_register()
    {

    }
    public t_provision_register(Guid studentUid,Guid academicYearUid,Guid academicSemesterUid,int totalSubjects,int totalCredits,string registerTypeCode, short registerStatusId )
    {
      this.student_uid = studentUid;
      this.academic_year_uid = academicYearUid;
      this.academic_semester_uid = academicSemesterUid;
      this.total_subject = (short)totalSubjects;
      this.total_credit = (short)totalCredits;
      this.register_type_code = registerTypeCode;
      this.register_status_id = registerStatusId;
    }
    public List<t_provision_register_subject> provision_register_subjects { get; set; }
    public List<t_provision_register_fee> provision_register_fees { get; set; }
    public List<t_provision_register_advance> provision_register_advances { get; set; }
    public List<t_provision_register_scholarship> provision_register_scholarships { get; set; }
  }

  public class v_provision_register : provision_register
  {
    public string student_code { get; set; }
    public List<v_provision_register_subject> provision_register_subjects { get; set; }
    public List<v_provision_register_fee> provision_register_fees { get; set; }
    public List<v_provision_register_advance> provision_register_advances { get; set; }
    public List<v_provision_register_scholarship> provision_register_scholarships { get; set; }
  }
}
