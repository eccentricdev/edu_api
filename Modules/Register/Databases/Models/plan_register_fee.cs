using System;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;
using edu_api.Modules.EducationPlan.Databases;
using edu_api.Modules.Register.Models;
using edu_api.Modules.Register.Models.Student;
using SeventyOneDev.Utilities;

namespace edu_api.Modules.Register.Databases
{
  public class plan_register_fee:base_table
  {
    [Key]
    public Guid? plan_register_fee_uid {get;set;}
    public Guid? plan_register_uid {get;set;}
    public Guid? register_fee_detail_uid {get;set;}
    public Guid? register_fee_uid { get; set; }
    public string fee_name_th { get; set; }
    public string fee_name_en { get; set; }
    public decimal? fee_amount {get;set;}
    // public plan_register plan_register {get;set;}
    // public plan_register_fee() {}
    // public plan_register_fee(v_education_plan_fee fee)
    // {
    //   this.register_fee_id = fee.register_fee_id;
    //   this.amount = fee.amount;
    // }
  }

  public class t_plan_register_fee : plan_register_fee
  {
    [JsonIgnore]
    public t_plan_register plan_register { get; set; }

    public t_plan_register_fee()
    {

    }

    public t_plan_register_fee(register_fee registerFee)
    {
      this.register_fee_detail_uid = registerFee.register_fee_detail_uid;
      //register_fee_uid = register_fee_uid;
      fee_name_th = registerFee.fee_name_th;
      fee_name_en = registerFee.fee_name_en;
      fee_amount = registerFee.fee_amount;

    }
    public t_plan_register_fee(counter_register_fee registerFee)
    {
      this.register_fee_detail_uid = registerFee.register_fee_detail_uid;
      //register_fee_uid = register_fee_uid;
      fee_name_th = registerFee.fee_name_th;
      fee_name_en = registerFee.fee_name_en;
      fee_amount = registerFee.fee_amount;

    }

    public t_plan_register_fee(v_education_plan_fee educationPlanFee)
    {
      //this.register_fee_detail_uid = educationPlanFee.register_fee_uid;
      register_fee_uid = educationPlanFee.register_fee_uid;
      //fee_name_th = educationPlanFee.fee_name_th;
      //fee_name_en = educationPlanFee.fee_name_en;
      fee_amount = educationPlanFee.fee_amount;
    }

  }

  public class v_plan_register_fee : plan_register_fee
  {
    [JsonIgnore]
    public v_plan_register plan_register { get; set; }
  }
}
