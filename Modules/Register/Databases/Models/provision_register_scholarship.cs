using System;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;
using SeventyOneDev.Utilities;

namespace edu_api.Modules.Register.Databases
{
  public class provision_register_scholarship:base_table
  {
    [Key]
    public Guid? provision_register_scholarship_uid {get;set;}
    public Guid? provision_register_uid {get;set;}
  }

  public class t_provision_register_scholarship : provision_register_scholarship
  {
    [JsonIgnore]
    public t_provision_register provision_register { get; set; }
  }

  public class v_provision_register_scholarship : provision_register_scholarship
  {
    [JsonIgnore]
    public v_provision_register provision_register { get; set; }
  }


}
