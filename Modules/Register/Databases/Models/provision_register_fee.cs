using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;
using edu_api.Modules.EducationPlan.Databases;
using edu_api.Modules.Register.Models;
using edu_api.Modules.Register.Models.Student;
using SeventyOneDev.Utilities;

namespace edu_api.Modules.Register.Databases
{
  public class provision_register_fee:base_table
  {
    [Key]
    public Guid? provision_register_fee_uid {get;set;}
    public Guid? provision_register_uid {get;set;}
    public Guid? register_fee_detail_uid {get;set;}
    public Guid? register_fee_uid { get; set; }
    public string fee_name_th { get; set; }
    public string fee_name_en { get; set; }
    public decimal? fee_amount {get;set;}
    // public provision_register provision_register {get;set;}
    // public provision_register_fee() {}
    // public provision_register_fee(v_education_plan_fee fee)
    // {
    //   this.register_fee_id = fee.register_fee_id;
    //   this.amount = fee.amount;
    // }
  }

  public class t_provision_register_fee : provision_register_fee
  {
    [JsonIgnore]
    public t_provision_register provision_register { get; set; }

    public t_provision_register_fee()
    {

    }

    public t_provision_register_fee(register_fee registerFee)
    {
      this.register_fee_detail_uid = registerFee.register_fee_detail_uid;
      register_fee_uid = register_fee_uid;
      fee_name_th = registerFee.fee_name_th;
      fee_name_en = registerFee.fee_name_en;
      fee_amount = registerFee.fee_amount;

    }
    public t_provision_register_fee(counter_register_fee registerFee)
    {
      this.register_fee_detail_uid = registerFee.register_fee_detail_uid;
      register_fee_uid = register_fee_uid;
      fee_name_th = registerFee.fee_name_th;
      fee_name_en = registerFee.fee_name_en;
      fee_amount = registerFee.fee_amount;

    }

  }

  public class v_provision_register_fee : provision_register_fee
  {
    [JsonIgnore]
    public v_provision_register provision_register { get; set; }
  }
}
