using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using edu_api.Modules.EducationPlan.Databases;
using SeventyOneDev.Utilities;
using SeventyOneDev.Utilities.Attributes;

namespace edu_api.Modules.Register.Databases
{

    public class plan_register:base_table
    {
        [Key]
        public Guid? plan_register_uid {get;set;}
        public int? applicant_apply_id {get;set;}
        public Guid? academic_year_uid {get;set;}
        public Guid? semester_uid {get;set;}
        public string bill_no {get;set;}
        public Guid? bill_uid { get; set; }
        public DateTime? register_date {get;set;}
        public bool? is_package {get;set;}
        public decimal? package_amount {get;set;}
        public decimal? credit_amount {get;set;}
        public decimal? fee_amount {get;set;}
        public decimal? scholarship_amount {get;set;}
        public decimal? loan_amount {get;set;}
        public decimal? advanced_amount {get;set;}
        public short? total_subject {get;set;}
        public short? total_credit {get;set;}
        public decimal? payment_amount {get;set;}

        public string register_type_code {get;set;}
        public short? register_status_id {get;set;}
        public bool? is_paid {get;set;}
        public DateTime? paid_datetime {get;set;}

        public string invoice_no { get; set; }
        public DateTime? invoice_due_date { get; set; }
        public decimal? paid_amount { get; set; }
        public string receipt_no { get; set; }


  }
    [GeneratedUidController("api/register/plan_register")]
    public class t_plan_register : plan_register
    {

      [Include]public List<t_plan_register_subject> plan_register_subjects { get; set; }
      [Include]public List<t_plan_register_fee> plan_register_fees { get; set; }
      public t_plan_register() {}
      public t_plan_register(int applicant_apply_id, v_education_plan plan)
      {
        //this.is_student = true;
        this.applicant_apply_id = applicant_apply_id;
        this.academic_year_uid = plan.academic_year_uid;
        this.semester_uid = plan.academic_semester_uid;
        this.register_date = DateTime.Now;
        this.is_package = false;
        this.package_amount = 0;
        this.credit_amount = plan.credit_amount;
        this.fee_amount = plan.fee_amount;
        this.scholarship_amount = 0;
        this.loan_amount = 0;
        this.advanced_amount = 0;
        this.total_subject = plan.total_subject;
        this.total_credit = plan.total_credit;
        this.payment_amount = plan.total_amount;
        this.plan_register_subjects = plan.education_plan_subjects.Select(p=>new t_plan_register_subject(p)).ToList();
        this.plan_register_fees = plan.education_plan_fees.Select(f=>new t_plan_register_fee(f)).ToList();
      }
    }

    public class v_plan_register : plan_register
    {
      [Include]public List<v_plan_register_subject> plan_register_subjects { get; set; }
      [Include]public List<v_plan_register_fee> plan_register_fees { get; set; }
    }
}
