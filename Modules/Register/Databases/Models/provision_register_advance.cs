using System;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;
using SeventyOneDev.Utilities;

namespace edu_api.Modules.Register.Databases
{
  public class provision_register_advance:base_table
  {
    [Key]
    public Guid? provision_register_advance_uid { get; set; }
    public Guid? provision_register_uid { get; set; }
    public Guid? advance_payment_uid { get; set; }
    public decimal? used_amount { get; set; }
    public int? row_order { get; set; }
  }

  public class t_provision_register_advance : provision_register_advance
  {
    [JsonIgnore]
    public t_provision_register provision_register { get; set; }
  }

  public class v_provision_register_advance : provision_register_advance
  {
    [JsonIgnore]
    public v_provision_register provision_register { get; set; }
  }
}
