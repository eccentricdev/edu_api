using System;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;
using edu_api.Modules.Register.Models;
using SeventyOneDev.Utilities;

namespace edu_api.Modules.Register.Databases
{
  public class provision_register_subject:base_table
    {
        [Key]
        public Guid? provision_register_subject_uid {get;set;}
        public Guid? provision_register_uid {get;set;}
        public Guid? year_subject_uid {get;set;}
        public Guid? year_subject_section_uid {get;set;}
        //For display
        public string year_subject_code { get; set; }
        public string year_subject_name_th { get; set; }
        public string year_subject_name_en { get; set; }
        public short? credit { get; set; }
        public Guid? lecture_year_subject_section_uid {get;set;}
        public string lecture_section_code { get; set; }
        [MaxLength(50)]
        public string lecture_study_time { get; set; }
        [MaxLength(100)]
        public string lecture_study_time_text { get; set; }
        public Guid? topic_uid { get; set; }
        public string topic_code { get; set; }
        public string topic_name_th { get; set; }
        public string topic_name_en { get; set; }
        public short? topic_credit { get; set; }
        public Guid? lab_year_subject_section_uid {get;set;}
        public string lab_section_code { get; set; }
        [MaxLength(50)]
        public string lab_study_time { get; set; }
        [MaxLength(1000)]
        public string lab_study_time_text { get; set; }
        public byte? subject_type_id {get;set;}
        public decimal? lecture_credit_amount {get;set;}
        public decimal? total_lab_amount {get;set;}
        public decimal? total_lecture_amount {get;set;}
        public decimal? total_amount {get;set;}
        public DateTime? midterm_lecture_exam_from_datetime { get; set; }
        public DateTime? midterm_lecture_exam_to_datetime { get; set; }
        public DateTime? midterm_lab_exam_from_datetime { get; set; }
        public DateTime? midterm_lab_exam_to_datetime { get; set; }
        public DateTime? final_lecture_exam_from_datetime { get; set; }
        public DateTime? final_lecture_exam_to_datetime { get; set; }
        public DateTime? final_lab_exam_from_datetime { get; set; }
        public DateTime? final_lab_exam_to_datetime { get; set; }
        // public long? midterm_lecture_exam_from_unixtime { get; set; }
        // public long? midterm_lecture_exam_to_unixtime { get; set; }
        // public long? midterm_lab_exam_from_unixtime { get; set; }
        // public long? midterm_lab_exam_to_unixtime { get; set; }
        // public long? final_lecture_exam_from_unixtime { get; set; }
        // public long? final_lecture_exam_to_unixtime { get; set; }
        // public long? final_lab_exam_from_unixtime { get; set; }
        // public long? final_lab_exam_to_unixtime { get; set; }
        //public short? register_subject_status_id { get; set; }
        public short? register_subject_status_id { get; set; }
        public short? register_subject_flag_id { get; set; }
        //
        // [JsonIgnore]
        // public provision_register provision_register {get;set;}
        // //public study_subject_section lecture_study_subject_section {get;set;}
        // //public study_subject_section lab_study_subject_section {get;set;}
        // public provision_register_subject() {}
        // public provision_register_subject(v_education_plan_subject subject)
        // {
        //     this.subject_year_id = subject.subject_year_id;
        //     this.lecture_study_subject_section_id = subject.lecture_study_subject_section_id;
        //     this.lab_study_subject_section_id = subject.lab_study_subject_section_id;
        //     //this.subject_type_id = subject.subject_year_id;
        //     this.lecture_credit_amount = subject.lecture_credit_amount;
        //     this.total_lab_amount = subject.total_lab_amount;
        //     this.total_lecture_amount = subject.total_lab_amount;
        //     this.total_amount = subject.total_amount;
        //
        // }


    }

  public class t_provision_register_subject : provision_register_subject
  {
     public t_provision_register_subject()
        {

        }
        public t_provision_register_subject(register_subject registerSubject)
    {
      this.year_subject_uid = registerSubject.year_subject_uid;
      this.year_subject_code = registerSubject.year_subject_code;
      this.year_subject_name_en = registerSubject.year_subject_name_en;
      this.year_subject_name_th = registerSubject.year_subject_name_th;
      this.credit = registerSubject.credit;
      if (registerSubject.lecture_section != null)
      {
        this.lecture_year_subject_section_uid = registerSubject.lecture_section.year_subject_section_uid;
        this.lecture_section_code = registerSubject.lecture_section.section_code;
        this.lecture_study_time = registerSubject.lecture_section.study_time;
        this.lecture_study_time_text = registerSubject.lecture_section.study_time_text_en;
        //this.topic_code = registerSubject.lecture_section.topic_code;
        this.topic_uid = registerSubject.lecture_section.topic_uid;
        this.topic_name_th = registerSubject.lecture_section.topic_name_th;
        this.topic_name_en = registerSubject.lecture_section.topic_name_en;
        this.topic_credit = registerSubject.lecture_section.topic_credit;
      }
      if (registerSubject.lab_section != null)
      {
        this.lab_year_subject_section_uid = registerSubject.lab_section.year_subject_section_uid;
        this.lab_section_code = registerSubject.lab_section.section_code;
        this.lab_study_time = registerSubject.lab_section.study_time;
        this.lab_study_time_text = registerSubject.lab_section.study_time_text_en;
      }

      this.subject_type_id = 0;
      this.lecture_credit_amount = registerSubject.lecture_credit_amount;
      this.total_lab_amount = registerSubject.total_lab_amount;
      this.total_lecture_amount = registerSubject.total_lecture_amount;
    this.total_amount = registerSubject.total_amount;

    this.midterm_lecture_exam_from_datetime = registerSubject.midterm_lecture_exam_from_datetime;
    this.midterm_lecture_exam_to_datetime = registerSubject.midterm_lecture_exam_to_datetime;
    this.midterm_lab_exam_from_datetime = registerSubject.midterm_lab_exam_from_datetime;
    this.midterm_lab_exam_to_datetime = registerSubject.midterm_lab_exam_to_datetime;
    this.final_lecture_exam_from_datetime = registerSubject.final_lecture_exam_from_datetime;
    this.final_lecture_exam_to_datetime = registerSubject.final_lecture_exam_to_datetime;
    this.final_lab_exam_from_datetime = registerSubject.final_lab_exam_from_datetime;
    this.final_lab_exam_to_datetime = registerSubject.final_lab_exam_to_datetime;
    this.register_subject_status_id = registerSubject.register_subject_status_id;
    this.register_subject_flag_id = registerSubject.register_subject_flag_id;

    }
    [JsonIgnore]
    public t_provision_register provision_register { get; set; }
  }

  public class v_provision_register_subject : provision_register_subject
  {
    [JsonIgnore]
    public v_provision_register provision_register { get; set; }
  }
}
