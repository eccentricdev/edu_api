using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace edu_api.Modules.Register.Databases
{
    public class t_applicant_apply
    {
        [Key]
        public int? applicant_apply_id {get;set;}
        public Guid? academic_year_uid {get;set;}
        public Guid? academic_semester_uid {get;set;}
        public int? applicant_id {get;set;}
        public int? program_id {get;set;}
        public int? program_schedule_id {get;set;}
        public int? course_schedule_id {get;set;}
        public long? application_id {get;set;}
        public string application_invoice_no { get; set; }
        public DateTime? application_invoice_due_date { get; set; }
        public int? payin_id {get;set;}
        public long? payin_no {get;set;}
        [Column(TypeName = "decimal(18,4)")]
        public decimal? application_amount {get;set;}
        [Column(TypeName = "decimal(18,4)")]
        public decimal? exam_amount {get;set;}
        [Column(TypeName = "decimal(18,4)")]
        public decimal? coupon_amount {get;set;}
        public int? coupon_id {get;set;}
        [Column(TypeName = "decimal(18,4)")]
        public decimal? payment_amount {get;set;}
        public bool? payment_flag {get;set;}
        public bool? document_flag {get;set;}
        public bool? is_document_completed {get;set;}
        [NotMapped]
        public bool? is_document_incomplete {get;set;}
        [NotMapped]
        public bool? is_document_wrong {get;set;}
        [NotMapped]
        public bool? is_document_update {get;set;}
        [NotMapped]
        public bool? is_not_approve {get;set;}

        [MaxLength(10)]
        public string payment_type_code {get;set;}
        public int? status_id {get;set;}
        [MaxLength(50)]
        public string create_by {get;set;}
        public DateTime? create_datetime {get;set;}
        [MaxLength(50)]
        public string last_update_by {get;set;}
        public DateTime? last_update_datetime {get;set;}
        public int? application_status_id {get;set;}
        [Column(TypeName="DATE")]
        public DateTime? apply_date {get;set;}
        public long? plan_register_pay_in_no  {get;set;}
        public string plan_register_invoice_no { get; set; }
        [Column(TypeName = "decimal(18,4)")]
        public decimal? plan_register_payment_amount {get;set;}
        public DateTime? plan_register_payment_due_date { get; set; }
        public bool? plan_register_payment_flag {get;set;}
        public int? plan_register_id {get;set;}
        public Guid? education_type_uid {get;set;}
        public Guid? college_faculty_uid {get;set;}
        public Guid? faculty_curriculum_uid {get;set;}
        [MaxLength(10)]
        public string student_code {get;set;}
        [MaxLength(200)]
        public string rsu_email {get;set;}
        public int? validate_by_id {get;set;}
        public short? apply_type_id { get; set; }
        public Guid? bill_uid { get; set; }
        public Guid? plan_register_bill_uid { get; set; }
        public Guid? plan_register_uid { get; set; }
        public string plan_register_bill_no { get; set; }
        [MaxLength(50)]
        public string application_no {get;set;}
        public int? written_exam_room_id {get;set;}
        public int? seat_no {get;set;}
        [MaxLength(50)]
        public string exam_seat_no {get;set;}
        public bool? is_written_exam_passed {get;set;}
        public bool? is_interview_exam_passed {get;set;}
    }
    public class v_applicant_apply
    {
        [Key]
        public int? applicant_apply_id {get;set;}
        public Guid? academic_year_uid {get;set;}
        public Guid? academic_semester_uid {get;set;}
        public int? applicant_id {get;set;}
        public int? program_id {get;set;}
        public int? program_schedule_id {get;set;}
        public int? course_schedule_id {get;set;}
        public long? application_id {get;set;}
        public string application_invoice_no { get; set; }
        public DateTime? application_invoice_due_date { get; set; }
        public long? payin_no {get;set;}
        public decimal? application_amount {get;set;}
        public decimal? exam_amount {get;set;}
        public decimal? coupon_amount {get;set;}
        public int? coupon_id {get;set;}
        public decimal? payment_amount {get;set;}
        public bool? payment_flag {get;set;}
        public bool? document_flag {get;set;}
        public bool? is_document_completed {get;set;}
        [NotMapped]
        public bool? is_document_incomplete {get;set;}
        [NotMapped]
        public bool? is_document_wrong {get;set;}
        [NotMapped]
        public bool? is_document_update {get;set;}
        [NotMapped]
        public bool? is_not_approve {get;set;}
        // public bool? is_document_incomplete {get;set;}
        // public bool? is_document_wrong {get;set;}
        // public bool? is_document_update {get;set;}
        public string payment_type_code {get;set;}
        public int? status_id {get;set;}
        public int? create_by {get;set;}
        public DateTime? create_datetime {get;set;}
        public int? last_update_by {get;set;}
        public DateTime? last_update_datetime {get;set;}
        public string program_schedule_code {get;set;}
        public string program_schedule_name_th {get;set;}
        public string program_schedule_name_en {get;set;}
        public string first_name_th {get;set;}
        public string last_name_th {get;set;}
        public string first_name_en {get;set;}
        public string middle_name_en {get;set;}
        public string last_name_en {get;set;}
        public string email {get;set;}
        public string mobile {get;set;}
        public string prefix_code {get;set;}
        public string prefix_name_th {get;set;}
        public string prefix_name_en {get;set;}
        public int? course_id {get;set;}
        public string course_code {get;set;}
        public string course_name_th {get;set;}
        public string course_name_en {get;set;}
        public string program_code {get;set;}
        public string program_name_th {get;set;}
        public string program_name_en {get;set;}
        public string education_type_code {get;set;}
        public string education_type_name_th {get;set;}
        public string education_type_name_en {get;set;}
        public string faculty_code {get;set;}
        public string faculty_name_th {get;set;}
        public string faculty_name_en {get;set;}
        public string major_code {get;set;}
        public string major_name_th {get;set;}
        public string major_name_en {get;set;}
        public string academic_year_code {get;set;}
        public string academic_year_name_th {get;set;}
        public string academic_year_name_en {get;set;}
        public string academic_semester_code {get;set;}
        public string academic_semester_name_th {get;set;}
        public string academic_semester_name_en {get;set;}
        public string personal_id {get;set;}
        public int? application_status_id {get;set;}
        public string application_status_code {get;set;}
        public string application_status_name_th {get;set;}
        public string application_status_name_en {get;set;}
        public bool? show_upload {get;set;}
        public bool? show_modify {get;set;}
        public bool? show_payment {get;set;}
        public bool? show_enrollment {get;set;}
        public bool? show_scholarship {get;set;}
        public bool? show_register {get;set;}
        public int? age {get;set;}
        public DateTime? date_of_birth {get;set;}
        public int? gender_id {get;set;}
        public DateTime? apply_date {get;set;}
        public long? plan_register_pay_in_no  {get;set;}

        public string plan_register_invoice_no { get; set; }
        public decimal? plan_register_payment_amount {get;set;}

        public DateTime? plan_register_payment_due_date { get; set; }
        public bool? plan_register_payment_flag {get;set;}
        public int? plan_register_id {get;set;}
        public Guid? education_type_uid {get;set;}
        public Guid? college_faculty_uid {get;set;}
        public Guid? faculty_curriculum_uid {get;set;}
        public string student_code {get;set;}
        public string rsu_email {get;set;}
        public int? validate_by_id {get;set;}
        [NotMapped]
        public string apply_date_search {get;set;}
        public int? school_id {get;set;}
        public string school_name {get;set;}
        public bool? foreign_school_flag {get;set;}
        public string school_country_id {get;set;}
        public int? school_province_id {get;set;}
        public string school_province_name_en {get;set;}
        public string school_province_name_th {get;set;}
        public short? apply_type_id { get; set; }
        public string room_name { get; set; }
        public string building_name { get; set; }
        public Guid? bill_uid { get; set; }
        public Guid? plan_register_bill_uid { get; set; }
        public Guid? plan_register_uid { get; set; }
        public string plan_register_bill_no { get; set; }

        
        // public List<v_applicant_apply_course> courses {get;set;}
        // public List<v_applicant_apply_document> documents {get;set;}
        // public List<v_applicant_apply_fee> fees {get;set;}
        // //[NotMapped]
        // public List<v_written_exam_result> writtenExamResults {get;set;}
        // public List<v_interview_exam_result> interviewExamResults {get;set;}
        // public List<v_practical_exam_result> practicalExamResults {get;set;}
        // [NotMapped]
        // public List<v_program_exam_subject> examSubjects {get;set;}
        public string application_no {get;set;}
        public int? written_exam_room_id {get;set;}
        public int? seat_no {get;set;}
        public string exam_seat_no {get;set;}
        public bool? is_written_exam_passed {get;set;}
        public bool? is_interview_exam_passed {get;set;}
        public bool? written_flag {get;set;}
        public bool? interview_flag {get;set;}
        public DateTime? written_exam_start_date {get;set;}
        [Column(TypeName="Date")]
        public DateTime? written_exam_end_date {get;set;}
        [Column(TypeName="Date")]
        public DateTime? written_exam_announcement_date {get;set;}
        public TimeSpan? written_exam_announcement_time {get;set;}
        [Column(TypeName="Date")]
        public DateTime? interview_exam_start_date {get;set;}
        [Column(TypeName="Date")]
        public DateTime? interview_exam_end_date {get;set;}
        [Column(TypeName="Date")]
        public DateTime? enrollment_start_date {get;set;}
        [Column(TypeName="Date")]
        public DateTime? enrollment_end_date {get;set;}
        [Column(TypeName="Date")]
        public DateTime? interview_exam_announcement_date {get;set;}
        public TimeSpan? interview_exam_announcement_time {get;set;}

    }
}