using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;
using SeventyOneDev.Utilities;
using SeventyOneDev.Utilities.Attributes;

namespace edu_api.Modules.Register.Databases
{
  public class final_register_subject : base_table
  {
    [Key] public Guid? final_register_subject_uid { get; set; }
    public Guid? final_register_uid { get; set; }
    public Guid? year_subject_uid { get; set; }
    public Guid? year_subject_section_uid { get; set; }

    //For display
    public string year_subject_code { get; set; }
    public string year_subject_name_th { get; set; }
    public string year_subject_name_en { get; set; }
    public short? credit { get; set; }
    public Guid? lecture_year_subject_section_uid { get; set; }
    public string lecture_section_code { get; set; }
    [MaxLength(50)] public string lecture_study_time { get; set; }
    [MaxLength(100)] public string lecture_study_time_text { get; set; }
    public Guid? topic_uid { get; set; }
    public string topic_code { get; set; }
    public string topic_name_th { get; set; }
    public string topic_name_en { get; set; }
    public short? topic_credit { get; set; }
    public Guid? lab_year_subject_section_uid { get; set; }
    public string lab_section_code { get; set; }
    [MaxLength(50)] public string lab_study_time { get; set; }
    [MaxLength(1000)] public string lab_study_time_text { get; set; }
    public byte? subject_type_id { get; set; }
    public decimal? lecture_credit_amount { get; set; }
    public decimal? total_lab_amount { get; set; }
    public decimal? total_lecture_amount { get; set; }
    public decimal? total_amount { get; set; }
    public DateTime? midterm_lecture_exam_from_datetime { get; set; }
    public DateTime? midterm_lecture_exam_to_datetime { get; set; }
    public DateTime? midterm_lab_exam_from_datetime { get; set; }
    public DateTime? midterm_lab_exam_to_datetime { get; set; }
    public DateTime? final_lecture_exam_from_datetime { get; set; }
    public DateTime? final_lecture_exam_to_datetime { get; set; }
    public DateTime? final_lab_exam_from_datetime { get; set; }
    public DateTime? final_lab_exam_to_datetime { get; set; }

    public short? register_subject_status_id { get; set; }

    //[Column(TypeName = "decimal(18,2)")]
    //public decimal exam_score { get; set; }
    [Column(TypeName = "decimal(18,2)")] public decimal? midterm_score { get; set; }
    [Column(TypeName = "decimal(18,2)")] public decimal? final_score { get; set; }
    [Column(TypeName = "decimal(18,2)")] public decimal? total_score { get; set; }
    [Column(TypeName = "decimal(18,2)")] public decimal? exam_score { get; set; }
    public Guid? grade_uid { get; set; }
    public Guid? grade_reason_uid { get; set; }
    public string grade_remark { get; set; }
    public Guid? primary_year_subject_section_uid { get; set; }


    public Guid? grade_criteria_uid { get; set; }
public int? row_order { get; set; }
    //public Guid? grade_uid { get; set; }
    //public Guid? grade_reason_uid { get; set; }

  }

  [GeneratedUidController("api/register/final_register_subject")]
  public class t_final_register_subject : final_register_subject
  {
    [JsonIgnore] public t_final_register final_register { get; set; }

    public t_final_register_subject()
    {
    }

    public t_final_register_subject(t_provision_register_subject provisionRegisterSubject)
    {
      year_subject_uid = provisionRegisterSubject.year_subject_uid;
      year_subject_section_uid = provisionRegisterSubject.year_subject_section_uid;

      //For display
      year_subject_code = provisionRegisterSubject.year_subject_code;
      year_subject_name_th = provisionRegisterSubject.year_subject_name_th;
      year_subject_name_en = provisionRegisterSubject.year_subject_name_en;
      credit = provisionRegisterSubject.credit;
      lecture_year_subject_section_uid = provisionRegisterSubject.lecture_year_subject_section_uid;
      lecture_section_code = provisionRegisterSubject.lecture_section_code;
      lecture_study_time = provisionRegisterSubject.lecture_study_time;
      lecture_study_time_text = provisionRegisterSubject.lecture_study_time_text;
      topic_uid = provisionRegisterSubject.topic_uid;
      topic_code = provisionRegisterSubject.topic_code;
      topic_name_th = provisionRegisterSubject.topic_name_th;
      topic_name_en = provisionRegisterSubject.topic_name_en;
      topic_credit = provisionRegisterSubject.topic_credit;
      lab_year_subject_section_uid = provisionRegisterSubject.lab_year_subject_section_uid;
      lab_section_code = provisionRegisterSubject.lab_section_code;
      lab_study_time = provisionRegisterSubject.lab_study_time;
      lab_study_time_text = provisionRegisterSubject.lab_study_time_text;
      subject_type_id = provisionRegisterSubject.subject_type_id;
      lecture_credit_amount = provisionRegisterSubject.lecture_credit_amount;
      total_lab_amount = provisionRegisterSubject.total_lab_amount;
      total_lecture_amount = provisionRegisterSubject.total_lecture_amount;
      total_amount = provisionRegisterSubject.total_amount;
      midterm_lecture_exam_from_datetime = provisionRegisterSubject.midterm_lecture_exam_from_datetime;
      midterm_lecture_exam_to_datetime = provisionRegisterSubject.midterm_lecture_exam_to_datetime;
      midterm_lab_exam_from_datetime = provisionRegisterSubject.midterm_lab_exam_from_datetime;
      midterm_lab_exam_to_datetime = provisionRegisterSubject.midterm_lab_exam_to_datetime;
      final_lecture_exam_from_datetime = provisionRegisterSubject.final_lecture_exam_from_datetime;
      final_lecture_exam_to_datetime = provisionRegisterSubject.final_lecture_exam_to_datetime;
      final_lab_exam_from_datetime = provisionRegisterSubject.final_lab_exam_from_datetime;
      final_lab_exam_to_datetime = provisionRegisterSubject.final_lab_exam_to_datetime;
      register_subject_status_id = provisionRegisterSubject.register_subject_status_id;
      if (provisionRegisterSubject.lecture_year_subject_section_uid.HasValue)
      {
        primary_year_subject_section_uid = lecture_year_subject_section_uid;
      }
      else
      {
        primary_year_subject_section_uid = lab_year_subject_section_uid;
      }
    }
  }

  public class v_final_register_subject : final_register_subject
  {
    public Guid? student_uid { get; set; }
    public string student_code { get; set; }
    public string display_name_th { get; set; }
    public string display_name_en { get; set; }
    public string grade_name_th { get; set; }
    public string grade_name_en { get; set; }
    public Guid? academic_year_uid { get; set; }
    public Guid? academic_semester_uid { get; set; }
    public Guid? subject_uid { get; set; }
    public Guid? education_type_uid { get; set; }
    public decimal? grade_point { get; set; }
    [JsonIgnore] public v_final_register final_register { get; set; }
  }
}
