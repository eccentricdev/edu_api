using System;
using System.ComponentModel.DataAnnotations;
using SeventyOneDev.Utilities;

namespace edu_api.Modules.Register.Databases
{
  public class loan_semester:base_table
  {
    [Key]
    public Guid? loan_semester_uid { get; set; }
    public Guid? semester_uid { get; set; }
    public Guid? loan_uid { get; set; }
    public decimal? loan_amount { get; set; }
    public decimal? use_amount { get; set; }
  }

  public class t_loan_semester : loan_semester
  {

  }

  public class v_loan_semester : loan_semester
  {
    public Guid? student_uid { get; set; }
    public Guid? academic_year_uid { get; set; }
    public string contract_no { get; set; }
  }
}
