using System;

namespace edu_api.Modules.Register.Databases
{
    public class v_student_register_subject
    {
        public string academic_year_code { get; set; }
        public string semester_code{ get; set; }
        public string year_subject_code{ get; set; }
        public string lecture_section_code{ get; set; }
        public string student_code{ get; set; }
        public string college_faculty_name_th{ get; set; }
        public string faculty_curriculum_name_th{ get; set; }
        public string grade_name_th{ get; set; }
        public Guid? lecture_year_subject_section_uid { get; set; }
    }
    public class v_student_register_subject_detail
    {
        public string academic_year_code { get; set; }
        public string semester_code{ get; set; }
        public string year_subject_code{ get; set; }
        public string year_subject_name_th { get; set; }
        public string year_subject_name_en { get; set; }
        public decimal? credit { get; set; }
        public string lecture_section_code{ get; set; }
       
        public string student_code{ get; set; }
        public string college_faculty_name_th{ get; set; }
        public string faculty_curriculum_name_th{ get; set; }
        public string grade_name_th{ get; set; }
        public Guid? lecture_year_subject_section_uid { get; set; }
        public string lecture_instructor_no { get; set; }
        public string lecture_instructor_name_th { get; set; }
        public string lecture_instructor_name_en { get; set; }
        public string lecture_study_day { get; set; }
        public string lecture_room_code { get; set; }
        public TimeSpan? lecture_study_from_time { get; set; }
        public TimeSpan? lecture_study_to_time { get; set; }
        public DateTime? midterm_lecture_exam_from_datetime{ get; set; } 
        public DateTime? midterm_lecture_exam_to_datetime{ get; set; } 
        public DateTime? final_lecture_exam_from_datetime{ get; set; } 
        public DateTime? final_lecture_exam_to_datetime{ get; set; } 
        public Guid? lab_year_subject_section_uid { get; set; }
        public string lab_section_code { get; set; }
        public string lab_instructor_no { get; set; }
        public string lab_instructor_name_th { get; set; }
        public string lab_instructor_name_en { get; set; }
        public string lab_study_day { get; set; }
        public string lab_room_code { get; set; }
        public TimeSpan? lab_study_from_time { get; set; }
        public TimeSpan? lab_study_to_time { get; set; }
        public DateTime? midterm_lab_exam_from_datetime{ get; set; } 
        public DateTime? midterm_lab_exam_to_datetime { get; set; }
        public DateTime? final_lab_exam_from_datetime { get; set; }
        public DateTime? final_lab_exam_to_datetime { get; set; }
    }
}