using System;
using System.ComponentModel.DataAnnotations;

namespace edu_api.Modules.Register.Databases
{
    public class v_year_subject_section_schedule
    {
        // public Guid? academic_year_uid {get;set;}
        // public Guid? academic_semester_uid {get;set;}
        // public Guid? student_uid {get;set;} 
        public Guid? year_subject_section_uid { get; set; }
        public string year_subject_code {get;set;} 
        public string year_subject_name_th {get;set;}
        public string year_subject_name_en {get;set;}
        public string room_code {get;set;}
        public TimeSpan? study_from_time {get;set;}
        public TimeSpan? study_to_time {get;set;}
        public string day_name_th {get;set;}
        public string day_name_en {get;set;}
        public Guid? study_day_uid {get;set;}
        public string section_code {get;set;}
        public string section_type_code {get;set;} 
        // [Key]
        // public Guid? study_day_uid {get;set;}
        // public Guid? year_subject_uid  {get;set;}
        // public Guid? year_subject_section_uid {get;set;}
        // public string year_subject_code {get;set;}
        // public string section_code  {get;set;}
        // public string section_type_code  {get;set;}
        // public string day_name_en {get;set;}
        // public string day_name_th {get;set;}
        // public TimeSpan? start_time  {get;set;}
        // public TimeSpan? end_time  {get;set;}
        // public string room_code {get;set;}
        // public string year_subject_name_th {get;set;}
        // public string year_subject_name_en {get;set;}
    }
}