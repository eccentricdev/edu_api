namespace edu_api.Modules.Register.Databases
{
  public class v_year_subject_section_instructor_summary
  {
    public string academic_year_code {get;set;}
    public string semester_code {get;set;}
    public string year_subject_code {get;set;}
    public string section_code {get;set;}
    public string student_code {get;set;}
    public string study_time_text_en {get;set;}
    public string personnel_no {get;set;}
    public string personnel_type_code { get; set; }
  }
}
