using System;
using System.ComponentModel.DataAnnotations;

namespace edu_api.Modules.Register.Databases
{
    public class v_applicant
    {
        [Key]
        public int? applicant_id {get;set;}
        //public int? sign_in_channel_id {get;set;}
        //public string user_id {get;set;}
        //public int? title_id {get;set;}
        public int? prefix_id {get;set;}
        //public string prefix_code {get;set;}
        public int? gender_id {get;set;}
        public string first_name_th {get;set;}
        public string last_name_th {get;set;}
        public string first_name_en {get;set;}
        public string middle_name_en {get;set;}
        public string last_name_en {get;set;}
        public DateTime? date_of_birth {get;set;}
        public int? age {get;set;}
        public string email {get;set;}
        public string mobile {get;set;}
        // public int? country_id {get;set;}
        // public string country_name {get;set;}
        //public int? province_id {get;set;}
        public string province_name {get;set;}
        //public int? district_id {get;set;}
        public string district_name {get;set;}
        //public int? sub_district_id {get;set;}
        public string sub_district_name {get;set;}
        public string address1 {get;set;}
        public string address2 {get;set;}
        //public string zipcode {get;set;}
        public int? education_level_id {get;set;}
        public string graduate_year {get;set;}
        //public int? graduate_from_id {get;set;}
        //public string graduate_from_name {get;set;}
        public int? program_type_id {get;set;}
        public string education_level_code {get;set;}
        public string education_level_name_en {get;set;}
        public string education_level_name_th {get;set;}
        public string program_type_code {get;set;}
        public string program_type_name_en {get;set;}
        public string program_type_name_th {get;set;}
        //public string major {get;set;}
        public string gpa {get;set;}
        public int? status_id {get;set;}
        public int? create_by {get;set;}
        public DateTime? create_datetime {get;set;}
        public int? last_update_by {get;set;}
        public DateTime? last_update_datetime  {get;set;}
        public int? sub_district_id {get;set;}
        public string postal_code {get;set;}
        public string road {get;set;}
        public string soi {get;set;}
        public int? school_id {get;set;}
        //public string school_code {get;set;}
        public string school_name {get;set;}
        public string moo {get;set;}
        public bool? foreign_school_flag {get;set;}
        public int? school_country_id {get;set;}
        public string personal_id {get;set;}
        public int? school_province_id {get;set;}
       // public string school_province_code {get;set;}
        public string school_province_name_en {get;set;}
        public string school_province_name_th {get;set;}
    }
}