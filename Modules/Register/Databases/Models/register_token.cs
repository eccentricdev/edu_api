using System;
using System.ComponentModel.DataAnnotations;
using System.Security.Permissions;
using SeventyOneDev.Utilities;

namespace edu_api.Modules.Register.Databases
{
  public class register_token:base_table
  {
    [Key]
    public Guid? register_token_uid { get; set; }
    //public string token { get; set; }
    public Guid? student_uid { get; set; }
    public Guid? education_type_uid { get; set; }
    public Guid? college_faculty_uid { get; set; }
    public Guid? faculty_curriculum_uid { get; set; }
    public Guid? entry_academic_year_uid { get; set; }
    public Guid? student_status_uid { get; set; }
    public Guid? pre_academic_register_calendar_uid { get; set; }
    public Guid? pre_register_academic_year_uid { get; set; }
    public Guid? pre_register_academic_semester_uid { get; set; }
    public Guid? late_academic_register_calendar_uid { get; set; }
    public Guid? late_register_academic_year_uid { get; set; }
    public Guid? late_register_academic_semester_uid { get; set; }
    public Guid? add_drop_academic_register_calendar_uid { get; set; }
    public Guid? add_drop_register_academic_year_uid { get; set; }
    public Guid? add_drop_register_academic_semester_uid { get; set; }
    public string id_card_no { get; set; }
    public string register_academic_year_code { get; set; }
    public string register_semester_code { get; set; }
    public Guid? register_semester_uid { get; set; }
    public short? min_credit { get; set; }
    public short? max_credit { get; set; }
    public bool? is_package { get; set; }
    public decimal? package_amount { get; set; }
    //public Guid? provision_register_uid { get; set; }

  }

  public class t_register_token : register_token
  {

  }

  public class v_register_token : register_token
  {

  }

}
