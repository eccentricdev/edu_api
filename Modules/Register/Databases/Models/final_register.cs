using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using SeventyOneDev.Utilities;
using SeventyOneDev.Utilities.Attributes;

namespace edu_api.Modules.Register.Databases
{
  public class final_register:base_table
  {
    [Key]
    public Guid? final_register_uid { get; set; }
    public Guid? student_uid {get;set;}
    public Guid? academic_year_uid {get;set;}
    public Guid? academic_semester_uid {get;set;}
    public string bill_no {get;set;}
    public Guid? bill_uid { get; set; }
    public DateTime? register_date {get;set;}
    public bool? is_package {get;set;}
    public decimal? package_amount {get;set;}
    public decimal? credit_amount {get;set;}
    public decimal? fee_amount {get;set;}
    public decimal? scholarship_amount {get;set;}
    public decimal? loan_amount {get;set;}
    public decimal? advanced_amount {get;set;}
    public short? total_subject {get;set;}
    public short? total_credit {get;set;}
    public decimal? payment_amount {get;set;}
    public string register_type_code {get;set;}
    public short? register_status_id {get;set;}
    public DateTime? paid_datetime {get;set;}
    public decimal? paid_amount { get; set; }
    public string receipt_no { get; set; }
    public int? row_order { get; set; }

  }

  public class t_final_register : final_register
  {
    [Include]public List<t_final_register_subject> final_register_subjects { get; set; }

    public t_final_register()
    {

    }

    public t_final_register(t_provision_register provisionRegister)
    {
      student_uid = provisionRegister.student_uid;
      academic_year_uid = provisionRegister.academic_year_uid;
      academic_semester_uid = provisionRegister.academic_semester_uid;
      register_date = provisionRegister.register_date;
      is_package = provisionRegister.is_package;
      package_amount = provisionRegister.package_amount;
      credit_amount = provisionRegister.credit_amount;
      fee_amount = provisionRegister.fee_amount;
      scholarship_amount = provisionRegister.scholarship_amount;
      loan_amount = provisionRegister.loan_amount;
      advanced_amount = provisionRegister.advanced_amount;
      total_subject = provisionRegister.total_subject;
      total_credit = provisionRegister.total_credit;
      payment_amount = provisionRegister.payment_amount;
      register_type_code = provisionRegister.register_type_code;
      register_status_id = provisionRegister.register_status_id;
      paid_datetime = provisionRegister.paid_datetime;
      paid_amount = provisionRegister.paid_amount;
      receipt_no = provisionRegister.receipt_no;
    }
  }

  public class v_final_register : final_register
  {
    public Guid? entry_academic_year_uid{ get; set; }
    public string entry_academic_year_code { get; set; }
    public Guid? education_type_uid { get; set; }
    public Guid? student_status_uid { get; set; }
    public Guid? college_faculty_uid { get; set; }
    public Guid? faculty_curriculum_uid { get; set; }
    public string academic_year_code { get; set; }
    public string semester_code { get; set; }
    public string education_type_code { get; set; }
    public string student_code { get; set; }
    [Include]public List<v_final_register_subject> final_register_subjects { get; set; }
  }
}
