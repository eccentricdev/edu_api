using System;
using System.ComponentModel.DataAnnotations;
using SeventyOneDev.Utilities;
using SeventyOneDev.Utilities.Attributes;

namespace edu_api.Modules.Register.Databases
{
  public class year_subject_waiting_list:base_table
  {
    [Key]
    public Guid? subject_waiting_list_uid { get; set; }
    public Guid? academic_year_uid { get; set; }
    public Guid? academic_semester_uid { get; set; }
    public Guid? student_uid { get; set; }
    public Guid? provision_register_subject_uid { get; set; }
    public Guid? year_subject_uid { get; set; }
    public Guid? lecture_year_subject_section_uid {get;set;}
    public Guid? lab_year_subject_section_uid {get;set;}
    public Guid? topic_uid { get; set; }
  }
  [GeneratedUidController("api/register/year_subject_waiting_list")]
  public class t_year_subject_waiting_list : year_subject_waiting_list
  {

  }

  public class v_year_subject_waiting_list : year_subject_waiting_list
  {
    public string display_name_th {get;set;}
    public string display_name_en {get;set;}
    public string year_subject_code {get;set;}
    public string year_subject_name_th {get;set;}
    public string year_subject_name_en {get;set;}
    //public string section_code  {get;set;}
    public string lecture_section_code {get;set;}
    public string lab_section_code  {get;set;}
  }
}
