using edu_api.Modules.Register.Databases;
using Microsoft.EntityFrameworkCore;

namespace SeventyOneDev.Utilities
{
  public partial class Db : DbContext
  {
    public DbSet<t_register_token> t_register_token {get;set;}
    public DbSet<v_register_token> v_register_token {get;set;}
    public DbSet<t_provision_register> t_provision_register {get;set;}
    public DbSet<v_provision_register> v_provision_register {get;set;}
    public DbSet<t_provision_register_subject> t_provision_register_subject {get;set;}
    public DbSet<v_provision_register_subject> v_provision_register_subject {get;set;}
    public DbSet<t_provision_register_fee> t_provision_register_fee {get;set;}
    public DbSet<v_provision_register_fee> v_provision_register_fee {get;set;}
    public DbSet<t_provision_register_advance> t_provision_register_advance {get;set;}
    public DbSet<v_provision_register_advance> v_provision_register_advance {get;set;}
    public DbSet<t_provision_register_scholarship> t_provision_register_scholarship {get;set;}
    public DbSet<v_provision_register_scholarship> v_provision_register_scholarship {get;set;}


    public DbSet<t_year_subject_waiting_list> t_year_subject_waiting_list {get;set;}
    public DbSet<v_year_subject_waiting_list> v_year_subject_waiting_list {get;set;}

    public DbSet<t_loan> t_loan {get;set;}
    public DbSet<v_loan> v_loan {get;set;}

    public DbSet<t_loan_semester> t_loan_semester {get;set;}
    public DbSet<v_loan_semester> v_loan_semester {get;set;}
    public DbSet<t_final_register> t_final_register {get;set;}
    public DbSet<v_final_register> v_final_register {get;set;}

    public DbSet<t_final_register_subject> t_final_register_subject {get;set;}
    public DbSet<v_final_register_subject> v_final_register_subject {get;set;}
    //public DbSet<t_pro>

    public DbSet<t_plan_register> t_plan_register {get;set;}
    public DbSet<v_plan_register> v_plan_register {get;set;}
    public DbSet<t_plan_register_subject> t_plan_register_subject {get;set;}
    public DbSet<v_plan_register_subject> v_plan_register_subject {get;set;}
    public DbSet<t_plan_register_fee> t_plan_register_fee {get;set;}
    public DbSet<v_plan_register_fee> v_plan_register_fee {get;set;}

    public DbSet<v_year_subject_section_instructor_summary> v_year_subject_section_instructor_summary {get;set;}
  public DbSet<v_student_register_subject> v_student_register_subject { get; set; }
  public DbSet<v_student_register_subject_detail> v_student_register_subject_detail { get; set; }
  public DbSet<v_year_subject_section_schedule> v_year_subject_section_schedule { get; set; }
  public DbSet<t_applicant_apply> t_applicant_apply { get; set; }
  public DbSet<v_applicant_apply> v_applicant_apply { get; set; }
  public DbSet<v_applicant> v_applicant { get; set; }

    private void OnRegisterModelCreating(ModelBuilder builder, string schema)
    {
      builder.Entity<v_year_subject_section_instructor_summary>()
        .ToView("v_year_subject_section_instructor_summary", schema).HasNoKey();
      builder.Entity<t_register_token>().ToTable("register_token", schema);
      builder.Entity<v_register_token>().ToView("v_register_token", schema);

      builder.Entity<t_provision_register>().ToTable("provision_register", schema);
      builder.Entity<v_provision_register>().ToView("v_provision_register", schema);


      builder.Entity<t_provision_register_subject>().ToTable("provision_register_subject", schema);
      builder.Entity<t_provision_register_subject>().HasOne(p => p.provision_register)
        .WithMany(p => p.provision_register_subjects).HasForeignKey(p => p.provision_register_uid)
        .OnDelete(DeleteBehavior.Cascade);
      builder.Entity<v_provision_register_subject>().ToView("v_provision_register_subject", schema);
      builder.Entity<v_provision_register_subject>().HasOne(p => p.provision_register)
        .WithMany(p => p.provision_register_subjects).HasForeignKey(p => p.provision_register_uid)
        .OnDelete(DeleteBehavior.Cascade);

      builder.Entity<t_provision_register_fee>().ToTable("provision_register_fee", schema);
      builder.Entity<t_provision_register_fee>().HasOne(p => p.provision_register)
        .WithMany(p => p.provision_register_fees).HasForeignKey(p => p.provision_register_uid)
        .OnDelete(DeleteBehavior.Cascade);
      builder.Entity<v_provision_register_fee>().ToView("v_provision_register_fee", schema);
      builder.Entity<v_provision_register_fee>().HasOne(p => p.provision_register)
        .WithMany(p => p.provision_register_fees).HasForeignKey(p => p.provision_register_uid)
        .OnDelete(DeleteBehavior.Cascade);


      builder.Entity<t_provision_register_advance>().ToTable("provision_register_advance", schema);
      builder.Entity<t_provision_register_advance>().HasOne(p => p.provision_register)
        .WithMany(p => p.provision_register_advances).HasForeignKey(p => p.provision_register_uid)
        .OnDelete(DeleteBehavior.Cascade);
      builder.Entity<v_provision_register_advance>().ToView("v_provision_register_advance", schema);
      builder.Entity<v_provision_register_advance>().HasOne(p => p.provision_register)
        .WithMany(p => p.provision_register_advances).HasForeignKey(p => p.provision_register_uid)
        .OnDelete(DeleteBehavior.Cascade);


      builder.Entity<t_provision_register_scholarship>().ToTable("provision_register_scholarship", schema);
      builder.Entity<t_provision_register_scholarship>().HasOne(p => p.provision_register)
        .WithMany(p => p.provision_register_scholarships).HasForeignKey(p => p.provision_register_uid)
        .OnDelete(DeleteBehavior.Cascade);

      builder.Entity<v_provision_register_scholarship>().ToView("v_provision_register_scholarship", schema);
      builder.Entity<v_provision_register_scholarship>().HasOne(p => p.provision_register)
        .WithMany(p => p.provision_register_scholarships).HasForeignKey(p => p.provision_register_uid)
        .OnDelete(DeleteBehavior.Cascade);

      builder.Entity<t_year_subject_waiting_list>().ToTable("year_subject_waiting_list", schema);
      builder.Entity<v_year_subject_waiting_list>().ToView("v_year_subject_waiting_list", schema);

      builder.Entity<t_loan>().ToTable("loan", schema);
      builder.Entity<v_loan>().ToView("v_loan", schema);
      builder.Entity<t_loan_semester>().ToTable("loan_semester", schema);
      builder.Entity<v_loan_semester>().ToView("v_loan_semester", schema);

      builder.Entity<t_final_register>().ToTable("final_register", schema);
      builder.Entity<v_final_register>().ToView("v_final_register", schema);

      builder.Entity<t_final_register_subject>().ToTable("final_register_subject", schema);
      builder.Entity<t_final_register_subject>().HasOne(f => f.final_register).WithMany(f => f.final_register_subjects)
        .HasForeignKey(f => f.final_register_uid).OnDelete(DeleteBehavior.Cascade);
      builder.Entity<v_final_register_subject>().ToView("v_final_register_subject", schema);
      builder.Entity<v_final_register_subject>().HasOne(f => f.final_register).WithMany(f => f.final_register_subjects)
        .HasForeignKey(f => f.final_register_uid).OnDelete(DeleteBehavior.Cascade);



      builder.Entity<t_plan_register>().ToTable("plan_register", schema);
      builder.Entity<v_plan_register>().ToView("v_plan_register", schema);


      builder.Entity<t_plan_register_subject>().ToTable("plan_register_subject", schema);
      builder.Entity<t_plan_register_subject>().HasOne(p => p.plan_register)
        .WithMany(p => p.plan_register_subjects).HasForeignKey(p => p.plan_register_uid)
        .OnDelete(DeleteBehavior.Cascade);
      builder.Entity<v_plan_register_subject>().ToView("v_plan_register_subject", schema);
      builder.Entity<v_plan_register_subject>().HasOne(p => p.plan_register)
        .WithMany(p => p.plan_register_subjects).HasForeignKey(p => p.plan_register_uid)
        .OnDelete(DeleteBehavior.Cascade);

      builder.Entity<t_plan_register_fee>().ToTable("plan_register_fee", schema);
      builder.Entity<t_plan_register_fee>().HasOne(p => p.plan_register)
        .WithMany(p => p.plan_register_fees).HasForeignKey(p => p.plan_register_uid)
        .OnDelete(DeleteBehavior.Cascade);
      builder.Entity<v_plan_register_fee>().ToView("v_plan_register_fee", schema);
      builder.Entity<v_plan_register_fee>().HasOne(p => p.plan_register)
        .WithMany(p => p.plan_register_fees).HasForeignKey(p => p.plan_register_uid)
        .OnDelete(DeleteBehavior.Cascade);

      builder.Entity<v_student_register_subject>().ToView("v_student_register_subject", schema).HasNoKey();
      builder.Entity<v_student_register_subject_detail>().ToView("v_student_register_subject_detail", schema).HasNoKey();
      builder.Entity<v_year_subject_section_schedule>().ToView("v_year_subject_section_schedule", schema).HasNoKey();
      builder.Entity<t_applicant_apply>().ToTable("applicant_apply", "RSUAPP");
      builder.Entity<v_applicant_apply>().ToView("v_applicant_apply", "RSUAPP");
      builder.Entity<v_applicant>().ToView("v_applicant", "RSUAPP");
    }
  }
}
