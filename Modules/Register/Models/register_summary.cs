using System;
using System.Collections.Generic;
using System.Linq;
using payment_core.Models;

namespace edu_api.Modules.Register.Models
{
  public class register_summary
  {
    public int currentCredit {get;set;}
    public int totalCredit {get;set;}
    // public decimal creditFee {get;set;}
    // public decimal academicFee {get;set;}
    // public decimal accidentInsuranceFee {get;set;}
    // public decimal fundAmount {get;set;}
    // public decimal loanAmount {get;set;}
    // public decimal advanceAmount {get;set;}
    // public decimal package {get;set;}
    public decimal totalPaymentAmount {get;set;}
    //public string payInNo {get;set;}
    public List<register_subject> subjects {get;set;}
    public List<payment> credits {get;set;}
    public List<payment> debits {get;set;}
    public payment total {get;set;}
    public string payment_url { get; set; }
    public Guid? provision_register_uid { get; set; }

    public List<bill_request_item> ToBillRequestItem()
    {
      var billRequestItems = this.subjects.Select((r, i) =>
        new bill_request_item()
        {
          row_order = i + 1,
          item_code = r.year_subject_code,
          item_name_th = r.year_subject_name_th,
          item_name_en = r.year_subject_name_th,
          unit_price = r.total_amount,
          quantity = 1,
          discount_type_id = 0,
          discount_amount = 0,
          before_vat_amount = r.total_amount,
          vat_amount = 0,
          amount = r.total_amount,
          sub_item1_name = "LECTURE",
          sub_item1_quantity = 1,
          sub_item1_unit_price = r.total_lecture_amount,
          sub_item1_amount = r.total_lecture_amount,
          sub_item2_name = "LAB",
          sub_item2_quantity = 1,
          sub_item2_unit_price = r.total_lab_amount,
          sub_item2_amount = r.total_lab_amount,
          sub_item3_name = "CREDIT",
          sub_item3_quantity = r.lecture_credit_amount,
          sub_item3_unit_price = 0,
          sub_item3_amount = 0,
          item_ext1_name = r.lecture_section?.section_code,
          item_ext2_name = r.lab_section?.section_code,
          share1_percent = 50,
          share2_percent = 50
        }).ToList();
      var totalSubjectCount = billRequestItems.Count;
      billRequestItems
        .AddRange(this.credits
          .Where(c=>c.type_name!="CREDITFEE")
          .Select((c, i) => new bill_request_item()
      {
        row_order = i + 1 + totalSubjectCount,
        item_code = c.code,
        item_name_th = c.name,
        item_name_en = c.name,
        unit_price = c.amount,
        quantity = 1,
        discount_type_id = 0,
        discount_amount = 0,
        before_vat_amount = c.amount,
        vat_amount = 0,
        amount = c.amount,

      }));
      var totalSubjectCreditCount = billRequestItems.Count;
      billRequestItems.AddRange(this.debits.Select((d, i) => new bill_request_item()
      {
        row_order = i + 1 + totalSubjectCreditCount,
        item_code = d.code,
        item_name_th = d.name,
        item_name_en = d.name,
        unit_price = d.amount,
        quantity = 1,
        discount_type_id = 0,
        discount_amount = 0,
        before_vat_amount = d.amount,
        vat_amount = 0,
        amount = d.amount,
      }));
      return billRequestItems;

    }
  }
}
