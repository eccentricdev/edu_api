using System.Collections.Generic;

namespace edu_api.Modules.Register.Models
{
    public class student_register_subject_request
    {
        public string academic_year_code { get; set; }
        public string semester_code { get; set; }
        public string year_subject_code { get; set; }
    }
    public class student_register_subject_response
    {
        public string academic_year_code { get; set; }
        public string semester_code { get; set; }
        public string year_subject_code { get; set; }
       public List<student_register_subject_section> sections { get; set; }       
    }

    public class student_register_subject_section
    {
        public string section_code { get; set; }
        public List<string> rooms { get; set; }
        public List<string> instructors { get; set; }
        public List<student_register_subject_student> students { get; set; }
    }

    public class student_register_subject_student
    {
        public string student_code { get; set; }
        public string college_faculty_name_th { get; set; }
        public string faculty_curriculum_name_th { get; set; }
        public string grade_name_th { get; set; }
    }
}