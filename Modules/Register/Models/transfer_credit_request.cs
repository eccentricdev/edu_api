using System;
using System.Collections.Generic;

namespace edu_api.Modules.Register.Models
{
    public class transfer_credit_request
    {
        public Guid? student_uid { get; set; }
        public Guid? academic_year_uid { get; set; }
        public Guid? academic_semester_uid { get; set; }
        public List<transfer_credit_subject> transfer_credit_subjects { get; set; }
    }

    public class transfer_credit_subject
    {
        public Guid? year_subject_uid { get; set; }
        public Guid? grade_uid { get; set; }
    }
    
}