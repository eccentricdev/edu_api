using System;
using edu_api.Modules.Study.Databases.Models;

namespace edu_api.Modules.Register.Models
{
  public class register_subject_section_year
  {
    public Guid? entry_academic_year_uid { get; set; }
    public Guid? year_subject_section_uid { get; set; }

    public register_subject_section_year()
    {

    }

    public register_subject_section_year(t_year_subject_section_year yearSubjectSectionYear)
    {
      entry_academic_year_uid = yearSubjectSectionYear.entry_academic_year_uid;
      year_subject_section_uid = yearSubjectSectionYear.year_subject_section_uid;
    }
  }
}
