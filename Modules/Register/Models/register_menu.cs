namespace edu_api.Modules.Register.Models
{
  public class register_menu
  {
    public string code { get; set; }
    public string name { get; set; }
    public bool visible { get; set; }
    public bool enable { get; set; }

    public register_menu()
    {
    }

    public register_menu(string code, string name, bool visible, bool enable)
    {
      this.code = code;
      this.name = name;
      this.visible = visible;
      this.enable = enable;
    }


  }
}
