using System;
using edu_api.Modules.Study.Databases.Models;

namespace edu_api.Modules.Register.Models
{
  public class register_subject_section_detail
  {
    public Guid? section_uid { get; set; }
    public string section_code { get; set; }
    public string study_time { get; set; }
    public string study_time_text { get; set; }

    public register_subject_section_detail()
    {

    }

    public register_subject_section_detail(Guid? sectionUid,string sectionCode, string studyTime,string studyTimeText)
    {
      this.section_uid = sectionUid;
      this.section_code = sectionCode;
      this.study_time = studyTime;
      this.study_time_text = studyTimeText;
    }

    public register_subject_section_detail(register_subject_section labSection)
    {
      this.section_uid = labSection.year_subject_section_uid;
      this.section_code = labSection.section_code;
      this.study_time = labSection.study_time;
      this.study_time_text = labSection.study_time_text_en;
    }
  }
}
