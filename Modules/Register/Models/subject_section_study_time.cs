using System;

namespace edu_api.Modules.Register.Models
{
  public class subject_section_study_time
  {
    public Guid? year_subject_section_uid { get; set; }
    public short? section_type_id { get; set; }
    public TimeSpan? study_from_time { get; set; }
    public TimeSpan? study_to_time { get; set; }
    public string study_time_text_en { get; set; }
    //TimeSpan( 10, 20, 30, 40, 50 )
    //string.Format("{0} ({1}-{2})",s.studyDay,s.studyStartTime.Value.ToString(@"hh\:mm"),s.studyEndTime.Value.ToString(@"hh\:mm"))));
  }
}
