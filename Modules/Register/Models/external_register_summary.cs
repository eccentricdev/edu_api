namespace edu_api.Modules.Register.Models
{
  public class external_register_summary
  {
    public string academic_year_code { get; set; }
    public string semester_code { get; set; }
    public string education_type_code { get; set; }
    public string student_code { get; set; }
    public decimal? payment_amount { get; set; }
  }
}
