using System;

namespace edu_api.Modules.Register.Models
{
  public class DataException : Exception
  {
    public string referenceMessage {get;set;}
    public DataException(string message) : base(message)
    {
    }
    public DataException(string message,string referenceMessage) : base(message)
    {
      this.referenceMessage = referenceMessage;
    }
  }
}
