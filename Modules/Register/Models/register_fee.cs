using System;
using edu_api.Modules.Register.Models.Student;

namespace edu_api.Modules.Register.Models
{
  public class register_fee
  {
    public Guid? register_fee_detail_uid { get; set; }
    public Guid? register_fee_uid { get; set; }
    public string fee_name_th { get; set; }
    public string fee_name_en { get; set; }
    public decimal? fee_amount { get; set; }
    public string fee_code { get; set; }

    public register_fee()
    {
    }

    public register_fee(counter_register_fee studentRegisterFee)
    {

    }
  }
}
