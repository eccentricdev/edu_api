using System.Collections.Generic;
using edu_api.Modules.Fee.Databases.Models;

namespace edu_api.Modules.Register.Models
{
  public class scholarship_response
  {
  public bool status { get; set; }
  public string message { get; set; }
  public List<scholarship> results { get; set; }
  }
  public class scholarship
  {
    public string scholarship_code { get; set; }
    public string scholarship_name { get; set; }

    public decimal? used_amount { get; set; }
    public List<scholarship_fee> fees { get; set; }
    public List<scholarship_expense> expense { get; set; }
  }

  public class scholarship_fee
  {
    public string feeCode { get; set; }
    public string feeName { get; set; }
    public string amountType { get; set; }
    public decimal amount { get; set; }
  }

  public class scholarship_expense
  {
    public int expenseId { get; set; }
    public string expenseName { get; set; }
    public string amountType { get; set; }
    public decimal amount { get; set; }
  }

}
