namespace edu_api.Modules.Register.Models
{
  public class register_advance_payment
  {

    public string advance_no { get; set; }
    public decimal? left_amount { get; set; }
  }
}
