using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace edu_api.Modules.Register.Models
{
 // [Table("payment_request",Schema="RSUAPP")]
  public class payment_update
  {
    [Key,DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public int? payment_request_id {get;set;}
    [MaxLength(10)]
    public string application_code {get;set;}
    [MaxLength(50)]
    public string application_key {get;set;}
    public decimal? amount {get;set;}
    [MaxLength(10)]
    public string payment_method_code {get;set;}
    public int? payin_no {get;set;}
    [Column(TypeName = "decimal(18,4)")]
    public decimal? fee_amount {get;set;}
    [Column(TypeName = "decimal(18,4)")]
    public decimal? total_amount {get;set;}
    [MaxLength(50)]
    public string invoice {get;set;}
    [MaxLength(50)]
    public string ref1 {get;set;}
    [MaxLength(50)]
    public string ref2 {get;set;}
    public DateTime? paid_datetime {get;set;}
    [MaxLength(50)]
    public string channel {get;set;}
    [Column(TypeName = "decimal(18,4)")]
    public decimal? paid_amount {get;set;}
    [MaxLength(50)]
    public string ref_code {get;set;}
    public bool? active_flag {get;set;}
    public bool? update_flag {get;set;}
    //public int?
  }
}
