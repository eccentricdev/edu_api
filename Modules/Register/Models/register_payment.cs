namespace edu_api.Modules.Register.Models
{
  public class register_payment
  {
    public int? payment_method_id { get; set; }
    public decimal paid_amount { get; set; }
    public string reference_code { get; set; }
    public string remark { get; set; }
  }
}
