namespace edu_api.Modules.Register.Models
{
  public class exam_time_range
  {
    public string subject_code { get; set; }
    public long from_time { get; set; }
    public long to_time { get; set; }

    public exam_time_range()
    {

    }

    public exam_time_range(string subjectCode, long fromTime, long toTime)
    {
      this.subject_code = subjectCode;
      this.from_time = fromTime;
      this.to_time = toTime;
    }
  }
}
