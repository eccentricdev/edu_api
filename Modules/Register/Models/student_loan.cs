namespace edu_api.Modules.Register.Models
{
  public class student_loan
  {
    public string contract_no { get; set; }
    public decimal? loan_amount { get; set; }
    public decimal? use_amount { get; set; }
  }
}
