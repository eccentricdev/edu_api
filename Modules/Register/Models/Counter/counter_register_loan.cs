using System;

namespace edu_api.Modules.Register.Models.Student
{
  public class counter_register_loan
  {
    //public Guid? loan { get; set; }
    //public Guid? academic_year_uid { get; set; }
    public string contract_no { get; set; }
    public decimal? loan_amount { get; set; }
    public decimal? use_amount { get; set; }

    public counter_register_loan()
    {

    }

    public counter_register_loan(student_loan studentLoan)
    {
      contract_no = studentLoan.contract_no;
      loan_amount = studentLoan.loan_amount;
      use_amount = studentLoan.use_amount;
    }
  }

}
