namespace edu_api.Modules.Register.Models.Student
{
  using System;
  using System.Collections.Generic;

  namespace edu_api.Modules.Register.Models
  {
    public class counter_register_detail
    {
      public decimal min_credit {get;set;}
      public decimal max_credit {get;set;}
      public Guid academic_year_uid {get;set;}
      public Guid academic_semester_uid {get;set;}
      public bool allow_register {get;set;}
      public bool is_readonly {get;set;}
      //public string messageEn {get;set;}
      //public string messageTh {get;set;}
      public string message {get;set;}
      public int total_credit {get;set;}
      public string register_type_code {get;set;}
      public List<register_subject> register_subjects {get;set;}
      public List<counter_register_fee> fees { get; set; }
      public List<counter_register_advance_payment> advance_payments { get; set; }
      public List<counter_register_scholarship> scholarships { get; set; }
      public counter_register_loan loan { get; set; }
      public List<payment> credits {get;set;}
      public List<payment> debits {get;set;}
      public payment total {get;set;}
      public string last_payment_date {get;set;}
      public string payment_url { get; set; }
      public bool? is_package { get; set; }
      public decimal? package_amount { get; set; }
      public counter_register_detail()
      {
      }

      public void Update(register_summary registerSummary)
      {
        this.credits = registerSummary?.credits;
        this.debits = registerSummary?.debits;
        this.total = registerSummary?.total;

      }

      // public register_detail()
      // {
      // }
      //
      // public void Update(register_summary registerSummary)
      // {
      //   this.credits = registerSummary.credits;
      //   this.debits = registerSummary.debits;
      // }


    }
  }

}
