using System.Collections.Generic;

namespace edu_api.Modules.Register.Models.Student
{
  public class counter_register_scholarship
  {
    public string scholarship_code { get; set; }
    public string scholarship_name { get; set; }
    public decimal? used_amount { get; set; }
    public List<scholarship_fee> fees { get; set; }
    public List<scholarship_expense> expense { get; set; }

    public counter_register_scholarship()
    {
    }

    public counter_register_scholarship(scholarship scholarship)
    {
      scholarship_code = scholarship.scholarship_code;
      scholarship_name = scholarship.scholarship_name;
      fees = scholarship.fees;
      expense = scholarship.expense;
    }
  }


}
