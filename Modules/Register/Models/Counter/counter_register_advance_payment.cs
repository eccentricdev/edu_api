namespace edu_api.Modules.Register.Models.Student
{
  public class counter_register_advance_payment
  {
    public string advance_no { get; set; }
    //public string advance_amount { get; set; }
    public decimal? left_amount { get; set; }
    public bool? is_use { get; set; }

    public counter_register_advance_payment()
    {

    }

    public counter_register_advance_payment(register_advance_payment registerAdvancePayment)
    {
      advance_no = registerAdvancePayment.advance_no;
      left_amount = registerAdvancePayment.left_amount;
    }
  }
}
