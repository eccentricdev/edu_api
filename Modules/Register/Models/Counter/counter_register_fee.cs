using System;

namespace edu_api.Modules.Register.Models.Student
{
  public class counter_register_fee
  {
    public Guid? register_fee_detail_uid { get; set; }
    public Guid? register_fee_uid { get; set; }
    public string fee_name_th { get; set; }
    public string fee_name_en { get; set; }
    public decimal? fee_amount { get; set; }
    public string fee_code { get; set; }
    public decimal? actual_amount { get; set; }

    public counter_register_fee()
    {
    }

    public counter_register_fee(register_fee registerFee)
    {
      fee_name_th = registerFee.fee_name_th;
      fee_name_en = registerFee.fee_name_en;
      fee_code = registerFee.fee_code;
      fee_amount = registerFee.fee_amount;
    }
  }
}
