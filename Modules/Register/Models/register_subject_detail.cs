using System;
using System.Collections.Generic;

namespace edu_api.Modules.Register.Models
{
  public class register_subject_detail
  {
    public Guid? provision_register_subject_uid { get; set; }
    public Guid? year_subject_uid {get;set;}
    public string year_subject_code {get;set;}
    public string year_subject_name_th {get;set;}
    public string year_subject_name_en {get;set;}
    public short? credit {get;set;}
    public decimal? lecture_credit_amount { get; set; }
    public decimal? total_lab_amount {get;set;}
    public decimal? total_lecture_amount {get;set;}
    public decimal? total_amount { get; set; }
    public bool? is_topic_subject {get;set;}
    public string status {get;set;}
    public string flag {get;set;}
    public DateTime? midterm_lecture_exam_from_datetime { get; set; }
    public DateTime? midterm_lecture_exam_to_datetime { get; set; }
    public DateTime? midterm_lab_exam_from_datetime { get; set; }
    public DateTime? midterm_lab_exam_to_datetime { get; set; }
    public DateTime? final_lecture_exam_from_datetime { get; set; }
    public DateTime? final_lecture_exam_to_datetime { get; set; }
    public DateTime? final_lab_exam_from_datetime { get; set; }
    public DateTime? final_lab_exam_to_datetime { get; set; }
    public List<register_subject_section> lab_sections { get; set; }
    public List<register_subject_section> lecture_sections { get; set; }
    public register_subject_detail() {}

    public register_subject_detail(register_subject subject)
    {
      this.year_subject_uid = subject.year_subject_uid;
      //this.educationType = "";
      this.year_subject_code = subject.year_subject_code;
      this.year_subject_name_en = subject.year_subject_name_en;
      this.year_subject_name_th = subject.year_subject_name_th;
      this.credit = subject.credit ?? 0;
      this.lecture_credit_amount = subject.lecture_credit_amount;
      this.total_lab_amount = subject.total_lab_amount ?? 0;
      this.total_lecture_amount = subject.total_lecture_amount ?? 0;
      this.total_amount = subject.total_amount ?? 0;
      this.is_topic_subject = false;
      this.status = "NORMAL";

    }
  }
}
