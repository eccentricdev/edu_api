using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace edu_api.Modules.Register.Models
{
  public class subject_study_time
  {
    public TimeSpan? study_to_time { get; set; }
    public TimeSpan? study_from_time { get; set; }
    public string subject_code { get; set; }
    public static List<subject_study_time> Parse(string studyTimeText,string subjectCode)
    {
      var subjectStudyTimes = new List<subject_study_time>();
      var studyTimes = studyTimeText.Split(",");
      foreach(var studyTime in studyTimes)
      {
        //TUE (13:00-15:49)
        var pattern = "^(?<day>.{3})\\s\\((?<start>\\d{1,2}:\\d{2})-(?<end>\\d{1,2}:\\d{2})\\)$";
        //Regex expression = new Regex(pattern);



        var matches=Regex.Matches(studyTime,pattern);
        if (matches.Count>0)
        {
          var day = matches[0].Groups[1].Value switch
          {
            "MON" => 1,
            "TUE" => 2,
            "WEN" => 3,
            "THU" => 4,
            "FRI" => 5,
            "SAT" => 6,
            "SUN" => 7,
            _ => 0
          };
          var fromTime = TimeSpan.Parse(matches[0].Groups[2].Value);
          fromTime = fromTime.Add(new TimeSpan(day, 0, 0, 0));
          var toTime = TimeSpan.Parse(matches[0].Groups[3].Value);
          toTime = toTime.Add(new TimeSpan(day, 0, 0, 0));
          var s = new subject_study_time(){
            study_from_time = fromTime,
            study_to_time = toTime,
            subject_code=subjectCode
          };
          subjectStudyTimes.Add(s);


        };
      }
      return subjectStudyTimes;
    }
  }
}
