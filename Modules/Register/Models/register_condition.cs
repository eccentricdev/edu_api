namespace edu_api.Modules.Register.Models
{
  public class register_condition
  {
      public bool missing_evaluation {get;set;}
      public bool has_financial_debt {get;set;}
      public bool has_dorm_debt  {get;set;}
      public bool advisor_not_approve {get;set;}

  }
}
