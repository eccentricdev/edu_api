using System;
using edu_api.Modules.Register.Databases;
using edu_api.Modules.Study.Databases.Models;

namespace edu_api.Modules.Register.Models
{
  public class register_subject
  {
    public Guid? year_subject_uid { get; set; }
    public Guid? subject_uid { get; set; }
    public Guid? education_type_uid { get;set; }
    public string year_subject_code { get; set; }
    public string year_subject_name_th { get; set; }
    public string year_subject_name_en { get; set; }
    public short? credit { get; set; }
    public decimal? lecture_credit_amount { get; set; }
    public decimal? total_lecture_amount {get;set;}
    public decimal? total_lab_amount {get;set;}
    public decimal? total_amount {get;set;}
    public short? register_subject_status_id { get; set; }
    public short? register_subject_flag_id { get; set; }

    public DateTime? midterm_lecture_exam_from_datetime { get; set; }
    public DateTime? midterm_lecture_exam_to_datetime { get; set; }
    public DateTime? midterm_lab_exam_from_datetime { get; set; }
    public DateTime? midterm_lab_exam_to_datetime { get; set; }
    public DateTime? final_lecture_exam_from_datetime { get; set; }
    public DateTime? final_lecture_exam_to_datetime { get; set; }
    public DateTime? final_lab_exam_from_datetime { get; set; }
    public DateTime? final_lab_exam_to_datetime { get; set; }

    public register_subject_section lecture_section { get; set; }
    public register_subject_section lab_section { get; set; }

    public register_subject()
    {

    }

    public register_subject(t_year_subject yearSubject)
    {
      this.year_subject_uid = yearSubject.year_subject_uid;
      //this.subject_uid = yearSubject.subject_uid;
      this.year_subject_code = yearSubject.year_subject_code;
      this.year_subject_name_th = yearSubject.year_subject_name_th;
      this.year_subject_name_en = yearSubject.year_subject_name_en;
      this.subject_uid = yearSubject.subject_uid;
      this.education_type_uid = yearSubject.education_type_uid;
      // this.credit = yearSubject.credit;
      // lecture_credit_amount = yearSubject.lecture_amount;
      // total_lab_amount = yearSubject.lab_amount;
      // total_lecture_amount = yearSubject.lecture_amount * yearSubject.credit;
      // total_amount = (total_lab_amount ?? 0) + (total_lecture_amount ?? 0);


    }
    public register_subject(v_year_subject yearSubject)
    {
      this.year_subject_uid = yearSubject.year_subject_uid;
      //this.subject_uid = yearSubject.subject_uid;
      this.year_subject_code = yearSubject.year_subject_code;
      this.year_subject_name_th = yearSubject.year_subject_name_th;
      this.year_subject_name_en = yearSubject.year_subject_name_en;
      this.subject_uid = yearSubject.subject_uid;
      this.education_type_uid = yearSubject.education_type_uid;
      // this.credit = yearSubject.credit;
      // lecture_credit_amount = yearSubject.lecture_amount;
      // total_lab_amount = yearSubject.lab_amount;
      // total_lecture_amount = yearSubject.lecture_amount * yearSubject.credit;
      // total_amount = (total_lab_amount ?? 0) + (total_lecture_amount ?? 0);


    }
    public register_subject(year_subject_cache yearSubject)
    {
      this.year_subject_uid = yearSubject.year_subject_uid;
      //this.subject_uid = yearSubject.subject_uid;
      this.year_subject_code = yearSubject.year_subject_code;
      this.year_subject_name_th = yearSubject.year_subject_name_th;
      this.year_subject_name_en = yearSubject.year_subject_name_en;
      this.subject_uid = yearSubject.subject_uid;
      this.education_type_uid = yearSubject.education_type_uid;
      // this.credit = yearSubject.credit;
      // lecture_credit_amount = yearSubject.lecture_amount;
      // total_lab_amount = yearSubject.lab_amount;
      // total_lecture_amount = yearSubject.lecture_amount * yearSubject.credit;
      // total_amount = (total_lab_amount ?? 0) + (total_lecture_amount ?? 0);


    }

    public register_subject(t_provision_register_subject provisionRegisterSubject)
    {
      this.year_subject_uid = provisionRegisterSubject.year_subject_uid;
      this.year_subject_code = provisionRegisterSubject.year_subject_code;
      this.year_subject_name_en = provisionRegisterSubject.year_subject_name_en;
      this.year_subject_name_th = provisionRegisterSubject.year_subject_name_th;
      this.credit = provisionRegisterSubject.credit;
      this.register_subject_status_id = provisionRegisterSubject.register_subject_status_id;
      this.register_subject_flag_id = provisionRegisterSubject.register_subject_flag_id;
      if (provisionRegisterSubject.lecture_year_subject_section_uid.HasValue)
      {
        this.lecture_section = new register_subject_section()
        {
          year_subject_section_uid = provisionRegisterSubject.lecture_year_subject_section_uid,
          section_code = provisionRegisterSubject.lecture_section_code,
          study_time = provisionRegisterSubject.lecture_study_time,
          study_time_text_en = provisionRegisterSubject.lecture_study_time_text,
          //topic_code = provisionRegisterSubject.topic_code,
          topic_uid = provisionRegisterSubject.topic_uid,
          topic_name_th = provisionRegisterSubject.topic_name_th,
          topic_name_en = provisionRegisterSubject.topic_name_en,
          topic_credit = provisionRegisterSubject.topic_credit,

        };

      }
      if (provisionRegisterSubject.lab_year_subject_section_uid.HasValue)
      {
        this.lab_section = new register_subject_section()
        {
          year_subject_section_uid = provisionRegisterSubject.lab_year_subject_section_uid,
          section_code = provisionRegisterSubject.lab_section_code,
          study_time = provisionRegisterSubject.lab_study_time,
          study_time_text_en = provisionRegisterSubject.lab_study_time_text,

        };

      }

      //this.subject_type_id = 0;
      this.lecture_credit_amount = provisionRegisterSubject.lecture_credit_amount;
      this.total_lab_amount = provisionRegisterSubject.total_lab_amount;
      this.total_lecture_amount = provisionRegisterSubject.total_lecture_amount;
    this.total_amount = provisionRegisterSubject.total_amount;

    this.midterm_lecture_exam_from_datetime = provisionRegisterSubject.midterm_lecture_exam_from_datetime;
    this.midterm_lecture_exam_to_datetime = provisionRegisterSubject.midterm_lecture_exam_to_datetime;
    this.midterm_lab_exam_from_datetime = provisionRegisterSubject.midterm_lab_exam_from_datetime;
    this.midterm_lab_exam_to_datetime = provisionRegisterSubject.midterm_lab_exam_to_datetime;
    this.final_lecture_exam_from_datetime = provisionRegisterSubject.final_lecture_exam_from_datetime;
    this.final_lecture_exam_to_datetime = provisionRegisterSubject.final_lecture_exam_to_datetime;
    this.final_lab_exam_from_datetime = provisionRegisterSubject.final_lab_exam_from_datetime;
    this.final_lab_exam_to_datetime = provisionRegisterSubject.final_lab_exam_to_datetime;

    }
    public register_subject(v_final_register_subject finalRegisterSubject)
    {
      this.year_subject_uid = finalRegisterSubject.year_subject_uid;
      //this.subject_uid = finalRegisterSubject.subject_uid;
      this.year_subject_code = finalRegisterSubject.year_subject_code;
      this.year_subject_name_th = finalRegisterSubject.year_subject_name_th;
      this.year_subject_name_en = finalRegisterSubject.year_subject_name_en;
      this.subject_uid = finalRegisterSubject.subject_uid;
      this.education_type_uid = finalRegisterSubject.education_type_uid;
      this.credit = finalRegisterSubject.credit;
      lecture_credit_amount = finalRegisterSubject.lecture_credit_amount;
      total_lab_amount = finalRegisterSubject.total_lab_amount;
      total_lecture_amount = finalRegisterSubject.lecture_credit_amount * finalRegisterSubject.credit;
      total_amount = (total_lab_amount ?? 0) + (total_lecture_amount ?? 0);


    }
    // public register_subject(t_regi yearSubject)
    // {
    //   this.year_subject_uid = yearSubject.year_subject_uid;
    //   this.subject_uid = yearSubject.subject_uid;
    //   this.subject_code = yearSubject.year_subject_code;
    //   this.year_subject_name_th = yearSubject.year_subject_name_th;
    //   this.year_subject_name_en = yearSubject.year_subject_name_en;
    //   this.credit = yearSubject.credit;
    // }
  }
}
