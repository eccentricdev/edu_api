using System;

namespace edu_api.Modules.Register.Models
{
  public class register_token_detail
  {
    //public string token { get; set; }
    public Guid student_uid { get; set; }
    public Guid education_type_uid { get; set; }
    public Guid college_faculty_uid { get; set; }
    public Guid faculty_curriculum_uid { get; set; }
    public Guid entry_academic_year_uid { get; set; }
    public Guid student_status_uid { get; set; }
    public Guid academic_register_calendar_uid { get; set; }
    public Guid register_academic_year_uid { get; set; }
    public Guid register_academic_semester_uid { get; set; }
    public string id_card_no { get; set; }
    public string register_academic_year_code { get; set; }
    public string register_semester_code { get; set; }
    public Guid register_semester_uid { get; set; }
    public short? min_credit { get; set; }
    public short? max_credit { get; set; }
    public bool? is_package { get; set; }
    public decimal? package_amount { get; set; }
  }
}
