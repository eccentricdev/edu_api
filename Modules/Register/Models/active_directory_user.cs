namespace edu_api.Modules.Register.Models
{
  public class active_directory_user
  {
    public string student_code { get; set; }
    public string first_name { get; set; }
    public string last_name { get; set; }
    public string email { get; set; }
    public string login_name { get; set; }
    public string password { get; set; }
    public string ou { get; set; }
  }
}
