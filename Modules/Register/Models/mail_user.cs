namespace edu_api.Modules.Register.Models
{
  public class mail_user
  {
    public string primaryEmail { get; set; }
    public mail_user_name name { get; set; }
    public string password { get; set; }
    public bool? changePasswordAtNextLogin { get; set; }
  }

  public class mail_user_name
  {
    public string givenName { get; set; }
    public string familyName { get; set; }
    public string fullName { get; set; }
  }

}
