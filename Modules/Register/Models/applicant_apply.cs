using System;

namespace edu_api.Modules.Register.Models
{
  public class applicant_apply
  {
    public long? application_id { get; set; }
    public string first_name_th { get; set; }
    public string middle_name_th { get; set; }
    public string last_name_th { get; set; }
    public string first_name_en { get; set; }
    public string middle_name_en { get; set; }
    public string last_name_en { get; set; }
    public string display_name_th { get; set; }
    public string display_name_en { get; set; }
    public string academic_year_code { get; set; }
    public string academic_semester_code { get; set; }
    public Guid? college_faculty_uid { get; set; }
    public string college_faculty_code { get; set; }
    public string college_faculty_name_th { get; set; }
    public string college_faculty_name_en { get; set; }
    public Guid? faculty_curriculum_uid { get; set; }
    public string faculty_curriculum_code { get; set; }
    public string faculty_curriculum_name_th { get; set; }
    public string faculty_curriculum_name_en { get; set; }
    public string program_code { get; set; }
    public string program_name_th { get; set; }
    public string program_name_en { get; set; }
    public int? applicant_id { get; set; }
    public int? program_id { get; set; }

  }

}
