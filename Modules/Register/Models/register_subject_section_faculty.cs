using System;
using edu_api.Modules.Study.Databases.Models;

namespace edu_api.Modules.Register.Models
{
  public class register_subject_section_faculty
  {
    public Guid? college_faculty_uid { get; set; }
    public Guid? year_subject_section_uid { get; set; }

    public register_subject_section_faculty()
    {

    }

    public register_subject_section_faculty(t_year_subject_section_faculty yearSubjectSectionFaculty)
    {
      college_faculty_uid = yearSubjectSectionFaculty.college_faculty_uid;
      year_subject_section_uid = yearSubjectSectionFaculty.year_subject_section_uid;
    }
    public register_subject_section_faculty(v_year_subject_section_faculty yearSubjectSectionFaculty)
    {
      college_faculty_uid = yearSubjectSectionFaculty.college_faculty_uid;
      year_subject_section_uid = yearSubjectSectionFaculty.year_subject_section_uid;
    }
  }
}
