using System;
using edu_api.Modules.Study.Databases.Models;

namespace edu_api.Modules.Register.Models
{
  public class register_subject_section
  {
    public Guid? year_subject_section_uid {get;set;}
    public Guid? year_subject_uid {get;set;}
    public string section_code {get;set;}
    public short? section_type_id {get;set;}
    public Guid? topic_uid {get;set;}
    public short? quota {get;set;}
    public short? reserved {get;set;}
    public short? confirmed {get;set;}
    public string topic_name_th {get;set;}
    public string topic_name_en {get;set;}
    public short? topic_credit {get;set;}
    public string study_time_text_en {get;set;}
    public string study_time { get; set; }
    public bool? is_full { get; set; }
    public string building_name_th { get; set; }
    public string room_name_th { get; set; }
    public register_subject_section()
    {

    }

    public register_subject_section(t_year_subject_section yearSubjectSection)
    {
      this.year_subject_section_uid = yearSubjectSection.year_subject_section_uid;
      this.year_subject_uid = yearSubjectSection.year_subject_uid;
      this.section_code = yearSubjectSection.section_code;
      this.topic_uid = yearSubjectSection.subject_topic_uid;
      this.quota = yearSubjectSection.quota;
      this.reserved = yearSubjectSection.reserved;
      this.confirmed = yearSubjectSection.confirmed;
      this.section_type_id = yearSubjectSection.section_type_id;
      this.topic_name_th = yearSubjectSection.topic_name_th;
      this.topic_name_en = yearSubjectSection.topic_name_en;
      this.topic_credit = yearSubjectSection.topic_credit;
      
      //this.study_time_text_en yearSubjectSection.

    }
    public register_subject_section(v_year_subject_section yearSubjectSection)
    {
      this.year_subject_section_uid = yearSubjectSection.year_subject_section_uid;
      this.year_subject_uid = yearSubjectSection.year_subject_uid;
      this.section_code = yearSubjectSection.section_code;
      this.topic_uid = yearSubjectSection.subject_topic_uid;
      this.quota = yearSubjectSection.quota;
      this.reserved = yearSubjectSection.reserved;
      this.confirmed = yearSubjectSection.confirmed;
      this.section_type_id = yearSubjectSection.section_type_id;
      this.topic_name_th = yearSubjectSection.topic_name_th;
      this.topic_name_en = yearSubjectSection.topic_name_en;
      this.topic_credit = yearSubjectSection.topic_credit;
      this.room_name_th = yearSubjectSection.room_name_th;
      this.building_name_th = yearSubjectSection.building_name_th;
      //this.study_time_text_en yearSubjectSection.

    }
  }
}
