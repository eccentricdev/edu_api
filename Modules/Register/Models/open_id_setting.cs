using System.Collections.Generic;

namespace edu_api.Modules.Register.Models
{
  public class open_id_setting
  {

    public string authority { get; set; }
    public List<string> scopes { get; set; }
    public string client_id { get; set; }
    public string client_secret { get; set; }

  }
}
