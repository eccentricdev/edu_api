using System;
using System.Collections.Generic;

namespace edu_api.Modules.Register.Models
{
  public class validate_response
  {
    // public string name { get; set; }
    // public string asName { get; set; }
    // public string refreshToken { get; set; }
    // public string token { get; set; }
    //
    // public string tokenExpiryDate { get; set; }
    // public List<string> roles { get; set; }
    public Guid? register_token_uid { get; set; }
    public List<register_menu> menus { get; set; }
    public bool is_eligible { get; set; }
    public string message { get; set; }
    public string register_type_code { get; set; }
    public string register_status_code { get; set; }
    public short? min_credit { get; set; }
    public short? max_credit { get; set; }


  }
}
