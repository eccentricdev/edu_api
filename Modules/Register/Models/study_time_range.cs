namespace edu_api.Modules.Register.Models
{
  public class study_time_range
  {
    public string subject_code { get; set; }
    public double from_time { get; set; }
    public double to_time { get; set; }

    public study_time_range()
    {
    }

    public study_time_range(string subjectCode, double fromTime, double toTime)
    {
      this.subject_code = subjectCode;
      this.from_time = fromTime;
      this.to_time = toTime;
    }
  }
}
