namespace edu_api.Modules.Register.Models
{
  public class register_subject_cost
  {
    public short? credit { get; set; }
    public decimal? lecture_amount { get; set; }
    public decimal? lab_amount { get; set; }
    public decimal? practice_amount { get; set; }
  }
}
