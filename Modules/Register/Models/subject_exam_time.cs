using System;
using System.Collections.Generic;

namespace edu_api.Modules.Register.Models
{
  public class subject_exam_time
  {
    public DateTime? exam_start_time {get;set;}
    public DateTime? exam_end_time {get;set;}
    public string subject_code {get;set;}

    public static List<subject_exam_time> Parse(register_subject registerSubject, string subjectCode)
    {
      List<subject_exam_time> subjectExamTimes = new List<subject_exam_time>();
      if (registerSubject.midterm_lecture_exam_from_datetime.HasValue &&
          registerSubject.midterm_lecture_exam_to_datetime.HasValue)
      {
        subjectExamTimes.Add(new subject_exam_time(){exam_start_time = registerSubject.midterm_lecture_exam_from_datetime,exam_end_time = registerSubject.midterm_lecture_exam_to_datetime,subject_code = subjectCode});
      }
      if (registerSubject.midterm_lab_exam_from_datetime.HasValue &&
          registerSubject.midterm_lab_exam_to_datetime.HasValue)
      {
        subjectExamTimes.Add(new subject_exam_time(){exam_start_time = registerSubject.midterm_lab_exam_from_datetime,exam_end_time = registerSubject.midterm_lab_exam_to_datetime,subject_code = subjectCode});
      }
      if (registerSubject.final_lecture_exam_from_datetime.HasValue &&
          registerSubject.final_lecture_exam_to_datetime.HasValue)
      {
        subjectExamTimes.Add(new subject_exam_time(){exam_start_time = registerSubject.final_lecture_exam_from_datetime,exam_end_time = registerSubject.final_lecture_exam_to_datetime,subject_code = subjectCode});
      }
      if (registerSubject.final_lab_exam_from_datetime.HasValue &&
          registerSubject.final_lab_exam_to_datetime.HasValue)
      {
        subjectExamTimes.Add(new subject_exam_time(){exam_start_time = registerSubject.final_lab_exam_from_datetime,exam_end_time = registerSubject.final_lab_exam_to_datetime,subject_code = subjectCode});
      }

      return subjectExamTimes;
    }
  }
}
