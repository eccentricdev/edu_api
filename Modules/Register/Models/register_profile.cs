using System;

namespace edu_api.Modules.Register.Models
{
  public class register_profile
  {
    public Guid? student_uid { get; set; }
    public string student_code { get; set; }
    public string display_name_th { get; set; }
    public string display_name_en { get; set; }
    public Guid? entry_academic_year_uid { get; set; }
    public Guid? entry_semester_uid { get; set; }
    public Guid? education_type_uid { get; set; }
    public Guid? college_faculty_uid { get; set; }
    public Guid? faculty_curriculum_uid { get; set; }
    public Guid? student_status_uid { get; set; }
    public string citizen_id { get; set; }
  }
}
