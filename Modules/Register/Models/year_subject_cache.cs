using System;

namespace edu_api.Modules.Register.Models
{
    public class year_subject_cache
    {
        public Guid? year_subject_uid { get; set; }
        public string year_subject_code { get; set; }
        public string year_subject_name_th { get; set; }
        public string year_subject_name_en { get; set; }
        public Guid? subject_uid { get; set; }
        public Guid? education_type_uid { get; set; }
    }
}