using System;
using edu_api.Modules.Study.Databases.Models;

namespace edu_api.Modules.Register.Models
{
  public class register_subject_exam
  {
    public DateTime? midterm_lecture_exam_from_datetime { get; set; }
    public DateTime? midterm_lecture_exam_to_datetime { get; set; }
    public string midterm_lecture_exam_time_text { get; set; }

    public DateTime? midterm_lab_exam_from_datetime { get; set; }
    public DateTime? midterm_lab_exam_to_datetime { get; set; }
    public string midterm_lab_exam_time_text { get; set; }

    public DateTime? final_lecture_exam_from_datetime { get; set; }
    public DateTime? final_lecture_exam_to_datetime { get; set; }
    public string final_lecture_exam_time_text { get; set; }

    public DateTime? final_lab_exam_from_datetime { get; set; }
    public DateTime? final_lab_exam_to_datetime { get; set; }
    public string final_lab_exam_time_text { get; set; }

    public register_subject_exam()
    {

    }

    public register_subject_exam(t_year_subject_exam yearSubjectExam)
    {
      if (yearSubjectExam.midterm_lecture_exam_from_time.HasValue &&
          yearSubjectExam.midterm_lecture_exam_to_time.HasValue)
      {
        midterm_lecture_exam_from_datetime = yearSubjectExam.midterm_lecture_exam_date.Value.Add(yearSubjectExam.midterm_lecture_exam_from_time.Value);
        midterm_lecture_exam_to_datetime = yearSubjectExam.midterm_lecture_exam_date.Value.Add(yearSubjectExam.midterm_lecture_exam_to_time.Value);

      }

      if (yearSubjectExam.midterm_lab_exam_from_time.HasValue &&
          yearSubjectExam.midterm_lab_exam_to_time.HasValue)
      {
        midterm_lab_exam_from_datetime =
          yearSubjectExam.midterm_lab_exam_date.Value.Add(yearSubjectExam.midterm_lab_exam_from_time.Value);
        midterm_lab_exam_to_datetime =
          yearSubjectExam.midterm_lab_exam_date.Value.Add(yearSubjectExam.midterm_lab_exam_to_time.Value);
      }

      if (yearSubjectExam.final_lecture_exam_from_time.HasValue &&
          yearSubjectExam.final_lecture_exam_to_time.HasValue)
      {
        final_lecture_exam_from_datetime =
          yearSubjectExam.final_lecture_exam_date.Value.Add(yearSubjectExam.final_lecture_exam_from_time.Value);
        final_lecture_exam_to_datetime =
          yearSubjectExam.final_lecture_exam_date.Value.Add(yearSubjectExam.final_lecture_exam_to_time.Value);
      }

      if (yearSubjectExam.final_lab_exam_from_time.HasValue &&
          yearSubjectExam.final_lab_exam_to_time.HasValue)
      {
        final_lab_exam_from_datetime =
          yearSubjectExam.final_lab_exam_date.Value.Add(yearSubjectExam.final_lab_exam_from_time.Value);
        final_lab_exam_to_datetime =
          yearSubjectExam.final_lab_exam_date.Value.Add(yearSubjectExam.final_lab_exam_to_time.Value);
      }

    }
  }
}
