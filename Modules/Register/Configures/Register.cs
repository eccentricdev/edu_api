using edu_api.Modules.Organizations.Services;
using edu_api.Modules.Register.Services;
using Microsoft.Extensions.DependencyInjection;

namespace edu_api.Modules.Register.Configures
{
  public static class RegisterCollectionExtension
  {
    public static IServiceCollection AddRegisterServices(this IServiceCollection services)
    {
      services.AddSingleton<CacheService>();
      services.AddSingleton<MessageService>();
      services.AddScoped<PreCheckService>();
      services.AddScoped<CoreRegisterService>();
      services.AddScoped<AddDropRegisterService>();

      return services;
    }
  }
}
