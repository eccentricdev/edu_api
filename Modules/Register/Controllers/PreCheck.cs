using System.Security.Claims;
using System.Threading.Tasks;
using edu_api.Modules.Register.Models;
using edu_api.Modules.Register.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace edu_api.Modules.Register.Controllers
{
  [Route("/api/register/precheck", Name = "precheck")]
  public class PreCheckController:Controller
  {
    private readonly PreCheckService _preCheckService;

    public PreCheckController(PreCheckService preCheckService)
    {
      _preCheckService = preCheckService;
    }
    [Authorize]
    [HttpGet]
    public async Task<ActionResult<validate_response>> Validate()
    {
      var validateResult= await _preCheckService.Validate(User.Identity as ClaimsIdentity);
      return Ok(validateResult);
    }

    [Authorize]
    [HttpGet("{student_code}")]
    public async Task<ActionResult<validate_response>> CounterValidate([FromRoute]string student_code)
    {
      var validateResult= await _preCheckService.CounterValidate(student_code);
      return Ok(validateResult);
    }
  }
}
