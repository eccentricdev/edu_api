

using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;
using edu_api.Modules.Register.Databases;
using edu_api.Modules.Register.Models;
using edu_api.Modules.Register.Services;
using Humanizer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using payment_core.Utilities;
using SeventyOneDev.Utilities;

namespace edu_api.Modules.Register.Controllers
{
  [Route("api/register/external_register", Name = "external_register")]
  public class ExternalRegisterController:Controller
  {
    private readonly Db _db;
    public ExternalRegisterController(Db db)
    {
      _db = db;
    }
    [Authorize]
    [HttpGet("summary")]
    public async Task<ActionResult<List<external_register_summary>>> GetRegisterSummary(
      [FromQuery] external_register_summary externalRegisterSummary)
    {
      var finalRegister = new v_final_register();
      externalRegisterSummary.CloneTo(finalRegister);
      var finalRegisters = await _db.v_final_register.AsNoTracking()
        .WhereIf(!string.IsNullOrEmpty(externalRegisterSummary.academic_year_code),
          r => r.academic_year_code == externalRegisterSummary.academic_year_code)
        .WhereIf(!string.IsNullOrEmpty(externalRegisterSummary.semester_code),
          r => r.semester_code == externalRegisterSummary.semester_code)
        .WhereIf(!string.IsNullOrEmpty(externalRegisterSummary.student_code),
          r => r.student_code == externalRegisterSummary.student_code)
        .WhereIf(!string.IsNullOrEmpty(externalRegisterSummary.education_type_code),
          r => r.education_type_code == externalRegisterSummary.education_type_code)
        .ProjectTo<v_final_register, external_register_summary>().OrderBy(s => s.student_code).ToListAsync();
      return Ok (finalRegisters);
    }

    [Authorize]
    [HttpGet("detail")]
    public async Task<ActionResult<List<v_year_subject_section_instructor_summary>>> GetRegisterDetail(
      [FromQuery] v_year_subject_section_instructor_summary yearSubjectSectionInstructorSummary)
    {

      var instructorSummaries = await _db.v_year_subject_section_instructor_summary.AsNoTracking()
        .WhereIf(!string.IsNullOrEmpty(yearSubjectSectionInstructorSummary.academic_year_code),
          r => r.academic_year_code == yearSubjectSectionInstructorSummary.academic_year_code)
        .WhereIf(!string.IsNullOrEmpty(yearSubjectSectionInstructorSummary.semester_code),
          r => r.semester_code == yearSubjectSectionInstructorSummary.semester_code)
        .WhereIf(!string.IsNullOrEmpty(yearSubjectSectionInstructorSummary.student_code),
          r => r.student_code == yearSubjectSectionInstructorSummary.student_code).OrderBy(s => s.student_code).ToListAsync();
      return Ok (instructorSummaries);
    }
    [HttpPost("to_loan")]
    public async Task<ActionResult<string>> SendProvisionRegister()
    {
      var provisionRegister = _db.v_provision_register.AsNoTracking()
        .Include(p=>p.provision_register_advances)
        .Include(p=>p.provision_register_fees)
        .Include(p=>p.provision_register_scholarships)
        .Include(p=>p.provision_register_subjects)
        .FirstOrDefaultAsync();
      JsonSerializerOptions options = new JsonSerializerOptions();
      options.Converters.Add(new DateTimeConverterUsingDateTimeParse());
      options.IgnoreNullValues = true;
      var data = new ByteArrayContent(JsonSerializer.SerializeToUtf8Bytes(provisionRegister, options));
      var httpRequestMessage = new HttpRequestMessage
      {
        Method = HttpMethod.Post,
        RequestUri = new Uri("https://demo.aesystem.co.th/rsu_studentloan/api/saveLoan")
      };
      var httpClient = new HttpClient();
      //httpRequestMessage.Headers.Add("Authorization", "Bearer " + openIdToken.access_token);
      httpRequestMessage.Headers.Add(HttpRequestHeader.ContentType.ToString(), "application/json");
      HttpResponseMessage res = null;
      data.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");
      httpRequestMessage.Content = data;
      res = await httpClient.SendAsync(httpRequestMessage);

      if (res.StatusCode == HttpStatusCode.OK)
      {
        return Ok("Success");
      }
      else
      {
        var response = await res.Content.ReadAsStringAsync();
        return BadRequest(response);
      }

    }

    [HttpGet("student_register_subject/{academic_year_code}/{semester_code}/{student_code}")]
    public async Task<ActionResult<List<v_student_register_subject_detail>>> GetStudentRegisterSubject(
      [FromRoute] string academic_year_code, [FromRoute] string semester_code, [FromRoute] string student_code)
    {
      var studentRegisterSubjects = await _db.v_student_register_subject_detail.AsNoTracking().Where(s =>
        s.academic_year_code == academic_year_code &&
        s.semester_code == semester_code &&
        s.student_code == student_code
      ).ToListAsync();
      return Ok(studentRegisterSubjects);
    }

    [HttpGet("register_subject/{academic_year_code}/{semester_code}/{year_subject_code}")]
    public async Task<ActionResult<student_register_subject_response>> GetRegisterSubject(
      [FromRoute] string academic_year_code,[FromRoute] string semester_code,[FromRoute] string year_subject_code)
    {
      var studentRegisterSubjects = await _db.v_student_register_subject.AsNoTracking().Where(s =>
        s.academic_year_code == academic_year_code &&
        s.semester_code == semester_code &&
        s.year_subject_code == year_subject_code
      ).ToListAsync();
      
      var sections=studentRegisterSubjects.GroupBy(s => s.lecture_year_subject_section_uid)
        .Select(s =>
          {
            var roomCodes = _db.t_year_subject_section_day.AsNoTracking()
              .Where(d => d.year_subject_section_uid == s.Key && d.room_code!=null).Select(d => d.room_code).ToList();
            var instructors = _db.v_year_subject_section_instructor.AsNoTracking()
              .Where(d => d.year_subject_section_uid == s.Key && d.instructor_code!=null).Select(d => d.instructor_code).ToList();

            var studentRegisterSubjectSection = new student_register_subject_section()
            {
              section_code = s.FirstOrDefault().lecture_section_code,
              rooms = roomCodes,
              instructors = instructors,
              students = s.Select(x => new student_register_subject_student()
              {
                student_code = x.student_code,
                college_faculty_name_th = x.college_faculty_name_th,
                faculty_curriculum_name_th = x.faculty_curriculum_name_th,
                grade_name_th = x.grade_name_th
              }).ToList()
            };
            return studentRegisterSubjectSection;
          }
).ToList();
      var studentRegisterSubjectResponse = new student_register_subject_response()
      {
        academic_year_code = academic_year_code,
        semester_code = semester_code,
        year_subject_code = year_subject_code,
        sections = sections
      };
      return Ok(studentRegisterSubjectResponse);
    }






  }
}
