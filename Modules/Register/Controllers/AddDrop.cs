using System;
using System.Diagnostics.CodeAnalysis;
using System.Threading.Tasks;
using edu_api.Modules.Register.Models;
using edu_api.Modules.Register.Services;
using Microsoft.AspNetCore.Mvc;

namespace edu_api.Modules.Register.Controllers
{
  [Route("api/register/add_drop", Name = "add_drop")]
  public class AddDropRegisterController: Controller
  {
    private readonly AddDropRegisterService _addDropRegisterService;
    private readonly CoreRegisterService _coreRegisterService;
    public AddDropRegisterController(AddDropRegisterService addDropRegisterService,CoreRegisterService coreRegisterService)
    {
      _addDropRegisterService = addDropRegisterService;
      _coreRegisterService = coreRegisterService;
    }

    [HttpGet("{register_token_uid:guid}/{subject_code}")]
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    // ReSharper disable once InconsistentNaming
    public  async Task<ActionResult<register_subject_detail>> GetSubject([FromRoute]Guid register_token_uid,[FromRoute]string subject_code)
    {
      var subject=await _coreRegisterService.GetRegisterSubject("ADDDROP",register_token_uid,subject_code);
      return subject;
    }
    [HttpPost("{register_token_uid:guid}")]
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    public async Task<ActionResult<register_detail>> AddSubject([FromRoute]Guid register_token_uid, [FromBody]register_subject registerSubject)
    {
      var registerDetail=await _addDropRegisterService.AddSubject("ADDDROP",register_token_uid,registerSubject);
      return registerDetail;
    }

    [HttpDelete("{register_token_uid:guid}/{year_subject_uid}")]
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    public async Task<ActionResult<register_detail>> RemoveSubject([FromRoute]Guid register_token_uid, [FromRoute]Guid year_subject_uid)
    {
      var registerDetail=await _addDropRegisterService.RemoveSubject("ADDDROP",register_token_uid,year_subject_uid);
      return registerDetail;
    }
    [HttpGet("precheck/{register_token_uid:guid}")]
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    public async Task<ActionResult<register_detail>> PreRegisterCheck([FromRoute]Guid register_token_uid)
    {
      var registerDetail=await _addDropRegisterService.AddDropRegisterCheck("ADDDROP",register_token_uid);
      return registerDetail;
    }



  }
}
