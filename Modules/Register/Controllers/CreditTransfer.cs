using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using edu_api.Modules.Register.Databases;
using edu_api.Modules.Register.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SeventyOneDev.Utilities;

namespace edu_api.Modules.Register.Controllers
{
    [ApiController]
    [Route("api/register/credit_transfer", Name = "add_credit_transfer")]
    public class CreditTransferController:ControllerBase
    {
        private readonly Db _db;
        public CreditTransferController(Db db)
        {
            _db = db;
        }
        [HttpPost]
        public async Task<ActionResult> AddSubjects([FromBody]transfer_credit_request transferCreditRequest)
        {
            var finalRegister = new t_final_register()
            {
                student_uid = transferCreditRequest.student_uid,
                academic_year_uid = transferCreditRequest.academic_year_uid,
                academic_semester_uid = transferCreditRequest.academic_semester_uid,
                register_date = DateTime.Today,
                final_register_subjects = new List<t_final_register_subject>()
            };
            foreach (var transferCreditSubject in transferCreditRequest.transfer_credit_subjects)
            {
                var yearSubject = await _db.t_year_subject.AsNoTracking()
                    .FirstOrDefaultAsync(y => y.year_subject_uid == transferCreditSubject.year_subject_uid);
                if (yearSubject is not null)
                {
                    var finalRegisterSubject = new t_final_register_subject()
                    {
                        year_subject_uid = yearSubject.year_subject_uid,
                        year_subject_code = yearSubject.year_subject_code,
                        year_subject_name_th = yearSubject.year_subject_name_th,
                        year_subject_name_en = yearSubject.year_subject_name_en,
                        credit = yearSubject.credit,
                        grade_uid = transferCreditSubject.grade_uid
                    };
                    finalRegister.final_register_subjects.Add(finalRegisterSubject);
                    
                }
                else
                {
                    return BadRequest("ไม่พบรายวิชา");
                }

                await _db.t_final_register.AddAsync(finalRegister);
            }

            await _db.SaveChangesAsync();
            return Ok();

        }
        
        
    }
}