using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using edu_api.Modules.Register.Databases;
using edu_api.Modules.Register.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SeventyOneDev.Utilities;

namespace edu_api.Modules.Register.Controllers
{
    [Route("api/register/student_schedule", Name = "student_schedule")]
    public class StudentScheduleController:Controller
    {
        private readonly Db _db;
        public StudentScheduleController(Db db)
        {
            _db = db;
        }
        [Authorize]
        [HttpGet]
        public async Task<ActionResult<student_schedule>> GetStudentSchedule()
        {
            var claimsIdentity = User.Identity as ClaimsIdentity;
            var userId = claimsIdentity?.FindFirst(c => c.Type == "user_uid")?.Value;
            if (string.IsNullOrEmpty(userId)) return BadRequest();
            var latestRegister = await _db.v_final_register.Where(r=>r.student_uid==Guid.Parse(userId))
                .OrderByDescending(a=>a.academic_year_code).ThenByDescending(a=>a.semester_code).FirstOrDefaultAsync();
            var schedules = new List<v_year_subject_section_schedule>();
            if (latestRegister!=null)
            {
                var subjects = await _db.t_final_register_subject.Where(r=>r.final_register_uid == latestRegister.final_register_uid).ToListAsync();
                foreach(var subject in subjects)
                {
                    if (subject.lecture_year_subject_section_uid.HasValue)
                    {
                        var schedule = await _db.v_year_subject_section_schedule.FirstOrDefaultAsync(ss=>ss.year_subject_section_uid==subject.lecture_year_subject_section_uid);
                        schedules.Add(schedule);

                    }
                    else if (subject.lab_year_subject_section_uid.HasValue)
                    {
                        var schedule = await _db.v_year_subject_section_schedule.FirstOrDefaultAsync(ss=>ss.year_subject_section_uid== subject.lab_year_subject_section_uid);
                        schedules.Add(schedule);
                    }
                }
               
            }

            return Ok(schedules);

        }
        // public async Task<student_schedule> GetSchedule(string student_code)
        // {
        //     var _studySchedules = await GetStudyLatestSchedule(student_code);
        //     var _schedule = new student_schedule() {
        //         study_section_schedules = _studySchedules
        //     };
        //     return _schedule;
        // }
        // private async Task<List<v_study_section_schedule>> GetStudyLatestSchedule(string student_code)
        // {
            
           // return _schedules;
        
    }
}