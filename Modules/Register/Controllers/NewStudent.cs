using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text.Json;
using System.Threading.Tasks;
using edu_api.Modules.EducationPlan.Databases;
using edu_api.Modules.Register.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using rsu_common_api.models;
using SeventyOneDev.Utilities;

namespace edu_api.Modules.Register.Controllers
{
  [Route("api/register/new_student", Name = "new_student")]
  public class NewStudentController : Controller
  {
    private readonly Db _db;

    public NewStudentController(Db db)
    {
      _db = db;
    }

    [HttpGet("{application_id}")]
    public async Task<ActionResult<applicant_apply>> GetApplicantApply([FromRoute] long application_id)
    {
      var httpClient = new HttpClient();
      var result = await httpClient.GetAsync("https://rsu.71dev.com/app-api/api/app/applicant_apply/" + application_id);
      if (result.StatusCode == HttpStatusCode.OK)
      {
        var data = await result.Content.ReadAsStreamAsync();
        var applicantApply = await JsonSerializer.DeserializeAsync<applicant_apply>(data);
        return Ok(applicantApply);
      }

      return BadRequest();

    }
    [HttpPost]
    public async Task<student> NewStudent([FromBody]applicant_apply applicantApply)
    {

      //Console.WriteLine("Create new student:" + JsonSerializer.Serialize(student));

      // var _code = await _db.t_generate_code.FirstOrDefaultAsync(g => g.code == "STD_CODE");
      // var _next = _code.current_number.Value + 1;
      // var _studentCode = _code.data1 + _next.ToString("D" + _code.digit);
      //student.student_code = _studentCode;
      // if (await _db.t_student.AnyAsync(c =>
      //       c.student_code == _studentCode))
      // {
      //   Console.WriteLine("Create student error: Duplicate student code");
      //   throw new Exception("DATA_EXISTS");
      // }
      //var lastStudentCode = _db.t_student.AsNoTracking().Where(s=>s.student_code.StartsWith(applicantApply.academic_year_code.Substring(2,2))) .OrderByDescending(s => s.student_code).FirstOrDefault()?.student_code;
      var lastStudentCode = _db.t_student.AsNoTracking().Where(s=>s.student_code.StartsWith("62")) .OrderByDescending(s => s.student_code).FirstOrDefault()?.student_code;

      var collegeFaculty = _db.t_college_faculty.AsNoTracking()
        .FirstOrDefault(s => s.college_faculty_code == applicantApply.college_faculty_code);
      var facultyCurriculum = _db.t_faculty_curriculum.AsNoTracking()
        .FirstOrDefault(f => f.faculty_curriculum_code == applicantApply.faculty_curriculum_code);
      var academicYear = await _db.t_academic_year.AsNoTracking()
        .FirstOrDefaultAsync(a => a.academic_year_code == applicantApply.academic_year_code);
      var academicSemester = await _db.t_academic_semester.AsNoTracking().FirstOrDefaultAsync(a =>
        a.academic_year_uid == academicYear.academic_year_uid &&
        a.academic_semester_code == applicantApply.academic_semester_code);


      var student = new t_student()
      {
        student_code = (int.Parse(lastStudentCode)+1).ToString(),
        first_name_th = applicantApply.first_name_th,
        last_name_th = applicantApply.last_name_th,
        display_name_th = applicantApply.display_name_th,
        first_name_en = applicantApply.first_name_en,
        middle_name_en = applicantApply.middle_name_en,
        last_name_en = applicantApply.last_name_en,
        display_name_en = applicantApply.display_name_en,
        college_faculty_uid = collegeFaculty?.college_faculty_uid,
        faculty_curriculum_uid = facultyCurriculum?.faculty_curriculum_uid,
        entry_academic_year_uid = academicYear.academic_year_uid,
        entry_semester_uid = academicSemester.academic_semester_uid
      };
      await _db.t_student.AddAsync (student);
      await _db.SaveChangesAsync();
      try
      {
        var result=await CreateStudentEmail(student.student_code, student.first_name_th, student.last_name_th,student.display_name_th, "Password@1");
        var userResult = await CreateActiveDirectoryUser(student.student_code, student.first_name_th, student.last_name_th,student.display_name_th, "Password@1");
      }
      catch (Exception ex)
      {
        Console.WriteLine("Create email error:" + ex.Message);
      }
      student.rsu_email = student.student_code + "@avitechproject.com";
      await _db.SaveChangesAsync();
      //var email = stu + "@avitechproject.com";

      return student;
    }

    private async Task<bool> CreateStudentEmail(string studentCode,string firstNameTh,string lastNameTh,string displayNameTh,string password)
    {
      var httpClient = new HttpClient();
      var mailUser = new mail_user()
      {
        primaryEmail = studentCode + "@avitechproject.com",
        name = new mail_user_name() {givenName = firstNameTh,familyName = lastNameTh, fullName = displayNameTh},
        password = password,
        changePasswordAtNextLogin = false
      };
      var body = new ByteArrayContent(JsonSerializer.SerializeToUtf8Bytes(mailUser));
       body.Headers.ContentType = new MediaTypeHeaderValue("application/json");
      //body.Headers.Add(HttpRequestHeader.ContentType.ToString(), "application/json");
      httpClient.DefaultRequestHeaders.Accept.Clear();
      httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
      var result = await httpClient.PostAsync("https://rsu.71dev.com/mail-connector/api/mail/gmail", body);
      if (result.StatusCode == HttpStatusCode.OK)
      {
        var response = await result.Content.ReadAsStringAsync();

        return true;
      }
      else
      {
        return false;
      }
      // {
      //   "primaryEmail": "string",
      //   "name": {
      //     "givenName": "string",
      //     "familyName": "string",
      //     "fullName": "string"
      //   },
      //   "password": "string",
      //   "changePasswordAtNextLogin": true
      // }
    }

    private async Task<bool> CreateActiveDirectoryUser(string studentCode, string firstNameTh, string lastNameTh,
      string displayNameTh, string password)
    {
      var httpClient = new HttpClient();
      var activeDirectoryUser = new active_directory_user()
      {
        student_code = studentCode,
        first_name = firstNameTh,
        last_name = lastNameTh,
        password = password,
        email = studentCode + "@avitechproject.com",
        login_name = studentCode,
        ou = "Students"
      };
      var body = new ByteArrayContent(JsonSerializer.SerializeToUtf8Bytes(new List<active_directory_user>()
        { activeDirectoryUser }));
      body.Headers.ContentType = new MediaTypeHeaderValue("application/json");
      //body.Headers.Add(HttpRequestHeader.ContentType.ToString(), "application/json");
      httpClient.DefaultRequestHeaders.Accept.Clear();
      httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
      var result = await httpClient.PostAsync("http://111.223.48.141/api/ad/create_users", body);
      if (result.StatusCode == HttpStatusCode.OK)
      {
        return true;
      }
      else
      {
        return false;
      }

    }
    [HttpGet("education_plan/{student_uid}")]
    public async Task<v_education_plan> GetEducationPlan([FromRoute]Guid student_uid)
    {
      var student = await _db.t_student.AsNoTracking().FirstOrDefaultAsync(s => s.student_uid == student_uid);


      var educationPlanCurriculum = await _db.v_education_plan_curriculum.AsNoTracking()
        .Where(e => e.academic_year_uid == student.entry_academic_year_uid &&
                    e.academic_semester_uid == student.entry_semester_uid &&
                    e.curriculum_uid == student.faculty_curriculum_uid &&
                    e.reserve < e.quota).FirstOrDefaultAsync();
      var educationPlan = await _db.v_education_plan.AsNoTracking()
        .Include(e => e.education_plan_subjects)
        .Include(e => e.education_plan_fees)
        .FirstOrDefaultAsync(e => e.education_plan_uid == educationPlanCurriculum.education_plan_uid);
      return educationPlan;

    }
  }
}
