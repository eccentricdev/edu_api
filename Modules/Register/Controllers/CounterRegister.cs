using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Threading.Tasks;
using edu_api.Modules.Register.Models;
using edu_api.Modules.Register.Models.Student;
using edu_api.Modules.Register.Models.Student.edu_api.Modules.Register.Models;
using edu_api.Modules.Register.Services;
using Microsoft.AspNetCore.Mvc;
using payment_core.Models;
using SeventyOneDev.Utilities;

namespace edu_api.Modules.Register.Controllers
{
   [Route("api/register/counter", Name = "counter")]
  public class CounterRegisterController: Controller
  {
    private readonly CoreRegisterService _preRegisterService;
    private readonly MessageService _messageService;
    public CounterRegisterController(CoreRegisterService preRegisterService,MessageService messageService)
    {
      _preRegisterService = preRegisterService;
      _messageService = messageService;
    }

    [HttpGet("{register_token_uid:guid}/{subject_code}")]
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    // ReSharper disable once InconsistentNaming
    public  async Task<ActionResult<register_subject_detail>> GetSubject([FromRoute]Guid register_token_uid,[FromRoute]string subject_code)
    {
      var subject=await _preRegisterService.GetRegisterSubject("PRE",register_token_uid,subject_code);
      return subject;
    }
    [HttpPost("{register_token_uid:guid}")]
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    public async Task<ActionResult<counter_register_detail>> CounterAddSubject([FromRoute]Guid register_token_uid, [FromBody]counter_register_subject counterRegisterSubject)
    {
      try
      {
        var registerDetail = await _preRegisterService.CounterAddSubject( counterRegisterSubject.counter_register_detail,"PRE", register_token_uid, counterRegisterSubject.register_subject);
        return registerDetail;
      }
      catch (DataException e)
      {
        Console.WriteLine(e);
        var message=_messageService.GetErrorMessage(e,"TH");
        return BadRequest(new Error(400, message));
      }
      catch (Exception e)
      {
        Console.WriteLine(e);
        var message=_messageService.GetErrorMessage(e.Message,"TH");
        return BadRequest(new Error(400, message));
      }

    }

    [HttpDelete("{register_token_uid:guid}/{year_subject_uid}")]
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    public async Task<ActionResult<counter_register_detail>> CounterRemoveSubject([FromRoute]Guid register_token_uid, [FromRoute]Guid year_subject_uid,[FromBody]counter_register_subject counterRegisterSubject)
    {
      try
      {
        var registerDetail=await _preRegisterService.CounterRemoveSubject(counterRegisterSubject.counter_register_detail,"PRE",register_token_uid,year_subject_uid);
        return registerDetail;
      }
      catch (Exception e)
      {
        Console.WriteLine(e);
        var message=_messageService.GetMessage(e.Message,"TH");
        return BadRequest(new Error(400, message));
      }

    }
    [HttpGet("check/{register_token_uid:guid}")]
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    public async Task<ActionResult<counter_register_detail>> CounterRegisterCheck([FromRoute]Guid register_token_uid)
    {
      var registerDetail=await _preRegisterService.CounterRegisterCheck("PRE",register_token_uid);
      return registerDetail;
    }

    [HttpPut("confirm/{register_token_uid:guid}")]
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    public async Task<ActionResult<register_summary>> CounterRegisterConfirm([FromRoute]Guid register_token_uid,[FromBody]counter_register_subject counterRegisterSubject)
    {
      var registerSummary=await _preRegisterService.CounterRegisterConfirm(counterRegisterSubject.counter_register_detail,"PRE",register_token_uid,"TH");
      return registerSummary;
    }
    [HttpGet("payment_method")]
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    public async Task<ActionResult<List<payment_method>>> GetOfflinePaymentMethod()
    {
      var offlinePaymentMethods=await _preRegisterService.GetOfflinePaymentMethod();
      return offlinePaymentMethods;
    }
    [HttpPost("paid/{provision_register_uid:guid}")]
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    public async Task<ActionResult> OfflinePayment([FromRoute]Guid provision_register_uid,[FromBody]List<register_payment> registerPayments)
    {
      var offlinePaymentMethods=await _preRegisterService.OfflinePayment(provision_register_uid,registerPayments);
      return NoContent();
    }



  }
}
