using System;
using System.Diagnostics.CodeAnalysis;
using System.Threading.Tasks;
using edu_api.Modules.Register.Models;
using edu_api.Modules.Register.Services;
using Microsoft.AspNetCore.Mvc;
using SeventyOneDev.Utilities;

namespace edu_api.Modules.Register.Controllers
{
  [Route("api/register/pre", Name = "pre")]
  public class PreRegisterController: Controller
  {
    private readonly CoreRegisterService _preRegisterService;
    private readonly MessageService _messageService;
    public PreRegisterController(CoreRegisterService preRegisterService,MessageService messageService)
    {
      _preRegisterService = preRegisterService;
      _messageService = messageService;
    }

    [HttpGet("{register_token_uid:guid}/{subject_code}")]
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    // ReSharper disable once InconsistentNaming
    public  async Task<ActionResult<register_subject_detail>> GetSubject([FromRoute]Guid register_token_uid,[FromRoute]string subject_code)
    {
      var subject=await _preRegisterService.GetRegisterSubject("PRE",register_token_uid,subject_code);
      return subject;
    }
    [HttpPost("{register_token_uid:guid}")]
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    public async Task<ActionResult<register_detail>> AddSubject([FromRoute]Guid register_token_uid, [FromBody]register_subject registerSubject)
    {
      try
      {
        var registerDetail = await _preRegisterService.AddSubject("PRE", register_token_uid, registerSubject);
        return registerDetail;
      }
      catch (DataException e)
      {
        Console.WriteLine(e);
        var message=_messageService.GetErrorMessage(e,"TH");
        return BadRequest(new Error(400, message));
      }
      catch (Exception e)
      {
        Console.WriteLine(e);
        var message=_messageService.GetErrorMessage(e.Message,"TH");
        return BadRequest(new Error(400, message));
      }
    }
    [HttpDelete("{register_token_uid:guid}/{year_subject_uid}")]
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    public async Task<ActionResult<register_detail>> RemoveSubject([FromRoute]Guid register_token_uid, [FromRoute]Guid year_subject_uid)
    {
      try
      {
        var registerDetail=await _preRegisterService.RemoveSubject("PRE",register_token_uid,year_subject_uid);
        return registerDetail;
      }
      catch (Exception e)
      {
        Console.WriteLine(e);
        var message=_messageService.GetMessage(e.Message,"TH");
        return BadRequest(new Error(400, message));
      }

    }
    [HttpGet("precheck/{register_token_uid:guid}")]
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    public async Task<ActionResult<register_detail>> PreRegisterCheck([FromRoute]Guid register_token_uid)
    {
      var registerDetail=await _preRegisterService.PreRegisterCheck("PRE",register_token_uid);
      return registerDetail;
    }

    [HttpPut("confirm/{register_token_uid:guid}")]
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    public async Task<ActionResult<register_summary>> PreRegisterConfirm([FromRoute]Guid register_token_uid)
    {
      var registerSummary=await _preRegisterService.PreRegisterConfirm("PRE",register_token_uid,"TH");
      return registerSummary;
    }



  }
}
