using System;
using System.Diagnostics.CodeAnalysis;
using System.Threading.Tasks;
using edu_api.Modules.Register.Models;
using edu_api.Modules.Register.Services;
using Microsoft.AspNetCore.Mvc;
using SeventyOneDev.Utilities;

namespace edu_api.Modules.Register.Controllers
{
  [Route("api/register/late", Name = "late")]
  public class LateRegisterController: Controller
  {
    private readonly CoreRegisterService _lateRegisterService;
    private readonly MessageService _messageService;
    public LateRegisterController(CoreRegisterService preRegisterService,MessageService messageService)
    {
      _lateRegisterService = preRegisterService;
      _messageService = messageService;
    }

    [HttpGet("{register_token_uid:guid}/{subject_code}")]
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    // ReSharper disable once InconsistentNaming
    public  async Task<ActionResult<register_subject_detail>> GetSubject([FromRoute]Guid register_token_uid,[FromRoute]string subject_code)
    {
      var subject=await _lateRegisterService.GetRegisterSubject("LATE",register_token_uid,subject_code);
      return subject;
    }
    [HttpPost("{register_token_uid:guid}")]
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    public async Task<ActionResult<register_detail>> AddSubject([FromRoute]Guid register_token_uid, [FromBody]register_subject registerSubject)
    {
      try
      {
        var registerDetail = await _lateRegisterService.AddSubject("LATE", register_token_uid, registerSubject);
        return registerDetail;
      }
      catch (DataException e)
      {
        Console.WriteLine(e);
        var message=_messageService.GetErrorMessage(e,"TH");
        return BadRequest(new Error(400, message));
      }
      catch (Exception e)
      {
        Console.WriteLine(e);
        var message=_messageService.GetErrorMessage(e.Message,"TH");
        return BadRequest(new Error(400, message));
      }
    }
    [HttpDelete("{register_token_uid:guid}/{year_subject_uid}")]
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    public async Task<ActionResult<register_detail>> RemoveSubject([FromRoute]Guid register_token_uid, [FromRoute]Guid year_subject_uid)
    {
      try
      {
        var registerDetail=await _lateRegisterService.RemoveSubject("LATE",register_token_uid,year_subject_uid);
        return registerDetail;
      }
      catch (Exception e)
      {
        Console.WriteLine(e);
        var message=_messageService.GetMessage(e.Message,"TH");
        return BadRequest(new Error(400, message));
      }

    }
    [HttpGet("precheck/{register_token_uid:guid}")]
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    public async Task<ActionResult<register_detail>> PreRegisterCheck([FromRoute]Guid register_token_uid)
    {
      var registerDetail=await _lateRegisterService.PreRegisterCheck("LATE",register_token_uid);
      return registerDetail;
    }

    [HttpPut("confirm/{register_token_uid:guid}")]
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    public async Task<ActionResult<register_summary>> PreRegisterConfirm([FromRoute]Guid register_token_uid)
    {
      var registerSummary=await _lateRegisterService.PreRegisterConfirm("LATE",register_token_uid,"TH");
      return registerSummary;
    }



  }
}
