using System;
using System.Threading.Tasks;
using edu_api.Modules.Register.Databases;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SeventyOneDev.Utilities;

namespace edu_api.Modules.Register.Controllers
{
  [Route("api/register/provision_register", Name = "provision_register")]
  public class ProvisionRegisterController:Controller
  {
    private readonly Db _db;
    public ProvisionRegisterController(Db db)
    {
      _db = db;
    }
    
    
    [HttpGet("{provision_register_uid}")]
    public async Task<ActionResult<v_provision_register>> GetProvisionRegister([FromRoute]Guid provision_register_uid)
    {
      var provisionRegister = await _db.t_provision_register.AsNoTracking()
        .Include(p => p.provision_register_subjects)
        .Include(p => p.provision_register_advances)
        .Include(p => p.provision_register_fees)
        .Include(p => p.provision_register_scholarships)
        .FirstOrDefaultAsync(p => p.provision_register_uid == provision_register_uid);
      return Ok(provisionRegister);
    }
    
    [HttpGet("student/{student_code}")]
    public async Task<ActionResult<v_provision_register>> GetProvisionRegister([FromRoute]string student_code)
    {
      var provisionRegister = await _db.v_provision_register.AsNoTracking()
        .Include(p => p.provision_register_subjects)
        .Include(p => p.provision_register_advances)
        .Include(p => p.provision_register_fees)
        .Include(p => p.provision_register_scholarships)
        .FirstOrDefaultAsync(p => p.student_code == student_code);
      return Ok(provisionRegister);
    }
  }
}
