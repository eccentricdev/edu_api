using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text.Json;
using System.Threading.Tasks;
using edu_api.Modules.Register.Databases;
using edu_api.Modules.Register.Models;
using edu_api.Modules.Register.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using rsu_common_api.models;
using rsu_common_api.Modules.Profiles.Services;
using SeventyOneDev.Utilities;

namespace edu_api.Modules.Register.Controllers
{
  [Route("api/register/payment", Name = "pre")]
  public class PaymentController:Controller
  {
    private readonly Db _db;
    private readonly CoreRegisterService _preRegisterService;
    private readonly IamService _iamService;
    public PaymentController(CoreRegisterService preRegisterService,Db db,IamService iamService)
    {
      _preRegisterService = preRegisterService;
      _db = db;
      _iamService = iamService;
    }
    [HttpPost("paid")]
    public async Task<ActionResult> UpdatePayment([FromBody]payment_update paymentUpdate)
    {

      //var getResult = await appService.PaymentPrecheck(id);
      Console.WriteLine("Payment Update:" + JsonSerializer.Serialize(paymentUpdate));
      var creditCard=await _preRegisterService.PaymentUpdate(paymentUpdate);
      string billNo = paymentUpdate.ref2.Substring(5, 7); //paymentRequest.ref2.Length - 6);
      Console.WriteLine("Paid invoice no:" + billNo);

      var provisionRegister = await _db.t_provision_register.FirstOrDefaultAsync(a => a.bill_no == billNo);
      if (provisionRegister != null)
      {
        provisionRegister.is_paid = true;
        var finalRegister = new t_final_register(provisionRegister);
        var provisionRegisterSubjects = await _db.t_provision_register_subject
          .Where(p => p.provision_register_uid == provisionRegister.provision_register_uid).ToListAsync();
        finalRegister.final_register_subjects.AddRange(provisionRegisterSubjects.Select(p=>new t_final_register_subject(p)));
        await _db.AddAsync(finalRegister);
        await _db.SaveChangesAsync();
        return Ok();
      }
      var planRegister = await _db.t_plan_register.FirstOrDefaultAsync(a => a.bill_no == billNo);
      if (planRegister != null)
      {
        planRegister.is_paid = true;
        var applicantApplyView = await _db.v_applicant_apply.AsNoTracking()
          .FirstOrDefaultAsync(a => a.applicant_apply_id == planRegister.applicant_apply_id);
        if (applicantApplyView != null)
        {
          var _applicantApply = new applicant_apply();
          if (applicantApplyView.college_faculty_uid.HasValue && applicantApplyView.faculty_curriculum_uid.HasValue)
          {
            applicantApplyView.CloneTo(_applicantApply);
         
            var student = await NewStudent(_applicantApply);
            var applicantApplyTable = await _db.t_applicant_apply.AsNoTracking()
              .FirstOrDefaultAsync(a => a.applicant_apply_id == planRegister.applicant_apply_id);
            applicantApplyTable.student_code = student.student_code;
            applicantApplyTable.rsu_email = student.rsu_email;
            await _db.SaveChangesAsync();
            HttpClient httpClient = new HttpClient();
            var res=await httpClient.PutAsync(
              "https://rsu.71dev.com/app-api/api/app/applicant_applys/complete/" + planRegister.applicant_apply_id,
              null!);
            if (res.StatusCode!=HttpStatusCode.OK)
            {
              Console.WriteLine("Error update status");
            }
            
            return Ok();
          }
          
        }
        //planRegister.applicant_apply_id
        // var finalRegister = new t_final_register(provisionRegister);
        // var provisionRegisterSubjects = await _db.t_provision_register_subject
        //   .Where(p => p.provision_register_uid == provisionRegister.provision_register_uid).ToListAsync();
        // finalRegister.final_register_subjects.AddRange(provisionRegisterSubjects.Select(p=>new t_final_register_subject(p)));
        // await _db.AddAsync(finalRegister);
        // await _db.SaveChangesAsync();
        // return Ok();
      }

      return BadRequest();





    }
    [HttpGet("token")]
    public async Task<ActionResult<payment_token>> GetWebPaymentToken()
    {

      //var getResult = await appService.PaymentPrecheck(id);
      //Console.WriteLine("Payment Update:" + JsonSerializer.Serialize(paymentUpdate));
      var webPaymentToken=await _preRegisterService.GetWebPaymentToken();
      // appService.SendHtmlMail(email_template_id,applicant_apply_id);
      // return Ok();
      
      return Ok(webPaymentToken);



    }
    private async Task<student> NewStudent([FromBody]applicant_apply applicantApply)
    {

      //Console.WriteLine("Create new student:" + JsonSerializer.Serialize(student));

      // var _code = await _db.t_generate_code.FirstOrDefaultAsync(g => g.code == "STD_CODE");
      // var _next = _code.current_number.Value + 1;
      // var _studentCode = _code.data1 + _next.ToString("D" + _code.digit);
      //student.student_code = _studentCode;
      // if (await _db.t_student.AnyAsync(c =>
      //       c.student_code == _studentCode))
      // {
      //   Console.WriteLine("Create student error: Duplicate student code");
      //   throw new Exception("DATA_EXISTS");
      // }
      //var lastStudentCode = _db.t_student.AsNoTracking().Where(s=>s.student_code.StartsWith(applicantApply.academic_year_code.Substring(2,2))) .OrderByDescending(s => s.student_code).FirstOrDefault()?.student_code;
      var lastStudentCode = _db.t_student.AsNoTracking().Where(s=>s.student_code.StartsWith("62")) .OrderByDescending(s => s.student_code).FirstOrDefault()?.student_code;

      // var collegeFaculty = _db.t_college_faculty.AsNoTracking()
      //   .FirstOrDefault(s => s.college_faculty_code == applicantApply.college_faculty_code);
      // var facultyCurriculum = _db.t_faculty_curriculum.AsNoTracking()
      //   .FirstOrDefault(f => f.faculty_curriculum_code == applicantApply.faculty_curriculum_code);
      var academicYear = await _db.t_academic_year.AsNoTracking()
        .FirstOrDefaultAsync(a => a.academic_year_code == applicantApply.academic_year_code);
      var academicSemester = await _db.t_academic_semester.AsNoTracking().FirstOrDefaultAsync(a =>
        a.academic_year_uid == academicYear.academic_year_uid &&
        a.academic_semester_code == applicantApply.academic_semester_code);


      var student = new t_student()
      {
        student_code = (int.Parse(lastStudentCode)+1).ToString(),
        first_name_th = applicantApply.first_name_th,
        last_name_th = applicantApply.last_name_th,
        display_name_th = applicantApply.display_name_th,
        first_name_en = applicantApply.first_name_en,
        middle_name_en = applicantApply.middle_name_en,
        last_name_en = applicantApply.last_name_en,
        display_name_en = applicantApply.display_name_en,
        college_faculty_uid = applicantApply.college_faculty_uid,
        faculty_curriculum_uid = applicantApply.faculty_curriculum_uid,
        entry_academic_year_uid = academicYear.academic_year_uid,
        entry_semester_uid = academicSemester.academic_semester_uid
      };
      await _db.t_student.AddAsync (student);
      await _db.SaveChangesAsync();
      try
      {
        var iamResult = await _iamService.AddOrUpdateIamUser(student);
        var result=await CreateStudentEmail(student.student_code, student.first_name_th, student.last_name_th,student.display_name_th, "Password@1");
        //var userResult = await CreateActiveDirectoryUser(student.student_code, student.first_name_th, student.last_name_th,student.display_name_th, "Password@1");
        
      }
      catch (Exception ex)
      {
        Console.WriteLine("Create email error:" + ex.Message);
      }
      student.rsu_email = student.student_code + "@avitechproject.com";
      await _db.SaveChangesAsync();
      //var email = stu + "@avitechproject.com";

      return student;
    }

    private async Task<bool> CreateStudentEmail(string studentCode,string firstNameTh,string lastNameTh,string displayNameTh,string password)
    {
      var httpClient = new HttpClient();
      var mailUser = new mail_user()
      {
        primaryEmail = studentCode + "@avitechproject.com",
        name = new mail_user_name() {givenName = firstNameTh,familyName = lastNameTh, fullName = displayNameTh},
        password = password,
        changePasswordAtNextLogin = false
      };
      var body = new ByteArrayContent(JsonSerializer.SerializeToUtf8Bytes(mailUser));
       body.Headers.ContentType = new MediaTypeHeaderValue("application/json");
      //body.Headers.Add(HttpRequestHeader.ContentType.ToString(), "application/json");
      httpClient.DefaultRequestHeaders.Accept.Clear();
      httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
      var result = await httpClient.PostAsync("https://rsu.71dev.com/mail-connector/api/mail/gmail", body);
      if (result.StatusCode == HttpStatusCode.OK)
      {
        var response = await result.Content.ReadAsStringAsync();

        return true;
      }
      else
      {
        return false;
      }
      // {
      //   "primaryEmail": "string",
      //   "name": {
      //     "givenName": "string",
      //     "familyName": "string",
      //     "fullName": "string"
      //   },
      //   "password": "string",
      //   "changePasswordAtNextLogin": true
      // }
    }

    private async Task<bool> CreateActiveDirectoryUser(string studentCode, string firstNameTh, string lastNameTh,
      string displayNameTh, string password)
    {
      var httpClient = new HttpClient();
      var activeDirectoryUser = new active_directory_user()
      {
        student_code = studentCode,
        first_name = firstNameTh,
        last_name = lastNameTh,
        password = password,
        email = studentCode + "@avitechproject.com",
        login_name = studentCode,
        ou = "Students"
      };
      var body = new ByteArrayContent(JsonSerializer.SerializeToUtf8Bytes(new List<active_directory_user>()
        { activeDirectoryUser }));
      body.Headers.ContentType = new MediaTypeHeaderValue("application/json");
      //body.Headers.Add(HttpRequestHeader.ContentType.ToString(), "application/json");
      httpClient.DefaultRequestHeaders.Accept.Clear();
      httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
      var result = await httpClient.PostAsync("http://111.223.48.141/api/ad/create_users", body);
      if (result.StatusCode == HttpStatusCode.OK)
      {
        return true;
      }
      else
      {
        return false;
      }

    }
  }
}
