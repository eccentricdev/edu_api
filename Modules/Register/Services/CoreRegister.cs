using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Reflection.Metadata.Ecma335;
using System.Runtime.InteropServices;
using System.Text.Json;
using System.Threading.Tasks;
using edu_api.Modules.Fee.Databases.Models;
using edu_api.Modules.Register.Databases;
using edu_api.Modules.Register.Models;
using edu_api.Modules.Register.Models.Student;
using edu_api.Modules.Register.Models.Student.edu_api.Modules.Register.Models;
using edu_api.Modules.Study.Databases.Models;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion.Internal;
using Microsoft.Extensions.DependencyModel.Resolution;
using Microsoft.Extensions.Options;
using payment_core.Models;
using payment_core.Utilities;
using rsu_common_api.models;
using SeventyOneDev.Utilities;
using register_fee = edu_api.Modules.Register.Models.register_fee;

namespace edu_api.Modules.Register.Services
{
  public class CoreRegisterService
  {
    private readonly Db _context;
    private readonly CacheService _cacheService;
    private readonly MessageService _messageService;
    private readonly IHttpClientFactory _httpClientFactory;
    private readonly open_id_setting _openIdSetting;

    public CoreRegisterService(Db context,CacheService cacheService,MessageService messageService,IHttpClientFactory httpClientFactory,IOptions<open_id_setting> openIdSetting)
    {
      _context = context;
      _cacheService = cacheService;
      _messageService = messageService;
      _httpClientFactory = httpClientFactory;
      _openIdSetting = openIdSetting.Value;
    }

    public string CheckOverlappedStudyTime(List<subject_study_time> studentStudyTimes,List<subject_study_time> subjectStudyTimes)
    {
      foreach (var subjectStudyTime in subjectStudyTimes)
      {
        var sameTime=studentStudyTimes.FirstOrDefault(s =>
          s.study_from_time >= subjectStudyTime.study_from_time && s.study_to_time <= subjectStudyTime.study_to_time);
        if (sameTime != null)
        {
          return sameTime.subject_code;
        }
      }

      return "";
    }
    public string CheckOverlappedExamTime(List<subject_exam_time> studentExamTimes,List<subject_exam_time> subjectExamTimes)
    {
      foreach (var subjectExamTime in subjectExamTimes)
      {
        var sameTime=studentExamTimes.FirstOrDefault(s =>
          s.exam_start_time >= subjectExamTime.exam_start_time && s.exam_end_time <= subjectExamTime.exam_end_time);
        if (sameTime != null)
        {
          return sameTime.subject_code;
        }
      }

      return "";
    }

    // public List<exam_time_range> GetExamTimes(List<register_subject> registerSubjects)
    // {
    //   var examTimeRanges = new List<exam_time_range>();
    //   foreach (var registerSubject in registerSubjects)
    //   {
    //     if (registerSubject.midterm_lecture_exam_from_unixtime.HasValue &&
    //         registerSubject.midterm_lecture_exam_to_unixtime.HasValue)
    //     {
    //       examTimeRanges.Add(new exam_time_range(registerSubject.year_subject_code,
    //         registerSubject.midterm_lecture_exam_from_unixtime.Value,
    //         registerSubject.midterm_lecture_exam_to_unixtime.Value));
    //     }
    //
    //     if (registerSubject.final_lecture_exam_from_unixtime.HasValue &&
    //         registerSubject.final_lecture_exam_to_unixtime.HasValue)
    //     {
    //       examTimeRanges.Add(new exam_time_range(registerSubject.year_subject_code,
    //         registerSubject.final_lecture_exam_from_unixtime.Value,
    //         registerSubject.final_lecture_exam_to_unixtime.Value));
    //     }
    //
    //     if (registerSubject.midterm_lab_exam_from_unixtime.HasValue &&
    //         registerSubject.midterm_lab_exam_to_unixtime.HasValue)
    //     {
    //       examTimeRanges.Add(new exam_time_range(registerSubject.year_subject_code,
    //         registerSubject.midterm_lab_exam_from_unixtime.Value, registerSubject.midterm_lab_exam_to_unixtime.Value));
    //     }
    //
    //     if (registerSubject.final_lab_exam_from_unixtime.HasValue &&
    //         registerSubject.final_lab_exam_to_unixtime.HasValue)
    //     {
    //       examTimeRanges.Add(new exam_time_range(registerSubject.year_subject_code,
    //         registerSubject.final_lab_exam_from_unixtime.Value, registerSubject.final_lab_exam_to_unixtime.Value));
    //     }
    //   }
    //
    //   return examTimeRanges;
    // }

    // public List<exam_time_range> GetSubjectExamTimes(register_subject registerSubject)
    // {
    //   var examTimeRanges = new List<exam_time_range>();
    //   if (registerSubject.midterm_lecture_exam_from_unixtime.HasValue &&
    //       registerSubject.midterm_lecture_exam_to_unixtime.HasValue)
    //   {
    //     examTimeRanges.Add(new exam_time_range(registerSubject.year_subject_code,
    //       registerSubject.midterm_lecture_exam_from_unixtime.Value,
    //       registerSubject.midterm_lecture_exam_to_unixtime.Value));
    //   }
    //
    //   if (registerSubject.final_lecture_exam_from_unixtime.HasValue &&
    //       registerSubject.final_lecture_exam_to_unixtime.HasValue)
    //   {
    //     examTimeRanges.Add(new exam_time_range(registerSubject.year_subject_code,
    //       registerSubject.final_lecture_exam_from_unixtime.Value,
    //       registerSubject.final_lecture_exam_to_unixtime.Value));
    //   }
    //
    //   if (registerSubject.midterm_lab_exam_from_unixtime.HasValue &&
    //       registerSubject.midterm_lab_exam_to_unixtime.HasValue)
    //   {
    //     examTimeRanges.Add(new exam_time_range(registerSubject.year_subject_code,
    //       registerSubject.midterm_lab_exam_from_unixtime.Value,
    //       registerSubject.midterm_lab_exam_to_unixtime.Value));
    //   }
    //
    //   if (registerSubject.final_lab_exam_from_unixtime.HasValue &&
    //       registerSubject.final_lab_exam_to_unixtime.HasValue)
    //   {
    //     examTimeRanges.Add(new exam_time_range(registerSubject.year_subject_code,
    //       registerSubject.final_lab_exam_from_unixtime.Value, registerSubject.final_lab_exam_to_unixtime.Value));
    //   }
    //
    //
    //   return examTimeRanges;
    // }

    private async Task<List<subject_section_study_time>> GetYearSubjectStudyTime(Guid? yearSubjectUid)
    {
      var yearSubjectStudyTimes = await _context.v_year_subject_section_day.AsNoTracking()
        .Where(y => y.year_subject_uid == yearSubjectUid)
        .ProjectTo<v_year_subject_section_day,subject_section_study_time>()
        .ToListAsync();
      return yearSubjectStudyTimes;

    }

    private async Task<register_subject_exam> GetYearSubjectExam(Guid? yearSubjectUid)
    {
      var yearSubjectExam = await _context.t_year_subject_exam.AsNoTracking()
        .FirstOrDefaultAsync(y => y.year_subject_uid == yearSubjectUid);
      return yearSubjectExam == null ? null : new register_subject_exam(yearSubjectExam);
    }
    //private async Task<List<register_fee>> GetRegisterFee(Guid? entryAcademicYearUid,Guid? academicSemesterUid,Guid? educationTypeUid)


    private async Task<List<register_fee>> GetRegisterFee(register_token_detail registerTokenDetail)
    {
      var semesterUid =await _context.t_academic_semester.AsNoTracking()
        .Where(a => a.academic_semester_uid == registerTokenDetail.register_academic_semester_uid).Select(a=>a.semester_uid).FirstOrDefaultAsync();

      var registerFee = await  _context.t_register_fee.AsNoTracking().FirstOrDefaultAsync(f =>
        f.entry_academic_year_uid == registerTokenDetail.entry_academic_year_uid &&
        f.semester_uid == semesterUid &&
        f.education_type_uid == registerTokenDetail.education_type_uid);
      if (registerFee != null)
      {
        var registerFeeDetails = await _context.v_register_fee_detail.AsNoTracking()
          .Where(f => f.register_fee_uid == registerFee.register_fee_uid).ProjectTo<v_register_fee_detail,register_fee>().ToListAsync();
        return registerFeeDetails;
      }
      else
      {
        return new List<register_fee>();
      }

    }

    private async Task<List<register_advance_payment>> GetRegisterAdvancePayment(register_token_detail registerTokenDetail)
    {
      var advancePayments = await _context.t_advance_payment.AsNoTracking()
        .Where(a => a.student_uid == registerTokenDetail.student_uid && a.is_use == false).ProjectTo<t_advance_payment,register_advance_payment>().ToListAsync();
      return advancePayments;
    }
    public async Task<t_provision_register> GetProvisionRegister(Guid studentUid, Guid academicYearUid,
      Guid semesterUid, string registerTypCode)
    {
      var provisionRegister = await _context.t_provision_register.AsNoTracking()
        .FirstOrDefaultAsync(p => p.student_uid == studentUid && p.academic_year_uid==academicYearUid && p.academic_semester_uid==semesterUid);
      return provisionRegister;
    }

    private async Task<register_subject> GetRegisterSubject(string subjectCode, Guid? academicYearUid,
      Guid? semesterUid,Guid? educationTypeUid)
    {
      if (_cacheService.HasCache(academicYearUid , semesterUid))
      {
        return _cacheService.GerSubject(subjectCode, educationTypeUid);
      }
      var yearSubject = await _context.t_year_subject.AsNoTracking().FirstOrDefaultAsync(s =>
        s.year_subject_code == subjectCode &&
        s.academic_year_uid == academicYearUid &&
        s.academic_semester_uid == semesterUid &&
        s.education_type_uid == educationTypeUid
        );//.ProjectTo<t_year_subject,register_subject>().FirstOrDefaultAsync();
      if (yearSubject == null)
      {
        throw new DataException("NOT_FOUND_SUBJECT");
      }
      return new register_subject(yearSubject);
    }
    private async Task<List<register_subject_section>> GetRegisterSubjectSection(Guid? yearSubjectUid)
    {
      if (_cacheService.HasYearSubjectSection(yearSubjectUid))
      {
        return _cacheService.GetYearSubjectSection(yearSubjectUid);
      }
      var yearSubjectSections = await _context.v_year_subject_section.AsNoTracking()
        .Where(s => s.year_subject_uid == yearSubjectUid).ToListAsync();
      return yearSubjectSections.Select(s => new register_subject_section(s)).ToList();
    }

    private async Task<List<register_subject_section_faculty>> GetRegisterSubjectSectionFaculty(Guid? yearSubjectSectionUid)
    {
      var yearSubjectSectionFaculties = await _context.t_year_subject_section_faculty.AsNoTracking()
        .Where(s => s.year_subject_section_uid == yearSubjectSectionUid).ToListAsync();
      return yearSubjectSectionFaculties.Select(s => new register_subject_section_faculty(s)).ToList();
    }

    private async Task<List<register_subject_section_year>> GetRegisterSubjectSectionYear(Guid? yearSubjectUid)
    {
      var yearSubjectSectionYears = await _context.t_year_subject_section_year.AsNoTracking()
        .Where(s => s.year_subject_section_uid == yearSubjectUid).ToListAsync();
      return yearSubjectSectionYears.Select(s => new register_subject_section_year(s)).ToList();
    }

    private async Task<register_subject_cost> GetRegisterSubjectYearCost(Guid subjectUid, Guid entryAcademicYearUid,Guid educationTypeUid)
    {
      if (_cacheService.HasYearSubjectCost(subjectUid, entryAcademicYearUid, educationTypeUid))
      {
        return _cacheService.GetSubjectYearCost(subjectUid, entryAcademicYearUid, educationTypeUid);
      }
      var subjectYear = await _context.t_subject_year.AsNoTracking().FirstOrDefaultAsync(s =>
        s.subject_uid == subjectUid && s.curriculum_year_uid == entryAcademicYearUid);

      var subjectYearCost = await _context.t_subject_year_cost.AsNoTracking().FirstOrDefaultAsync(s =>
        s.subject_year_uid == subjectYear.subject_year_uid && s.education_type_uid == educationTypeUid);
      var registerSubjectCost = new register_subject_cost()
      {
        credit = subjectYear.credit,
        lecture_amount = subjectYearCost.lecture_amount,
        lab_amount = subjectYearCost.lab_amount
      };
      return registerSubjectCost;

    }

    private async Task<student_loan> GetStudentLoan(register_token_detail registerTokenDetail)
    {
      var studentLoan = await _context.v_loan_semester.AsNoTracking().Where(l =>
        l.academic_year_uid == registerTokenDetail.register_academic_year_uid && l.semester_uid == registerTokenDetail.register_semester_uid && l.student_uid == registerTokenDetail.student_uid)
          .ProjectTo<v_loan_semester,student_loan>().FirstOrDefaultAsync();
      return studentLoan;
    }



    private async Task<register_subject_exam> GetRegisterSubjectExam(Guid? yearSubjectUid)
    {
      var yearSubjectExam = await _context.t_year_subject_exam.AsNoTracking()
        .FirstOrDefaultAsync(s => s.year_subject_uid == yearSubjectUid);
      return new register_subject_exam(yearSubjectExam); //.ToList();
    }

    private async Task<List<scholarship>> GetScholarship(register_token_detail registerTokenDetail)
    {
      if (string.IsNullOrEmpty(registerTokenDetail.id_card_no))
      {
        return new List<scholarship>();
      }
      var httpClient = _httpClientFactory.CreateClient("scholarship");
      var httpRequestMessage = new HttpRequestMessage
      {
        Method = HttpMethod.Get,
        RequestUri = new Uri(httpClient.BaseAddress + "scholar-api/api/Scholarship/GetByStudent/" + registerTokenDetail.id_card_no + "/" + registerTokenDetail.register_academic_year_code + "/" + registerTokenDetail.register_semester_code)
      };
      httpRequestMessage.Headers.Add(HttpRequestHeader.ContentType.ToString(), "application/json");
      HttpResponseMessage res = null;
      res = await httpClient.SendAsync(httpRequestMessage);

      if (res.StatusCode == HttpStatusCode.OK)
      {
        var result = await res.Content.ReadAsByteArrayAsync();
        var scholarshipResponse = JsonSerializer.Deserialize<scholarship_response>(result);
        return scholarshipResponse.results;
      }
      else
      {
        return new List<scholarship>();
      }
    }

    public async Task<register_token_detail> GetRegisterToken(string registerTypeCode,Guid registerTokenUid)
    {
      var registerTokenDetail = new register_token_detail();
      var registerToken= await _context.t_register_token.AsNoTracking()
        .FirstOrDefaultAsync(r => r.register_token_uid == registerTokenUid);
      registerTokenDetail.student_uid = registerToken.student_uid??Guid.Empty;
      registerTokenDetail.education_type_uid = registerToken.education_type_uid??Guid.Empty;
      registerTokenDetail.college_faculty_uid = registerToken.college_faculty_uid??Guid.Empty;
      registerTokenDetail.faculty_curriculum_uid = registerToken.faculty_curriculum_uid??Guid.Empty;
      registerTokenDetail.entry_academic_year_uid = registerToken.entry_academic_year_uid??Guid.Empty;
      registerTokenDetail.student_status_uid = registerToken.student_status_uid??Guid.Empty;
      registerTokenDetail.id_card_no = registerToken.id_card_no;
      registerTokenDetail.register_academic_year_code = registerToken.register_academic_year_code;
      registerTokenDetail.register_semester_code = registerToken.register_semester_code;
      registerTokenDetail.register_semester_uid = registerToken.register_semester_uid??Guid.Empty;
      registerTokenDetail.min_credit = registerToken.min_credit;
      registerTokenDetail.max_credit = registerToken.max_credit;
      registerTokenDetail.is_package = registerToken.is_package;
      registerTokenDetail.package_amount = registerToken.package_amount;


      switch (registerTypeCode)
      {
        case "PRE":
          registerTokenDetail.academic_register_calendar_uid = registerToken.pre_academic_register_calendar_uid??Guid.Empty;
          registerTokenDetail.register_academic_year_uid = registerToken.pre_register_academic_year_uid??Guid.Empty;
          registerTokenDetail.register_academic_semester_uid = registerToken.pre_register_academic_semester_uid??Guid.Empty;

          break;
        case "LATE":
          registerTokenDetail.academic_register_calendar_uid = registerToken.late_academic_register_calendar_uid??Guid.Empty;
          registerTokenDetail.register_academic_year_uid = registerToken.late_register_academic_year_uid??Guid.Empty;
          registerTokenDetail.register_academic_semester_uid = registerToken.late_register_academic_semester_uid??Guid.Empty;
          break;
        // ReSharper disable once StringLiteralTypo
        case "ADDDROP":
          registerTokenDetail.academic_register_calendar_uid = registerToken.add_drop_academic_register_calendar_uid??Guid.Empty;
          registerTokenDetail.register_academic_year_uid = registerToken.add_drop_register_academic_year_uid??Guid.Empty;
          registerTokenDetail.register_academic_semester_uid = registerToken.add_drop_register_academic_semester_uid??Guid.Empty;
          break;
      }

      return registerTokenDetail;
    }

    // public async Task<register_subject_detail> GetRegisterSubject(string subjectCode, Guid? entryAcademicYearUid,
    //   Guid? educationTypeUid, string studentCode, Guid? facultyUid, Guid? academicYearUid, Guid? semesterUid)
    public async Task<register_subject_detail> GetRegisterSubject(string registerTypeCode,Guid registerTokenUid,string subjectCode)
    {
      var registerTokenDetail = await GetRegisterToken(registerTypeCode, registerTokenUid);
      var subject = await GetRegisterSubject(subjectCode, registerTokenDetail.register_academic_year_uid, registerTokenDetail.register_academic_semester_uid,registerTokenDetail.education_type_uid);
      var subjectCost = await GetRegisterSubjectYearCost(subject.subject_uid.Value,
        registerTokenDetail.entry_academic_year_uid, registerTokenDetail.education_type_uid);
      if (subjectCost != null)
      {
        subject.credit = subjectCost.credit;
        subject.lecture_credit_amount = subjectCost.lecture_amount;
        subject.total_lab_amount = subjectCost.lab_amount??0;
        subject.total_lecture_amount = (subjectCost.lecture_amount??0) * (subjectCost.credit??0);
        subject.total_amount = subject.total_lab_amount + subject.total_lecture_amount;
      }
      var subjectSections = await GetRegisterSubjectSection(subject.year_subject_uid);
      var facultySections = await GetRegisterSubjectSectionFaculty(subject.year_subject_uid);
      var yearSections = await GetRegisterSubjectSectionYear(subject.year_subject_uid);
      var subjectExam = await GetYearSubjectExam(subject.year_subject_uid);
      var sectionStudyTimes = await GetYearSubjectStudyTime(subject.year_subject_uid);
      var sectionStudyTimeDictionary=sectionStudyTimes.GroupBy(s=>new {s.year_subject_section_uid,s.section_type_id})
        .Select(s=>new {
          key=s.Key.year_subject_section_uid.ToString()+s.Key.section_type_id.ToString(),
          value=s.Where(t=>t.study_time_text_en!=null)
            .Select(x=>new section_study_time(){
              study_from_time = x.study_from_time,
              study_to_time = x.study_to_time,
              study_time_text_en = x.study_time_text_en}).ToList()})
        .ToDictionary(s=>s.key,s=>s.value);
      //var studyTimes = await GetStudyTimes()
      //List<study_subject_section> _studySubjectSections;
      if (facultySections.Any())
      {
        var currentFacultySections =
          facultySections.Where(s => s.college_faculty_uid == registerTokenDetail.college_faculty_uid).ToList();
        if (currentFacultySections.Any())
        {
          subjectSections = subjectSections.Where(s =>
            currentFacultySections.Any(c => c.year_subject_section_uid == s.year_subject_section_uid)).ToList();
        }
        else
        {
          subjectSections = subjectSections.Where(s =>
            currentFacultySections.All(c => c.year_subject_section_uid == s.year_subject_section_uid)).ToList();
        }
      }

      if (yearSections.Any())
      {
        var currentYearSections = yearSections
          .Where(s => s.entry_academic_year_uid == registerTokenDetail.entry_academic_year_uid).ToList();
        if (currentYearSections.Any())
        {
          subjectSections = subjectSections.Where(s =>
            currentYearSections.Any(c => c.year_subject_section_uid == s.year_subject_section_uid)).ToList();
        }
        else
        {
          subjectSections = subjectSections.Where(s =>
            currentYearSections.All(c => c.year_subject_section_uid == s.year_subject_section_uid)).ToList();
        }
      }

      var availableLectureSections = subjectSections.Where(s => s.section_type_id == 1).ToList();
      var availableLabSections = subjectSections.Where(s => s.section_type_id == 2).ToList();
      //var _availableTopics = _subjectSections.Where(s => s.topic_id != null).ToList();

      foreach (var availableLectureSection in availableLectureSections)
      {
        availableLectureSection.is_full = (availableLectureSection.reserved??0) + (availableLectureSection.confirmed??0) >= (availableLectureSection.quota??0);
        var sectionStudyTimeKey = availableLectureSection.year_subject_section_uid.ToString() +
                                       availableLectureSection.section_type_id.ToString();
        if (sectionStudyTimeDictionary.ContainsKey(sectionStudyTimeKey))
        {
          availableLectureSection.study_time_text_en= string.Join(",", sectionStudyTimeDictionary[sectionStudyTimeKey].Select(x => x.study_time_text_en));

        }


      }
      foreach (var availableLabSection in availableLabSections)
      {
        availableLabSection.is_full = (availableLabSection.reserved??0) + (availableLabSection.confirmed??0) >= (availableLabSection.quota??0);
        var sectionStudyTimeKey = availableLabSection.year_subject_section_uid.ToString() +
                                  availableLabSection.section_type_id.ToString();
        if (sectionStudyTimeDictionary.ContainsKey(sectionStudyTimeKey))
        {
          availableLabSection.study_time_text_en= string.Join(",", sectionStudyTimeDictionary[sectionStudyTimeKey].Select(x => x.study_time_text_en));

        }
      }

      //var subjectExam = new List<register_subject_exam>(); //await _dapperDb.GetStudySubjectExam(_subject.subject_year_id.Value, academicSemesterId);
      var registerSubject = new register_subject_detail(subject)
      {
        lecture_sections = availableLectureSections, //.Select(a => new register_subject_section(a)).ToList(),
        lab_sections = availableLabSections //.Select(a => new register_subject_section(a)).ToList()
      };

      if (subjectExam != null)
      {
        registerSubject.midterm_lecture_exam_from_datetime = subjectExam.midterm_lecture_exam_from_datetime;
        registerSubject.midterm_lecture_exam_to_datetime = subjectExam.midterm_lecture_exam_to_datetime;

        registerSubject.midterm_lab_exam_from_datetime = subjectExam.midterm_lab_exam_from_datetime;
        registerSubject.midterm_lab_exam_to_datetime = subjectExam.midterm_lab_exam_to_datetime;

        registerSubject.final_lecture_exam_from_datetime = subjectExam.final_lecture_exam_from_datetime;
        registerSubject.final_lecture_exam_to_datetime = subjectExam.final_lecture_exam_to_datetime;

        registerSubject.final_lab_exam_from_datetime = subjectExam.final_lab_exam_from_datetime;
        registerSubject.final_lab_exam_to_datetime = subjectExam.final_lab_exam_to_datetime;

      }
      // _registerSubject.availableTopics =
      //   _availableLabSections.Select(a => new MDEEnrollmentSubjectSection(a)).ToList();

      // return registerSubject;
      return registerSubject;
    }

    // public async Task<register_detail> AddSubject(string registerTypeCode,Guid registerTokenUid,Guid studentUid, register_subject registerSubject,
    //   Guid academicYearUid, Guid semesterUid, Guid educationTypeUid, int facultyId, int majorId, string package,
    //   Guid academicYearEntryUid, string language, short provisionRegisterTypeId, string studentStatus)
    public async Task<register_detail> AddSubject(string registerTypeCode,Guid registerTokenUid,register_subject registerSubject)
    {
      // if (registerSubject.lecture_section == null && registerSubject.lab_section == null)
      // {
      //   throw new Exception("NOT_FOUND_SECTION");
      // }
      //Console.WriteLine("Add subject for " + studentCode + ":" + JsonSerializer.Serialize(selectedSubject));

      //var registerTypeFlag = type;// GetRegisterTypeFlag(type);
      //Console.WriteLine("RegisterTypeFlag:" + registerTypeFlag);
      // if (type=="ADDDROP")
      // {
      //     selectedSubject.status="A";
      // }
      // Status 0,1

      // Flag 0=>Save 1=>Waiting
      // register_subject_status_id
      // 0 => Exist
      // 1 => Add
      // 2 => Drop
      // register_subject_flag_id
      // 0 => Ok
      // 1 => Waiting
      // 3 => Approve
      registerSubject.register_subject_status_id =1;
      registerSubject.register_subject_flag_id = 0;
      if (registerSubject.lecture_section!=null)
      {
          if (registerSubject.lecture_section.is_full??false)
          {
              registerSubject.register_subject_flag_id = 1;
          }
      }
      if (registerSubject.lab_section!=null)
      {
        if (registerSubject.lab_section.is_full??false)
        {
          registerSubject.register_subject_flag_id = 1;
        }
      }

      var registerTokenDetail = await GetRegisterToken(registerTypeCode, registerTokenUid);

      //Check is precheck is call
      var currentRegisterSubjects = _cacheService.GetRegisterSubjects(registerTokenDetail.student_uid);// cacheService.GetStudentSelectedSubjects(studentCode);
      if ((currentRegisterSubjects.Sum(r => r.credit)??0) + (registerSubject.credit??0) > (registerTokenDetail.max_credit??0))
      {
        throw new DataException("OVER_MAX_CREDIT");
      }

      var provisionRegister = await GetProvisionRegister(registerTokenDetail.student_uid, registerTokenDetail.register_academic_year_uid,
        registerTokenDetail.register_academic_semester_uid, registerTypeCode);;

      if (!currentRegisterSubjects.Any())
      {
        if (provisionRegister != null)
        {
          var provisionRegisterSubjects =
            await GetProvisionRegisterSubject(provisionRegister.provision_register_uid.Value);
          var subjects = provisionRegisterSubjects.Select(s=>new register_subject(s)).ToList();
          if (provisionRegisterSubjects.Any())
          {
            _cacheService.AddRegisterSubjects(registerTokenDetail.student_uid, subjects);
            currentRegisterSubjects = subjects;
          }
        }
      }
      if (currentRegisterSubjects.Any(s => s.year_subject_uid == registerSubject.year_subject_uid))
      {

        throw new DataException("EXISTING_SUBJECT",registerSubject.year_subject_code);
      }

      if (currentRegisterSubjects != null)
      {

        // if (currentRegisterSubjects.Any(s=>s.subject_code==registerSubject.subject_code))
        // {
        //     throw new Exception("EXISTING_SUBJECT");
        // }
        //var currentSelectedSubjects = studentEnrollments;

        //bool overlappedStudyTime = false;
        //string overlappedStudySubjectCode = "";
        var studentStudyTimes = _cacheService.GetStudentStudyTime(registerTokenDetail.student_uid);
        var subjectStudyTimes = _cacheService.GetSubjectStudyTime(registerSubject);
        var overlappedStudyTime = CheckOverlappedStudyTime(studentStudyTimes, subjectStudyTimes);
        if (overlappedStudyTime!="")
        {
          throw new DataException("OVERLAPPED_STUDY_TIME", overlappedStudyTime);
        }

        var studentExamTimes = _cacheService.GetStudentExamTime(registerTokenDetail.student_uid);
        var subjectExamTimes = _cacheService.GetSubjectExamTime(registerSubject);
        var overlappedExamTime = CheckOverlappedExamTime(studentExamTimes, subjectExamTimes);
        if (overlappedExamTime!="")
        {
          throw new DataException("OVERLAPPED_EXAM_TIME", overlappedExamTime);
        }

        //bool overlappedExamTime = false;
        //string overlappedExamSubjectCode = "";

        // var allExamTimes =
        //   GetExamTimes(currentRegisterSubjects); //.GetStudentExamTime(studentEnrollments);
        // var subjectExamTimes = GetSubjectExamTimes(registerSubject);
        // if (subjectExamTimes.Any())
        // {
        //   foreach (var allExamTime in allExamTimes)
        //   {
        //     foreach (var subjectExamTime in subjectExamTimes)
        //     {
        //       if ((subjectExamTime.from_time >= allExamTime.from_time &&
        //            subjectExamTime.from_time <= allExamTime.to_time) ||
        //           (subjectExamTime.to_time >= allExamTime.from_time &&
        //            subjectExamTime.to_time <= allExamTime.to_time))
        //       {
        //         overlappedExamTime = true;
        //         overlappedExamSubjectCode = allExamTime.subject_code;
        //         break;
        //       }
        //     }
        //   }
        // }
        //
        // if (overlappedExamTime)
        // {
        //   //throw new DataException("OVERLAPPED_EXAM_TIME", overlappedExamSubjectCode);
        // }
        // if (registerSubject.is_topic_subject.HasValue && registerSubject.is_topic_subject.Value)
        // {
        //   registerSubject.credit = registerSubject.lecture_section.topic_credit;
        // }


      }

      // else
      // {
      //     if (selectedSubject.isSpecialTopic=="Y")
      //     {
      //         selectedSubject.credit = selectedSubject.selectedTopic.topicCredit;
      //     }
      //     //diEnrollmentSubject.TryAdd(studentCode,new List<MDESelectedSubject>(){selectedSubject});
      //
      // }





      //
      // if (registerSubject.lecture_section != null)
      // {
      //   if (registerSubject.status != "WAITING" && registerSubject.lecture_section.section_id.HasValue)
      //   {
      //     await _dapperDb.AcquireReservedSeat(registerSubject.lecture_section.section_id.Value);
      //     //await dapperDB.ReserveSeat(registerYear,registerSemester,selectedSubject.subjectSeq,selectedSubject.subjectCode,"C",selectedSubject.selectedLectureGroup.sectionId);
      //   }
      //
      // }
      //
      // if (registerSubject.lab_section != null)
      // {
      //   if (registerSubject.status != "WAITING" && registerSubject.lab_section.section_id.HasValue)
      //   {
      //     await _dapperDb.AcquireReservedSeat(registerSubject.lab_section.section_id.Value);
      //     //await dapperDB.ReserveSeat(registerYear,registerSemester,selectedSubject.subjectSeq,selectedSubject.subjectCode,"B",selectedSubject.selectedLabGroup.sectionId);
      //   }
      //
      // }




      if (registerTypeCode is "PRE" or "LATE")
      {
        var registerSubjects = _cacheService.GetRegisterSubjects(registerTokenDetail.student_uid);

        // else
        // {
        //
        // }
        // if (registerSubjects != null)
        // {
        //   registerSubjects.Add(registerSubject);
        // }
        // else
        // {
        //   registerSubjects = _cacheService.AddNewRegisterSubject(registerTokenDetail.student_uid, registerSubject);
        //
        // }

        if (provisionRegister == null)
        {
          provisionRegister = new t_provision_register(registerTokenDetail.student_uid, registerTokenDetail.register_academic_year_uid, registerTokenDetail.register_academic_semester_uid,
            registerSubjects.Count, registerSubjects.Sum(s => s.credit) ?? 0, registerTypeCode , 1);
          //Add provisionRegister
          await _context.t_provision_register.AddAsync(provisionRegister);
          await _context.SaveChangesAsync();



        }
        if (registerSubject.register_subject_status_id==2)
        {
          //Add waiting list
        }
        else
        {
          if (registerSubject.lecture_section != null)
          {
            var yearSubjectSection = await _context.t_year_subject_section.FirstOrDefaultAsync(y =>
              y.year_subject_section_uid == registerSubject.lecture_section.year_subject_section_uid);
            yearSubjectSection.reserved += 1;
          }
          if (registerSubject.lab_section != null)
          {
            var yearSubjectSection = await _context.t_year_subject_section.FirstOrDefaultAsync(y =>
              y.year_subject_section_uid == registerSubject.lab_section.year_subject_section_uid);
            yearSubjectSection.reserved += 1;
          }
          var tProvisionRegisterSubject =
            new t_provision_register_subject(registerSubject);
          tProvisionRegisterSubject.provision_register_uid = provisionRegister.provision_register_uid;
          await _context.t_provision_register_subject.AddAsync(tProvisionRegisterSubject);
          await _context.SaveChangesAsync();
          _cacheService.AddRegisterSubject(registerTokenDetail.student_uid,registerSubject);
          if (registerSubject.register_subject_flag_id == 1)
          {
            var yearSubjectWaitingList = new t_year_subject_waiting_list()
            {
              student_uid = registerTokenDetail.student_uid,
              academic_year_uid = registerTokenDetail.register_academic_year_uid,
              academic_semester_uid = registerTokenDetail.register_academic_semester_uid,
              year_subject_uid = registerSubject.year_subject_uid,
              lecture_year_subject_section_uid = registerSubject.lecture_section?.year_subject_section_uid,
              lab_year_subject_section_uid = registerSubject.lab_section?.year_subject_section_uid
            };
            await _context.t_year_subject_waiting_list.AddAsync(yearSubjectWaitingList);
            await _context.SaveChangesAsync();
          }
          //tProvisionRegister.provision_register_subjects = tProvisionRegisterSubject;
          //Add provisionRegisterSubject
          //await _dapperDb.AddProvisionRegisterSubject(provisionRegisterSubject);
          //provisionRegister.pro

        }



        //await _context.t_provision_register.AddAsync(provisionRegister);

        return await GetRegisterDetail(registerSubjects, "TH", registerTypeCode,
          registerTokenDetail);



      }
      else //ADDDROP
      {
        var registerSubjects = _cacheService.GetRegisterSubjects(registerTokenDetail.student_uid);

        // else
        // {
        //
        // }
        // if (registerSubjects != null)
        // {
        //   registerSubjects.Add(registerSubject);
        // }
        // else
        // {
        //   registerSubjects = _cacheService.AddNewRegisterSubject(registerTokenDetail.student_uid, registerSubject);
        //
        // }

        if (provisionRegister == null)
        {
          provisionRegister = new t_provision_register(registerTokenDetail.student_uid, registerTokenDetail.register_academic_year_uid, registerTokenDetail.register_academic_semester_uid,
            registerSubjects.Count, registerSubjects.Sum(s => s.credit) ?? 0, registerTypeCode , 1);
          //Add provisionRegister
          await _context.t_provision_register.AddAsync(provisionRegister);
          await _context.SaveChangesAsync();



        }
        if (registerSubject.register_subject_status_id==2)
        {
          //Add waiting list
        }
        else
        {
          if (registerSubject.lecture_section != null)
          {
            var yearSubjectSection = await _context.t_year_subject_section.FirstOrDefaultAsync(y =>
              y.year_subject_section_uid == registerSubject.lecture_section.year_subject_section_uid);
            yearSubjectSection.reserved += 1;
          }
          if (registerSubject.lab_section != null)
          {
            var yearSubjectSection = await _context.t_year_subject_section.FirstOrDefaultAsync(y =>
              y.year_subject_section_uid == registerSubject.lab_section.year_subject_section_uid);
            yearSubjectSection.reserved += 1;
          }
          var tProvisionRegisterSubject =
            new t_provision_register_subject(registerSubject);
          tProvisionRegisterSubject.provision_register_uid = provisionRegister.provision_register_uid;
          await _context.t_provision_register_subject.AddAsync(tProvisionRegisterSubject);
          await _context.SaveChangesAsync();
          
          _cacheService.AddRegisterSubject(registerTokenDetail.student_uid,registerSubject);
          if (registerSubject.register_subject_flag_id == 1)
          {
            var yearSubjectWaitingList = new t_year_subject_waiting_list()
            {
              student_uid = registerTokenDetail.student_uid,
              academic_year_uid = registerTokenDetail.register_academic_year_uid,
              academic_semester_uid = registerTokenDetail.register_academic_semester_uid,
              year_subject_uid = registerSubject.year_subject_uid,
              lecture_year_subject_section_uid = registerSubject.lecture_section?.year_subject_section_uid,
              lab_year_subject_section_uid = registerSubject.lab_section?.year_subject_section_uid
            };
            await _context.t_year_subject_waiting_list.AddAsync(yearSubjectWaitingList);
            await _context.SaveChangesAsync();
          }
          //tProvisionRegister.provision_register_subjects = tProvisionRegisterSubject;
          //Add provisionRegisterSubject
          //await _dapperDb.AddProvisionRegisterSubject(provisionRegisterSubject);
          //provisionRegister.pro

        }



        //await _context.t_provision_register.AddAsync(provisionRegister);

        return await GetRegisterDetail(registerSubjects, "TH", registerTypeCode,
          registerTokenDetail);


        //return null;
      }



      //if (diSubjectSection.ContainsKey(selectedSubject.subjectCode + selectedSubject))

    }

     public async Task<register_detail> RemoveSubject(string registerTypeCode,Guid registerTokenUid,Guid yearSubjectUid)
    {

      var registerTokenDetail = await GetRegisterToken(registerTypeCode, registerTokenUid);
      var currentRegisterSubjects = _cacheService.GetRegisterSubjects(registerTokenDetail.student_uid);// cacheService.GetStudentSelectedSubjects(studentCode);
      var removeRegisterSubject = currentRegisterSubjects.FirstOrDefault(s => s.year_subject_uid == yearSubjectUid);
      if (removeRegisterSubject == null)
      {
        throw new Exception("SUBJECT_NOT_FOUND");
      }
      if (registerTypeCode is "PRE" or "LATE")
      {
        var provisionRegister = await GetProvisionRegister(registerTokenDetail.student_uid, registerTokenDetail.register_academic_year_uid,
          registerTokenDetail.register_academic_semester_uid, registerTypeCode);
        var provisionRegisterSubject = await _context.t_provision_register_subject.FirstOrDefaultAsync(p =>
          p.provision_register_uid == provisionRegister.provision_register_uid && p.year_subject_uid == yearSubjectUid);
        _context.t_provision_register_subject.Remove(provisionRegisterSubject);
        await _context.SaveChangesAsync();
        var remainRegisterSubjects=_cacheService.RemoveRegisterSubject(registerTokenDetail.student_uid, removeRegisterSubject);




        //await _context.t_provision_register.AddAsync(provisionRegister);

        return await GetRegisterDetail(remainRegisterSubjects,  "TH", registerTypeCode,
          registerTokenDetail);



      }
      else //ADDDROP
      {
        return null;
      }



      //if (diSubjectSection.ContainsKey(selectedSubject.subjectCode + selectedSubject))

    }

     public async Task<register_summary> CounterRegisterConfirm(counter_register_detail counterRegisterDetail,
       string registerTypeCode, Guid registerTokenUid,
       string language)
     {
       var registerTokenDetail = await GetRegisterToken(registerTypeCode, registerTokenUid);
       var provisionRegister = await GetProvisionRegister(registerTokenDetail, registerTypeCode, true);
       List<t_provision_register_subject> provisionRegisterSubjects = null;
       if (provisionRegister is { provision_register_uid: { } })
       {
         provisionRegisterSubjects =
           await GetProvisionRegisterSubject(provisionRegister.provision_register_uid.Value);
       }

       var totalCredit = provisionRegisterSubjects.Where(s => s.register_subject_status_id != 1).Sum(s => s.credit);
       //Total subject exclude waiting
       int totalSubjects = provisionRegisterSubjects.Count(s => s.register_subject_status_id != 1);
       decimal totalLectureAmount = 0;
       decimal totalLabAmount = 0;
       decimal creditAmount = 0;
       decimal totalCreditAmount = 0;
       //List<MDESubject> _subjects = new List<MDESubject>();
       //List<EN_WEB_REGISTER_DETAIL> registerItems = new List<EN_WEB_REGISTER_DETAIL>();
       //var studentEnrollment = _cacheService.GetStudentEnrollment(studentCode);
       //Subjects exclude waiting
       provisionRegisterSubjects = provisionRegisterSubjects.Where(s => s.register_subject_status_id != 1).ToList();
       //Remove waiting list
       //await _dapperDb.RemoveWaitingList(studentCode,registerYear,registerSemester,_pendingRegister.TX_RUNNING.Value);
       foreach (var provisionRegisterSubject in provisionRegisterSubjects)
       {
         //registerItems.Add(new EN_WEB_REGISTER_DETAIL(studentCode,registerYear,registerSemester,_selectedSubject));
         //_subjects.Add(new MDESubject(_selectedSubject,language));
         //_totalCredit += (int)_selectedSubject.credit;
         totalLectureAmount += provisionRegisterSubject.total_lecture_amount ?? 0;
         totalLabAmount += provisionRegisterSubject.total_lab_amount ?? 0; //  * _selectedSubject.credit;

       }

       decimal lateRegistrationFee = 0;

       // if (provisionRegisterTypeId==2)
       // {
       //     var lateRegistration = await dapperDB.GetLateRegistrationFee(registerYear,registerSemester,educationTypeCode,studentStatus);
       //
       //     if (lateRegistration!=null)
       //     {
       //         Console.WriteLine(lateRegistration.LATE_TRANSFER_DATE_FROM.Value.ToString());
       //         var totalDays = (DateTime.Today - lateRegistration.LATE_TRANSFER_DATE_FROM.Value).TotalDays + 1;
       //         lateRegistrationFee = lateRegistration.FINE_RATE.Value * (decimal)totalDays;
       //         if (lateRegistrationFee > lateRegistration.MAX_FINE)
       //         {
       //             lateRegistrationFee = lateRegistration.MAX_FINE.Value;
       //         }
       //         // credits.Add(new payment(){name=cacheService.GetMessage("LATEFEE",language),amount=lateRegistrationFee});
       //         // _totalPayment += lateRegistrationFee;
       //     }
       // }

       decimal usedScholarship = 0;
       decimal feeAmount = 0;
       decimal advanceAmount = 0;

       var loan = await GetStudentLoan(registerTokenDetail);

       var scholarships = counterRegisterDetail.scholarships;


       var advances = counterRegisterDetail.advance_payments;
       if (advances.Any())
       {
         advanceAmount = advances.Sum(a => a.left_amount) ?? 0;
       }


       var fees = counterRegisterDetail.fees; //.Select(f=>new re)
       if (fees.Any())
       {
         feeAmount = fees.Sum(f => f.fee_amount) ?? 0;
       }

       if (advances.Any())
       {
         advanceAmount = advances.Sum(a => a.left_amount) ?? 0;
       }


       if (fees.Any())
       {
         feeAmount = fees.Sum(f => f.fee_amount) ?? 0;
       }

       var package = "N";
       if (package.ToUpper() == "Y")
       {


         var packageAmount = registerTokenDetail.package_amount ?? 0;
         if (scholarships != null && scholarships.Any())
         {
           foreach (var scholarship in scholarships)
           {
             scholarship.used_amount = 0;
             var creditScholarship = scholarship.expense.FirstOrDefault(e => e.expenseId == 2);
             if (creditScholarship == null) continue;
             if (creditScholarship.amountType == "money")
             {
               usedScholarship += creditScholarship.amount;
               packageAmount -= usedScholarship;
               scholarship.used_amount += creditScholarship.amount;
             }
             else
             {
               var used = (packageAmount * (creditScholarship.amount / 100));
               usedScholarship += used;
               packageAmount -= usedScholarship;
               scholarship.used_amount += used;
             }
           }
         }
         decimal _extraFeeAmount = 0;
         decimal _totalFeeAmount = feeAmount;
         decimal _totalExtraFeeAmount = 0;

         // List<MDERegisterFeeWeb> registerFeeWebs = new List<MDERegisterFeeWeb>();
         // if (fees.Count() >0)
         // {
         //     int seq = 1;
         //     foreach(var _fee in _fees)
         //     {
         //         registerFeeWebs.Add(new MDERegisterFeeWeb(studentCode,registerYear,registerSemester,seq,_fee));
         //         seq += 1;
         //         if (_fee.feeCode=="003" || _fee.feeCode=="006")
         //         {
         //             _totalExtraFeeAmount += _fee.amount;
         //             if (_scholarshipFees!=null)
         //             {
         //                 var _scholarshipFee = _scholarshipFees.FirstOrDefault(sf=>sf.feeCode==_fee.feeCode);
         //                 if (_scholarshipFee!=null)
         //                 {
         //                     _usedScholarship += _fee.amount * (_scholarshipFee.percentDiscount/100);
         //                     _extraFeeAmount += _fee.amount - (_fee.amount * (_scholarshipFee.percentDiscount/100)); //* (100 - _scholarshipFee.percentDiscount);
         //                 }
         //                 else
         //                 {
         //                     _extraFeeAmount += _fee.amount;
         //                 }
         //             }
         //             else
         //             {
         //                 _extraFeeAmount += _fee.amount;
         //             }
         //         }
         //         else
         //         {
         //             _totalFeeAmount += _fee.amount;
         //             // if (_scholarshipFees!=null)
         //             // {
         //             //     var _scholarshipFee = _scholarshipFees.FirstOrDefault(sf=>sf.feeCode==_fee.feeCode);
         //             //     if (_scholarshipFee!=null)
         //             //     {
         //             //         // _usedScholarship += _fee.amount * (_scholarshipFee.percentDiscount/100);
         //             //         // _feeAmount += _fee.amount - (_fee.amount * (_scholarshipFee.percentDiscount/100)); //* (100 - _scholarshipFee.percentDiscount);
         //             //         _usedScholarship += _fee.amount * (_scholarshipFee.percentDiscount/100);
         //             //         _feeAmount += _fee.amount - (_fee.amount * (_scholarshipFee.percentDiscount/100));
         //             //     }
         //             //     else
         //             //     {
         //             //         _feeAmount += _fee.amount;
         //             //     }
         //             // }
         //             // else
         //             // {
         //                 _feeAmount += _fee.amount;
         //             // }
         //         }
         //
         //
         //     }
         //
         // }
         decimal usedLoan = 0;
         decimal _loanAmount = 0;
         if (loan != null)
         {
           _loanAmount = (loan.loan_amount ?? 0) - (loan.use_amount ?? 0);
         }

         // _feeAmount -= _scholarshipFeeDiscount;

         decimal usedAdvance = 0;
         if (advanceAmount > 0)
         {
           if (lateRegistrationFee > 0)
           {
             if (advanceAmount <= lateRegistrationFee)
             {

               usedAdvance = advanceAmount;
               //lateRegistrationFee = lateRegistrationFee - advanceAmount;
               advanceAmount = 0;
             }
             else
             {
               usedAdvance = lateRegistrationFee;
               advanceAmount = advanceAmount - lateRegistrationFee;
               //lateRegistrationFee = 0;

             }

           }

           if (packageAmount > 0)
           {
             if (advanceAmount <= packageAmount)
             {

               usedAdvance += advanceAmount;
               packageAmount = packageAmount - advanceAmount;
               advanceAmount = 0;
             }
             else
             {
               usedAdvance += packageAmount;
               advanceAmount = advanceAmount - packageAmount;
               packageAmount = 0;

             }
           }

           if (advanceAmount > 0 && feeAmount > 0)
           {
             if (advanceAmount <= feeAmount)
             {
               usedAdvance += advanceAmount;
               feeAmount = feeAmount - advanceAmount;
               advanceAmount = 0;
             }
             else
             {
               usedAdvance += feeAmount;
               advanceAmount = advanceAmount - feeAmount;
               feeAmount = 0;
             }
           }

           if (advanceAmount > 0 && _extraFeeAmount > 0)
           {
             if (advanceAmount <= _extraFeeAmount)
             {
               usedAdvance += advanceAmount;
               _extraFeeAmount = _extraFeeAmount - advanceAmount;
               advanceAmount = 0;
             }
             else
             {
               usedAdvance += _extraFeeAmount;
               advanceAmount = advanceAmount - _extraFeeAmount;
               _extraFeeAmount = 0;
             }
           }
         }

         // Console.WriteLine("Credit Amount:" + _creditAmount.ToString());
         // Console.WriteLine("Advanced Amount:" + _advanceAmount.ToString());
         // Console.WriteLine("Used Advanced Amount:" + _usedAdvance.ToString());
         if (packageAmount > 0)
         {
           if (_loanAmount > 0)
           {
             if (_loanAmount <= packageAmount)
             {
               usedLoan = _loanAmount;
               packageAmount = packageAmount - _loanAmount;
               _loanAmount = 0;


             }
             else
             {
               _loanAmount = _loanAmount - packageAmount;
               usedLoan = packageAmount;
               packageAmount = 0;
             }
           }
         }

         // if (feeAmount > 0)
         // {
         //   if (_loanAmount > 0)
         //   {
         //     if (_loanAmount <= feeAmount)
         //     {
         //       usedLoan = usedLoan + _loanAmount;
         //       feeAmount = feeAmount - _loanAmount;
         //       _loanAmount = 0;
         //
         //
         //     }
         //     else
         //     {
         //       _loanAmount = _loanAmount - feeAmount;
         //       usedLoan = usedLoan + feeAmount;
         //       feeAmount = 0;
         //
         //
         //
         //     }
         //   }
         // }

         decimal totalPayment = packageAmount + _totalExtraFeeAmount + lateRegistrationFee -
                                usedScholarship - usedLoan - usedAdvance;

         var newAdvanceAmount = advanceAmount - usedAdvance;
         List<payment> credits = new List<payment>();
         List<payment> debits = new List<payment>();
         payment total = new payment();
         // if (totalCreditAmount > 0)
         // {
         credits.Add(new payment()
         {
           type_name = "PACKAGE",
           name = "ค่าเหมาจ่าย", //_messageService.GetMessage("CREDITFEE",language)
           amount = registerTokenDetail.package_amount ?? 0
         });
         // }

         // if (_totalFeeAmount + _totalExtraFeeAmount > 0)
         // {
         //   credits.AddRange(fees.Select(f => new payment() { name = f.fee_name_th, amount = f.fee_amount ?? 0 }));
         //  }

         if (registerTypeCode is "LATE")
         {
           // var lateRegistration = await dapperDB.GetLateRegistrationFee(registerYear,registerSemester,educationTypeCode,studentStatus);
           // if (lateRegistration!=null)
           // {

           //     var totalDays = (DateTime.Today - lateRegistration.LATE_TRANSFER_DATE_FROM.Value).TotalDays + 1;
           //     var lateRegistrationFee = lateRegistration.FINE_RATE.Value * (decimal)totalDays;
           //     if (lateRegistrationFee > lateRegistration.MAX_FINE)
           //     {
           //         lateRegistrationFee = lateRegistration.MAX_FINE.Value;
           //     }
           credits.Add(new payment()
             { name = _messageService.GetMessage("LATEFEE", language), amount = lateRegistrationFee });
           //    _totalPayment += lateRegistrationFee;
           //}
         }

         if (usedScholarship > 0)
         {
           //debits.Add(new payment(){name=cacheService.GetMessage("SCHOLARSHIP",language) + " " + _scholarship.scholarshipName,amount=_usedScholarship});
           debits.AddRange(scholarships.Where(s => s.used_amount > 0).Select(s => new payment()
           {
             code = s.scholarship_code,
             name = s.scholarship_name,
             type_name = "SCH",
             amount = s.used_amount.Value
           }));
         }

         if (usedAdvance > 0)
         {
           debits.Add(new payment()
           {
             name = _messageService.GetMessage("ADVANCE", language) + " " +
                    String.Join("/", advances.Select(a => a.advance_no).ToList()),
             amount = usedAdvance
           });
         }

         if (usedLoan > 0)
         {
           debits.Add(new payment()
             { name = _messageService.GetMessage("LOAN", language) + " " + loan.contract_no, amount = usedLoan });
         }

         //var payEnable = true;
         if (totalPayment == 0 && newAdvanceAmount > 0)
         {
           total.name = _messageService.GetMessage("ADVANCEDLEFT", language);
           total.amount = newAdvanceAmount;
           //payEnable = false;
         }
         else
         {
           // if (_totalPayment == 0)
           // {
           //     payEnable = false;
           // }
           total.name = _messageService.GetMessage("TOTALPAYMENT", language);
           total.amount = totalPayment;
         }
         provisionRegister.register_status_id = 2;
         provisionRegister.payment_amount = totalPayment;
         provisionRegister.package_amount = packageAmount;
         provisionRegister.advanced_amount = usedAdvance;
         provisionRegister.scholarship_amount = usedScholarship;
         var registerSummary= new register_summary()
         {
           currentCredit = totalCredit.Value,
           totalCredit = totalCredit.Value,
           // creditFee = totalCreditAmount, //totalLectureAmount + totalLabAmount,
           // academicFee = _totalFeeAmount + _totalExtraFeeAmount,
           // accidentInsuranceFee = 0,
           // fundAmount = usedScholarship,
           // loanAmount = _usedLoan,
           // advanceAmount = _usedAdvance,
           totalPaymentAmount = totalPayment,
           //loan = _loanDetail,
           //academics = _fees.ToList(),
           //fund = _scholarshipDetail,
           //subjects = _subjects,
           subjects = provisionRegisterSubjects.Select(s => new register_subject(s)).ToList(),
           debits = debits,
           credits = credits,
           total = total
         };
         await CreateRegisterBill(registerTokenDetail.student_uid, registerSummary, provisionRegister.provision_register_uid.Value);

         return registerSummary;


       }
       else
       {

         creditAmount = totalLectureAmount + totalLabAmount;
         totalCreditAmount = totalLectureAmount + totalLabAmount;

         var provisionRegisterFees = new List<t_provision_register_fee>();
         if (fees.Any())
         {
           int seq = 1;
           foreach (var fee in fees)
           {
             //registerFeeWebs.Add(new MDERegisterFeeWeb(studentCode,registerYear,registerSemester,seq,_fee));
             seq += 1;
             provisionRegisterFees.Add(new t_provision_register_fee(fee)
               { provision_register_uid = provisionRegister.provision_register_uid });
             feeAmount += (fee.fee_amount ?? 0);
             // totalFeeAmount +=(fee.fee_amount ?? 0);



           }

         }

         if (scholarships != null)
         {
           if (scholarships.Any())
           {
             foreach (var scholarship in scholarships)
             {
               scholarship.used_amount = 0;
               var creditScholarship = scholarship.expense.FirstOrDefault(e => e.expenseId == 2);
               if (creditScholarship != null)
               {
                 if (creditScholarship.amountType == "money")
                 {
                   usedScholarship += creditScholarship.amount;
                   creditAmount -= usedScholarship;
                   scholarship.used_amount += creditScholarship.amount;
                 }
                 else
                 {
                   var used = (creditAmount * (creditScholarship.amount / 100));
                   usedScholarship += used;
                   creditAmount -= usedScholarship;
                   scholarship.used_amount += used;
                 }
               }


             }
           }
         }


         decimal _extraFeeAmount = 0;
         decimal _totalFeeAmount = feeAmount;
         decimal _totalExtraFeeAmount = 0;

         decimal usedLoan = 0;
         decimal _loanAmount = 0;
         if (loan != null)
         {
           _loanAmount = (loan.loan_amount ?? 0) - (loan.use_amount ?? 0);
         }

         decimal usedAdvance = 0;
         if (advanceAmount > 0)
         {
           if (lateRegistrationFee > 0)
           {
             if (advanceAmount <= lateRegistrationFee)
             {

               usedAdvance = advanceAmount;
               //lateRegistrationFee = lateRegistrationFee - advanceAmount;
               advanceAmount = 0;
             }
             else
             {
               usedAdvance = lateRegistrationFee;
               advanceAmount = advanceAmount - lateRegistrationFee;
               //lateRegistrationFee = 0;

             }

           }

           if (creditAmount > 0)
           {
             if (advanceAmount <= creditAmount)
             {

               usedAdvance += advanceAmount;
               creditAmount = creditAmount - advanceAmount;
               advanceAmount = 0;
             }
             else
             {
               usedAdvance += creditAmount;
               advanceAmount = advanceAmount - creditAmount;
               creditAmount = 0;

             }
           }

           if (advanceAmount > 0 && feeAmount > 0)
           {
             if (advanceAmount <= feeAmount)
             {
               usedAdvance += advanceAmount;
               feeAmount = feeAmount - advanceAmount;
               advanceAmount = 0;
             }
             else
             {
               usedAdvance += feeAmount;
               advanceAmount = advanceAmount - feeAmount;
               feeAmount = 0;
             }
           }

           if (advanceAmount > 0 && _extraFeeAmount > 0)
           {
             if (advanceAmount <= _extraFeeAmount)
             {
               usedAdvance += advanceAmount;
               _extraFeeAmount = _extraFeeAmount - advanceAmount;
               advanceAmount = 0;
             }
             else
             {
               usedAdvance += _extraFeeAmount;
               advanceAmount = advanceAmount - _extraFeeAmount;
               _extraFeeAmount = 0;
             }
           }
         }

         // Console.WriteLine("Credit Amount:" + _creditAmount.ToString());
         // Console.WriteLine("Advanced Amount:" + _advanceAmount.ToString());
         // Console.WriteLine("Used Advanced Amount:" + _usedAdvance.ToString());
         if (creditAmount > 0)
         {
           if (_loanAmount > 0)
           {
             if (_loanAmount <= creditAmount)
             {
               usedLoan = _loanAmount;
               creditAmount = creditAmount - _loanAmount;
               _loanAmount = 0;


             }
             else
             {
               _loanAmount = _loanAmount - creditAmount;
               usedLoan = creditAmount;
               creditAmount = 0;
             }
           }
         }

         if (feeAmount > 0)
         {
           if (_loanAmount > 0)
           {
             if (_loanAmount <= feeAmount)
             {
               usedLoan = usedLoan + _loanAmount;
               feeAmount = feeAmount - _loanAmount;
               _loanAmount = 0;


             }
             else
             {
               _loanAmount = _loanAmount - feeAmount;
               usedLoan = usedLoan + feeAmount;
               feeAmount = 0;



             }
           }
         }

         decimal totalPayment = totalCreditAmount + _totalFeeAmount + _totalExtraFeeAmount + lateRegistrationFee -
                                usedScholarship - usedLoan - usedAdvance;

         var newAdvanceAmount = advanceAmount - usedAdvance;


         //totalFeeAmount = feeAmount;


         List<payment> credits = new List<payment>();
         List<payment> debits = new List<payment>();
         payment total = new payment();
         if (totalCreditAmount > 0)
         {
           credits.Add(new payment()
             { name = _messageService.GetMessage("CREDITFEE", language), amount = totalCreditAmount });
         }

         if (_totalFeeAmount + _totalExtraFeeAmount > 0)
         {
           foreach (var fee in fees)
           {
             credits.Add(new payment()
               { type_name = "FEE", code = fee.fee_code, name = fee.fee_name_th, amount = fee.fee_amount ?? 0 });
           }
           //credits.Add(new payment(){name="Fee",amount=_totalFeeAmount + _totalExtraFeeAmount});
         }


         if (registerTypeCode is "LATE")
         {
           // var lateRegistration = await dapperDB.GetLateRegistrationFee(registerYear,registerSemester,educationTypeCode,studentStatus);
           // if (lateRegistration!=null)
           // {

           //     var totalDays = (DateTime.Today - lateRegistration.LATE_TRANSFER_DATE_FROM.Value).TotalDays + 1;
           //     var lateRegistrationFee = lateRegistration.FINE_RATE.Value * (decimal)totalDays;
           //     if (lateRegistrationFee > lateRegistration.MAX_FINE)
           //     {
           //         lateRegistrationFee = lateRegistration.MAX_FINE.Value;
           //     }
           credits.Add(new payment()
             { name = _messageService.GetMessage("LATEFEE", language), amount = lateRegistrationFee });
           //    _totalPayment += lateRegistrationFee;
           //}
         }

         if (usedScholarship > 0)
         {
           debits.AddRange(scholarships.Where(s => s.used_amount > 0).Select(s => new payment()
           {
             type_name = "SCH",
             code = s.scholarship_code,
             name = s.scholarship_name,
             amount = s.used_amount.Value
           }));
         }

         if (usedAdvance > 0)
         {
           debits.Add(new payment()
           {
             type_name = "ADV",
             name = _messageService.GetMessage("ADVANCE", language) + " " +
                    String.Join("/", advances.Select(a => a.advance_no).ToList()),

             code = String.Join("/", advances.Select(a => a.advance_no).ToList()),
             amount = usedAdvance
           });
         }

         if (usedLoan > 0)
         {
           debits.Add(new payment()
           {
             type_name = "LOAN",
             code = loan.contract_no,
             name = _messageService.GetMessage("LOAN", language) + " " + loan.contract_no,
             amount = usedLoan
           });
         }

         //var payEnable = true;
         if (totalPayment == 0 && newAdvanceAmount > 0)
         {
           total.name = _messageService.GetMessage("ADVANCEDLEFT", language);
           total.amount = newAdvanceAmount;
           //payEnable = false;
         }
         else
         {
           // if (_totalPayment == 0)
           // {
           //     payEnable = false;
           // }
           total.name = _messageService.GetMessage("TOTALPAYMENT", language);
           total.amount = totalPayment;
         }

         var payEnable = true;
         // if (totalPayment ==0 && newAdvanceAmount >0)
         // {
         //     total.name =_messageService.GetMessage("ADVANCEDLEFT",language);
         //     total.amount = newAdvanceAmount;
         //     payEnable = false;
         // }
         // else
         // {
         if (totalPayment == 0)
         {
           payEnable = false;
         }

         //total.name = _messageService.GetMessage("TOTALPAYMENT", language);
         //total.amount = totalPayment;
         //}
         // if (totalPayment==0) //Create receipt
         // {
         //     // var enReceipt = new EN_RECEIPT(_webRegister,_usedScholarship > 0?_scholarship.scholarshipCode:null,_usedLoan > 0?_loan.contractNo:null);
         //     // enReceipt.FAC_CODE = faculty;
         //     // enReceipt.MAJOR_CODE = major;
         //     // var enReceiptAdvances = enReceiptAdvanceWebs.Select(a=>new EN_RECEIPT_ADVANCE(a)).ToList();
         //     // var enRegisterFinalDetails = registerItems.Select(i=>new EN_REGISTER_FINAL_DETAIL(i)).ToList();
         //     // EN_ADVANCE_PAYMENT enAdvancePayment = null;
         //     // if (_newEnAdvancePayment!=null)
         //     // {
         //     //     enAdvancePayment = _newEnAdvancePayment;
         //     // }
         //     // var receiptFeeCounters = _fees.Select((f,i)=>new EN_RECEIPT_FEE_COUNTER(f,i+1)).ToList();
         //     // var enRegisterCounterDetails = registerItems.Select(i=>new EN_REGISTER_COUNTER_DETAIL(i)).ToList();
         //     //
         //     // await dapperDB.RegisterPayment(studentCode,registerYear,registerSemester,"B",enReceipt,enReceiptAdvances,null,null,enRegisterCounterDetails,enRegisterFinalDetails,enAdvancePayment,receiptFeeCounters);
         // }

         // var registerDetail = await GetRegisterDetail(registerSubjects, studentCode, academicYearId,
         //   academicSemesterId, educationTypeId, facultyId, majorId, package, academicYearEntryId, language,
         //   provisionRegisterTypeId, studentStatus);

         // var registerDetail =  await GetRegisterDetail(provisionRegisterSubjects.Select(s=>new register_subject(s)).ToList(),  "TH", registerTypeCode,
         //   registerTokenDetail);
         //_cacheService.RemoveStudentCache(studentCode);
         await _context.t_provision_register_fee.AddRangeAsync(provisionRegisterFees);
         provisionRegister.register_status_id = 2;
         provisionRegister.payment_amount = totalPayment;
         provisionRegister.fee_amount = feeAmount;
         provisionRegister.advanced_amount = usedAdvance;
         provisionRegister.credit_amount = creditAmount;
         provisionRegister.scholarship_amount = usedScholarship;
         await _context.SaveChangesAsync();
         var registerSummary = new register_summary()
         {
           totalCredit = totalCredit ?? 0,
           totalPaymentAmount = 0,
           //public string payInNo {get;set;}
           subjects = provisionRegisterSubjects.Select(s => new register_subject(s)).ToList(),
           credits = credits,
           debits = debits,
           total = total,
           payment_url = "https://rsu.71dev.com/payment-api/api/payment/redirect/" +
                         provisionRegister.bill_uid.ToString(),
           provision_register_uid = provisionRegister.provision_register_uid

         };
         await CreateRegisterBill(registerTokenDetail.student_uid, registerSummary,
           provisionRegister.provision_register_uid.Value);
         return registerSummary;




       }
     }

     private async Task<bool> CreateRegisterBill(Guid studentUid, register_summary registerSummary,
       Guid provisioningRegisterUid)
     {
       try
       {
         var student = await _context.v_student.AsNoTracking().FirstOrDefaultAsync(s => s.student_uid == studentUid);
         var newBill = new bill_request(
           DateTime.Today,
           15,
           1,
           student.student_code,
           student.display_name_th,
           "",
           // applicant.address1 + " " + (string.IsNullOrEmpty(applicant.soi) ? "" : applicant.soi) + " " +
           // (string.IsNullOrEmpty(applicant.road) ? "" : applicant.road) + " " +
           // (string.IsNullOrEmpty(applicant.sub_district_name) ? "" : applicant.sub_district_name) + " " +
           // (string.IsNullOrEmpty(applicant.district_name) ? "" : applicant.district_name) + " " +
           // (string.IsNullOrEmpty(applicant.province_name) ? "" : applicant.province_name),
           "",
           student.citizen_id,
           null,
           null,
           student.rsu_email,
           student.mobile_no,
           DateTime.Today.AddDays(15),
           "",
           registerSummary.total.amount,
           0, 0,
           registerSummary.total.amount,
           //applicantApply.payment_amount.Value, 0, 0,
           //applicantApply.payment_amount.Value,
           "", 0);
         newBill.customer_uid = student.student_uid;
      


         newBill.bill_items = registerSummary.ToBillRequestItem();
         var iamHttpClient = _httpClientFactory.CreateClient("open_id");
         //Console.WriteLine("Client ID:" + _openIdSetting.client_id);
         var dict = new Dictionary<string, string>
         {
           { "grant_type", "client_credentials" },
           { "scope", string.Join(" ", _openIdSetting.scopes) },
           { "client_id", _openIdSetting.client_id },
           { "client_secret", _openIdSetting.client_secret }
         };
         var req = new HttpRequestMessage(HttpMethod.Post, _openIdSetting.authority + "/connect/token")
           { Content = new FormUrlEncodedContent(dict) };
         var iamResponse = await iamHttpClient.SendAsync(req);
         if (iamResponse.StatusCode == HttpStatusCode.OK)
         {
           var iamResult = await iamResponse.Content.ReadAsByteArrayAsync();
           var openIdToken = JsonSerializer.Deserialize<open_id_token>(iamResult);
           //Console.WriteLine("OpenID token:" + JsonSerializer.Serialize(openIdToken));
           var httpClient = _httpClientFactory.CreateClient("ar_payment");
           JsonSerializerOptions options = new JsonSerializerOptions();
           options.Converters.Add(new DateTimeConverterUsingDateTimeParse());
           options.IgnoreNullValues = true;
           Console.WriteLine("Bill Request:" + JsonSerializer.Serialize(newBill, options));
           Console.WriteLine("Token:" + openIdToken.access_token);
           var data = new ByteArrayContent(JsonSerializer.SerializeToUtf8Bytes(newBill, options));
           var httpRequestMessage = new HttpRequestMessage
           {
             Method = HttpMethod.Post,
             RequestUri = new Uri(httpClient.BaseAddress + "v1/accounts_receivables/bills")
           };

           httpRequestMessage.Headers.Add("Authorization", "Bearer " + openIdToken.access_token);
           httpRequestMessage.Headers.Add(HttpRequestHeader.ContentType.ToString(), "application/json");
           HttpResponseMessage res = null;
           data.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");
           httpRequestMessage.Content = data;
           res = await httpClient.SendAsync(httpRequestMessage);

           if (res.StatusCode == HttpStatusCode.Created)
           {
             var provisionRegister =
               await _context.t_provision_register.FirstOrDefaultAsync(p =>
                 p.provision_register_uid == provisioningRegisterUid);
             var result = await res.Content.ReadAsByteArrayAsync();
             var createdBill = JsonSerializer.Deserialize<bill>(result);
             Console.WriteLine("Bill:" + JsonSerializer.Serialize(createdBill));
             provisionRegister.bill_no = createdBill.bill_no;
             //applicantApply.application_invoice_due_date = createdBill.payment_due_date;
             provisionRegister.bill_uid = createdBill.bill_uid;
             //createdBill.bill_url
             await _context.SaveChangesAsync();
           }
           else
           {
             Console.WriteLine("Create bill:" + res.StatusCode.ToString());
           }
         }


       }
       catch (Exception e)
       {
         Console.WriteLine(e);
         return false;

       }

       return true;
     }

     public async Task<bool> OfflinePayment(Guid provisionRegisterUid, List<register_payment> registerPayments)
     {
       var provisionRegister =
         await _context.t_provision_register.FirstOrDefaultAsync(p => p.provision_register_uid == provisionRegisterUid);
       if (provisionRegister == null) throw new Exception("Provision register not found");
       if (provisionRegister.register_status_id != 2) throw new Exception("Invalid register status");
       var receiptRequest = new receipt_request()
       {
         bill_uid = provisionRegister.bill_uid,
         payment_receives = registerPayments.Select(r => new payment_receive()
         {
           payment_method_id = r.payment_method_id,
           receive_amount = r.paid_amount,
           reference_code = r.reference_code
         }).ToList()
       };
       var iamHttpClient = _httpClientFactory.CreateClient("open_id");
       //Console.WriteLine("Client ID:" + _openIdSetting.client_id);
       var dict = new Dictionary<string, string>
       {
         { "grant_type", "client_credentials" },
         { "scope", string.Join(" ", _openIdSetting.scopes) },
         { "client_id", _openIdSetting.client_id },
         { "client_secret", _openIdSetting.client_secret }
       };
       var req = new HttpRequestMessage(HttpMethod.Post, _openIdSetting.authority + "/connect/token")
         { Content = new FormUrlEncodedContent(dict) };
       var iamResponse = await iamHttpClient.SendAsync(req);
       if (iamResponse.StatusCode == HttpStatusCode.OK)
       {
         var iamResult = await iamResponse.Content.ReadAsByteArrayAsync();
         var openIdToken = JsonSerializer.Deserialize<open_id_token>(iamResult);
         //Console.WriteLine("OpenID token:" + JsonSerializer.Serialize(openIdToken));
         var httpClient = _httpClientFactory.CreateClient("ar_payment");
         JsonSerializerOptions options = new JsonSerializerOptions();
         options.Converters.Add(new DateTimeConverterUsingDateTimeParse());
         options.IgnoreNullValues = true;
         Console.WriteLine("Token:" + openIdToken.access_token);
         var data = new ByteArrayContent(JsonSerializer.SerializeToUtf8Bytes(receiptRequest, options));
         var httpRequestMessage = new HttpRequestMessage
         {
           Method = HttpMethod.Post,
           RequestUri = new Uri(httpClient.BaseAddress + "v1/accounts_receivables/receipts/offline")
         };

         httpRequestMessage.Headers.Add("Authorization", "Bearer " + openIdToken.access_token);
         httpRequestMessage.Headers.Add(HttpRequestHeader.ContentType.ToString(), "application/json");
         HttpResponseMessage res = null;
         data.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");
         httpRequestMessage.Content = data;
         res = await httpClient.SendAsync(httpRequestMessage);

         if (res.StatusCode == HttpStatusCode.Created)
         {
           var result = await res.Content.ReadAsByteArrayAsync();
           var receipt = JsonSerializer.Deserialize<receipt>(result);
           Console.WriteLine("Receipt");
           Console.WriteLine(JsonSerializer.Serialize(receipt));
           //var provisionRegister = await _context.t_provision_register.FirstOrDefaultAsync(a => a.bill_no == billNo);
           provisionRegister.is_paid = true;
           var finalRegister = new t_final_register(provisionRegister);
           //finalRegister.final_register_subjects = new List<t_final_register_subject>()
           var provisionRegisterSubjects = await _context.t_provision_register_subject
             .Where(p => p.provision_register_uid == provisionRegister.provision_register_uid).ToListAsync();
           finalRegister.final_register_subjects =
             provisionRegisterSubjects.Select(p => new t_final_register_subject(p)).ToList();
           await _context.AddAsync(finalRegister);
           await _context.SaveChangesAsync();

           return true;
           //return paymentMethods;
         }
         else
         {
           Console.WriteLine(res.StatusCode.ToString());
           return false;
         }
       }
       else
       {
         return false;
       }
     }

     public async Task<List<payment_method>> GetOfflinePaymentMethod()
     {
       var iamHttpClient = _httpClientFactory.CreateClient("open_id");
       //Console.WriteLine("Client ID:" + _openIdSetting.client_id);
       var dict = new Dictionary<string, string>
       {
         { "grant_type", "client_credentials" },
         { "scope", string.Join(" ", _openIdSetting.scopes) },
         { "client_id", _openIdSetting.client_id },
         { "client_secret", _openIdSetting.client_secret }
       };
       var req = new HttpRequestMessage(HttpMethod.Post, _openIdSetting.authority + "/connect/token")
         { Content = new FormUrlEncodedContent(dict) };
       var iamResponse = await iamHttpClient.SendAsync(req);
       if (iamResponse.StatusCode == HttpStatusCode.OK)
       {
         var iamResult = await iamResponse.Content.ReadAsByteArrayAsync();
         var openIdToken = JsonSerializer.Deserialize<open_id_token>(iamResult);
         //Console.WriteLine("OpenID token:" + JsonSerializer.Serialize(openIdToken));
         var httpClient = _httpClientFactory.CreateClient("ar_payment");
         JsonSerializerOptions options = new JsonSerializerOptions();
         options.Converters.Add(new DateTimeConverterUsingDateTimeParse());
         options.IgnoreNullValues = true;
         Console.WriteLine("Token:" + openIdToken.access_token);
         //var data = new ByteArrayContent(JsonSerializer.SerializeToUtf8Bytes(newBill, options));
         var httpRequestMessage = new HttpRequestMessage
         {
           Method = HttpMethod.Get,
           RequestUri = new Uri(httpClient.BaseAddress + "v1/fees/payment_methods/offline")
         };

         httpRequestMessage.Headers.Add("Authorization", "Bearer " + openIdToken.access_token);
         httpRequestMessage.Headers.Add(HttpRequestHeader.ContentType.ToString(), "application/json");
         HttpResponseMessage res = null;
         res = await httpClient.SendAsync(httpRequestMessage);

         if (res.StatusCode == HttpStatusCode.OK)
         {
           var result = await res.Content.ReadAsByteArrayAsync();
           var paymentMethods = JsonSerializer.Deserialize<List<payment_method>>(result);
           return paymentMethods;
         }
       }

       return new List<payment_method>();
     }

     public async Task<register_detail> GetRegisterDetail(List<register_subject> registerSubjects, string language,
       string registerTypeCode, register_token_detail registerTokenDetail)
     {
       if (registerSubjects.Any())
       {
         var en = await GetEstimationCost(registerSubjects, language, registerTypeCode, registerTokenDetail);
         var registerDetail = new register_detail()
         {
           //academic_year_id = academicYearId,
           //academic_semester_id = academicSemesterId,
           // min_credit = minCredit,
           // max_credit = maxCredit,
           // allow_register = isEligibleToEnroll,
           // message = _messageService.GetMessage(messages[0], language),
           register_subjects = registerSubjects,
           // is_readonly = readOnly,
           //provision_register_type_id = provisionRegisterTypeId,
           total_credit = (int)registerSubjects.Sum(s => s.credit),
           //last_payment_date = lastPaymentDateText
         };
         registerDetail.Update(en);
         return registerDetail;
       }
       else
       {
         var registerDetail = new register_detail()
         {
           //academic_year_id = academicYearId,
           //academic_semester_id = academicSemesterId,
           // min_credit = minCredit,
           // max_credit = maxCredit,
           // allow_register = isEligibleToEnroll,
           // message = _messageService.GetMessage(messages[0], language),
           register_subjects = new List<register_subject>(),
           // is_readonly = readOnly,
           //provision_register_type_id = provisionRegisterTypeId,
           total_credit = 0,
           //last_payment_date = lastPaymentDateText
         };
         registerDetail.credits = new List<payment>();
         registerDetail.debits = new List<payment>();
         registerDetail.total = new payment();
         return registerDetail;
       }

     }

     async Task<register_summary> GetEstimationCost(List<register_subject> registerSubjects, string language,
       string registerTypeCode, register_token_detail registerTokenDetail)
     {
       if (!registerSubjects.Any())
       {
         registerSubjects = _cacheService.GetRegisterSubjects(registerTokenDetail.student_uid);
         if (!registerSubjects.Any())
         {
           Console.WriteLine("GetEstimationCost: No subjects found in cache");
           return null;
         }
       }

       var confirmRegisterSubjects = registerSubjects.Where(c => c.register_subject_flag_id != 1).ToList();
       var totalCredits = confirmRegisterSubjects.Sum(s => s.credit) ?? 0;
       var totalSubjects = confirmRegisterSubjects.Count();
       decimal totalLectureAmount = confirmRegisterSubjects.Sum(r => r.total_lecture_amount) ?? 0;
       decimal totalLabAmount = confirmRegisterSubjects.Sum(r => r.total_lab_amount) ?? 0;
       ;
       decimal creditAmount = 0;
       decimal totalCreditAmount = 0;
       //var _subjects = new List<MDESubject>();
       //var registerItems = new List<MDERegisterItem>();
       //var studentEnrollment = cacheService.GetStudentEnrollment(studentCode);

       // foreach (var registerSubject in registerSubjects.Where(registerSubject => registerSubject.status!="WAITING"))
       // {
       //   registerItems.Add(new MDERegisterItem(studentCode,registerYear,registerSemester,_selectedSubject));
       //   _subjects.Add(new MDESubject(_selectedSubject,language));
       //   //_totalCredit += (int)_selectedSubject.credit;
       //   totalLectureAmount += _selectedSubject.lectureAmount * _selectedSubject.credit;
       //   totalLabAmount += _selectedSubject.labAmount;//  * _selectedSubject.credit;
       // }
       decimal lateRegistrationFee = 0;
       if (registerTypeCode is "LATE")
       {
         var registerCalendarUid = registerTokenDetail.academic_register_calendar_uid;
         var academicRegisterCalendar = await _context.t_academic_register_calendar.AsNoTracking()
           .FirstOrDefaultAsync(a => a.academic_register_calendar_uid == registerCalendarUid);
         var totalDays = (DateTime.Today - academicRegisterCalendar.last_payment_date.Value).TotalDays + 1;
         lateRegistrationFee = academicRegisterCalendar.fine_rate_amount??0 * (decimal)totalDays;
         if (lateRegistrationFee > academicRegisterCalendar.max_fine_amount)
         {
             lateRegistrationFee = academicRegisterCalendar.max_fine_amount.Value;
         }

         // Get late register fee
         // var lateRegistration = await dapperDB.GetLateRegistrationFee(registerYear,registerSemester,educationTypeCode,studentStatus);
         //
         // if (lateRegistration!=null)
         // {
         //     Console.WriteLine(lateRegistration.LATE_TRANSFER_DATE_FROM.Value.ToString());
         //     var totalDays = (DateTime.Today - lateRegistration.LATE_TRANSFER_DATE_FROM.Value).TotalDays + 1;
         //     lateRegistrationFee = lateRegistration.FINE_RATE.Value * (decimal)totalDays;
         //     if (lateRegistrationFee > lateRegistration.MAX_FINE)
         //     {
         //         lateRegistrationFee = lateRegistration.MAX_FINE.Value;
         //     }
         // }
       }


       decimal usedScholarship = 0;
       decimal feeAmount = 0;
       decimal advanceAmount = 0;
       // MDScholarship _scholarship = await dapperDB.GetScholarship(registerYear,registerSemester,studentCode);
       // MDLoan _loan = await dapperDB.GetLoan(studentCode,int.Parse(registerYear));
       var loan = await GetStudentLoan(registerTokenDetail);
       var scholarships = await GetScholarship(registerTokenDetail);
       var advances =
         await GetRegisterAdvancePayment(registerTokenDetail);
       if (advances.Any())
       {
         advanceAmount = advances.Sum(a => a.left_amount) ?? 0;
       }



       // MDEScholarship _scholarshipDetail = null;
       //var package = "N";


       if (registerTokenDetail.is_package ?? false)
       {
         var packageAmount = registerTokenDetail.package_amount ?? 0;
         if (scholarships != null && scholarships.Any())
         {
           foreach (var scholarship in scholarships)
           {
             scholarship.used_amount = 0;
             var creditScholarship = scholarship.expense.FirstOrDefault(e => e.expenseId == 2);
             if (creditScholarship != null)
             {
               if (creditScholarship.amountType == "money")
               {
                 usedScholarship += creditScholarship.amount;
                 packageAmount -= usedScholarship;
                 scholarship.used_amount += creditScholarship.amount;
               }
               else
               {
                 var used = (packageAmount * (creditScholarship.amount / 100));
                 usedScholarship += used;
                 packageAmount -= usedScholarship;
                 scholarship.used_amount += used;
               }
             }


           }
         }


         decimal _extraFeeAmount = 0;
         //decimal _totalFeeAmount = 0;
         decimal _totalFeeAmount = feeAmount;
         decimal _totalExtraFeeAmount = 0;

         // List<MDERegisterFeeWeb> registerFeeWebs = new List<MDERegisterFeeWeb>();
         // if (fees.Count() >0)
         // {
         //     int seq = 1;
         //     foreach(var _fee in _fees)
         //     {
         //         registerFeeWebs.Add(new MDERegisterFeeWeb(studentCode,registerYear,registerSemester,seq,_fee));
         //         seq += 1;
         //         if (_fee.feeCode=="003" || _fee.feeCode=="006")
         //         {
         //             _totalExtraFeeAmount += _fee.amount;
         //             if (_scholarshipFees!=null)
         //             {
         //                 var _scholarshipFee = _scholarshipFees.FirstOrDefault(sf=>sf.feeCode==_fee.feeCode);
         //                 if (_scholarshipFee!=null)
         //                 {
         //                     _usedScholarship += _fee.amount * (_scholarshipFee.percentDiscount/100);
         //                     _extraFeeAmount += _fee.amount - (_fee.amount * (_scholarshipFee.percentDiscount/100)); //* (100 - _scholarshipFee.percentDiscount);
         //                 }
         //                 else
         //                 {
         //                     _extraFeeAmount += _fee.amount;
         //                 }
         //             }
         //             else
         //             {
         //                 _extraFeeAmount += _fee.amount;
         //             }
         //         }
         //         else
         //         {
         //             _totalFeeAmount += _fee.amount;
         //             // if (_scholarshipFees!=null)
         //             // {
         //             //     var _scholarshipFee = _scholarshipFees.FirstOrDefault(sf=>sf.feeCode==_fee.feeCode);
         //             //     if (_scholarshipFee!=null)
         //             //     {
         //             //         // _usedScholarship += _fee.amount * (_scholarshipFee.percentDiscount/100);
         //             //         // _feeAmount += _fee.amount - (_fee.amount * (_scholarshipFee.percentDiscount/100)); //* (100 - _scholarshipFee.percentDiscount);
         //             //         _usedScholarship += _fee.amount * (_scholarshipFee.percentDiscount/100);
         //             //         _feeAmount += _fee.amount - (_fee.amount * (_scholarshipFee.percentDiscount/100));
         //             //     }
         //             //     else
         //             //     {
         //             //         _feeAmount += _fee.amount;
         //             //     }
         //             // }
         //             // else
         //             // {
         //                 _feeAmount += _fee.amount;
         //             // }
         //         }
         //
         //
         //     }
         //
         // }
         decimal usedLoan = 0;
         decimal _loanAmount = 0;
         if (loan != null)
         {
           _loanAmount = (loan.loan_amount ?? 0) - (loan.use_amount ?? 0);
         }

         // _feeAmount -= _scholarshipFeeDiscount;

         decimal usedAdvance = 0;
         if (advanceAmount > 0)
         {
           if (lateRegistrationFee > 0)
           {
             if (advanceAmount <= lateRegistrationFee)
             {

               usedAdvance = advanceAmount;
               //lateRegistrationFee = lateRegistrationFee - advanceAmount;
               advanceAmount = 0;
             }
             else
             {
               usedAdvance = lateRegistrationFee;
               advanceAmount = advanceAmount - lateRegistrationFee;
               //lateRegistrationFee = 0;

             }

           }

           if (packageAmount > 0)
           {
             if (advanceAmount <= packageAmount)
             {

               usedAdvance += advanceAmount;
               packageAmount = packageAmount - advanceAmount;
               advanceAmount = 0;
             }
             else
             {
               usedAdvance += packageAmount;
               advanceAmount = advanceAmount - packageAmount;
               packageAmount = 0;

             }
           }

           if (advanceAmount > 0 && feeAmount > 0)
           {
             if (advanceAmount <= feeAmount)
             {
               usedAdvance += advanceAmount;
               feeAmount = feeAmount - advanceAmount;
               advanceAmount = 0;
             }
             else
             {
               usedAdvance += feeAmount;
               advanceAmount = advanceAmount - feeAmount;
               feeAmount = 0;
             }
           }

           if (advanceAmount > 0 && _extraFeeAmount > 0)
           {
             if (advanceAmount <= _extraFeeAmount)
             {
               usedAdvance += advanceAmount;
               _extraFeeAmount = _extraFeeAmount - advanceAmount;
               advanceAmount = 0;
             }
             else
             {
               usedAdvance += _extraFeeAmount;
               advanceAmount = advanceAmount - _extraFeeAmount;
               _extraFeeAmount = 0;
             }
           }
         }

         // Console.WriteLine("Credit Amount:" + _creditAmount.ToString());
         // Console.WriteLine("Advanced Amount:" + _advanceAmount.ToString());
         // Console.WriteLine("Used Advanced Amount:" + _usedAdvance.ToString());
         if (packageAmount > 0)
         {
           if (_loanAmount > 0)
           {
             if (_loanAmount <= packageAmount)
             {
               usedLoan = _loanAmount;
               packageAmount = packageAmount - _loanAmount;
               _loanAmount = 0;


             }
             else
             {
               _loanAmount = _loanAmount - packageAmount;
               usedLoan = packageAmount;
               packageAmount = 0;
             }
           }
         }

         // if (feeAmount > 0)
         // {
         //   if (_loanAmount > 0)
         //   {
         //     if (_loanAmount <= feeAmount)
         //     {
         //       usedLoan = usedLoan + _loanAmount;
         //       feeAmount = feeAmount - _loanAmount;
         //       _loanAmount = 0;
         //
         //
         //     }
         //     else
         //     {
         //       _loanAmount = _loanAmount - feeAmount;
         //       usedLoan = usedLoan + feeAmount;
         //       feeAmount = 0;
         //
         //
         //
         //     }
         //   }
         // }

         decimal totalPayment = packageAmount + _totalExtraFeeAmount + lateRegistrationFee -
                                usedScholarship - usedLoan - usedAdvance;

         var newAdvanceAmount = advanceAmount - usedAdvance;
         List<payment> credits = new List<payment>();
         List<payment> debits = new List<payment>();
         payment total = new payment();
         // if (totalCreditAmount > 0)
         // {
         credits.Add(new payment()
         {
           type_name = "PACKAGE",
           name = "ค่าเหมาจ่าย", //_messageService.GetMessage("CREDITFEE",language)
           amount = registerTokenDetail.package_amount ?? 0
         });
         // }

         // if (_totalFeeAmount + _totalExtraFeeAmount > 0)
         // {
         //   credits.AddRange(fees.Select(f => new payment() { name = f.fee_name_th, amount = f.fee_amount ?? 0 }));
         //  }

         if (registerTypeCode is "LATE")
         {
           // var lateRegistration = await dapperDB.GetLateRegistrationFee(registerYear,registerSemester,educationTypeCode,studentStatus);
           // if (lateRegistration!=null)
           // {

           //     var totalDays = (DateTime.Today - lateRegistration.LATE_TRANSFER_DATE_FROM.Value).TotalDays + 1;
           //     var lateRegistrationFee = lateRegistration.FINE_RATE.Value * (decimal)totalDays;
           //     if (lateRegistrationFee > lateRegistration.MAX_FINE)
           //     {
           //         lateRegistrationFee = lateRegistration.MAX_FINE.Value;
           //     }
           credits.Add(new payment()
             { name = _messageService.GetMessage("LATEFEE", language), amount = lateRegistrationFee });
           //    _totalPayment += lateRegistrationFee;
           //}
         }

         if (usedScholarship > 0)
         {
           //debits.Add(new payment(){name=cacheService.GetMessage("SCHOLARSHIP",language) + " " + _scholarship.scholarshipName,amount=_usedScholarship});
           debits.AddRange(scholarships.Where(s => s.used_amount > 0).Select(s => new payment()
           {
             code = s.scholarship_code,
             name = s.scholarship_name,
             type_name = "SCH",
             amount = s.used_amount.Value
           }));
         }

         if (usedAdvance > 0)
         {
           debits.Add(new payment()
           {
             name = _messageService.GetMessage("ADVANCE", language) + " " +
                    String.Join("/", advances.Select(a => a.advance_no).ToList()),
             amount = usedAdvance
           });
         }

         if (usedLoan > 0)
         {
           debits.Add(new payment()
             { name = _messageService.GetMessage("LOAN", language) + " " + loan.contract_no, amount = usedLoan });
         }

         //var payEnable = true;
         if (totalPayment == 0 && newAdvanceAmount > 0)
         {
           total.name = _messageService.GetMessage("ADVANCEDLEFT", language);
           total.amount = newAdvanceAmount;
           //payEnable = false;
         }
         else
         {
           // if (_totalPayment == 0)
           // {
           //     payEnable = false;
           // }
           total.name = _messageService.GetMessage("TOTALPAYMENT", language);
           total.amount = totalPayment;
         }

         return new register_summary()
         {
           currentCredit = totalCredits,
           totalCredit = totalCredits,
           // creditFee = totalCreditAmount, //totalLectureAmount + totalLabAmount,
           // academicFee = _totalFeeAmount + _totalExtraFeeAmount,
           // accidentInsuranceFee = 0,
           // fundAmount = usedScholarship,
           // loanAmount = _usedLoan,
           // advanceAmount = _usedAdvance,
           totalPaymentAmount = totalPayment,
           //loan = _loanDetail,
           //academics = _fees.ToList(),
           //fund = _scholarshipDetail,
           //subjects = _subjects,
           subjects = registerSubjects,
           debits = debits,
           credits = credits,
           total = total
         };

       }
       else //Credit
       {
         var fees = await GetRegisterFee(
           registerTokenDetail);

         if (fees.Any())
         {
           feeAmount = fees.Sum(f => f.fee_amount) ?? 0;
         }

         creditAmount = (totalLectureAmount + totalLabAmount);
         totalCreditAmount = totalLectureAmount + totalLabAmount;

         if (scholarships != null && scholarships.Any())
         {
           foreach (var scholarship in scholarships)
           {
             scholarship.used_amount = 0;
             var creditScholarship = scholarship.expense.FirstOrDefault(e => e.expenseId == 2);
             if (creditScholarship != null)
             {
               if (creditScholarship.amountType == "money")
               {
                 usedScholarship += creditScholarship.amount;
                 creditAmount -= usedScholarship;
                 scholarship.used_amount += creditScholarship.amount;
               }
               else
               {
                 var used = (creditAmount * (creditScholarship.amount / 100));
                 usedScholarship += used;
                 creditAmount -= usedScholarship;
                 scholarship.used_amount += used;
               }
             }


           }
         }


         decimal _extraFeeAmount = 0;
         //decimal _totalFeeAmount = 0;
         decimal _totalFeeAmount = feeAmount;
         decimal _totalExtraFeeAmount = 0;

         // List<MDERegisterFeeWeb> registerFeeWebs = new List<MDERegisterFeeWeb>();
         // if (fees.Count() >0)
         // {
         //     int seq = 1;
         //     foreach(var _fee in _fees)
         //     {
         //         registerFeeWebs.Add(new MDERegisterFeeWeb(studentCode,registerYear,registerSemester,seq,_fee));
         //         seq += 1;
         //         if (_fee.feeCode=="003" || _fee.feeCode=="006")
         //         {
         //             _totalExtraFeeAmount += _fee.amount;
         //             if (_scholarshipFees!=null)
         //             {
         //                 var _scholarshipFee = _scholarshipFees.FirstOrDefault(sf=>sf.feeCode==_fee.feeCode);
         //                 if (_scholarshipFee!=null)
         //                 {
         //                     _usedScholarship += _fee.amount * (_scholarshipFee.percentDiscount/100);
         //                     _extraFeeAmount += _fee.amount - (_fee.amount * (_scholarshipFee.percentDiscount/100)); //* (100 - _scholarshipFee.percentDiscount);
         //                 }
         //                 else
         //                 {
         //                     _extraFeeAmount += _fee.amount;
         //                 }
         //             }
         //             else
         //             {
         //                 _extraFeeAmount += _fee.amount;
         //             }
         //         }
         //         else
         //         {
         //             _totalFeeAmount += _fee.amount;
         //             // if (_scholarshipFees!=null)
         //             // {
         //             //     var _scholarshipFee = _scholarshipFees.FirstOrDefault(sf=>sf.feeCode==_fee.feeCode);
         //             //     if (_scholarshipFee!=null)
         //             //     {
         //             //         // _usedScholarship += _fee.amount * (_scholarshipFee.percentDiscount/100);
         //             //         // _feeAmount += _fee.amount - (_fee.amount * (_scholarshipFee.percentDiscount/100)); //* (100 - _scholarshipFee.percentDiscount);
         //             //         _usedScholarship += _fee.amount * (_scholarshipFee.percentDiscount/100);
         //             //         _feeAmount += _fee.amount - (_fee.amount * (_scholarshipFee.percentDiscount/100));
         //             //     }
         //             //     else
         //             //     {
         //             //         _feeAmount += _fee.amount;
         //             //     }
         //             // }
         //             // else
         //             // {
         //                 _feeAmount += _fee.amount;
         //             // }
         //         }
         //
         //
         //     }
         //
         // }
         decimal usedLoan = 0;
         decimal _loanAmount = 0;
         if (loan != null)
         {
           _loanAmount = (loan.loan_amount ?? 0) - (loan.use_amount ?? 0);
         }

         // _feeAmount -= _scholarshipFeeDiscount;

         decimal usedAdvance = 0;
         if (advanceAmount > 0)
         {
           if (lateRegistrationFee > 0)
           {
             if (advanceAmount <= lateRegistrationFee)
             {

               usedAdvance = advanceAmount;
               //lateRegistrationFee = lateRegistrationFee - advanceAmount;
               advanceAmount = 0;
             }
             else
             {
               usedAdvance = lateRegistrationFee;
               advanceAmount = advanceAmount - lateRegistrationFee;
               //lateRegistrationFee = 0;

             }

           }

           if (creditAmount > 0)
           {
             if (advanceAmount <= creditAmount)
             {

               usedAdvance += advanceAmount;
               creditAmount = creditAmount - advanceAmount;
               advanceAmount = 0;
             }
             else
             {
               usedAdvance += creditAmount;
               advanceAmount = advanceAmount - creditAmount;
               creditAmount = 0;

             }
           }

           if (advanceAmount > 0 && feeAmount > 0)
           {
             if (advanceAmount <= feeAmount)
             {
               usedAdvance += advanceAmount;
               feeAmount = feeAmount - advanceAmount;
               advanceAmount = 0;
             }
             else
             {
               usedAdvance += feeAmount;
               advanceAmount = advanceAmount - feeAmount;
               feeAmount = 0;
             }
           }

           if (advanceAmount > 0 && _extraFeeAmount > 0)
           {
             if (advanceAmount <= _extraFeeAmount)
             {
               usedAdvance += advanceAmount;
               _extraFeeAmount = _extraFeeAmount - advanceAmount;
               advanceAmount = 0;
             }
             else
             {
               usedAdvance += _extraFeeAmount;
               advanceAmount = advanceAmount - _extraFeeAmount;
               _extraFeeAmount = 0;
             }
           }
         }

         // Console.WriteLine("Credit Amount:" + _creditAmount.ToString());
         // Console.WriteLine("Advanced Amount:" + _advanceAmount.ToString());
         // Console.WriteLine("Used Advanced Amount:" + _usedAdvance.ToString());
         if (creditAmount > 0)
         {
           if (_loanAmount > 0)
           {
             if (_loanAmount <= creditAmount)
             {
               usedLoan = _loanAmount;
               creditAmount = creditAmount - _loanAmount;
               _loanAmount = 0;


             }
             else
             {
               _loanAmount = _loanAmount - creditAmount;
               usedLoan = creditAmount;
               creditAmount = 0;
             }
           }
         }

         if (feeAmount > 0)
         {
           if (_loanAmount > 0)
           {
             if (_loanAmount <= feeAmount)
             {
               usedLoan = usedLoan + _loanAmount;
               feeAmount = feeAmount - _loanAmount;
               _loanAmount = 0;


             }
             else
             {
               _loanAmount = _loanAmount - feeAmount;
               usedLoan = usedLoan + feeAmount;
               feeAmount = 0;



             }
           }
         }

         decimal totalPayment = totalCreditAmount + _totalFeeAmount + _totalExtraFeeAmount + lateRegistrationFee -
                                usedScholarship - usedLoan - usedAdvance;

         var newAdvanceAmount = advanceAmount - usedAdvance;
         List<payment> credits = new List<payment>();
         List<payment> debits = new List<payment>();
         payment total = new payment();
         if (totalCreditAmount > 0)
         {
           credits.Add(new payment()
           {
             type_name = "CREDITFEE",
             name = "ค่าหน่วยกิต", //_messageService.GetMessage("CREDITFEE",language)
             amount = totalCreditAmount
           });
         }

         if (_totalFeeAmount + _totalExtraFeeAmount > 0)
         {
           credits.AddRange(fees.Select(f => new payment() { name = f.fee_name_th, amount = f.fee_amount ?? 0 }));
         }

         if (registerTypeCode is "LATE")
         {
           // var lateRegistration = await dapperDB.GetLateRegistrationFee(registerYear,registerSemester,educationTypeCode,studentStatus);
           // if (lateRegistration!=null)
           // {

           //     var totalDays = (DateTime.Today - lateRegistration.LATE_TRANSFER_DATE_FROM.Value).TotalDays + 1;
           //     var lateRegistrationFee = lateRegistration.FINE_RATE.Value * (decimal)totalDays;
           //     if (lateRegistrationFee > lateRegistration.MAX_FINE)
           //     {
           //         lateRegistrationFee = lateRegistration.MAX_FINE.Value;
           //     }
           credits.Add(new payment()
             { name = _messageService.GetMessage("LATEFEE", language), amount = lateRegistrationFee });
           //    _totalPayment += lateRegistrationFee;
           //}
         }

         if (usedScholarship > 0)
         {
           //debits.Add(new payment(){name=cacheService.GetMessage("SCHOLARSHIP",language) + " " + _scholarship.scholarshipName,amount=_usedScholarship});
           debits.AddRange(scholarships.Where(s => s.used_amount > 0).Select(s => new payment()
           {
             code = s.scholarship_code,
             name = s.scholarship_name,
             type_name = "SCH",
             amount = s.used_amount.Value
           }));
         }

         if (usedAdvance > 0)
         {
           debits.Add(new payment()
           {
             name = _messageService.GetMessage("ADVANCE", language) + " " +
                    String.Join("/", advances.Select(a => a.advance_no).ToList()),
             amount = usedAdvance
           });
         }

         if (usedLoan > 0)
         {
           debits.Add(new payment()
             { name = _messageService.GetMessage("LOAN", language) + " " + loan.contract_no, amount = usedLoan });
         }

         //var payEnable = true;
         if (totalPayment == 0 && newAdvanceAmount > 0)
         {
           total.name = _messageService.GetMessage("ADVANCEDLEFT", language);
           total.amount = newAdvanceAmount;
           //payEnable = false;
         }
         else
         {
           // if (_totalPayment == 0)
           // {
           //     payEnable = false;
           // }
           total.name = _messageService.GetMessage("TOTALPAYMENT", language);
           total.amount = totalPayment;
         }

         return new register_summary()
         {
           currentCredit = totalCredits,
           totalCredit = totalCredits,
           // creditFee = totalCreditAmount, //totalLectureAmount + totalLabAmount,
           // academicFee = _totalFeeAmount + _totalExtraFeeAmount,
           // accidentInsuranceFee = 0,
           // fundAmount = usedScholarship,
           // loanAmount = _usedLoan,
           // advanceAmount = _usedAdvance,
           totalPaymentAmount = totalPayment,
           //loan = _loanDetail,
           //academics = _fees.ToList(),
           //fund = _scholarshipDetail,
           //subjects = _subjects,
           subjects = registerSubjects,
           debits = debits,
           credits = credits,
           total = total
         };
       }
     }

     private async Task<t_provision_register> GetProvisionRegister(register_token_detail registerTokenDetail,string registerTypeCode,bool forUpdate)
    {
      if (forUpdate)
      {
        var provisionRegister =await  _context.t_provision_register.FirstOrDefaultAsync(r =>
          r.student_uid == registerTokenDetail.student_uid &&
          r.academic_year_uid == registerTokenDetail.register_academic_year_uid &&
          r.academic_semester_uid == registerTokenDetail.register_academic_semester_uid &&
          r.register_type_code == registerTypeCode);
        return provisionRegister;
      }
      else
      {
        var provisionRegister =await  _context.t_provision_register.AsNoTracking().FirstOrDefaultAsync(r =>
          r.student_uid == registerTokenDetail.student_uid &&
          r.academic_year_uid == registerTokenDetail.register_academic_year_uid &&
          r.academic_semester_uid == registerTokenDetail.register_academic_semester_uid &&
          r.register_type_code == registerTypeCode);
        return provisionRegister;
      }

    }

    public async Task<List<t_provision_register_subject>> GetProvisionRegisterSubject(Guid provisionRegisterUid)
    {
      var provisionRegisterSubjects = await _context.t_provision_register_subject.AsNoTracking()
        .Where(r => r.provision_register_uid == provisionRegisterUid).ToListAsync();
      return provisionRegisterSubjects;
    }

    private async Task<List<T>> GetProvisionRegisterSubject<T>(Guid provisionRegisterUid)
    {
      var provisionRegisterSubjects = await _context.t_provision_register_subject.AsNoTracking()
        .Where(r => r.provision_register_uid == provisionRegisterUid).ProjectTo<t_provision_register_subject,T>().ToListAsync();
      return provisionRegisterSubjects;
    }
    public async Task<register_detail> PreRegisterCheck(string registerTypeCode,Guid registerTokenUid)
    {
      var registerTokenDetail = await GetRegisterToken(registerTypeCode, registerTokenUid);
      var messages = new List<string>();
      var preRegisterCalendar = await _context.t_academic_register_calendar.AsNoTracking().FirstOrDefaultAsync(c =>
        c.academic_register_calendar_uid == registerTokenDetail.academic_register_calendar_uid);
      //Get existing register
      var lastPaymentDate = preRegisterCalendar?.last_payment_date ?? DateTime.Today;
      var language = "TH";
      var lastPaymentDateText = "";
      if (language == "TH")
      {
        CultureInfo culture = new CultureInfo("th-TH");
        lastPaymentDateText = lastPaymentDate.ToString("dd MMM yyyy", culture);
      }
      else
      {
        CultureInfo culture = new CultureInfo("en-US");
        lastPaymentDateText = lastPaymentDate.ToString("dd MMM yyyy", culture);
      }
      var registerSubjects = new List<register_subject>();
      var provisionRegister =await  _context.t_provision_register.AsNoTracking().FirstOrDefaultAsync(r =>
        r.student_uid == registerTokenDetail.student_uid &&
        r.academic_year_uid == registerTokenDetail.register_academic_year_uid &&
        r.academic_semester_uid == registerTokenDetail.register_academic_semester_uid);
      var academicYearCode = "";
      var academicSemesterCode = "";
      if (provisionRegister != null)
      {
        academicYearCode = _context.t_academic_year.AsNoTracking()
          .FirstOrDefault(a => a.academic_year_uid == provisionRegister.academic_year_uid)?.academic_year_code;
        academicSemesterCode = _context.t_academic_semester.AsNoTracking()
          .FirstOrDefault(a => a.academic_semester_uid == provisionRegister.academic_semester_uid)?.academic_semester_code;

        if (provisionRegister.register_status_id == 2)
        {
          var provisionRegisterSubjects = await _context.t_provision_register_subject.AsNoTracking()
            .Where(r => r.provision_register_uid == provisionRegister.provision_register_uid).ToListAsync();
            //.ProjectTo<t_provision_register_subject,register_subject>().ToListAsync();
          registerSubjects = provisionRegisterSubjects.Select(p => new register_subject(p)).ToList();
           // (await _dapperDb.GetProvisionRegisterSubject(provisionRegister.provision_register_id.Value)).ToList();
          //registerSubjects = //provisionRegisterSubjects.Select(rs => new register_subject(rs)).ToList();

          var registerDetail = new register_detail()
          {
            academic_year_uid =  provisionRegister.academic_year_uid??Guid.Empty,
            academic_semester_uid = provisionRegister.academic_semester_uid??Guid.Empty,
            academic_year_code = academicYearCode,
            academic_semester_code = academicSemesterCode,
            min_credit = 0, //credit.minCredit.Value,
            max_credit = 0, //credit.maxCredit.Value,
            allow_register = false,
            message = "คุณละทะเบียนเรียบร้อยแล้ว", //_messageService.GetMessage(messages[0], language),
            register_subjects = registerSubjects,
            is_readonly = true,
            register_type_code = "PRE",
            total_credit = (int) registerSubjects.Sum(s => s.credit),
            last_payment_date = lastPaymentDateText,
            payment_url ="https://rsu.71dev.com/payment-api/api/payment/redirect/" + provisionRegister.bill_uid.ToString(),
            provision_register_uid = provisionRegister.provision_register_uid
          };
          var cost= await  GetEstimationCost(registerSubjects, language, registerTypeCode, registerTokenDetail);
          registerDetail.Update(cost);
          return registerDetail;
        }
      }


      var credit = new register_credit()
      {
        min_credit = preRegisterCalendar?.min_credit ?? 0,
        max_credit = preRegisterCalendar?.max_credit ?? 6

      };
      // var lastPaymentDate = registerCalendar.last_payment_date.HasValue
      //   ? registerCalendar.last_payment_date.Value
      //   : DateTime.Today;

      //var _condition=await dapperDB.GetEnrollmentCondition(studentCode,registerYear,registerSemester);

      var condition = new register_condition();

      var isEligibleToEnroll = true;
      var readOnly = true;

      if (provisionRegister?.register_type_code != null)
      {

          isEligibleToEnroll = true;
          readOnly = false;
          messages.Add("ELIGIBLE");
          // string lastPaymentDateText = "";
          var tProvisionRegisterSubjects = await _context.t_provision_register_subject.AsNoTracking()
            .Where(r => r.provision_register_uid == provisionRegister.provision_register_uid).ToListAsync();
          var provisionRegisterSubjects =  tProvisionRegisterSubjects.Select(p=>new register_subject(p)).ToList();
            //.ProjectTo<t_provision_register_subject,register_subject>().ToListAsync();
          _cacheService.AddRegisterSubjects(registerTokenDetail.student_uid,provisionRegisterSubjects);
          if (language == "TH")
          {
            CultureInfo culture = new CultureInfo("th-TH");
            lastPaymentDateText = lastPaymentDate.ToString("dd MMM yyyy", culture);
          }
          else
          {
            CultureInfo culture = new CultureInfo("en-US");
            lastPaymentDateText = lastPaymentDate.ToString("dd MMM yyyy", culture);
          }
          _cacheService.AddRegisterSubjects(registerTokenDetail.student_uid ,provisionRegisterSubjects);
          var registerDetail = new register_detail()
          {
            academic_year_uid =  provisionRegister.academic_year_uid??Guid.Empty,
            academic_semester_uid = provisionRegister.academic_semester_uid??Guid.Empty,
            academic_year_code = academicYearCode,
            academic_semester_code = academicSemesterCode,
            min_credit = credit.min_credit,
            max_credit = credit.max_credit,
            allow_register = isEligibleToEnroll,
            message = _messageService.GetMessage(messages[0], language),
            register_subjects = provisionRegisterSubjects,
            is_readonly = readOnly,
            register_type_code = "PRE",
            total_credit = (int) provisionRegisterSubjects.Sum(s => s.credit),
            last_payment_date = lastPaymentDateText,
            provision_register_uid = provisionRegister.provision_register_uid
          };
          if (provisionRegisterSubjects.Any())
          {
            var en=await GetEstimationCost(provisionRegisterSubjects,language,registerTypeCode,registerTokenDetail);

            //var en=await GetEstimationCost(registerSubjects, studentCode,academicYearId,academicSemesterId,educationTypeId,facultyId,majorId,package,academicYearEntryId,language,provisionRegisterTypeId,studentStatus);
            registerDetail.Update(en);
          }

         var cost= await  GetEstimationCost(registerSubjects, language, registerTypeCode, registerTokenDetail);
        registerDetail.Update(cost);

          return registerDetail;
        //}

        return null; //Temporary
      }
      else //New Register
      {

        isEligibleToEnroll = true;
        readOnly = false;
        // if (studentStatus != "N" && studentStatus != "P" && studentStatus != "I" && studentStatus != "1" &&
        //     studentStatus != "2" && studentStatus != "3")

        // Check Student Status Id
        // if (studentStatusId == 0)
        // {
        //   Console.WriteLine(studentCode + " unmatched status code");
        //   messages.Add("UNMATCHSTATUS");
        //   //_message = "MISSINGEVAL";
        //   isEligibleToEnroll = false;
        // }

        if (condition.missing_evaluation)
        {
          //Console.WriteLine(studentCode + " missing evaluation");
          messages.Add("MISSINGEVAL");
          //_message = "MISSINGEVAL";
          isEligibleToEnroll = false;
        }

        if (condition.has_dorm_debt)
        {

          //Console.WriteLine(studentCode + " has dorm debt");
          messages.Add("DORMDEBT");
          //_message = "DORMDEBT";
          isEligibleToEnroll = false;
        }

        if (condition.has_financial_debt)
        {

          //Console.WriteLine(studentCode + " has financial debt");
          messages.Add("FINDEBT");
          isEligibleToEnroll = false;
        }

        // if (!condition.hasAdvisor)
        // {
        //
        //   Console.WriteLine(studentCode + " missing advisor");
        //   messages.Add("MISSINGADV");
        //   isEligibleToEnroll = false;
        // }

        if (messages.Count == 0)
        {
          messages.Add("ELIGIBLE");
          isEligibleToEnroll = true;
        }

        var _msg = "";
        if (!isEligibleToEnroll)
        {
          var msgs = messages.Select(m => _messageService.GetMessage(m, language)).ToList();
          _msg = string.Join("|", msgs);
        }

        var registerDetail = new register_detail()
        {
          academic_year_uid =  registerTokenDetail.register_academic_year_uid,
          academic_semester_uid = registerTokenDetail.register_academic_semester_uid,
          min_credit = credit.min_credit,
          max_credit = credit.max_credit,
          allow_register = isEligibleToEnroll,
          message = _msg, //cacheService.GetMessage(_message,language),
          register_subjects = registerSubjects,
          is_readonly = false,
          total_credit = (int) registerSubjects.Where(s => s.register_subject_status_id!=1).Sum(s => s.credit),
          register_type_code = "PRE",
          last_payment_date = lastPaymentDateText
          //messageEn = "Student eligible to enroll",
          //messageTh = "นักศึกษาสามารถลงทะเบียนได้"
        };
        return registerDetail;
        //}

      }


    }


    public async Task<bool> PaymentUpdate(payment_update paymentUpdate)
    {
      //payment_update.payment_request_id = null;
      //await _context.app_payment_request.AddAsync(paymentRequest);
      //await _context.SaveChangesAsync();
      string billNo = paymentUpdate.ref2.Substring(5, 7); //paymentRequest.ref2.Length - 6);
      //110006400244011221
      // if (paymentRequest.payment_method_code.ToLower() == "CREDITCARD")
      // {
      //
      //   //var invoiceNo = paid.ref2.Substring(0,paid.ref2.Length - 6);
      //   invoiceNo = paymentRequest.ref2.Substring(0,paymentRequest.ref2.Length - 6);
      // }
      Console.WriteLine("Paid invoice no:" + billNo);

      var provisionRegister = await _context.t_provision_register.FirstOrDefaultAsync(a => a.bill_no == billNo);
      if (provisionRegister != null)
      {
        provisionRegister.is_paid = true;
        var finalRegister = new t_final_register(provisionRegister);
        var provisionRegisterSubjects = await _context.t_provision_register_subject
          .Where(p => p.provision_register_uid == provisionRegister.provision_register_uid).ToListAsync();
        finalRegister.final_register_subjects.AddRange(provisionRegisterSubjects.Select(p=>new t_final_register_subject(p)));
        await _context.AddAsync(finalRegister);
        await _context.SaveChangesAsync();
      }




      return true;
    }
    public async Task<payment_token> GetWebPaymentToken()
        {
          var iamHttpClient = _httpClientFactory.CreateClient("open_id");
          //Console.WriteLine("Client ID:" + _openIdSetting.client_id);
          var dict = new Dictionary<string, string>
          {
            { "grant_type", "client_credentials" },
            { "scope", string.Join(" ", _openIdSetting.scopes) },
            { "client_id", _openIdSetting.client_id },
            { "client_secret", _openIdSetting.client_secret }
          };
          var req = new HttpRequestMessage(HttpMethod.Post, _openIdSetting.authority + "/connect/token")
            { Content = new FormUrlEncodedContent(dict) };
          var iamResponse = await iamHttpClient.SendAsync(req);
          if (iamResponse.StatusCode == HttpStatusCode.OK)
          {
            var iamResult = await iamResponse.Content.ReadAsByteArrayAsync();
            var openIdToken = JsonSerializer.Deserialize<open_id_token>(iamResult);
            return new payment_token() { token = openIdToken.access_token };

          }

          return new payment_token();
        }
    public async Task<counter_register_detail> CounterRegisterCheck(string registerTypeCode,Guid registerTokenUid)
    {
      var registerTokenDetail = await GetRegisterToken(registerTypeCode, registerTokenUid);
      var messages = new List<string>();
      var preRegisterCalendar = await _context.t_academic_register_calendar.AsNoTracking().FirstOrDefaultAsync(c =>
        c.academic_register_calendar_uid == registerTokenDetail.academic_register_calendar_uid);
      //Get existing register
      var lastPaymentDate = preRegisterCalendar.last_payment_date ?? DateTime.Today;
      var language = "TH";
      var lastPaymentDateText = "";
      if (language == "TH")
      {
        CultureInfo culture = new CultureInfo("th-TH");
        lastPaymentDateText = lastPaymentDate.ToString("dd MMM yyyy", culture);
      }
      else
      {
        CultureInfo culture = new CultureInfo("en-US");
        lastPaymentDateText = lastPaymentDate.ToString("dd MMM yyyy", culture);
      }
      var registerSubjects = new List<register_subject>();
      //var provisionRegister = await _dapperDb.GetProvisionRegister(studentCode, academicYearId, academicSemesterId,provisionRegisterTypeId);
      var provisionRegister =await  _context.t_provision_register.AsNoTracking().FirstOrDefaultAsync(r =>
        r.student_uid == registerTokenDetail.student_uid &&
        r.academic_year_uid == registerTokenDetail.register_academic_year_uid &&
        r.academic_semester_uid == registerTokenDetail.register_academic_semester_uid);
      //No register
      if (provisionRegister == null)
      {
        var fees = await GetRegisterFee(registerTokenDetail);
        var advancePayments = await GetRegisterAdvancePayment(registerTokenDetail);
        var scholarships = await GetScholarship(registerTokenDetail);
        var registerDetail = new counter_register_detail()
        {
          academic_year_uid =  Guid.Empty,
          academic_semester_uid = Guid.Empty,
          fees = fees.Select(f=>new counter_register_fee(f)).ToList(),
          advance_payments = advancePayments.Select(a=>new counter_register_advance_payment(a)).ToList(),
          scholarships = scholarships.Select(s=>new counter_register_scholarship(s)).ToList(),
          min_credit = 0,
          max_credit = 6,
          // allow_register = isEligibleToEnroll,
          // message = _messageService.GetMessage(messages[0], language),
          register_subjects = new List<register_subject>(),
          is_package = registerTokenDetail.is_package
          // is_readonly = readOnly,
          // register_type_code = "PRE",
          // total_credit = (int) provisionRegisterSubjects.Sum(s => s.credit),
          // last_payment_date = lastPaymentDateText
        };
        if (registerTokenDetail.is_package ?? false)
        {
          registerDetail.package_amount = registerTokenDetail.package_amount;
        }
        return registerDetail;
      }
      else
      {
        var provisionRegisterSubjects = (await _context.t_provision_register_subject.AsNoTracking()
          .Where(r => r.provision_register_uid == provisionRegister.provision_register_uid).ToListAsync()).Select(p=>new register_subject(p)).ToList();

        if (provisionRegister.register_status_id == 1)
        {
          var fees = await GetRegisterFee(registerTokenDetail);
          var advancePayments = await GetRegisterAdvancePayment(registerTokenDetail);
          var scholarships = await GetScholarship(registerTokenDetail);
          var loan = await GetStudentLoan(registerTokenDetail);

          _cacheService.AddRegisterSubjects(registerTokenDetail.student_uid ,provisionRegisterSubjects);
          var registerDetail = new counter_register_detail()
          {
            academic_year_uid =  provisionRegister.academic_year_uid??Guid.Empty,
            academic_semester_uid = provisionRegister.academic_semester_uid??Guid.Empty,
            fees = fees.Select(f=>new counter_register_fee(f)).ToList(),
            advance_payments = advancePayments.Select(a=>new counter_register_advance_payment(a)).ToList(),
            loan = loan!=null?new counter_register_loan(loan):null,
            //scholarships =
            // min_credit = credit.min_credit,
            // max_credit = credit.max_credit,
            // allow_register = isEligibleToEnroll,
            // message = _messageService.GetMessage(messages[0], language),
            register_subjects = provisionRegisterSubjects,
            // is_readonly = readOnly,
            // register_type_code = "PRE",
            // total_credit = (int) provisionRegisterSubjects.Sum(s => s.credit),
            // last_payment_date = lastPaymentDateText
          };
          if (provisionRegisterSubjects.Any())
          {
            var en=await GetCounterRegisterCost(registerDetail,registerTypeCode,registerTokenDetail);

            //var en=await GetEstimationCost(registerSubjects, studentCode,academicYearId,academicSemesterId,educationTypeId,facultyId,majorId,package,academicYearEntryId,language,provisionRegisterTypeId,studentStatus);
            registerDetail.Update(en);
          }


          return registerDetail;
        }
        else
        {

        }
      }

      // if (provisionRegister != null)
      // {
      //   if (provisionRegister.register_status_id == 2)
      //   {
      //     var provisionRegisterSubjects = await _context.t_provision_register_subject.AsNoTracking()
      //       .Where(r => r.provision_register_uid == provisionRegister.provision_register_uid).ToListAsync();
      //       //.ProjectTo<t_provision_register_subject,register_subject>().ToListAsync();
      //     registerSubjects = provisionRegisterSubjects.Select(p => new register_subject(p)).ToList();
      //      // (await _dapperDb.GetProvisionRegisterSubject(provisionRegister.provision_register_id.Value)).ToList();
      //     //registerSubjects = //provisionRegisterSubjects.Select(rs => new register_subject(rs)).ToList();
      //
      //     var registerDetail = new register_detail()
      //     {
      //       academic_year_uid =  provisionRegister.academic_year_uid??Guid.Empty,
      //       academic_semester_uid = provisionRegister.semester_uid??Guid.Empty,
      //       min_credit = 0, //credit.minCredit.Value,
      //       max_credit = 0, //credit.maxCredit.Value,
      //       allow_register = false,
      //       message = "คุณละทะเบียนเรียบร้อยแล้ว", //_messageService.GetMessage(messages[0], language),
      //       register_subjects = registerSubjects,
      //       is_readonly = true,
      //       register_type_code = "PRE",
      //       total_credit = (int) registerSubjects.Sum(s => s.credit),
      //       last_payment_date = lastPaymentDateText,
      //       payment_url ="https://rsu.71dev.com/payment-api/api/payment/redirect/" + provisionRegister.bill_uid.ToString()
      //     };
      //     return registerDetail;
      //   }
      //  }


      var credit = new register_credit()
      {
        min_credit = preRegisterCalendar.min_credit ?? 0,
        max_credit = preRegisterCalendar.max_credit ?? 6

      };
      // var lastPaymentDate = registerCalendar.last_payment_date.HasValue
      //   ? registerCalendar.last_payment_date.Value
      //   : DateTime.Today;

      //var _condition=await dapperDB.GetEnrollmentCondition(studentCode,registerYear,registerSemester);

      var condition = new register_condition();

      var isEligibleToEnroll = true;
      var readOnly = true;

      if (provisionRegister?.register_type_code != null)
      {
          isEligibleToEnroll = true;
          readOnly = false;
          messages.Add("ELIGIBLE");
          // string lastPaymentDateText = "";
          var provisionRegisterSubjects = (await _context.t_provision_register_subject.AsNoTracking()
            .Where(r => r.provision_register_uid == provisionRegister.provision_register_uid).ToListAsync()).Select(p=>new register_subject(p)).ToList();
          var fees = await GetRegisterFee(registerTokenDetail);
          var advancePayments = await GetRegisterAdvancePayment(registerTokenDetail);
          var scholarships = await GetScholarship(registerTokenDetail);
          var loan = await GetStudentLoan(registerTokenDetail);


          _cacheService.AddRegisterSubjects(registerTokenDetail.student_uid ,provisionRegisterSubjects);
          var registerDetail = new counter_register_detail()
          {
            academic_year_uid =  provisionRegister.academic_year_uid??Guid.Empty,
            academic_semester_uid = provisionRegister.academic_semester_uid??Guid.Empty,
            fees = fees.Select(f=>new counter_register_fee(f)).ToList(),
            advance_payments = advancePayments.Select(a=>new counter_register_advance_payment(a)).ToList(),
            //scholarships =
            // min_credit = credit.min_credit,
            // max_credit = credit.max_credit,
            // allow_register = isEligibleToEnroll,
            // message = _messageService.GetMessage(messages[0], language),
            register_subjects = provisionRegisterSubjects,
            is_package = registerTokenDetail.is_package
            // is_readonly = readOnly,
            // register_type_code = "PRE",
            // total_credit = (int) provisionRegisterSubjects.Sum(s => s.credit),
            // last_payment_date = lastPaymentDateText
          };
          if (registerTokenDetail.is_package ?? false)
          {
            registerDetail.package_amount = registerTokenDetail.package_amount;
          }
          if (provisionRegisterSubjects.Any())
          {
            var en=await GetCounterRegisterCost(registerDetail,registerTypeCode,registerTokenDetail);

            //var en=await GetEstimationCost(registerSubjects, studentCode,academicYearId,academicSemesterId,educationTypeId,facultyId,majorId,package,academicYearEntryId,language,provisionRegisterTypeId,studentStatus);
            registerDetail.Update(en);
          }


          return registerDetail;
        //}

        return null; //Temporary
      }
      // else //New Register
      // {
      //
      //   isEligibleToEnroll = true;
      //   readOnly = false;
      //   // if (studentStatus != "N" && studentStatus != "P" && studentStatus != "I" && studentStatus != "1" &&
      //   //     studentStatus != "2" && studentStatus != "3")
      //
      //   // Check Student Status Id
      //   // if (studentStatusId == 0)
      //   // {
      //   //   Console.WriteLine(studentCode + " unmatched status code");
      //   //   messages.Add("UNMATCHSTATUS");
      //   //   //_message = "MISSINGEVAL";
      //   //   isEligibleToEnroll = false;
      //   // }
      //
      //   if (condition.missing_evaluation)
      //   {
      //     //Console.WriteLine(studentCode + " missing evaluation");
      //     messages.Add("MISSINGEVAL");
      //     //_message = "MISSINGEVAL";
      //     isEligibleToEnroll = false;
      //   }
      //
      //   if (condition.has_dorm_debt)
      //   {
      //
      //     //Console.WriteLine(studentCode + " has dorm debt");
      //     messages.Add("DORMDEBT");
      //     //_message = "DORMDEBT";
      //     isEligibleToEnroll = false;
      //   }
      //
      //   if (condition.has_financial_debt)
      //   {
      //
      //     //Console.WriteLine(studentCode + " has financial debt");
      //     messages.Add("FINDEBT");
      //     isEligibleToEnroll = false;
      //   }
      //
      //   // if (!condition.hasAdvisor)
      //   // {
      //   //
      //   //   Console.WriteLine(studentCode + " missing advisor");
      //   //   messages.Add("MISSINGADV");
      //   //   isEligibleToEnroll = false;
      //   // }
      //
      //   if (messages.Count == 0)
      //   {
      //     messages.Add("ELIGIBLE");
      //     isEligibleToEnroll = true;
      //   }
      //
      //   var _msg = "";
      //   if (!isEligibleToEnroll)
      //   {
      //     var msgs = messages.Select(m => _messageService.GetMessage(m, language)).ToList();
      //     _msg = string.Join("|", msgs);
      //   }
      //
      //   var registerDetail = new register_detail()
      //   {
      //     academic_year_uid =  registerTokenDetail.register_academic_year_uid,
      //     academic_semester_uid = registerTokenDetail.register_academic_semester_uid,
      //     min_credit = credit.min_credit,
      //     max_credit = credit.max_credit,
      //     allow_register = isEligibleToEnroll,
      //     message = _msg, //cacheService.GetMessage(_message,language),
      //     register_subjects = registerSubjects,
      //     is_readonly = false,
      //     total_credit = (int) registerSubjects.Where(s => s.register_subject_status_id!=1).Sum(s => s.credit),
      //     register_type_code = "PRE",
      //     last_payment_date = lastPaymentDateText
      //     //messageEn = "Student eligible to enroll",
      //     //messageTh = "นักศึกษาสามารถลงทะเบียนได้"
      //   };
      //   return registerDetail;
      //   //}
      //
      // }
      return new counter_register_detail();


    }
    async Task<register_summary> GetCounterRegisterCost(counter_register_detail studentRegisterDetail,
       string registerTypeCode, register_token_detail registerTokenDetail)
     {
       if (!studentRegisterDetail.register_subjects.Any())
       {

           Console.WriteLine("GetEstimationCost: No subjects found in cache");
           return null;
         }

       var confirmRegisterSubjects = studentRegisterDetail.register_subjects.Where(c => c.register_subject_status_id == 1).ToList();
       var totalCredits = confirmRegisterSubjects.Sum(s => s.credit) ?? 0;
       var totalSubjects = confirmRegisterSubjects.Count();
       decimal totalLectureAmount = confirmRegisterSubjects.Sum(r => r.total_lecture_amount) ?? 0;
       decimal totalLabAmount = confirmRegisterSubjects.Sum(r => r.total_lab_amount) ?? 0;
       ;
       decimal creditAmount = 0;
       decimal totalCreditAmount = 0;
       //var _subjects = new List<MDESubject>();
       //var registerItems = new List<MDERegisterItem>();
       //var studentEnrollment = cacheService.GetStudentEnrollment(studentCode);

       // foreach (var registerSubject in registerSubjects.Where(registerSubject => registerSubject.status!="WAITING"))
       // {
       //   registerItems.Add(new MDERegisterItem(studentCode,registerYear,registerSemester,_selectedSubject));
       //   _subjects.Add(new MDESubject(_selectedSubject,language));
       //   //_totalCredit += (int)_selectedSubject.credit;
       //   totalLectureAmount += _selectedSubject.lectureAmount * _selectedSubject.credit;
       //   totalLabAmount += _selectedSubject.labAmount;//  * _selectedSubject.credit;
       // }
       decimal lateRegistrationFee = 0;
       if (registerTypeCode is "LATE")
       {
         // Get late register fee
         // var lateRegistration = await dapperDB.GetLateRegistrationFee(registerYear,registerSemester,educationTypeCode,studentStatus);
         //
         // if (lateRegistration!=null)
         // {
         //     Console.WriteLine(lateRegistration.LATE_TRANSFER_DATE_FROM.Value.ToString());
         //     var totalDays = (DateTime.Today - lateRegistration.LATE_TRANSFER_DATE_FROM.Value).TotalDays + 1;
         //     lateRegistrationFee = lateRegistration.FINE_RATE.Value * (decimal)totalDays;
         //     if (lateRegistrationFee > lateRegistration.MAX_FINE)
         //     {
         //         lateRegistrationFee = lateRegistration.MAX_FINE.Value;
         //     }
         // }
       }


       decimal usedScholarship = 0;
       decimal feeAmount = 0;
       decimal advanceAmount = 0;
       // MDScholarship _scholarship = await dapperDB.GetScholarship(registerYear,registerSemester,studentCode);
       // MDLoan _loan = await dapperDB.GetLoan(studentCode,int.Parse(registerYear));
       var loan = await GetStudentLoan(registerTokenDetail);

       var scholarships = studentRegisterDetail.scholarships;


       var advances = studentRegisterDetail.advance_payments;
       if (advances.Any())
       {
         advanceAmount = advances.Where(a=>a.is_use==true) .Sum(a => a.left_amount) ?? 0;
       }


       var fees = studentRegisterDetail.fees;//.Select(f=>new re)
        if (fees.Any())
       {
         feeAmount = fees.Sum(f => f.fee_amount) ?? 0;
       }

       // MDEScholarship _scholarshipDetail = null;
       var package = "N";
       if (package.ToUpper()=="Y")
       {
       //
       //     MDPackage _package = await dapperDB.GetPackage(academicYearEntry.Substring(2,2),registerYear,registerSemester,faculty,major,educationTypeCode);
       //
       //     if (_package!=null)
       //     {
       //         var _packageAmount = _package.packageAmount;
       //         if (_scholarship!=null)
       //         {
       //             _usedScholarship = _package.packageAmount * (_scholarship.packagePercent/100);
       //             _scholarshipDetail = new MDEScholarship(_scholarship);
       //         }
       //         //var _loan = await dapperDB.GetLoan(studentCode,2562);
       //     //Total Loan
       //         decimal _usedLoan = 0;
       //         decimal _loanAmount = 0;
       //         MDELoan _loanDetail = null;
       //         if (_loan!=null)
       //         {
       //             _loanDetail = new MDELoan() {
       //                 contractNo = _loan.contractNo
       //             };
       //             if (registerSemester=="1")
       //             {
       //                 _loanAmount = _loan.requestSemester1 - _loan.useSemester1;
       //             }
       //             else if (registerSemester=="2")
       //             {
       //                 _loanAmount = _loan.requestSemester2 - _loan.useSemester2;
       //             }
       //             else if (registerSemester=="3")
       //             {
       //                 _loanAmount = _loan.requestSemester3 - _loan.useSemester3;
       //             }
       //             _loanDetail.amount = _loanAmount;
       //         }
       //         decimal _usedAdvance = 0;
       //     if (_advanceAmount > 0)
       //     {
       //         if (lateRegistrationFee >0)
       //         {
       //             if (_advanceAmount <= lateRegistrationFee)
       //             {
       //
       //                 _usedAdvance = _advanceAmount;
       //                 //lateRegistrationFee = lateRegistrationFee - _advanceAmount;
       //                 _advanceAmount = 0;
       //             }
       //             else
       //             {
       //                 _usedAdvance = lateRegistrationFee;
       //                 _advanceAmount = _advanceAmount - lateRegistrationFee;
       //                 //lateRegistrationFee = 0;
       //
       //             }
       //
       //         }
       //         if (_packageAmount > 0)
       //         {
       //             if (_advanceAmount <= _packageAmount)
       //             {
       //
       //                 _usedAdvance += _advanceAmount;
       //                 _packageAmount = _packageAmount - _advanceAmount;
       //                 _advanceAmount = 0;
       //             }
       //             else
       //             {
       //                 _usedAdvance += _packageAmount;
       //                 _advanceAmount = _advanceAmount - _packageAmount;
       //                 _packageAmount = 0;
       //
       //             }
       //         }
       //
       //     }
       //
       //     if (_packageAmount > 0)
       //     {
       //         if (_loanAmount > 0)
       //         {
       //             if (_loanAmount <= _packageAmount)
       //             {
       //                 _usedLoan = _loanAmount;
       //                 _packageAmount = _packageAmount - _loanAmount;
       //                 _loanAmount = 0;
       //
       //
       //             }
       //             else
       //             {
       //                 _loanAmount = _packageAmount;
       //                 _packageAmount = 0;
       //                 _usedLoan = _loanAmount - _packageAmount;
       //
       //
       //             }
       //         }
       //     }
       //
       //
       //
       //     //Console.WriteLine("ADV:" + _advanceAmount.ToString());
       //     decimal _totalPayment = _package.packageAmount + lateRegistrationFee - _usedScholarship -_usedLoan - _usedAdvance;
       //
       //     List<MDEAdvance> advanceUseds = new List<MDEAdvance>();
       //     //List<MDEAdvanceUseWeb> advanceUseWebs = new List<MDEAdvanceUseWeb>();
       //     foreach(var _advance in _advances)
       //     {
       //         var _advanceUsed = new MDEAdvance(){advanceNo=_advance.advanceNo,amount=_advance.advanceAmount};
       //         advanceUseds.Add(_advanceUsed);
       //         //advanceUseWebs.Add(new MDEAdvanceUseWeb(studentCode,registerYear,registerSemester,_advanceUsed));
       //     }
       //
       //     MDELoan loanUsed = null;
       //     if (_usedLoan>0)
       //     {
       //         loanUsed = new MDELoan(){contractNo=_loan.contractNo,amount=_usedLoan};
       //     }
       //     //var _newAdvanceNo =await dapperDB.GetAdvanceNo(registerYear,registerSemester);
       //     var _newAdvanceAmount = _advanceAmount-_usedAdvance;
       //     //var _newAdvance = new EN_ADVANCE_PAYMENT(studentCode,_newAdvanceNo,_advanceAmount-_usedAdvance,0,"",0,"Y","","MOBILE","");
       //     // string registerFlag = "1";
       //     // if (type=="LATE")
       //     // {
       //     //     registerFlag = "2";
       //     // }
       //     // if (type=="ADDDROP")
       //     // {
       //     //     registerFlag = "3";
       //     // }
       //     List<payment> credits = new List<payment>();
       //     List<payment> debits = new List<payment>();
       //     payment total = new payment();
       //
       //
       //     if (_package.packageAmount > 0)
       //     {
       //         credits.Add(new payment(){name=cacheService.GetMessage("PACKAGE",language),amount=_package.packageAmount});
       //     }
       //     if (type=="LATE")
       //     {
       //         // var lateRegistration = await dapperDB.GetLateRegistrationFee(registerYear,registerSemester,educationTypeCode,studentStatus);
       //
       //         // if (lateRegistration!=null)
       //         // {
       //         //     Console.WriteLine(lateRegistration.LATE_TRANSFER_DATE_FROM.Value.ToString());
       //         //     var totalDays = (DateTime.Today - lateRegistration.LATE_TRANSFER_DATE_FROM.Value).TotalDays + 1;
       //         //     var lateRegistrationFee = lateRegistration.FINE_RATE.Value * (decimal)totalDays;
       //         //     if (lateRegistrationFee > lateRegistration.MAX_FINE)
       //         //     {
       //         //         lateRegistrationFee = lateRegistration.MAX_FINE.Value;
       //         //     }
       //         if (lateRegistrationFee > 0)
       //         {
       //             credits.Add(new payment(){name=cacheService.GetMessage("LATEFEE",language),amount=lateRegistrationFee});
       //             //_totalPayment += lateRegistrationFee;
       //         }
       //         //}
       //     }
       //     if (_usedScholarship > 0)
       //     {
       //         debits.Add(new payment(){name=cacheService.GetMessage("SCHOLARSHIP",language)  + " " + _scholarship.scholarshipName,amount=_usedScholarship});
       //     }
       //     if (_usedAdvance > 0)
       //     {
       //         debits.Add(new payment(){name=cacheService.GetMessage("ADVANCE",language) + " " + String.Join("/",_advances.Select(a=>a.advanceNo).ToList()),amount=_usedAdvance});
       //     }
       //     if (_usedLoan > 0)
       //     {
       //         debits.Add(new payment(){name=cacheService.GetMessage("LOAN",language) + " " + _loan.contractNo,amount=_usedLoan});
       //     }
       //
       //     //var payEnable = true;
       //     if (_totalPayment ==0 && _newAdvanceAmount >0)
       //     {
       //         total.name =cacheService.GetMessage("ADVANCEDLEFT",language);
       //         total.amount = _newAdvanceAmount;
       //         //payEnable = false;
       //     }
       //     else
       //     {
       //         if (_totalPayment ==0)
       //         {
       //             //payEnable = false;
       //         }
       //         total.name = cacheService.GetMessage("TOTALPAYMENT",language);
       //         total.amount = _totalPayment;
       //     }
       //     return new e_enrollment_summary()
       //     {
       //         currentCredit = _totalCredit,
       //         totalCredit= _totalCredit,
       //         creditFee = 0, //totalLectureAmount + totalLabAmount,
       //         academicFee = 0,
       //         accidentInsuranceFee = 0,
       //         fundAmount = _usedScholarship,
       //         loanAmount = _usedLoan,
       //         advanceAmount = _usedAdvance,
       //         package = _package.packageAmount,
       //         totalPaymentAmount = _totalPayment,
       //         loan = _loanDetail,
       //         //academics = new List<MDFee>(),
       //         fund = _scholarshipDetail,
       //         selectedSubjects = _selectedSubjects,
       //         // subjects = _subjects,
       //         // payInNo = webRegister.PAY_IN_NO,
       //         credits = credits,
       //         debits = debits,
       //         total = total
       //
       //     };
       //     }
       //     else
       //     {
       //         throw new Exception("Package not found");
       //     }
       //
       //
       //
       return new register_summary();
       }
       else //Credit
       {
         creditAmount = (totalLectureAmount + totalLabAmount);
         totalCreditAmount = totalLectureAmount + totalLabAmount;
         if (scholarships != null)
         {
           if (scholarships.Any())
           {
             foreach (var scholarship in scholarships)
             {
               scholarship.used_amount = 0;
               var creditScholarship = scholarship.expense.FirstOrDefault(e => e.expenseId == 2);
               if (creditScholarship != null)
               {
                 if (creditScholarship.amountType == "money")
                 {
                   usedScholarship += creditScholarship.amount;
                   creditAmount -= usedScholarship;
                   scholarship.used_amount += creditScholarship.amount;
                 }
                 else
                 {
                   var used = (creditAmount * (creditScholarship.amount / 100));
                   usedScholarship += used;
                   creditAmount -= usedScholarship;
                   scholarship.used_amount += used;
                 }
               }


             }
           }
         }


         decimal _extraFeeAmount = 0;
         //decimal _totalFeeAmount = 0;
         decimal _totalFeeAmount = feeAmount;
         decimal _totalExtraFeeAmount = 0;

         // List<MDERegisterFeeWeb> registerFeeWebs = new List<MDERegisterFeeWeb>();
         // if (fees.Count() >0)
         // {
         //     int seq = 1;
         //     foreach(var _fee in _fees)
         //     {
         //         registerFeeWebs.Add(new MDERegisterFeeWeb(studentCode,registerYear,registerSemester,seq,_fee));
         //         seq += 1;
         //         if (_fee.feeCode=="003" || _fee.feeCode=="006")
         //         {
         //             _totalExtraFeeAmount += _fee.amount;
         //             if (_scholarshipFees!=null)
         //             {
         //                 var _scholarshipFee = _scholarshipFees.FirstOrDefault(sf=>sf.feeCode==_fee.feeCode);
         //                 if (_scholarshipFee!=null)
         //                 {
         //                     _usedScholarship += _fee.amount * (_scholarshipFee.percentDiscount/100);
         //                     _extraFeeAmount += _fee.amount - (_fee.amount * (_scholarshipFee.percentDiscount/100)); //* (100 - _scholarshipFee.percentDiscount);
         //                 }
         //                 else
         //                 {
         //                     _extraFeeAmount += _fee.amount;
         //                 }
         //             }
         //             else
         //             {
         //                 _extraFeeAmount += _fee.amount;
         //             }
         //         }
         //         else
         //         {
         //             _totalFeeAmount += _fee.amount;
         //             // if (_scholarshipFees!=null)
         //             // {
         //             //     var _scholarshipFee = _scholarshipFees.FirstOrDefault(sf=>sf.feeCode==_fee.feeCode);
         //             //     if (_scholarshipFee!=null)
         //             //     {
         //             //         // _usedScholarship += _fee.amount * (_scholarshipFee.percentDiscount/100);
         //             //         // _feeAmount += _fee.amount - (_fee.amount * (_scholarshipFee.percentDiscount/100)); //* (100 - _scholarshipFee.percentDiscount);
         //             //         _usedScholarship += _fee.amount * (_scholarshipFee.percentDiscount/100);
         //             //         _feeAmount += _fee.amount - (_fee.amount * (_scholarshipFee.percentDiscount/100));
         //             //     }
         //             //     else
         //             //     {
         //             //         _feeAmount += _fee.amount;
         //             //     }
         //             // }
         //             // else
         //             // {
         //                 _feeAmount += _fee.amount;
         //             // }
         //         }
         //
         //
         //     }
         //
         // }
         decimal usedLoan = 0;
         decimal _loanAmount = 0;
         if (loan != null)
         {
           _loanAmount = (loan.loan_amount ?? 0) - (loan.use_amount ?? 0);
         }

         // _feeAmount -= _scholarshipFeeDiscount;

         decimal usedAdvance = 0;
         if (advanceAmount > 0)
         {
           if (lateRegistrationFee > 0)
           {
             if (advanceAmount <= lateRegistrationFee)
             {

               usedAdvance = advanceAmount;
               //lateRegistrationFee = lateRegistrationFee - advanceAmount;
               advanceAmount = 0;
             }
             else
             {
               usedAdvance = lateRegistrationFee;
               advanceAmount = advanceAmount - lateRegistrationFee;
               //lateRegistrationFee = 0;

             }

           }

           if (creditAmount > 0)
           {
             if (advanceAmount <= creditAmount)
             {

               usedAdvance += advanceAmount;
               creditAmount = creditAmount - advanceAmount;
               advanceAmount = 0;
             }
             else
             {
               usedAdvance += creditAmount;
               advanceAmount = advanceAmount - creditAmount;
               creditAmount = 0;

             }
           }

           if (advanceAmount > 0 && feeAmount > 0)
           {
             if (advanceAmount <= feeAmount)
             {
               usedAdvance += advanceAmount;
               feeAmount = feeAmount - advanceAmount;
               advanceAmount = 0;
             }
             else
             {
               usedAdvance += feeAmount;
               advanceAmount = advanceAmount - feeAmount;
               feeAmount = 0;
             }
           }

           if (advanceAmount > 0 && _extraFeeAmount > 0)
           {
             if (advanceAmount <= _extraFeeAmount)
             {
               usedAdvance += advanceAmount;
               _extraFeeAmount = _extraFeeAmount - advanceAmount;
               advanceAmount = 0;
             }
             else
             {
               usedAdvance += _extraFeeAmount;
               advanceAmount = advanceAmount - _extraFeeAmount;
               _extraFeeAmount = 0;
             }
           }
         }

         // Console.WriteLine("Credit Amount:" + _creditAmount.ToString());
         // Console.WriteLine("Advanced Amount:" + _advanceAmount.ToString());
         // Console.WriteLine("Used Advanced Amount:" + _usedAdvance.ToString());
         if (creditAmount > 0)
         {
           if (_loanAmount > 0)
           {
             if (_loanAmount <= creditAmount)
             {
               usedLoan = _loanAmount;
               creditAmount = creditAmount - _loanAmount;
               _loanAmount = 0;


             }
             else
             {
               _loanAmount = _loanAmount - creditAmount;
               usedLoan = creditAmount;
               creditAmount = 0;
             }
           }
         }

         if (feeAmount > 0)
         {
           if (_loanAmount > 0)
           {
             if (_loanAmount <= feeAmount)
             {
               usedLoan = usedLoan + _loanAmount;
               feeAmount = feeAmount - _loanAmount;
               _loanAmount = 0;


             }
             else
             {
               _loanAmount = _loanAmount - feeAmount;
               usedLoan = usedLoan + feeAmount;
               feeAmount = 0;



             }
           }
         }

         decimal totalPayment = totalCreditAmount + _totalFeeAmount + _totalExtraFeeAmount + lateRegistrationFee -
                                usedScholarship - usedLoan - usedAdvance;

         var newAdvanceAmount = advanceAmount - usedAdvance;
         List<payment> credits = new List<payment>();
         List<payment> debits = new List<payment>();
         payment total = new payment();
         if (totalCreditAmount > 0)
         {
           credits.Add(new payment()
           {
             type_name = "CREDITFEE",
             name = "ค่าหน่วยกิต", //_messageService.GetMessage("CREDITFEE",language)
             amount = totalCreditAmount
           });
         }

         if (_totalFeeAmount + _totalExtraFeeAmount > 0)
         {
           credits.AddRange(fees.Select(f => new payment() { name = f.fee_name_th, amount = f.fee_amount ?? 0 }));
          }

         if (registerTypeCode is "LATE")
         {
           // var lateRegistration = await dapperDB.GetLateRegistrationFee(registerYear,registerSemester,educationTypeCode,studentStatus);
           // if (lateRegistration!=null)
           // {

           //     var totalDays = (DateTime.Today - lateRegistration.LATE_TRANSFER_DATE_FROM.Value).TotalDays + 1;
           //     var lateRegistrationFee = lateRegistration.FINE_RATE.Value * (decimal)totalDays;
           //     if (lateRegistrationFee > lateRegistration.MAX_FINE)
           //     {
           //         lateRegistrationFee = lateRegistration.MAX_FINE.Value;
           //     }
           credits.Add(new payment()
             { name = _messageService.GetMessage("LATEFEE", "TH"), amount = lateRegistrationFee });
           //    _totalPayment += lateRegistrationFee;
           //}
         }

         if (usedScholarship > 0)
         {
           //debits.Add(new payment(){name=cacheService.GetMessage("SCHOLARSHIP",language) + " " + _scholarship.scholarshipName,amount=_usedScholarship});
           debits.AddRange(scholarships.Where(s => s.used_amount > 0).Select(s => new payment()
           {
             code = s.scholarship_code,
             name = s.scholarship_name,
             type_name = "SCH",
             amount = s.used_amount.Value
           }));
         }

         if (usedAdvance > 0)
         {
           debits.Add(new payment()
           {
             name = _messageService.GetMessage("ADVANCE", "TH") + " " +
                    String.Join("/", advances.Select(a => a.advance_no).ToList()),
             amount = usedAdvance
           });
         }

         if (usedLoan > 0)
         {
           debits.Add(new payment()
             { name = _messageService.GetMessage("LOAN", "TH") + " " + loan.contract_no, amount = usedLoan });
         }

         //var payEnable = true;
         if (totalPayment == 0 && newAdvanceAmount > 0)
         {
           total.name = _messageService.GetMessage("ADVANCEDLEFT", "TH");
           total.amount = newAdvanceAmount;
           //payEnable = false;
         }
         else
         {
           // if (_totalPayment == 0)
           // {
           //     payEnable = false;
           // }
           total.name = _messageService.GetMessage("TOTALPAYMENT", "TH");
           total.amount = totalPayment;
         }

         return new register_summary()
         {
           currentCredit = totalCredits,
           totalCredit = totalCredits,
           // creditFee = totalCreditAmount, //totalLectureAmount + totalLabAmount,
           // academicFee = _totalFeeAmount + _totalExtraFeeAmount,
           // accidentInsuranceFee = 0,
           // fundAmount = usedScholarship,
           // loanAmount = _usedLoan,
           // advanceAmount = _usedAdvance,
           totalPaymentAmount = totalPayment,
           //loan = _loanDetail,
           //academics = _fees.ToList(),
           //fund = _scholarshipDetail,
           //subjects = _subjects,
           subjects = studentRegisterDetail.register_subjects,
           debits = debits,
           credits = credits,
           total = total
         };
       }
     }
    public async Task<counter_register_detail> CounterAddSubject(counter_register_detail currentStudentRegisterDetail,string registerTypeCode,Guid registerTokenUid,register_subject registerSubject)
    {
      // Flag 0=>Save 1=>Waiting
      // register_subject_status_id
      // 0 => Exist
      // 1 => Add
      // 2 => Drop
      // register_subject_flag_id
      // 0 => Ok
      // 1 => Waiting
      // 3 => Approve
      // 4 => Overlapped Study Time
      // 5 => Overlapped Exam Time
      registerSubject.register_subject_status_id =1;
      registerSubject.register_subject_flag_id = 0;


      var registerTokenDetail = await GetRegisterToken(registerTypeCode, registerTokenUid);


      var currentRegisterSubjects = _cacheService.GetRegisterSubjects(registerTokenDetail.student_uid);// cacheService.GetStudentSelectedSubjects(studentCode);
      var provisionRegister = await GetProvisionRegister(registerTokenDetail.student_uid, registerTokenDetail.register_academic_year_uid,
        registerTokenDetail.register_academic_semester_uid, registerTypeCode);;

      if (!currentRegisterSubjects.Any())
      {
        if (provisionRegister != null)
        {
          var provisionRegisterSubjects =
            await GetProvisionRegisterSubject(provisionRegister.provision_register_uid.Value);
          var subjects = provisionRegisterSubjects.Select(s=>new register_subject(s)).ToList();
          if (provisionRegisterSubjects.Any())
          {
            _cacheService.AddRegisterSubjects(registerTokenDetail.student_uid, subjects);
            currentRegisterSubjects = subjects;
          }
        }
      }
      if (currentRegisterSubjects.Any(s => s.year_subject_uid == registerSubject.year_subject_uid))
      {

        throw new DataException("EXISTING_SUBJECT",registerSubject.year_subject_code);
      }

      if (currentRegisterSubjects != null)
      {

        // if (currentRegisterSubjects.Any(s=>s.subject_code==registerSubject.subject_code))
        // {
        //     throw new Exception("EXISTING_SUBJECT");
        // }
        //var currentSelectedSubjects = studentEnrollments;

        //bool overlappedStudyTime = false;
        //string overlappedStudySubjectCode = "";
        var studentStudyTimes = _cacheService.GetStudentStudyTime(registerTokenDetail.student_uid);
        var subjectStudyTimes = _cacheService.GetSubjectStudyTime(registerSubject);
        var overlappedStudyTime = CheckOverlappedStudyTime(studentStudyTimes, subjectStudyTimes);
        if (overlappedStudyTime!="")
        {
          registerSubject.register_subject_flag_id = 4;
          //throw new DataException("OVERLAPPED_STUDY_TIME", overlappedStudyTime);
        }

        var studentExamTimes = _cacheService.GetStudentExamTime(registerTokenDetail.student_uid);
        var subjectExamTimes = _cacheService.GetSubjectExamTime(registerSubject);
        var overlappedExamTime = CheckOverlappedExamTime(studentExamTimes, subjectExamTimes);
        if (overlappedExamTime!="")
        {
          registerSubject.register_subject_flag_id = 5;
          //throw new DataException("OVERLAPPED_EXAM_TIME", overlappedExamTime);
        }

        //bool overlappedExamTime = false;
        //string overlappedExamSubjectCode = "";

        // var allExamTimes =
        //   GetExamTimes(currentRegisterSubjects); //.GetStudentExamTime(studentEnrollments);
        // var subjectExamTimes = GetSubjectExamTimes(registerSubject);
        // if (subjectExamTimes.Any())
        // {
        //   foreach (var allExamTime in allExamTimes)
        //   {
        //     foreach (var subjectExamTime in subjectExamTimes)
        //     {
        //       if ((subjectExamTime.from_time >= allExamTime.from_time &&
        //            subjectExamTime.from_time <= allExamTime.to_time) ||
        //           (subjectExamTime.to_time >= allExamTime.from_time &&
        //            subjectExamTime.to_time <= allExamTime.to_time))
        //       {
        //         overlappedExamTime = true;
        //         overlappedExamSubjectCode = allExamTime.subject_code;
        //         break;
        //       }
        //     }
        //   }
        // }
        //
        // if (overlappedExamTime)
        // {
        //   //throw new DataException("OVERLAPPED_EXAM_TIME", overlappedExamSubjectCode);
        // }
        // if (registerSubject.is_topic_subject.HasValue && registerSubject.is_topic_subject.Value)
        // {
        //   registerSubject.credit = registerSubject.lecture_section.topic_credit;
        // }


      }

      // else
      // {
      //     if (selectedSubject.isSpecialTopic=="Y")
      //     {
      //         selectedSubject.credit = selectedSubject.selectedTopic.topicCredit;
      //     }
      //     //diEnrollmentSubject.TryAdd(studentCode,new List<MDESelectedSubject>(){selectedSubject});
      //
      // }





      //
      // if (registerSubject.lecture_section != null)
      // {
      //   if (registerSubject.status != "WAITING" && registerSubject.lecture_section.section_id.HasValue)
      //   {
      //     await _dapperDb.AcquireReservedSeat(registerSubject.lecture_section.section_id.Value);
      //     //await dapperDB.ReserveSeat(registerYear,registerSemester,selectedSubject.subjectSeq,selectedSubject.subjectCode,"C",selectedSubject.selectedLectureGroup.sectionId);
      //   }
      //
      // }
      //
      // if (registerSubject.lab_section != null)
      // {
      //   if (registerSubject.status != "WAITING" && registerSubject.lab_section.section_id.HasValue)
      //   {
      //     await _dapperDb.AcquireReservedSeat(registerSubject.lab_section.section_id.Value);
      //     //await dapperDB.ReserveSeat(registerYear,registerSemester,selectedSubject.subjectSeq,selectedSubject.subjectCode,"B",selectedSubject.selectedLabGroup.sectionId);
      //   }
      //
      // }




      if (registerTypeCode is "PRE" or "LATE")
      {
        var registerSubjects = _cacheService.GetRegisterSubjects(registerTokenDetail.student_uid);

        // else
        // {
        //
        // }
        // if (registerSubjects != null)
        // {
        //   registerSubjects.Add(registerSubject);
        // }
        // else
        // {
        //   registerSubjects = _cacheService.AddNewRegisterSubject(registerTokenDetail.student_uid, registerSubject);
        //
        // }

        if (provisionRegister == null)
        {
          provisionRegister = new t_provision_register(registerTokenDetail.student_uid, registerTokenDetail.register_academic_year_uid, registerTokenDetail.register_academic_semester_uid,
            registerSubjects.Count, registerSubjects.Sum(s => s.credit) ?? 0, registerTypeCode , 1);
          //Add provisionRegister
          await _context.t_provision_register.AddAsync(provisionRegister);
          await _context.SaveChangesAsync();



        }
        // if (registerSubject.register_subject_status_id==2)
        // {
        //   //Add waiting list
        // }
        // else
        // {
          if (registerSubject.lecture_section != null)
          {
            var yearSubjectSection = await _context.t_year_subject_section.FirstOrDefaultAsync(y =>
              y.year_subject_section_uid == registerSubject.lecture_section.year_subject_section_uid);
            yearSubjectSection.reserved += 1;
          }
          if (registerSubject.lab_section != null)
          {
            var yearSubjectSection = await _context.t_year_subject_section.FirstOrDefaultAsync(y =>
              y.year_subject_section_uid == registerSubject.lab_section.year_subject_section_uid);
            yearSubjectSection.reserved += 1;
          }
          var newProvisionRegisterSubject =
            new t_provision_register_subject(registerSubject)
            {
              provision_register_uid = provisionRegister.provision_register_uid
            };
          await _context.t_provision_register_subject.AddAsync(newProvisionRegisterSubject);
          await _context.SaveChangesAsync();

          _cacheService.AddRegisterSubject(registerTokenDetail.student_uid,registerSubject);
          if (registerSubject.register_subject_flag_id == 1)
          {
            var yearSubjectWaitingList = new t_year_subject_waiting_list()
            {
              student_uid = registerTokenDetail.student_uid,
              academic_year_uid = registerTokenDetail.register_academic_year_uid,
              academic_semester_uid = registerTokenDetail.register_academic_semester_uid,
              year_subject_uid = registerSubject.year_subject_uid,
              lecture_year_subject_section_uid = registerSubject.lecture_section?.year_subject_section_uid,
              lab_year_subject_section_uid = registerSubject.lab_section?.year_subject_section_uid
            };
            await _context.t_year_subject_waiting_list.AddAsync(yearSubjectWaitingList);
            await _context.SaveChangesAsync();
          }
          //tProvisionRegister.provision_register_subjects = tProvisionRegisterSubject;
          //Add provisionRegisterSubject
          //await _dapperDb.AddProvisionRegisterSubject(provisionRegisterSubject);
          //provisionRegister.pro

       // }



        //await _context.t_provision_register.AddAsync(provisionRegister);
        var studentRegisterDetail = new counter_register_detail()
        {
          //academic_year_id = academicYearId,
          //academic_semester_id = academicSemesterId,
          // min_credit = minCredit,
          // max_credit = maxCredit,
          // allow_register = isEligibleToEnroll,
          // message = _messageService.GetMessage(messages[0], language),
          fees = currentStudentRegisterDetail.fees,
          scholarships = currentStudentRegisterDetail.scholarships,
          loan = currentStudentRegisterDetail.loan,
          advance_payments = currentStudentRegisterDetail.advance_payments,
          register_subjects = registerSubjects,
          // is_readonly = readOnly,
          //provision_register_type_id = provisionRegisterTypeId,
          total_credit = (int) registerSubjects.Sum(s => s.credit),
          //last_payment_date = lastPaymentDateText
        };
        var en=await GetCounterRegisterCost(studentRegisterDetail,registerTypeCode,registerTokenDetail);

        studentRegisterDetail.Update(en);
        return studentRegisterDetail;
        // return await GetRegisterDetail(registerSubjects, "TH", registerTypeCode,
        //   registerTokenDetail);



      }
      else //ADDDROP
      {

        return null;
      }



      //if (diSubjectSection.ContainsKey(selectedSubject.subjectCode + selectedSubject))

    }
    public async Task<counter_register_detail> CounterRemoveSubject(counter_register_detail currentStudentRegisterDetail,string registerTypeCode,Guid registerTokenUid,Guid yearSubjectUid)
    {

      var registerTokenDetail = await GetRegisterToken(registerTypeCode, registerTokenUid);
      var currentRegisterSubjects = _cacheService.GetRegisterSubjects(registerTokenDetail.student_uid);// cacheService.GetStudentSelectedSubjects(studentCode);
      var removeRegisterSubject = currentRegisterSubjects.FirstOrDefault(s => s.year_subject_uid == yearSubjectUid);
      if (removeRegisterSubject == null)
      {
        throw new Exception("SUBJECT_NOT_FOUND");
      }
      if (registerTypeCode is "PRE" or "LATE")
      {
        var provisionRegister = await GetProvisionRegister(registerTokenDetail.student_uid, registerTokenDetail.register_academic_year_uid,
          registerTokenDetail.register_academic_semester_uid, registerTypeCode);
        var provisionRegisterSubject = await _context.t_provision_register_subject.FirstOrDefaultAsync(p =>
          p.provision_register_uid == provisionRegister.provision_register_uid && p.year_subject_uid == yearSubjectUid);
        _context.t_provision_register_subject.Remove(provisionRegisterSubject);
        await _context.SaveChangesAsync();
        var remainRegisterSubjects=_cacheService.RemoveRegisterSubject(registerTokenDetail.student_uid, removeRegisterSubject);




        //await _context.t_provision_register.AddAsync(provisionRegister);
        var studentRegisterDetail = new counter_register_detail()
        {
          //academic_year_id = academicYearId,
          //academic_semester_id = academicSemesterId,
          // min_credit = minCredit,
          // max_credit = maxCredit,
          // allow_register = isEligibleToEnroll,
          // message = _messageService.GetMessage(messages[0], language),
          fees = currentStudentRegisterDetail.fees,
          scholarships = currentStudentRegisterDetail.scholarships,
          loan = currentStudentRegisterDetail.loan,
          advance_payments = currentStudentRegisterDetail.advance_payments,
          register_subjects = remainRegisterSubjects,
          // is_readonly = readOnly,
          //provision_register_type_id = provisionRegisterTypeId,
          total_credit = (int) remainRegisterSubjects.Sum(s => s.credit),
          //last_payment_date = lastPaymentDateText
        };
        var en=await GetCounterRegisterCost(studentRegisterDetail,registerTypeCode,registerTokenDetail);

        studentRegisterDetail.Update(en);
        return studentRegisterDetail;
        // return await GetRegisterDetail(remainRegisterSubjects,  "TH", registerTypeCode,
        //   registerTokenDetail);



      }
      else //ADDDROP
      {
        return null;
      }



      //if (diSubjectSection.ContainsKey(selectedSubject.subjectCode + selectedSubject))

    }
    public async Task<register_summary> PreRegisterConfirm(string registerTypeCode, Guid registerTokenUid,
       string language)
     {

       var registerTokenDetail = await GetRegisterToken(registerTypeCode, registerTokenUid);

       // var registerSubjects = _cacheService.GetRegisterSubject(studentCode);
       // if (!registerSubjects.Any())
       // {
       //     Console.WriteLine("No subject found in cache");
       //     throw new DataException("NO_SUBJECT_FOUND");
       // }

       var provisionRegister = await GetProvisionRegister(registerTokenDetail, registerTypeCode,true);
       // if (provisionRegister!=null && provisionRegister.provision_register_uid != null)
       List<t_provision_register_subject> provisionRegisterSubjects = null;
       if (provisionRegister is { provision_register_uid: { } })
       {
         provisionRegisterSubjects =
           await GetProvisionRegisterSubject(provisionRegister.provision_register_uid.Value);
       }

       if (provisionRegisterSubjects == null)
       {
         throw new DataException("UNDER_MIN_CREDIT");
       }
       if (provisionRegisterSubjects.Sum(r => r.credit) < (registerTokenDetail.min_credit??0))
       {
         throw new DataException("UNDER_MIN_CREDIT");
       }

       //var _pendingRegister = await GetPendingRegister(studentCode,registerYear,registerSemester,registerTypeFlag);
       //Total credit exclude waiting
       var totalCredit = provisionRegisterSubjects.Where(s => s.register_subject_status_id == 1).Sum(s => s.credit);
       //Total subject exclude waiting
       int totalSubjects = provisionRegisterSubjects.Count(s => s.register_subject_status_id == 1);
       decimal totalLectureAmount = 0;
       decimal totalLabAmount = 0;
       decimal creditAmount = 0;
       decimal totalCreditAmount = 0;
       //List<MDESubject> _subjects = new List<MDESubject>();
       //List<EN_WEB_REGISTER_DETAIL> registerItems = new List<EN_WEB_REGISTER_DETAIL>();
       //var studentEnrollment = _cacheService.GetStudentEnrollment(studentCode);
       //Subjects exclude waiting
       provisionRegisterSubjects = provisionRegisterSubjects.Where(s => s.register_subject_status_id == 1).ToList();
       //Remove waiting list
       //await _dapperDb.RemoveWaitingList(studentCode,registerYear,registerSemester,_pendingRegister.TX_RUNNING.Value);
       foreach (var provisionRegisterSubject in provisionRegisterSubjects)
       {
         //registerItems.Add(new EN_WEB_REGISTER_DETAIL(studentCode,registerYear,registerSemester,_selectedSubject));
         //_subjects.Add(new MDESubject(_selectedSubject,language));
         //_totalCredit += (int)_selectedSubject.credit;
         totalLectureAmount += provisionRegisterSubject.total_lecture_amount ?? 0;
         totalLabAmount += provisionRegisterSubject.total_lab_amount ?? 0; //  * _selectedSubject.credit;

       }

       decimal lateRegistrationFee = 0;

       // if (provisionRegisterTypeId==2)
       // {
       //     var lateRegistration = await dapperDB.GetLateRegistrationFee(registerYear,registerSemester,educationTypeCode,studentStatus);
       //
       //     if (lateRegistration!=null)
       //     {
       //         Console.WriteLine(lateRegistration.LATE_TRANSFER_DATE_FROM.Value.ToString());
       //         var totalDays = (DateTime.Today - lateRegistration.LATE_TRANSFER_DATE_FROM.Value).TotalDays + 1;
       //         lateRegistrationFee = lateRegistration.FINE_RATE.Value * (decimal)totalDays;
       //         if (lateRegistrationFee > lateRegistration.MAX_FINE)
       //         {
       //             lateRegistrationFee = lateRegistration.MAX_FINE.Value;
       //         }
       //         // credits.Add(new payment(){name=cacheService.GetMessage("LATEFEE",language),amount=lateRegistrationFee});
       //         // _totalPayment += lateRegistrationFee;
       //     }
       // }

       decimal usedScholarship = 0;
       decimal feeAmount = 0;
       decimal advanceAmount = 0;
       // MDScholarship _scholarship = await dapperDB.GetScholarship(registerYear,registerSemester,studentCode);
       // MDLoan _loan = await dapperDB.GetLoan(studentCode,int.Parse(registerYear));
       var loan = await GetStudentLoan(registerTokenDetail);
       var scholarships = await GetScholarship(registerTokenDetail);
       var advances =
         await GetRegisterAdvancePayment(registerTokenDetail);
       if (advances.Any())
       {
         advanceAmount = advances.Sum(a => a.left_amount) ?? 0;
       }

       var fees = await GetRegisterFee(registerTokenDetail);

       // if (fees.Any())
       // {
       //   feeAmount = fees.Sum(f => f.fee_amount) ?? 0;
       // }

       var package = "N";
       if (package.ToUpper() == "Y")
       {

         var packageAmount = registerTokenDetail.package_amount ?? 0;
         if (scholarships != null && scholarships.Any())
         {
           foreach (var scholarship in scholarships)
           {
             scholarship.used_amount = 0;
             var creditScholarship = scholarship.expense.FirstOrDefault(e => e.expenseId == 2);
             if (creditScholarship == null) continue;
             if (creditScholarship.amountType == "money")
             {
               usedScholarship += creditScholarship.amount;
               packageAmount -= usedScholarship;
               scholarship.used_amount += creditScholarship.amount;
             }
             else
             {
               var used = (packageAmount * (creditScholarship.amount / 100));
               usedScholarship += used;
               packageAmount -= usedScholarship;
               scholarship.used_amount += used;
             }
           }
         }
         decimal _extraFeeAmount = 0;
         decimal _totalFeeAmount = feeAmount;
         decimal _totalExtraFeeAmount = 0;

         // List<MDERegisterFeeWeb> registerFeeWebs = new List<MDERegisterFeeWeb>();
         // if (fees.Count() >0)
         // {
         //     int seq = 1;
         //     foreach(var _fee in _fees)
         //     {
         //         registerFeeWebs.Add(new MDERegisterFeeWeb(studentCode,registerYear,registerSemester,seq,_fee));
         //         seq += 1;
         //         if (_fee.feeCode=="003" || _fee.feeCode=="006")
         //         {
         //             _totalExtraFeeAmount += _fee.amount;
         //             if (_scholarshipFees!=null)
         //             {
         //                 var _scholarshipFee = _scholarshipFees.FirstOrDefault(sf=>sf.feeCode==_fee.feeCode);
         //                 if (_scholarshipFee!=null)
         //                 {
         //                     _usedScholarship += _fee.amount * (_scholarshipFee.percentDiscount/100);
         //                     _extraFeeAmount += _fee.amount - (_fee.amount * (_scholarshipFee.percentDiscount/100)); //* (100 - _scholarshipFee.percentDiscount);
         //                 }
         //                 else
         //                 {
         //                     _extraFeeAmount += _fee.amount;
         //                 }
         //             }
         //             else
         //             {
         //                 _extraFeeAmount += _fee.amount;
         //             }
         //         }
         //         else
         //         {
         //             _totalFeeAmount += _fee.amount;
         //             // if (_scholarshipFees!=null)
         //             // {
         //             //     var _scholarshipFee = _scholarshipFees.FirstOrDefault(sf=>sf.feeCode==_fee.feeCode);
         //             //     if (_scholarshipFee!=null)
         //             //     {
         //             //         // _usedScholarship += _fee.amount * (_scholarshipFee.percentDiscount/100);
         //             //         // _feeAmount += _fee.amount - (_fee.amount * (_scholarshipFee.percentDiscount/100)); //* (100 - _scholarshipFee.percentDiscount);
         //             //         _usedScholarship += _fee.amount * (_scholarshipFee.percentDiscount/100);
         //             //         _feeAmount += _fee.amount - (_fee.amount * (_scholarshipFee.percentDiscount/100));
         //             //     }
         //             //     else
         //             //     {
         //             //         _feeAmount += _fee.amount;
         //             //     }
         //             // }
         //             // else
         //             // {
         //                 _feeAmount += _fee.amount;
         //             // }
         //         }
         //
         //
         //     }
         //
         // }
         decimal usedLoan = 0;
         decimal _loanAmount = 0;
         if (loan != null)
         {
           _loanAmount = (loan.loan_amount ?? 0) - (loan.use_amount ?? 0);
         }

         // _feeAmount -= _scholarshipFeeDiscount;

         decimal usedAdvance = 0;
         if (advanceAmount > 0)
         {
           if (lateRegistrationFee > 0)
           {
             if (advanceAmount <= lateRegistrationFee)
             {

               usedAdvance = advanceAmount;
               //lateRegistrationFee = lateRegistrationFee - advanceAmount;
               advanceAmount = 0;
             }
             else
             {
               usedAdvance = lateRegistrationFee;
               advanceAmount = advanceAmount - lateRegistrationFee;
               //lateRegistrationFee = 0;

             }

           }

           if (packageAmount > 0)
           {
             if (advanceAmount <= packageAmount)
             {

               usedAdvance += advanceAmount;
               packageAmount = packageAmount - advanceAmount;
               advanceAmount = 0;
             }
             else
             {
               usedAdvance += packageAmount;
               advanceAmount = advanceAmount - packageAmount;
               packageAmount = 0;

             }
           }

           if (advanceAmount > 0 && feeAmount > 0)
           {
             if (advanceAmount <= feeAmount)
             {
               usedAdvance += advanceAmount;
               feeAmount = feeAmount - advanceAmount;
               advanceAmount = 0;
             }
             else
             {
               usedAdvance += feeAmount;
               advanceAmount = advanceAmount - feeAmount;
               feeAmount = 0;
             }
           }

           if (advanceAmount > 0 && _extraFeeAmount > 0)
           {
             if (advanceAmount <= _extraFeeAmount)
             {
               usedAdvance += advanceAmount;
               _extraFeeAmount = _extraFeeAmount - advanceAmount;
               advanceAmount = 0;
             }
             else
             {
               usedAdvance += _extraFeeAmount;
               advanceAmount = advanceAmount - _extraFeeAmount;
               _extraFeeAmount = 0;
             }
           }
         }

         // Console.WriteLine("Credit Amount:" + _creditAmount.ToString());
         // Console.WriteLine("Advanced Amount:" + _advanceAmount.ToString());
         // Console.WriteLine("Used Advanced Amount:" + _usedAdvance.ToString());
         if (packageAmount > 0)
         {
           if (_loanAmount > 0)
           {
             if (_loanAmount <= packageAmount)
             {
               usedLoan = _loanAmount;
               packageAmount = packageAmount - _loanAmount;
               _loanAmount = 0;


             }
             else
             {
               _loanAmount = _loanAmount - packageAmount;
               usedLoan = packageAmount;
               packageAmount = 0;
             }
           }
         }

         // if (feeAmount > 0)
         // {
         //   if (_loanAmount > 0)
         //   {
         //     if (_loanAmount <= feeAmount)
         //     {
         //       usedLoan = usedLoan + _loanAmount;
         //       feeAmount = feeAmount - _loanAmount;
         //       _loanAmount = 0;
         //
         //
         //     }
         //     else
         //     {
         //       _loanAmount = _loanAmount - feeAmount;
         //       usedLoan = usedLoan + feeAmount;
         //       feeAmount = 0;
         //
         //
         //
         //     }
         //   }
         // }

         decimal totalPayment = packageAmount + _totalExtraFeeAmount + lateRegistrationFee -
                                usedScholarship - usedLoan - usedAdvance;

         var newAdvanceAmount = advanceAmount - usedAdvance;
         List<payment> credits = new List<payment>();
         List<payment> debits = new List<payment>();
         payment total = new payment();
         // if (totalCreditAmount > 0)
         // {
         credits.Add(new payment()
         {
           type_name = "PACKAGE",
           name = "ค่าเหมาจ่าย", //_messageService.GetMessage("CREDITFEE",language)
           amount = registerTokenDetail.package_amount ?? 0
         });
         // }

         // if (_totalFeeAmount + _totalExtraFeeAmount > 0)
         // {
         //   credits.AddRange(fees.Select(f => new payment() { name = f.fee_name_th, amount = f.fee_amount ?? 0 }));
         //  }

         if (registerTypeCode is "LATE")
         {
           // var lateRegistration = await dapperDB.GetLateRegistrationFee(registerYear,registerSemester,educationTypeCode,studentStatus);
           // if (lateRegistration!=null)
           // {

           //     var totalDays = (DateTime.Today - lateRegistration.LATE_TRANSFER_DATE_FROM.Value).TotalDays + 1;
           //     var lateRegistrationFee = lateRegistration.FINE_RATE.Value * (decimal)totalDays;
           //     if (lateRegistrationFee > lateRegistration.MAX_FINE)
           //     {
           //         lateRegistrationFee = lateRegistration.MAX_FINE.Value;
           //     }
           credits.Add(new payment()
             { name = _messageService.GetMessage("LATEFEE", language), amount = lateRegistrationFee });
           //    _totalPayment += lateRegistrationFee;
           //}
         }

         if (usedScholarship > 0)
         {
           //debits.Add(new payment(){name=cacheService.GetMessage("SCHOLARSHIP",language) + " " + _scholarship.scholarshipName,amount=_usedScholarship});
           debits.AddRange(scholarships.Where(s => s.used_amount > 0).Select(s => new payment()
           {
             code = s.scholarship_code,
             name = s.scholarship_name,
             type_name = "SCH",
             amount = s.used_amount.Value
           }));
         }

         if (usedAdvance > 0)
         {
           debits.Add(new payment()
           {
             name = _messageService.GetMessage("ADVANCE", language) + " " +
                    String.Join("/", advances.Select(a => a.advance_no).ToList()),
             amount = usedAdvance
           });
         }

         if (usedLoan > 0)
         {
           debits.Add(new payment()
             { name = _messageService.GetMessage("LOAN", language) + " " + loan.contract_no, amount = usedLoan });
         }

         //var payEnable = true;
         if (totalPayment == 0 && newAdvanceAmount > 0)
         {
           total.name = _messageService.GetMessage("ADVANCEDLEFT", language);
           total.amount = newAdvanceAmount;
           //payEnable = false;
         }
         else
         {
           // if (_totalPayment == 0)
           // {
           //     payEnable = false;
           // }
           total.name = _messageService.GetMessage("TOTALPAYMENT", language);
           total.amount = totalPayment;
         }
         //await _context.t_provision_register_fee.AddRangeAsync(provisionRegisterFees);
         provisionRegister.register_status_id = 2;
         provisionRegister.payment_amount = totalPayment;
         provisionRegister.package_amount = packageAmount;
         provisionRegister.advanced_amount = usedAdvance;
         provisionRegister.scholarship_amount = usedScholarship;
         var registerSummary= new register_summary()
         {
           currentCredit = totalCredit.Value,
           totalCredit = totalCredit.Value,
           // creditFee = totalCreditAmount, //totalLectureAmount + totalLabAmount,
           // academicFee = _totalFeeAmount + _totalExtraFeeAmount,
           // accidentInsuranceFee = 0,
           // fundAmount = usedScholarship,
           // loanAmount = _usedLoan,
           // advanceAmount = _usedAdvance,
           totalPaymentAmount = totalPayment,
           //loan = _loanDetail,
           //academics = _fees.ToList(),
           //fund = _scholarshipDetail,
           //subjects = _subjects,
           subjects = provisionRegisterSubjects.Select(s => new register_subject(s)).ToList(),
           debits = debits,
           credits = credits,
           total = total
         };
         await CreateRegisterBill(registerTokenDetail.student_uid, registerSummary, provisionRegister.provision_register_uid.Value);

         return registerSummary;


       }
       else
       {

         creditAmount = totalLectureAmount + totalLabAmount;
         totalCreditAmount = totalLectureAmount + totalLabAmount;

         var provisionRegisterFees = new List<t_provision_register_fee>();
         if (fees.Any())
         {
           int seq = 1;
           foreach (var fee in fees)
           {
             //registerFeeWebs.Add(new MDERegisterFeeWeb(studentCode,registerYear,registerSemester,seq,_fee));
             seq += 1;
             provisionRegisterFees.Add(new t_provision_register_fee(fee)
               { provision_register_uid = provisionRegister.provision_register_uid });
             feeAmount += (fee.fee_amount ?? 0);
             // totalFeeAmount +=(fee.fee_amount ?? 0);



           }

         }

         if (scholarships != null && scholarships.Any())
         {
           foreach (var scholarship in scholarships)
           {
             scholarship.used_amount = 0;
             var creditScholarship = scholarship.expense.FirstOrDefault(e => e.expenseId == 2);
             if (creditScholarship != null)
             {
               if (creditScholarship.amountType == "money")
               {
                 usedScholarship += creditScholarship.amount;
                 creditAmount -= usedScholarship;
                 scholarship.used_amount += creditScholarship.amount;
               }
               else
               {
                 var used = (creditAmount * (creditScholarship.amount / 100));
                 usedScholarship += used;
                 creditAmount -= usedScholarship;
                 scholarship.used_amount += used;
               }
             }


           }
         }


         decimal _extraFeeAmount = 0;
         //decimal _totalFeeAmount = 0;
         decimal _totalFeeAmount = feeAmount;
         decimal _totalExtraFeeAmount = 0;

         // List<MDERegisterFeeWeb> registerFeeWebs = new List<MDERegisterFeeWeb>();
         // if (fees.Count() >0)
         // {
         //     int seq = 1;
         //     foreach(var _fee in _fees)
         //     {
         //         registerFeeWebs.Add(new MDERegisterFeeWeb(studentCode,registerYear,registerSemester,seq,_fee));
         //         seq += 1;
         //         if (_fee.feeCode=="003" || _fee.feeCode=="006")
         //         {
         //             _totalExtraFeeAmount += _fee.amount;
         //             if (_scholarshipFees!=null)
         //             {
         //                 var _scholarshipFee = _scholarshipFees.FirstOrDefault(sf=>sf.feeCode==_fee.feeCode);
         //                 if (_scholarshipFee!=null)
         //                 {
         //                     _usedScholarship += _fee.amount * (_scholarshipFee.percentDiscount/100);
         //                     _extraFeeAmount += _fee.amount - (_fee.amount * (_scholarshipFee.percentDiscount/100)); //* (100 - _scholarshipFee.percentDiscount);
         //                 }
         //                 else
         //                 {
         //                     _extraFeeAmount += _fee.amount;
         //                 }
         //             }
         //             else
         //             {
         //                 _extraFeeAmount += _fee.amount;
         //             }
         //         }
         //         else
         //         {
         //             _totalFeeAmount += _fee.amount;
         //             // if (_scholarshipFees!=null)
         //             // {
         //             //     var _scholarshipFee = _scholarshipFees.FirstOrDefault(sf=>sf.feeCode==_fee.feeCode);
         //             //     if (_scholarshipFee!=null)
         //             //     {
         //             //         // _usedScholarship += _fee.amount * (_scholarshipFee.percentDiscount/100);
         //             //         // _feeAmount += _fee.amount - (_fee.amount * (_scholarshipFee.percentDiscount/100)); //* (100 - _scholarshipFee.percentDiscount);
         //             //         _usedScholarship += _fee.amount * (_scholarshipFee.percentDiscount/100);
         //             //         _feeAmount += _fee.amount - (_fee.amount * (_scholarshipFee.percentDiscount/100));
         //             //     }
         //             //     else
         //             //     {
         //             //         _feeAmount += _fee.amount;
         //             //     }
         //             // }
         //             // else
         //             // {
         //                 _feeAmount += _fee.amount;
         //             // }
         //         }
         //
         //
         //     }
         //
         // }
         decimal usedLoan = 0;
         decimal _loanAmount = 0;
         if (loan != null)
         {
           _loanAmount = (loan.loan_amount ?? 0) - (loan.use_amount ?? 0);
         }

         // _feeAmount -= _scholarshipFeeDiscount;

         decimal usedAdvance = 0;
         if (advanceAmount > 0)
         {
           if (lateRegistrationFee > 0)
           {
             if (advanceAmount <= lateRegistrationFee)
             {

               usedAdvance = advanceAmount;
               //lateRegistrationFee = lateRegistrationFee - advanceAmount;
               advanceAmount = 0;
             }
             else
             {
               usedAdvance = lateRegistrationFee;
               advanceAmount = advanceAmount - lateRegistrationFee;
               //lateRegistrationFee = 0;

             }

           }

           if (creditAmount > 0)
           {
             if (advanceAmount <= creditAmount)
             {

               usedAdvance += advanceAmount;
               creditAmount = creditAmount - advanceAmount;
               advanceAmount = 0;
             }
             else
             {
               usedAdvance += creditAmount;
               advanceAmount = advanceAmount - creditAmount;
               creditAmount = 0;

             }
           }

           if (advanceAmount > 0 && feeAmount > 0)
           {
             if (advanceAmount <= feeAmount)
             {
               usedAdvance += advanceAmount;
               feeAmount = feeAmount - advanceAmount;
               advanceAmount = 0;
             }
             else
             {
               usedAdvance += feeAmount;
               advanceAmount = advanceAmount - feeAmount;
               feeAmount = 0;
             }
           }

           if (advanceAmount > 0 && _extraFeeAmount > 0)
           {
             if (advanceAmount <= _extraFeeAmount)
             {
               usedAdvance += advanceAmount;
               _extraFeeAmount = _extraFeeAmount - advanceAmount;
               advanceAmount = 0;
             }
             else
             {
               usedAdvance += _extraFeeAmount;
               advanceAmount = advanceAmount - _extraFeeAmount;
               _extraFeeAmount = 0;
             }
           }
         }

         // Console.WriteLine("Credit Amount:" + _creditAmount.ToString());
         // Console.WriteLine("Advanced Amount:" + _advanceAmount.ToString());
         // Console.WriteLine("Used Advanced Amount:" + _usedAdvance.ToString());
         if (creditAmount > 0)
         {
           if (_loanAmount > 0)
           {
             if (_loanAmount <= creditAmount)
             {
               usedLoan = _loanAmount;
               creditAmount = creditAmount - _loanAmount;
               _loanAmount = 0;


             }
             else
             {
               _loanAmount = _loanAmount - creditAmount;
               usedLoan = creditAmount;
               creditAmount = 0;
             }
           }
         }

         if (feeAmount > 0)
         {
           if (_loanAmount > 0)
           {
             if (_loanAmount <= feeAmount)
             {
               usedLoan = usedLoan + _loanAmount;
               feeAmount = feeAmount - _loanAmount;
               _loanAmount = 0;


             }
             else
             {
               _loanAmount = _loanAmount - feeAmount;
               usedLoan = usedLoan + feeAmount;
               feeAmount = 0;



             }
           }
         }

         decimal totalPayment = totalCreditAmount + _totalFeeAmount + _totalExtraFeeAmount + lateRegistrationFee -
                                usedScholarship - usedLoan - usedAdvance;

         var newAdvanceAmount = advanceAmount - usedAdvance;


         //totalFeeAmount = feeAmount;


         List<payment> credits = new List<payment>();
         List<payment> debits = new List<payment>();
         payment total = new payment();
         if (totalCreditAmount > 0)
         {
           credits.Add(new payment()
             { name = _messageService.GetMessage("CREDITFEE", language), amount = totalCreditAmount });
         }

         if (_totalFeeAmount + _totalExtraFeeAmount > 0)
         {
           foreach (var fee in fees)
           {
             credits.Add(new payment() {type_name = "FEE",code =fee.fee_code, name = fee.fee_name_th, amount = fee.fee_amount??0 });
           }
           //credits.Add(new payment(){name="Fee",amount=_totalFeeAmount + _totalExtraFeeAmount});
         }

         //Get Invoice No

         // var httpClient = _httpClientFactory.CreateClient("ar_payment");
         // var salesInvoice = new sales_invoice()
         // {
         //   customer_code = studentCode,
         //   sales_invoice_date = DateTime.Today,
         //   payment_due_date = DateTime.Today.AddDays(14),
         //   total_amount = totalPayment,
         //
         // };
         // var salesInvoiceItems = new List<sales_invoice_item>();
         // salesInvoiceItems.AddRange(registerSubjects.Select(r=>new sales_invoice_item()
         // {
         //   item_code = r.subject_code,
         //   total_amount = r.lecture_amount + r.lab_amount
         // }));
         // foreach(var fee in fees)
         // {
         //   salesInvoiceItems.Add(new sales_invoice_item(){item_code= fee.fee_code,total_amount= fee.amount.Value});
         // }
         //
         // salesInvoice.sales_invoice_items = salesInvoiceItems;
         // var data = new ByteArrayContent(JsonSerializer.SerializeToUtf8Bytes(salesInvoice,new JsonSerializerOptions(){IgnoreNullValues=true})); //new ByteArrayContent(MessagePackSerializer.Serialize(body));
         //
         // data.Headers.ContentType = new  System.Net.Http.Headers.MediaTypeHeaderValue("application/json");
         // var res = await httpClient.PostAsync(_apiSetting.payment_url + "/v1/accounts_receivables/sales_invoices",data);
         // if (res.StatusCode == HttpStatusCode.Created)
         // {
         //   var result = await res.Content.ReadAsByteArrayAsync();
         //   var returnSalesInvoice = JsonSerializer.Deserialize<sales_invoice>(result);
         //   provisionRegister.advanced_amount = advanceAmount;
         // provisionRegister.credit_amount = creditAmount;
         // provisionRegister.package_amount = 0;
         // provisionRegister.is_package = 0;
         // provisionRegister.is_paid = 0;
         // provisionRegister.loan_amount = loanAmount;
         // provisionRegister.fee_amount = feeAmount;
         // provisionRegister.pay_in_no = returnSalesInvoice.sales_invoice_number;
         // provisionRegister.invoice_no = returnSalesInvoice.sales_invoice_number;
         // provisionRegister.invoice_due_date = returnSalesInvoice.payment_due_date;
         // provisionRegister.payment_amount = totalPayment;
         // provisionRegister.provision_register_status_id = 2;
         // provisionRegister.register_date = DateTime.Now;
         // provisionRegister.scholarship_amount = 0;
         // provisionRegister.total_credit = (short)totalCredit;
         // provisionRegister.total_subject = (short)totalSubjects;
         // await _dapperDb.PreRegisterConfirm(provisionRegister, provisionRegisterAdvances, provisionRegisterFees);

         ////////////////////////////////////// Confirm Payment


         //var webRegister = await dapperDB.ConfirmEnrollment(studentCode,registerYear,registerSemester,advanceUseds,loanUsed,_selectedSubjects,_webRegister,registerItems,advanceUseWebs, registerFeeWebs,_newEnAdvancePayment);

         // if (_usedScholarship > 0)
         // {
         //     debits.Add(new payment(){name=cacheService.GetMessage("SCHOLARSHIP",language) + " " + _scholarship.scholarshipName,amount=_usedScholarship});
         // }
         // if (_usedLoan > 0)
         // {
         //     debits.Add(new payment(){name=cacheService.GetMessage("LOAN",language) + " " + _loan.contractNo,amount=_usedLoan});
         // }


         // if (usedAdvance > 0)
         // {
         //     debits.Add(new payment()
         //     {
         //       type_name = "ADV",
         //       code = String.Join("/",advances.Select(a=>a.advance_no).ToList()),
         //       name="เงินโอนเก็บ",
         //       amount=usedAdvance
         //     });
         // }
          if (registerTypeCode is "LATE")
         {
           // var lateRegistration = await dapperDB.GetLateRegistrationFee(registerYear,registerSemester,educationTypeCode,studentStatus);
           // if (lateRegistration!=null)
           // {

           //     var totalDays = (DateTime.Today - lateRegistration.LATE_TRANSFER_DATE_FROM.Value).TotalDays + 1;
           //     var lateRegistrationFee = lateRegistration.FINE_RATE.Value * (decimal)totalDays;
           //     if (lateRegistrationFee > lateRegistration.MAX_FINE)
           //     {
           //         lateRegistrationFee = lateRegistration.MAX_FINE.Value;
           //     }
           credits.Add(new payment()
             { name = _messageService.GetMessage("LATEFEE", language), amount = lateRegistrationFee });
           //    _totalPayment += lateRegistrationFee;
           //}
         }

         if (usedScholarship > 0)
         {
           debits.AddRange(scholarships.Where(s => s.used_amount > 0).Select(s => new payment()
           {
             type_name = "SCH",
             code = s.scholarship_code,
             name = s.scholarship_name,
             amount = s.used_amount.Value
           }));
         }

         if (usedAdvance > 0)
         {
           debits.Add(new payment()
           {
             type_name = "ADV",
             name = _messageService.GetMessage("ADVANCE", language) + " " +
                    String.Join("/", advances.Select(a => a.advance_no).ToList()),

             code = String.Join("/",advances.Select(a=>a.advance_no).ToList()),
             amount=usedAdvance
           });
         }

         if (usedLoan > 0)
         {
           debits.Add(new payment()
           {
             type_name = "LOAN",
             code = loan.contract_no,
             name = _messageService.GetMessage("LOAN", language) + " " + loan.contract_no,
             amount = usedLoan
           });
         }

         //var payEnable = true;
         if (totalPayment == 0 && newAdvanceAmount > 0)
         {
           total.name = _messageService.GetMessage("ADVANCEDLEFT", language);
           total.amount = newAdvanceAmount;
           //payEnable = false;
         }
         else
         {
           // if (_totalPayment == 0)
           // {
           //     payEnable = false;
           // }
           total.name = _messageService.GetMessage("TOTALPAYMENT", language);
           total.amount = totalPayment;
         }
         var payEnable = true;
         // if (totalPayment ==0 && newAdvanceAmount >0)
         // {
         //     total.name =_messageService.GetMessage("ADVANCEDLEFT",language);
         //     total.amount = newAdvanceAmount;
         //     payEnable = false;
         // }
         // else
         // {
         if (totalPayment == 0)
         {
           payEnable = false;
         }

         //total.name = _messageService.GetMessage("TOTALPAYMENT", language);
         //total.amount = totalPayment;
         //}
         // if (totalPayment==0) //Create receipt
         // {
         //     // var enReceipt = new EN_RECEIPT(_webRegister,_usedScholarship > 0?_scholarship.scholarshipCode:null,_usedLoan > 0?_loan.contractNo:null);
         //     // enReceipt.FAC_CODE = faculty;
         //     // enReceipt.MAJOR_CODE = major;
         //     // var enReceiptAdvances = enReceiptAdvanceWebs.Select(a=>new EN_RECEIPT_ADVANCE(a)).ToList();
         //     // var enRegisterFinalDetails = registerItems.Select(i=>new EN_REGISTER_FINAL_DETAIL(i)).ToList();
         //     // EN_ADVANCE_PAYMENT enAdvancePayment = null;
         //     // if (_newEnAdvancePayment!=null)
         //     // {
         //     //     enAdvancePayment = _newEnAdvancePayment;
         //     // }
         //     // var receiptFeeCounters = _fees.Select((f,i)=>new EN_RECEIPT_FEE_COUNTER(f,i+1)).ToList();
         //     // var enRegisterCounterDetails = registerItems.Select(i=>new EN_REGISTER_COUNTER_DETAIL(i)).ToList();
         //     //
         //     // await dapperDB.RegisterPayment(studentCode,registerYear,registerSemester,"B",enReceipt,enReceiptAdvances,null,null,enRegisterCounterDetails,enRegisterFinalDetails,enAdvancePayment,receiptFeeCounters);
         // }

         // var registerDetail = await GetRegisterDetail(registerSubjects, studentCode, academicYearId,
         //   academicSemesterId, educationTypeId, facultyId, majorId, package, academicYearEntryId, language,
         //   provisionRegisterTypeId, studentStatus);

         // var registerDetail =  await GetRegisterDetail(provisionRegisterSubjects.Select(s=>new register_subject(s)).ToList(),  "TH", registerTypeCode,
         //   registerTokenDetail);
         //_cacheService.RemoveStudentCache(studentCode);
         var existingFees=_context.t_provision_register_fee.Where(f =>
           f.provision_register_uid == provisionRegister.provision_register_uid);
         if (existingFees.Any())
         {
           _context.t_provision_register_fee.RemoveRange(existingFees);
         }
         await _context.t_provision_register_fee.AddRangeAsync(provisionRegisterFees);
         provisionRegister.register_status_id = 2;
         provisionRegister.payment_amount = totalPayment;
         provisionRegister.fee_amount = feeAmount;
         provisionRegister.advanced_amount = usedAdvance;
         provisionRegister.credit_amount = creditAmount;
         provisionRegister.scholarship_amount = usedScholarship;


         await _context.SaveChangesAsync();
         var registerSummary = new register_summary()
         {
           totalCredit = totalCredit ?? 0,
           totalPaymentAmount = 0,
           //public string payInNo {get;set;}
           subjects = provisionRegisterSubjects.Select(s => new register_subject(s)).ToList(),
           credits = credits,
           debits = debits,
           total = total,
           payment_url ="https://rsu.71dev.com/payment-api/api/payment/redirect/" + provisionRegister.bill_uid.ToString(),
           provision_register_uid = provisionRegister.provision_register_uid

         };
         await CreateRegisterBill(registerTokenDetail.student_uid, registerSummary, provisionRegister.provision_register_uid.Value);
         return registerSummary;




       }
     }
  }

}
