using System.Collections.Generic;
using edu_api.Modules.Register.Models;

namespace edu_api.Modules.Register.Services
{
  public class MessageService
  {

    Dictionary<string, string> diErrorMessage;
    Dictionary<string, string> diMessage;


    public MessageService()
    {

      diErrorMessage = new Dictionary<string, string>();
      diErrorMessage.TryAdd("NOT_FOUND_SUBJECT|EN", "{0} not found");
      diErrorMessage.TryAdd("NOT_FOUND_SUBJECT|TH", "ไม่พบข้อมูล {0}");
      diErrorMessage.TryAdd("EXISTING_SUBJECT|EN", "{0} already exists");
      diErrorMessage.TryAdd("EXISTING_SUBJECT|TH", "{0} มีในรายการอยู่แล้ว");
      diErrorMessage.TryAdd("INVALID_USER_OR_PASSWORD|EN", "Invalid user or password");
      diErrorMessage.TryAdd("INVALID_USER_OR_PASSWORD|TH", "ชื่อผู้ใช้หรือรหัสผ่านผิด");
      diErrorMessage.TryAdd("OVERLAPPED_STUDY_TIME|EN", "STUDY TIME OVERLAP WITH {0}");
      diErrorMessage.TryAdd("OVERLAPPED_STUDY_TIME|TH", "ตารางเรียนตรงกับวิชา {0}");
      diErrorMessage.TryAdd("OVERLAPPED_EXAM_TIME|EN", "EXAM TIME OVERLAP WITH {0}");
      diErrorMessage.TryAdd("OVERLAPPED_EXAM_TIME|TH", "ตารางสอบตรงกับวิชา {0}");
      diErrorMessage.TryAdd("EXAM_TIME|EN", "EXAM TIME OVERLAP WITH {0}");
      diErrorMessage.TryAdd("EXAM_TIME|TH", "ตารางสอบตรงกับวิชา {0}");
      diErrorMessage.TryAdd("HAVE_WAITING_LIST|TH", "คุณยังมีวิชาที่ต้องรออนุมัติอยู่จำนวน {0} วิชา");
      diErrorMessage.TryAdd("HAVE_WAITING_LIST|TH", "You have {0} subjects waiting for approve");
      diErrorMessage.TryAdd("UNDER_MIN_CREDIT|TH", "จำนวนหน่วยกิตที่ลงทะเบียนน้อยกว่าที่กำหนดไว้");
      diErrorMessage.TryAdd("UNDER_MIN_CREDIT|EN", "Total enrollment credit less than requirement");
      diErrorMessage.TryAdd("OVER_MAX_CREDIT|TH", "จำนวนหน่วยกิตที่ลงทะเบียนมากกว่าที่กำหนดไว้");
      diErrorMessage.TryAdd("OVER_MAX_CREDIT|EN", "Total enrollment credit greater than requirement");
      diErrorMessage.TryAdd("NO_SUBJECT_FOUND|TH", "ไม่สามารถลงทะเบียนโดยไม่มีรายวิชาได้");
      diErrorMessage.TryAdd("NO_SUBJECT_FOUND|EN", "Cannot register with no subject");
      diErrorMessage.TryAdd("FACULTY_YEAR_RESTRICTION|TH", "วิชานี้ไม่เปิดให้คณะหรือชั้นปีของคุณลงทะเบียน");
      diErrorMessage.TryAdd("FACULTY_YEAR_RESTRICTION|EN", "This subject not open for your faculty or level");



      diMessage = new Dictionary<string, string>();
      diMessage.TryAdd("CREDITFEE|EN", "Credit Fee");
      diMessage.TryAdd("CREDITFEE|TH", "ค่าหน่วยกิต");
      diMessage.TryAdd("PACKAGE|EN", "Package Fee");
      diMessage.TryAdd("PACKAGE|TH", "ค่าเหมาจ่าย");
      diMessage.TryAdd("LATEFEE|TH", "ค่าธรรมเนียมลงทะเบียนล่าช้า");
      diMessage.TryAdd("LATEFEE|EN", "Late registration fee");
      diMessage.TryAdd("SCHOLARSHIP|EN", "Scholarship");
      diMessage.TryAdd("SCHOLARSHIP|TH", "ทุนการศึกษา");
      diMessage.TryAdd("LOAN|EN", "Loan No.");
      diMessage.TryAdd("LOAN|TH", "ทุนกู้ยืม เลขที่");
      diMessage.TryAdd("ADVANCE|EN", "Advance No.");
      diMessage.TryAdd("ADVANCE|TH", "เงินโอนเก็บ หมายเลข");
      diMessage.TryAdd("ADVANCELEFT|EN", "Advance Left");
      diMessage.TryAdd("ADVANCELEFT|TH", "เหลือเงินโอนเก็บ");
      diMessage.TryAdd("TOTALPAYMENT|EN", "Total Payment");
      diMessage.TryAdd("TOTALPAYMENT|TH", "ชำระเงิน");

      diMessage.TryAdd("UNMATCHSTATUS|EN", "Cannot enroll, please contact registration office");
      diMessage.TryAdd("UNMATCHSTATUS|TH", "ไม่สามารถลงทะเบียนได้ กรุณาติดต่อฝ่ายทะเบียน");
      diMessage.TryAdd("MISSINGEVAL|EN", "Cannot enroll, please contact you advisor");
      diMessage.TryAdd("MISSINGEVAL|TH", "ไม่สามารถลงทะเบียนได้ กรุณาติดต่ออาจารย์ที่ปรึกษา");
      diMessage.TryAdd("DORMDEBT|EN", "Cannot enroll, please contact dormitory financial");
      diMessage.TryAdd("DORMDEBT|TH", "ไม่สามารถลงทะเบียนได้ กรุณาติดต่อฝ่ายการเงิน หอพัก");
      diMessage.TryAdd("FINDEBT|EN", "Cannot enroll, please contact financial department");
      diMessage.TryAdd("FINDEBT|TH", "ไม่สามารถลงทะเบียนได้ กรุณาติดต่อฝ่ายการเงิน");
      diMessage.TryAdd("MISSINGADV|EN", "Cannot enroll, please contact you advisor");
      diMessage.TryAdd("MISSINGADV|TH", "ไม่สามารถลงทะเบียนได้ กรุณาติดต่ออาจารย์ที่ปรึกษา");
      diMessage.TryAdd("ELIGIBLE|EN", "Student is eligible to enroll");
      diMessage.TryAdd("ELIGIBLE|TH", "คุณสามารถลงทะเบียนได้");
      diMessage.TryAdd("PENDINGPAYMENT|EN", "You have pending payment for enrollment");
      diMessage.TryAdd("PENDINGPAYMENT|TH", "คุณยังไม่ได้ชำระค่าลงทะเบียน");
      diMessage.TryAdd("ENROLLEXISTS|EN", "Enrollment for this semester already exists");
      diMessage.TryAdd("ENROLLEXISTS|TH", "คุณได้ลงทะเบียนในเทอมนี้ไปแล้ว");
      diMessage.TryAdd("ENROLLNOTFOUND|EN", "Cannot find enrollment on this semester");
      diMessage.TryAdd("ENROLLNOTFOUND|TH", "ไม่พบข้อมูลการลงทะเบียนของเทอมนี้");
      diMessage.TryAdd("HAVEPRE|TH", "คุณได้ลงทะเบียนล่วงหน้าไว้เรียบร้อยแล้ว");
      diMessage.TryAdd("HAVEPRE|EN", "You have pre registration already");
      diMessage.TryAdd("NEEDREG|TH", "คุญจำเป็นต้องลงทะเบียนก่อน ถึงสามารถเพิ่มถอนรายวิชาได้");
      diMessage.TryAdd("NEEDREG|EN", "Registration is required before add or drop subject");
      diMessage.TryAdd("TOKENINVALID|TH", "มีการใช้ชื่อคุณบนอุปกรณ์ตัวอื่น โปรดกรุณาเข้าระบบใหม่");
      diMessage.TryAdd("TOKENINVALID|EN", "Your account was login on other device, please login again");

      diMessage.TryAdd("REGISTERED|TH", "คุณได้ลงทะเบียนไว้เรียบร้อยแล้ว");
      diMessage.TryAdd("REGISTERED|EN", "You already registered");
      diMessage.TryAdd("NOREGISTERED|TH", "คุณได้ยังไม่ได้ลงทะเบียน");
      diMessage.TryAdd("NOREGISTERED|EN", "You are not registered");







      //diSubject = subjects.ToDictionary(s=>s.subjectCode + s.yearEntry.ToString() + s.educationTypeCode,s=>s);
    }
    public string GetMessage(string code,string language)
    {
      if (diMessage.ContainsKey(code +"|" + language))
      {
        return diMessage[code +"|" + language];
      }
      else
      {
        return "Unspecified error occur";
      }
    }
    public string GetErrorMessage(string code,string language,string data)
    {
      if (diErrorMessage.ContainsKey(code +"|" + language))
      {
        return string.Format(diErrorMessage[code +"|" + language],data);
      }
      else
      {
        return "Unspecified error occur";
      }
    }
    public string GetErrorMessage(string code,string language)
    {
      if (diErrorMessage.ContainsKey(code +"|" + language))
      {
        return diErrorMessage[code +"|" + language];
      }
      else
      {
        return "Unspecified error occur";
      }
    }
    public string GetErrorMessage(DataException dataException,string language)
    {
      if (diErrorMessage.ContainsKey(dataException.Message +"|" + language))
      {
        return string.Format(diErrorMessage[dataException.Message +"|" + language],dataException.referenceMessage);
      }
      else
      {
        return "Unspecified error occur";
      }
    }
  }
}
