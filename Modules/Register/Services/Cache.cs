using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using edu_api.Modules.Register.Models;
using edu_api.Modules.Study.Databases.Models;
using Hangfire;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using SeventyOneDev.Utilities;

namespace edu_api.Modules.Register.Services
{
  public class CacheService
  {
    private readonly Db _db;
    private HashSet<string> registerYearSemesterHash;
    Dictionary<Guid,List<register_subject>> diRegisterSubject;
    Dictionary<string,register_subject> yearSubjectDict;
    Dictionary<Guid?, List<register_subject_section>> yearSubjectSectionDict;
    Dictionary<Guid?, List<register_subject_section_faculty>> yearSubjectSectionFacultyDict;
    Dictionary<Guid?, List<v_year_subject_section_year>> yearSubjectSectionYearDict;
    Dictionary<string, register_subject_cost> subjectYearCostDict;

    //private Dictionary<Guid?, string> subjectYearDict;


    public CacheService(IServiceScopeFactory serviceScopeFactory)
    {
      diRegisterSubject = new Dictionary<Guid, List<register_subject>>();
      _db = serviceScopeFactory.CreateScope().ServiceProvider.GetRequiredService<Db>();
      registerYearSemesterHash = new HashSet<string>();
    }

    public bool HasCache(Guid? academicYearUid,Guid? academicSemesterUid)
    {
      var hasCache= registerYearSemesterHash.Contains("" + academicYearUid + academicSemesterUid);
      Console.WriteLine("Have cache?:" + hasCache);
      // if (hasCache) return true;
      // Console.WriteLine("" + academicYearUid + "|" + academicSemesterUid);
      // AddCache(academicYearUid ?? Guid.Empty, academicSemesterUid ?? Guid.Empty);
      return false;
      //BackgroundJob.Enqueue(() => AddCache(academicYearUid??Guid.Empty,academicSemesterUid??Guid.Empty));

    }

    public void AddCache(Guid academicYearUid,Guid academicSemesterUid)
    {
      Console.WriteLine("Start building cache");
      LoadRegisterSubjectCache(academicYearUid, academicSemesterUid);
    }

    public register_subject GerSubject(string yearSubjectCode,Guid? educationTypeUid)
    {
      if (yearSubjectDict.ContainsKey(yearSubjectCode + educationTypeUid))
      {
        return yearSubjectDict[yearSubjectCode + educationTypeUid];
      }
      throw new DataException("NOT_FOUND_SUBJECT");
    }

    public bool HasYearSubjectSection(Guid? yearSubjectUid)
    {
      if (yearSubjectSectionDict!=null)
      return yearSubjectSectionDict.ContainsKey(yearSubjectUid);
      return false;
    }

    public List<register_subject_section> GetYearSubjectSection(Guid? yearSubjectUid)
    {

      return yearSubjectSectionDict[yearSubjectUid];
    }

    public bool HasYearSubjectSectionFaculty(Guid? yearSubjectSectionUid)
    {
      if (yearSubjectSectionFacultyDict!=null)
      return yearSubjectSectionFacultyDict.ContainsKey(yearSubjectSectionUid);
      return false;
    }
    public bool HasYearSubjectSectionYear(Guid? yearSubjectSectionUid)
    {
      if (yearSubjectSectionYearDict!=null)
      return yearSubjectSectionYearDict.ContainsKey(yearSubjectSectionUid);
      return false;
    }

    public bool HasYearSubjectCost(Guid? subjectYearUid,Guid? entryAcademicYearUid, Guid? educationTypeUid)
    {
      if (subjectYearCostDict!=null)
      return subjectYearCostDict.ContainsKey("" + subjectYearUid + entryAcademicYearUid + educationTypeUid);
      return false;
    }
    public register_subject_cost GetSubjectYearCost(Guid? subjectYearUid,Guid? entryAcademicYearUid, Guid? educationTypeUid)
    {
      return  subjectYearCostDict["" + subjectYearUid + entryAcademicYearUid +  educationTypeUid];
    }




    public void LoadRegisterSubjectCache(Guid academicYearUid,Guid academicSemesterUid)
    {
      Console.WriteLine("Start load data ......");
      var yearSubjects = _db.v_year_subject.AsNoTracking().Where(s =>
        s.academic_year_uid == academicYearUid &&
        s.academic_semester_uid == academicSemesterUid
      ).ProjectTo<v_year_subject,year_subject_cache>().ToList();//.ProjectTo<t_year_subject,register_subject>().FirstOrDefaultAsync();
      Console.WriteLine("Year subject loaded");
      if (yearSubjectDict == null) yearSubjectDict = new Dictionary<string, register_subject>();
      foreach (var yearSubject in yearSubjects)
      {
        yearSubjectDict.TryAdd(yearSubject.year_subject_code + yearSubject.education_type_uid, new register_subject(yearSubject));
      }
      //yearSubjectDict = yearSubjects.ToDictionary(ys => ys.year_subject_code + ys.education_type_uid, ys => new register_subject(ys));
      
      var yearSubjectSections = _db.v_year_subject_section.AsNoTracking().Where(s =>
        s.academic_year_uid == academicYearUid &&
        s.academic_semester_uid == academicSemesterUid
      ).ToList();
      yearSubjectSectionDict = yearSubjectSections.GroupBy(yss => yss.year_subject_uid).ToDictionary(yss => yss.Key, yss => yss.Select(y=>new register_subject_section(y)).ToList());
      Console.WriteLine("Year subject section loaded");

      var yearSubjectSectionFaculties = _db.v_year_subject_section_faculty.AsNoTracking().Where(s =>
        s.academic_year_uid == academicYearUid &&
        s.academic_semester_uid == academicSemesterUid
      ).ToList();
      yearSubjectSectionFacultyDict = yearSubjectSectionFaculties.GroupBy(yss => yss.year_subject_section_uid)
        .ToDictionary(yss => yss.Key, yss => yss.Select(y=>new register_subject_section_faculty(y)).ToList());
      Console.WriteLine("Year subject section faculty loaded");

      var yearSubjectSectionYears = _db.v_year_subject_section_year.AsNoTracking().Where(s =>
        s.academic_year_uid == academicYearUid &&
        s.academic_semester_uid == academicSemesterUid
      ).ToList();
      yearSubjectSectionYearDict = yearSubjectSectionYears.GroupBy(yss => yss.year_subject_section_uid).ToDictionary(yss => yss.Key, yss => yss.ToList());
      Console.WriteLine("Year subject section year loaded");
      // var subjectYears = _db.t_subject_year.AsNoTracking().ToList();

      // var subjectYear = await _context.t_subject_year.AsNoTracking().FirstOrDefaultAsync(s =>
      //   s.subject_uid == subjectUid && s.curriculum_year_uid == entryAcademicYearUid);

      // var subjectYearCost = _db.v_subject_year_cost.AsNoTracking().ToList();
      //
      // subjectYearCostDict = subjectYearCost.ToDictionary(s => ""+s.subject_year_uid + s.curriculum_year_uid + s.education_type_uid, s => new register_subject_cost()
      // {
      //   credit = s.credit,
      //   lecture_amount = s.lecture_amount,
      //   lab_amount = s.lab_amount
      // });
      registerYearSemesterHash.Add("" + academicYearUid + academicSemesterUid);


    }
    public List<register_subject> GetRegisterSubjects(Guid studentUid)
    {
      if (diRegisterSubject.ContainsKey(studentUid))
      {
        return diRegisterSubject[studentUid];
      }
      else
      {
        //var registerSubjects = con
        return new List<register_subject>();
      }

    }
    public List<register_subject> AddNewRegisterSubject(Guid studentUid, register_subject registerSubject)
    {
      if (diRegisterSubject.ContainsKey(studentUid))
      {
        diRegisterSubject[studentUid].Add(registerSubject);
      }
      else
      {
        diRegisterSubject.TryAdd(studentUid, new List<register_subject>() {registerSubject});
      }

      return diRegisterSubject[studentUid].ToList();
    }

    public List<register_subject> RemoveRegisterSubject(Guid studentUid, register_subject registerSubject)
    {
      if (diRegisterSubject.ContainsKey(studentUid))
      {
        // var registerSubjects = diRegisterSubject[studentUid];
        // registerSubjects =

        diRegisterSubject[studentUid] = diRegisterSubject[studentUid].Where(r =>
          r.year_subject_uid != registerSubject.year_subject_uid).ToList();
      }
      // else
      // {
      //   diRegisterSubject.TryAdd(studentUid, new List<register_subject>() {registerSubject});
      // }

      return diRegisterSubject[studentUid].ToList();
      //return new List<register_subject>();
    }
    public List<register_subject> UpdateRegisterSubject(Guid studentUid, register_subject registerSubject)
    {
      if (!diRegisterSubject.ContainsKey(studentUid)) return diRegisterSubject[studentUid].ToList();
      var leftRegisterSubjects = diRegisterSubject[studentUid].Where(r =>
        r.year_subject_uid != registerSubject.year_subject_uid).ToList();
     
      leftRegisterSubjects.Add(registerSubject);
      diRegisterSubject[studentUid] = leftRegisterSubjects;

      return diRegisterSubject[studentUid].ToList();
    }
    public List<register_subject> GetRegisterSubject(Guid studentUid)
    {
      return diRegisterSubject[studentUid].ToList();
    }

    public void AddRegisterSubjects(Guid studentUid,List<register_subject> registerSubjects)
    {
      if (diRegisterSubject.ContainsKey(studentUid))
      {
        diRegisterSubject[studentUid] = registerSubjects;
      }
      else
      {
        diRegisterSubject.TryAdd(studentUid, registerSubjects);
      }
    }
    public void AddRegisterSubject(Guid studentUid,register_subject registerSubject)
    {
      if (diRegisterSubject.ContainsKey(studentUid))
      {
        var registerSubjects = diRegisterSubject[studentUid];
        registerSubjects.Add(registerSubject);
        diRegisterSubject[studentUid] = registerSubjects;
      }
      else
      {
        diRegisterSubject.TryAdd(studentUid, new List<register_subject>(){registerSubject});
      }
    }

    public void RemoveStudentCache(Guid studentUid)
    {
      if (diRegisterSubject.ContainsKey(studentUid))
      {
        diRegisterSubject.Remove(studentUid);
      }
    }

    public List<subject_study_time> GetStudentStudyTime(Guid studentUid)
    {
      var studentStudyTimes = new List<subject_study_time>();
      if (diRegisterSubject.ContainsKey(studentUid))
      {
        foreach (var registerSubject in diRegisterSubject[studentUid])
        {
          if (registerSubject.lecture_section != null)
          {
            if (registerSubject.lecture_section.study_time_text_en != null)
            {
              studentStudyTimes.AddRange(subject_study_time.Parse(registerSubject.lecture_section.study_time_text_en,registerSubject.year_subject_code));
            }
          }

          if (registerSubject.lab_section != null)
          {
            if (registerSubject.lab_section.study_time_text_en != null)
            {
              studentStudyTimes.AddRange(subject_study_time.Parse(registerSubject.lab_section.study_time_text_en,registerSubject.year_subject_code));
            }
          }
        }
      }


      return studentStudyTimes;
    }

    public List<subject_study_time> GetSubjectStudyTime(register_subject registerSubject)
    {
      var studentStudyTimes = new List<subject_study_time>();
        if (registerSubject.lecture_section != null)
        {
          if (registerSubject.lecture_section.study_time_text_en != null)
          {
            studentStudyTimes.AddRange(subject_study_time.Parse(registerSubject.lecture_section.study_time_text_en,registerSubject.year_subject_code));
          }
        }

        if (registerSubject.lab_section != null)
        {
          if (registerSubject.lab_section.study_time_text_en != null)
          {
            studentStudyTimes.AddRange(subject_study_time.Parse(registerSubject.lab_section.study_time_text_en,registerSubject.year_subject_code));
          }
        }


      return studentStudyTimes;
    }

    public List<subject_exam_time> GetStudentExamTime(Guid studentUid)
    {
      var studentExamTimes = new List<subject_exam_time>();
      if (diRegisterSubject.ContainsKey(studentUid))
      {
        foreach (var registerSubject in diRegisterSubject[studentUid])
        {
          studentExamTimes.AddRange(subject_exam_time.Parse(registerSubject,registerSubject.year_subject_code));
        }
      }

      return studentExamTimes;
    }

    public List<subject_exam_time> GetSubjectExamTime(register_subject registerSubject)
    {
      var studentExamTimes = new List<subject_exam_time>();

      studentExamTimes.AddRange(subject_exam_time.Parse(registerSubject,registerSubject.year_subject_code));


      return studentExamTimes;
    }

  }

}
