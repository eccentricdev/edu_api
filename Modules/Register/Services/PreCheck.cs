using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using edu_api.Modules.Register.Databases;
using edu_api.Modules.Register.Models;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using rsu_common_api.models;
using SeventyOneDev.Utilities;
using JsonSerializer = System.Text.Json.JsonSerializer;

namespace edu_api.Modules.Register.Services
{
  public class PreCheckService
  {
    private readonly Db _context;

    public PreCheckService(Db context)
    {
      _context = context;
    }

    public async Task<validate_response> CounterValidate(string studentCode)
    {
      var validateResponse = new validate_response();
      //Get student info
      //var studentUid = claimsIdentity.Claims.FirstOrDefault(c => c.Type == "user_uid")?.Value;
      // 'entry_academic_semester_uid' is not a member of type 'rsu_common_api.models.t_student'
      var studentProfile = await _context.t_student.AsNoTracking().Where(s => s.student_code == studentCode).ProjectTo<t_student,register_profile>().FirstOrDefaultAsync();

      var academicRegisterCalendars = await _context.v_academic_register_calendar.AsNoTracking().Where(a =>
        a.education_type_uid == studentProfile.education_type_uid &&
        a.from_date <= DateTime.Today &&
        a.to_date >= DateTime.Today).ToListAsync();
      var academicRegisterTypes = await _context.t_academic_register_type.AsNoTracking().ToListAsync();
      var preRegisterType = academicRegisterTypes.FirstOrDefault(t => t.academic_register_type_code == "PRE");
      if (preRegisterType==null) throw new Exception("NO_PRE_REGISTER_TYPE");
      var lateRegisterType = academicRegisterTypes.FirstOrDefault(t => t.academic_register_type_code == "LATE");
      if (lateRegisterType==null) throw new Exception("NO_LATE_REGISTER_TYPE");
      var addDropRegisterType = academicRegisterTypes.FirstOrDefault(t => t.academic_register_type_code == "ADDDROP");
      if (addDropRegisterType==null) throw new Exception("NO_ADDDROP_REGISTER_TYPE");

      var preRegister = academicRegisterCalendars.FirstOrDefault(a => a.academic_register_type_uid == preRegisterType.academic_register_type_uid);
      var lateRegister = academicRegisterCalendars.FirstOrDefault(a => a.academic_register_type_uid == lateRegisterType.academic_register_type_uid);
      var addDropRegister = academicRegisterCalendars.FirstOrDefault(a => a.academic_register_type_uid == addDropRegisterType.academic_register_type_uid);
      var registerToken = new t_register_token()
      {
        student_uid = studentProfile.student_uid,
        education_type_uid = studentProfile.education_type_uid,
        college_faculty_uid = studentProfile.college_faculty_uid,
        faculty_curriculum_uid = studentProfile.faculty_curriculum_uid,
        entry_academic_year_uid = studentProfile.entry_academic_year_uid,
        student_status_uid = studentProfile.student_status_uid,
        id_card_no = studentProfile.citizen_id,
        is_package = false
      };
      var package = await _context.t_curriculum.AsNoTracking()
        .FirstOrDefaultAsync(a => a.faculty_curriculum_uid == studentProfile.faculty_curriculum_uid && a.curriculum_year_uid==studentProfile.entry_academic_year_uid);

      if (package != null)
      {
        registerToken.is_package = true;

      }
      if (preRegister != null)
      {
        registerToken.pre_academic_register_calendar_uid = preRegister.academic_register_calendar_uid;
        registerToken.pre_register_academic_year_uid = preRegister.academic_year_uid;
        registerToken.pre_register_academic_semester_uid = preRegister.academic_semester_uid;
        registerToken.register_academic_year_code = preRegister.academic_year_code;
        registerToken.register_semester_code = preRegister.semester_code;
        registerToken.register_semester_uid = preRegister.semester_uid;
        validateResponse.register_type_code = "PRE";
        validateResponse.min_credit = (short?)preRegister.min_credit;
        validateResponse.max_credit = (short?)preRegister.max_credit;
        if (registerToken.is_package ?? false)
        {
          switch (preRegister.semester_code)
          {
            case "1":
              registerToken.package_amount = package.semester_1_amount;
              break;
            case "2":
              registerToken.package_amount = package.semester_2_amount;
              break;
            case "3":
              registerToken.package_amount = package.semester_3_amount;
              break;

          }
        }


        var provisionRegister = await _context.t_provision_register.AsNoTracking().FirstOrDefaultAsync(s =>
          s.student_uid == studentProfile.student_uid && s.academic_year_uid == preRegister.academic_year_uid &&
          s.academic_semester_uid == preRegister.academic_semester_uid && s.register_type_code=="PRE"
          );
        if (provisionRegister != null)
        {
          if (provisionRegister.register_status_id == 1)
          {
            validateResponse.register_status_code = "SAVE";
          }
          else
          {
            validateResponse.register_status_code = "CONF";
          }

        }
        else
        {
          validateResponse.register_status_code = "NONE";
        }
      }
      else if (lateRegister != null)
      {
        registerToken.late_academic_register_calendar_uid = lateRegister.academic_register_calendar_uid;
        registerToken.late_register_academic_year_uid = lateRegister.academic_year_uid;
        registerToken.late_register_academic_semester_uid = lateRegister.academic_semester_uid;
        registerToken.register_academic_year_code = lateRegister.academic_year_code;
        registerToken.register_semester_code = lateRegister.semester_code;
        registerToken.register_semester_uid = lateRegister.semester_uid;
        validateResponse.register_type_code = "LATE";
        validateResponse.min_credit = (short?)lateRegister.min_credit;
        validateResponse.max_credit = (short?)lateRegister.max_credit;
        if (registerToken.is_package ?? false)
        {
          switch (lateRegister.semester_code)
          {
            case "1":
              registerToken.package_amount = package.semester_1_amount;
              break;
            case "2":
              registerToken.package_amount = package.semester_2_amount;
              break;
            case "3":
              registerToken.package_amount = package.semester_3_amount;
              break;

          }
        }
        var provisionRegister = await _context.t_provision_register.AsNoTracking().FirstOrDefaultAsync(s =>
          s.student_uid == studentProfile.student_uid && s.academic_year_uid == lateRegister.academic_year_uid &&
          s.academic_semester_uid == lateRegister.academic_semester_uid  && s.register_type_code=="LATE");
        if (provisionRegister != null)
        {
          if (provisionRegister.register_status_id == 1)
          {
            validateResponse.register_status_code = "SAVE";
          }
          else
          {
            validateResponse.register_status_code = "CONF";
          }

        }
        else
        {
          validateResponse.register_status_code = "NONE";
        }
      }
      else if (addDropRegister != null)
      {
        registerToken.add_drop_academic_register_calendar_uid = addDropRegister.academic_register_calendar_uid;
        registerToken.add_drop_register_academic_year_uid = addDropRegister.academic_year_uid;
        registerToken.add_drop_register_academic_semester_uid = addDropRegister.academic_semester_uid;
        registerToken.register_academic_year_code = addDropRegister.academic_year_code;
        registerToken.register_semester_code = addDropRegister.semester_code;
        registerToken.register_semester_uid = addDropRegister.semester_uid;
        validateResponse.register_type_code = "ADDDROP";
        validateResponse.min_credit = (short?)addDropRegister.min_credit;
        validateResponse.max_credit = (short?)addDropRegister.max_credit;
        if (registerToken.is_package ?? false)
        {
          switch (addDropRegister.semester_code)
          {
            case "1":
              registerToken.package_amount = package.semester_1_amount;
              break;
            case "2":
              registerToken.package_amount = package.semester_2_amount;
              break;
            case "3":
              registerToken.package_amount = package.semester_3_amount;
              break;

          }
        }

      }
      await _context.t_register_token.AddAsync(registerToken);
      await _context.SaveChangesAsync();
      var menus = GetMenus(preRegister != null, lateRegister != null,addDropRegister != null, "TH");
      validateResponse.menus = menus;
      validateResponse.is_eligible = true;
      validateResponse.register_token_uid = registerToken.register_token_uid;
      validateResponse.message = "";
      return validateResponse;
    }

    public async Task<validate_response> Validate(ClaimsIdentity claimsIdentity)
    {
      var validateResponse = new validate_response();
      //Get student info
      var studentUid = claimsIdentity.Claims.FirstOrDefault(c => c.Type == "user_uid")?.Value;
      // 'entry_academic_semester_uid' is not a member of type 'rsu_common_api.models.t_student'
      var studentProfile = await _context.t_student.AsNoTracking().Where(s => s.student_uid == Guid.Parse(studentUid)).ProjectTo<t_student,register_profile>().FirstOrDefaultAsync();
      var academicRegisterCalendars = await _context.v_academic_register_calendar.AsNoTracking().Where(a =>
        a.education_type_uid == studentProfile.education_type_uid &&
        a.from_date <= DateTime.Today &&
        a.to_date >= DateTime.Today).ToListAsync();
      var academicRegisterTypes = await _context.t_academic_register_type.AsNoTracking().ToListAsync();
      if (!academicRegisterTypes.Any()) throw new Exception("NO_REGISTER_TYPE");
      var preRegisterType = academicRegisterTypes.FirstOrDefault(t => t.academic_register_type_code == "PRE");
      if (preRegisterType==null) throw new Exception("NO_PRE_REGISTER_TYPE");
      var lateRegisterType = academicRegisterTypes.FirstOrDefault(t => t.academic_register_type_code == "LATE");
      if (lateRegisterType==null) throw new Exception("NO_LATE_REGISTER_TYPE");
      var addDropRegisterType = academicRegisterTypes.FirstOrDefault(t => t.academic_register_type_code == "ADDDROP");
      if (addDropRegisterType==null) throw new Exception("NO_ADDDROP_REGISTER_TYPE");


      var preRegister = academicRegisterCalendars.FirstOrDefault(a => a.academic_register_type_uid == preRegisterType.academic_register_type_uid);
      var lateRegister = academicRegisterCalendars.FirstOrDefault(a => a.academic_register_type_uid == lateRegisterType.academic_register_type_uid);
      var addDropRegister = academicRegisterCalendars.FirstOrDefault(a => a.academic_register_type_uid == addDropRegisterType.academic_register_type_uid);
      var registerToken = new t_register_token()
      {
        student_uid = studentProfile.student_uid,
        education_type_uid = studentProfile.education_type_uid,
        college_faculty_uid = studentProfile.college_faculty_uid,
        faculty_curriculum_uid = studentProfile.faculty_curriculum_uid,
        entry_academic_year_uid = studentProfile.entry_academic_year_uid,
        student_status_uid = studentProfile.student_status_uid,
        id_card_no = studentProfile.citizen_id,
        is_package = false
      };
      var package = await _context.t_curriculum.AsNoTracking()
        .FirstOrDefaultAsync(a => a.faculty_curriculum_uid == studentProfile.faculty_curriculum_uid && a.curriculum_year_uid==studentProfile.entry_academic_year_uid);
      if (package != null)
      {
        if (package.is_package ?? false)
        {

          registerToken.is_package = true;
        }

      }
      if (preRegister != null)
      {
        registerToken.pre_academic_register_calendar_uid = preRegister.academic_register_calendar_uid;
        registerToken.pre_register_academic_year_uid = preRegister.academic_year_uid;
        registerToken.pre_register_academic_semester_uid = preRegister.academic_semester_uid;
        registerToken.register_academic_year_code = preRegister.academic_year_code;
        registerToken.register_semester_code = preRegister.semester_code;
        registerToken.register_semester_uid = preRegister.semester_uid;

        validateResponse.register_type_code = "PRE";
        registerToken.min_credit = (short?)preRegister.min_credit;
        registerToken.max_credit = (short?)preRegister.max_credit;
        if (registerToken.is_package ?? false)
        {
          switch (preRegister.semester_code)
          {
            case "1":
              registerToken.package_amount = package.semester_1_amount;
              break;
            case "2":
              registerToken.package_amount = package.semester_2_amount;
              break;
            case "3":
              registerToken.package_amount = package.semester_3_amount;
              break;

          }
        }
        var provisionRegister = await _context.t_provision_register.AsNoTracking().FirstOrDefaultAsync(s =>
          s.student_uid == studentProfile.student_uid && s.academic_year_uid == preRegister.academic_year_uid &&
          s.academic_semester_uid == preRegister.academic_semester_uid && s.register_type_code=="PRE"
          );
        if (provisionRegister != null)
        {
          if (provisionRegister.register_status_id == 1)
          {
            validateResponse.register_status_code = "SAVE";
          }
          else
          {
            validateResponse.register_status_code = "CONF";
          }

        }
        else
        {
          validateResponse.register_status_code = "NONE";
        }
      }
      else if (lateRegister != null)
      {
        registerToken.late_academic_register_calendar_uid = lateRegister.academic_register_calendar_uid;
        registerToken.late_register_academic_year_uid = lateRegister.academic_year_uid;
        registerToken.late_register_academic_semester_uid = lateRegister.academic_semester_uid;
        registerToken.register_academic_year_code = lateRegister.academic_year_code;
        registerToken.register_semester_code = lateRegister.semester_code;
        registerToken.register_semester_uid = lateRegister.semester_uid;
        validateResponse.register_type_code = "LATE";
        registerToken.min_credit = (short?)lateRegister.min_credit;
        registerToken.max_credit = (short?)lateRegister.max_credit;
        if (registerToken.is_package ?? false)
        {
          switch (lateRegister.semester_code)
          {
            case "1":
              registerToken.package_amount = package.semester_1_amount;
              break;
            case "2":
              registerToken.package_amount = package.semester_2_amount;
              break;
            case "3":
              registerToken.package_amount = package.semester_3_amount;
              break;

          }
        }
        var provisionRegister = await _context.t_provision_register.AsNoTracking().FirstOrDefaultAsync(s =>
          s.student_uid == studentProfile.student_uid && s.academic_year_uid == lateRegister.academic_year_uid &&
          s.academic_semester_uid == lateRegister.academic_semester_uid  && s.register_type_code=="LATE");
        if (provisionRegister != null)
        {
          if (provisionRegister.register_status_id == 1)
          {
            validateResponse.register_status_code = "SAVE";
          }
          else
          {
            validateResponse.register_status_code = "CONF";
          }

        }
        else
        {
          validateResponse.register_status_code = "NONE";
        }
      }
      else if (addDropRegister != null)
      {
        registerToken.add_drop_academic_register_calendar_uid = addDropRegister.academic_register_calendar_uid;
        registerToken.add_drop_register_academic_year_uid = addDropRegister.academic_year_uid;
        registerToken.add_drop_register_academic_semester_uid = addDropRegister.academic_semester_uid;
        registerToken.register_academic_year_code = addDropRegister.academic_year_code;
        registerToken.register_semester_code = addDropRegister.semester_code;
        registerToken.register_semester_uid = addDropRegister.semester_uid;
        validateResponse.register_type_code = "ADDDROP";
        registerToken.min_credit = (short?)addDropRegister.min_credit;
        registerToken.max_credit = (short?)addDropRegister.max_credit;
        if (registerToken.is_package ?? false)
        {
          switch (addDropRegister.semester_code)
          {
            case "1":
              registerToken.package_amount = package.semester_1_amount;
              break;
            case "2":
              registerToken.package_amount = package.semester_2_amount;
              break;
            case "3":
              registerToken.package_amount = package.semester_3_amount;
              break;

          }
        }
      }
      Console.WriteLine("Token:" + JsonSerializer.Serialize(registerToken));
      await _context.t_register_token.AddAsync(registerToken);
      await _context.SaveChangesAsync();
      var menus = GetMenus(preRegister != null, lateRegister != null,addDropRegister != null, "TH");
      validateResponse.min_credit = registerToken.min_credit;
      validateResponse.max_credit = registerToken.max_credit;
      validateResponse.menus = menus;
      validateResponse.is_eligible = true;
      validateResponse.register_token_uid = registerToken.register_token_uid;
      validateResponse.message = "";
      return validateResponse;
    }

    private List<register_menu> GetMenus(bool pre, bool late, bool addDrop, string language)
    {
      // if (type == "STUDENT")
      // {
        if (language == "EN")
        {
          var _menus = new List<register_menu>();
          _menus.Add(new register_menu("PRE-REG", "Pre-Registration", pre, true));
          //_menus.Add(new register_menu("LATE-REG","Late-Registration",false,true));
          _menus.Add(new register_menu("LATE-REG", "Late-Registration", late, true));
          _menus.Add(new register_menu("ADD-REMOVE", "Add/Remove Subject", addDrop, true));
          _menus.Add(new register_menu("PAYMENT", "Registration Cost Summary and Payment", true, true));
          _menus.Add(new register_menu("SCHEDULE", "Learning Schedule/ Exam Schedule", false, true));
          _menus.Add(new register_menu("WAITTING", "Waitting List", false, true));
          _menus.Add(new register_menu("SIM", "Registration Cost Simulation", false, true));
          return _menus;
        }
        else
        {
          var _menus = new List<register_menu>();
          _menus.Add(new register_menu("PRE-REG", "ลงทะเบียนล่วงหน้า", pre, true));
          //_menus.Add(new register_menu("LATE-REG","ลงทะเบียนล่าช้า",false,false));
          _menus.Add(new register_menu("LATE-REG", "ลงทะเบียนล่าช้า", late, true));
          _menus.Add(new register_menu("ADD-REMOVE", "เพิ่ม / ถอน", addDrop, true));
          _menus.Add(new register_menu("PAYMENT", "สรุปค่าใช้จ่ายและชำระเงิน", true, true));
          _menus.Add(new register_menu("SCHEDULE", "ตารางเรียน / ตารางสอบ", false, false));
          _menus.Add(new register_menu("WAITTING", "อนุมัติกลุ่มเต็ม", false, true));
          _menus.Add(new register_menu("SIM", "จำลองการลงทะเบียน", false, true));
          return _menus;
        }

      //}
      // else
      // {
      //   if (language == "EN")
      //   {
      //     var _menus = new List<register_menu>();
      //     _menus.Add(new register_menu("PRE-REG", "Pre-Registration", false, true));
      //     _menus.Add(new register_menu("LATE-REG", "Late-Registration", false, true));
      //     _menus.Add(new register_menu("ADD-REMOVE", "Add/Remove Subject", false, true));
      //     _menus.Add(new register_menu("PAYMENT", "Registration Cost Summary and Payment", false, true));
      //     _menus.Add(new register_menu("SCHEDULE", "Learning Schedule/ Exam Schedule", false, false));
      //     _menus.Add(new register_menu("WAITTING", "Waitting List", true, true));
      //     _menus.Add(new register_menu("SIM", "Registration Cost Simulation", false, true));
      //     return _menus;
      //   }
      //   else
      //   {
      //     var _menus = new List<register_menu>();
      //     _menus.Add(new register_menu("PRE-REG", "ลงทะเบียนล่วงหน้า", false, true));
      //     _menus.Add(new register_menu("LATE-REG", "ลงทะเบียนล่าช้า", false, true));
      //     _menus.Add(new register_menu("ADD-REMOVE", "เพิ่ม / ถนน", false, true));
      //     _menus.Add(new register_menu("PAYMENT", "สรุปค่าใช้จ่ายและชำระเงิน", false, true));
      //     _menus.Add(new register_menu("SCHEDULE", "ตารางเรียน / ตารางสอบ", false, false));
      //     _menus.Add(new register_menu("WAITTING", "อนุมัติกลุ่มเต็ม", true, true));
      //     _menus.Add(new register_menu("SIM", "จำลองการลงทะเบียน", false, true));
      //     return _menus;
      //   }
      //
      // }
    }


  }
}
