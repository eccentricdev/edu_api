using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using edu_api.Modules.Register.Databases;
using edu_api.Modules.Register.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using SeventyOneDev.Utilities;

namespace edu_api.Modules.Register.Services
{
  public class AddDropRegisterService
  {
    private readonly Db _context;
    private readonly CacheService _cacheService;
    private readonly CoreRegisterService _coreRegisterService;
    private readonly MessageService _messageService;
    private readonly IHttpClientFactory _httpClientFactory;
    private readonly open_id_setting _openIdSetting;

    public AddDropRegisterService(Db context, CacheService cacheService, MessageService messageService,
      IHttpClientFactory httpClientFactory, IOptions<open_id_setting> openIdSetting,
      CoreRegisterService coreRegisterService)
    {
      _context = context;
      _cacheService = cacheService;
      _messageService = messageService;
      _httpClientFactory = httpClientFactory;
      _openIdSetting = openIdSetting.Value;
      _coreRegisterService = coreRegisterService;
    }

    public async Task<register_detail> AddDropRegisterCheck(string registerTypeCode, Guid registerTokenUid)
    {
      var registerTokenDetail = await _coreRegisterService.GetRegisterToken(registerTypeCode, registerTokenUid);
      var messages = new List<string>();
      var registerCalendar = await _context.t_academic_register_calendar.AsNoTracking().FirstOrDefaultAsync(c =>
        c.academic_register_calendar_uid == registerTokenDetail.academic_register_calendar_uid);
      var lastPaymentDate = registerCalendar?.last_payment_date ?? DateTime.Today;
      var language = "TH";
      var lastPaymentDateText = "";
      if (language == "TH")
      {
        CultureInfo culture = new CultureInfo("th-TH");
        lastPaymentDateText = lastPaymentDate.ToString("dd MMM yyyy", culture);
      }
      else
      {
        CultureInfo culture = new CultureInfo("en-US");
        lastPaymentDateText = lastPaymentDate.ToString("dd MMM yyyy", culture);
      }

      var finalRegisterSubjects = await _context.v_final_register_subject.AsNoTracking().Where(r =>
        r.student_uid == registerTokenDetail.student_uid &&
        r.academic_year_uid == registerTokenDetail.register_academic_year_uid &&
        r.academic_semester_uid == registerTokenDetail.register_academic_semester_uid).ToListAsync();
      if (!finalRegisterSubjects.Any()) throw new Exception("NOT_FOUND_REGISTER");

      var registerSubjects = finalRegisterSubjects
        .Select(finalRegisterSubject => new register_subject(finalRegisterSubject)).ToList();
      var addDropProvisionRegister = await _context.t_provision_register.AsNoTracking().FirstOrDefaultAsync(r =>
        r.student_uid == registerTokenDetail.student_uid &&
        r.academic_year_uid == registerTokenDetail.register_academic_year_uid &&
        r.academic_semester_uid == registerTokenDetail.register_academic_semester_uid &&
        r.register_type_code == "ADDDROP" &&
        r.register_status_id == 1);

      var academicYearCode = "";
      var academicSemesterCode = "";
      if (addDropProvisionRegister != null)
      {
        var addDropRegisterSubjects = await _context.t_provision_register_subject.AsNoTracking()
          .Where(r => r.provision_register_uid == addDropProvisionRegister.provision_register_uid).ToListAsync();
        if (addDropRegisterSubjects.Any())
        {
          registerSubjects.AddRange(addDropRegisterSubjects.Select(s => new register_subject(s)));
        }

        // var registerDetail = new register_detail()
        // {
        //   academic_year_uid =  addDropProvisionRegister.academic_year_uid??Guid.Empty,
        //   academic_semester_uid = addDropProvisionRegister.academic_semester_uid??Guid.Empty,
        //   academic_year_code = academicYearCode,
        //   academic_semester_code = academicSemesterCode,
        //   min_credit = 0,//credit.min_credit,
        //   max_credit = 0,//credit.max_credit,
        //   allow_register = isEligibleToEnroll,
        //   message = _messageService.GetMessage(messages[0], language),
        //   register_subjects = addDropProvisionRegister,
        //   is_readonly = readOnly,
        //   register_type_code = "PRE",
        //   total_credit = (int) provisionRegisterSubjects.Sum(s => s.credit),
        //   last_payment_date = lastPaymentDateText,
        //   provision_register_uid = provisionRegister.provision_register_uid
        // };
        // if (provisionRegisterSubjects.Any())
        // {
        //   var en=await GetEstimationCost(provisionRegisterSubjects,language,registerTypeCode,registerTokenDetail);
        //
        //   //var en=await GetEstimationCost(registerSubjects, studentCode,academicYearId,academicSemesterId,educationTypeId,facultyId,majorId,package,academicYearEntryId,language,provisionRegisterTypeId,studentStatus);
        //   registerDetail.Update(en);
        // }
        //
        // var cost= await  GetEstimationCost(registerSubjects, language, registerTypeCode, registerTokenDetail);
        // registerDetail.Update(cost);
        //
        // return registerDetail;
        var registerDetail = new register_detail()
        {
          academic_year_uid = registerTokenDetail.register_academic_year_uid,
          academic_semester_uid = registerTokenDetail.register_academic_semester_uid,
          min_credit = 0, //credit.min_credit,
          max_credit = 0, //credit.max_credit,
          allow_register = true, // isEligibleToEnroll,
          message = "", //cacheService.GetMessage(_message,language),
          register_subjects = registerSubjects,
          is_readonly = false,
          total_credit = (int)registerSubjects.Where(s => s.register_subject_status_id != 1).Sum(s => s.credit),
          register_type_code = "PRE",
          last_payment_date = lastPaymentDateText
          //messageEn = "Student eligible to enroll",
          //messageTh = "นักศึกษาสามารถลงทะเบียนได้"
        };
        return registerDetail;
      }
      else //No active ADDDROP
      {
        var registerDetail = new register_detail()
        {
          academic_year_uid = registerTokenDetail.register_academic_year_uid,
          academic_semester_uid = registerTokenDetail.register_academic_semester_uid,
          min_credit = 0, //credit.min_credit,
          max_credit = 0, //credit.max_credit,
          allow_register = true, // isEligibleToEnroll,
          message = "", //cacheService.GetMessage(_message,language),
          register_subjects = registerSubjects,
          is_readonly = false,
          total_credit = (int)registerSubjects.Where(s => s.register_subject_status_id != 1).Sum(s => s.credit),
          register_type_code = "PRE",
          last_payment_date = lastPaymentDateText
          //messageEn = "Student eligible to enroll",
          //messageTh = "นักศึกษาสามารถลงทะเบียนได้"
        };
        return registerDetail;
      }



    }

    public async Task<register_detail> AddSubject(string registerTypeCode, Guid registerTokenUid,
      register_subject registerSubject)
    {
      // if (registerSubject.lecture_section == null && registerSubject.lab_section == null)
      // {
      //   throw new Exception("NOT_FOUND_SECTION");
      // }
      //Console.WriteLine("Add subject for " + studentCode + ":" + JsonSerializer.Serialize(selectedSubject));

      //var registerTypeFlag = type;// GetRegisterTypeFlag(type);
      //Console.WriteLine("RegisterTypeFlag:" + registerTypeFlag);
      // if (type=="ADDDROP")
      // {
      //     selectedSubject.status="A";
      // }
      // Status 0,1

      // Flag 0=>Save 1=>Waiting
      // register_subject_status_id
      // 0 => Exist
      // 1 => Add
      // 2 => Drop
      // register_subject_flag_id
      // 0 => Ok
      // 1 => Waiting
      // 3 => Approve
      registerSubject.register_subject_status_id = 1;
      registerSubject.register_subject_flag_id = 0;
      if (registerSubject.lecture_section != null)
      {
        if (registerSubject.lecture_section.is_full ?? false)
        {
          registerSubject.register_subject_flag_id = 1;
        }
      }

      if (registerSubject.lab_section != null)
      {
        if (registerSubject.lab_section.is_full ?? false)
        {
          registerSubject.register_subject_flag_id = 1;
        }
      }

      var registerTokenDetail = await _coreRegisterService.GetRegisterToken(registerTypeCode, registerTokenUid);

      //Check is precheck is call
      var currentRegisterSubjects =
        _cacheService.GetRegisterSubjects(registerTokenDetail
          .student_uid); // cacheService.GetStudentSelectedSubjects(studentCode);
      if ((currentRegisterSubjects.Sum(r => r.credit) ?? 0) + (registerSubject.credit ?? 0) >
          (registerTokenDetail.max_credit ?? 0))
      {
        throw new DataException("OVER_MAX_CREDIT");
      }

      var provisionRegister = await _coreRegisterService.GetProvisionRegister(
        registerTokenDetail.student_uid,
        registerTokenDetail.register_academic_year_uid,
        registerTokenDetail.register_academic_semester_uid,
        registerTypeCode);


      if (!currentRegisterSubjects.Any())
      {
        if (provisionRegister != null)
        {
          var provisionRegisterSubjects =
            await _coreRegisterService.GetProvisionRegisterSubject(provisionRegister.provision_register_uid.Value);
          var subjects = provisionRegisterSubjects.Select(s => new register_subject(s)).ToList();
          if (provisionRegisterSubjects.Any())
          {
            _cacheService.AddRegisterSubjects(registerTokenDetail.student_uid, subjects);
            currentRegisterSubjects = subjects;
          }
        }
      }

      if (currentRegisterSubjects.Any(s => s.year_subject_uid == registerSubject.year_subject_uid))
      {

        throw new DataException("EXISTING_SUBJECT", registerSubject.year_subject_code);
      }

      if (currentRegisterSubjects != null)
      {

        // if (currentRegisterSubjects.Any(s=>s.subject_code==registerSubject.subject_code))
        // {
        //     throw new Exception("EXISTING_SUBJECT");
        // }
        //var currentSelectedSubjects = studentEnrollments;

        //bool overlappedStudyTime = false;
        //string overlappedStudySubjectCode = "";
        var studentStudyTimes = _cacheService.GetStudentStudyTime(registerTokenDetail.student_uid);
        var subjectStudyTimes = _cacheService.GetSubjectStudyTime(registerSubject);
        var overlappedStudyTime = _coreRegisterService.CheckOverlappedStudyTime(studentStudyTimes, subjectStudyTimes);
        if (overlappedStudyTime != "")
        {
          throw new DataException("OVERLAPPED_STUDY_TIME", overlappedStudyTime);
        }

        var studentExamTimes = _cacheService.GetStudentExamTime(registerTokenDetail.student_uid);
        var subjectExamTimes = _cacheService.GetSubjectExamTime(registerSubject);
        var overlappedExamTime = _coreRegisterService.CheckOverlappedExamTime(studentExamTimes, subjectExamTimes);
        if (overlappedExamTime != "")
        {
          throw new DataException("OVERLAPPED_EXAM_TIME", overlappedExamTime);
        }



      }

      var registerSubjects = _cacheService.GetRegisterSubjects(registerTokenDetail.student_uid);

      // else
      // {
      //
      // }
      // if (registerSubjects != null)
      // {
      //   registerSubjects.Add(registerSubject);
      // }
      // else
      // {
      //   registerSubjects = _cacheService.AddNewRegisterSubject(registerTokenDetail.student_uid, registerSubject);
      //
      // }

      if (provisionRegister == null)
      {
        provisionRegister = new t_provision_register(registerTokenDetail.student_uid,
          registerTokenDetail.register_academic_year_uid, registerTokenDetail.register_academic_semester_uid,
          registerSubjects.Count, registerSubjects.Sum(s => s.credit) ?? 0, registerTypeCode, 1);
        //Add provisionRegister
        await _context.t_provision_register.AddAsync(provisionRegister);
        await _context.SaveChangesAsync();



      }

      if (registerSubject.register_subject_status_id == 2)
      {
        //Add waiting list
      }
      else
      {
        if (registerSubject.lecture_section != null)
        {
          var yearSubjectSection = await _context.t_year_subject_section.FirstOrDefaultAsync(y =>
            y.year_subject_section_uid == registerSubject.lecture_section.year_subject_section_uid);
          yearSubjectSection.reserved += 1;
        }

        if (registerSubject.lab_section != null)
        {
          var yearSubjectSection = await _context.t_year_subject_section.FirstOrDefaultAsync(y =>
            y.year_subject_section_uid == registerSubject.lab_section.year_subject_section_uid);
          yearSubjectSection.reserved += 1;
        }

        var tProvisionRegisterSubject =
          new t_provision_register_subject(registerSubject);
        tProvisionRegisterSubject.provision_register_uid = provisionRegister.provision_register_uid;
        await _context.t_provision_register_subject.AddAsync(tProvisionRegisterSubject);
        await _context.SaveChangesAsync();

        _cacheService.AddRegisterSubject(registerTokenDetail.student_uid, registerSubject);
        if (registerSubject.register_subject_flag_id == 1)
        {
          var yearSubjectWaitingList = new t_year_subject_waiting_list()
          {
            student_uid = registerTokenDetail.student_uid,
            academic_year_uid = registerTokenDetail.register_academic_year_uid,
            academic_semester_uid = registerTokenDetail.register_academic_semester_uid,
            year_subject_uid = registerSubject.year_subject_uid,
            lecture_year_subject_section_uid = registerSubject.lecture_section?.year_subject_section_uid,
            lab_year_subject_section_uid = registerSubject.lab_section?.year_subject_section_uid
          };
          await _context.t_year_subject_waiting_list.AddAsync(yearSubjectWaitingList);
          await _context.SaveChangesAsync();
        }
        //tProvisionRegister.provision_register_subjects = tProvisionRegisterSubject;
        //Add provisionRegisterSubject
        //await _dapperDb.AddProvisionRegisterSubject(provisionRegisterSubject);
        //provisionRegister.pro

      }



      //await _context.t_provision_register.AddAsync(provisionRegister);

      return await _coreRegisterService.GetRegisterDetail(registerSubjects, "TH", registerTypeCode,
        registerTokenDetail);


      //return null;




      //if (diSubjectSection.ContainsKey(selectedSubject.subjectCode + selectedSubject))

    }

    public async Task<register_detail> RemoveSubject(string registerTypeCode, Guid registerTokenUid,
      Guid yearSubjectUid)
    {

      var registerTokenDetail = await _coreRegisterService.GetRegisterToken(registerTypeCode, registerTokenUid);
      var currentRegisterSubjects =
        _cacheService.GetRegisterSubjects(registerTokenDetail
          .student_uid); // cacheService.GetStudentSelectedSubjects(studentCode);

      var registerSubject = currentRegisterSubjects.FirstOrDefault(s => s.year_subject_uid == yearSubjectUid);
      if (registerSubject is null) throw new Exception("SUBJECT_NOT_FOUND");

      var provisionRegister = await _coreRegisterService.GetProvisionRegister(registerTokenDetail.student_uid,
        registerTokenDetail.register_academic_year_uid,
        registerTokenDetail.register_academic_semester_uid, registerTypeCode);
      if (provisionRegister is null)
      {
        provisionRegister = new t_provision_register(registerTokenDetail.student_uid,
          registerTokenDetail.register_academic_year_uid, registerTokenDetail.register_academic_semester_uid,
          0, 0, registerTypeCode, 1);
        await _context.t_provision_register.AddAsync(provisionRegister);
        await _context.SaveChangesAsync();
        // if (registerSubject.lecture_section != null)
        // {
        //   var yearSubjectSection = await _context.t_year_subject_section.FirstOrDefaultAsync(y =>
        //     y.year_subject_section_uid == registerSubject.lecture_section.year_subject_section_uid);
        //   yearSubjectSection.reserved += 1;
        // }
        //
        // if (registerSubject.lab_section != null)
        // {
        //   var yearSubjectSection = await _context.t_year_subject_section.FirstOrDefaultAsync(y =>
        //     y.year_subject_section_uid == registerSubject.lab_section.year_subject_section_uid);
        //   yearSubjectSection.reserved += 1;
        // }

      }

      
      if (registerSubject.register_subject_flag_id==0)
      {
        registerSubject.register_subject_status_id = 2;
        var dropRegisterSubject =
          new t_provision_register_subject(registerSubject)
          {
            provision_register_uid = provisionRegister.provision_register_uid
          };
        await _context.t_provision_register_subject.AddAsync(dropRegisterSubject);
        await _context.SaveChangesAsync();
        _cacheService.UpdateRegisterSubject(registerTokenDetail.student_uid, registerSubject);
      }
      else if (registerSubject.register_subject_flag_id == 1)
      {
        var provisionRegisterSubject = await _context.t_provision_register_subject.FirstOrDefaultAsync(p =>
          p.provision_register_uid == provisionRegister.provision_register_uid && p.year_subject_uid == yearSubjectUid);
        _context.t_provision_register_subject.Remove(provisionRegisterSubject);
        await _context.SaveChangesAsync();
        _cacheService.RemoveRegisterSubject(registerTokenDetail.student_uid, registerSubject);
      }
      else
      {
        registerSubject.register_subject_status_id = 0;
        var provisionRegisterSubject = await _context.t_provision_register_subject.FirstOrDefaultAsync(p =>
          p.provision_register_uid == provisionRegister.provision_register_uid && p.year_subject_uid == yearSubjectUid);
        _context.t_provision_register_subject.Remove(provisionRegisterSubject);
        await _context.SaveChangesAsync();
        _cacheService.UpdateRegisterSubject(registerTokenDetail.student_uid, registerSubject);
      }


      // if (registerSubject.register_subject_flag_id == 1)
      // {
      //   var provisionRegisterSubject = await _context.t_provision_register_subject.FirstOrDefaultAsync(p =>
      //     p.provision_register_uid == provisionRegister.provision_register_uid && p.year_subject_uid == yearSubjectUid);
      //   _context.t_provision_register_subject.Remove(provisionRegisterSubject);
      //   await _context.SaveChangesAsync();
      //   var remainRegisterSubjects =
      //     _cacheService.RemoveRegisterSubject(registerTokenDetail.student_uid, removeRegisterSubject);
      // }
      // else
      // {
      //   
      // }






      //await _context.t_provision_register.AddAsync(provisionRegister);
      currentRegisterSubjects =
        _cacheService.GetRegisterSubjects(registerTokenDetail
          .student_uid); // cacheService.GetStudentSelectedSubjects(studentCode);
      return await _coreRegisterService.GetRegisterDetail(currentRegisterSubjects, "TH", registerTypeCode,
        registerTokenDetail);




      //if (diSubjectSection.ContainsKey(selectedSubject.subjectCode + selectedSubject))

    }
  }
}