using System;
using SeventyOneDev.Utilities;
using SeventyOneDev.Utilities.Attributes;

namespace edu_api.Modules.StudentGroup.Databases.Models
{
  public class group_manage
  {
    public Guid faculty_curriculum_uid { get; set; }
    public int major_id { get; set; }
    public Guid academic_year_uid { get; set; }
    public int amount_per_group { get; set; }
    public Boolean is_random { get; set; }
    public Boolean is_both_gender { get; set; }
    public string prefix { get; set; }
    public string suffix { get; set; }
  }

}
