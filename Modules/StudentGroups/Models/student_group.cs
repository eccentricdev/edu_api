using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;
using SeventyOneDev.Utilities;
using SeventyOneDev.Utilities.Attributes;

namespace edu_api.Modules.StudentGroup.Databases.Models
{
  public class student_group:base_table
  {
    [Key]
    public Guid? student_group_uid { get; set; }
    public Guid? student_uid { get; set; }
    public Guid? group_uid { get; set; }
  }

  [GeneratedUidController("api/student_group/student_group")]
  public class t_student_group : student_group
  {
    [JsonIgnore]
    public v_group group { get; set; }
  }

  public class v_student_group : student_group
  {
    [JsonIgnore]
    public v_group group { get; set; }
  }

}
