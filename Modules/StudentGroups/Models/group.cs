using System;
using System.ComponentModel.DataAnnotations;
using System.Security.Permissions;
using System.Text.Json.Serialization;
using SeventyOneDev.Utilities;
using System.Collections.Generic;
using SeventyOneDev.Utilities.Attributes;

namespace edu_api.Modules.StudentGroup.Databases.Models
{
  public class group:base_table
  {
    [Key]
    public Guid? group_uid { get; set; }
    public string group_name { get; set; }
    public string group_name_en { get; set; }
    public string group_code { get; set; }
    public string group_image { get; set; }
    public string group_prefix { get; set; }
    public string group_suffix { get; set; }
    public Guid? instructor_uid { get; set; }
    public Guid? faculty_curriculum_uid { get; set; }
    public int? major_id { get; set; }
    public Guid? academic_year_uid { get; set; }
  }

  [GeneratedUidController("api/student_group/group")]
  public class t_group : group
  {
    [Include]
    public List<t_student_group> student_groups { get; set; }
  }

  public class v_group : group
  {

    [Include]
    public List<v_student_group> student_groups { get; set; }
  }

}
