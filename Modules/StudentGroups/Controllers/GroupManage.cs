using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics.CodeAnalysis;
using System.Threading.Tasks;
using edu_api.Modules.Calendars.Databases.Models;
using edu_api.Modules.StudentGroup.Databases.Models;
using edu_api.Modules.StudentGroup.Services;
using Microsoft.AspNetCore.Mvc;
using SeventyOneDev.Utilities;

namespace edu_api.Modules.StudentGroup.Controllers
{
  [Route("api/student_group/group", Name = "group")]
  public class GroupController 
  {
    private readonly GroupManageService _groupService;
    public GroupController(GroupManageService entityUidService) 
    {
      _groupService = entityUidService;
    }

    [HttpPost("new_group_auto")]
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    public async Task<ActionResult<List<v_group>>> NewAuto([FromBody, Required] group_manage group_manage)
    {
      return await _groupService.NewAuto(group_manage);
    }  
  }
}



