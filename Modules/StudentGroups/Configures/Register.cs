// using edu_api.Modules.StudentGroup.Services;
using edu_api.Modules.StudentGroup.Services;
using Microsoft.Extensions.DependencyInjection;

namespace edu_api.Modules.StudentGroup.Configures
{
  public static class StudentGroupCollectionExtension
  {
    public static IServiceCollection AddStudentGroupServices(this IServiceCollection services)
    {
      //services.AddAutoMapper(typeof(ProfileMapping));
      // services.AddScoped<YearSubjectService>();
      services.AddScoped<GroupManageService>();
      return services;
    }
  }
}
