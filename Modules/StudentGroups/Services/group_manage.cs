using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using edu_api.Modules.StudentGroup.Databases.Models;
using Microsoft.EntityFrameworkCore;
using rsu_common_api.models;
using SeventyOneDev.Utilities;

namespace edu_api.Modules.StudentGroup.Services
{
  public class GroupManageService:EntityUidService<t_group,v_group>
  {
    private readonly Db _context;
    public GroupManageService(Db context) : base(context)
    {
      _context = context;
    }

    public async Task<List<v_group>> NewAuto(group_manage _group_manage)
    {
      var groupcheck = await _context.v_group.AsTracking().Where(c=>
        c.faculty_curriculum_uid == _group_manage.faculty_curriculum_uid &&
        c.academic_year_uid == _group_manage.academic_year_uid &&
        c.major_id == _group_manage.major_id
      ).ToListAsync();
      if(groupcheck.Count()>0){
        var students = _context.v_student.AsTracking().Where(c=>
          c.faculty_curriculum_uid == _group_manage.faculty_curriculum_uid &&
          c.entry_academic_year_uid == _group_manage.academic_year_uid &&
          c.major_id == _group_manage.major_id
        ).ToList();
        var groupNumber = 1;
        if(_group_manage.is_both_gender==true){
          groupNumber = createGroup(_group_manage,students,groupNumber);
        }else{
          var maleStudent = students.Where(c=>c.gender_id == 1);
          var femaleStudent = students.Where(c=>c.gender_id == 2);
          var noGenderStudent = students.Where(c=>c.gender_id == 0);
          groupNumber = createGroup(_group_manage,maleStudent.ToList(),groupNumber);
          groupNumber = createGroup(_group_manage,femaleStudent.ToList(),groupNumber);
          groupNumber = createGroup(_group_manage,noGenderStudent.ToList(),groupNumber);
        }
        return await _context.v_group.AsNoTracking().Where(c=>
          c.faculty_curriculum_uid == _group_manage.faculty_curriculum_uid &&
          c.academic_year_uid == _group_manage.academic_year_uid &&
          c.major_id == _group_manage.major_id
        ).ToListAsync();
      }else{
        return groupcheck;
      }
      
    }
    public int createGroup(group_manage _group_manage,List<v_student> students,int groupNumber)
    {
      
        if(_group_manage.is_random==true){
          students.OrderBy(r => Guid.NewGuid());
        }
        var count=1;
        var newGroup = new t_group(){};
        foreach(var student in students){
          if(count==1){
            newGroup.group_name = "-";
            newGroup.group_name_en = "-";
            newGroup.group_code = _group_manage.prefix+groupNumber+_group_manage.suffix;
            newGroup.group_image = "-";
            newGroup.group_prefix = _group_manage.prefix;
            newGroup.group_suffix = _group_manage.suffix;
            newGroup.faculty_curriculum_uid = _group_manage.faculty_curriculum_uid;
            newGroup.major_id = _group_manage.major_id;
            newGroup.academic_year_uid = _group_manage.academic_year_uid;
          }
          newGroup.student_groups.Add(new t_student_group(){
            student_uid = student.student_uid
          });

          var studentGroup = new t_student_group();
          if(count>_group_manage.amount_per_group){
            count=1;
            groupNumber++;
            _context.t_group.Add(newGroup);
            newGroup.group_uid = newGroup.group_uid;
            _context.SaveChanges();
            newGroup = new t_group();
          }
        }
        return groupNumber;
      
    }
  }
}
