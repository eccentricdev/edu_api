using edu_api.Modules.StudentGroup.Databases.Models;
using Microsoft.EntityFrameworkCore;

namespace SeventyOneDev.Utilities
{
  public partial class Db : DbContext
  {
    public DbSet<t_student_group> t_student_group {get;set;}
    public DbSet<v_student_group> v_student_group {get;set;}
    public DbSet<t_group> t_group {get;set;}
    public DbSet<v_group> v_group {get;set;}

    private void OnStudentGroupModelCreating(ModelBuilder builder, string schema)
    {
      builder.Entity<t_student_group>().ToTable("student_group", schema);
      builder.Entity<v_student_group>().ToView("v_student_group", schema);

      builder.Entity<t_group>().ToTable("group", schema);
      builder.Entity<v_group>().ToView("v_group", schema);
      builder.Entity<v_student_group>().HasOne(a => a.group).WithMany(a => a.student_groups)
        .HasForeignKey(a => a.group_uid).OnDelete(DeleteBehavior.Cascade);
    }
  }
}
