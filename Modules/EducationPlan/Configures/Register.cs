using edu_api.Modules.EducationPlan.Services;
using edu_api.Modules.InstructorEvaluations.Services;
using Microsoft.Extensions.DependencyInjection;

namespace edu_api.Modules.EducationPlan.Configures
{
  public static class EducationPlanCollectionExtension
  {
    public static IServiceCollection AddEducationPlanServices(this IServiceCollection services)
    {
      services.AddScoped<AdmissionPlanRegisterService>();

      return services;
    }
  }
}
