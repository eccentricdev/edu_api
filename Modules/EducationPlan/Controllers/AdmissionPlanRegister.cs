using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Claims;
using System.Text.Json;
using System.Threading.Tasks;
using edu_api.Modules.EducationPlan.Databases;
using edu_api.Modules.EducationPlan.Models;
using edu_api.Modules.EducationPlan.Services;
using edu_api.Modules.Register.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using rsu_common_api.models;
using rsu_common_api.Modules.Profiles.Services;
using SeventyOneDev.Utilities;

namespace edu_api.Modules.EducationPlan.Controllers
{
  [Route("api/education_plan/admission_plan_register",Name="admission_plan_register")]
  public class AdmissionPlanRegisterController:Controller
  {
    private readonly AdmissionPlanRegisterService _admissionPlanRegisterService;
    private readonly Db _db;
    private readonly IamService _iamService;
    public AdmissionPlanRegisterController(AdmissionPlanRegisterService admissionPlanRegisterService,Db db,IamService iamService)
    {
      _admissionPlanRegisterService = admissionPlanRegisterService;
      _db = db;
      _iamService = iamService;

    }
    [HttpGet("{academic_year_code}/{academic_semester_code}/{major_code}")]
    public async Task<ActionResult<v_education_plan>> GetEducationPlanForNewStudent([FromRoute]string academic_year_code,[FromRoute]string academic_semester_code,[FromRoute]string major_code)
    {

      var getResult = await _admissionPlanRegisterService.GetEducationPlanForNewStudent(academic_year_code,academic_semester_code,major_code);
      if (getResult==null)
      {
        return NotFound();
      }
      else
      {
        return Ok(getResult);
      }
    }
    [HttpPost]
    public async Task<ActionResult> NewEducationPlan([FromBody]plan_register_request planRegisterRequest)
    {
      try
      {
        if (!planRegisterRequest.education_plan.total_amount.HasValue)
        {
          var credits = planRegisterRequest.education_plan.credits?.Sum(c => c.amount);
          var debits = planRegisterRequest.education_plan.debits?.Sum(c => c.amount);
          planRegisterRequest.education_plan.total_amount = (credits ?? 0 - debits ?? 0);
        }
        var newResult = await _admissionPlanRegisterService.NewPlanRegister(planRegisterRequest.applicant,planRegisterRequest.education_plan,User.Identity as ClaimsIdentity);
        
        return Created("/api/register/plan_register/" + newResult.plan_register_uid.ToString(),newResult);
      }
      // catch(DuplicateException ex)
      // {
      //     response = ApiHelper.ApiError<v_education_plan>(ex.ErrorId,ex.Message);
      // }
      catch(Exception ex) {
        Console.WriteLine("Create plan_register failed: " + ex.Message);
        return BadRequest();
      }

    }

    [HttpPut("paid/{bill_no}")]
    public async Task<ActionResult<student>> PaidEducationPlan([FromRoute]string bill_no)
    {
      var planRegister = await _db.t_plan_register.FirstOrDefaultAsync(a => a.bill_no == bill_no);
      if (planRegister != null)
      {
        planRegister.is_paid = true;
        var applicantApplyView = await _db.v_applicant_apply.AsNoTracking()
          .FirstOrDefaultAsync(a => a.applicant_apply_id == planRegister.applicant_apply_id);
        if (applicantApplyView != null)
        {
          var _applicantApply = new applicant_apply();
          if (applicantApplyView.college_faculty_uid.HasValue && applicantApplyView.faculty_curriculum_uid.HasValue)
          {
            applicantApplyView.CloneTo(_applicantApply);

            var student = await NewStudent(_applicantApply);
            // var applicantApplyTable = await _db.t_applicant_apply.AsNoTracking()
            //   .FirstOrDefaultAsync(a => a.applicant_apply_id == planRegister.applicant_apply_id);
            // applicantApplyTable.student_code = student.student_code;
            // applicantApplyTable.rsu_email = student.rsu_email;
            // await _db.SaveChangesAsync();
            
            
            
            // HttpClient httpClient = new HttpClient();
            // var res = await httpClient.PutAsync(
            //   "https://rsu.71dev.com/app-api/api/app/applicant_applys/complete/" + planRegister.applicant_apply_id,
            //   null!);
            // if (res.StatusCode != HttpStatusCode.OK)
            // {
            //   Console.WriteLine("Error update status");
            // }

            return Ok(student);
          }

        }

        
        //planRegister.applicant_apply_id
        // var finalRegister = new t_final_register(provisionRegister);
        // var provisionRegisterSubjects = await _db.t_provision_register_subject
        //   .Where(p => p.provision_register_uid == provisionRegister.provision_register_uid).ToListAsync();
        // finalRegister.final_register_subjects.AddRange(provisionRegisterSubjects.Select(p=>new t_final_register_subject(p)));
        // await _db.AddAsync(finalRegister);
        // await _db.SaveChangesAsync();
        // return Ok();
      }
      return BadRequest();
    }

    private async Task<student> NewStudent([FromBody]applicant_apply applicantApply)
    {

      //Console.WriteLine("Create new student:" + JsonSerializer.Serialize(student));

      // var _code = await _db.t_generate_code.FirstOrDefaultAsync(g => g.code == "STD_CODE");
      // var _next = _code.current_number.Value + 1;
      // var _studentCode = _code.data1 + _next.ToString("D" + _code.digit);
      //student.student_code = _studentCode;
      // if (await _db.t_student.AnyAsync(c =>
      //       c.student_code == _studentCode))
      // {
      //   Console.WriteLine("Create student error: Duplicate student code");
      //   throw new Exception("DATA_EXISTS");
      // }
      //var lastStudentCode = _db.t_student.AsNoTracking().Where(s=>s.student_code.StartsWith(applicantApply.academic_year_code.Substring(2,2))) .OrderByDescending(s => s.student_code).FirstOrDefault()?.student_code;
      var lastStudentCode = _db.t_student.AsNoTracking().Where(s=>s.student_code.StartsWith("62")) .OrderByDescending(s => s.student_code).FirstOrDefault()?.student_code;

      // var collegeFaculty = _db.t_college_faculty.AsNoTracking()
      //   .FirstOrDefault(s => s.college_faculty_code == applicantApply.college_faculty_code);
      // var facultyCurriculum = _db.t_faculty_curriculum.AsNoTracking()
      //   .FirstOrDefault(f => f.faculty_curriculum_code == applicantApply.faculty_curriculum_code);
      var academicYear = await _db.t_academic_year.AsNoTracking()
        .FirstOrDefaultAsync(a => a.academic_year_code == applicantApply.academic_year_code);
      var academicSemester = await _db.t_academic_semester.AsNoTracking().FirstOrDefaultAsync(a =>
        a.academic_year_uid == academicYear.academic_year_uid &&
        a.academic_semester_code == applicantApply.academic_semester_code);

      var applicant = await _db.v_applicant.AsNoTracking()
        .FirstOrDefaultAsync(a => a.applicant_id == applicantApply.applicant_id);
      var student = new t_student()
      {
        student_code = (int.Parse(lastStudentCode)+1).ToString(),
        first_name_th = applicantApply.first_name_th,
        last_name_th = applicantApply.last_name_th,
        display_name_th = applicantApply.display_name_th,
        first_name_en = applicantApply.first_name_en,
        middle_name_en = applicantApply.middle_name_en,
        last_name_en = applicantApply.last_name_en,
        display_name_en = applicantApply.display_name_en,
        college_faculty_uid = applicantApply.college_faculty_uid,
        faculty_curriculum_uid = applicantApply.faculty_curriculum_uid,
        entry_academic_year_uid = academicYear.academic_year_uid,
        entry_semester_uid = academicSemester.academic_semester_uid,
        birth_date = applicant.date_of_birth,
        gender_id = applicant.gender_id,
        prefix_id = applicant.prefix_id,
        citizen_id = applicant.personal_id,
        mobile_no = applicant.mobile,
        personal_email = applicant.email,
        entry_program_id = applicantApply.program_id,
        
        
      };
      if (string.IsNullOrEmpty(student.display_name_th))
      {
        student.display_name_th = student.first_name_th + " " + student.last_name_th;
      }
      if (string.IsNullOrEmpty(student.display_name_en))
      {
        student.display_name_en = student.first_name_en + " " + student.last_name_en;
      }
      await _db.t_student.AddAsync (student);
      await _db.SaveChangesAsync();
      try
      {
        var iamResult = await _iamService.AddOrUpdateIamUser(student);
        var result=await CreateStudentEmail(student.student_code, student.first_name_th, student.last_name_th,student.display_name_th, "Password@1");
        //var userResult = await CreateActiveDirectoryUser(student.student_code, student.first_name_th, student.last_name_th,student.display_name_th, "Password@1");
        
      }
      catch (Exception ex)
      {
        Console.WriteLine("Create email error:" + ex.Message);
      }
      student.rsu_email = student.student_code + "@avitechproject.com";
      await _db.SaveChangesAsync();
      //var email = stu + "@avitechproject.com";

      return student;
    }

    private async Task<bool> CreateStudentEmail(string studentCode,string firstNameTh,string lastNameTh,string displayNameTh,string password)
    {
      var httpClient = new HttpClient();
      var mailUser = new mail_user()
      {
        primaryEmail = studentCode + "@avitechproject.com",
        name = new mail_user_name() {givenName = firstNameTh,familyName = lastNameTh, fullName = displayNameTh},
        password = password,
        changePasswordAtNextLogin = false
      };
      var body = new ByteArrayContent(JsonSerializer.SerializeToUtf8Bytes(mailUser));
       body.Headers.ContentType = new MediaTypeHeaderValue("application/json");
      //body.Headers.Add(HttpRequestHeader.ContentType.ToString(), "application/json");
      httpClient.DefaultRequestHeaders.Accept.Clear();
      httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
      var result = await httpClient.PostAsync("https://rsu.71dev.com/mail-connector/api/mail/gmail", body);
      if (result.StatusCode == HttpStatusCode.OK)
      {
        var response = await result.Content.ReadAsStringAsync();

        return true;
      }
      else
      {
        return false;
      }
      // {
      //   "primaryEmail": "string",
      //   "name": {
      //     "givenName": "string",
      //     "familyName": "string",
      //     "fullName": "string"
      //   },
      //   "password": "string",
      //   "changePasswordAtNextLogin": true
      // }
    }

    private async Task<bool> CreateActiveDirectoryUser(string studentCode, string firstNameTh, string lastNameTh,
      string displayNameTh, string password)
    {
      var httpClient = new HttpClient();
      var activeDirectoryUser = new active_directory_user()
      {
        student_code = studentCode,
        first_name = firstNameTh,
        last_name = lastNameTh,
        password = password,
        email = studentCode + "@avitechproject.com",
        login_name = studentCode,
        ou = "Students"
      };
      var body = new ByteArrayContent(JsonSerializer.SerializeToUtf8Bytes(new List<active_directory_user>()
        { activeDirectoryUser }));
      body.Headers.ContentType = new MediaTypeHeaderValue("application/json");
      //body.Headers.Add(HttpRequestHeader.ContentType.ToString(), "application/json");
      httpClient.DefaultRequestHeaders.Accept.Clear();
      httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
      var result = await httpClient.PostAsync("http://111.223.48.141/api/ad/create_users", body);
      if (result.StatusCode == HttpStatusCode.OK)
      {
        return true;
      }
      else
      {
        return false;
      }

    }
    
  }
}
