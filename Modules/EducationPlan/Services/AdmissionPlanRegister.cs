using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Text.Json;
using System.Threading.Tasks;
using edu_api.Modules.EducationPlan.Databases;
using edu_api.Modules.EducationPlan.Models;
using edu_api.Modules.Register.Databases;
using edu_api.Modules.Register.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using payment_core.Models;
using payment_core.Utilities;
using SeventyOneDev.Utilities;

namespace edu_api.Modules.EducationPlan.Services
{
  public class AdmissionPlanRegisterService
  {
    private readonly Db _context;
    private readonly IHttpClientFactory _httpClientFactory;
    private readonly open_id_setting _openIdSetting;
    public AdmissionPlanRegisterService(Db context,IHttpClientFactory httpClientFactory,IOptions<open_id_setting> openIdSetting)
    {
      _context = context;
      _httpClientFactory = httpClientFactory;
      _openIdSetting = openIdSetting.Value;
    }
    public async Task<v_education_plan> GetEducationPlanForNewStudent(string academic_year_code,string academic_semester_code,string major_code)
    {
      var academicYear = await _context.t_academic_year.AsNoTracking()
        .FirstOrDefaultAsync(a => a.academic_year_code == academic_year_code);
      var academicSemester = await _context.t_academic_semester.AsNoTracking().FirstOrDefaultAsync(a =>
        a.academic_year_uid == academicYear.academic_year_uid && a.academic_semester_code == academic_semester_code);
      var facultyCurriculum = await _context.t_faculty_curriculum.AsNoTracking()
        .FirstOrDefaultAsync(f => f.faculty_curriculum_code == major_code);
      var planCurriculum = await _context.v_education_plan_curriculum.Where(p=>p.academic_year_uid==academicYear.academic_year_uid && p.academic_semester_uid==academicSemester.academic_semester_uid && p.curriculum_uid==facultyCurriculum.faculty_curriculum_uid).FirstOrDefaultAsync();
      var plan = await _context.v_education_plan.Where(p=>p.education_plan_uid==planCurriculum.education_plan_uid).FirstOrDefaultAsync();
      var subjects = await _context.v_education_plan_subject
        // .Include(p=>p.lab_study_subject_section)
        // .Include(p=>p.lecture_study_subject_section)
        .Where(p=>p.education_plan_uid==plan.education_plan_uid).ToListAsync();
      var fees = await _context.v_education_plan_fee.Where(p=>p.education_plan_uid==plan.education_plan_uid).ToListAsync();
      plan.education_plan_subjects = subjects;
      plan.education_plan_fees = fees;
      List<plan_register_payment> credits = new List<plan_register_payment>();
      credits.Add(new plan_register_payment(){name="ค่าหน่วยกิต",amount=plan.credit_amount??0});
      credits.AddRange(fees.Select(f=>new plan_register_payment(){name=f.fee_name_th,amount=f.fee_amount??0}));
      plan.credits = credits;
      return plan;
      // return await _context.v_education_plan.FirstOrDefaultAsync(a=>a.education_plan_id==id);
      // documentTypes=documentTypes.FirstOrDefault().OrderBy(c=>c.educationPlan_id);
      // return documentTypes;
    }

    public async Task<t_plan_register> NewPlanRegister(applicant applicant, v_education_plan educationPlan,
      ClaimsIdentity claimsIdentity)
    {
      var planRegister = new t_plan_register(applicant.applicant_apply_id??0, educationPlan);
      await _context.t_plan_register.AddAsync(planRegister);
      await _context.SaveChangesAsync();
      planRegister = await CreatePlanRegisterBill(applicant, educationPlan,planRegister.plan_register_uid.Value);
      
      return planRegister;
    }

    // public async Task<bool> PaidPlanRegister(int plan_register_id,string student_code,ClaimsIdentity claimsIdentity)
        // {
        //     var _planRegister = await _context.plan_register.FirstOrDefaultAsync(p=>p.plan_register_id==plan_register_id);
        //     var _finalRegister = new t_final_register(_planRegister);
        //     _finalRegister.student_code = student_code;
        //     var _finalRegisterDetails = await _context.plan_register_subject.Where(p=>p.plan_register_id == plan_register_id).Select(p=>new t_final_register_subject(p)).ToListAsync();
        //     _finalRegister.final_register_subjects = _finalRegisterDetails;
        //     _context.Add(_finalRegister);
        //     await _context.SaveChangesAsync();
        //     return true;
        // }
        private async Task<t_plan_register> CreatePlanRegisterBill(applicant applicant, v_education_plan educationPlan,
       Guid planRegisterUid)
        {
          t_plan_register planRegister = null;
       try
       {
         
         //var student = await _context.v_student.AsNoTracking().FirstOrDefaultAsync(s => s.student_uid == studentUid);
         var newBill = new bill_request(
           DateTime.Today,
           15,
           1,
           null,
           applicant.display_name_th,
           "",
           // applicant.address1 + " " + (string.IsNullOrEmpty(applicant.soi) ? "" : applicant.soi) + " " +
           // (string.IsNullOrEmpty(applicant.road) ? "" : applicant.road) + " " +
           // (string.IsNullOrEmpty(applicant.sub_district_name) ? "" : applicant.sub_district_name) + " " +
           // (string.IsNullOrEmpty(applicant.district_name) ? "" : applicant.district_name) + " " +
           // (string.IsNullOrEmpty(applicant.province_name) ? "" : applicant.province_name),
           "",
           applicant.id_card,
           null,
           null,
           applicant.email,
           applicant.mobile_no,
           DateTime.Today.AddDays(15),
           "",
           educationPlan.total_amount??0,
           0, 0,
           educationPlan.total_amount??0,
           //applicantApply.payment_amount.Value, 0, 0,
           //applicantApply.payment_amount.Value,
           "", 0);
         // {
         //   customer_code = applicantApply.application_id.ToString(),
         //   customer_name = applicant.first_name_th + " " + applicant.last_name_th,
         //   customer_address = applicant.address1,
         //   bill_date = DateTime.Today,
         //   payment_due_date = DateTime.Today.AddDays(14),
         //   total_amount = applicantApply.payment_amount,
         //   department_code = "20"
         //
         // };

         // var billItems = registerSummary.subjects.Select((r,i) =>
         //     new bill_request_item()
         //     {
         //        row_order = i+1,
         //        item_code = r.year_subject_code,
         //        item_name_th = r.year_subject_name_th,
         //        item_name_en = r.year_subject_name_th,
         //        unit_price = r.total_amount,
         //        quantity = 1,
         //        discount_type_id = 0,
         //        discount_amount = 0,
         //        before_vat_amount = r.total_amount,
         //        vat_amount = 0,
         //        amount = r.total_amount,
         //        sub_item1_name = "LECTURE",
         //        sub_item1_quantity = 1,
         //        sub_item1_unit_price = r.total_lecture_amount,
         //        sub_item1_amount = r.total_lecture_amount,
         //        sub_item2_name = "LAB",
         //        sub_item2_quantity = 1,
         //        sub_item2_unit_price = r.total_lab_amount,
         //        sub_item2_amount = r.total_lab_amount,
         //        sub_item3_name = "CREDIT",
         //        sub_item3_quantity = r.lecture_credit_amount,
         //        sub_item3_unit_price = 0,
         //        sub_item3_amount = 0,
         //        item_ext1_name = r.lecture_section?.section_code,
         //        item_ext2_name = r.lab_section?.section_code,
         //        share1_percent = 50,
         //        share2_percent = 50
         //     }
         //   // new bill_request_item(
         //   //   0,
         //   //   r.year_subject_code,
         //   //   "",
         //   //   r.year_subject_name_en,
         //   //   r.year_subject_name_th,
         //   //   r.total_amount.Value,
         //   //   0,
         //   //   0,
         //   //   1,
         //   //   r.total_amount.Value,
         //   //   0,
         //   //   r.total_amount.Value)
         // ).ToList();


         newBill.bill_items = educationPlan.ToBillRequestItem();
         var iamHttpClient = _httpClientFactory.CreateClient("open_id");
         //Console.WriteLine("Client ID:" + _openIdSetting.client_id);
         var dict = new Dictionary<string, string>
         {
           { "grant_type", "client_credentials" },
           { "scope", string.Join(" ", _openIdSetting.scopes) },
           { "client_id", _openIdSetting.client_id },
           { "client_secret", _openIdSetting.client_secret }
         };
         var req = new HttpRequestMessage(HttpMethod.Post, _openIdSetting.authority + "/connect/token")
           { Content = new FormUrlEncodedContent(dict) };
         var iamResponse = await iamHttpClient.SendAsync(req);
         if (iamResponse.StatusCode == HttpStatusCode.OK)
         {
           var iamResult = await iamResponse.Content.ReadAsByteArrayAsync();
           var openIdToken = JsonSerializer.Deserialize<open_id_token>(iamResult);
           //Console.WriteLine("OpenID token:" + JsonSerializer.Serialize(openIdToken));
           var httpClient = _httpClientFactory.CreateClient("ar_payment");
           JsonSerializerOptions options = new JsonSerializerOptions();
           options.Converters.Add(new DateTimeConverterUsingDateTimeParse());
           options.IgnoreNullValues = true;
           Console.WriteLine("Bill Request:" + JsonSerializer.Serialize(newBill, options));
           Console.WriteLine("Token:" + openIdToken.access_token);
           var data = new ByteArrayContent(JsonSerializer.SerializeToUtf8Bytes(newBill, options));
           var httpRequestMessage = new HttpRequestMessage
           {
             Method = HttpMethod.Post,
             RequestUri = new Uri(httpClient.BaseAddress + "v1/accounts_receivables/bills")
           };

           httpRequestMessage.Headers.Add("Authorization", "Bearer " + openIdToken.access_token);
           httpRequestMessage.Headers.Add(HttpRequestHeader.ContentType.ToString(), "application/json");
           HttpResponseMessage res = null;
           data.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");
           httpRequestMessage.Content = data;
           res = await httpClient.SendAsync(httpRequestMessage);

           if (res.StatusCode == HttpStatusCode.Created)
           {
             planRegister =
               await _context.t_plan_register.FirstOrDefaultAsync(p =>
                 p.plan_register_uid == planRegisterUid);
             var result = await res.Content.ReadAsByteArrayAsync();
             var createdBill = JsonSerializer.Deserialize<bill>(result);
             Console.WriteLine("Bill:" + JsonSerializer.Serialize(createdBill));
             planRegister.bill_no = createdBill.bill_no;
             //applicantApply.application_invoice_due_date = createdBill.payment_due_date;
             planRegister.bill_uid = createdBill.bill_uid;
             //createdBill.bill_url
             await _context.SaveChangesAsync();
           }
           else
           {
             Console.WriteLine("Create bill:" + res.StatusCode.ToString());
           }
         }


       }
       catch (Exception e)
       {
         Console.WriteLine(e);
         //return null;

       }

       return planRegister;
     }
  }
}
