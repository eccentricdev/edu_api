namespace edu_api.Modules.EducationPlan.Models
{
  public class applicant
  {
    public int? applicant_apply_id { get; set; }
    public string id_card { get; set; }
    public string display_name_th { get; set; }
    public string display_name_en { get; set; }
    public string email { get; set; }
    public string mobile_no { get; set; }
  }
}
