using edu_api.Modules.EducationPlan.Databases;

namespace edu_api.Modules.EducationPlan.Models
{
  public class plan_register_request
  {
    public applicant applicant { get; set; }
    public v_education_plan education_plan { get; set; }
  }
}
