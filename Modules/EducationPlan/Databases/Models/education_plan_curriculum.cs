using System;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace edu_api.Modules.EducationPlan.Databases
{
  public class education_plan_curriculum
  {
    [Key]
    public Guid? education_plan_curriculum_uid {get;set;}
    public Guid? education_plan_uid {get;set;}
    public Guid? curriculum_uid { get; set; }

  }

  public class t_education_plan_curriculum : education_plan_curriculum
  {
    [JsonIgnore]
    public t_education_plan education_plan { get; set; }
  }
  public class v_education_plan_curriculum : education_plan_curriculum
  {
    public string curriculum_name_th {get;set;}
    public Guid? academic_year_uid { get; set; }
    public Guid? academic_semester_uid { get; set; }
    public short? quota {get;set;}
    public short? reserve {get;set;}
    public short? confirm {get;set;}
    [JsonIgnore]
    public v_education_plan education_plan { get; set; }
  }
}
