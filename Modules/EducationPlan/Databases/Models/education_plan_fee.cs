using System;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace edu_api.Modules.EducationPlan.Databases
{
  public class education_plan_fee
  {
    [Key]
    public Guid? education_plan_fee_uid {get;set;}
    public Guid? education_plan_uid {get;set;}
    public Guid? register_fee_uid {get;set;}
    public Guid? fee_uid { get; set; }
    public decimal? fee_amount { get; set; }

  }

  public class t_education_plan_fee : education_plan_fee
  {
    [JsonIgnore]
    public t_education_plan education_plan { get; set; }
  }
  public class v_education_plan_fee : education_plan_fee
  {
    public string fee_code {get;set;}
    public string fee_name_th {get;set;}
    public string fee_name_en {get;set;}
    [JsonIgnore]
    public v_education_plan education_plan { get; set; }
  }
}
