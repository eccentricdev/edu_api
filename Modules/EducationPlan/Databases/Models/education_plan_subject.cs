using System;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;
using SeventyOneDev.Utilities;

namespace edu_api.Modules.EducationPlan.Databases
{
  public class education_plan_subject:base_table
  {
    [Key]
    public Guid? education_plan_subject_id {get;set;}
    public Guid? education_plan_uid {get;set;}
    public Guid? subject_year_uid {get;set;}
    public Guid? lecture_study_subject_section_uid {get;set;}
    public Guid? lab_study_subject_section_uid {get;set;}
    // [MaxLength(10)]
    // public string lecture_section {get;set;}
    // [MaxLength(10)]
    // public string lab_section {get;set;}
    // public string academic_semester_code {get;set;}

  }

  public class t_education_plan_subject : education_plan_subject
  {
    [JsonIgnore]
    public t_education_plan education_plan { get; set; }
  }

  public class v_education_plan_subject : education_plan_subject
  {
    public string year_subject_code {get;set;}
    public string year_subject_name_th {get;set;}
    public string year_subject_name_en {get;set;}
    public decimal? lecture_amount {get;set;}
    public decimal? lab_amount {get;set;}
    public string lecture_section_code {get;set;}
    public string lab_section_code {get;set;}
    public short? credit {get;set;}
    public string study_time_text_en {get;set;}
    [JsonIgnore]
    public v_education_plan education_plan { get; set; }
  }
}
