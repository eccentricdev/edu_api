using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using edu_api.Modules.EducationPlan.Models;
using payment_core.Models;
using SeventyOneDev.Utilities;
using SeventyOneDev.Utilities.Attributes;

namespace edu_api.Modules.EducationPlan.Databases
{
  public class education_plan:base_table
  {
    [Key]
    public Guid? education_plan_uid {get;set;}
    public Guid? faculty_curriculum_uid {get;set;}
    public Guid? academic_year_uid { get; set; }
    public Guid? academic_semester_uid {get;set;}
    public string education_plan_code {get;set;}
    public string education_plan_name_th {get;set;}
    public string education_plan_name_en {get;set;}
    public Guid? advisor_uid { get; set; }
    public string description {get;set;}
    public string remark {get;set;}
    public short? total_subject {get;set;}
    public short? total_credit {get;set;}
    public decimal? credit_amount {get;set;}
    public decimal? fee_amount {get;set;}
    public decimal? total_amount {get;set;}
    public short? quota {get;set;}
    public short? reserve {get;set;}
    public short? confirm {get;set;}
    public Guid? education_type_uid {get;set;}

  }
  [GeneratedUidController("api/education_plan/education_plan")]
  public class t_education_plan : education_plan
  {
    [Include]
    public List<t_education_plan_subject> education_plan_subjects { get; set; }
    [Include]
    public List<t_education_plan_fee> education_plan_fees { get; set; }
    [Include]
    public List<t_education_plan_curriculum> education_plan_curriculums { get; set; }
  }

  public class v_education_plan : education_plan
  {
    public string education_type_name_th { get; set; }
    public string college_faculty_name_th { get; set; }
    [Include]
    public List<v_education_plan_subject> education_plan_subjects { get; set; }
    [Include]
    public List<v_education_plan_fee> education_plan_fees { get; set; }
    [Include]
    public List<v_education_plan_curriculum> education_plan_curriculums { get; set; }
    [NotMapped]
    public List<plan_register_payment> credits {get;set;}
    [NotMapped]
    public List<plan_register_payment> debits {get;set;}

    public List<bill_request_item> ToBillRequestItem()
    {
      // var billRequestItems = this.education_plan_subjects.Select((r, i) =>
      //   new bill_request_item()
      //   {
      //     row_order = i + 1,
      //     item_code = "ZZZ000",//r.year_subject_code,
      //     item_name_th = "Test Subject",//r.year_subject_name_th,
      //     item_name_en ="Test Subject",// r.year_subject_name_th,
      //     unit_price = 1000,// r.total_amount,
      //     quantity = 1,
      //     discount_type_id = 0,
      //     discount_amount = 0,
      //     before_vat_amount = 2000,// r.total_amount,
      //     vat_amount = 0,
      //     amount = 2000,//r.total_amount,
      //     sub_item1_name = "LECTURE",
      //     sub_item1_quantity = 1,
      //     sub_item1_unit_price = 1000,// r.total_lecture_amount,
      //     sub_item1_amount = 1000,//r.total_lecture_amount,
      //     sub_item2_name = "LAB",
      //     sub_item2_quantity = 1,
      //     sub_item2_unit_price = 0,//r.total_lab_amount,
      //     sub_item2_amount = 0,//r.total_lab_amount,
      //     sub_item3_name = "CREDIT",
      //     sub_item3_quantity = 0,//r.lecture_credit_amount,
      //     sub_item3_unit_price = 0,
      //     sub_item3_amount = 0,
      //     item_ext1_name = "00",//r.lecture_section?.section_code,
      //     item_ext2_name = "00",//r.lab_section?.section_code,
      //     share1_percent = 50,
      //     share2_percent = 50
      //   }).ToList();
      // var totalSubjectCount = billRequestItems.Count;
      //
      //
      
      var billRequestItems = new List<bill_request_item>();
      if (this.credits!=null)
      {
        billRequestItems
          .AddRange(this.credits
            .Where(c=>c.name!="CREDITFEE")
            .Select((c, i) => new bill_request_item()
            {
              row_order = i + 1,
              item_code = "",// c.code,
              item_name_th = c.name,
              item_name_en = c.name,
              unit_price = c.amount,
              quantity = 1,
              discount_type_id = 0,
              discount_amount = 0,
              before_vat_amount = c.amount,
              vat_amount = 0,
              amount = c.amount,
      
            }));
      }
      
      var totalSubjectCreditCount = billRequestItems.Count;
      if (debits != null)
      {
        billRequestItems.AddRange(this.debits.Select((d, i) => new bill_request_item()
        {
          row_order = i + 1 + totalSubjectCreditCount,
          item_code = "",//d.code,
          item_name_th = d.name,
          item_name_en = d.name,
          unit_price = d.amount,
          quantity = 1,
          discount_type_id = 0,
          discount_amount = 0,
          before_vat_amount = d.amount,
          vat_amount = 0,
          amount = d.amount,
        }));
      }
      
      return billRequestItems;

    }
  }
}
