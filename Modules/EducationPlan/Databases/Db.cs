using edu_api.Modules.EducationPlan.Databases;
using edu_api.Modules.Fee.Databases.Models;
using Microsoft.EntityFrameworkCore;

namespace SeventyOneDev.Utilities
{
  public partial class Db : DbContext
  {
    public DbSet<t_education_plan> t_education_plan {get;set;}
    public DbSet<v_education_plan> v_education_plan {get;set;}
    public DbSet<t_education_plan_fee> t_education_plan_fee {get;set;}
    public DbSet<v_education_plan_fee> v_education_plan_fee {get;set;}
    public DbSet<t_education_plan_subject> t_education_plan_subject {get;set;}
    public DbSet<v_education_plan_subject> v_education_plan_subject {get;set;}
    public DbSet<t_education_plan_curriculum> t_education_plan_curriculum {get;set;}
    public DbSet<v_education_plan_curriculum> v_education_plan_curriculum {get;set;}
    private void OnEducationPlanModelCreating(ModelBuilder builder, string schema)
    {
      builder.Entity<t_education_plan>().ToTable("education_plan", schema);
      builder.Entity<v_education_plan>().ToView("v_education_plan", schema);

      builder.Entity<t_education_plan_fee>().ToTable("education_plan_fee", schema);
      builder.Entity<t_education_plan_fee>().HasOne(e => e.education_plan).WithMany(e => e.education_plan_fees)
        .HasForeignKey(e => e.education_plan_uid).OnDelete(DeleteBehavior.Cascade);
      builder.Entity<v_education_plan_fee>().ToView("v_education_plan_fee", schema);
      builder.Entity<v_education_plan_fee>().HasOne(e => e.education_plan).WithMany(e => e.education_plan_fees)
        .HasForeignKey(e => e.education_plan_uid).OnDelete(DeleteBehavior.Cascade);
      builder.Entity<t_education_plan_subject>().ToTable("education_plan_subject", schema);
      builder.Entity<t_education_plan_subject>().HasOne(e => e.education_plan).WithMany(e => e.education_plan_subjects)
        .HasForeignKey(e => e.education_plan_uid).OnDelete(DeleteBehavior.Cascade);
      builder.Entity<v_education_plan_subject>().ToView("v_education_plan_subject", schema);
      builder.Entity<v_education_plan_subject>().HasOne(e => e.education_plan).WithMany(e => e.education_plan_subjects)
        .HasForeignKey(e => e.education_plan_uid).OnDelete(DeleteBehavior.Cascade);
     
      builder.Entity<t_education_plan_curriculum>().ToTable("education_plan_curriculum", schema);
      builder.Entity<t_education_plan_curriculum>().HasOne(e => e.education_plan).WithMany(e => e.education_plan_curriculums)
        .HasForeignKey(e => e.education_plan_uid).OnDelete(DeleteBehavior.Cascade);
      builder.Entity<v_education_plan_curriculum>().ToView("v_education_plan_curriculum", schema);
      builder.Entity<v_education_plan_curriculum>().HasOne(e => e.education_plan).WithMany(e => e.education_plan_curriculums)
        .HasForeignKey(e => e.education_plan_uid).OnDelete(DeleteBehavior.Cascade);
    }
  }
}
