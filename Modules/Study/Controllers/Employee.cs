using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using edu_api.Modules.Calendars.Databases.Models;
using edu_api.Modules.Study.Databases.Models;
using edu_api.Modules.Study.Services;
using Microsoft.AspNetCore.Mvc;
using SeventyOneDev.Utilities;

namespace edu_api.Modules.Study.Controllers
{
  [Route("api/study/employee", Name = "employee")]
  public class EmployeeController : BaseUidController<t_employee, v_employee>
  {
    private readonly EmployeeService _employeeService;
    public EmployeeController(EmployeeService entityUidService) : base(entityUidService)
    {
      _employeeService = entityUidService;
    }

    
    

    [HttpGet("get_employee/{employee_code}")]
    public async Task<ActionResult<v_employee>> GetByCode([FromRoute, Required] string employee_code)
    {
      var result = await _employeeService.GetByCode(employee_code);
      return result == null ? NotFound() : Ok(result);
    }    
    [HttpGet("get_instructor/{instructor_code}")]
    public async Task<ActionResult<v_instructor>> GetByCodeInstructor([FromRoute, Required] string instructor_code)
    {
      var result = await _employeeService.GetByCodeInstructor(instructor_code);
      return result == null ? NotFound() : Ok(result);
    }
  }
}



