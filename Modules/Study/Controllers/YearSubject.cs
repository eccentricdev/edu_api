using System;
using edu_api.Modules.Calendars.Databases.Models;
using edu_api.Modules.Study.Databases.Models;
using edu_api.Modules.Study.Models;
using edu_api.Modules.Study.Services;
using Microsoft.AspNetCore.Mvc;
using SeventyOneDev.Utilities;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Drawing.Printing;
using System.Linq;
using System.Security.Claims;
using DocumentFormat.OpenXml.Bibliography;
using edu_api.Modules.Commons.Databases.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.EntityFrameworkCore;


namespace edu_api.Modules.Study.Controllers
{
  [Route("api/study/year_subject", Name = "year_subject")]
  public class YearSubjectController:BaseUidController<t_year_subject,v_year_subject>
  {
    private readonly Db _db;
    private readonly YearSubjectService _yearSubjectService;
    public YearSubjectController(YearSubjectService entityUidService,Db db) : base(entityUidService)
    {
      _db = db;
      _yearSubjectService = entityUidService;
    }
    [Authorize]
    [HttpGet("list/{page:int}/{size:int}")]    
    public async Task<ActionResult<PagedResult<year_subject_list>>> GetYearSubjectList(year_subject_list yearSubjectList,[FromRoute]int page=1,[FromRoute]int size=20)
    {
      var yearSubject = new v_year_subject();
      yearSubjectList.CloneTo(yearSubject);
      var yearSubjects = await _yearSubjectService.PageEntityOrderId<year_subject_list> (yearSubject, page, size, "year_subject_code", false);
      return Ok(yearSubjects);
    }
    [Authorize]
    [HttpGet("section/{year_subject_uid}")]    
    public async Task<ActionResult<List<year_subject_section_list>>> GetYearSubjectSectionList([FromRoute]Guid year_subject_uid)
    {
      var yearSubjectSections = await _db.v_year_subject_section.AsNoTracking()
        .Where(s => s.year_subject_uid == year_subject_uid)
        .ProjectTo<v_year_subject_section,year_subject_section_list>()
        .OrderBy(s => s.section_code)
        .ToListAsync();
      return Ok(yearSubjectSections);
    }
    [Authorize]
    [HttpPut("responsible")]    
    public async Task<ActionResult> GetYearSubjectSectionList([FromBody]responsible_person_update responsiblePersonUpdate)
    {
      var claimsIdentiy = User.Identity as ClaimsIdentity;
      var yearSubjectSection = await _db.t_year_subject_section.FirstOrDefaultAsync(s =>
        s.year_subject_section_uid == responsiblePersonUpdate.year_subject_section_uid);
      yearSubjectSection.responsible_person_uid = responsiblePersonUpdate.responsible_person_uid;
      yearSubjectSection.responsible_instructor_uid = responsiblePersonUpdate.responsible_instructor_uid;
      await _db.SaveChangesAsync(claimsIdentiy);
      return Ok();
    }
    

  }
}
