using System;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
using edu_api.Modules.Study.Databases.Models;
using Microsoft.AspNetCore.Mvc;
using SeventyOneDev.Utilities;

namespace edu_api.Modules.Study.Controllers
{
    [Route("api/study/year_subject_section", Name = "year_subject_section")]
    public class YearSubjectSectionController:BaseUidController<t_year_subject_section,v_year_subject_section>
    {
        private readonly Db _db;
        public YearSubjectSectionController(EntityUidService<t_year_subject_section, v_year_subject_section> entityUidService,Db db) : base(entityUidService)
        {
            _db = db;
        }

        public override async Task<ActionResult> UpdateEntityId(t_year_subject_section entity)
        {
            Console.WriteLine("Update data:" + JsonSerializer.Serialize(entity));
            if (entity.year_subject_section_days != null)
            {
                entity.study_time_text_en = string.Join( ",",entity.year_subject_section_days.Select(d => d.study_time_text_en).ToList());
                   // string.Join(entity.year_subject_section_days.Select(d => d.study_time_text_en).ToList(), ",");
            }
            return await base.UpdateEntityId(entity);
        }
       
    }
}