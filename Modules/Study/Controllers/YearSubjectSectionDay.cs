using edu_api.Modules.Study.Databases.Models;
using edu_api.Modules.Study.Services;
using Microsoft.AspNetCore.Mvc;
using SeventyOneDev.Utilities;

namespace edu_api.Modules.Study.Controllers
{
  [Route("api/study/year_subject_section_day", Name = "year_subject_section_day")]
  public class YearSubjectSectionDayController:BaseUidController<t_year_subject_section_day,v_year_subject_section_day>
  {
    public YearSubjectSectionDayController(YearSubjectSectionDayService entityUidService) : base(entityUidService)
    {

    }
  }
}
