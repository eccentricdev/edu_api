using System;

namespace edu_api.Modules.Study.Models
{
  public class study_time_text
  {
    public TimeSpan? start_time { get; set; }
    public TimeSpan? end_time { get; set; }
  }
}
