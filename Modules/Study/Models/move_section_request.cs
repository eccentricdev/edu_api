using System;
using System.Collections.Generic;

namespace edu_api.Modules.Study.Models
{
    public class move_section_request
    {
        public List<Guid> final_register_subject_uids { get; set; }
        public Guid? lecture_year_subject_section_uid { get; set; }
        public Guid? lab_year_subject_section_uid { get; set; }
    }
}