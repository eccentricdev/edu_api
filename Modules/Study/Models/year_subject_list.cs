using System;
namespace edu_api.Modules.Study.Models
{
    public class year_subject_list
    {
        public Guid? year_subject_uid { get;set; }
        public string year_subject_code { get; set; }
        public string year_subject_name_th { get; set; }
        public Guid? education_type_uid { get; set; }
        public string education_type_name_th { get; set; }
        public Guid? academic_year_uid { get; set; }
        public string academic_year_code { get; set; }
        public Guid? academic_semester_uid { get;set; }
        public string semester_code { get; set; }
    }
}