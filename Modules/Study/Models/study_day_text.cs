namespace edu_api.Modules.Study.Models
{
  public class study_day_text
  {
    public string day_short_name_en {get;set;}
    public short? row_order {get;set;}
  }
}
