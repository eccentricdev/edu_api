using System;

namespace edu_api.Modules.Study.Models
{
    public class responsible_person_update
    {
        public Guid? year_subject_section_uid { get; set; }
        public Guid? responsible_person_uid { get; set; }
        public Guid? responsible_instructor_uid { get; set; } 
    }
}