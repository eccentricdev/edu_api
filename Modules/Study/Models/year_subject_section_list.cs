using System;
namespace edu_api.Modules.Study.Models
{
    public class year_subject_section_list
    {
        public Guid? year_subject_section_uid { get; set; }
        public string section_code { get; set; }
        public string section_type_name_th { get; set; }
        public string section_type_code { get; set; }
        public string topic_name_th { get; set; }
        public short? topic_credit { get; set; }
        public Guid? responsible_person_uid { get; set; }
        public string responsible_person_display_name_th { get; set; }
        public Guid? responsible_instructor_uid { get; set; }
        public string study_time_text_en { get; set; }
    }
}