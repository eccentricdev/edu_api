using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;
using SeventyOneDev.Utilities;
using SeventyOneDev.Utilities.Attributes;

namespace edu_api.Modules.Study.Databases.Models
{
  public class employee:base_table
  {
    [Key]
    public Guid? employee_uid {get;set;}
    [MaxLength(50),Unique]
    public string employee_code {get;set;}
    [MaxLength(50)]
    public string prefix_name_th {get;set;}
    [MaxLength(50)]
    public string prefix_name_en {get;set;}
    [MaxLength(255)]
    public string first_name_th {get;set;}
    [MaxLength(255)]
    public string first_name_en {get;set;}
    [MaxLength(255)]
    public string last_name_th {get;set;}
    [MaxLength(255)]
    public string last_name_en {get;set;}
    [MaxLength(500)]
    public string display_name_th {get;set;}
    [MaxLength(500)]
    public string display_name_en {get;set;}
    [MaxLength(10)]
    public string faculty_code {get;set;}
    public bool? is_quit {get;set;}
  }
  //[GeneratedUidController("api/study/employee")]
  public class t_employee : employee
  {

  }

  public class v_employee : employee
  {
    public string faculty_name_th {get;set;}
    public string faculty_name_en {get;set;}
  }
  public class v_instructor
  {

    [Key]
    public Guid? personnel_uid { get; set; }
    public string instructor_code {get;set;}
    public string prefix_name_th {get;set;}
    public string prefix_name_en {get;set;}
    public string first_name_th {get;set;}
    public string first_name_en {get;set;}
    public string last_name_th {get;set;}
    public string last_name_en {get;set;}
    public string display_name_th {get;set;}
    public string display_name_en {get;set;}
    public string faculty_code {get;set;}
    public bool? is_quit {get;set;}

  }
}
