using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;
using SeventyOneDev.Utilities;

namespace edu_api.Modules.Study.Databases.Models
{
    public class year_subject_exam:base_table
    {
        [Key]
        public Guid? year_subject_exam_uid {get;set;}
        public Guid? year_subject_uid {get;set;}

        public DateTime? midterm_lecture_exam_date { get; set; }
        public Guid? midterm_lecture_exam_time_uid { get; set; }
        public TimeSpan? midterm_lecture_exam_from_time { get; set; }
        public TimeSpan? midterm_lecture_exam_to_time { get; set; }
        //public string midterm_lecture_exam_time_text { get; set; }
        public DateTime? midterm_lab_exam_date { get; set; }
        public Guid? midterm_lab_exam_time_uid { get; set; }
        public TimeSpan? midterm_lab_exam_from_time { get; set; }
        public TimeSpan? midterm_lab_exam_to_time { get; set; }
        //public string midterm_lab_exam_time_text { get; set; }

        public DateTime? final_lecture_exam_date { get; set; }
        public Guid? final_lecture_exam_time_uid { get; set; }
        public TimeSpan? final_lecture_exam_from_time { get; set; }
        public TimeSpan? final_lecture_exam_to_time { get; set; }
        //public string final_lecture_exam_time_text { get; set; }

        public DateTime? final_lab_exam_date { get; set; }
        public Guid? final_lab_exam_time_uid { get; set; }
        public TimeSpan? final_lab_exam_from_time { get; set; }
        public TimeSpan? final_lab_exam_to_time { get; set; }
        //public string final_lab_exam_time_text { get; set; }


    }

    public class t_year_subject_exam : year_subject_exam
    {
      [JsonIgnore]
      public t_year_subject year_subject { get; set; }
    }

    public class v_year_subject_exam : year_subject_exam
    {
      [JsonIgnore]
      public v_year_subject year_subject { get; set; }
    }
    // public class v_study_subject_exam
    // {
    //     [Key]
    //     public int? study_subject_exam_id {get;set;}
    //     public int? subject_year_id {get;set;}
    //
    //     public int? academic_semester_id { get; set; }
    //     public DateTime? midterm_lecture_exam_date { get; set; }
    //     public int? midterm_lecture_exam_time_id { get; set; }
    //     public string midterm_lecture_exam_time_text { get; set; }
    //
    //     public DateTime? midterm_lab_exam_date { get; set; }
    //     public int? midterm_lab_exam_time_id { get; set; }
    //     public string midterm_lab_exam_time_text { get; set; }
    //
    //     public DateTime? final_lecture_exam_date { get; set; }
    //     public int? final_lecture_exam_time_id { get; set; }
    //     public string final_lecture_exam_time_text { get; set; }
    //
    //     public DateTime? final_lab_exam_date { get; set; }
    //     public int? final_lab_exam_time_id { get; set; }
    //     public string final_lab_exam_time_text { get; set; }
    //     [JsonIgnore]
    //     public v_subject_year subject_year { get; set; }
    // }
}
