using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;
using SeventyOneDev.Utilities;

namespace edu_api.Modules.Study.Databases.Models
{
  public class year_subject_section_year:base_table
  {
    [Key]
    public Guid? year_subject_section_year_uid { get; set; }
    public Guid? year_subject_section_uid { get; set; }
    public Guid? entry_academic_year_uid { get; set; }
    // [JsonIgnore]
    // public study_subject_section study_subject_section { get; set; }
  }

  public class t_year_subject_section_year : year_subject_section_year
  {
    [JsonIgnore]
    public t_year_subject_section year_subject_section { get; set; }
  }

  public class v_year_subject_section_year : year_subject_section_year
  {
    public Guid? academic_year_uid { get; set; }
    public Guid? academic_semester_uid { get; set; }
    [JsonIgnore]
    public v_year_subject_section year_subject_section { get; set; }
  }
  // public class v_study_table_year
  // {
  //   [Key]
  //   public int? study_table_year_id { get; set; }
  //   public int? study_subject_section_id { get; set; }
  //   public int? subject_year_id {get;set;}
  //   public int? entry_academic_year_id { get; set; }
  //   public string entry_academic_year_code { get; set; }
  //   public string entry_academic_year_name_th { get; set; }
  //   public string entry_academic_year_name_en { get; set; }
  //   [JsonIgnore]
  //   public v_study_subject_section study_subject_section { get; set; }
  // }
}
