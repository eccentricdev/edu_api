using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;
using SeventyOneDev.Utilities;
using SeventyOneDev.Utilities.Attributes;

namespace edu_api.Modules.Study.Databases.Models
{
  public class study_day:base_table
  {
    [Key]
    public Guid? study_day_uid {get;set;}
    public string day_code {get;set;}
    public string day_short_name_th {get;set;}
    public string day_short_name_en {get;set;}
    public string day_name_th {get;set;}
    public string day_name_en {get;set;}
    public short? row_order {get;set;}
  }
  [GeneratedUidController("api/study/study_day")]
  public class t_study_day : study_day
  {
    [JsonIgnore]
    public List<t_year_subject_section_day> year_subject_section_days { get; set; }
  }

  public class v_study_day : study_day
  {
    [JsonIgnore]
    public List<v_year_subject_section_day> year_subject_section_days { get; set; }
  }
}
