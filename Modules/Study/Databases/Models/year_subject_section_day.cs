using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;
using SeventyOneDev.Utilities;
using SeventyOneDev.Utilities.Attributes;

namespace edu_api.Modules.Study.Databases.Models
{
  public class year_subject_section_day:base_table
  {
    [Key]
    public Guid? year_subject_section_day_uid {get;set;}
    public Guid? year_subject_section_uid {get;set;}
    public Guid? study_day_uid {get;set;}
    public Guid? study_time_uid {get;set;}

    public Guid? room_uid {get;set;}

    public TimeSpan? study_from_time { get; set; }
    public TimeSpan? study_to_time { get; set; }
    public string room_code {get;set;}
    public string study_time_text_en {get;set;}
    // [JsonIgnore]
    // public study_subject_section study_subject_section { get; set; }
  }
  //[GeneratedUidController("api/study/year_subject_section_day")]
  public class t_year_subject_section_day : year_subject_section_day
  {
    [JsonIgnore]
    public t_year_subject_section year_subject_section { get; set; }
    [JsonIgnore]
    public t_study_day study_day { get; set; }
    [JsonIgnore]
    public t_study_time study_time { get; set; }
  }

  public class v_year_subject_section_day : year_subject_section_day
  {
    public Guid? academic_year_uid { get; set; }
    public Guid? academic_semester_uid { get; set; }
    public string building_name_th {get;set;}
    public string room_name_th {get;set;}
    public Guid? year_subject_uid { get; set; }
    public short? section_type_id { get; set; }
    [JsonIgnore]
    public v_year_subject_section year_subject_section { get; set; }
    [JsonIgnore]
    public v_study_day study_day { get; set; }
    [JsonIgnore]
    public v_study_time study_time { get; set; }
  }
  // public class v_study_table_day
  // {
  //   [Key]
  //   public int? study_table_day_id {get;set;}
  //   public int? study_subject_section_id {get;set;}
  //   public int? study_day_id {get;set;}
  //   public int? study_time_id {get;set;}
  //   public TimeSpan? study_from_time { get; set; }
  //   public TimeSpan? study_to_time { get; set; }
  //   public int? room_id {get;set;}
  //   public string room_code {get;set;}
  //   public string study_time_text_en {get;set;}
  //   [JsonIgnore]
  //   public v_study_subject_section study_subject_section { get; set; }
  // }
}
