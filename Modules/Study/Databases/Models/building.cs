using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SeventyOneDev.Utilities;
using SeventyOneDev.Utilities.Attributes;

namespace edu_api.Modules.Study.Databases.Models
{
  [GeneratedUidController("api/study/building")]
  public class v_building:base_table
  {
    [Key]
    public Guid? building_uid { get; set; }
    public string building_code { get; set; }
    public string building_name_th { get; set; }
    public string building_name_en { get; set; }
  }
}
