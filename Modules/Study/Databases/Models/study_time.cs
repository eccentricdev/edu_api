using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;
using SeventyOneDev.Utilities;
using SeventyOneDev.Utilities.Attributes;

namespace edu_api.Modules.Study.Databases.Models
{
  public class study_time:base_table
  {
    [Key]
    public Guid? study_time_uid {get;set;}
    public string study_time_code {get;set;}
    public TimeSpan? start_time {get;set;}
    public TimeSpan? end_time {get;set;}
    public string start_time_text { get; set; }
    public string end_time_text { get; set; }
    public bool? is_exam {get;set;}
  }
  [GeneratedUidController("api/study/study_time")]
  public class t_study_time : study_time
  {
    [JsonIgnore]
    public List<t_year_subject_section_day> year_subject_section_days { get; set; }
  }

  public class v_study_time : study_time
  {
    [JsonIgnore]
    public List<v_year_subject_section_day> year_subject_section_days { get; set; }
  }
}
