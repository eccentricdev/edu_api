using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;
using SeventyOneDev.Utilities;
using SeventyOneDev.Utilities.Attributes;

namespace edu_api.Modules.Study.Databases.Models
{
  public class year_subject_section_instructor:base_table
  {
    [Key]
    public Guid? year_subject_section_instructor_uid {get;set;}
    public Guid? year_subject_section_uid {get;set;}
    public Guid? instructor_uid { get; set; }
    // [MaxLength(10)]
    // public string instructor_code {get;set;}
    // [MaxLength(200)]
    // public string instrcutor_name_th {get;set;}
    // [MaxLength(200)]
    // public string instructor_name_en {get;set;}
    public decimal? responsible_percent {get;set;}
    public string remark { get; set; }
    // [JsonIgnore]
    // public study_subject_section study_subject_section { get; set; }
  }
  [GeneratedUidController("api/study/year_subject_section_instructor")]
  public class t_year_subject_section_instructor : year_subject_section_instructor
  {
    [JsonIgnore]
    public t_year_subject_section year_subject_section { get; set; }
  }

  public class v_year_subject_section_instructor : year_subject_section_instructor
  {
    public string instructor_code {get;set;}
    public string display_name_th {get;set;}
    public string display_name_en {get;set;}
    [JsonIgnore]
    public v_year_subject_section year_subject_section { get; set; }
  }
  // public class v_year_subject_section_instructor
  // {
  //   [Key]
  //   public int? year_subject_section_instructor_id {get;set;}
  //   public int? study_subject_section_id {get;set;}
  //   public string instructor_code {get;set;}
  //   public string instrcutor_name_th {get;set;}
  //   public string instructor_name_en {get;set;}
  //   public byte? responsible_percent {get;set;}
  //   [JsonIgnore]
  //   public v_study_subject_section study_subject_section { get; set; }
  // }
}
