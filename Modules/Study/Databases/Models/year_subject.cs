using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using SeventyOneDev.Utilities;
using SeventyOneDev.Utilities.Attributes;

namespace edu_api.Modules.Study.Databases.Models
{
  public class year_subject : base_table
  {
    [Key]
    public Guid? year_subject_uid { get; set; }
    public Guid? subject_uid { get; set; }
    public Guid? academic_year_uid { get; set; }
    public Guid? academic_semester_uid {get;set;}
    public Guid? education_type_uid { get; set; }

    //Temporary
    public string subject_seq_code { get; set; }
    public string academic_year_code { get; set; }
    public string education_type_code { get; set; }



    public string year_subject_code { get; set; }
    public string year_subject_name_th { get; set; }
    public string year_subject_name_en { get; set; }
    public string credit_text { get; set; }
    public string remark { get; set; }
    public short? lecture_hour { get; set; }
    public short? lab_hour { get; set; }
    public short? practice_hour { get; set; }
    public string min_grade { get; set; }
    public short? credit { get; set; }
    public bool? is_gpa_include { get; set; }
    public bool? has_prerequisite { get; set; }
    public bool? has_co_subject { get; set; }
    public bool? allow_scholarship { get; set; }
    public short? subject_type_id { get; set; }
    public short? study_type_id { get; set; }

    public Guid? owner_education_type_uid { get; set; }
    public Guid? owner_college_faculty_uid { get; set; }
    public Guid? owner_faculty_curriculum_uid { get; set; }

    public decimal? lecture_amount { get; set; }
    public decimal? lab_amount { get; set; }
    public decimal? practice_amount { get; set; }
    [OrderBy(true)]
    public new DateTime? created_datetime { get; set; }
 }
  public class t_year_subject : year_subject
  {
    [Include]
    public List<t_year_subject_section> year_subject_sections { get; set; }
    [Include]
    public List<t_year_subject_exam> year_subject_exams { get; set; }
  }

  public class v_year_subject : year_subject
  {
    public string academic_year_name_th {get;set;}
    public string subject_name_th {get;set;}
    public string subject_code {get;set;}
    public string academic_year_name_en {get;set;}
    public string semester_name_en {get;set;}
    public string semester_name_th {get;set;}
    public string education_type_name_en {get;set;}
    public string education_type_name_th {get;set;}
    public string college_faculty_name_en {get;set;}
    public string college_faculty_name_th {get;set;}
    public string faculty_curriculum_name_en {get;set;}
    public string faculty_curriculum_name_th { get; set; }
    public string semester_code { get; set; }
    [Include]
    public List<v_year_subject_section> year_subject_sections { get; set; }
    [Include]
    public List<v_year_subject_exam> year_subject_exams { get; set; }
  }
}
