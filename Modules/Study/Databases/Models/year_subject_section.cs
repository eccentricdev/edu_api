using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;
using SeventyOneDev.Utilities;
using SeventyOneDev.Utilities.Attributes;

namespace edu_api.Modules.Study.Databases.Models
{
    public class year_subject_section:base_table
    {
        [Key]
        public Guid? year_subject_section_uid {get;set;}
        public Guid? year_subject_uid {get;set;}


        public string section_code {get;set;}
        public short? section_type_id {get;set;}
        public string section_type_code {get;set;}


        public short? web_quota { get; set; }
        public short? package_quota { get; set; }
        public short? quota {get;set;}
        public short? reserved {get;set;}
        public short? web_reserved { get; set; }
        public short? package_reserved { get; set; }
        public short? confirmed {get;set;}
        public short? web_confirmed { get; set; }
        public short? package_confirmed { get; set; }


        public string remark {get;set;}
        public bool? is_approved {get;set;}
        public Guid? college_faculty_uid {get;set;}

        public Guid? subject_topic_uid {get;set;}
        public string topic_code { get; set; }
        public string topic_name_th { get; set; }
        public string topic_name_en { get; set; }
        public short? topic_credit { get; set; }
        public string primary_instructor_name_th { get; set; }
        public string primary_instructor_name_en { get; set; }
        public Guid? evaluate_status_uid { get; set; }
        public Guid? grade_criteria_uid { get; set; }
        public bool? is_primary { get; set; }
        public Guid? responsible_person_uid { get; set; }
        public Guid? responsible_instructor_uid { get; set; }
        [MaxLength(200)]public string study_time_text_en { get; set; }


    }
    //[GeneratedUidController("api/study/year_subject_section")]
    public class t_year_subject_section : year_subject_section
    {
      [JsonIgnore]
      public t_year_subject year_subject { get; set; }
      [Include]
      public List<t_year_subject_section_day> year_subject_section_days { get; set; }
      [Include]
      public List<t_year_subject_section_instructor> year_subject_section_instructors { get; set; }
      [Include]
      public List<t_year_subject_section_faculty> year_subject_section_faculties { get; set; }
      [Include]
      public List<t_year_subject_section_year> year_subject_section_years { get; set; }
    }

    public class v_year_subject_section : year_subject_section
    {
      public Guid? academic_year_uid { get; set; }
      public Guid? academic_semester_uid { get; set; }
      public string building_name_th { get; set; }
      public string room_name_th { get; set; }
      public string section_type_name_th { get; set; }
      public string responsible_person_display_name_th { get; set; }
        //public string section_type_code { get; set; }
      [JsonIgnore]
      public v_year_subject year_subject { get; set; }
      [Include]
      public List<v_year_subject_section_day> year_subject_section_days { get; set; }
      [Include]
      public List<v_year_subject_section_instructor> year_subject_section_instructors { get; set; }
      [Include]
      public List<v_year_subject_section_faculty> year_subject_section_faculties { get; set; }
      [Include]
      public List<v_year_subject_section_year> year_subject_section_years { get; set; }
    }
    // public class v_study_subject_section
    // {
    //     [Key]
    //     public int? study_subject_section_id {get;set;}
    //     public int? subject_year_id {get;set;}
    //     public string section_code {get;set;}
    //     public byte? section_type_id {get;set;}
    //     //public string section_type_code {get;set;}
    //     public int? topic_id {get;set;}
    //     public short? quota {get;set;}
    //     public short? reserved {get;set;}
    //     public short? confirmed {get;set;}
    //     public string remark {get;set;}
    //     public string status {get;set;}
    //     public bool? is_approved {get;set;}
    //     // Owner
    //     // public int? education_type_id {get;set;}
    //     // public string education_type_code {get;set;}
    //     // public int? faculty_id {get;set;}
    //     // public int? major_id {get;set;}
    //     // Search optimization
    //
    //
    //     public string subject_code {get;set;}
    //     //public string subject_seq_code {get;set;}
    //     public string topic_name_th {get;set;}
    //     public string topic_name_en {get;set;}
    //     public byte? topic_credit {get;set;}
    //     public string study_time_text_en {get;set;}
    //     public string study_time {get;set;}
    //     public string primary_instructor_code { get; set; }
    //     public string primary_instructor_name_th { get; set; }
    //     public string primary_instructor_name_en { get; set; }
    //     //public string instructors { get; set; }
    //     public short? study_result_status_id { get; set; }
    //
    //     public string section_type_code { get; set; }
    //     public string section_type_name_th { get; set; }
    //     public string section_type_name_en { get; set; }
    //
    //     public int? academic_year_id {get;set;}
    //     public string academic_year_code {get;set;}
    //     public string academic_year_name_th { get; set; }
    //     public string academic_year_name_en { get; set; }
    //
    //     public int? academic_semester_id {get;set;}
    //     public string academic_semester_code {get;set;}
    //     public string academic_semester_name_th {get;set;}
    //     public string academic_semester_name_en {get;set;}
    //
    //     public string study_result_status_code { get; set; }
    //     public string study_result_status_name_th { get; set; }
    //     public string study_result_status_name_en { get; set; }
    //     public List<v_study_table_day> study_table_days { get; set; }
    //     public List<v_study_table_instructor> study_table_instructors { get; set; }
    //     public List<v_study_table_faculty> study_table_faculties { get; set; }
    //     public List<v_study_table_year> study_table_years { get; set; }
    //     [JsonIgnore]
    //     public v_subject_year subject_year { get; set; }
    //     //[JsonIgnore]
    //     //public List<v_education_plan_subject> education_plan_subjects {get;set;}
    // }
}
