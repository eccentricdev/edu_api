using System;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;
using SeventyOneDev.Utilities;
using SeventyOneDev.Utilities.Attributes;

namespace edu_api.Modules.Study.Databases.Models
{
    [GeneratedUidController("api/study/room")]
    public class v_room : base_table
    {
        [Key] public Guid? room_uid { get; set; }
        public Guid? building_uid { get; set; }
        public Guid? floor_uid { get; set; }
        public string room_no { get; set; }
        public string room_name_th { get; set; }
        public string room_name_en { get; set; }
        public int? total_seat { get; set; }
        public int? exam_seat { get; set; }
        [JsonIgnore] public v_floor floor { get; set; }
    }
}
