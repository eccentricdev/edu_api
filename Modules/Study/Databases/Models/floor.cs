using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace edu_api.Modules.Study.Databases.Models
{
    public class v_floor
    {
        [Key]
        public Guid? floor_uid { get; set; }
        public string floor_no { get; set; }
        public string floor_name_th { get; set; }
        public string floor_name_en { get; set; }
        public Guid? building_uid { get; set; }
        public int? total_exam_seats { get; set; }
        public List<v_room> rooms { get; set; }
    }
}