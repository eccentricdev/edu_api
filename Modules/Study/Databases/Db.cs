using edu_api.Modules.Fee.Databases.Models;
using edu_api.Modules.Study.Databases.Models;
using Microsoft.EntityFrameworkCore;

namespace SeventyOneDev.Utilities
{
  public partial class Db : DbContext
  {
    public DbSet<t_year_subject> t_year_subject {get;set;}
    public DbSet<v_year_subject> v_year_subject {get;set;}
    public DbSet<t_year_subject_section> t_year_subject_section {get;set;}
    public DbSet<v_year_subject_section> v_year_subject_section {get;set;}
    public DbSet<t_year_subject_section_day> t_year_subject_section_day {get;set;}
    public DbSet<v_year_subject_section_day> v_year_subject_section_day {get;set;}
    public DbSet<t_year_subject_section_instructor> t_year_subject_section_instructor {get;set;}
    public DbSet<v_year_subject_section_instructor> v_year_subject_section_instructor {get;set;}
    public DbSet<t_year_subject_section_faculty> t_year_subject_section_faculty {get;set;}
    public DbSet<v_year_subject_section_faculty> v_year_subject_section_faculty {get;set;}
    public DbSet<t_year_subject_section_year> t_year_subject_section_year {get;set;}
    public DbSet<v_year_subject_section_year> v_year_subject_section_year {get;set;}

    public DbSet<t_year_subject_exam> t_year_subject_exam {get;set;}
    public DbSet<v_year_subject_exam> v_year_subject_exam {get;set;}

    public DbSet<t_study_day> t_study_day {get;set;}
    public DbSet<v_study_day> v_study_day {get;set;}

    public DbSet<t_study_time> t_study_time {get;set;}
    public DbSet<v_study_time> v_study_time {get;set;}
    public DbSet<v_room> v_room {get;set;}
    public DbSet<v_building> v_building {get;set;}
    public DbSet<v_floor> v_floor {get;set;}
    public DbSet<t_employee> t_employee {get;set;}
    public DbSet<v_employee> v_employee {get;set;}
    public DbSet<v_instructor> v_instructor {get;set;}


    private void OnStudyModelCreating(ModelBuilder builder, string schema)
    {
      builder.Entity<t_year_subject>().ToTable("year_subject", schema);
      builder.Entity<v_year_subject>().ToView("v_year_subject", schema);

      builder.Entity<t_year_subject_section>().ToTable("year_subject_section", schema);
      builder.Entity<t_year_subject_section>().HasOne(y => y.year_subject).WithMany(y => y.year_subject_sections)
        .HasForeignKey(y => y.year_subject_uid).OnDelete(DeleteBehavior.Restrict);
      builder.Entity<v_year_subject_section>().ToView("v_year_subject_section", schema);
      builder.Entity<v_year_subject_section>().HasOne(y => y.year_subject).WithMany(y => y.year_subject_sections)
        .HasForeignKey(y => y.year_subject_uid).OnDelete(DeleteBehavior.Restrict);

      builder.Entity<t_year_subject_section_day>().ToTable("year_subject_section_day", schema);
      builder.Entity<t_year_subject_section_day>().HasOne(y => y.year_subject_section).WithMany(y => y.year_subject_section_days)
        .HasForeignKey(y => y.year_subject_section_uid).OnDelete(DeleteBehavior.Restrict);
      builder.Entity<t_year_subject_section_day>().HasOne(y => y.study_day).WithMany(y => y.year_subject_section_days)
        .HasForeignKey(y => y.study_day_uid).OnDelete(DeleteBehavior.Restrict);
      builder.Entity<t_year_subject_section_day>().HasOne(y => y.study_time).WithMany(y => y.year_subject_section_days)
        .HasForeignKey(y => y.study_time_uid).OnDelete(DeleteBehavior.Restrict);

      builder.Entity<v_year_subject_section_day>().ToView("v_year_subject_section_day", schema);
      builder.Entity<v_year_subject_section_day>().HasOne(y => y.year_subject_section).WithMany(y => y.year_subject_section_days)
        .HasForeignKey(y => y.year_subject_section_uid).OnDelete(DeleteBehavior.Restrict);
      builder.Entity<v_year_subject_section_day>().HasOne(y => y.study_day).WithMany(y => y.year_subject_section_days)
        .HasForeignKey(y => y.study_day_uid).OnDelete(DeleteBehavior.Restrict);
      builder.Entity<v_year_subject_section_day>().HasOne(y => y.study_time).WithMany(y => y.year_subject_section_days)
        .HasForeignKey(y => y.study_time_uid).OnDelete(DeleteBehavior.Restrict);

      builder.Entity<t_year_subject_section_instructor>().ToTable("year_subject_section_instructor", schema);
      builder.Entity<t_year_subject_section_instructor>().HasOne(y => y.year_subject_section).WithMany(y => y.year_subject_section_instructors)
        .HasForeignKey(y => y.year_subject_section_uid).OnDelete(DeleteBehavior.Restrict);
      builder.Entity<v_year_subject_section_instructor>().ToView("v_year_subject_section_instructor", schema);
      builder.Entity<v_year_subject_section_instructor>().HasOne(y => y.year_subject_section).WithMany(y => y.year_subject_section_instructors)
        .HasForeignKey(y => y.year_subject_section_uid).OnDelete(DeleteBehavior.Restrict);

      builder.Entity<t_year_subject_section_faculty>().ToTable("year_subject_section_faculty", schema);
      builder.Entity<t_year_subject_section_faculty>().HasOne(y => y.year_subject_section).WithMany(y => y.year_subject_section_faculties)
        .HasForeignKey(y => y.year_subject_section_uid).OnDelete(DeleteBehavior.Restrict);
      builder.Entity<v_year_subject_section_faculty>().ToView("v_year_subject_section_faculty", schema);
      builder.Entity<v_year_subject_section_faculty>().HasOne(y => y.year_subject_section).WithMany(y => y.year_subject_section_faculties)
        .HasForeignKey(y => y.year_subject_section_uid).OnDelete(DeleteBehavior.Restrict);

      builder.Entity<t_year_subject_section_year>().ToTable("year_subject_section_year", schema);
      builder.Entity<t_year_subject_section_year>().HasOne(y => y.year_subject_section).WithMany(y => y.year_subject_section_years)
        .HasForeignKey(y => y.year_subject_section_uid).OnDelete(DeleteBehavior.Restrict);
      builder.Entity<v_year_subject_section_year>().ToView("v_year_subject_section_year", schema);
      builder.Entity<v_year_subject_section_year>().HasOne(y => y.year_subject_section).WithMany(y => y.year_subject_section_years)
        .HasForeignKey(y => y.year_subject_section_uid).OnDelete(DeleteBehavior.Restrict);



      builder.Entity<t_year_subject_exam>().ToTable("year_subject_exam", schema);
      builder.Entity<t_year_subject_exam>().HasOne(y => y.year_subject).WithMany(y => y.year_subject_exams)
        .HasForeignKey(y => y.year_subject_uid).OnDelete(DeleteBehavior.Cascade);


      builder.Entity<v_year_subject_exam>().ToView("v_year_subject_exam", schema);
      builder.Entity<v_year_subject_exam>().HasOne(y => y.year_subject).WithMany(y => y.year_subject_exams)
        .HasForeignKey(y => y.year_subject_uid).OnDelete(DeleteBehavior.Cascade);

      builder.Entity<t_study_day>().ToTable("study_day", schema);
      builder.Entity<v_study_day>().ToView("v_study_day", schema);
      builder.Entity<t_study_time>().ToTable("study_time", schema);
      builder.Entity<v_study_time>().ToView("v_study_time", schema);
      builder.Entity<v_room>().ToView("v_room", schema);
      builder.Entity<v_room>().HasOne(r => r.floor).WithMany(r => r.rooms).HasForeignKey(r => r.floor_uid);
      builder.Entity<v_building>().ToView("v_building", schema);
      builder.Entity<v_floor>().ToView("v_floor", schema);

      builder.Entity<t_employee>().ToTable("employee", schema);
      builder.Entity<v_employee>().ToView("v_employee",schema);
      builder.Entity<v_instructor>().ToView("v_instructor",schema);
    }
  }
}
