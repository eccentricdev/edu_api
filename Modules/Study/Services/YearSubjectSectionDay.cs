using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using edu_api.Modules.Study.Databases.Models;
using edu_api.Modules.Study.Models;
using Microsoft.EntityFrameworkCore;
using SeventyOneDev.Utilities;

namespace edu_api.Modules.Study.Services
{
  public class YearSubjectSectionDayService:EntityUidService<t_year_subject_section_day,v_year_subject_section_day>
  {
    private readonly Db _context;
    public YearSubjectSectionDayService(Db context) : base(context)
    {
      _context = context;
    }

    public override async Task<t_year_subject_section_day> NewEntity(t_year_subject_section_day entity, ClaimsIdentity claimsIdentity)
    {

      var studyDay=await _context.t_study_day.AsNoTracking().Where(s => s.study_day_uid == entity.study_day_uid).ProjectTo<t_study_day,study_day_text>().FirstOrDefaultAsync();
      var studyTime = await _context.t_study_time.AsNoTracking().Where(s => s.study_time_uid == entity.study_time_uid)
        .ProjectTo<t_study_time, study_time_text>().FirstOrDefaultAsync();
      var startTime = new TimeSpan(studyDay.row_order??0, 0, 0, 0, 0).Add(studyTime.start_time.Value);
      var endTime = new TimeSpan(studyDay.row_order??0, 0, 0, 0, 0).Add(studyTime.end_time.Value);
      var studyTimeText = string.Format("{0} ({1}-{2})",studyDay.day_short_name_en,studyTime.start_time.Value.ToString(@"hh\:mm"),studyTime.end_time.Value.ToString(@"hh\:mm"));
      entity.study_from_time = startTime;
      entity.study_to_time = endTime;
      entity.study_time_text_en = studyTimeText;
      return await base.NewEntity(entity, claimsIdentity);
    }

    public override async Task<List<t_year_subject_section_day>> NewEntity(List<t_year_subject_section_day> entities, ClaimsIdentity claimsIdentity)
    {
      foreach (var entity in entities)
      {
        var studyDay=await _context.t_study_day.AsNoTracking().Where(s => s.study_day_uid == entity.study_day_uid).ProjectTo<t_study_day,study_day_text>().FirstOrDefaultAsync();
        var studyTime = await _context.t_study_time.AsNoTracking().Where(s => s.study_time_uid == entity.study_time_uid)
          .ProjectTo<t_study_time, study_time_text>().FirstOrDefaultAsync();
        var startTime = new TimeSpan(studyDay.row_order??0, 0, 0, 0, 0).Add(studyTime.start_time.Value);
        var endTime = new TimeSpan(studyDay.row_order??0, 0, 0, 0, 0).Add(studyTime.end_time.Value);
        var studyTimeText = string.Format("{0} ({1}-{2})",studyDay.day_short_name_en,studyTime.start_time.Value.ToString(@"hh\:mm"),studyTime.end_time.Value.ToString(@"hh\:mm"));
        entity.study_from_time = startTime;
        entity.study_to_time = endTime;
        entity.study_time_text_en = studyTimeText;
      }
      return await base.NewEntity(entities, claimsIdentity);
    }

    public override async Task<int> UpdateEntity(t_year_subject_section_day entity, ClaimsIdentity claimsIdentity)
    {
      var studyDay=await _context.t_study_day.AsNoTracking().Where(s => s.study_day_uid == entity.study_day_uid).ProjectTo<t_study_day,study_day_text>().FirstOrDefaultAsync();
      var studyTime = await _context.t_study_time.AsNoTracking().Where(s => s.study_time_uid == entity.study_time_uid)
        .ProjectTo<t_study_time, study_time_text>().FirstOrDefaultAsync();
      var startTime = new TimeSpan(studyDay.row_order??0, 0, 0, 0, 0).Add(studyTime.start_time.Value);
      var endTime = new TimeSpan(studyDay.row_order??0, 0, 0, 0, 0).Add(studyTime.end_time.Value);
      var studyTimeText = string.Format("{0} ({1}-{2})",studyDay.day_short_name_en,studyTime.start_time.Value.ToString(@"hh\:mm"),studyTime.end_time.Value.ToString(@"hh\:mm"));
      entity.study_from_time = startTime;
      entity.study_to_time = endTime;
      entity.study_time_text_en = studyTimeText;
      return await base.UpdateEntity(entity, claimsIdentity);
    }

    public override async Task<int> UpdateEntity(List<t_year_subject_section_day> entities, ClaimsIdentity claimsIdentity)
    {
      foreach (var entity in entities)
      {
        var studyDay=await _context.t_study_day.AsNoTracking().Where(s => s.study_day_uid == entity.study_day_uid).ProjectTo<t_study_day,study_day_text>().FirstOrDefaultAsync();
        var studyTime = await _context.t_study_time.AsNoTracking().Where(s => s.study_time_uid == entity.study_time_uid)
          .ProjectTo<t_study_time, study_time_text>().FirstOrDefaultAsync();
        var startTime = new TimeSpan(studyDay.row_order??0, 0, 0, 0, 0).Add(studyTime.start_time.Value);
        var endTime = new TimeSpan(studyDay.row_order??0, 0, 0, 0, 0).Add(studyTime.end_time.Value);
        var studyTimeText = string.Format("{0} ({1}-{2})",studyDay.day_short_name_en,studyTime.start_time.Value.ToString(@"hh\:mm"),studyTime.end_time.Value.ToString(@"hh\:mm"));
        entity.study_from_time = startTime;
        entity.study_to_time = endTime;
        entity.study_time_text_en = studyTimeText;
      }
      return await base.UpdateEntity(entities, claimsIdentity);
    }
  }
}
