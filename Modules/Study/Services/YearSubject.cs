using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using edu_api.Modules.Study.Databases.Models;
using edu_api.Modules.Study.Models;
using Microsoft.EntityFrameworkCore;
using SeventyOneDev.Utilities;

namespace edu_api.Modules.Study.Services
{
  public class YearSubjectService:EntityUidService<t_year_subject,v_year_subject>
  {
    private readonly Db _context;
    public YearSubjectService(Db context) : base(context)
    {
      _context = context;
    }

    public override async Task<v_year_subject> GetEntity(Guid uid, List<Guid?> agencies = null, string userName = null)
    {
      var yearSubject = await _context.v_year_subject.AsNoTracking()
        .Include(s => s.year_subject_exams)
        .Include(s => s.year_subject_sections).ThenInclude(s => s.year_subject_section_years)
        .Include(s => s.year_subject_sections).ThenInclude(s => s.year_subject_section_faculties)
        .Include(s => s.year_subject_sections).ThenInclude(s => s.year_subject_section_days)
        .Include(s => s.year_subject_sections).ThenInclude(s => s.year_subject_section_instructors)
        .FirstOrDefaultAsync(s => s.year_subject_uid == uid);

      return yearSubject;
    }

    public override async Task<List<v_year_subject>> ListEntityId(v_year_subject entity, List<Guid?> agencies = null, string userName = null)
    {
      IQueryable < v_year_subject > year_subjects = _context.v_year_subject.AsNoTracking();


      if (entity != null)
      {
        year_subjects = entity.search != null ? year_subjects.Search(entity.search) : year_subjects.Search(entity);
      }

      year_subjects = year_subjects.OrderBy(c => c.updated_datetime);

      return await  year_subjects.ToListAsync();
    }

    public override async Task<int> UpdateEntity(t_year_subject entity, ClaimsIdentity claimsIdentity)
    {
      // if (entity.year_subject_exams != null)
      // {
      //   var currentYearSubjectExams = await _context.t_year_subject_exam.AsNoTracking()
      //     .Where(a => a.year_subject_uid == entity.year_subject_uid).ToListAsync();
      //   var removeYearSubjectExams = currentYearSubjectExams.Where(c =>
      //     entity.year_subject_exams.All((a => a.year_subject_uid != c.year_subject_uid))).ToList();
      //   if (removeYearSubjectExams.Any())
      //   {
      //     _context.t_year_subject_exam.RemoveRange(removeYearSubjectExams);
      //   }
      // }

      if (entity.year_subject_sections != null)
      {
        //entity.year_subject_sections.Select(y=>y.)
        var haveLecture = entity.year_subject_sections.Any(y => y.section_type_id == 1);
        var haveLab = entity.year_subject_sections.Any(y => y.section_type_id == 2);
        
        
          foreach (var yearSubjectSection in entity.year_subject_sections)
          {
            if (haveLecture)
            {
              if (yearSubjectSection.section_type_id==1)
              {
                yearSubjectSection.is_primary = true;
              }
            }
            else 
            {
                yearSubjectSection.is_primary = true;
              
            }
            if (yearSubjectSection.year_subject_section_days != null)
            {
              foreach (var yearSubjectSectionDay in yearSubjectSection.year_subject_section_days)
              {
                var studyDay = await _context.t_study_day.AsNoTracking()
                  .Where(s => s.study_day_uid == yearSubjectSectionDay.study_day_uid)
                  .ProjectTo<t_study_day, study_day_text>().FirstOrDefaultAsync();
                var studyTime = await _context.t_study_time.AsNoTracking()
                  .Where(s => s.study_time_uid == yearSubjectSectionDay.study_time_uid)
                  .ProjectTo<t_study_time, study_time_text>().FirstOrDefaultAsync();
                var startTime = new TimeSpan(studyDay.row_order ?? 0, 0, 0, 0, 0).Add(studyTime.start_time.Value);
                var endTime = new TimeSpan(studyDay.row_order ?? 0, 0, 0, 0, 0).Add(studyTime.end_time.Value);
                var studyTimeText = string.Format("{0} ({1}-{2})", studyDay.day_short_name_en,
                  studyTime.start_time.Value.ToString(@"hh\:mm"), studyTime.end_time.Value.ToString(@"hh\:mm"));
                yearSubjectSectionDay.study_from_time = startTime;
                yearSubjectSectionDay.study_to_time = endTime;
                yearSubjectSectionDay.study_time_text_en = studyTimeText;
              }
            }
          }


        var currentYearSubjectSections = await _context.t_year_subject_section.AsNoTracking().
          //Include(c=>c.year_subject_section_days).Include(c=>c.year_subject_section_faculties).Include(c=>c.year_subject_section_instructors).Include(c=>c.year_subject_section_years)
          Where(a => a.year_subject_uid == entity.year_subject_uid).ToListAsync();
        //var removeYearSubjectSections = currentYearSubjectSections.Where(y => entity.year_subject_sections.All(s => s.year_subject_section_uid != y.year_subject_section_uid)).ToList();
        var removeYearSubjectSections = currentYearSubjectSections.Where(c =>
          entity.year_subject_sections.All((a => a.year_subject_uid != c.year_subject_uid))).ToList();
        if (removeYearSubjectSections.Any())
        {
          _context.t_year_subject_section.RemoveRange(removeYearSubjectSections);
          await _context.SaveChangesAsync();
          _context.ChangeTracker.Clear();
        }
        
     

        var updateYearSubjectSections = currentYearSubjectSections.Where(c =>
          entity.year_subject_sections.Any(a => a.year_subject_uid == c.year_subject_uid)).ToList();
        foreach (var updateYearSubjectSection in updateYearSubjectSections)
        {
          var currentYearSubjectSectionDays = await _context.t_year_subject_section_day.AsNoTracking().Where(d =>
            d.year_subject_section_uid == updateYearSubjectSection.year_subject_section_uid).ToListAsync();
          var removeYearSubjectSectionDays = currentYearSubjectSectionDays.Where(c =>
            entity.year_subject_sections.FirstOrDefault(s=>s.year_subject_section_uid==updateYearSubjectSection.year_subject_section_uid).year_subject_section_days.
              All((a => a.year_subject_section_day_uid != c.year_subject_section_day_uid))).ToList();
          if (removeYearSubjectSectionDays.Any())
          {
            _context.t_year_subject_section_day.RemoveRange(removeYearSubjectSectionDays);
            await _context.SaveChangesAsync();
          }
          
          var currentYearSubjectInstructors = await _context.t_year_subject_section_instructor.AsNoTracking().Where(d =>
            d.year_subject_section_uid == updateYearSubjectSection.year_subject_section_uid).ToListAsync();
          var removeYearSubjectSectionInstructors = currentYearSubjectInstructors.Where(c =>
            entity.year_subject_sections.FirstOrDefault(s=>s.year_subject_section_uid==updateYearSubjectSection.year_subject_section_uid).year_subject_section_instructors.
              All((a => a.year_subject_section_instructor_uid != c.year_subject_section_instructor_uid))).ToList();
          if (removeYearSubjectSectionInstructors.Any())
          {
            _context.t_year_subject_section_instructor.RemoveRange(removeYearSubjectSectionInstructors);
            await _context.SaveChangesAsync();
          }
        }
        
       

        // foreach (var currentYearSubjectSection in currentYearSubjectSections)
        // {
        //   foreach (var yearSubjectSection in entity.year_subject_sections)
        //   {
        //     var remove = currentYearSubjectSection.year_subject_section_days.Where(c =>
        //       yearSubjectSection.year_subject_section_days.All(a => a.year_subject_section_uid != c.year_subject_section_uid)).ToList();
        //     if (remove.Count>0)
        //     {
        //       _context.t_year_subject_section_day.RemoveRange(remove);
        //     }
        //
        //     var remove2 = currentYearSubjectSection.year_subject_section_faculties.Where(c =>
        //       yearSubjectSection.year_subject_section_faculties.All(a => a.year_subject_section_uid != c.year_subject_section_uid)).ToList();
        //     if (remove.Count>0)
        //     {
        //       _context.t_year_subject_section_faculty.RemoveRange(remove2);
        //     }
        //
        //     var remove3 = currentYearSubjectSection.year_subject_section_instructors.Where(c =>
        //       yearSubjectSection.year_subject_section_instructors.All(a => a.year_subject_section_uid != c.year_subject_section_uid)).ToList();
        //     if (remove.Count>0)
        //     {
        //       _context.t_year_subject_section_instructor.RemoveRange(remove3);
        //     }
        //
        //     var remove4 = currentYearSubjectSection.year_subject_section_years.Where(c =>
        //       yearSubjectSection.year_subject_section_years.All(a => a.year_subject_section_uid != c.year_subject_section_uid)).ToList();
        //     if (remove.Count>0)
        //     {
        //       _context.t_year_subject_section_year.RemoveRange(remove4);
        //     }
        //   }
        // }

        //await _context.SaveChangesAsync();



      }
      _context.ChangeTracker.Clear();
      _context.t_year_subject.Update(entity);
      var result=await _context.SaveChangesAsync();

      //await _context.SaveChangesAsync();
      //_context.ChangeTracker.Clear();
      return result; //await base.UpdateEntity(entity, claimsIdentity);
    }

    public override async Task<t_year_subject> NewEntity(t_year_subject entity, ClaimsIdentity claimsIdentity)
    {
      if (entity.year_subject_sections != null)
      {
        foreach (var yearSubjectSection in entity.year_subject_sections)
        {
          if (yearSubjectSection.year_subject_section_days != null)
          {
            foreach (var yearSubjectSectionDay in yearSubjectSection.year_subject_section_days)
            {
              var studyDay = await _context.t_study_day.AsNoTracking()
                .Where(s => s.study_day_uid == yearSubjectSectionDay.study_day_uid)
                .ProjectTo<t_study_day, study_day_text>().FirstOrDefaultAsync();
              var studyTime = await _context.t_study_time.AsNoTracking()
                .Where(s => s.study_time_uid == yearSubjectSectionDay.study_time_uid)
                .ProjectTo<t_study_time, study_time_text>().FirstOrDefaultAsync();
              var startTime = new TimeSpan(studyDay.row_order ?? 0, 0, 0, 0, 0).Add(studyTime.start_time.Value);
              var endTime = new TimeSpan(studyDay.row_order ?? 0, 0, 0, 0, 0).Add(studyTime.end_time.Value);
              var studyTimeText = string.Format("{0} ({1}-{2})", studyDay.day_short_name_en,
                studyTime.start_time.Value.ToString(@"hh\:mm"), studyTime.end_time.Value.ToString(@"hh\:mm"));
              yearSubjectSectionDay.study_from_time = startTime;
              yearSubjectSectionDay.study_to_time = endTime;
              yearSubjectSectionDay.study_time_text_en = studyTimeText;
            }
          }

          if (entity.study_type_id == 1)
          {
            if (yearSubjectSection.section_type_id == 1)
            {
              yearSubjectSection.is_primary = true;
            }
          }
          else if (entity.study_type_id == 2)
          {
            if (yearSubjectSection.section_type_id == 2)
            {
              yearSubjectSection.is_primary = true;
            }
          }
        }

      }

      // if (entity.year_subject_exams != null)
      // {
      //   if (entity.year_subject_exams.Any())
      //   {
      //     var subjectExam = entity.year_subject_exams.FirstOrDefault();
      //     if (subjectExam.mi)
      //   }
      // }

      var subject = await _context.t_subject.AsNoTracking().FirstOrDefaultAsync(s => s.subject_uid == entity.subject_uid);
      if (entity.year_subject_code == null)
      {
        entity.year_subject_code = subject.subject_code;
        entity.year_subject_name_th = subject.subject_name_th;
        entity.year_subject_name_en = subject.subject_name_en;
      }
      // var subjectYear =  await _context.t_subject_year.AsNoTracking().FirstOrDefaultAsync(s => s.subject_uid == entity.subject_uid && s.curriculum_year_uid);
      //
      // var subjectYearCost =  await _context.t_subject.AsNoTracking().FirstOrDefaultAsync(s => s.subject_uid == entity.subject_uid);

      if (!entity.credit.HasValue)
      {
        entity.credit = subject.credit;
      }

      if (!entity.lecture_amount.HasValue)
      {
        entity.lecture_amount = 1000;
      }
      if (!entity.lab_amount.HasValue)
      {
        entity.lab_amount = 1200;
      }
      return await base.NewEntity(entity, claimsIdentity);
    }
  }
}
