using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using edu_api.Modules.Study.Databases.Models;
using Microsoft.EntityFrameworkCore;
using SeventyOneDev.Utilities;

namespace edu_api.Modules.Study.Services
{
  public class EmployeeService:EntityUidService<t_employee,v_employee>
  {
    private readonly Db _context;
    public EmployeeService(Db context) : base(context)
    {
      _context = context;
    }

    public async Task<v_employee> GetByCode(string _employee_code)
    {
      var employee = await _context.v_employee.AsTracking()
        .Where(c=>c.employee_code == _employee_code).FirstOrDefaultAsync();
      
      return employee;
    }
    
    public async Task<v_instructor> GetByCodeInstructor(string _instructor_code)
    {
      var instructor = await _context.v_instructor.AsTracking()
        .Where(c=>c.instructor_code == _instructor_code).FirstOrDefaultAsync();
      
      return instructor;
    }
  }
}
