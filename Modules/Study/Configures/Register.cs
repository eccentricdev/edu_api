using edu_api.Modules.Study.Services;
using Microsoft.Extensions.DependencyInjection;
using rsu_common_api.Modules.Profiles.Services;

namespace edu_api.Modules.Study.Configures
{
  public static class StudyCollectionExtension
  {
    public static IServiceCollection AddStudyServices(this IServiceCollection services)
    {
      //services.AddAutoMapper(typeof(ProfileMapping));
      services.AddScoped<YearSubjectService>();
      services.AddScoped<YearSubjectSectionDayService>();
      services.AddScoped<EmployeeService>();
      return services;
    }
  }
}
