using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using edu_api.Modules.Organizations.Databases.Models;
using edu_api.Modules.Organizations.Services;
using Microsoft.AspNetCore.Mvc;
using SeventyOneDev.Utilities;

namespace edu_api.Modules.Organizations.Controllers
{
  [Route("/api/organization/agency", Name = "agency")]
  public class AgencyController: ReadOnlyBaseUidController<v_agency>
  {
    private readonly AgencyService _agencyService;
    public AgencyController(AgencyService entityUidService) : base(entityUidService)
    {
      _agencyService = entityUidService;
    }
    [HttpGet, Route("college")]
    public async Task<ActionResult<List<v_agency>>> GetCollege()
    {
      return Ok(await _agencyService.GetCollege());
    }
    [HttpGet, Route("faculty")]
    public async Task<ActionResult<List<v_agency>>> GetFaculty()
    {
      return Ok(await _agencyService.GetFaculty(Guid.Empty));
    }
    [HttpGet, Route("{college_uid}/faculty")]
    public async Task<ActionResult<List<v_agency>>> GetFaculty([FromRoute]Guid college_uid)
    {
      return Ok(await _agencyService.GetFaculty(college_uid));
    }
  }
}
