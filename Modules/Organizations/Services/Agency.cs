using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using edu_api.Modules.Organizations.Databases.Models;
using Microsoft.EntityFrameworkCore;
using SeventyOneDev.Utilities;

namespace edu_api.Modules.Organizations.Services
{
  public class AgencyService: ReadOnlyEntityUidService<v_agency>
  {
    private readonly Db _context;
    public AgencyService(Db context) : base(context)
    {
      _context = context;
    }

    public async Task<List<v_agency>> GetCollege()
    {
      var colleges = _context.v_agency.AsNoTracking().Where(async => async.agency_type_id == 45);
      colleges = colleges.OrderBy(a => a.agency_name_th);
      return await colleges.ToListAsync();

    }
    public async Task<List<v_agency>> GetFaculty(Guid agencyUid)
    {
      if (agencyUid == Guid.Empty)
      {
        var colleges = _context.v_agency.AsNoTracking().Where(a => a.agency_type_id == 2);
        colleges = colleges.OrderBy(a => a.agency_name_th);
        return await colleges.ToListAsync();
      }
      else
      {
        var colleges = _context.v_agency.AsNoTracking().Where(a => a.agency_type_id == 2 && a.parent_agency_key_uid==agencyUid);
        colleges = colleges.OrderBy(a => a.agency_name_th);
        return await colleges.ToListAsync();
      }


    }

    public override async Task<List<v_agency>> ListEntityId(v_agency entity, List<Guid?> agencies = null, string userName = null)
    {
      Stopwatch sw = new Stopwatch();
      sw.Start();

      var result= await base.ListEntityId(entity, agencies, userName);
      sw.Stop();
      Console.WriteLine("Total:" + sw.ElapsedMilliseconds.ToString());
      return result;
    }

    public override async Task<v_agency> GetEntity(Guid uid, List<Guid?> agencies = null, string userName = null)
    {
      Stopwatch sw = new Stopwatch();
      sw.Start();

      var result= await base.GetEntity(uid, agencies, userName);
      sw.Stop();
      Console.WriteLine("Total:" + sw.ElapsedMilliseconds.ToString());
      return result;
    }
  }
}
