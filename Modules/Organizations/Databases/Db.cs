using edu_api.Modules.Organizations.Databases.Models;
using Microsoft.EntityFrameworkCore;

namespace SeventyOneDev.Utilities
{
  public partial class Db : DbContext
  {
    public DbSet<t_education_type> t_education_type { get; set; }
    public DbSet<v_education_type> v_education_type { get; set; }
    public DbSet<t_college_faculty> t_college_faculty { get; set; }
    public DbSet<v_college_faculty> v_college_faculty { get; set; }
    public DbSet<t_faculty_curriculum> t_faculty_curriculum { get; set; }
    public DbSet<v_faculty_curriculum> v_faculty_curriculum { get; set; }
    public DbSet<t_major> t_major { get; set; }
    public DbSet<v_major> v_major { get; set; }
    public DbSet<v_agency> v_agency { get; set; }
    public DbSet<t_education_type_list> t_education_type_list { get; set; }
    public DbSet<v_education_type_list> v_education_type_list { get; set; }

    public DbSet<t_education_type_level> t_education_type_level { get; set; }
    public DbSet<v_education_type_level> v_education_type_level { get; set; }

    private void OnOrganizationModelCreating(ModelBuilder builder, string schema)
    {
      builder.Entity<t_education_type>().ToTable("education_type", schema);
      builder.Entity<v_education_type>().ToView("v_education_type", schema);

      builder.Entity<t_education_type_list>().ToTable("education_type_list", schema);
      builder.Entity<v_education_type_list>().ToView("v_education_type_list", schema);

      builder.Entity<t_education_type_list>().HasOne(e => e.dynamic_fee_education_type)
        .WithMany(e => e.education_type_lists).HasForeignKey(e => e.dynamic_fee_education_type_uid)
        .OnDelete(DeleteBehavior.Cascade);
      builder.Entity<v_education_type_list>().HasOne(e => e.dynamic_fee_education_type)
        .WithMany(e => e.education_type_lists).HasForeignKey(e => e.dynamic_fee_education_type_uid)
        .OnDelete(DeleteBehavior.Cascade);
      builder.Entity<t_college_faculty>().ToTable("college_faculty", schema);
      builder.Entity<v_college_faculty>().ToView("v_college_faculty", schema);

      builder.Entity<t_faculty_curriculum>().ToTable("faculty_curriculum", schema);
      builder.Entity<v_faculty_curriculum>().ToView("v_faculty_curriculum", schema);

      builder.Entity<t_major>().ToTable("major", schema);
      builder.Entity<v_major>().ToView("v_major", schema);
      builder.Entity<v_agency>().ToView("v_agency","RSUHR");

      builder.Entity<t_education_type_level>().ToTable("education_type_level", schema);
      builder.Entity<v_education_type_level>().ToView("v_education_type_level", schema);
    }
  }
}
