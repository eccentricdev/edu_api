using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;
using edu_api.Modules.Fee.Databases.Models;
using SeventyOneDev.Utilities;
using SeventyOneDev.Utilities.Attributes;

namespace edu_api.Modules.Organizations.Databases.Models
{
  //[Table("education_type",Schema="RSU")]
    public class education_type:base_table
    {
        [Key,DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid? education_type_uid { get; set; }
        //public int? education_type_id {get;set;}
        //public int? education_type_level_id {get;set;}
        [OrderBy(false)]
        public string education_type_code {get;set;}
        public string education_type_name_th {get;set;}
        public string education_type_name_en {get;set;}
        public string remark {get;set;}
        public string education_type_level_code { get; set; }
        public Guid? education_type_level_uid { get; set; }
        public Guid? dynamic_fee_education_type_uid { get; set; }
        // [JsonIgnore]
        // public education_type_level educationTypeLevel {get;set;}

        //public List<faculty> faculties {get;set;}
    }

    //[GeneratedIdController("api/organization/education_type")]
    [GeneratedUidController("api/organization/education_type")]
    public class t_education_type : education_type
    {

    }
    public class v_education_type: education_type
    {

        // [Key]
        // public int? education_type_id {get;set;}
        // public int? education_type_level_id {get;set;}
        // public string education_type_code {get;set;}
        // public string education_type_name_th {get;set;}
        // public string education_type_name_en {get;set;}
        // public string status {get;set;}
        // public string update_by {get;set;}
        // public DateTime? update_datetime {get;set;}
        // public string update_program {get;set;}
        // public string create_by {get;set;}
        // public DateTime? create_datetime {get;set;}
        // public string remark {get;set;}
        // public string education_type_level_code {get;set;}
        // public string education_type_level_name_th {get;set;}
        // public string education_type_level_name_en {get;set;}
        // [NotMapped]
        // public string search {get;set;}
        // [JsonIgnore]
        // public education_type_level educationTypeLevel {get;set;}

        //public List<faculty> faculties {get;set;}
    }
}
