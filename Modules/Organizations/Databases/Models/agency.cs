using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using SeventyOneDev.Utilities;
using SeventyOneDev.Utilities.Attributes;

namespace edu_api.Modules.Organizations.Databases.Models
{
  public class agency : base_table
    {
        [Key]
        public Guid? agency_uid { get; set; }
        public short? agency_type_id { get; set; }
        public short? agency_group_id { get; set; }
        [Search]
        public string agency_code { get; set; }
        [Search]
        public string agency_name_en { get; set; }
        [Search]
        public string agency_name_th { get; set; }
        [Search]
        public string agency_short_name_en { get; set; }
        [Search]
        public string agency_short_name_th { get; set; }
        public DateTime? start_date { get; set; }
        public DateTime? end_date { get; set; }
        public Guid? parent_agency_uid { get; set; }
        public short? agency_level_id { get; set; }
        public bool? is_internal { get; set; }
        public bool? is_approve { get; set; }
        public Guid? agency_key_uid { get; set; }
        public Guid? parent_agency_key_uid { get; set; }
    }


    public class v_agency : agency
    {
    }
}
