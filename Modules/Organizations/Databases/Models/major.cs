using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using SeventyOneDev.Utilities;
using SeventyOneDev.Utilities.Attributes;

namespace edu_api.Modules.Organizations.Databases.Models
{
  public class major:base_table
    {
        [Key,DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int? major_id {get;set;}
        //public int? education_type_id {get;set;}
        public int? faculty_id {get;set;}
        public string major_code {get;set;}
        public string major_name_th {get;set;}
        public string major_name_en {get;set;}
        public string remark {get;set;}
        public string education_type_code {get;set;}

    }
  [GeneratedIdController("api/organization/major")]
  public class t_major:major
  {
    // [JsonIgnore]
    // public faculty faculty {get;set;}
  }
    public class v_major:major
    {
        // [Key]
        // public int? major_id {get;set;}
        // public int? education_type_id {get;set;}
        // public int? faculty_id {get;set;}
        // public string major_code {get;set;}
        // public string major_name_th {get;set;}
        // public string major_name_en {get;set;}
        // public string status {get;set;}
        // public string update_by {get;set;}
        // public DateTime? update_datetime {get;set;}
        // public string update_program {get;set;}
        // public string create_by {get;set;}
        // public DateTime? create_datetime {get;set;}
        // public string remark {get;set;}
        // public string education_type_level_code {get;set;}
        // public string education_type_level_name_th {get;set;}
        // public string education_type_level_name_en {get;set;}
        // public string education_type_code {get;set;}
        // public string education_type_name_th {get;set;}
        // public string education_type_name_en {get;set;}
        // public string faculty_code {get;set;}
        // public string faculty_name_th {get;set;}
        // public string faculty_name_en {get;set;}
        // [NotMapped]
        // public string search {get;set;}
        // [JsonIgnore]
        // public faculty faculty {get;set;}
    }
}
