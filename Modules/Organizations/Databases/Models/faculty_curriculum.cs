using System;
using System.ComponentModel.DataAnnotations;
using SeventyOneDev.Utilities;
using SeventyOneDev.Utilities.Attributes;

namespace edu_api.Modules.Organizations.Databases.Models
{
  public class faculty_curriculum:base_table
  {
    [Key]
    public Guid? faculty_curriculum_uid { get; set; }
    //public int? faculty_curriculum_id {get;set;}
    public Guid? education_type_uid { get; set; }
    public Guid? college_faculty_uid { get; set; }
    //public int? education_type_id {get;set;}
    //public int? faculty_id {get;set;}
    public string faculty_curriculum_code {get;set;}
    public string faculty_curriculum_name_th {get;set;}
    public string faculty_curriculum_name_en {get;set;}
    public string remark {get;set;}
    public string education_type_code {get;set;}
    public string faculty_code { get; set; }

  }
  [GeneratedUidController("api/organization/faculty_curriculum")]
  public class t_faculty_curriculum : faculty_curriculum
  {

  }

  public class v_faculty_curriculum : faculty_curriculum
  {
    //public string question_group_name_th { get; set; }
    //public string question_set_name_th { get; set; }

  }
}
