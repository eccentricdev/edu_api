using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using SeventyOneDev.Utilities;
using SeventyOneDev.Utilities.Attributes;

namespace edu_api.Modules.Organizations.Databases.Models
{
  public class college_faculty:base_table
    {
        [Key]
        public Guid? college_faculty_uid { get; set; }
        //public int? faculty_id {get;set;}
        //public int? education_type_id {get;set;}
        public string college_faculty_code {get;set;}
        public string college_faculty_name_th {get;set;}
        public string college_faculty_name_en {get;set;}

        public string remark {get;set;}
        public Guid? agency_uid { get; set; }

    }
  [GeneratedUidController("api/organization/college_faculty")]
  public class t_college_faculty : college_faculty
  {
    // [JsonIgnore]
    // //public education_type educationType {get;set;}
    // public List<major> majors {get;set;}
  }
    public class v_college_faculty:college_faculty

    {
        // [Key]
        // public int? faculty_id {get;set;}
        // public int? education_type_id {get;set;}
        // public string faculty_code {get;set;}
        // public string faculty_name_th {get;set;}
        // public string faculty_name_en {get;set;}
        // public string status {get;set;}
        // public string update_by {get;set;}
        // public DateTime? update_datetime {get;set;}
        // public string update_program {get;set;}
        // public string create_by {get;set;}
        // public DateTime? create_datetime {get;set;}
        // public string remark {get;set;}
        // public string education_type_level_code {get;set;}
        // public string education_type_level_name_th {get;set;}
        // public string education_type_level_name_en {get;set;}
        // public string education_type_code {get;set;}
        // public string education_type_name_th {get;set;}
        // public string education_type_name_en {get;set;}
        // [NotMapped]
        // public string search {get;set;}
        //[JsonIgnore]
        //public education_type educationType {get;set;}
        // public List<major> majors {get;set;}
    }
}
