using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using SeventyOneDev.Utilities;
using SeventyOneDev.Utilities.Attributes;

namespace edu_api.Modules.Organizations.Databases.Models
{
  public class education_type_level:base_table
  {
    [Key,DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public Guid? education_type_level_uid { get; set; }
    public string education_type_level_code {get;set;}
    public string education_type_level_name_th {get;set;}
    public string education_type_level_name_en {get;set;}
  }
  [GeneratedUidController("api/organization/education_type_level")]
  public class t_education_type_level : education_type_level
  {

  }

  public class v_education_type_level : education_type_level
  {

  }
}
