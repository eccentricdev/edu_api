using edu_api.Modules.Commons.Services;
using edu_api.Modules.Organizations.Services;
using Microsoft.Extensions.DependencyInjection;

namespace edu_api.Modules.Organizations.Configures
{
  public static class OrganizationCollectionExtension
  {
    public static IServiceCollection AddOrganizationServices(this IServiceCollection services)
    {
      services.AddScoped<AgencyService>();

      return services;
    }
  }
}
