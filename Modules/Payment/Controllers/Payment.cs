using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using edu_api.Modules.Payment.Services;
using Microsoft.AspNetCore.Mvc;
using payment_core.Models;

namespace edu_api.Modules.Payment.Controllers
{
    [ApiController]
    [Route("api/payment/payment", Name = "payment")]
    public class PaymentController:ControllerBase
    {
        private readonly PaymentService _paymentService;

        public PaymentController(PaymentService paymentService)
        {
            _paymentService = paymentService;
        }

        [HttpPost("{student_uid}/{amount}")]
        public async Task<ActionResult<bill>> NewBill([FromRoute] Guid student_uid, [FromRoute] decimal amount,
            [FromBody] List<bill_request_item> billRequestItems)
        {
            //bill_uid,bill_no
            var result=await _paymentService.CreateBill(Guid.Empty, 1000, new List<bill_request_item>()
            {
                new bill_request_item(){}
            });
            
            return Ok();
        }
    }
}