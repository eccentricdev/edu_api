using System.Threading.Tasks;
using edu_api.Modules.Payment.Models;
using Microsoft.AspNetCore.Mvc;
using SeventyOneDev.Utilities;

namespace edu_api.Modules.Payment.Controllers
{
  [ApiController]
  [Route("/api/payment/debt",Name="debt")]
  public class DebtController:ControllerBase
  {
    private readonly Db _db;
    public DebtController(Db _db)
    {
      _db = _db;
    }

    [HttpGet("{student_code}")]
    public ActionResult<debt_response> CheckStudentDebt([FromRoute] string student_code)
    {
      return Ok(new debt_response() { has_debt = false });
    }
  }
}
