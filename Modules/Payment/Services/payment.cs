using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;
using edu_api.Modules.Register.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using payment_core.Models;
using payment_core.Utilities;
using SeventyOneDev.Utilities;

namespace edu_api.Modules.Payment.Services
{
    public class PaymentService
    {
      private readonly Db _db;
      private readonly IHttpClientFactory _httpClientFactory;
      private readonly open_id_setting _openIdSetting;
      public PaymentService(Db db,IHttpClientFactory httpClientFactory,IOptions<open_id_setting> openIdSetting)
      {
        _db = db;
        _httpClientFactory = httpClientFactory;
        _openIdSetting = openIdSetting.Value;
      }
      public async Task<bill> CreateBill(Guid studentUid, decimal amount,List<bill_request_item> items)
     {
       try
       {
         var student = await _db.v_student.AsNoTracking().FirstOrDefaultAsync(s => s.student_uid == studentUid);
         var newBill = new bill_request(
           DateTime.Today,
           15,
           1,
           student.student_code,
           student.display_name_th,
           "",
           // applicant.address1 + " " + (string.IsNullOrEmpty(applicant.soi) ? "" : applicant.soi) + " " +
           // (string.IsNullOrEmpty(applicant.road) ? "" : applicant.road) + " " +
           // (string.IsNullOrEmpty(applicant.sub_district_name) ? "" : applicant.sub_district_name) + " " +
           // (string.IsNullOrEmpty(applicant.district_name) ? "" : applicant.district_name) + " " +
           // (string.IsNullOrEmpty(applicant.province_name) ? "" : applicant.province_name),
           "",
           student.citizen_id,
           null,
           null,
           student.rsu_email,
           student.mobile_no,
           DateTime.Today.AddDays(15),
           "",
           amount,
           0, 0,
           amount,
           //applicantApply.payment_amount.Value, 0, 0,
           //applicantApply.payment_amount.Value,
           "", 0);
         newBill.customer_uid = student.student_uid;



         newBill.bill_items = items; //items.Select(i => new bill_request_item()).ToList();
         var iamHttpClient = _httpClientFactory.CreateClient("open_id");
         //Console.WriteLine("Client ID:" + _openIdSetting.client_id);
         var dict = new Dictionary<string, string>
         {
           { "grant_type", "client_credentials" },
           { "scope", string.Join(" ", _openIdSetting.scopes) },
           { "client_id", _openIdSetting.client_id },
           { "client_secret", _openIdSetting.client_secret }
         };
         var req = new HttpRequestMessage(HttpMethod.Post, _openIdSetting.authority + "/connect/token")
           { Content = new FormUrlEncodedContent(dict) };
         var iamResponse = await iamHttpClient.SendAsync(req);
         if (iamResponse.StatusCode == HttpStatusCode.OK)
         {
           var iamResult = await iamResponse.Content.ReadAsByteArrayAsync();
           var openIdToken = JsonSerializer.Deserialize<open_id_token>(iamResult);
           //Console.WriteLine("OpenID token:" + JsonSerializer.Serialize(openIdToken));
           var httpClient = _httpClientFactory.CreateClient("ar_payment");
           JsonSerializerOptions options = new JsonSerializerOptions();
           options.Converters.Add(new DateTimeConverterUsingDateTimeParse());
           options.IgnoreNullValues = true;
           Console.WriteLine("Bill Request:" + JsonSerializer.Serialize(newBill, options));
           Console.WriteLine("Token:" + openIdToken.access_token);
           var data = new ByteArrayContent(JsonSerializer.SerializeToUtf8Bytes(newBill, options));
           var httpRequestMessage = new HttpRequestMessage
           {
             Method = HttpMethod.Post,
             RequestUri = new Uri(httpClient.BaseAddress + "v1/accounts_receivables/bills")
           };

           httpRequestMessage.Headers.Add("Authorization", "Bearer " + openIdToken.access_token);
           httpRequestMessage.Headers.Add(HttpRequestHeader.ContentType.ToString(), "application/json");
           HttpResponseMessage res = null;
           data.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");
           httpRequestMessage.Content = data;
           res = await httpClient.SendAsync(httpRequestMessage);

           if (res.StatusCode == HttpStatusCode.Created)
           {
             // var provisionRegister =
             //   await _context.t_provision_register.FirstOrDefaultAsync(p =>
             //     p.provision_register_uid == provisioningRegisterUid);
             var result = await res.Content.ReadAsByteArrayAsync();
             var createdBill = JsonSerializer.Deserialize<bill>(result);
             return createdBill;
             // Console.WriteLine("Bill:" + JsonSerializer.Serialize(createdBill));
             // provisionRegister.bill_no = createdBill.bill_no;
             // //applicantApply.application_invoice_due_date = createdBill.payment_due_date;
             // provisionRegister.bill_uid = createdBill.bill_uid;
             // //createdBill.bill_url
             // await _context.SaveChangesAsync();
           }
           else
           {
             Console.WriteLine("Create bill:" + res.StatusCode.ToString());
           }
         }


       }
       catch (Exception e)
       {
         Console.WriteLine(e);
         return null;

       }

       return null;
     }
    }
}