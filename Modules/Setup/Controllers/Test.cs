using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;
using System.Xml.Schema;
using edu_api.Modules.Fee.Databases.Models;
using edu_api.Modules.Register.Databases;
using edu_api.Modules.Register.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using payment_core.Models;
using payment_core.Utilities;
using SeventyOneDev.Utilities;

namespace edu_api.Modules.Setup.Controllers
{
  [Route("api/setup/test", Name = "test")]
  public class TestController:Controller
  {
    private readonly Db _db;
    private readonly IHttpClientFactory _httpClientFactory;
    private readonly open_id_setting _openIdSetting;

    public TestController(Db db,IHttpClientFactory httpClientFactory,IOptions<open_id_setting> openIdSetting)
    {
      _db = db;
      _httpClientFactory = httpClientFactory;
      _openIdSetting = openIdSetting.Value;
    }
    [HttpGet("advance/{student_code}/{amount}")]
    public async Task<ActionResult> SetAdvance([FromRoute]string student_code,[FromRoute]string amount)
    {
      var studentUid = _db.t_student.FirstOrDefault(s => s.student_code == student_code)?.student_uid;
      var advancePayments = await _db.t_advance_payment.Where(s => s.student_uid == studentUid).ToListAsync();
      if (advancePayments.Any())
      {
        _db.t_advance_payment.RemoveRange(advancePayments);
        await _db.SaveChangesAsync();
      }

      var advanceAmounts = amount.Split("|");
      var no = 1;
      foreach (var advanceAmount in advanceAmounts)
      {
        var advancePayment = new t_advance_payment()
        {
          advance_amount = decimal.Parse(advanceAmount),
          is_use = false,
          left_amount = decimal.Parse(advanceAmount),
          student_uid = studentUid,
          advance_date = DateTime.Today,
          advance_no = student_code + no
        };
        await _db.t_advance_payment.AddAsync(advancePayment);
        no += 1;

      }

      await _db.SaveChangesAsync();
      return Ok("Add advanced payment success");
    }

    [HttpGet("package/{year_code}/{curriculum_code}/{semester_code}/{amount}")]
    public async Task<ActionResult> SetPackage([FromRoute] string year_code, [FromRoute] string curriculum_code,
      [FromRoute] string semester_code, [FromRoute] decimal amount)
    {
      var yearUid = _db.t_academic_year.AsNoTracking().FirstOrDefault(y => y.academic_year_code == year_code)?
        .academic_year_uid;
      var curriculum =
        _db.t_curriculum.FirstOrDefault(c => c.curriculum_code == curriculum_code && c.curriculum_year_uid == yearUid);
      if (curriculum != null)
      {
        curriculum.is_package = true;
        switch (semester_code)
        {
          case "1":
            curriculum.semester_1_amount = amount;
            break;
          case "2":
            curriculum.semester_2_amount = amount;
            break;
          case "3":
            curriculum.semester_3_amount = amount;
            break;
        }

        await _db.SaveChangesAsync();
        return Ok("Update package success");
      }
      else
      {
        return BadRequest("ไม่พบหลักสูตรที่ต้องการ");
      }
    }
    [HttpGet("loan/{student_code}/{year_code}/{semester_code}/{amount}")]
    public async Task<ActionResult> SetLoan([FromRoute]string student_code,[FromRoute]string year_code,[FromRoute]string semester_code,  [FromRoute]decimal amount)
    {
      var studentUid = _db.t_student.FirstOrDefault(s => s.student_code == student_code)?.student_uid;
      var yearUid = _db.t_academic_year.AsNoTracking().FirstOrDefault(y => y.academic_year_code == year_code)?
        .academic_year_uid;
      var semesterUid = _db.t_semester.AsNoTracking().FirstOrDefault(y => y.semester_code == semester_code)?
        .semester_uid;
      var loan = await _db.t_loan.AsNoTracking().FirstOrDefaultAsync(l => l.student_uid == studentUid && l.academic_year_uid==yearUid);
      if (loan==null)
      {
        loan = new t_loan()
        {
          student_uid = studentUid,
          academic_year_uid = yearUid,
          contract_no = "00000000"

        };
        await _db.t_loan.AddAsync(loan);
        await _db.SaveChangesAsync();
        var loanSemester = new t_loan_semester()
        {
          loan_uid = loan.loan_uid,
          semester_uid = semesterUid,
          loan_amount = amount,
          use_amount = 0
        };
        await _db.t_loan_semester.AddAsync(loanSemester);
        await _db.SaveChangesAsync();
        return Ok("Add new loan success");




      }
      else
      {
        var loanSemester =
          await _db.t_loan_semester.FirstOrDefaultAsync(s =>
            s.loan_uid == loan.loan_uid && s.semester_uid == semesterUid);
        if (loanSemester == null)
        {
          loanSemester = new t_loan_semester()
          {
            loan_uid = loan.loan_uid,
            semester_uid = semesterUid,
            loan_amount = amount,
            use_amount = 0
          };
          await _db.t_loan_semester.AddAsync(loanSemester);
          await _db.SaveChangesAsync();
          return Ok("Add new loan semester success");
        }
        else
        {
          loanSemester.loan_amount = amount;
          loanSemester.use_amount = 0;
          await _db.SaveChangesAsync();
          return Ok("Update loan success");
        }

      }






    }
    [HttpGet("register_paid/{student_code}/{year_code}/{semester_code}")]
    public async Task<ActionResult> RegisterPaid([FromRoute] string student_code, [FromRoute] string year_code,
      [FromRoute] string semester_code)
    {
      var studentUid = _db.t_student.FirstOrDefault(s => s.student_code == student_code)?.student_uid;
      var academicYearUid =
        _db.t_academic_year.AsNoTracking().FirstOrDefault(a => a.academic_year_code == year_code)?.academic_year_uid;
      var academicSemesterUid = _db.t_academic_semester.AsNoTracking()
        .FirstOrDefault(s => s.academic_year_uid == academicYearUid && s.academic_semester_code == semester_code)?
        .academic_semester_uid;

      var provisionRegister = _db.t_provision_register.FirstOrDefault(r =>
        r.student_uid == studentUid && r.academic_year_uid == academicYearUid && r.academic_semester_uid == academicSemesterUid);
      if (provisionRegister == null) return BadRequest("Cannot find register");
      if (provisionRegister.register_status_id != 2) return BadRequest("Invalid register status");

      var receiptRequest = new receipt_request()
      {
        bill_uid = provisionRegister.bill_uid,
        payment_receives = new List<payment_receive>()
        {
          new payment_receive() { payment_method_id = 21, receive_amount = provisionRegister.payment_amount }
        }
      };
      Console.WriteLine("Receipt for:");
      Console.WriteLine(JsonSerializer.Serialize(receiptRequest));
      var iamHttpClient = _httpClientFactory.CreateClient("open_id");
         //Console.WriteLine("Client ID:" + _openIdSetting.client_id);
         var dict = new Dictionary<string, string>
         {
           { "grant_type", "client_credentials" },
           { "scope", string.Join(" ", _openIdSetting.scopes) },
           { "client_id", _openIdSetting.client_id },
           { "client_secret", _openIdSetting.client_secret }
         };
         var req = new HttpRequestMessage(HttpMethod.Post, _openIdSetting.authority + "/connect/token")
           { Content = new FormUrlEncodedContent(dict) };
         var iamResponse = await iamHttpClient.SendAsync(req);
         if (iamResponse.StatusCode == HttpStatusCode.OK)
         {
           var iamResult = await iamResponse.Content.ReadAsByteArrayAsync();
           var openIdToken = JsonSerializer.Deserialize<open_id_token>(iamResult);
           //Console.WriteLine("OpenID token:" + JsonSerializer.Serialize(openIdToken));
           var httpClient = _httpClientFactory.CreateClient("ar_payment");
           JsonSerializerOptions options = new JsonSerializerOptions();
           options.Converters.Add(new DateTimeConverterUsingDateTimeParse());
           options.IgnoreNullValues = true;
           Console.WriteLine("Token:" + openIdToken.access_token);
           var data = new ByteArrayContent(JsonSerializer.SerializeToUtf8Bytes(receiptRequest, options));
           var httpRequestMessage = new HttpRequestMessage
           {
             Method = HttpMethod.Post,
             RequestUri = new Uri(httpClient.BaseAddress + "v1/accounts_receivables/receipts/offline")
           };

           httpRequestMessage.Headers.Add("Authorization", "Bearer " + openIdToken.access_token);
           httpRequestMessage.Headers.Add(HttpRequestHeader.ContentType.ToString(), "application/json");
           HttpResponseMessage res = null;
           data.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");
           httpRequestMessage.Content = data;
           res = await httpClient.SendAsync(httpRequestMessage);

           if (res.StatusCode == HttpStatusCode.Created)
           {
             var result = await res.Content.ReadAsByteArrayAsync();
             var receipt = JsonSerializer.Deserialize<receipt>(result);
             Console.WriteLine("Receipt");
             Console.WriteLine(JsonSerializer.Serialize(receipt));
             //var provisionRegister = await _context.t_provision_register.FirstOrDefaultAsync(a => a.bill_no == billNo);
               provisionRegister.is_paid = true;
               var finalRegister = new t_final_register(provisionRegister);
               //finalRegister.final_register_subjects = new List<t_final_register_subject>()
               var provisionRegisterSubjects = await _db.t_provision_register_subject
                 .Where(p => p.provision_register_uid == provisionRegister.provision_register_uid).ToListAsync();
               finalRegister.final_register_subjects=provisionRegisterSubjects.Select(p=>new t_final_register_subject(p)).ToList();
               await _db.AddAsync(finalRegister);
               await _db.SaveChangesAsync();

             return Ok();
             //return paymentMethods;
           }
           else
           {
             Console.WriteLine(res.StatusCode.ToString());
             return BadRequest();
           }
         }
      return Ok();
    }

  }
}
