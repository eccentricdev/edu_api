using System;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using edu_api.Modules.Commons.Databases.Models;
using Microsoft.EntityFrameworkCore;
using SeventyOneDev.Utilities;

namespace edu_api.Modules.Commons.Services
{
    public class HospitalService:EntityIdService<t_hospital,v_hospital>
    {
        private readonly Db _context;
        public HospitalService(Db context) : base(context)
        {
            _context = context;
        }

        public Task<int> DeleteHospital(int id, ClaimsIdentity claimsIdentity)
        {
            var studentIllness = _context.v_student_illness.AsNoTracking()
                .Where(w => w.hospital_id == id).ToList();
            
            if (studentIllness.Count > 0) throw new Exception("INUSE");
            return base.DeleteById(id, claimsIdentity);
        }
        
    }
}