using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using edu_api.Modules.Commons.Databases.Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SeventyOneDev.Utilities;

namespace edu_api.Modules.Commons.Services
{
    public class CaseFailedService:EntityIdService<t_cause_failed,v_cause_failed>
    {
        private readonly Db _context;
        public CaseFailedService(Db context) : base(context)
        {
            _context = context;
        }

        public Task<int> DeleteCauseFailed(int id, ClaimsIdentity claimsIdentity)
        {
            var studentEducationChecks = _context.v_student_education_check.AsNoTracking()
                .Where(w => w.cause_failed_id == id).ToList();
            if (studentEducationChecks.Count > 0) throw new Exception("INUSE");
            return base.DeleteById(id, claimsIdentity);
        }

        public async Task<t_cause_failed> NewCauseFailed(t_cause_failed entity, ClaimsIdentity claimsIdentity)
        {
            entity.cause_failed_code = entity.cause_failed_code.Trim();
            entity.cause_failed_name_th = entity.cause_failed_name_th.Trim();

            var causeFailed = await _context.v_cause_failed.AsNoTracking().FirstOrDefaultAsync(w =>
                w.cause_failed_code == entity.cause_failed_code ||
                w.cause_failed_name_th == entity.cause_failed_name_th);
            if(causeFailed != null ) throw new Exception("NEW_DUPLICATE");
            return await base.NewEntity(entity, claimsIdentity);
        }

        public async Task<int> UpdateCauseFailed(t_cause_failed entity, ClaimsIdentity claimsIdentity)
        {
            entity.cause_failed_code = entity.cause_failed_code.Trim();
            entity.cause_failed_name_th = entity.cause_failed_name_th.Trim();
            
            var causeFailed = await _context.v_cause_failed.AsNoTracking().FirstOrDefaultAsync(w =>
                w.cause_failed_code == entity.cause_failed_code);
            if (causeFailed == null || causeFailed.cause_failed_id == entity.cause_failed_id)
            {
                causeFailed = await _context.v_cause_failed.AsNoTracking().FirstOrDefaultAsync(w =>
                    w.cause_failed_name_th == entity.cause_failed_name_th);
            }

            if (causeFailed != null)
            {
                if(!causeFailed.cause_failed_id.Equals(entity.cause_failed_id))throw new Exception("NEW_DUPLICATE");
            }
            
            return await base.UpdateEntity(entity, claimsIdentity);
        }
    }
}