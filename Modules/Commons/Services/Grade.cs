using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using edu_api.Modules.Commons.Databases.Models;
using SeventyOneDev.Utilities;

namespace edu_api.Modules.Commons.Services
{
    public class GradeService:EntityUidService<t_grade,v_grade>
    {
        private readonly Db _context;
        public GradeService(Db context) : base(context)
        {
            _context = context;
        }


        public override async Task<List<v_grade>> ListEntityId(v_grade entity, List<Guid?> agencies = null, string userName = null)
        {
            var rs = await base.ListEntityId(entity, agencies, userName);

            if (entity != null && entity.grade_point != null)
            {
                rs = rs.Where(w => w.grade_point == entity.grade_point).ToList();
            }

            if (entity != null && entity.grade_point == 0)
            {
                rs = rs.Where(w => w.grade_point == 0).ToList();
            }

            return rs;
        }
    }
}