using System;
using System.Collections.Generic;
using edu_api.Modules.Commons.Databases.Models;

namespace edu_api.Modules.Commons.Initials
{
  public class Grade
  {
    public static IEnumerable<t_grade> Get()
    {
      return new[]{
        new t_grade(Guid.Parse(""),"X","Not specified","ไม่ระบุ",false,false,99,"",0),
        // A	4	Y	Y		1	Y
        // AU	0	N	N		14	Y
        // B	3	Y	Y		3	Y
        // B+	3.5	Y	Y		2	Y
        // C	2	Y	Y		5	Y
        // C+	2.5	Y	Y		4	Y
        new t_grade(Guid.Parse(""),"A","A","A",true,true,1,"",4),
        new t_grade(Guid.Parse(""),"AU","AU","AU",false,false,14,"",0),
        new t_grade(Guid.Parse(""),"B","B","B",true,true,3,"",3),
        new t_grade(Guid.Parse(""),"B+","B+","B+",true,true,2,"",3.5M),
        new t_grade(Guid.Parse(""),"C","C","C",true,true,5,"",2),
        new t_grade(Guid.Parse(""),"C+","C+","C+",false,false,4,"",2.5M),

        // CE	0	N	N		16	Y
        // CP	0	N	N		17	Y
        // CS	0	N	N		18	Y
        // CT	0	N	N		19	Y
        // D	1	Y	Y		7	Y
        // D+	1.5	Y	Y		6	Y
        new t_grade(Guid.Parse(""),"CE","CE","CE",false,false,16,"",0),
        new t_grade(Guid.Parse(""),"CP","CP","CP",false,false,17,"",0),
        new t_grade(Guid.Parse(""),"CS","CS","CS",false,false,18,"",0),
        new t_grade(Guid.Parse(""),"CT","CT","CT",false,false,19,"",0),
        new t_grade(Guid.Parse(""),"D","D","D",true,true,7,"",1.5M),
        new t_grade(Guid.Parse(""),"D+","D+","D+",true,true,6,"",1),

        // F	0	Y	Y		8	Y
        // I	0	N	N		11	Y
        // IP	0	N	N		12	Y
        // P	1	N	Y		15	Y
        // S	1	N	Y		9	Y
        // SE	0	N	N		20	Y
        new t_grade(Guid.Parse(""),"F","F","F",true,true,8,"",0),
        new t_grade(Guid.Parse(""),"I","I","I",false,false,11,"",0),
        new t_grade(Guid.Parse(""),"IP","IP","IP",false,false,12,"",0),
        new t_grade(Guid.Parse(""),"P","P","P",false,true,15,"",1),
        new t_grade(Guid.Parse(""),"S","S","S",false,true,9,"",1),
        new t_grade(Guid.Parse(""),"SE","SE","SE",false,false,20,"",0),
        // U	0	N	Y		10	Y
        // W	0	N	N	w	13	Y
        new t_grade(Guid.Parse(""),"U","U","U",false,true,10,"",0),
        new t_grade(Guid.Parse(""),"W","W","W",false,false,13,"",0),
      };
    }





  }
}
