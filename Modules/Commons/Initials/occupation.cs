// using System.Collections.Generic;
// using rsu_common_api.models;
//
// namespace edu_api.Modules.Commons.Initials
// {
//   internal class Occupation
//   {
//     public static IEnumerable<t_occupation> Get()
//     {
//       return new[]{
//         new t_occupation(0,"0","Not specified","ไม่ระบุ"),
//         new t_occupation(1,"1","รับราชการ","Government Officer"),
//         new t_occupation(2,"2","พนักงานรัฐวิสาหกิจ","State Enterprise Employees"),
//         new t_occupation(3,"3","พนักงาน/ลูกจ้างหน่วยงานเอกชน","Private Employees"),
//         new t_occupation(4,"4","ทำธุรกิจส่วนตัว","Private business"),
//         new t_occupation(5,"5","อื่น ๆ","Other"),
//       };
//     }
//   }
// }
