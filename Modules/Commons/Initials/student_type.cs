using System.Collections.Generic;
using edu_api.Modules.Commons.Databases.Models;
using rsu_common_api.models;

namespace rsu_common_api.constants
{
  internal class StudentType
  {
    public static IEnumerable<t_student_type> Get()
    {
      return new[]{
        new t_student_type(0,"0","Not specified","ไม่ระบุ"),
        new t_student_type(1,"1","นักศึกษาปกติ","นักศึกษาปกติ"),
        new t_student_type(2,"2","นักศึกษา ก.ว.","นักศึกษา ก.ว."),
        new t_student_type(3,"3","นักศึกษาต่างสถาบัน","นักศึกษาต่างสถาบัน"),
        new t_student_type(4,"4","นักศึกษาโครงการแลกเปลี่ยน","นักศึกษาโครงการแลกเปลี่ยน")
      };
    }
  }
}
