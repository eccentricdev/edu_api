// using System.Collections.Generic;
// using edu_api.Modules.Commons.Databases.Models;
//
// namespace edu_api.Modules.Commons.Initials
// {
//   internal class Religion
//   {
//     public static IEnumerable<t_religion> Get()
//     {
//       return new[]{
//         new t_religion(0,"X","Not specified","ไม่ระบุ"),
//         new t_religion(1,"B","Christianity","พุทธ"),
//         new t_religion(2,"C","Married","คริสต์"),
//         new t_religion(3,"I","Islam","อิสลาม"),
//         new t_religion(4,"N","Other","อื่น ๆ"),
//       };
//     }
//   }
// }
