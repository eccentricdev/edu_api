using System.Collections.Generic;
using edu_api.Modules.Commons.Databases.Models;

namespace edu_api.Modules.Commons.Initials
{
  internal class FamilyStatus
  {
    public static IEnumerable<t_family_status> Get()
    {
      return new[]{
        new t_family_status(0,"0","Not specified ","ไม่ระบุ"),
        new t_family_status(1,"1","Stay together ","อยู่ด้วยกัน"),
        new t_family_status(2,"2","Separated","แยกกันอยู่"),
        new t_family_status(3,"3","Divorced","หย่าร้าง"),
        new t_family_status(4,"4","Father remarried","บิดาแต่งงานใหม่"),
        new t_family_status(5,"5","Mother remarried","มารดาแต่งงานใหม่"),
        new t_family_status(6,"6","Father and mothor remarried","บิดาและมารดาแต่งงานใหม่"),
        new t_family_status(7,"7","Other","อื่น ๆ"),
      };
    }
  }
}
