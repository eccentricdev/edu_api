using System.Collections.Generic;
using edu_api.Modules.Commons.Databases.Models;

namespace edu_api.Modules.Commons.Initials
{
  internal class AdvanceType
  {
    public static IEnumerable<t_advance_type> Get()
    {
      return new[]{
        new t_advance_type(0,"X","Not specified","ไ่ม่ระบุ"),
        new t_advance_type(1,"C","Cash","เงินสด"),
        new t_advance_type(2,"L","Loan","ทุนกยส"),
      };
    }
  }
}
