// using System.Collections.Generic;
// using edu_api.Modules.Commons.Databases.Models;
//
// namespace edu_api.Modules.Commons.Initials
// {
//   internal class MilitaryStatus
//   {
//     public static IEnumerable<t_military_status> Get()
//     {
//       return new[]{
//         new t_military_status(0,"X","Not specified","ไม่ระบุ"),
//         new t_military_status(1,"N","Not Exempted","ยังไม่ได้เกณฑ์ทหาร"),
//         new t_military_status(2,"E","Exempted","ได้รับการยกเว้น"),
//         new t_military_status(3,"C","Conscripted","เคยเกณฑ์ทหาร"),
//         new t_military_status(4,"D","Discharged","ปลดประจำการ"),
//         new t_military_status(5,"S","Serving","อยู่ในระหว่างการเป็นทหารเกณฑ์"),
//       };
//     }
//   }
// }
