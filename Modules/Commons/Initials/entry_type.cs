using System.Collections.Generic;
using edu_api.Modules.Commons.Databases.Models;

namespace edu_api.Modules.Commons.Initials
{
  internal class EntryType
  {
    public static IEnumerable<t_entry_type> Get()
    {
      return new[]{
        new t_entry_type(0,"0","Not specified","ไม่ระบุ"),
        new t_entry_type(1,"1","Ministry of University Affairs","ทบวงมหาวิทยาลัย"),
        new t_entry_type(2,"2","Rangsit university","มหาวิทยาลัยรังสิต"),
        new t_entry_type(3,"3","Other","อื่น ๆ")
      };
    }
  }
}
