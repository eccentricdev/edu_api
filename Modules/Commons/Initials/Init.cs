using System.Linq;
using SeventyOneDev.Utilities;
using edu_api.Databases;
using rsu_common_api.constants;

namespace edu_api.Modules.Commons.Initials
{
  public static class CommonInit
  {
    public static void Run(Db context)
    {
      if (!context.t_status.Any())
      {
        context.t_status.AddRange(Status.Get());
        context.SaveChanges();
      }
      if (!context.t_advance_status.Any())
      {
        context.t_advance_status.AddRange(AdvanceStatus.Get());
        context.SaveChanges();
      }
      if (!context.t_advance_type.Any())
      {
        context.t_advance_type.AddRange(AdvanceType.Get());
        context.SaveChanges();
      }
      // if (!context.t_blood_group.Any())
      // {
      //   context.t_blood_group.AddRange(BloodGroup.Get());
      //   context.SaveChanges();
      // }


      if (!context.t_entry_type.Any())
      {
        context.t_entry_type.AddRange(EntryType.Get());
        context.SaveChanges();
      }
      if (!context.t_family_status.Any())
      {
        context.t_family_status.AddRange(FamilyStatus.Get());
        context.SaveChanges();
      }
      // if (!context.t_gender.Any())
      // {
      //   context.t_gender.AddRange(Gender.Get());
      //   context.SaveChanges();
      // }
      // if (!context.t_grade.Any())
      // {
      //   context.t_grade.AddRange(Grade.Get());
      //   context.SaveChanges();
      // }
      if (!context.t_honor_status.Any())
      {
        context.t_honor_status.AddRange(HonorStatus.Get());
        context.SaveChanges();
      }

      if (!context.t_income_level.Any())
      {
        context.t_income_level.AddRange(IncomeLevel.Get());
        context.SaveChanges();
      }
      // if (!context.t_marital_status.Any())
      // {
      //   context.t_marital_status.AddRange(MaritalStatus.Get());
      //   context.SaveChanges();
      // }
      // if (!context.t_military_status.Any())
      // {
      //   context.t_military_status.AddRange(MilitaryStatus.Get());
      //   context.SaveChanges();
      // }
      // if (!context.t_occupation.Any())
      // {
      //   context.t_occupation.AddRange(Occupation.Get());
      //   context.SaveChanges();
      // }
      // if (!context.t_religion.Any())
      // {
      //   context.t_religion.AddRange(Religion.Get());
      //   context.SaveChanges();
      // }
      if (!context.t_school_level.Any())
      {
        context.t_school_level.AddRange(SchoolLevel.Get());
        context.SaveChanges();
      }


      if (!context.t_student_type.Any())
      {
        context.t_student_type.AddRange(StudentType.Get());

        context.SaveChanges();
      }
      if (!context.t_study_type.Any())
      {
        context.t_study_type.AddRange(StudyType.Get());
        context.SaveChanges();
      }
      if (!context.t_support_by.Any())
      {
        context.t_support_by.AddRange(SupportBy.Get());
        context.SaveChanges();
      }
      if (!context.t_support_per_month.Any())
      {
        context.t_support_per_month.AddRange(SupportPerMonth.Get());
        context.SaveChanges();
      }

    }
  }
}
