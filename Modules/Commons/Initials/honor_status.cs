using System.Collections.Generic;
using rsu_common_api.models;

namespace edu_api.Modules.Commons.Initials
{
  internal class HonorStatus
  {
    public static IEnumerable<t_honor_status> Get()
    {
      return new[]{
        new t_honor_status(0,"0","Not specified","ไม่ระบุ"),
        new t_honor_status(1,"1","1st class honors","เกียรตินิยม อันดับ 1"),
        new t_honor_status(2,"2","2nd class honors","เกียรตินิยม อันดับ 2"),
        new t_honor_status(3,"N","No honors","ไม่ได้รับเกียรตินิยม"),
      };
    }
  }
}
