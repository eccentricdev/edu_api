// using System.Collections.Generic;
// using rsu_common_api.models;
//
// namespace edu_api.Modules.Commons.Initials
// {
//   internal class BloodGroup
//   {
//     public static IEnumerable<t_blood_group> Get()
//     {
//       return new[]{
//         new t_blood_group(0,"X","Not specified","ไม่ระบุ"),
//         new t_blood_group(1,"A","A","เอ"),
//         new t_blood_group(2,"B","B","บี"),
//         new t_blood_group(3,"AB","AB","เอบี"),
//         new t_blood_group(4,"O","O","โอ"),
//       };
//     }
//   }
// }
