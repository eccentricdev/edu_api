using System.Collections.Generic;
using edu_api.Modules.Commons.Databases.Models;

namespace edu_api.Modules.Commons.Initials
{
  internal class AdvanceStatus
  {
    public static IEnumerable<t_advance_status> Get()
    {
      return new[]{
        new t_advance_status(0,"X","Not specified","ไ่ม่ระบุ"),
        new t_advance_status(1,"N","Unused","ยังไม่ได้ใช้"),
        new t_advance_status(2,"Y","Used","ใช้แล้ว"),
        new t_advance_status(3,"R","Reserved","จอง"),
        new t_advance_status(4,"S","Unused loan","ยังไม่ได้ใช้(กยส)"),
        new t_advance_status(5,"C","Cancel","ยกเลิก"),
      };
    }
  }
}
