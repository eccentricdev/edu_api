using System.Collections.Generic;
using edu_api.Modules.Commons.Databases.Models;

namespace edu_api.Modules.Commons.Initials
{
  internal class IncomeLevel
  {
    public static IEnumerable<t_income_level> Get()
    {
      return new[]{
        new t_income_level(0,"0","Not specified","ไม่ระบุ"),
        new t_income_level(1,"1","Lessthan 150,000 baht","ต่ำกว่า 150,000 บาทต่อปี"),
        new t_income_level(2,"2","150,001-300,000 baht","150,001-300,000 บาทต่อปี"),
        new t_income_level(3,"3","Morethan 300,000 baht","มากกว่า 300,000 บาทต่อปี"),
      };
    }
  }
}
