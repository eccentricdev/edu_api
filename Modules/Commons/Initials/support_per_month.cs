using System.Collections.Generic;
using edu_api.Modules.Commons.Databases.Models;

namespace edu_api.Modules.Commons.Initials
{
  internal class SupportPerMonth
  {
    public static IEnumerable<t_support_per_month> Get()
    {
      return new[]{
        new t_support_per_month(0,"0","Not specified","ไม่ระบุ"),
        new t_support_per_month(1,"1","ต่ำกว่า 3,000 บาทต่อเดือน","ต่ำกว่า 3,000 บาทต่อเดือน"),
        new t_support_per_month(2,"2","3,001-5,000 บาทต่อเดือน","3,001-5,000 บาทต่อเดือน"),
        new t_support_per_month(3,"3","5,001-7,000 บาทต่อเดือน","5,001-7,000 บาทต่อเดือน"),
        new t_support_per_month(4,"4","7,001-10,000 บาทต่อเดือน","7,001-10,000 บาทต่อเดือน"),
        new t_support_per_month(5,"5","มากกว่า 10,000 บาทต่อเดือน","มากกว่า 10,000 บาทต่อเดือน"),
      };
    }
  }
}
