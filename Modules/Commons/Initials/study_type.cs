using System.Collections.Generic;
using edu_api.Modules.Commons.Databases.Models;

namespace edu_api.Modules.Commons.Initials
{
  internal class StudyType
  {
    public static IEnumerable<t_study_type> Get()
    {
      return new[]{
        new t_study_type(0,"0","Not specified","ไม่ระบุ"),
        new t_study_type(1,"1","Lecture","บรรยาย"),
        new t_study_type(2,"2","Lab","ปฏิบัติ"),
         };
    }
  }
}
