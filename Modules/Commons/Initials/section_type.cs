using System.Collections.Generic;
using edu_api.Modules.Commons.Databases.Models;

namespace edu_api.Modules.Commons.Initials
{
  internal class SectionType
  {
    public static IEnumerable<t_section_type> Get()
    {
      return new[]{
        new t_section_type(1,"C","Lecture","บรรยาย"),
        new t_section_type(2,"B","Lab","ปฏิบัติ")
      };
    }
  }
}
