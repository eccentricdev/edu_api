using System.Collections.Generic;
using edu_api.Modules.Commons.Databases.Models;

namespace edu_api.Modules.Commons.Initials
{
  internal class SupportBy
  {
    public static IEnumerable<t_support_by> Get()
    {
      return new[]{
        new t_support_by(0,"0","Not specified","ไม่ระบุ"),
        new t_support_by(1,"1","พ่อ","พ่อ"),
        new t_support_by(2,"2","แม่","แม่"),
        new t_support_by(3,"3","พี่","พี่"),
        new t_support_by(4,"4","ญาติ","ญาติ"),
        new t_support_by(5,"5","อื่น ๆ","อื่น ๆ"),
      };
    }
  }
}
