// using System.Collections.Generic;
// using edu_api.Modules.Commons.Databases.Models;
//
// namespace edu_api.Modules.Commons.Initials
// {
//   internal class MaritalStatus
//   {
//     public static IEnumerable<t_marital_status> Get()
//     {
//       return new[]{
//         new t_marital_status(0,"X","Not specified","ไม่ระบุ"),
//         new t_marital_status(1,"S","Single","โสด"),
//         new t_marital_status(2,"M","Married","สมรส"),
//         new t_marital_status(3,"D","Divorced","หย่าร้าง"),
//         new t_marital_status(4,"W","Widowed","หม้าย"),
//         new t_marital_status(5,"N","Other","อื่น ๆ"),
//       };
//     }
//   }
// }
