using System.Collections.Generic;
using edu_api.Modules.Commons.Databases.Models;

namespace edu_api.Modules.Commons.Initials
{
  internal class SchoolLevel
  {
    public static IEnumerable<t_school_level> Get()
    {
      return new[]{
        new t_school_level(0,"01","Not specified","ไม่ระบุ"),
        new t_school_level(1,"02","University","มหาวิทยาลัย"),
        new t_school_level(2,"03","Others","อื่น ๆ"),
      };
    }
  }
}
