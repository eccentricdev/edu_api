// using System;
// using System.Collections.Generic;
// using System.Linq;
// using System.Linq.Expressions;
// using SeventyOneDev.Utilities.Attributes;
//
// namespace SeventyOneDev.Utilities
// {
//   public static class Extension2
//   {
//     public static IQueryable<TEntity> Search2<TEntity>(this IQueryable<TEntity> source,string searchString)
//     {
//       //List<Expression<Func<TEntity, bool>>> predicates = new List<Expression<Func<TEntity, bool>>>();
//       List<Expression<Func<TEntity, bool>>> predicates = new List<Expression<Func<TEntity, bool>>>();
//
//       var param = Expression.Parameter(typeof(TEntity), "p");
//       var objectProperties =  typeof(TEntity).GetProperties();
//       var searchProperties = objectProperties.Where(x => Attribute.IsDefined(x, typeof(SearchAttribute)));
//       BinaryExpression expr=null;
//       MethodCallExpression exp = null;
//       bool first = true;
//       foreach(var searchProperty in searchProperties)
//       {
//         if (first)
//         {
//           var method = typeof(string).GetMethod("Contains", new[] {typeof(string)});
//           if (method == null) continue;
//           exp=   Expression.Call(Expression.Property(param, searchProperty.Name),method, Expression.Convert(Expression.Constant(searchString),typeof(string)));
//           first = false;
//
//         }
//         else
//         {
//           var method = typeof(string).GetMethod("Contains", new[] {typeof(string)});
//           if (method == null) continue;
//           if (expr == null)
//           {
//             expr = Expression.Or(exp, Expression.Call(Expression.Property(param, searchProperty.Name),method, Expression.Convert(Expression.Constant(searchString),typeof(string))));
//
//           }
//           else
//           {
//             expr = Expression.Or(expr, Expression.Call(Expression.Property(param, searchProperty.Name),method, Expression.Convert(Expression.Constant(searchString),typeof(string))));
//
//           }
//         }
//       }
//       var lambda = (Expression<Func<TEntity, bool>>) Expression.Lambda(expr, param);
//
//       var result = source.Where(lambda);
//       return result;
//
//     }
//
//   }
// }
