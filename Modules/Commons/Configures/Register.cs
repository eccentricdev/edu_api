using edu_api.Modules.Commons.Services;
using Microsoft.Extensions.DependencyInjection;

namespace edu_api.Modules.Commons.Configures
{
  public static class CommonCollectionExtension
  {
    public static IServiceCollection AddCommonServices(this IServiceCollection services)
    {
      services.AddScoped<StatusService>();
      services.AddScoped<CaseFailedService>();
      services.AddScoped<HospitalService>();
      services.AddScoped<GradeService>();

      return services;
    }
  }
}
