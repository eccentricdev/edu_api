using edu_api.Modules.Commons.Databases.Models;
using edu_api.Modules.Commons.Services;
using Microsoft.AspNetCore.Mvc;
using SeventyOneDev.Utilities;

namespace edu_api.Modules.Commons.Controllers
{
    [Route("api/common/grade", Name = "grade")]
    public class GradeController:BaseUidController<t_grade,v_grade>
    {
        public GradeController(GradeService entityUidService) : base(entityUidService)
        {
        }
    }
}