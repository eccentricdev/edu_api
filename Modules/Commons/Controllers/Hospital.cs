using System;
using System.Security.Claims;
using System.Threading.Tasks;
using edu_api.Modules.Commons.Databases.Models;
using edu_api.Modules.Commons.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SeventyOneDev.Utilities;

namespace edu_api.Modules.Commons.Controllers
{
    [Route("api/common/hospital", Name = "hospital")]
    public class HospitalController:BaseIdController<t_hospital,v_hospital>
    {
        private readonly HospitalService _entityIdService;
        public HospitalController(HospitalService entityIdService) : base(entityIdService)
        {
            _entityIdService = entityIdService;
        }
        
        [Authorize]
        [HttpDelete("chk_delete/{id:int}")]
        public async Task<ActionResult> DeleteHospitalId([FromRoute] int id)
        {
            try
            {
                var deleteCount = await _entityIdService.DeleteHospital(id, User.Identity as ClaimsIdentity);
                return deleteCount > 0 ? NoContent() : NotFound();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Delete failed: " + ex.Message);
                return BadRequest(ErrorMessage.Get(ex.Message));
            }

        }
    }
}