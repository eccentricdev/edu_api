using System;
using System.Security.Claims;
using System.Threading.Tasks;
using edu_api.Modules.Commons.Databases.Models;
using edu_api.Modules.Commons.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SeventyOneDev.Utilities;

namespace edu_api.Modules.Commons.Controllers
{
    [Route("api/common/cause_failed", Name = "cause_failed")]
    public class CaseFailedController:BaseIdController<t_cause_failed,v_cause_failed>
    {
        private readonly CaseFailedService _entityIdService;
        public CaseFailedController(CaseFailedService entityIdService) : base(entityIdService)
        {
            _entityIdService = entityIdService;
        }
        
        [Authorize]
        [HttpDelete("chk_delete/{id:int}")]
        public async Task<ActionResult> DeleteCauseFailedId([FromRoute] int id)
        {
            try
            {
                var deleteCount = await _entityIdService.DeleteCauseFailed(id, User.Identity as ClaimsIdentity);
                return deleteCount > 0 ? NoContent() : NotFound();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Delete failed: " + ex.Message);
                return BadRequest(ErrorMessage.Get(ex.Message));
            }

        }
        
        [Authorize]
        [HttpPost("new")]
        public async Task<ActionResult> NewCauseFailedId([FromBody] t_cause_failed entity)
        {
            try
            {
                await _entityIdService.NewCauseFailed(entity, User.Identity as ClaimsIdentity);
                return
                    NoContent();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Create failed: " + ex.Message);
                return BadRequest(ErrorMessage.Get(ex.Message));
            }

        }
        
        [Authorize]
        [HttpPut("update")]
        public async Task<ActionResult> UpdateCauseFailedId([FromBody] t_cause_failed entity)
        {
            try
            {
                await _entityIdService.UpdateCauseFailed(entity, User.Identity as ClaimsIdentity);
                return NoContent();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return BadRequest(ErrorMessage.Get(ex.Message));
            }

        }
    }
}