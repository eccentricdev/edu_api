using System;
using System.ComponentModel.DataAnnotations;
using SeventyOneDev.Utilities;

namespace edu_api.Modules.Commons.Databases.Models
{
    public class section_type : base_table
    {
        [Key] public short? section_type_id { get; set; }
        public string section_type_code { get; set; }
        public string section_type_name_th { get; set; }
        public string section_type_name_en { get; set; }
    }

    public class t_section_type : section_type
    {

        public t_section_type()
        {
            
        }

        public t_section_type(short? sectionTypeId, string sectionTypeCode, string sectionTypeNameEn,
            string sectionTypeNameTh)
        {
            this.section_type_id = sectionTypeId;
            this.section_type_code = sectionTypeCode;
            this.section_type_name_en = sectionTypeNameEn;
            this.section_type_name_th = sectionTypeNameTh;
            this.created_by = "SYSTEM";
            this.created_datetime = DateTime.Now;
            this.updated_by = "SYSTEM";
            this.updated_datetime = DateTime.Now;
        }

    }

    public class v_section_type : section_type
    {


    }
}
