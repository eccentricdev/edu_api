// using System;
// using System.ComponentModel.DataAnnotations;
// using SeventyOneDev.Utilities;
// using SeventyOneDev.Utilities.Attributes;
//
// namespace edu_api.Modules.Commons.Databases.Models
// {
//   public class religion : base_table
//   {
//     [Key]
//     public int? religion_id { get; set; }
//     public string religion_code { get; set; }
//     public string religion_name_th { get; set; }
//     public string religion_name_en { get; set; }
//   }
//   [GeneratedIdController("api/common/religion")]
//   public class t_religion : religion
//   {
//     public t_religion()
//     {
//     }
//
//     public t_religion(short? religionId, string religionCode, string religionNameEn, string religionNameTh)
//     {
//       this.religion_id = religionId;
//       this.religion_code = religionCode;
//       this.religion_name_en = religionNameEn;
//       this.religion_name_th = religionNameTh;
//       this.status_id = 1;
//       this.created_by = "SYSTEM";
//       this.updated_by = "SYSTEM";
//       this.created_datetime = DateTime.Now;
//       this.updated_datetime = DateTime.Now;
//     }
//
//   }
//
//   public class v_religion : religion
//   {
//
//
//   }
// }
