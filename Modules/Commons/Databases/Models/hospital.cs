using System.ComponentModel.DataAnnotations;
using SeventyOneDev.Utilities;
using SeventyOneDev.Utilities.Attributes;

namespace edu_api.Modules.Commons.Databases.Models
{
    public class hospital: base_table
    {
        [Key] 
        public int? hospital_id { get; set; }
        public string hospital_code { get; set; }
        [Unique]
        public string hospital_name_th { get; set; }
        public string hospital_name_en { get; set; }
        public int? province_id {get;set;}
        public int? district_id {get;set;}
    }
    // [GeneratedIdController("api/common/hospital")]
    public class t_hospital : hospital
    {
        

    }

    public class v_hospital : hospital
    {
        public string province_code {get;set;}
        public string province_name_th {get;set;}
        public string province_name_en {get;set;}
        public string district_code {get;set;}
        public string district_name_th {get;set;}
        public string district_name_en {get;set;}

    }
}