using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using SeventyOneDev.Utilities;
using SeventyOneDev.Utilities.Attributes;

namespace edu_api.Modules.Commons.Databases.Models
{
    public class school : base_table
    {
        [Key,DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int? school_id {get;set;}
        [MaxLength(10)]
        public string school_code {get;set;}
        [MaxLength(100)]
        public string school_name_th {get;set;}
        [MaxLength(100)]
        public string school_name_en {get;set;}
        [MaxLength(500)]
        public string remark {get;set;}
        [MaxLength(500)]
        public string address1_th {get;set;}
        [MaxLength(500)]
        public string address2_th {get;set;}
        [MaxLength(30)]
        public string postal_code {get;set;}
        [MaxLength(500)]
        public string address1_en {get;set;}
        [MaxLength(500)]
        public string address2_en {get;set;}
        [MaxLength(100)]
        public string phone_no {get;set;}
        [MaxLength(100)]
        public string fax_no {get;set;}
        [MaxLength(100)]
        public string url {get;set;}
        public int? country_id {get;set;}
        [MaxLength(100)]
        public string contact {get;set;}
        public int? province_id {get;set;}
        public int? district_id {get;set;}
        public int? sub_district_id {get;set;}
        [MaxLength(1)]
        public string school_type {get;set;}
        [MaxLength(1)]
        public string school_flag {get;set;}
        [MaxLength(100)]
        public string email {get;set;}

        public short? school_level_id { get; set; }
       
    }

    [GeneratedIdController("api/common/school")]
    public class t_school : school
    {
        // [JsonIgnore]
        // public province province {get;set;}
    }

    public class v_school : school
    {
        public string province_code {get;set;}
        public string province_name_th {get;set;}
        public string province_name_en {get;set;}
        public string district_code {get;set;}
        public string district_name_th {get;set;}
        public string district_name_en {get;set;}
        public string sub_district_code {get;set;}
        public string sub_district_name_th {get;set;}
        public string sub_district_name_en {get;set;}
        
        public string school_level_code { get; set; }
        public string school_level_name_th { get; set; }
        public string school_level_name_en { get; set; }
    }

}
