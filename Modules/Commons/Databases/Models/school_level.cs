using System;
using System.ComponentModel.DataAnnotations;
using SeventyOneDev.Utilities;
using SeventyOneDev.Utilities.Attributes;

namespace edu_api.Modules.Commons.Databases.Models
{
  public class school_level : base_table
  {
    [Key] public int? school_level_id { get; set; }
    public string school_level_code { get; set; }
    public string school_level_name_th { get; set; }
    public string school_level_name_en { get; set; }
  }
  [GeneratedIdController("api/common/school_level")]
  public class t_school_level : school_level
  {


    public t_school_level()
    {
    }

    public t_school_level(short? schoolLevelId, string schoolLevelCode, string schoolLevelNameEn,
      string schoolLevelNameTh)
    {
      this.school_level_id = schoolLevelId;
      this.school_level_code = schoolLevelCode;
      this.school_level_name_en = schoolLevelNameEn;
      this.school_level_name_th = schoolLevelNameTh;
      this.status_id = 1;
      this.created_by = "SYSTEM";
      this.updated_by = "SYSTEM";
      this.created_datetime = DateTime.Now;
      this.updated_datetime = DateTime.Now;
    }

  }

  public class v_school_level : school_level
  {


  }
}
