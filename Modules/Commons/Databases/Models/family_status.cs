using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;
using rsu_common_api.models;
using SeventyOneDev.Utilities;
using SeventyOneDev.Utilities.Attributes;

namespace edu_api.Modules.Commons.Databases.Models
{
  public class family_status : base_table
  {
    [Key] public int? family_status_id { get; set; }
    public string family_status_code { get; set; }
    public string family_status_name_th { get; set; }
    public string family_status_name_en { get; set; }
  }
  [GeneratedIdController("api/common/family_status")]
  public class t_family_status : family_status
  {
    public t_family_status()
    {
    }

    public t_family_status(short? familyStatusId, string familyStatusCode, string familyStatusNameEn,
      string familyStatusNameTh)
    {
      this.family_status_id = familyStatusId;
      this.family_status_code = familyStatusCode;
      this.family_status_name_en = familyStatusNameEn;
      this.family_status_name_th = familyStatusNameTh;
      this.status_id = 1;
      this.created_by = "SYSTEM";
      this.updated_by = "SYSTEM";
      this.created_datetime = DateTime.Now;
      this.updated_datetime = DateTime.Now;
    }

  }

  public class v_family_status : family_status
  {


  }
}
