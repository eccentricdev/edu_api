// using System;
// using System.Collections.Generic;
// using System.ComponentModel.DataAnnotations;
// using System.ComponentModel.DataAnnotations.Schema;
// using System.Text.Json.Serialization;
// using SeventyOneDev.Utilities;
// using SeventyOneDev.Utilities.Attributes;
//
// namespace rsu_common_api.models
// {
//   public class blood_group : base_table
//   {
//     [Key] public int? blood_group_id { get; set; }
//     public string blood_group_code { get; set; }
//     public string blood_group_name_th { get; set; }
//     public string blood_group_name_en { get; set; }
//   }
//   [GeneratedIdController("api/common/blood_group")]
//   public class t_blood_group : blood_group
//   {
//
//
//     public t_blood_group()
//     {
//     }
//
//     public t_blood_group(short? bloodGroupId, string bloodGroupCode, string bloodGroupNameEn,
//       string bloodGroupNameTh)
//     {
//       this.blood_group_id = bloodGroupId;
//       this.blood_group_code = bloodGroupCode;
//       this.blood_group_name_en = bloodGroupNameEn;
//       this.blood_group_name_th = bloodGroupNameTh;
//       this.status_id = 1;
//       this.created_by = "SYSTEM";
//       this.updated_by = "SYSTEM";
//       this.created_datetime = DateTime.Now;
//       this.updated_datetime = DateTime.Now;
//     }
//
//   }
//
//   public class v_blood_group : blood_group
//   {
//
//
//   }
// }
