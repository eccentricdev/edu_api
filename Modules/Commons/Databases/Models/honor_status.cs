using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;
using SeventyOneDev.Utilities;
using SeventyOneDev.Utilities.Attributes;

namespace rsu_common_api.models
{
  public class honor_status : base_table
  {
    [Key] public int? honor_status_id { get; set; }
    public string honor_status_code { get; set; }
    public string honor_status_name_th { get; set; }
    public string honor_status_name_en { get; set; }
  }
  [GeneratedIdController("api/common/honor_status")]
  public class t_honor_status : honor_status
  {


    public t_honor_status()
    {
    }

    public t_honor_status(short? honorStatusId, string honorStatusCode, string honorStatusNameEn,
      string honorStatusNameTh)
    {
      this.honor_status_id = honorStatusId;
      this.honor_status_code = honorStatusCode;
      this.honor_status_name_en = honorStatusNameEn;
      this.honor_status_name_th = honorStatusNameTh;
      this.status_id = 1;
      this.created_by = "SYSTEM";
      this.updated_by = "SYSTEM";
      this.created_datetime = DateTime.Now;
      this.updated_datetime = DateTime.Now;
    }

  }

  public class v_honor_status : honor_status
  {


  }
}
