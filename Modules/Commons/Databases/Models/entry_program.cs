using System;
using System.ComponentModel.DataAnnotations;
using SeventyOneDev.Utilities;
using SeventyOneDev.Utilities.Attributes;

namespace edu_api.Modules.Commons.Databases.Models
{
    public class entry_program : base_table
    {
    [Key] public int? entry_program_id { get; set; }
    public string entry_program_code { get; set; }
    public string entry_program_name_th { get; set; }
    public string entry_program_name_en { get; set; }
    }
    [GeneratedIdController("api/common/entry_program")]
    public class t_entry_program : entry_program
    {

        public t_entry_program()
        {
        }

        public t_entry_program(short? entryProgramId, string entryProgramCode, string entryProgramNameEn,
            string entryProgramNameTh)
        {
            this.entry_program_id = entryProgramId;
            this.entry_program_code = entryProgramCode;
            this.entry_program_name_en = entryProgramNameEn;
            this.entry_program_name_th = entryProgramNameTh;
            this.status_id = 1;
            this.created_by = "SYSTEM";
            this.updated_by = "SYSTEM";
            this.created_datetime = DateTime.Now;
            this.updated_datetime = DateTime.Now;
        }

    }

    public class v_entry_program : entry_program
    {


    }
}