using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using SeventyOneDev.Utilities;
using SeventyOneDev.Utilities.Attributes;

namespace edu_api.Modules.Commons.Databases.Models
{
  public class attached_document : base_table
  {
    [Key] public Guid? attached_document_uid { get; set; }
    public string attached_document_code { get; set; }
    public string attached_document_name_th { get; set; }
    public string attached_document_name_en { get; set; }
  }
  [GeneratedUidController("api/common/attached_document")]
  public class t_attached_document : attached_document
  {


    public t_attached_document()
    {
    }

    public t_attached_document(Guid? attachedDocumentUid, string attachedDocumentCode, string attachedDocumentNameEn,
      string attachedDocumentNameTh)
    {
      this.attached_document_uid = attachedDocumentUid;
      this.attached_document_code = attachedDocumentCode;
      this.attached_document_name_en = attachedDocumentNameEn;
      this.attached_document_name_th = attachedDocumentNameTh;
      this.status_id = 1;
      this.created_by = "SYSTEM";
      this.updated_by = "SYSTEM";
      this.created_datetime = DateTime.Now;
      this.updated_datetime = DateTime.Now;
    }

  }

  public class v_attached_document : attached_document
  {


  }
}
