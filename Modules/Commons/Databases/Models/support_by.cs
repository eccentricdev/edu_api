using System;
using System.ComponentModel.DataAnnotations;
using SeventyOneDev.Utilities;
using SeventyOneDev.Utilities.Attributes;

namespace edu_api.Modules.Commons.Databases.Models
{
  public class support_by : base_table
  {
    [Key] public int? support_by_id { get; set; }
    public string support_by_code { get; set; }
    public string support_by_name_th { get; set; }
    public string support_by_name_en { get; set; }
  }
  [GeneratedIdController("api/common/support_by")]
  public class t_support_by : support_by
  {
    public t_support_by()
    {
    }

    public t_support_by(short? supportPerMonthId, string supportPerMonthCode, string supportPerMonthNameEn,
      string supportPerMonthNameTh)
    {
      this.support_by_id = supportPerMonthId;
      this.support_by_code = supportPerMonthCode;
      this.support_by_name_en = supportPerMonthNameEn;
      this.support_by_name_th = supportPerMonthNameTh;
      this.status_id = 1;
      this.created_by = "SYSTEM";
      this.updated_by = "SYSTEM";
      this.created_datetime = DateTime.Now;
      this.updated_datetime = DateTime.Now;
    }

  }

  public class v_support_by : support_by
  {


  }
}
