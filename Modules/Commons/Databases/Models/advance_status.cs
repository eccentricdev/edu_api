using System;
using System.ComponentModel.DataAnnotations;
using SeventyOneDev.Utilities;
using SeventyOneDev.Utilities.Attributes;

namespace edu_api.Modules.Commons.Databases.Models
{
  public class advance_status : base_table
  {
    [Key] public int? advance_status_id { get; set; }
    public string advance_status_code { get; set; }
    public string advance_status_name_th { get; set; }
    public string advance_status_name_en { get; set; }


  }
  [GeneratedIdController("api/common/advance_status")]
  public class t_advance_status : advance_status
  {
    public t_advance_status()
    {
    }

    public t_advance_status(short? advanceStatusId, string advanceStatusCode, string advanceStatusNameEn,
      string advanceStatusNameTh)
    {
      this.advance_status_id = advanceStatusId;
      this.advance_status_code = advanceStatusCode;
      this.advance_status_name_en = advanceStatusNameEn;
      this.advance_status_name_th = advanceStatusNameTh;
      this.status_id = 1;
      this.created_by = "SYSTEM";
      this.updated_by = "SYSTEM";
      this.created_datetime = DateTime.Now;
      this.updated_datetime = DateTime.Now;
    }
  }

  public class v_advance_status : advance_status
  {


  }
}
