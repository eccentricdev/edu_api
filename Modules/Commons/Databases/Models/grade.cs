using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;
using rsu_common_api.models;
using SeventyOneDev.Utilities;
using SeventyOneDev.Utilities.Attributes;

namespace edu_api.Modules.Commons.Databases.Models
{
  public class grade : base_table
  {
    [Key] public Guid? grade_uid { get; set; }
    public string grade_code { get; set; }
    [Unique]
    public string grade_name_th { get; set; }
    [Unique]
    public string grade_name_en { get; set; }
    public bool? is_calculate_gpa { get; set; }
    public int? display_order { get; set; }
    public string description { get; set; }
    public bool? is_calculate_credit { get; set; }
    public bool? is_pass { get; set; }
    public bool? is_calculate_honor { get; set; }
    [Column(TypeName = "decimal(18,2)")] 
    public decimal? grade_point { get; set; }
    
    public bool? is_score { get; set; }
  }
  // [GeneratedUidController("api/common/grade")]
  public class t_grade : grade
  {

    public t_grade()
    {
    }

    public t_grade(Guid gradeUid, string gradeCode, string gradeNameEn,
      string gradeNameTh, bool isCalculateGpa, bool isCalculateCredit,
      int displayOrder, string description, decimal gradePoint)
    {
      this.grade_uid = gradeUid;
      this.grade_code = gradeCode;
      this.grade_name_en = gradeNameEn;
      this.grade_name_th = gradeNameTh;
      this.is_calculate_gpa = isCalculateGpa;
      this.is_calculate_credit = isCalculateCredit;
      this.grade_point = gradePoint;
      this.display_order = displayOrder;
      this.description = description;
      this.status_id = 1;
      this.created_by = "SYSTEM";
      this.updated_by = "SYSTEM";
      this.created_datetime = DateTime.Now;
      this.updated_datetime = DateTime.Now;
    }

  }

  public class v_grade : grade
  {


  }
}
