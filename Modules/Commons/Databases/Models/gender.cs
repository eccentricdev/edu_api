// using System;
// using System.Collections.Generic;
// using System.ComponentModel.DataAnnotations;
// using System.ComponentModel.DataAnnotations.Schema;
// using System.Text.Json.Serialization;
// using rsu_common_api.models;
// using SeventyOneDev.Utilities;
// using SeventyOneDev.Utilities.Attributes;
//
// namespace edu_api.Modules.Commons.Databases.Models
// {
//   public class gender : base_table
//   {
//     [Key] public int? gender_id { get; set; }
//     public string gender_code { get; set; }
//     public string gender_name_th { get; set; }
//     public string gender_name_en { get; set; }
//
//   }
//   [GeneratedIdController("api/common/gender")]
//   public class t_gender : gender
//   {
//
//     public t_gender()
//     {
//     }
//
//     public t_gender(short? genderId, string genderCode, string genderNameEn,
//       string genderNameTh)
//     {
//       this.gender_id = genderId;
//       this.gender_code = genderCode;
//       this.gender_name_en = genderNameEn;
//       this.gender_name_th = genderNameTh;
//       this.status_id = 1;
//       this.created_by = "SYSTEM";
//       this.updated_by = "SYSTEM";
//       this.created_datetime = DateTime.Now;
//       this.updated_datetime = DateTime.Now;
//     }
//
//   }
//
//   public class v_gender:gender
//   {
//
//
//   }
// }
