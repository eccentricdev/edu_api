using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;
using rsu_common_api.models;
using SeventyOneDev.Utilities;
using SeventyOneDev.Utilities.Attributes;

namespace edu_api.Modules.Commons.Databases.Models
{
  public class cause_failed : base_table
  {
    [Key] 
    public int? cause_failed_id { get; set; }
    [Unique]
    public string cause_failed_code { get; set; }
    [Unique]
    public string cause_failed_name_th { get; set; }
    public string cause_failed_name_en { get; set; }
  }
  // [GeneratedIdController("api/common/cause_failed")]
  public class t_cause_failed : cause_failed
  {

    public t_cause_failed()
    {
    }

    public t_cause_failed(short? honorStatusId, string honorStatusCode, string honorStatusNameEn,
      string honorStatusNameTh)
    {
      this.cause_failed_id = honorStatusId;
      this.cause_failed_code = honorStatusCode;
      this.cause_failed_name_en = honorStatusNameEn;
      this.cause_failed_name_th = honorStatusNameTh;
      this.status_id = 1;
      this.created_by = "SYSTEM";
      this.updated_by = "SYSTEM";
      this.created_datetime = DateTime.Now;
      this.updated_datetime = DateTime.Now;
    }

  }

  public class v_cause_failed : cause_failed
  {


  }
}
