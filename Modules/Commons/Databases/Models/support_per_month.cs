using System;
using System.ComponentModel.DataAnnotations;
using SeventyOneDev.Utilities;
using SeventyOneDev.Utilities.Attributes;

namespace edu_api.Modules.Commons.Databases.Models
{
  public class support_per_month : base_table
  {
    [Key] public int? support_per_month_id { get; set; }
    public string support_per_month_code { get; set; }
    public string support_per_month_name_th { get; set; }
    public string support_per_month_name_en { get; set; }
  }
  [GeneratedIdController("api/common/support_per_month")]
  public class t_support_per_month : support_per_month
  {

    public t_support_per_month()
    {
    }

    public t_support_per_month(short? supportPerMonthId, string supportPerMonthCode, string supportPerMonthNameEn,
      string supportPerMonthNameTh)
    {
      this.support_per_month_id = supportPerMonthId;
      this.support_per_month_code = supportPerMonthCode;
      this.support_per_month_name_en = supportPerMonthNameEn;
      this.support_per_month_name_th = supportPerMonthNameTh;
      this.status_id = 1;
      this.created_by = "SYSTEM";
      this.updated_by = "SYSTEM";
      this.created_datetime = DateTime.Now;
      this.updated_datetime = DateTime.Now;
    }

  }

  public class v_support_per_month : support_per_month
  {


  }
}
