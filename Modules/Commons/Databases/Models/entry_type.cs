using System;
using System.ComponentModel.DataAnnotations;
using SeventyOneDev.Utilities;
using SeventyOneDev.Utilities.Attributes;

namespace edu_api.Modules.Commons.Databases.Models
{
  public class entry_type : base_table
  {
    [Key] public int? entry_type_id { get; set; }
    public string entry_type_code { get; set; }
    public string entry_type_name_th { get; set; }
    public string entry_type_name_en { get; set; }
  }
  [GeneratedIdController("api/common/entry_type")]
  public class t_entry_type : entry_type
  {

    public t_entry_type()
    {
    }

    public t_entry_type(short? entryTypeId, string entryTypeCode, string entryTypeNameEn,
      string entryTypeNameTh)
    {
      this.entry_type_id = entryTypeId;
      this.entry_type_code = entryTypeCode;
      this.entry_type_name_en = entryTypeNameEn;
      this.entry_type_name_th = entryTypeNameTh;
      this.status_id = 1;
      this.created_by = "SYSTEM";
      this.updated_by = "SYSTEM";
      this.created_datetime = DateTime.Now;
      this.updated_datetime = DateTime.Now;
    }

  }

  public class v_entry_type : entry_type
  {


  }
}
