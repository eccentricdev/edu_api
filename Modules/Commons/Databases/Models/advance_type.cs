using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using SeventyOneDev.Utilities;
using SeventyOneDev.Utilities.Attributes;

namespace edu_api.Modules.Commons.Databases.Models
{
  public class advance_type : base_table
  {
    [Key] public int? advance_type_id { get; set; }
    public string advance_type_code { get; set; }
    public string advance_type_name_th { get; set; }
    public string advance_type_name_en { get; set; }


  }
  [GeneratedIdController("api/common/advance_type")]
  public class t_advance_type : advance_type
  {
    public t_advance_type()
    {
    }

    public t_advance_type(short? advanceTypeId, string advanceTypeCode, string advanceTypeNameEn,
      string advanceTypeNameTh)
    {
      this.advance_type_id = advanceTypeId;
      this.advance_type_code = advanceTypeCode;
      this.advance_type_name_en = advanceTypeNameEn;
      this.advance_type_name_th = advanceTypeNameTh;
      this.status_id = 1;
      this.created_by = "SYSTEM";
      this.updated_by = "SYSTEM";
      this.created_datetime = DateTime.Now;
      this.updated_datetime = DateTime.Now;
    }
  }

  public class v_advance_type : advance_type
  {


  }
}
