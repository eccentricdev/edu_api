using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;
using rsu_common_api.models;
using SeventyOneDev.Utilities;
using SeventyOneDev.Utilities.Attributes;

namespace edu_api.Modules.Commons.Databases.Models
{
  public class income_level : base_table
  {
    [Key] public int? income_level_id { get; set; }
    public string income_level_code { get; set; }
    public string income_level_name_th { get; set; }
    public string income_level_name_en { get; set; }
  }
  [GeneratedIdController("api/common/income_level")]
  public class t_income_level : income_level
  {

    public t_income_level()
    {
    }

    public t_income_level(short? honorStatusId, string honorStatusCode, string honorStatusNameEn,
      string honorStatusNameTh)
    {
      this.income_level_id = honorStatusId;
      this.income_level_code = honorStatusCode;
      this.income_level_name_en = honorStatusNameEn;
      this.income_level_name_th = honorStatusNameTh;
      this.status_id = 1;
      this.created_by = "SYSTEM";
      this.updated_by = "SYSTEM";
      this.created_datetime = DateTime.Now;
      this.updated_datetime = DateTime.Now;
    }

  }

  public class v_income_level : income_level
  {


  }
}
