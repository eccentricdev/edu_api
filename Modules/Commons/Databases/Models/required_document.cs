using System;
using System.ComponentModel.DataAnnotations;
using SeventyOneDev.Utilities;
using SeventyOneDev.Utilities.Attributes;

namespace edu_api.Modules.Commons.Databases.Models
{
  public class required_document : base_table
  {
    [Key] public Guid? required_document_uid { get; set; }
    public Guid? step_uid { get; set; }
    public Guid? attached_document_uid { get; set; }
    public bool? is_multiple { get; set; }




  }
  [GeneratedUidController("api/common/required_document")]
  public class t_required_document : required_document
  {

  }

  public class v_required_document : required_document
  {

    public string step_code { get; set; }
    public string step_name_th { get; set; }
    public string step_name_en { get; set; }
    public string attached_document_code { get; set; }
    public string attached_document_name_th { get; set; }
    public string attached_document_name_en { get; set; }
    public string status_code { get; set; }
    public string status_name_th { get; set; }
    public string status_name_en { get; set; }

  }
}
