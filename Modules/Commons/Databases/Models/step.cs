using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using SeventyOneDev.Utilities;
using SeventyOneDev.Utilities.Attributes;

namespace edu_api.Modules.Commons.Databases.Models
{
  public class step : base_table
  {
    [Key] public Guid? step_uid { get; set; }

    public string step_code { get; set; }

    public string step_name_th { get; set; }

    public string step_name_en { get; set; }
  }
  [GeneratedUidController("api/common/step")]
  public class t_step : step
  {

    public t_step()
    {
    }

    public t_step(Guid? stepUid, string stepCode, string stepNameEn,
      string stepNameTh)
    {
      this.step_uid = stepUid;
      this.step_code = stepCode;
      this.step_name_en = stepNameEn;
      this.step_name_th = stepNameTh;
      this.status_id = 1;
      this.created_by = "SYSTEM";
      this.updated_by = "SYSTEM";
      this.created_datetime = DateTime.Now;
      this.updated_datetime = DateTime.Now;
    }

  }

  public class v_step : step
  {


  }
}
