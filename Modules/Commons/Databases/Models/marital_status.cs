// using System;
// using System.Collections.Generic;
// using System.ComponentModel.DataAnnotations;
// using System.ComponentModel.DataAnnotations.Schema;
// using System.Text.Json.Serialization;
// using rsu_common_api.models;
// using SeventyOneDev.Utilities;
// using SeventyOneDev.Utilities.Attributes;
//
// namespace edu_api.Modules.Commons.Databases.Models
// {
//   public class marital_status : base_table
//   {
//     [Key] public int? marital_status_id { get; set; }
//
//     public string marital_status_code { get; set; }
//
//     public string marital_status_name_th { get; set; }
//
//     public string marital_status_name_en { get; set; }
//   }
//   [GeneratedIdController("api/common/marital_status")]
//   public class t_marital_status : marital_status
//   {
//
//     public t_marital_status()
//     {
//     }
//
//     public t_marital_status(short? maritalStatusId, string maritalStatusCode, string maritalStatusNameEn,
//       string maritalStatusNameTh)
//     {
//       this.marital_status_id = maritalStatusId;
//       this.marital_status_code = maritalStatusCode;
//       this.marital_status_name_en = maritalStatusNameEn;
//       this.marital_status_name_th = maritalStatusNameTh;
//       this.status_id = 1;
//       this.created_by = "SYSTEM";
//       this.updated_by = "SYSTEM";
//       this.created_datetime = DateTime.Now;
//       this.updated_datetime = DateTime.Now;
//     }
//
//   }
//
//   public class v_marital_status : marital_status
//   {
//
//
//   }
// }
