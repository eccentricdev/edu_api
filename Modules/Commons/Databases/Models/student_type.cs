using System;
using System.ComponentModel.DataAnnotations;
using SeventyOneDev.Utilities;
using SeventyOneDev.Utilities.Attributes;

namespace edu_api.Modules.Commons.Databases.Models
{
  public class student_type : base_table
  {
    [Key] public int? student_type_id { get; set; }
    public string student_type_code { get; set; }
    public string student_type_name_th { get; set; }
    public string student_type_name_en { get; set; }

  }
  [GeneratedIdController("api/common/student_type")]
  public class t_student_type : student_type
  {

    public t_student_type()
    {
    }

    public t_student_type(short? studentTypeId, string studentTypeCode, string studentTypeNameEn,
      string studentTypeNameTh)
    {
      this.student_type_id = studentTypeId;
      this.student_type_code = studentTypeCode;
      this.student_type_name_en = studentTypeNameEn;
      this.student_type_name_th = studentTypeNameTh;
      this.status_id = 1;
      this.created_by = "SYSTEM";
      this.updated_by = "SYSTEM";
      this.created_datetime = DateTime.Now;
      this.updated_datetime = DateTime.Now;
    }

  }

  public class v_student_type : student_type
  {


  }
}
