// using System;
// using System.Collections.Generic;
// using System.ComponentModel.DataAnnotations;
// using System.ComponentModel.DataAnnotations.Schema;
// using System.Text.Json.Serialization;
// using rsu_common_api.models;
// using SeventyOneDev.Utilities;
// using SeventyOneDev.Utilities.Attributes;
//
// namespace edu_api.Modules.Commons.Databases.Models
// {
//   public class military_status : base_table
//   {
//     [Key] public int? military_status_id { get; set; }
//     public string military_status_code { get; set; }
//     public string military_status_name_th { get; set; }
//     public string military_status_name_en { get; set; }
//   }
//   [GeneratedIdController("api/common/military_status")]
//   public class t_military_status : military_status
//   {
//     public t_military_status()
//     {
//     }
//
//     public t_military_status(short? militaryStatusId, string militaryStatusCode, string militaryStatusNameEn,
//       string militaryStatusNameTh)
//     {
//       this.military_status_id = militaryStatusId;
//       this.military_status_code = militaryStatusCode;
//       this.military_status_name_en = militaryStatusNameEn;
//       this.military_status_name_th = militaryStatusNameTh;
//       this.status_id = 1;
//       this.created_by = "SYSTEM";
//       this.updated_by = "SYSTEM";
//       this.created_datetime = DateTime.Now;
//       this.updated_datetime = DateTime.Now;
//     }
//
//
//   }
//
//   public class v_military_status : military_status
//   {
//
//
//   }
// }
