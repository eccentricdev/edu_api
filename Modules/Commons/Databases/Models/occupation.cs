// using System;
// using System.Collections.Generic;
// using System.ComponentModel.DataAnnotations;
// using System.ComponentModel.DataAnnotations.Schema;
// using System.Text.Json.Serialization;
// using SeventyOneDev.Utilities;
// using SeventyOneDev.Utilities.Attributes;
//
// namespace rsu_common_api.models
// {
//   public class occupation : base_table
//   {
//     [Key] public int? occupation_id { get; set; }
//     public string occupation_code { get; set; }
//     public string occupation_name_th { get; set; }
//     public string occupation_name_en { get; set; }
//   }
//   [GeneratedIdController("api/common/occupation")]
//   public class t_occupation : occupation
//   {
//
//
//     public t_occupation()
//     {
//     }
//
//     public t_occupation(short? honorStatusId, string honorStatusCode, string honorStatusNameEn,
//       string honorStatusNameTh)
//     {
//       this.occupation_id = honorStatusId;
//       this.occupation_code = honorStatusCode;
//       this.occupation_name_en = honorStatusNameEn;
//       this.occupation_name_th = honorStatusNameTh;
//       this.status_id = 1;
//       this.created_by = "SYSTEM";
//       this.updated_by = "SYSTEM";
//       this.created_datetime = DateTime.Now;
//       this.updated_datetime = DateTime.Now;
//     }
//
//   }
//
//   public class v_occupation : occupation
//   {
//
//   }
// }
