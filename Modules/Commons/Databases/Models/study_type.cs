using System;
using System.ComponentModel.DataAnnotations;
using SeventyOneDev.Utilities;
using SeventyOneDev.Utilities.Attributes;

namespace edu_api.Modules.Commons.Databases.Models
{
  public class study_type : base_table
  {
    [Key] public int? study_type_id { get; set; }
    public string study_type_code { get; set; }
    public string study_type_name_th { get; set; }
    public string study_type_name_en { get; set; }
  }
  [GeneratedIdController("api/common/study_type")]
  public class t_study_type : study_type
  {

    public t_study_type()
    {
    }

    public t_study_type(short? studyTypeId, string studyTypeCode, string studyTypeNameEn,
      string studyTypeNameTh)
    {
      this.study_type_id = studyTypeId;
      this.study_type_code = studyTypeCode;
      this.study_type_name_en = studyTypeNameEn;
      this.study_type_name_th = studyTypeNameTh;
      this.status_id = 1;
      this.created_by = "SYSTEM";
      this.updated_by = "SYSTEM";
      this.created_datetime = DateTime.Now;
      this.updated_datetime = DateTime.Now;
    }

  }

  public class v_study_type : study_type
  {


  }
}
