using Microsoft.EntityFrameworkCore;
using edu_api.Databases.Models;
using edu_api.Modules.Commons.Databases.Models;
using rsu_common_api.models;
using rsu_common_api.Modules.Profiles.Databases.Models;

namespace SeventyOneDev.Utilities
{
  public partial class Db : DbContext
  {
    // public DbSet<t_prefix> t_prefix { get; set; }
    //
    // public DbSet<v_prefix> v_prefix { get; set; }
    //
    // public DbSet<t_blood_group> t_blood_group { get; set; }
    // public DbSet<v_blood_group> v_blood_group { get; set; }
    // // public DbSet<company> company { get; set; }
    // // public DbSet<v_company> v_company { get; set; }
    // public DbSet<t_gender> t_gender { get; set; }
    // public DbSet<v_gender> v_gender { get; set; }
    public DbSet<t_entry_type> t_entry_type { get; set; }
    public DbSet<v_entry_type> v_entry_type { get; set; }
    public DbSet<t_entry_program> t_entry_program { get; set; }
    public DbSet<v_entry_program> v_entry_program { get; set; }
    public DbSet<t_family_status> t_family_status { get; set; }
    public DbSet<v_family_status> v_family_status { get; set; }
    public DbSet<t_honor_status> t_honor_status { get; set; }
    public DbSet<v_honor_status> v_honor_status { get; set; }
    // public DbSet<t_marital_status> t_marital_status { get; set; }
    // public DbSet<v_marital_status> v_marital_status { get; set; }
    // public DbSet<t_military_status> t_military_status { get; set; }
    // public DbSet<v_military_status> v_military_status { get; set; }
    // public DbSet<t_religion> t_religion { get; set; }
    // public DbSet<v_religion> v_religion { get; set; }
    public DbSet<t_student_type> t_student_type { get; set; }
    public DbSet<v_student_type> v_student_type { get; set; }
    //public DbSet<v_parent_type> v_parent_type { get; set; }
    public DbSet<t_income_level> t_income_level { get; set; }
    public DbSet<v_income_level> v_income_level { get; set; }
    // public DbSet<t_occupation> t_occupation { get; set; }
    // public DbSet<v_occupation> v_occupation { get; set; }
    public DbSet<t_support_per_month> t_support_per_month { get; set; }
    public DbSet<v_support_per_month> v_support_per_month { get; set; }
    public DbSet<t_support_by> t_support_by { get; set; }
    public DbSet<v_support_by> v_support_by { get; set; }
    public DbSet<t_grade> t_grade { get; set; }
    public DbSet<v_grade> v_grade { get; set; }
    public DbSet<t_advance_type> t_advance_type { get; set; }
    public DbSet<v_advance_type> v_advance_type { get; set; }
    public DbSet<t_advance_status> t_advance_status { get; set; }
    public DbSet<v_advance_status> v_advance_status { get; set; }
    public DbSet<t_attached_document> t_attached_document { get; set; }
    public DbSet<v_attached_document> v_attached_document { get; set; }
    public DbSet<t_required_document> t_required_document { get; set; }
    public DbSet<v_required_document> v_required_document { get; set; }
    public DbSet<t_step> t_step { get; set; }
    public DbSet<v_step> v_step { get; set; }
    public DbSet<t_study_type> t_study_type { get; set; }
    public DbSet<v_study_type> v_study_type { get; set; }
    public DbSet<t_school_level> t_school_level { get; set; }
    public DbSet<v_school_level> v_school_level { get; set; }
    public DbSet<t_status> t_status { get; set; }
    public DbSet<v_status> v_status { get; set; }
    public DbSet<t_cause_failed> t_cause_failed { get; set; }
    public DbSet<v_cause_failed> v_cause_failed { get; set; } 
    
    public DbSet<t_hospital> t_hospital { get; set; }
    public DbSet<v_hospital> v_hospital { get; set; }
    
    public DbSet<t_school> t_school { get; set; }
    public DbSet<v_school> v_school { get; set; }  
    
    public DbSet<t_section_type> t_section_type { get; set; }
    public DbSet<v_section_type> v_section_type { get; set; }
    private void OnCommonModelCreating(ModelBuilder builder,string schema)
    {
      // builder.Entity<t_prefix>().ToTable("prefix", schema);
      // builder.Entity<v_prefix>().ToView("v_prefix", schema);
      // builder.Entity<t_blood_group>().ToTable("blood_group", schema);
      // builder.Entity<v_blood_group>().ToView("v_blood_group", schema);
      //
      // builder.Entity<t_gender>().ToTable("gender", schema);
      // builder.Entity<v_gender>().ToView("v_gender", schema);

      builder.Entity<t_entry_type>().ToTable("entry_type", schema);
      builder.Entity<v_entry_type>().ToView("v_entry_type", schema); 
      builder.Entity<t_entry_program>().ToTable("entry_program", schema);
      builder.Entity<v_entry_program>().ToView("v_entry_program", schema);
      builder.Entity<t_family_status>().ToTable("family_status", schema);
      builder.Entity<v_family_status>().ToView("v_family_status", schema);

      builder.Entity<t_honor_status>().ToTable("honor_status", schema);
      builder.Entity<v_honor_status>().ToView("v_honor_status", schema);
      // builder.Entity<t_marital_status>().ToTable("marital_status", schema);
      // builder.Entity<v_marital_status>().ToView("v_marital_status", schema);
      // builder.Entity<t_military_status>().ToTable("military_status", schema);
      // builder.Entity<v_military_status>().ToView("v_military_status", schema);
      //
      // builder.Entity<t_religion>().ToTable("religion", schema);
      // builder.Entity<v_religion>().ToView("v_religion", schema);
      builder.Entity<t_student_type>().ToTable("student_type", schema);
      builder.Entity<v_student_type>().ToView("v_student_type", schema);
      builder.Entity<t_income_level>().ToTable("income_level", schema);
      builder.Entity<v_income_level>().ToView("v_income_level", schema);
      // builder.Entity<t_occupation>().ToTable("occupation", schema);
      // builder.Entity<v_occupation>().ToView("v_occupation", schema);

      builder.Entity<t_support_per_month>().ToTable("support_per_month", schema);
      builder.Entity<v_support_per_month>().ToView("v_support_per_month", schema);
      builder.Entity<t_support_by>().ToTable("support_by", schema);
      builder.Entity<v_support_by>().ToView("v_support_by", schema);
      builder.Entity<t_grade>().ToTable("grade", schema);
      builder.Entity<v_grade>().ToView("v_grade", schema);
      builder.Entity<t_advance_type>().ToTable("advance_type", schema);
      builder.Entity<v_advance_type>().ToView("v_advance_type", schema);

      builder.Entity<t_advance_status>().ToTable("advance_status", schema);
      builder.Entity<v_advance_status>().ToView("v_advance_status", schema);
      builder.Entity<t_attached_document>().ToTable("attached_document", schema);
      builder.Entity<v_attached_document>().ToView("v_attached_document", schema);
      builder.Entity<t_required_document>().ToTable("required_document", schema);
      builder.Entity<v_required_document>().ToView("v_required_document", schema);
      builder.Entity<t_step>().ToTable("step", schema);
      builder.Entity<v_step>().ToView("v_step", schema);
      builder.Entity<t_study_type>().ToTable("study_type", schema);
      builder.Entity<v_study_type>().ToView("v_study_type", schema);
      builder.Entity<t_school_level>().ToTable("school_level", schema);
      builder.Entity<v_school_level>().ToView("v_school_level", schema);

      builder.Entity<t_status>().ToTable("status", _schema);
      builder.Entity<v_status>().ToView("v_status", _schema);
      builder.Entity<t_cause_failed>().ToTable("cause_failed", _schema);
      builder.Entity<v_cause_failed>().ToView("v_cause_failed", _schema);
      
      builder.Entity<t_hospital>().ToTable("hospital", _schema);
      builder.Entity<v_hospital>().ToView("v_hospital", _schema);
      
      builder.Entity<t_school>().ToTable("school", _schema);
      builder.Entity<v_school>().ToView("v_school", _schema); 
      
      builder.Entity<t_section_type>().ToTable("section_type", _schema);
      builder.Entity<v_section_type>().ToView("v_section_type", _schema);
    }
  }
}
