using edu_api.Modules.StudentEvaluations.Services;
using Microsoft.Extensions.DependencyInjection;

namespace edu_api.Modules.StudentEvaluations.Configures
{
    public static class StudentEvaluationCollectionExtension
    {
        public static IServiceCollection AddStudentEvaluationServices(this IServiceCollection services)
        {
            services.AddScoped<StudySubjectSectionResultService>();
            services.AddScoped<FinalRegisterSubjectGradeService>();

            return services;
        }
    }
}