using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using edu_api.Modules.StudentEvaluations.Databases.Models;
using Microsoft.EntityFrameworkCore;
using SeventyOneDev.Utilities;

namespace edu_api.Modules.StudentEvaluations.Services
{
    public class StudySubjectSectionResultService 
    {
        readonly Db _context;

        public StudySubjectSectionResultService(Db context)
        {
            _context = context;
        }
        
        public async Task<List<v_study_subject_section_result>> ListStudySubjectSectionResult(v_study_subject_section_result _study_subject_section_result)
        {
            IQueryable<v_study_subject_section_result> _study_subject_section_results = _context.v_study_subject_section_result.AsNoTracking();
            if (_study_subject_section_result != null)
            {
                if (_study_subject_section_result.search != null)
                {
                    _study_subject_section_results = _study_subject_section_results.Search(_study_subject_section_result.search);
                }
                else
                {
                    _study_subject_section_results = _study_subject_section_results.Search(_study_subject_section_result);
                }
            }
            //_study_subject_section_results = _study_subject_section_results.OrderBy(c => c.study_subject_section_result_id);
            return await _study_subject_section_results.Take(100).ToListAsync();
        }

        public async Task<v_study_subject_section_result> GetStudySubjectSectionResult(Guid id)
        {
            return await _context.v_study_subject_section_result.FirstOrDefaultAsync(a => a.year_subject_section_uid == id);
        }
        
    }
}