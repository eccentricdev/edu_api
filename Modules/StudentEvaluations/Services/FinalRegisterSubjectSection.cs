using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using edu_api.Modules.StudentEvaluations.Databases.Models;
using Microsoft.EntityFrameworkCore;
using SeventyOneDev.Utilities;

namespace edu_api.Modules.StudentEvaluations.Services
{

  public class FinalRegisterSubjectGradeService
  {
    readonly Db _context;

    public FinalRegisterSubjectGradeService(Db context)
    {
      _context = context;
    }

    public async Task<List<v_final_register_subject_grade>> ListFinalRegisterSubjectGrade(v_final_register_subject_grade _final_register_subject_grade)
    {
      IQueryable<v_final_register_subject_grade> _final_register_subject_grades = _context.v_final_register_subject_grade.AsNoTracking();

      if (_final_register_subject_grade != null)
      {
        if (_final_register_subject_grade.search != null)
        {
          _final_register_subject_grades = _final_register_subject_grades.Search(_final_register_subject_grade.search);
        }
        else
        {
          _final_register_subject_grades = _final_register_subject_grades.Search(_final_register_subject_grade);
        }
      }

      _final_register_subject_grades = _final_register_subject_grades.OrderBy(c => c.final_register_subject_uid);
      return await _final_register_subject_grades.Take(100).ToListAsync();
    }

    public async Task<v_final_register_subject_grade> GetFinalRegisterSubjectGrade(Guid id)
    {
      return await _context.v_final_register_subject_grade.FirstOrDefaultAsync(a => a.final_register_subject_uid == id);
    }

    // public async Task<t_final_register_subject_grade> NewFinalRegisterSubjectGrade(t_final_register_subject_grade _final_register_subject_grade, ClaimsIdentity claimsIdentity)
    // {
    //
    //   await _context.t_final_register_subject_grade.AddAsync(_final_register_subject_grade);
    //   await _context.SaveChangesAsync();
    //   return _final_register_subject_grade;
    // }
    //
    // public async Task<int> UpdateFinalRegisterSubjectGrade(t_final_register_subject_grade _final_register_subject_grade, ClaimsIdentity claimsIdentity)
    // {
    //
    //   _context.t_final_register_subject_grade.Update(_final_register_subject_grade);
    //   return await _context.SaveChangesAsync();
    // }
    //
    // public async Task<int> DeleteFinalRegisterSubjectGrade(int id, ClaimsIdentity claimsIdentity)
    // {
    //   var _final_register_subject_grade = await _context.t_final_register_subject_grade.FirstOrDefaultAsync(c => c.final_register_subject_grade_id == id);
    //   _context.Remove(_final_register_subject_grade);
    //   return await _context.SaveChangesAsync();
    //
    // }
  }


}
