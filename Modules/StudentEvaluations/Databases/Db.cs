using edu_api.Modules.EducationPlan.Databases;
using edu_api.Modules.Fee.Databases.Models;
using edu_api.Modules.StudentEvaluations.Databases.Models;
using Microsoft.EntityFrameworkCore;

namespace SeventyOneDev.Utilities
{
  public partial class Db : DbContext
  {

    // public DbSet<t_grade_reason> t_grade_reason { get; set; }
    // public DbSet<v_grade_reason> v_grade_reason { get; set; }

    public DbSet<v_final_register_subject_grade> v_final_register_subject_grade { get; set; }
    public DbSet<v_study_subject_section_result> v_study_subject_section_result { get; set; }

    private void OnStudentEvaluationModelCreating(ModelBuilder builder, string schema)
    {
      // builder.Entity<t_grade_reason>().ToTable("grade_reason", schema);
      // builder.Entity<v_grade_reason>().ToView("v_grade_reason", schema);

      builder.Entity<v_study_subject_section_result>().ToView("v_study_subject_section_result", schema);
      builder.Entity<v_final_register_subject_grade>().ToView("v_final_register_subject_grade", schema);

    }
  }
}
