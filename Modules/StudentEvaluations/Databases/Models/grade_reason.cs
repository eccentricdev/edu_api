// using System;
// using System.ComponentModel.DataAnnotations;
// using System.ComponentModel.DataAnnotations.Schema;
// using SeventyOneDev.Utilities;
// using SeventyOneDev.Utilities.Attributes;
//
// namespace edu_api.Modules.StudentEvaluations.Databases.Models
// {
//     public class grade_reason:base_table
//     {
//         [Key]
//         public int? grade_reason_id { get; set; }
//         public string grade_reason_code { get; set; }
//         public string grade_reason_name_th { get; set; }
//         public string grade_reason_name_en { get; set; }
//         public string grade_reason_short_name_th { get; set; }
//         public string grade_reason_short_name_en { get; set; }
//     }
//
//     [GeneratedIdController("api/student_evaluation/grade_reason")]
//     public class t_grade_reason : grade_reason
//     {
//         public t_grade_reason()
//
//         {
//         }
//
//         public t_grade_reason(short? gradeReasonId, string gradeReasonCode, string gradeReasonNameEn,
//             string gradeReasonNameTh)
//         {
//             this.grade_reason_id = gradeReasonId;
//             this.grade_reason_code = gradeReasonCode;
//             this.grade_reason_name_en = gradeReasonNameEn;
//             this.grade_reason_name_th = gradeReasonNameTh;
//             this.created_by = "SYSTEM";
//             this.updated_by = "SYSTEM";
//             this.created_datetime = DateTime.Now;
//             this.updated_datetime = DateTime.Now;
//         }
//
//     }
//
//     public class v_grade_reason : grade_reason
//     {
//
//     }
//
//
// }
