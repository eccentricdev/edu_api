using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace edu_api.Modules.StudentEvaluations.Databases.Models
{
  public class v_study_subject_section_result
  {
    [Key]
    public Guid? year_subject_section_uid { get; set; }
    public Guid? education_type_uid {get;set;}
    public string education_type_code {get;set;}
    public string education_type_name_en {get;set;}
    public string education_type_name_th {get;set;}
    public Guid? subject_year_uid { get; set; }
    public string subject_year_name_en {get;set;}
    public string subject_year_name_th {get;set;}
    public Guid? academic_year_uid { get; set; }
    public string academic_year_code {get;set;}
    public string academic_year_name_th {get;set;}
    public string academic_year_name_en {get;set;}
    public Guid? academic_semester_uid { get; set; }
    public string academic_semester_code {get;set;}
    public string semester_name_th {get;set;}
    public string semester_name_en {get;set;}
    public string section_code {get;set;}
    // public string primary_instructor_code {get;set;}
    // public string primary_instructor_name_th {get;set;}
    // public string primary_instructor_name_en {get;set;}
    // public short? study_result_status_id {get;set;}
    // public string study_result_status_code {get;set;}
    // public string study_result_status_name_en {get;set;}
    // public string study_result_status_name_th {get;set;}
    public short? section_type_id {get;set;}
    public string section_type_code {get;set;}
    public string section_type_name_th {get;set;}
    public string section_type_name_en {get;set;}
    public short? study_type_id {get;set;}
    public string study_type_code {get;set;}
    public string study_type_name_th {get;set;}
    public string study_type_name_en {get;set;}
    [NotMapped]
    public string search { get; set; }
  }
}
