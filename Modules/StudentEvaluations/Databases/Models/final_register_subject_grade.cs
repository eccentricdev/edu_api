using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace edu_api.Modules.StudentEvaluations.Databases.Models
{
  public class v_final_register_subject_grade
  {
    [Key]
    public Guid? final_register_subject_uid {get;set;}
    public string student_code {get;set;}
    public string display_name_th {get;set;}
    public string display_name_en {get;set;}
    public int? major_id {get;set;}
    public string major_code {get;set;}
    public string major_name_th {get;set;}
    public string major_name_en {get;set;}
    public short? grade_id {get;set;}
    public string grade_code {get;set;}
    public string grade_name_th {get;set;}
    public string grade_name_en {get;set;}
    public short? grade_reason_id {get;set;}
    public string grade_reason_code {get;set;}
    public string grade_reason_name_th {get;set;}
    public string grade_reason_name_en { get; set; }
    public Guid? lecture_year_subject_section_uid {get;set;}
    public Guid? lab_year_subject_section_uid {get;set;}
    [NotMapped]
    public string search { get; set; }
  }
}
