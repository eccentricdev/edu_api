using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using edu_api.Modules.StudentEvaluations.Databases.Models;
using edu_api.Modules.StudentEvaluations.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SeventyOneDev.Utilities;

namespace edu_api.Modules.StudentEvaluations.Controllers
{
    [Route("api/student_evaluation/final_register_subject_grades", Name = "final_register_subject_grades")]
    public class FinalRegisterSubjectGradeController:Controller
    {
        readonly FinalRegisterSubjectGradeService _finalRegisterSubjectGradeService;
        
        public FinalRegisterSubjectGradeController(FinalRegisterSubjectGradeService finalRegisterSubjectGradeService){
            _finalRegisterSubjectGradeService = finalRegisterSubjectGradeService;
        }
        
        [HttpGet]
        public async Task<ActionResult<List<v_final_register_subject_grade>>> GetFinalRegisterSubjectGrades([FromQuery]v_final_register_subject_grade _final_register_subject_grade=null)
        {
            if (EFExtension.IsNullOrEmpty(_final_register_subject_grade))
            {
                var listResult = await _finalRegisterSubjectGradeService.ListFinalRegisterSubjectGrade(null);
                return Ok(listResult);
            }
            else
            {
                var getResult = await _finalRegisterSubjectGradeService.ListFinalRegisterSubjectGrade(_final_register_subject_grade);
                if (!getResult.Any())
                {
                    return Ok(new List<v_final_register_subject_grade>());
                }
                else
                {
                    return getResult.ToList();
                }

            }

        }

        [HttpGet("{final_register_subject_grade_uid}")]
        public async Task<ActionResult<v_final_register_subject_grade>> GetFinalRegisterSubjectGrade([FromRoute]Guid final_register_subject_grade_uid)
        {

            var getResult = await _finalRegisterSubjectGradeService.GetFinalRegisterSubjectGrade(final_register_subject_grade_uid);
            if (getResult==null)
            {
                return NotFound();
            }
            else
            {
                return Ok(getResult);
            }
        }
    }
}
