using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using edu_api.Modules.StudentEvaluations.Databases.Models;
using edu_api.Modules.StudentEvaluations.Services;
using Microsoft.AspNetCore.Mvc;
using SeventyOneDev.Utilities;

namespace edu_api.Modules.StudentEvaluations.Controllers
{
    [Route("api/student_evaluation/study_subject_section_results", Name = "study_subject_section_results")]
    public class StudySubjectSectionResultController:Controller
    {
        readonly StudySubjectSectionResultService _studySubjectSectionResultService;
        
        public StudySubjectSectionResultController(StudySubjectSectionResultService studySubjectSectionResultService){
            _studySubjectSectionResultService = studySubjectSectionResultService;
        }
        
        [HttpGet]
        public async Task<ActionResult<List<v_study_subject_section_result>>> GetStudySubjectSectionResults([FromQuery]v_study_subject_section_result _study_subject_section_result=null)
        {
            if (EFExtension.IsNullOrEmpty(_study_subject_section_result))
            {
                var listResult = await _studySubjectSectionResultService.ListStudySubjectSectionResult(null);
                return Ok(listResult);
            }
            else
            {
                var getResult = await _studySubjectSectionResultService.ListStudySubjectSectionResult(_study_subject_section_result);
                if (!getResult.Any())
                {
                    return Ok(new List<v_study_subject_section_result>());
                }
                else
                {
                    return getResult.ToList();
                }

            }
        }

        [HttpGet("{year_subject_section_result_uid}")]
        public async Task<ActionResult<v_study_subject_section_result>> GetStudySubjectSectionResult([FromRoute]Guid year_subject_section_result_uid)
        {
            var getResult = await _studySubjectSectionResultService.GetStudySubjectSectionResult(year_subject_section_result_uid);
            if (getResult==null)
            {
                return NotFound();
            }
            else
            {
                return Ok(getResult);
            }
        }
    }
}
