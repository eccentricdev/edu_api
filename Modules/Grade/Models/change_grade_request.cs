using System;
using System.Collections.Generic;

namespace edu_api.Modules.Grade.Models
{
  public class change_grade_request
  {
    public Guid? primary_year_subject_section_uid { get; set; }
    public Guid? grade_uid { get; set; }
    public List<change_grade> change_grades { get; set; }

  }

  public class change_grade
  {
    public Guid? student_uid { get; set; }
    public Guid? grade_uid { get; set; }
  }
}
