namespace edu_api.Modules.Grade.Models
{
  public class grade_summary
  {
   public string grade_name_th { get; set; }
   public int? grade_count { get; set; }
  }
}
