using System;
using System.Collections.Generic;

namespace edu_api.Modules.Grade.Models
{
  public class calculate_by_student_request
  {
    public List<Guid> student_uids { get; set; }
  }
}
