using System;

namespace edu_api.Modules.Grade.Models
{
  public class calculate_by_entry_year_request
  {
    public Guid? education_type_uid { get; set; }
    public string from_entry_year_code { get; set; }
    public string to_entry_year_code { get; set; }
  }
}
