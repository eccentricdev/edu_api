namespace edu_api.Modules.Grade.Models
{
  public class grade_import
  {
    public int? row_order { get; set; }
    public int? student_code { get; set; }
    public string student_name { get; set; }
    public string grade_name { get; set; }
    public string failed_reason { get; set; }


  }
}
