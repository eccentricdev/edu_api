using System;

namespace edu_api.Modules.Grade.Models
{
  public class calculate_by_register_year_request
  {
    public Guid? academic_year_uid { get; set; }
    public Guid? academic_semester_uid { get; set; }
    public Guid? education_type_uid { get; set; }
    public string from_entry_year_code { get; set; }
    public string to_entry_year_code { get; set; }

  }
}
