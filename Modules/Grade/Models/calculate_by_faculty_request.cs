using System;
using System.Collections.Generic;

namespace edu_api.Modules.Grade.Models
{
  public class calculate_by_faculty_request
  {
    public faculty_curriculum_request faculty_curriculums { get; set; }
    public Guid? student_status_uid { get; set; }
  }

  public class faculty_curriculum_request
  {
    public Guid? college_faculty_uid { get; set; }
    public Guid? faculty_curriculum_uid { get; set; }
  }
}
