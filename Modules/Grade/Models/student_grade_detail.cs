namespace edu_api.Modules.Grade.Models
{
    public class student_grade_detail
    {
        public string year_subject_code { get; set; }
        public string year_subject_name_th { get; set; }
        public short? credit { get; set; }
        public string grade_name_th { get; set; }
        public string grade_name_en { get; set; }
    }
}
