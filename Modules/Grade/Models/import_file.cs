namespace edu_api.Modules.Grade.Models
{
  public class import_file
  {
    public string name { get; set; }
    public string path { get; set; }
    public int? records {get;set;}
    public int? succeed {get;set;}
    public int? failed {get;set;}
    public string file_name {get;set;}
    public string file_id {get;set;}
  }
}
