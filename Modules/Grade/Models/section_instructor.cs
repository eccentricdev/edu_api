using System;

namespace edu_api.Modules.Grade.Models
{
  public class section_instructor
  {
    public Guid? instructor_uid {get;set;}
    public string instructor_code {get;set;}
    public string display_name_th {get;set;}
    public string display_name_en {get;set;}
  }
}
