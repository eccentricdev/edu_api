using System.Collections.Generic;
using edu_api.Modules.Grade.Databases.Models;

namespace edu_api.Modules.Grade.Models
{
  public class student_grade_report
  {
    public List<v_evaluate_year_subject_section> evaluate_year_subject_section { get; set; }
    public List<v_year_subject_section_student> year_subject_section_student { get; set; }
    public List<grade_summary> grade_summaries { get; set; }
    public int? a  { get; set; }
        public int? bplus  { get; set; }
        public int? b  { get; set; }
        public int? cplus  { get; set; }
        public int? c  { get; set; }
        public int? dplus  { get; set; }
        public int? d  { get; set; }
        public int? f { get; set; }
        public int? s { get; set; }
        public int? u { get; set; }
        public int? i { get; set; }
        public int? ip { get; set; }
        public int? w { get; set; }
        public int? au { get; set; }
        public int? other { get; set; }
        public decimal? gpa  { get; set; }
  }
}
