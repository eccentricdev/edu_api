using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using edu_api.Modules.Grade.Databases.Models;
using SeventyOneDev.Utilities;

namespace edu_api.Modules.Grade.Services
{
    public class GradeCalculationSettingService:EntityUidService<t_grade_calculation_setting,v_grade_calculation_setting>
    {
        private readonly Db _context;
        public GradeCalculationSettingService(Db context) : base(context)
        {
            _context = context;
        }


        public async Task<int> DeleteByIds(List<Guid> uids, ClaimsIdentity claimsIdentity)
        {
            var entitys = new List<t_grade_calculation_setting>();
            foreach (var uid in uids)
            {
                var entity = await _context.t_grade_calculation_setting.GetByKey(uid);
                if (entity == null) throw new Exception("DELETE_NOTFOUND");
                entitys.Add(entity);
            }
            _context.RemoveRange(entitys);
            return await _context.SaveChangesAsync(claimsIdentity);
        }
    }
}