using System;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using edu_api.Modules.Grade.Databases.Models;
using Microsoft.EntityFrameworkCore;
using SeventyOneDev.Utilities;

namespace edu_api.Modules.Grade.Services
{
    public class GradeCriteriaService:EntityUidService<t_grade_criteria,v_grade_criteria>
    {
        private readonly Db _context;
        public GradeCriteriaService(Db context) : base(context)
        {
            _context = context;
        }


        public override Task<int> DeleteById(Guid uid, ClaimsIdentity claimsIdentity)
        {
            var yearSubjectSection = _context.v_year_subject_section.AsNoTracking()
                .Where(w => w.grade_criteria_uid == uid)
                .ToList();
            if(yearSubjectSection.Count > 0) throw new Exception("INUSE");
            
            var finalRegisterSubject = _context.v_final_register_subject.AsNoTracking()
                .Where(w => w.grade_criteria_uid == uid)
                .ToList();
            if(finalRegisterSubject.Count > 0) throw new Exception("INUSE");
            
            return base.DeleteById(uid, claimsIdentity);
        }
    }
}