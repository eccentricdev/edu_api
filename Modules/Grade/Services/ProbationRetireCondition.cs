using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using edu_api.Modules.Grade.Databases.Models;
using Microsoft.EntityFrameworkCore;
using SeventyOneDev.Utilities;

namespace edu_api.Modules.Grade.Services
{
    public class ProbationRetireConditionService : EntityUidService<t_probation_retire_condition, v_probation_retire_condition>
    {
        private readonly Db _context;

        public ProbationRetireConditionService(Db context) : base(context)
        {
            _context = context;
        }
        
        public async  Task<int> DeleteByIds(List<Guid> uids, ClaimsIdentity claimsIdentity)
        {
            var result = new List<t_probation_retire_condition>();
            foreach (var uid in uids)
            {
                var probationRetire = await _context.t_probation_retire_condition.AsNoTracking().GetByKey(uid);
                if(probationRetire == null) throw new Exception("DELETE_NOTFOUND");
                // if(probationRetire.status_id == 1) throw new Exception("INUSE");
                
                result.Add(probationRetire);
            }
            
            _context.RemoveRange(result);
            return await _context.SaveChangesAsync(claimsIdentity);
        }
    }
}