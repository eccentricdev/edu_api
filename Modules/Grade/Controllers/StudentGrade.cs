using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using edu_api.Modules.Grade.Databases.Models;
using edu_api.Modules.Grade.Models;
using edu_api.Modules.Register.Databases;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SeventyOneDev.Utilities;

namespace edu_api.Modules.Grade.Controllers
{
    [Route("api/grade/student_grade",Name = "student_grade")]
    public class StudentGradeController:BaseUidController<t_student_grade,v_student_grade>
    {
        private readonly Db _db;
        public StudentGradeController(EntityUidService<t_student_grade, v_student_grade> entityUidService,Db db) : base(entityUidService)
        {
            _db = db;
        }
        [HttpGet("detail")]
        public async Task<ActionResult<List<v_student_grade>>> GetStudentGradeByStudentCode()
        {
            var claimsIdentity = User.Identity as ClaimsIdentity;
            var userUidString = claimsIdentity?.FindFirst(c => c.Type == "user_uid")?.Value;
            if (string.IsNullOrEmpty(userUidString)) return Unauthorized();
            var userUid = Guid.Parse(userUidString);
            var studentGrades = await _db.v_student_grade.AsNoTracking().Where(s => s.student_uid == userUid)
                .OrderBy(s=>s.academic_year_code).ThenBy(s=>s.academic_semester_code).ToListAsync();
            if (studentGrades.Any())
            {
                foreach (var studentGrade in studentGrades)
                {
                    var studentGradeDetails = await _db.v_final_register_subject.AsNoTracking().Where(f =>
                            f.student_uid == studentGrade.student_uid &&
                            f.academic_year_uid == studentGrade.academic_year_uid &&
                            f.academic_semester_uid == studentGrade.academic_semester_uid)
                        .ProjectTo<v_final_register_subject, student_grade_detail>().ToListAsync();
                    studentGrade.student_grade_details = studentGradeDetails;
                }
            }

            return Ok(studentGrades);

        }
        [HttpGet("detail/{student_uid}")]
        public async Task<ActionResult<List<v_student_grade>>> GetStudentGradeByStudentUid([FromRoute]Guid student_uid)
        {
            var studentGrades = await _db.v_student_grade.AsNoTracking().Where(s => s.student_uid == student_uid)
                .OrderBy(s=>s.academic_year_code).ThenBy(s=>s.academic_semester_code).ToListAsync();
            if (studentGrades.Any())
            {
                foreach (var studentGrade in studentGrades)
                {
                    var studentGradeDetails = await _db.v_final_register_subject.AsNoTracking().Where(f =>
                            f.student_uid == studentGrade.student_uid &&
                            f.academic_year_uid == studentGrade.academic_year_uid &&
                            f.academic_semester_uid == studentGrade.academic_semester_uid)
                        .ProjectTo<v_final_register_subject, student_grade_detail>().ToListAsync();
                    studentGrade.student_grade_details = studentGradeDetails;
                }
            }

            return Ok(studentGrades);

        }
        
    }
}