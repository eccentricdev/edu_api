using System;
using System.Linq;
using System.Threading.Tasks;
using edu_api.Modules.Commons.Databases.Models;
using edu_api.Modules.Grade.Databases.Models;
using edu_api.Modules.Grade.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SeventyOneDev.Utilities;

namespace edu_api.Modules.Grade.Controllers
{
  [ApiController]
  [Route("api/grade/calculation", Name = "calculation")]
  public class GradeCalculationController:ControllerBase
  {
    private readonly Db _db;
    public GradeCalculationController(Db db)
    {
      _db = db;
    }

    [HttpPost("register_year")]
    public async Task<ActionResult> CalcuateByRegisterYear([FromBody]calculate_by_register_year_request calculateByRegisterYearRequest)
    {
      if (calculateByRegisterYearRequest == null) return BadRequest(new Error(400, "ข้อมูลไม่ถูกต้อง"));

      var finalRegisters = await _db.v_final_register.AsNoTracking().Where(f =>
        f.academic_year_uid == calculateByRegisterYearRequest.academic_year_uid &&
        f.academic_semester_uid == calculateByRegisterYearRequest.academic_semester_uid &&
        f.education_type_uid==calculateByRegisterYearRequest.education_type_uid &&
        string.Compare(f.entry_academic_year_code, calculateByRegisterYearRequest.from_entry_year_code, StringComparison.Ordinal) >=0 &&
        string.Compare(f.entry_academic_year_code, calculateByRegisterYearRequest.to_entry_year_code, StringComparison.Ordinal) <= 0
      ).ToListAsync();
      if (finalRegisters.Any())
      {
        return Ok();
      }
      else
      {
        return BadRequest(new Error(400, "ไม่พบข้อมูลที่ต้องการ"));
      }
      //if (calculateByRegisterYearRequest)
      //var finalRegisters = _db.t_final_register_subject.AsNoTracking().Where(f=>f.re

    }
    [HttpPost("entry_year")]
    public async Task<ActionResult> CalcuateByEntryYear([FromBody]calculate_by_entry_year_request calculateByEntryYearRequest)
    {
      if (calculateByEntryYearRequest == null) return BadRequest(new Error(400, "ข้อมูลไม่ถูกต้อง"));
      var finalRegisters = await _db.v_final_register.AsNoTracking().Where(f =>

        f.education_type_uid==calculateByEntryYearRequest.education_type_uid &&
        string.Compare(f.entry_academic_year_code, calculateByEntryYearRequest.from_entry_year_code, StringComparison.Ordinal) >=0 &&
        string.Compare(f.entry_academic_year_code, calculateByEntryYearRequest.to_entry_year_code, StringComparison.Ordinal) <= 0
      ).ToListAsync();
      if (finalRegisters.Any())
      {
        return Ok();
      }
      else
      {
        return BadRequest(new Error(400, "ไม่พบข้อมูลที่ต้องการ"));
      }
    }
    [HttpPost("student")]
    public async Task<ActionResult> CalcuateByStudent([FromBody]calculate_by_student_request calculateByStudentRequest)
    {

      if (calculateByStudentRequest == null) return BadRequest(new Error(400, "ข้อมูลไม่ถูกต้อง"));
      foreach (var studentUid in calculateByStudentRequest.student_uids)
      {
        var currentStudentGrades = await _db.t_student_grade.Where(s => s.student_uid == studentUid).ToListAsync();
        if (currentStudentGrades.Any())
        {
          _db.t_student_grade.RemoveRange(currentStudentGrades);
          await _db.SaveChangesAsync();
        }
        var finalRegisters =
          await _db.v_final_register.AsNoTracking().Where(f => f.student_uid == studentUid).ToListAsync();
        if (finalRegisters.Any())
        {
          decimal totalScores = 0;
          decimal totalCredits = 0;
          foreach (var finalRegisterGroup in finalRegisters.GroupBy(f => new
                     { f.academic_year_code, f.semester_code, f.academic_year_uid, f.academic_semester_uid }))
          {
            var finalRegisterSubjects = await _db.v_final_register_subject.AsNoTracking().Where(s =>
                s.academic_year_uid == finalRegisterGroup.Key.academic_year_uid &&
                s.academic_semester_uid == finalRegisterGroup.Key.academic_semester_uid &&
                s.student_uid==studentUid)
              .ToListAsync();
            var scores = finalRegisterSubjects.Sum(s => s.credit * s.grade_point);
            var credits = finalRegisterSubjects.Sum(s => s.credit);
            if (credits > 0)
            {
              var gpa = scores / credits;
              totalScores += scores ?? 0;
              totalCredits += credits ?? 0;
              var gpax = totalScores / totalCredits;
              var studentGrade = new t_student_grade()
              {
                student_uid = studentUid,
                academic_year_uid = finalRegisterGroup.Key.academic_year_uid,
                academic_semester_uid = finalRegisterGroup.Key.academic_semester_uid,
                register_credits =credits,
                earn_credits =credits,
                total_register_credits =totalCredits,
                total_earn_credits =totalCredits,
                gpa = gpa,
                gpax = gpax
              };
              await _db.t_student_grade.AddAsync(studentGrade);
            }


          }

          await _db.SaveChangesAsync();
        }
      }

      // else
      // {
      //   return BadRequest(new Error(400, "ไม่พบข้อมูลที่ต้องการ"));
      // }
      return Ok();
    }
    [HttpPost("faculty_curriculum")]
    public async Task<ActionResult> CalcuateByFacultyCurriculum([FromBody]calculate_by_faculty_request calculateByFacultyRequest)
    {
      if (calculateByFacultyRequest == null) return BadRequest(new Error(400, "ข้อมูลไม่ถูกต้อง"));

      var finalRegisters = await _db.v_final_register.AsNoTracking().Where(f =>
        f.student_status_uid== calculateByFacultyRequest.student_status_uid &&
        f.college_faculty_uid==calculateByFacultyRequest.faculty_curriculums.college_faculty_uid &&
        f.faculty_curriculum_uid==calculateByFacultyRequest.faculty_curriculums.faculty_curriculum_uid
      ).ToListAsync();
      if (finalRegisters.Any())
      {






        return Ok();
      }
      else
      {
        return BadRequest(new Error(400, "ไม่พบข้อมูลที่ต้องการ"));
      }
    }

  }
}
