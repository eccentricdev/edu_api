using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using edu_api.Modules.Grade.Databases.Models;
using edu_api.Modules.Grade.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SeventyOneDev.Utilities;

namespace edu_api.Modules.Grade.Controllers
{
  [Route("api/grade/record", Name = "record")]
  public class RecordController:Controller
  {
    private readonly Db _db;

    public RecordController(Db db)
    {
      _db = db;
    }
    [Authorize]
    [HttpPut("change")]
    public async Task<ActionResult> ChangeGrade(
      [FromBody] change_grade_request changeGradeRequest)
    {
      var claimsIdentity = User.Identity as ClaimsIdentity;

      if (changeGradeRequest.primary_year_subject_section_uid == null)
        throw new Exception("ไม่มีข้อมูล section ที่ต้องการแก้ไข");
      // if (changeGradeRequest.grade_uid.HasValue)
      // {
      //   var finalRegisterSubjects = await _db.t_final_register_subject.Where(r =>
      //     r.primary_year_subject_section_uid == changeGradeRequest.primary_year_subject_section_uid).ToListAsync();
      //   foreach (var finalRegisterSubject in finalRegisterSubjects)
      //   {
      //     finalRegisterSubject.grade_uid = changeGradeRequest.grade_uid;
      //   }
      //
      //   await _db.SaveChangesAsync(claimsIdentity);
      // }
      // else
      // {
        foreach (var changeGrade in changeGradeRequest.change_grades)
        {
          var finalRegisterSubject = await _db.t_final_register_subject.FirstOrDefaultAsync(r =>
            r.primary_year_subject_section_uid == changeGradeRequest.primary_year_subject_section_uid);
          if (finalRegisterSubject == null) return BadRequest("ไม่พบข้อมูลนักศึกษาในวิชานี้");
          if (!changeGradeRequest.grade_uid.HasValue && !changeGrade.grade_uid.HasValue)
            return BadRequest("ไม่มีเกรดที่ต้องการ");
          finalRegisterSubject.grade_uid = changeGrade.grade_uid??changeGradeRequest.grade_uid;
        }

        await _db.SaveChangesAsync(claimsIdentity);
     // }

      return Ok();
    }

    [HttpPut("s")]
    public async Task<ActionResult> NewRecord([FromBody]List<v_year_subject_section_student> yearSubjectSectionStudents)
    {
      foreach (var yearSubjectSectionStudent in yearSubjectSectionStudents)
      {
        var finalRegisterSubject = await _db.t_final_register_subject.FirstOrDefaultAsync(f =>
          f.final_register_subject_uid == yearSubjectSectionStudent.final_register_subject_uid);
        finalRegisterSubject.midterm_score = yearSubjectSectionStudent.midterm_score;
        finalRegisterSubject.final_score = yearSubjectSectionStudent.final_score;
        finalRegisterSubject.total_score = yearSubjectSectionStudent.total_score;
        finalRegisterSubject.exam_score = yearSubjectSectionStudent.exam_score;
        finalRegisterSubject.grade_uid = yearSubjectSectionStudent.grade_uid;
        finalRegisterSubject.grade_reason_uid = yearSubjectSectionStudent.grade_reason_uid;
        finalRegisterSubject.grade_criteria_uid = yearSubjectSectionStudent.grade_criteria_uid;
        finalRegisterSubject.grade_remark = yearSubjectSectionStudent.grade_remark;
      }

      var evaluationStatus =
        await _db.t_evaluate_status.AsNoTracking().FirstOrDefaultAsync(e => e.evaluate_status_code == "01");

      var yearSubjectSectionUids = yearSubjectSectionStudents.GroupBy(s => s.primary_year_subject_section_uid)
        .Select(s => s.Key).ToList();
      foreach (var yearSubjectSectionUid in yearSubjectSectionUids)
      {
        var yearSubjectSection =
          await _db.t_year_subject_section.FirstOrDefaultAsync(y => y.year_subject_section_uid == yearSubjectSectionUid);
        if (yearSubjectSection != null)
        {
          yearSubjectSection.evaluate_status_uid = evaluationStatus.evaluate_status_uid;
        }
      }

      await _db.SaveChangesAsync();
      return NoContent();
    }

    [HttpPut("approve")]
    public async Task<ActionResult> GradeApprove([FromBody] List<Guid> yearSubjectSectionUids)
    {
      try
      {
        var evaluationStatus =
          await _db.t_evaluate_status.AsNoTracking().FirstOrDefaultAsync(e => e.evaluate_status_code == "02");
        foreach (var yearSubjectSectionUid in yearSubjectSectionUids)
        {
          //chk student
          var yearSubjectSectionStudent = await _db.v_year_subject_section_student.AsNoTracking()
            .Where(w => w.primary_year_subject_section_uid == yearSubjectSectionUid).ToListAsync();
          if (yearSubjectSectionStudent.Count == 0)
            throw new Exception("ไม่สามารถทำการ Approve ได้ เนื่องจากไม่มีนักศึกษาในรายวิชา ");

          var yearSubjectSection =
            await _db.t_year_subject_section.FirstOrDefaultAsync(y =>
              y.year_subject_section_uid == yearSubjectSectionUid);
          yearSubjectSection.evaluate_status_uid = evaluationStatus.evaluate_status_uid;
          yearSubjectSection.is_approved = true;
        }

        await _db.SaveChangesAsync();
        return NoContent();
      }
      catch (Exception ex)
      {
        Console.WriteLine("approve failed: " + ex.Message);
        return BadRequest(ex.Message);
      }
    }

    [HttpPut("draft")]
    public async Task<ActionResult> GradeDraft([FromBody]List<Guid> yearSubjectSectionUids)
    {
      var evaluationStatus =
        await _db.t_evaluate_status.AsNoTracking().FirstOrDefaultAsync(e => e.evaluate_status_code == "03");


      foreach (var yearSubjectSectionUid in yearSubjectSectionUids)
      {
        var yearSubjectSection =
          await _db.t_year_subject_section.FirstOrDefaultAsync(y => y.year_subject_section_uid == yearSubjectSectionUid);
        yearSubjectSection.evaluate_status_uid = evaluationStatus.evaluate_status_uid;
        yearSubjectSection.is_approved = false;
      }

      await _db.SaveChangesAsync();
      return NoContent();
    }
  }
}
