using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using edu_api.Modules.Grade.Databases.Models;
using edu_api.Modules.Grade.Models;
using edu_api.Modules.Study.Databases.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SeventyOneDev.Utilities;

namespace edu_api.Modules.Grade.Controllers
{
  [Route("api/grade/faculty_staff", Name = "faculty_staff")]
  public class FacultyStaffController:Controller
  {
    private readonly Db _db;

    public FacultyStaffController(Db db)
    {
      _db = db;
    }
    [HttpGet("{college_faculty_uid}")]
    public async Task<ActionResult<List<section_instructor>>> GetFacultyStaff([FromRoute] Guid college_faculty_uid,[FromQuery]v_faculty_staff facultyStaff)
    {
      if (EFExtension.IsNullOrEmpty(facultyStaff))
      {
        var facultyStaves = await _db.v_faculty_staff.AsNoTracking().ToListAsync();
        return Ok(facultyStaves);
      }
      else
      {
        var facultyStaves = await _db.v_faculty_staff.AsNoTracking().Search(facultyStaff).ToListAsync();
        return Ok(facultyStaves);
      }
    }
  }
}
