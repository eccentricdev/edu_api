using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using edu_api.Modules.Grade.Databases.Models;
using edu_api.Modules.Grade.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SeventyOneDev.Utilities;

namespace edu_api.Modules.Grade.Controllers
{
    [Route("api/grade/grade_calculation_setting", Name = "grade_calculation_setting")]
    public class GradeCalculationSettingController:BaseUidController<t_grade_calculation_setting,v_grade_calculation_setting>
    {
        private readonly GradeCalculationSettingService _entityUidService;
        public GradeCalculationSettingController(GradeCalculationSettingService entityUidService) : base(entityUidService)
        {
            _entityUidService = entityUidService;
        }
        
        [Authorize]
        [HttpDelete("s")]
        public virtual async Task<ActionResult> DeleteEntityIds([FromBody] List<Guid> uids)
        {
            try
            {
                var deleteCount = await _entityUidService.DeleteByIds(uids, User.Identity as ClaimsIdentity);
                return deleteCount > 0 ? NoContent() : NotFound();
            }
            catch (DbUpdateException)
            {
                return BadRequest(ErrorMessage.Get("INUSE"));
            }
            catch (Exception ex)
            {
                Console.WriteLine("Delete failed: " + ex.Message);
                return BadRequest(ErrorMessage.Get(ex.Message));
            }
        }


    }
}