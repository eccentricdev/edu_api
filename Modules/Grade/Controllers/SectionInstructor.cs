using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using edu_api.Modules.Grade.Models;
using edu_api.Modules.Study.Databases.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SeventyOneDev.Utilities;

namespace edu_api.Modules.Grade.Controllers
{
  [Route("api/grade/section_instructor", Name = "section_instructor")]
  public class SectionInstructorController:Controller
  {
    private readonly Db _db;

    public SectionInstructorController(Db db)
    {
      _db = db;
    }
    [HttpGet("{year_subject_section_uid}")]
    public async Task<ActionResult<List<section_instructor>>> GetSectionInstructor([FromRoute] Guid year_subject_section_uid)
    {
      var sectionInstructors = await _db.v_year_subject_section_instructor.AsNoTracking()
        .Where(y => y.year_subject_section_uid == year_subject_section_uid)
        .ProjectTo<v_year_subject_section_instructor, section_instructor>().ToListAsync();
      return sectionInstructors;
    }
  }
}
