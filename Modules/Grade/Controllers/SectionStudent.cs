using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using ClosedXML.Excel;
using edu_api.Modules.Grade.Databases.Models;
using edu_api.Modules.Grade.Models;
using edu_api.Modules.InstructorEvaluations.Databases.Models;
using edu_api.Modules.Utilities.Models;
using ExcelDataReader;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using SeventyOneDev.Utilities;

namespace edu_api.Modules.Grade.Controllers
{
  [Route("api/grade/section_student", Name = "section_student")]
  public class SectionStudentController:Controller
  {
    private readonly Db _db;
    private readonly application_setting _applicationSetting;
    public SectionStudentController(Db db,IOptions<application_setting> applicationSetting)
    {
      _db = db;
      _applicationSetting = applicationSetting.Value;
    }

    [HttpPost("import")]
    public async Task<ActionResult<v_year_subject_section_student>> EvaluationUpload()
    {
      var files = Request.Form.Files;
      if (!Directory.Exists(_applicationSetting.document_path + "/edu"))
      {
        Directory.CreateDirectory(_applicationSetting.document_path + "/edu");
      }

      var uploads = Path.Combine(_applicationSetting.document_path, "edu");
      var fileNames = new List<import_file>();
      foreach (var file in files)
      {
        if (file.Length > 0)
        {
          var id = Guid.NewGuid().ToString();
          var newFileName = id + file.FileName.Substring(file.FileName.LastIndexOf('.'));
          using (var fileStream = new FileStream(Path.Combine(uploads, newFileName), FileMode.Create))
          {
            await file.CopyToAsync(fileStream);
          }

          fileNames.Add(new import_file()
          {
            path = _applicationSetting.document_path + "/edu/" + newFileName, name = newFileName, file_name = file.FileName,
            file_id = id
          });

        }
      }

      var paths = _applicationSetting.document_path.Split("/");
      var physicalPath = string.Join("/", paths.Take(paths.Count() - 1).ToList());
      var allGradeImports = new List<grade_import>();
      //var existingGradeImports = new List<grade_data>();
      foreach (var fileName in fileNames)
      {
        System.Text.Encoding.RegisterProvider(System.Text.CodePagesEncodingProvider.Instance);

        await using var stream = new FileStream(fileName.path, FileMode.Open, FileAccess.Read);
        var subjectHeaders = new Dictionary<int, string>();
        var noOfSubjects = 0;
        //var defaultCurriculumUid = Guid.Empty;
        //var educationPlanSubjectMap = new List<v_education_plan_subject_map>();
        try
        {
          using var reader = ExcelReaderFactory.CreateReader(stream);
          do
          {
            reader.Read();
            while (reader.Read())
            {
              var rowOrder = reader.GetDouble(0);
              var studentCode = reader.GetDouble(1);
              var studentName = reader.GetValue(2).ToString();
              var gradeName = reader.GetValue(3)?.ToString();
              var failedReason = reader.GetValue(4)?.ToString();
              var gradeImport = new grade_import()
              {
                row_order = (int)rowOrder,
                student_code = (int)studentCode,
                student_name = studentName,
                grade_name = gradeName,
                failed_reason = failedReason
              };
              allGradeImports.Add(gradeImport);
            }
          } while (reader.NextResult());
        }
        catch (Exception e)
        {
          // ignored
          Console.WriteLine(e);
        }

      }

      return Ok(allGradeImports);
    }

    [HttpGet("export/{year_subject_section_uid}")]
    public async Task<ActionResult> ExcelExport([FromRoute]Guid year_subject_section_uid
     // [FromRoute] Guid question_category_uid
      )
    {
      var students = await _db.v_year_subject_section_student.AsNoTracking()
        .Where(y=>y.primary_year_subject_section_uid==year_subject_section_uid)
        .OrderBy(y=>y.student_code)
        .ToListAsync();
      using (var workbook = new XLWorkbook())
      {
        var worksheet = workbook.Worksheets.Add("รายชื่อนักศึกษา");
        var currentRow = 1;
        worksheet.Cell(currentRow, 1).Value = "ลำดับที่";
        worksheet.Cell(currentRow, 2).Value = "รหัสนักศึกษา";
        worksheet.Cell(currentRow, 3).Value = "ชื่อ-สกุล";
        worksheet.Cell(currentRow, 4).Value = "เกรด";
        worksheet.Cell(currentRow, 5).Value = "เหตุผลที่ได้ F";
        currentRow += 1;
        foreach (var student in students)
        {
          worksheet.Cell(currentRow, 1).Value = currentRow - 1;
          worksheet.Cell(currentRow, 2).Value = student.student_code;
          worksheet.Cell(currentRow, 3).Value = student.display_name_th;
          worksheet.Cell(currentRow, 4).Value = student.grade_name_th;
          worksheet.Cell(currentRow, 5).Value = student.grade_reason_name_th;
          currentRow += 1;
        }

        using (var stream = new MemoryStream())
        {
          workbook.SaveAs(stream);
          var content = stream.ToArray();

          return File(
            content,
            "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
            "student_grade_" + Guid.NewGuid().ToString() + ".xlsx");
        }
      }
    }
  }
}
