using System.Threading.Tasks;
using edu_api.Modules.Grade.Databases.Models;
using Microsoft.AspNetCore.Mvc;
using SeventyOneDev.Utilities;

namespace edu_api.Modules.Grade.Controllers
{
  [Route("api/grade/grade_re_evaluation", Name = "grade_re_evaluation")]
  public class GradeReEvaluationController:Controller
  {
    private readonly Db _db;
    public GradeReEvaluationController(Db db)
    {
      _db = db;
    }
    [HttpPost("re_evaluation")]
    public async Task<ActionResult> ReEvaluation(t_grade_re_evaluation gradeReEvaluation)
    {
      return Ok();
    }


  }
}
