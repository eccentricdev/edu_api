using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;
using edu_api.Modules.Grade.Databases.Models;
using edu_api.Modules.Grade.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SeventyOneDev.Utilities;

namespace edu_api.Modules.Grade.Controllers
{
  [Route("api/grade/student_grade_resport", Name = "student_grade_resport")]
  public class StudentGradeReportController : Controller
  {
    private readonly Db _db;
    private readonly IHttpClientFactory _httpClientFactory;

    public StudentGradeReportController(IHttpClientFactory httpClientFactory, Db db)
    {
      _httpClientFactory = httpClientFactory;
      _db = db;
    }

    [HttpGet("print/{year_subject_section_uid}/{type}")]
    public async Task<ActionResult> GetStudentGradeReport([FromRoute] Guid year_subject_section_uid,
      [FromRoute] string type,[FromQuery]bool? view_data)
    {
      var evaluateYearSubjectSection = await _db.v_evaluate_year_subject_section.AsNoTracking()
        .FirstOrDefaultAsync(y => y.year_subject_section_uid == year_subject_section_uid);
      var yearSubjectSectionStudents = await _db.v_year_subject_section_student.Where(y =>
        y.primary_year_subject_section_uid == year_subject_section_uid).OrderBy(y=>y.student_code).ToListAsync();

      //var listResult = await _studentDisciplineReportService.ListStudentDisciplineReport(student_code);
      var gradeSummaries = yearSubjectSectionStudents.GroupBy(y => y.grade_name_th).Select(y => new grade_summary()
      {
grade_name_th = y.Key,
grade_count = y.Count()
      }).ToList();

      // F
      //   A
      // B
      // D+
      //   C
      // D
      //   test
      // B+
      //   C+
      //   M
      // M
      //   IS
      var studentGradeReportReport = new student_grade_report()
      {
        evaluate_year_subject_section = new List<v_evaluate_year_subject_section>() { evaluateYearSubjectSection },
        year_subject_section_student = yearSubjectSectionStudents,
        grade_summaries = gradeSummaries,
        a = (gradeSummaries.FirstOrDefault(g=>g.grade_name_th=="A")?.grade_count)??0,
        bplus = (gradeSummaries.FirstOrDefault(g=>g.grade_name_th=="B+")?.grade_count)??0,
        b = (gradeSummaries.FirstOrDefault(g=>g.grade_name_th=="B")?.grade_count)??0,
        cplus = (gradeSummaries.FirstOrDefault(g=>g.grade_name_th=="C+")?.grade_count)??0,
        c = (gradeSummaries.FirstOrDefault(g=>g.grade_name_th=="C")?.grade_count)??0,
        dplus = (gradeSummaries.FirstOrDefault(g=>g.grade_name_th=="D+")?.grade_count)??0,
        d = (gradeSummaries.FirstOrDefault(g=>g.grade_name_th=="D")?.grade_count)??0,
        f = (gradeSummaries.FirstOrDefault(g=>g.grade_name_th=="F")?.grade_count)??0,
        s = (gradeSummaries.FirstOrDefault(g=>g.grade_name_th=="S")?.grade_count)??0,
        u = (gradeSummaries.FirstOrDefault(g=>g.grade_name_th=="U")?.grade_count)??0,
        i = (gradeSummaries.FirstOrDefault(g=>g.grade_name_th=="I")?.grade_count)??0,
        ip = (gradeSummaries.FirstOrDefault(g=>g.grade_name_th=="IP")?.grade_count)??0,
        w = (gradeSummaries.FirstOrDefault(g=>g.grade_name_th=="W")?.grade_count)??0,
        au = (gradeSummaries.FirstOrDefault(g=>g.grade_name_th=="AU")?.grade_count)??0,
        other = 0
      };
      if (view_data ?? false)
      {
        return Ok(studentGradeReportReport);
      }
      var httpClient = _httpClientFactory.CreateClient("report");
      var content = new ByteArrayContent(JsonSerializer.SerializeToUtf8Bytes(studentGradeReportReport));
      content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");
      var res = await httpClient.PostAsync("reports/student_grade/" + (type == "view" ? "pdf" : type),
        content);
      if (res.StatusCode == HttpStatusCode.OK)
      {
        var stream = await res.Content.ReadAsStreamAsync();
        switch (type)
        {
          case "view":
            return File(stream, "application/pdf");
          case "pdf":
            return File(stream, "application/pdf", Guid.NewGuid().ToString() + ".pdf");
          case "xls":
            return File(stream, "application/vnd.ms-excel", Guid.NewGuid().ToString() + ".xlsx");
          case "mht":
            return File(stream, "multipart/related", Guid.NewGuid().ToString() + ".mht");
          case "csv":
            return File(stream, "text/csv", Guid.NewGuid().ToString() + ".csv");

        }

        return NoContent();
      }
      else
      {
        return NotFound();
      }
    }
  }

}
