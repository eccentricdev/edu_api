using edu_api.Modules.Grade.Databases.Models;
using edu_api.Modules.Grade.Services;
using Microsoft.AspNetCore.Mvc;
using SeventyOneDev.Utilities;

namespace edu_api.Modules.Grade.Controllers
{
    [Route("api/grade/grade_criteria", Name = "grade_criteria")]
    
    public class GradeCriteriaController:BaseUidController<t_grade_criteria,v_grade_criteria>
    {
        public GradeCriteriaController(GradeCriteriaService entityUidService) : base(entityUidService)
        {

        }
    }
}