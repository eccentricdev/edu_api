using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Security.Claims;
using System.Threading.Tasks;
using edu_api.Modules.Discipline.Databases.Models;
using edu_api.Modules.Grade.Databases.Models;
using edu_api.Modules.Grade.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SeventyOneDev.Utilities;

namespace edu_api.Modules.Grade.Controllers
{
    [Route("api/grade/probation_retire_condition", Name = "probation_retire_condition")]
    public class ProbationRetireConditionController:BaseUidController<t_probation_retire_condition,v_probation_retire_condition>
    {
        private readonly ProbationRetireConditionService _entityUidService;
        
        public ProbationRetireConditionController(ProbationRetireConditionService entityUidService) : base(entityUidService)
        {
            _entityUidService = entityUidService;
        }
        
        
        [Authorize]
        [HttpDelete("uids")]
        public async  Task<ActionResult> DeleteEntityIds([FromBody] List<Guid> uids)
        {
            try
            {
                var deleteCount = await _entityUidService.DeleteByIds(uids, User.Identity as ClaimsIdentity);
                return deleteCount > 0 ? NoContent() : NotFound();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Delete failed: " + ex.Message);
                return BadRequest(ErrorMessage.Get(ex.Message));
            }
        }
    }
}