using edu_api.Modules.EducationChecks.Services;
using edu_api.Modules.Grade.Services;
using Microsoft.Extensions.DependencyInjection;

namespace edu_api.Modules.EducationChecks.Configures
{
    public static class GradeExtension
    {
        public static IServiceCollection AddGradeServices(this IServiceCollection services)
        {
            
            services.AddScoped<GradeCriteriaService>();
            services.AddScoped<ProbationRetireConditionService>();
            services.AddScoped<GradeCalculationSettingService>();

            return services;
        }
    }
}