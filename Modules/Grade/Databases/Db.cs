using edu_api.Modules.Grade.Databases.Models;
using edu_api.Modules.Profiles.Databases.Models;
using Microsoft.EntityFrameworkCore;
using rsu_common_api.models;

namespace SeventyOneDev.Utilities
{
  public partial class Db : DbContext
  {
    // public DbSet<t_grade> t_grade { get; set; }
    // public DbSet<v_grade> v_grade { get; set; }
    public DbSet<t_grade_criteria> t_grade_criteria { get; set; }
    public DbSet<v_grade_criteria> v_grade_criteria { get; set; }
    public DbSet<t_grade_criteria_option> t_grade_criteria_option { get; set; }
    public DbSet<v_grade_criteria_option> v_grade_criteria_option { get; set; }
    public DbSet<t_grade_reason> t_grade_reason { get; set; }
    public DbSet<v_grade_reason> v_grade_reason { get; set; }
    public DbSet<t_evaluate_status> t_evaluate_status { get; set; }
    public DbSet<v_evaluate_status> v_evaluate_status { get; set; }
    public DbSet<v_evaluate_year_subject_section> v_evaluate_year_subject_section { get; set; }
    public DbSet<v_year_subject_section_student> v_year_subject_section_student { get; set; }
    public DbSet<v_faculty_staff> v_faculty_staff { get; set; }

    public DbSet<t_grade_recorder> t_grade_recorder { get; set; }
    public DbSet<v_grade_recorder> v_grade_recorder { get; set; }

    public DbSet<t_grade_calendar> t_grade_calendar { get; set; }
    public DbSet<v_grade_calendar> v_grade_calendar { get; set; }

    public DbSet<t_grade_re_evaluation> t_grade_re_evaluation { get; set; }
    public DbSet<v_grade_re_evaluation> v_grade_re_evaluation { get; set; }

    public DbSet<t_probation_retire_condition> t_probation_retire_condition { get; set; }
    public DbSet<v_probation_retire_condition> v_probation_retire_condition { get; set; }
    public DbSet<t_probation_retire_condition_detail> t_probation_retire_condition_detail { get; set; }
    public DbSet<v_probation_retire_condition_detail> v_probation_retire_condition_detail { get; set; }

    public DbSet<t_grade_calculation_method> t_grade_calculation_method { get; set; }
    public DbSet<v_grade_calculation_method> v_grade_calculation_method { get; set; }

    public DbSet<t_grade_calculation_setting> t_grade_calculation_setting { get; set; }
    public DbSet<v_grade_calculation_setting> v_grade_calculation_setting { get; set; }

    public DbSet<t_re_evaluation_type> t_re_evaluation_type { get; set; }
    public DbSet<v_re_evaluation_type> v_re_evaluation_type { get; set; }

    public DbSet<t_year_subject_grade_criteria_option> t_year_subject_grade_criteria_option { get; set; }
    public DbSet<v_year_subject_grade_criteria_option> v_year_subject_grade_criteria_option { get; set; }

    public DbSet<t_student_grade> t_student_grade { get; set; }
    public DbSet<v_student_grade> v_student_grade { get; set; }

    private void OnGradeModelCreating(ModelBuilder builder, string schema)
    {

      // builder.Entity<t_grade>().ToTable("grade", schema);
      // builder.Entity<v_grade>().ToView("v_grade", schema);
      builder.Entity<t_grade_criteria>().ToTable("grade_criteria", schema);
      builder.Entity<v_grade_criteria>().ToView("v_grade_criteria", schema);
      builder.Entity<t_grade_criteria_option>().ToTable("grade_criteria_option", schema);
      builder.Entity<t_grade_criteria_option>().HasOne(g => g.grade_criteria).WithMany(g => g.grade_criteria_options)
        .HasForeignKey(g => g.grade_criteria_uid).OnDelete(DeleteBehavior.Cascade);
      builder.Entity<v_grade_criteria_option>().ToView("v_grade_criteria_option", schema);
      builder.Entity<v_grade_criteria_option>().HasOne(g => g.grade_criteria).WithMany(g => g.grade_criteria_options)
        .HasForeignKey(g => g.grade_criteria_uid).OnDelete(DeleteBehavior.Cascade);
      builder.Entity<t_grade_reason>().ToTable("grade_reason", schema);
      builder.Entity<v_grade_reason>().ToView("v_grade_reason", schema);

      builder.Entity<t_evaluate_status>().ToTable("evaluate_status", schema);
      builder.Entity<v_evaluate_status>().ToView("v_evaluate_status", schema);

      builder.Entity<v_evaluate_year_subject_section>().ToView("v_evaluate_year_subject_section", schema);
      builder.Entity<v_year_subject_section_student>().ToView("v_year_subject_section_student", schema);
      builder.Entity<v_faculty_staff>().ToView("v_faculty_staff", schema);

      builder.Entity<t_grade_recorder>().ToTable("grade_recorder", schema);
      builder.Entity<v_grade_recorder>().ToView("v_grade_recorder", schema);

      builder.Entity<t_grade_calendar>().ToTable("grade_calendar", schema);
      builder.Entity<v_grade_calendar>().ToView("v_grade_calendar", schema);
      builder.Entity<t_grade_re_evaluation>().ToTable("grade_re_evaluation", schema);
      builder.Entity<v_grade_re_evaluation>().ToView("v_grade_re_evaluation", schema);

      builder.Entity<t_probation_retire_condition>().ToTable("probation_retire_condition", schema);
      builder.Entity<v_probation_retire_condition>().ToView("v_probation_retire_condition", schema);
      builder.Entity<t_probation_retire_condition_detail>().ToTable("probation_retire_condition_detail", schema);
      builder.Entity<t_probation_retire_condition_detail>().HasOne(p => p.probation_retire_condition)
        .WithMany(p => p.probation_retire_condition_details).HasForeignKey(p => p.probation_retire_condition_uid)
        .OnDelete(DeleteBehavior.Cascade);
      builder.Entity<v_probation_retire_condition_detail>().ToView("v_probation_retire_condition_detail", schema);
      builder.Entity<v_probation_retire_condition_detail>().HasOne(p => p.probation_retire_condition)
        .WithMany(p => p.probation_retire_condition_details).HasForeignKey(p => p.probation_retire_condition_uid)
        .OnDelete(DeleteBehavior.Cascade);

      builder.Entity<t_grade_calculation_method>().ToTable("grade_calculation_method", schema);
      builder.Entity<v_grade_calculation_method>().ToView("v_grade_calculation_method", schema);

      builder.Entity<t_grade_calculation_setting>().ToTable("grade_calculation_setting", schema);
      builder.Entity<v_grade_calculation_setting>().ToView("v_grade_calculation_setting", schema);

      builder.Entity<t_re_evaluation_type>().ToTable("re_evaluation_type", schema);
      builder.Entity<v_re_evaluation_type>().ToView("v_re_evaluation_type", schema);

      builder.Entity<t_year_subject_grade_criteria_option>().ToTable("year_subject_grade_criteria_option", schema);
      builder.Entity<v_year_subject_grade_criteria_option>().ToView("v_year_subject_grade_criteria_option", schema);
      
      builder.Entity<t_student_grade>().ToTable("student_grade", schema);
      builder.Entity<v_student_grade>().ToView("v_student_grade", schema);



    }
  }
}
