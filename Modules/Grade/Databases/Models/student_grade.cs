using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using edu_api.Modules.Grade.Models;
using SeventyOneDev.Utilities;
using SeventyOneDev.Utilities.Attributes;

namespace edu_api.Modules.Grade.Databases.Models
{
    public class student_grade:base_table
    {
        [Key]
        public Guid? student_grade_uid { get; set; }
        public Guid? student_uid { get; set; }
        public Guid? academic_year_uid { get; set; }
        public Guid? academic_semester_uid { get; set; }
        [Column(TypeName = "decimal(5,1)")]public decimal? register_credits { get; set; }
        [Column(TypeName = "decimal(5,1)")]public decimal? earn_credits { get; set; }
        [Column(TypeName = "decimal(5,1)")]public decimal? total_register_credits { get; set; }
        [Column(TypeName = "decimal(5,1)")]public decimal? total_earn_credits { get; set; }
        
        [Column(TypeName = "decimal(5,2)")]public decimal? gpa { get; set; }
        [Column(TypeName = "decimal(5,2)")]public decimal? gpax { get; set; }
    }
    //[GeneratedUidController("api/grade/student_grade")]
    public class t_student_grade : student_grade
    {
        
    }

    public class v_student_grade : student_grade
    {
        public string academic_year_code {get;set;}
        public string academic_year_name_th {get;set;}
        public string academic_year_name_en {get;set;}
        public string academic_semester_code  {get;set;}
        [NotMapped]public List<student_grade_detail> student_grade_details { get; set; }
    }
}