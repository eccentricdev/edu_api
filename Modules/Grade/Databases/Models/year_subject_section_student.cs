using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.CodeAnalysis.CSharp;
using SeventyOneDev.Utilities;
using SeventyOneDev.Utilities.Attributes;

namespace edu_api.Modules.Grade.Databases.Models
{
  [GeneratedUidController("api/grade/year_subject_section_student")]
  public class v_year_subject_section_student:base_table
  {
    [Key]
    public Guid? final_register_subject_uid { get; set; }
    public Guid? student_uid { get; set; }
    public string student_code { get; set; }
    public string display_name_th { get; set; }
    public string display_name_en { get; set; }
    public string faculty_curriculum_code { get; set; }

    [Column(TypeName = "decimal(5,2)")]public decimal? midterm_score { get; set; }
    [Column(TypeName = "decimal(5,2)")]public decimal? final_score { get; set; }
    [Column(TypeName = "decimal(5,2)")]public decimal? total_score { get; set; }
    [Column(TypeName = "decimal(5,2)")]public decimal? exam_score { get; set; }
    public Guid? grade_uid { get; set; }
    public Guid? grade_reason_uid { get; set; }
    public Guid? grade_criteria_uid { get; set; }
    public string grade_remark { get; set; }
    public Guid? primary_year_subject_section_uid { get; set; }
    public Guid? academic_year_uid {get;set;}
    public Guid? academic_semester_uid {get;set;}
    public string year_subject_code {get;set;}
    public string year_subject_name_th {get;set;}
    public string year_subject_name_en {get;set;}
    public string lecture_section_code {get;set;}
    public string lab_section_code {get;set;}
    public string grade_name_th {get;set;}
    public string grade_reason_name_th {get;set;}
    public string grade_reason_name_en {get;set;}
    public int? study_type_id { get; set; }
    public string study_type_name_th { get; set; }
    public string study_type_name_en { get; set; }
    //public bool? is_approved { get; set; }
  }
}
