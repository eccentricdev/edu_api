using System;
using System.ComponentModel.DataAnnotations;
using SeventyOneDev.Utilities;
using SeventyOneDev.Utilities.Attributes;

namespace edu_api.Modules.Grade.Databases.Models
{
  public class grade_recorder:base_table
  {
    [Key]
    public Guid? year_subject_section_uid { get; set; }
    public Guid? recorder_uid { get; set; }
  }
  [GeneratedUidController("api/grade/grade_recorder")]
  public class t_grade_recorder : grade_recorder
  {

  }

  public class v_grade_recorder : grade_recorder
  {
    public Guid? education_type_uid { get; set; }
    public string education_type_code { get; set; }
    public string education_type_name_th { get; set; }
    public string education_type_name_en { get; set; }
    public string year_subject_code { get; set; }
    public string year_subject_name_th { get; set; }
    public string year_subject_name_en { get; set; }
    public Guid? academic_year_uid { get; set; }
    public string academic_year_code { get; set; }
    public Guid? academic_semester_uid { get; set; }
    public string semester_code { get; set; }
    public string section_code { get; set; }
    public string section_type_code { get; set; }
    public string primary_instructor_name_th { get; set; }
    public string primary_instructor_name_en { get; set; }
    public Guid? evaluate_status_uid { get; set; }
    public Guid? owner_college_faculty_uid { get; set; }
    public string display_name_th { get; set; }
    public string display_name_en  { get; set; }
  }
}
