using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;
using SeventyOneDev.Utilities;
using SeventyOneDev.Utilities.Attributes;

namespace edu_api.Modules.Grade.Databases.Models
{
  public class probation_retire_condition_detail:base_table
  {
    [Key]
    public Guid? probation_retire_condition_detail_uid { get; set; }
    public Guid? probation_retire_condition_uid { get; set; }
    [Column(TypeName = "decimal(5,2)")]public decimal? from_grade { get; set; }
    [Column(TypeName = "decimal(5,2)")]public decimal? to_grade { get; set; }
    [Column(TypeName = "decimal(5,2)")]public decimal? from_credit { get; set; }
    [Column(TypeName = "decimal(5,2)")]public decimal? to_credit { get; set; }
    public Guid? start_semester_uid { get; set; }
    public short? previous_probations { get; set; }
    public Guid? pre_student_status_uid { get; set; }
    public Guid? student_status_uid { get; set; }
  }

  [GeneratedUidController("api/grade/probation_retire_condition_detail")]
  public class t_probation_retire_condition_detail : probation_retire_condition_detail
  {
    [JsonIgnore]
    public t_probation_retire_condition probation_retire_condition { get; set; }
  }

  public class v_probation_retire_condition_detail : probation_retire_condition_detail
  {
    [JsonIgnore]
    public v_probation_retire_condition probation_retire_condition { get; set; }
    public string start_semester_name_th { get; set; }
    public string pre_student_status_name_th { get; set; }
    public string student_status_name_th { get; set; }
  }
}
