using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using SeventyOneDev.Utilities;
using SeventyOneDev.Utilities.Attributes;

namespace edu_api.Modules.Grade.Databases.Models
{
  public class year_subject_grade_criteria_option:base_table
  {
    [Key]
    public Guid? year_grade_criteria_option_uid { get; set; }
    public Guid? year_subject_uid { get; set; }
    [Column(TypeName = "decimal(18,2)")]
    public decimal? min_score { get; set; } // <50
    [Column(TypeName = "decimal(18,2)")]
    public decimal? max_score { get; set; }
    public Guid? grade_uid { get; set; } //F
    public string description { get; set; }
  }
  [GeneratedUidController("api/grade/year_subject_grade_criteria_option")]
  public class t_year_subject_grade_criteria_option : year_subject_grade_criteria_option
  {

  }

  public class v_year_subject_grade_criteria_option : year_subject_grade_criteria_option
  {

  }
}
