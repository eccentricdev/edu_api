using System;
using System.ComponentModel.DataAnnotations;

namespace edu_api.Modules.Grade.Databases.Models
{
  public class v_faculty_staff
  {
    [Key]
    public Guid? personnel_uid {get;set;}
    public string personnel_no {get;set;}
    public string display_name_th {get;set;}
    public string display_name_en {get;set;}
  }
}
