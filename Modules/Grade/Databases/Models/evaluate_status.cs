using System;
using System.ComponentModel.DataAnnotations;
using SeventyOneDev.Utilities;
using SeventyOneDev.Utilities.Attributes;

namespace edu_api.Modules.Grade.Databases.Models
{
  public class evaluate_status:base_table
  {
    [Key]
    public Guid? evaluate_status_uid { get; set; }
    public string evaluate_status_code { get; set; }
    public string evaluate_status_name_th { get; set; }
    public string evaluate_status_name_en { get; set; }
  }
  [GeneratedUidController("api/grade/evaluate_status")]
  public class t_evaluate_status : evaluate_status
  {

  }

  public class v_evaluate_status : evaluate_status
  {

  }
}
