using System;
using System.ComponentModel.DataAnnotations;
using SeventyOneDev.Utilities;
using SeventyOneDev.Utilities.Attributes;

namespace edu_api.Modules.Grade.Databases.Models
{
  public class grade_reason:base_table
  {
    [Key]
    public Guid? grade_reason_uid { get; set; }
    [Unique]
    public string grade_reason_code { get; set; }
    public string grade_reason_name_th { get; set; }
    public string grade_reason_name_en { get; set; }
    public Boolean? is_set_by_faculty { get; set; }
  }
  [GeneratedUidController("api/grade/grade_reason")]
  public class t_grade_reason : grade_reason
  {

  }

  public class v_grade_reason : grade_reason
  {

  }
}
