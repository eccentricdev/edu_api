using System;
using System.ComponentModel.DataAnnotations;
using SeventyOneDev.Utilities;

namespace edu_api.Modules.Grade.Databases.Models
{
  public class grade_re_evaluation:base_table
  {
    [Key]
    public Guid? grade_re_evaluation_uid { get; set; }
    public Guid? re_evaluation_type_uid { get; set; }
    public Guid? education_type_uid { get; set; }
    public Guid? academic_year_uid { get; set; }
    public Guid? academic_semester_uid { get; set; }
    public Guid? year_subject_uid { get; set; }
    public short? study_type_id { get; set; }
    public Guid? year_subject_section_uid { get; set; }
    public Guid? original_grade_uid { get; set; }
    public Guid? to_grade_uid { get; set; }

  }

  public class t_grade_re_evaluation : grade_re_evaluation
  {

  }

  public class v_grade_re_evaluation : grade_re_evaluation
  {
  }
}
