using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using SeventyOneDev.Utilities;
using SeventyOneDev.Utilities.Attributes;

namespace edu_api.Modules.Grade.Databases.Models
{
  public class grade_criteria:base_table
  {
    [Key]
    public Guid? grade_criteria_uid { get; set; }
    public string grade_criteria_code { get; set; }
    [Unique]
    public string grade_criteria_name_th { get; set; }
    public string grade_criteria_name_en { get; set; }
    public string grade_criteria_description { get; set; }
  }
  // [GeneratedUidController("api/grade/grade_criteria")]
  public class t_grade_criteria : grade_criteria
  {
    [Include]
    public List<t_grade_criteria_option> grade_criteria_options { get; set; }
  }

  public class v_grade_criteria : grade_criteria
  {
    [Include]
    public List<v_grade_criteria_option> grade_criteria_options { get; set; }
  }
}
