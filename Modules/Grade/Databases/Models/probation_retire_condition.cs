using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using SeventyOneDev.Utilities;
using SeventyOneDev.Utilities.Attributes;

namespace edu_api.Modules.Grade.Databases.Models
{
  public class probation_retire_condition:base_table
  {
    [Key]
    public Guid? probation_retire_condition_uid { get; set; }
    public Guid? education_type_uid { get; set; }
    public DateTime? start_date { get; set; }
    public DateTime? end_date { get; set; }


  }
  // [GeneratedUidController("api/grade/probation_retire_condition")]
  public class t_probation_retire_condition : probation_retire_condition
  {
    [Include]
    public List<t_probation_retire_condition_detail> probation_retire_condition_details { get; set; }
  }

  public class v_probation_retire_condition : probation_retire_condition
  {
    [Include]
    public List<v_probation_retire_condition_detail> probation_retire_condition_details { get; set; }
  }
}
