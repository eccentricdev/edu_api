// using System;
// using System.ComponentModel.DataAnnotations.Schema;
// using SeventyOneDev.Utilities;
//
// namespace edu_api.Modules.Grade.Databases.Models
// {
//   public class grade:base_table
//   {
//     public Guid? grade_uid { get; set; }
//     // public string grade_code { get; set; } //F
//     // public decimal? grade_value { get; set; }
//     // public string remark { get; set; }
//     public string grade_code { get; set; }
//     public string grade_name_th { get; set; }
//     public string grade_name_en { get; set; }
//     public bool? is_calculate_gpa { get; set; }
//     public int? display_order { get; set; }
//     public string description { get; set; }
//     public bool? is_calculate_credit { get; set; }
//     public bool? is_pass { get; set; }
//     public bool? is_calculate_honor { get; set; }
//     [Column(TypeName = "decimal(18,2)")] public decimal? grade_point { get; set; }
//   }
//
//   public class t_grade : grade
//   {
//
//   }
//
//   public class v_grade : grade
//   {
//
//   }
// }
