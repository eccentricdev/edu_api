using System;
using System.ComponentModel.DataAnnotations;
using SeventyOneDev.Utilities;
using SeventyOneDev.Utilities.Attributes;

namespace edu_api.Modules.Grade.Databases.Models
{
  public class grade_calendar:base_table
  {
    [Key]
    public Guid? grade_calendar_uid { get; set; }
    public Guid? academic_year_uid { get; set; }
    public Guid? academic_semester_uid { get; set; }
    public Guid? education_type_uid { get; set; }
    public bool? is_correct_calendar { get; set; }
    public DateTime? from_date { get; set; }
    public DateTime? to_date { get; set; }
  }
  [GeneratedUidController("api/grade/grade_calendar")]
  public class t_grade_calendar : grade_calendar
  {

  }

  public class v_grade_calendar : grade_calendar
  {
    public string academic_year_code { get; set; }
    public string academic_year_name_th { get; set; }
    public string academic_year_name_en { get; set; }
    public string semester_code { get; set; }
    public string semester_name_th { get; set; }
    public string semester_name_en { get; set; }
    public string education_type_code { get; set; }
    public string education_type_name_th { get; set; }
    public string education_type_name_en { get; set; }
  }
}
