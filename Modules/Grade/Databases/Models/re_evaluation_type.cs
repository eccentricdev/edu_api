using System;
using System.ComponentModel.DataAnnotations;
using SeventyOneDev.Utilities;
using SeventyOneDev.Utilities.Attributes;

namespace edu_api.Modules.Grade.Databases.Models
{
  public class re_evaluation_type:base_table
  {
    [Key]
    public Guid? re_evaluation_type_uid { get; set; }
    public string re_evaluation_type_code { get; set; }
    public string re_evaluation_type_name_th { get; set; }
    public string re_evaluation_type_name_en { get; set; }
    public int? row_order { get; set; }
  }
  [GeneratedUidController("api/grade/re_evaluation_type")]
  public class t_re_evaluation_type : re_evaluation_type
  {

  }

  public class v_re_evaluation_type : re_evaluation_type
  {

  }
}
