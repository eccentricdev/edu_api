using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using SeventyOneDev.Utilities;
using SeventyOneDev.Utilities.Attributes;

namespace edu_api.Modules.Grade.Databases.Models
{
  public class grade_calculation_setting:base_table
  {
    [Key]
    public Guid? grade_calculation_setting_uid { get; set; }
    public Guid? education_type_uid { get; set; }
    public DateTime? from_date { get; set; }
    public DateTime? to_date { get; set; }
    public Guid? grade_calculation_method_uid { get; set; }
    
    [Column(TypeName = "decimal(5,2)")]
    public decimal? max_re_grade_credit { get; set; }

  }
  // [GeneratedUidController("api/grade/grade_calculation_setting")]
  public class t_grade_calculation_setting : grade_calculation_setting
  {

  }

  public class v_grade_calculation_setting : grade_calculation_setting
  {
    public string grade_calculation_method_name_th { get; set; }
    public string education_type_name_th { get; set; }
  }
}
