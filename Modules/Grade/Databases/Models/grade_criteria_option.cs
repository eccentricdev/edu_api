using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;
using SeventyOneDev.Utilities;

namespace edu_api.Modules.Grade.Databases.Models
{
  public class grade_criteria_option:base_table
  {
    [Key]
    public Guid? grade_criteria_option_uid { get; set; }
    public Guid? grade_criteria_uid { get; set; }
    [Column(TypeName = "decimal(18,2)")]
    public decimal? min_score { get; set; } // <50
    [Column(TypeName = "decimal(18,2)")]
    public decimal? max_score { get; set; }
    public Guid? grade_uid { get; set; } //F
    public string description { get; set; }

  }

  public class t_grade_criteria_option : grade_criteria_option
  {
    [JsonIgnore]
    public t_grade_criteria grade_criteria { get; set; }
  }

  public class v_grade_criteria_option : grade_criteria_option
  {
    [JsonIgnore]
    public v_grade_criteria grade_criteria { get; set; }
    public string grade_code { get; set; }
    public string grade_name_th { get; set; }
    public string grade_name_en { get; set; }
    public int? display_order { get; set; }
    public bool? is_score { get; set; }

  }
}
