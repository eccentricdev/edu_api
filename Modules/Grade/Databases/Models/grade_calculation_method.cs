using System;
using System.ComponentModel.DataAnnotations;
using SeventyOneDev.Utilities;
using SeventyOneDev.Utilities.Attributes;

namespace edu_api.Modules.Grade.Databases.Models
{
  public class grade_calculation_method:base_table
  {
    [Key]
    public Guid? grade_calculation_method_uid { get; set; }
    public string grade_calculation_method_code { get; set; }
    public string grade_calculation_method_name_th { get; set; }
    public string grade_calculation_method_name_en { get; set; }
  }
  [GeneratedUidController("api/grade/grade_calculation_method")]
  public class t_grade_calculation_method : grade_calculation_method
  {

  }

  public class v_grade_calculation_method : grade_calculation_method
  {

  }
}
