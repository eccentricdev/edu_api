namespace edu_api.Modules.Discipline.Models
{
    public class student_discipline_statistic
    {
        public string faculty_name_th { get; set; }
        public string about { get; set; }
        public string detail { get; set; }
        public string quantity_student { get; set; }
        public string result { get; set; }
        public string quantity { get; set; }
        public string academic_year_name_th { get; set; }
    }
}