using System;

namespace edu_api.Modules.Discipline.Models
{
    public class student_illness_request
    {
        public string student_code { get; set; }
        public int? hospital_id { get; set; }
        public DateTime? start_date { get; set; }
        public DateTime? end_date { get; set; }
    }
}