using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using edu_api.Modules.Discipline.Databases.Models;
using Microsoft.EntityFrameworkCore;
using SeventyOneDev.Utilities;

namespace edu_api.Modules.Discipline.Services
{
    public class StudentIllnessPeportService
    {
        readonly Db _context;

        public StudentIllnessPeportService(Db context)
        {
            _context = context;
        }
        
        public async Task<List<v_student_illness_report>> ListStudentIllnessReport(student_illness_report_request request)
        {
            IQueryable <v_student_illness_report> studentIllnessReports = _context.v_student_illness_report.AsNoTracking();
            
            if (request?.college_faculty_uid != null)
            {
                studentIllnessReports = studentIllnessReports.Where(w => w.college_faculty_uid == request.college_faculty_uid);
            }

            if (request?.hospital_id != null)
            {
                studentIllnessReports = studentIllnessReports.Where(w => w.hospital_id == request.hospital_id);
            }

            if (request.start_date != null && request.end_date != null)
            {
                studentIllnessReports = studentIllnessReports.Where(w =>
                    w.illness_date >= request.start_date && w.illness_date <= request.end_date);
            }
            return await studentIllnessReports.OrderBy(w =>w.illness_date).ToListAsync();
        }

        public async Task<List<v_student_illness_report>> ListStudentIllnessDetailsReport(string student_code)
        {
            var results =  await _context.v_student_illness_report
                .Where(w => w.student_code == student_code)
                .ToListAsync();
            
            foreach (var result in results)
            {
                result.student_illness_photos = new List<v_student_illness_photo>();
                result.student_illness_files = new List<v_student_illness_file>();
                var photos = await _context.v_student_illness_photo
                    .Where(t => t.student_illness_uid == result.student_illness_uid)
                    .OrderBy(o => o.row_order).ToListAsync();

                var files = await _context.v_student_illness_file
                    .Where(t => t.student_illness_uid == result.student_illness_uid)
                    .OrderBy(o => o.row_order).ToListAsync();

                result.student_illness_photos = photos;
                result.student_illness_files = files;
            }

            return results;
        }
    }
}