using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using edu_api.Modules.Discipline.Databases.Models;
using Microsoft.EntityFrameworkCore;
using SeventyOneDev.Utilities;

namespace edu_api.Modules.Discipline.Services
{
    public class DisciplineYearService:EntityUidService<t_discipline_year,v_discipline_year>
    {
        private readonly Db _context;
        public DisciplineYearService(Db context) : base(context)
        {
            _context = context;
        }

        public override async Task<int> DeleteById(Guid uid, ClaimsIdentity claimsIdentity)
        {
            var disciplineCondition =
                await _context.v_discipline_condition.AsNoTracking().Where(w => w.discipline_year_uid == uid)
                    .ToListAsync();
            var disciplineScoreStatus = await _context.v_discipline_score_status.AsNoTracking()
                .Where(w => w.discipline_year_uid == uid).ToListAsync();
            var disciplineExamCondition =
                await _context.v_discipline_exam_condition.AsNoTracking().Where(w => w.discipline_year_uid == uid)
                    .ToListAsync();
            
            if(disciplineCondition.Count > 0 || disciplineScoreStatus.Count >0 || disciplineExamCondition.Count >0 ) throw new Exception("INUSE");
            return await base.DeleteById(uid, claimsIdentity);
        }
    }
}