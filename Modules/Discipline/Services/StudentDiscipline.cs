using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using edu_api.Modules.Discipline.Databases.Models;
using Microsoft.EntityFrameworkCore;
using SeventyOneDev.Utilities;

namespace edu_api.Modules.Discipline.Services
{
    public class StudentDisciplineService:EntityUidService<t_student_discipline,v_student_discipline>
    {
        private readonly Db _context;
        public StudentDisciplineService(Db context) : base(context)
        {
            _context = context;
        }

        public override async Task<v_student_discipline> GetEntity(Guid uid, List<Guid?> agencies = null, string userName = null)
        {
            var rs =  await _context.v_student_discipline.AsNoTracking()
                .Include(a => a.student_discipline_details)
                .Include(a => a.student_discipline_exams)
                .Include(a =>a.student_discipline_scores)
                .Include(a =>a.student_service_activities)
                .FirstOrDefaultAsync(a => a.student_discipline_uid == uid);

            if (rs != null)
            {
                foreach (var details in rs.student_discipline_details)
                {
                    details.student_discipline_detail_photos = new List<v_student_discipline_detail_photo>();
                    details.student_discipline_detail_files = new List<v_student_discipline_detail_file>();
                    var photos = await _context.v_student_discipline_detail_photo
                        .Where(t => t.student_discipline_detail_uid == details.student_discipline_detail_uid)
                        .OrderBy(o => o.row_order).ToListAsync();
                    
                    var files = await _context.v_student_discipline_detail_file
                        .Where(t => t.student_discipline_detail_uid == details.student_discipline_detail_uid)
                        .OrderBy(o => o.row_order).ToListAsync();

                    details.student_discipline_detail_photos = photos;
                    details.student_discipline_detail_files = files;
                }
            }

            return rs;
        }

        public override async Task<int> UpdateEntity(t_student_discipline entity, ClaimsIdentity claimsIdentity)
        {
            var removeDisciplineDetails = await _context.t_student_discipline_detail
                .Where(a => a.student_discipline_uid == entity.student_discipline_uid && entity
                    .student_discipline_details.Select(x => x.student_discipline_detail_uid)
                    .All(x => x != a.student_discipline_detail_uid)).ToListAsync();
            _context.t_student_discipline_detail.RemoveRange(removeDisciplineDetails);
            
            var removeDisciplineScores = await _context.t_student_discipline_score
                .Where(a => a.student_discipline_uid == entity.student_discipline_uid && entity
                    .student_discipline_scores.Select(x => x.student_discipline_score_uid)
                    .All(x => x != a.student_discipline_score_uid)).ToListAsync();
            _context.t_student_discipline_score.RemoveRange(removeDisciplineScores);
            
            var removeDisciplineExams = await _context.t_student_discipline_exam
                .Where(a => a.student_discipline_uid == entity.student_discipline_uid && entity
                    .student_discipline_exams.Select(x => x.student_discipline_exam_uid)
                    .All(x => x != a.student_discipline_exam_uid)).ToListAsync();
            _context.t_student_discipline_exam.RemoveRange(removeDisciplineExams);
            
            var removeServiceActivitys = await _context.t_student_service_activity
                .Where(a => a.student_discipline_uid == entity.student_discipline_uid && entity
                    .student_service_activities.Select(x => x.student_service_activity_uid)
                    .All(x => x != a.student_service_activity_uid)).ToListAsync();
            _context.t_student_service_activity.RemoveRange(removeServiceActivitys);

            foreach (var studentDisciplineDetail in entity.student_discipline_details)
            {
                if (studentDisciplineDetail.student_discipline_uid.HasValue)
                {
                    var removeStudentDisciplineDetailFiles = await _context.t_student_discipline_detail_file
                        .Where(a => a.student_discipline_detail_uid ==
                            studentDisciplineDetail.student_discipline_detail_uid && studentDisciplineDetail
                                .student_discipline_detail_files.Select(x => x.student_discipline_detail_file_uid)
                                .All(x => x != a.student_discipline_detail_file_uid)).ToListAsync();
                    _context.t_student_discipline_detail_file.RemoveRange(removeStudentDisciplineDetailFiles);

                    var removeStudentDisciplineDetailPhotos = await _context.t_student_discipline_detail_photo
                        .Where(a => a.student_discipline_detail_uid ==
                            studentDisciplineDetail.student_discipline_detail_uid && studentDisciplineDetail
                                .student_discipline_detail_photos.Select(x => x.student_discipline_detail_photo_uid)
                                .All(x => x != a.student_discipline_detail_photo_uid)).ToListAsync();
                    _context.t_student_discipline_detail_photo.RemoveRange(removeStudentDisciplineDetailPhotos);
                }
            }

            await _context.SaveChangesAsync();
            return await base.UpdateEntity(entity, claimsIdentity);

        }
    }
}