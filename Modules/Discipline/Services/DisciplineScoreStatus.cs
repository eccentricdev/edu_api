using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using edu_api.Modules.Discipline.Databases.Models;
using Microsoft.EntityFrameworkCore;
using SeventyOneDev.Utilities;

namespace edu_api.Modules.Discipline.Services
{
    public class DisciplineScoreStatusService:EntityUidService<t_discipline_score_status,v_discipline_score_status>
    {
        private readonly Db _context;
        public DisciplineScoreStatusService(Db context) : base(context)
        {
            _context = context;
        }
        
        public async Task<List<t_discipline_score_status>> Copy(Guid discipline_year_uid, Guid new_discipline_year_uid,
            ClaimsIdentity claimsIdentity)
        {
            var currentDisciplineScoreStatus = await _context.t_discipline_score_status.AsNoTracking()
                .Where(a => a.discipline_year_uid == discipline_year_uid).ToListAsync();

            foreach (var disciplineScoreStatus in currentDisciplineScoreStatus)
            {
                disciplineScoreStatus.discipline_score_status_uid = null;
                disciplineScoreStatus.discipline_year_uid = new_discipline_year_uid;
            }

            return await base.NewEntity(currentDisciplineScoreStatus, claimsIdentity);
        }
        
        public override async Task<int> DeleteById(Guid uid, ClaimsIdentity claimsIdentity)
        {
            var studentDisciplines =
                await _context.v_student_discipline.AsNoTracking().Where(w => w.discipline_score_status_uid == uid)
                    .ToListAsync();
            var studentDisciplineScores = await _context.v_student_discipline_score.AsNoTracking()
                .Where(w => w.discipline_score_status_uid == uid).ToListAsync();
            if(studentDisciplines.Count > 0 || studentDisciplineScores.Count() > 0) throw new Exception("INUSE");
            return await base.DeleteById(uid, claimsIdentity);
        }
    }
}