using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using edu_api.Modules.Discipline.Databases.Models;
using edu_api.Modules.Discipline.Models;
using Microsoft.EntityFrameworkCore;
using SeventyOneDev.Utilities;

namespace edu_api.Modules.Discipline.Services
{
  public class StudentDisciplineReportService
  {
    readonly Db _context;

    public StudentDisciplineReportService(Db context)
    {
      _context = context;
    }

    public async Task<List<v_student_discipline_report>> ListStudentDisciplineReport(student_discipline_report_request request)
    {
      IQueryable <v_student_discipline_report> studentDisciplineReports = _context.v_student_discipline_report.AsNoTracking();
            
      if (request?.college_faculty_uid != null)
      {
        studentDisciplineReports = studentDisciplineReports.Where(w => w.college_faculty_uid == request.college_faculty_uid);
      }
      if (request?.discipline_year_uid != null)
      {
        studentDisciplineReports = studentDisciplineReports.Where(w => w.discipline_condition_year_uid == request.discipline_year_uid && w.discipline_score_status_year_uid == request.discipline_year_uid);
      }

      if (request?.discipline_condition_uid != null)
      {
        studentDisciplineReports = studentDisciplineReports.Where(w => w.discipline_condition_uid == request.discipline_condition_uid);
      }

      if (request.start_date != null && request.end_date != null)
      {
        studentDisciplineReports = studentDisciplineReports.Where(w =>
          w.action_date >= request.start_date && w.action_date <= request.end_date);
      }

      if (request?.discipline_punishment_uid != null)
      {
        studentDisciplineReports = studentDisciplineReports.Where(w => w.discipline_punishment_uid == request.discipline_punishment_uid);
      }
      
      if (request?.discipline_score_status_uid != null)
      {
        studentDisciplineReports = studentDisciplineReports.Where(w => w.discipline_score_status_uid == request.discipline_score_status_uid);
      }

      return await studentDisciplineReports.OrderBy(w =>w.action_date).ToListAsync();
    }

    public async Task<List<v_student_discipline_detail_report>> ListStudentDisciplineReport(string student_code)
    {
      var results = await _context.v_student_discipline_detail_report
        .Where(w => w.student_code == student_code).ToListAsync();

      foreach (var result in results)
      {
        result.student_discipline_detail_photos = new List<v_student_discipline_detail_photo>();
        result.student_discipline_detail_files = new List<v_student_discipline_detail_file>();
        var photos = await _context.v_student_discipline_detail_photo
          .Where(t => t.student_discipline_detail_uid == result.student_discipline_detail_uid)
          .OrderBy(o => o.row_order).ToListAsync();

        var files = await _context.v_student_discipline_detail_file
          .Where(t => t.student_discipline_detail_uid == result.student_discipline_detail_uid)
          .OrderBy(o => o.row_order).ToListAsync();

        result.student_discipline_detail_photos = photos;
        result.student_discipline_detail_files = files;
      }

      return results;
    }
    
    
    public async Task<List<student_discipline_statistic>> ListStudentDisciplineStatisticReport(student_discipline_report_request request)
    {
      IQueryable <v_student_discipline_report> studentDisciplineReports = _context.v_student_discipline_report.AsNoTracking();
            
      if (request?.college_faculty_uid != null)
      {
        studentDisciplineReports = studentDisciplineReports.Where(w => w.college_faculty_uid == request.college_faculty_uid);
      }
      
      if (request?.discipline_year_uid != null)
      {
        studentDisciplineReports = studentDisciplineReports.Where(w => w.discipline_condition_year_uid == request.discipline_year_uid && w.discipline_score_status_year_uid == request.discipline_year_uid);
      }
    
      if (request?.discipline_condition_uid != null)
      {
        studentDisciplineReports = studentDisciplineReports.Where(w => w.discipline_condition_uid == request.discipline_condition_uid);
      }
    
      if (request.start_date != null && request.end_date != null)
      {
        studentDisciplineReports = studentDisciplineReports.Where(w =>
          w.action_date >= request.start_date && w.action_date <= request.end_date);
      }
    
      if (request?.discipline_punishment_uid != null)
      {
        studentDisciplineReports = studentDisciplineReports.Where(w => w.discipline_punishment_uid == request.discipline_punishment_uid);
      }
    
      if (request?.discipline_score_status_uid != null)
      {
        studentDisciplineReports = studentDisciplineReports.Where(w => w.discipline_score_status_uid == request.discipline_score_status_uid);
      }
    
      var result = await studentDisciplineReports.OrderBy(w =>w.action_date).ToListAsync();
      
      var rsGroupBy = result
        .GroupBy(g => new { g.college_faculty_uid, g.discipline_condition_uid ,
          g.detail_section_name_th,g.discipline_punishment_uid,g.amount,g.faculty_name_th,g.section_name_th,
          g.discipline_punishment_name_th,g.academic_year_name_th
        })
        .Select(g => new student_discipline_statistic
        {
          faculty_name_th = g.First().faculty_name_th,
          about = g.First().section_name_th,
          detail = g.First().detail_section_name_th,
          quantity_student = g.Count().ToString(),
          result = g.First().discipline_punishment_name_th,
          quantity = g.Key.amount.ToString(),
          academic_year_name_th = g.Key.academic_year_name_th,
        })
        .OrderBy(g => g.faculty_name_th);
      
      var resultList  = new List<student_discipline_statistic>();
      foreach (var rs in rsGroupBy)
      {
        resultList.Add(new student_discipline_statistic()
        {
          faculty_name_th = rs.faculty_name_th,
          about = rs.about,
          detail =rs.detail,
          quantity_student = rs.quantity_student,
          result = rs.result,
          quantity = rs.quantity,
        });
      }
    
      return resultList;
    }
  }
}
