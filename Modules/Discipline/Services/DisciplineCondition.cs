using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using edu_api.Modules.Discipline.Databases.Models;
using Microsoft.EntityFrameworkCore;
using SeventyOneDev.Utilities;

namespace edu_api.Modules.Discipline.Services
{
    public class DisciplineConditionService:EntityUidService<t_discipline_condition,v_discipline_condition>
    {
        private readonly Db _context;
        public DisciplineConditionService(Db context) : base(context)
        {
            _context = context;
        }

        public override async Task<List<v_discipline_condition>> ListEntityId(v_discipline_condition entity, List<Guid?> agencies = null, string userName = null)
        {
            var result = await base.ListEntityId(entity, agencies, userName);
            return result.OrderBy(w => w.created_datetime).ToList();
        }

        public override async Task<v_discipline_condition> GetEntity(Guid uid, List<Guid?> agencies = null, string userName = null)
        {
            //return base.GetEntity(uid, agencies, userName);
            return await _context.v_discipline_condition.AsNoTracking()
                .Include(a => a.discipline_condition_details.OrderBy(r =>r.row_order)).
                ThenInclude(a =>a.discipline_condition_detail_subs.OrderBy(r => r.row_order))
                .Include(a => a.student_discipline_details)
                .FirstOrDefaultAsync(a => a.discipline_condition_uid == uid);
        }

        public override async Task<t_discipline_condition> NewEntity(t_discipline_condition entity, ClaimsIdentity claimsIdentity)
        {
            var disciplineCondition = await _context.t_discipline_condition.Where(w =>
                w.discipline_year_uid == entity.discipline_year_uid && w.section_code.ToUpper() == entity.section_code.ToUpper()).ToListAsync();
            
            if(disciplineCondition.Count > 0) throw new Exception("NEW_DUPLICATE");
            return await base.NewEntity(entity, claimsIdentity);
        }

        public override async Task<int> UpdateEntity(t_discipline_condition entity, ClaimsIdentity claimsIdentity)
        {
            var removeDisciplineDetails = await _context.t_discipline_condition_detail
                .Where(a => a.discipline_condition_uid == entity.discipline_condition_uid && entity
                    .discipline_condition_details.Select(x => x.discipline_condition_detail_uid)
                    .All(x => x != a.discipline_condition_detail_uid)).ToListAsync();

            foreach (var disciplineDetail in removeDisciplineDetails)
            {
                var studentDisciplineDetails =
                    await _context.v_student_discipline_detail.AsNoTracking().Where(w => w.discipline_condition_detail_uid == disciplineDetail.discipline_condition_detail_uid)
                        .ToListAsync();
                if(studentDisciplineDetails.Count > 0) throw new Exception("INUSE");
            }
            
            _context.t_discipline_condition_detail.RemoveRange(removeDisciplineDetails);

            foreach (var disciplineConditionDetail in entity.discipline_condition_details)
            {
                if (disciplineConditionDetail.discipline_condition_uid.HasValue)
                {
                    var removeDisciplineConditionDetailSubs = await _context.t_discipline_condition_detail_sub
                        .Where(a => a.discipline_condition_detail_uid ==
                            disciplineConditionDetail.discipline_condition_detail_uid && disciplineConditionDetail
                                .discipline_condition_detail_subs.Select(x => x.discipline_condition_detail_sub_uid)
                                .All(x => x != a.discipline_condition_detail_sub_uid)).ToListAsync();
                    
                    foreach (var disciplineConditionDetailSubs in removeDisciplineConditionDetailSubs)
                    {
                        var studentDisciplineDetails =
                            await _context.v_student_discipline_detail.AsNoTracking().Where(w => w.discipline_condition_detail_sub_uid == disciplineConditionDetailSubs.discipline_condition_detail_sub_uid)
                                .ToListAsync();
                        if(studentDisciplineDetails.Count > 0) throw new Exception("INUSE");
                    }
                    
                    _context.t_discipline_condition_detail_sub.RemoveRange(removeDisciplineConditionDetailSubs);
                }
            }
            
            var disciplineCondition = await _context.t_discipline_condition.Where(w =>
                w.discipline_year_uid == entity.discipline_year_uid && w.section_code.ToUpper() == entity.section_code.ToUpper() && w.discipline_condition_uid != entity.discipline_condition_uid).ToListAsync();
            if(disciplineCondition.Count > 0) throw new Exception("UPDATE_DUPLICATE");

            await _context.SaveChangesAsync(claimsIdentity);
            return await base.UpdateEntity(entity, claimsIdentity);
            
        }

        public async Task<List<t_discipline_condition>> Copy(Guid discipline_year_uid, Guid new_discipline_year_uid,
            ClaimsIdentity claimsIdentity)
        {
            var currentDisciplineConditions = await _context.t_discipline_condition.AsNoTracking()
                .Include(a => a.discipline_condition_details).
                ThenInclude(a =>a.discipline_condition_detail_subs)
                .Where(a => a.discipline_year_uid == discipline_year_uid).ToListAsync();

            foreach (var disciplineCondition in currentDisciplineConditions)
            {
                disciplineCondition.discipline_condition_uid = null;
                disciplineCondition.discipline_year_uid = new_discipline_year_uid;
            
                foreach (var disciplineConditionDetail in disciplineCondition.discipline_condition_details)
                {
                    disciplineConditionDetail.discipline_condition_detail_uid = null;
                    disciplineConditionDetail.discipline_condition_uid = disciplineCondition.discipline_condition_uid;
            
                    foreach (var disciplineConditionDetailSub in disciplineConditionDetail.discipline_condition_detail_subs)
                    {
                        disciplineConditionDetailSub.discipline_condition_detail_sub_uid = null;
                        disciplineConditionDetailSub.discipline_condition_detail_uid =
                            disciplineConditionDetail.discipline_condition_detail_uid;
                        
                    }
                }
            }
            
            foreach (var entity in currentDisciplineConditions)
            {
                var agency = Helper.GetDataFilter(claimsIdentity, new List<string>() {"agency_uid"});
                if (agency.Any())
                {
                    entity.owner_agency_uid = agency.FirstOrDefault();
                }
                entity.status_id ??= 1;
            }
            
            await _context.t_discipline_condition.AddRangeAsync(currentDisciplineConditions);
            await _context.SaveChangesAsync(claimsIdentity);
            return currentDisciplineConditions;
            
            // return await base.NewEntity(currentDisciplineConditions, claimsIdentity);
        }

        public override async Task<int> DeleteById(Guid uid, ClaimsIdentity claimsIdentity)
        {
            var studentDisciplineDetails =
                await _context.v_student_discipline_detail.AsNoTracking().Where(w => w.discipline_condition_uid == uid)
                    .ToListAsync();
            
            if(studentDisciplineDetails.Count > 0) throw new Exception("INUSE");
            return await base.DeleteById(uid, claimsIdentity);
        }
    }
}
