using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using edu_api.Modules.Discipline.Databases.Models;
using edu_api.Modules.Discipline.Models;
using Microsoft.EntityFrameworkCore;
using SeventyOneDev.Utilities;

namespace edu_api.Modules.Discipline.Services
{
    public class StudentIllnessService:EntityUidService<t_student_illness,v_student_illness>
    {
        private readonly Db _context;
        public StudentIllnessService(Db context) : base(context)
        {
            _context = context;
        }

        public async Task<List<v_student_illness>> ListStudentIllnessByDate(student_illness_request request)
        {
            var entities = await _context.v_student_illness.AsNoTracking().ToListAsync();
            if (request?.hospital_id != null)
            {
                entities = entities.Where(w => w.hospital_id == request.hospital_id).ToList();
            }

            if (request?.student_code != null)
            {
                entities = entities.Where(w => w.student_code.Contains(request.student_code)).ToList();
            }

            if (request?.start_date != null && request.end_date != null)
            {
                entities = entities.Where(w => w.illness_date >= request.start_date && w.illness_date <= request.end_date).ToList();
            }
            return entities.OrderByDescending(w =>w.illness_date).ToList();
        }

        public async Task<List<v_student_illness>> ListStudentIllnessByStudentCode(string student_code)
        {
            var results =  await _context.v_student_illness
                .Where(w => w.student_code == student_code)
                .ToListAsync();
            
            foreach (var result in results)
            {
                result.student_illness_photos = new List<v_student_illness_photo>();
                result.student_illness_files = new List<v_student_illness_file>();
                var photos = await _context.v_student_illness_photo
                    .Where(t => t.student_illness_uid == result.student_illness_uid)
                    .OrderBy(o => o.row_order).ToListAsync();

                var files = await _context.v_student_illness_file
                    .Where(t => t.student_illness_uid == result.student_illness_uid)
                    .OrderBy(o => o.row_order).ToListAsync();


                result.student_illness_photos = photos;
                result.student_illness_files = files;
            }

            return results;
        }

    }
}