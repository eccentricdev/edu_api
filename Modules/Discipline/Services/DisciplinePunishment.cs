using System;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using edu_api.Modules.Discipline.Databases.Models;
using Microsoft.EntityFrameworkCore;
using SeventyOneDev.Utilities;

namespace edu_api.Modules.Discipline.Services
{
    public class DisciplinePunishmentService:EntityUidService<t_discipline_punishment,v_discipline_punishment>
    {
        private readonly Db _context;
        public DisciplinePunishmentService(Db context) : base(context)
        {
            _context = context;
        }

        public override async Task<t_discipline_punishment> NewEntity(t_discipline_punishment entity, ClaimsIdentity claimsIdentity)
        {
            var disciplinePunishments = await _context.t_discipline_punishment.Where(w =>
                w.discipline_punishment_name_th.ToUpper() == entity.discipline_punishment_name_th.ToUpper()).ToListAsync();
            
            if(disciplinePunishments.Count > 0) throw new Exception("NEW_DUPLICATE");
            
            return await base.NewEntity(entity, claimsIdentity);
        }

        public override async Task<int> UpdateEntity(t_discipline_punishment entity, ClaimsIdentity claimsIdentity)
        {
            var disciplinePunishments = await _context.t_discipline_punishment.Where(w =>
                w.discipline_punishment_name_th.ToUpper() == entity.discipline_punishment_name_th.ToUpper()
                && w.discipline_punishment_uid != entity.discipline_punishment_uid).ToListAsync();
            
            if(disciplinePunishments.Count > 0) throw new Exception("UPDATE_DUPLICATE");
            return await base.UpdateEntity(entity, claimsIdentity);
        }

        public override async Task<int> DeleteById(Guid uid, ClaimsIdentity claimsIdentity)
        {
            var studentDisciplineDetails =
                await _context.v_student_discipline_detail.AsNoTracking().Where(w => w.discipline_punishment_uid == uid)
                    .ToListAsync();
            var studentDisciplineExams = await _context.v_student_discipline_exam.AsNoTracking()
                .Where(w => w.discipline_punishment_uid == uid).ToListAsync();
            
            if(studentDisciplineDetails.Count > 0 || studentDisciplineExams.Count >0 ) throw new Exception("INUSE");
            return await base.DeleteById(uid, claimsIdentity);
        }
    }
}