using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using edu_api.Modules.Discipline.Databases.Models;
using Microsoft.EntityFrameworkCore;
using SeventyOneDev.Utilities;

namespace edu_api.Modules.Discipline.Services
{
    public class StudentDisciplineDetailService:EntityUidService<t_student_discipline_detail,v_student_discipline_detail>
    {
        private readonly Db _context;
        public StudentDisciplineDetailService(Db context) : base(context)
        {
            _context = context;
        }
        
        public override async Task<v_student_discipline_detail> GetEntity(Guid uid, List<Guid?> agencies = null, string userName = null)
        {
            var rs =  await _context.v_student_discipline_detail.AsNoTracking()
                .FirstOrDefaultAsync(a => a.student_discipline_uid == uid);

            if (rs != null)
            {
                rs.student_discipline_detail_photos = new List<v_student_discipline_detail_photo>();
                rs.student_discipline_detail_files = new List<v_student_discipline_detail_file>();
                var photos = await _context.v_student_discipline_detail_photo
                    .Where(t => t.student_discipline_detail_uid == rs.student_discipline_detail_uid)
                    .OrderBy(o => o.row_order).ToListAsync();

                var files = await _context.v_student_discipline_detail_file
                    .Where(t => t.student_discipline_detail_uid == rs.student_discipline_detail_uid)
                    .OrderBy(o => o.row_order).ToListAsync();

                rs.student_discipline_detail_photos = photos;
                rs.student_discipline_detail_files = files;
            }

            return rs;
        }
    }
}