using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using edu_api.Modules.Discipline.Databases.Models;
using Microsoft.EntityFrameworkCore;
using SeventyOneDev.Utilities;

namespace edu_api.Modules.Discipline.Services
{
    public class DisciplineExamConditionService:EntityUidService<t_discipline_exam_condition,v_discipline_exam_condition>
    {
        private readonly Db _context;
        public DisciplineExamConditionService(Db context) : base(context)
        {
            _context = context;
        }

        public override async Task<List<v_discipline_exam_condition>> ListEntityId(v_discipline_exam_condition entity, List<Guid?> agencies = null, string userName = null)
        {
            var result = await base.ListEntityId(entity, agencies, userName);
            return result.OrderBy(w => w.created_datetime).ToList();
        }

        public override async Task<v_discipline_exam_condition> GetEntity(Guid uid, List<Guid?> agencies = null, string userName = null)
        {
            //return base.GetEntity(uid, agencies, userName);
            return await _context.v_discipline_exam_condition.AsNoTracking()
                .FirstOrDefaultAsync(a => a.discipline_exam_condition_uid == uid);
        }

        public override async Task<t_discipline_exam_condition> NewEntity(t_discipline_exam_condition entity, ClaimsIdentity claimsIdentity)
        {
            var disciplineCondition = await _context.t_discipline_exam_condition.Where(w =>
                w.discipline_year_uid == entity.discipline_year_uid && w.section_name_th.ToUpper() == entity.section_name_th.ToUpper()).ToListAsync();
            
            if(disciplineCondition.Count > 0) throw new Exception("NEW_DUPLICATE");
            return await base.NewEntity(entity, claimsIdentity);
        }

        public override async Task<int> UpdateEntity(t_discipline_exam_condition entity, ClaimsIdentity claimsIdentity)
        {
            var disciplineCondition = await _context.t_discipline_exam_condition.Where(w =>
                w.discipline_year_uid == entity.discipline_year_uid && w.section_name_th.ToUpper() == entity.section_name_th.ToUpper() && w.discipline_exam_condition_uid != entity.discipline_exam_condition_uid).ToListAsync();
            if(disciplineCondition.Count > 0) throw new Exception("UPDATE_DUPLICATE");

            await _context.SaveChangesAsync(claimsIdentity);
            return await base.UpdateEntity(entity, claimsIdentity);
            
        }

        public async Task<List<t_discipline_exam_condition>> Copy(Guid discipline_year_uid, Guid new_discipline_year_uid,
            ClaimsIdentity claimsIdentity)
        {
            var currentDisciplineExamConditions = await _context.t_discipline_exam_condition.AsNoTracking()
                .Where(a => a.discipline_year_uid == discipline_year_uid).ToListAsync();

            foreach (var disciplineCondition in currentDisciplineExamConditions)
            {
                disciplineCondition.discipline_exam_condition_uid = null;
                disciplineCondition.discipline_year_uid = new_discipline_year_uid;
            }
            
            foreach (var entity in currentDisciplineExamConditions)
            {
                var agency = Helper.GetDataFilter(claimsIdentity, new List<string>() {"agency_uid"});
                if (agency.Any())
                {
                    entity.owner_agency_uid = agency.FirstOrDefault();
                }
                entity.status_id ??= 1;
            }
            
            await _context.t_discipline_exam_condition.AddRangeAsync(currentDisciplineExamConditions);
            await _context.SaveChangesAsync(claimsIdentity);
            return currentDisciplineExamConditions;
            
            // return await base.NewEntity(currentDisciplineExamConditions, claimsIdentity);
        }

        public override async Task<int> DeleteById(Guid uid, ClaimsIdentity claimsIdentity)
        {
            var studentDisciplineDetails =
                await _context.v_student_discipline_exam.AsNoTracking().Where(w => w.discipline_exam_condition_uid == uid)
                    .ToListAsync();
            
            if(studentDisciplineDetails.Count > 0) throw new Exception("INUSE");
            return await base.DeleteById(uid, claimsIdentity);
        }
    }
}
