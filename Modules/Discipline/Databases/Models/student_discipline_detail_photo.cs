using System;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using SeventyOneDev.Utilities;
using SeventyOneDev.Utilities.Attributes;

namespace edu_api.Modules.Discipline.Databases.Models
{
    public class student_discipline_detail_photo : base_table
    {
        [Key]
        public Guid? student_discipline_detail_photo_uid { get; set; }
        public Guid? student_discipline_detail_uid { get; set; }
        public int? row_order { get; set; }
        public string photo_name { get; set; }
        [MaxLength(500)]
        public string photo_path { get; set; }
        public string action_by { get; set; }
    }
    
    [GeneratedUidController("api/discipline/student_discipline_detail_photo")]
    public class t_student_discipline_detail_photo :student_discipline_detail_photo
    {
        [JsonIgnore]
        public t_student_discipline_detail student_discipline_detail { get; set; }
    }
    
    public class v_student_discipline_detail_photo :student_discipline_detail_photo
    {
        [JsonIgnore]
        public v_student_discipline_detail student_discipline_detail { get; set; }
        
        public string create_by_display_name { get; set; }
    }
}