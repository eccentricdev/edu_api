using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;
using SeventyOneDev.Utilities;

namespace edu_api.Modules.Discipline.Databases.Models
{
  public class student_discipline_score:base_table
  {
    [Key]
    public Guid? student_discipline_score_uid { get; set; }
    public Guid? student_discipline_uid { get; set; }
    public Guid? discipline_score_status_uid { get; set; }
    public bool? is_approve { get; set; }
    public DateTime? discipline_score_status_date { get; set; }
    public string action_by { get; set; }
  }

  public class t_student_discipline_score : student_discipline_score
  {
    [JsonIgnore]
    public t_student_discipline student_discipline { get; set; }
  }

  public class v_student_discipline_score : student_discipline_score
  {
    [JsonIgnore]
    public v_student_discipline student_discipline { get; set; }

    public string discipline_display { get; set; }
    
    public string create_by_display_name { get; set; }
  }
}
