using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;
using SeventyOneDev.Utilities;
using SeventyOneDev.Utilities.Attributes;

namespace edu_api.Modules.Discipline.Databases.Models
{
  public class student_discipline_detail:base_table
  {
    [Key]
    public Guid? student_discipline_detail_uid { get; set; }
    public Guid? student_discipline_uid { get; set; }
    public Guid? discipline_condition_uid { get; set; }
    public Guid? discipline_condition_detail_uid { get; set; }

    public Guid? discipline_condition_detail_sub_uid { get; set; }
    public short? deduct_score { get; set; }
    public DateTime? action_date { get; set; }
    public TimeSpan? action_time { get; set; }
    public Guid? discipline_punishment_uid { get; set; }
    public string remark { get; set; }
    public string action_by { get; set; }
    public Guid? case_owner_teacher_uid { get; set; }
  }

  // [GeneratedUidController("api/discipline/student_discipline_detail")]
  public class t_student_discipline_detail : student_discipline_detail
  {
    [Include]
    public List<t_student_discipline_detail_file> student_discipline_detail_files { get; set; }
    [Include]
    public List<t_student_discipline_detail_photo> student_discipline_detail_photos { get; set; }
    
    [JsonIgnore]
    public t_student_discipline student_discipline { get; set; }
    [JsonIgnore]
    public t_discipline_condition discipline_condition { get; set; }
    [JsonIgnore]
    public t_discipline_condition_detail discipline_condition_detail { get; set; }

    [JsonIgnore]
    public t_discipline_condition_detail_sub discipline_condition_detail_sub { get; set; }
    [JsonIgnore]
    public t_discipline_punishment discipline_punishment { get; set; }
  }

  public class v_student_discipline_detail : student_discipline_detail
  {
    [Include]
    public List<v_student_discipline_detail_file> student_discipline_detail_files { get; set; }
    [Include]
    public List<v_student_discipline_detail_photo> student_discipline_detail_photos { get; set; }
    [JsonIgnore]
    public v_student_discipline student_discipline { get; set; }
    [JsonIgnore]
    public v_discipline_condition discipline_condition { get; set; }
    [JsonIgnore]
    public v_discipline_condition_detail discipline_condition_detail { get; set; }
    
    [JsonIgnore]
    public v_discipline_condition_detail_sub discipline_condition_detail_sub { get; set; }
    [JsonIgnore]
    public v_discipline_punishment discipline_punishment { get; set; }
    public string section_code { get; set; }
    public string section_name_th { get; set; }
    public string detail_section_code { get; set; }
    public string detail_section_name_th { get; set; }
    public string detail_sub_section_code { get; set; }
    public string detail_sub_section_name_th { get; set; }
    public string discipline_punishment_name_th { get; set; }
    public string discipline_punishment_code { get; set; }
    
    public string create_by_display_name { get; set; }
    public string case_owner_teacher_name { get; set; }
  }
}
