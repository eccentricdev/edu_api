using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace edu_api.Modules.Discipline.Databases.Models
{
  public class v_student_discipline_report
  {
    public string student_code {get;set;}
    public string display_name_th {get;set;}
    public string gender_name_th {get;set;}
    public Guid? college_faculty_uid { get; set; }
    public string faculty_name_th {get;set;}
    public Guid? faculty_curriculum_uid { get; set; }
   
    public string major_name_th {get;set;}
    public string section_name_th {get;set;}
    public string detail_section_name_th {get;set;}
    public string discipline_punishment_name_th {get;set;}
    public short? deduct_score { get; set; }
    public DateTime? action_date {get;set;}
    public string action_dates {get;set;}
    public string remark {get;set;}
    public string academic_year_name_th {get;set;}
    public string discipline_status_name_th { get; set; }
    public string discipline_period_type_name_th { get; set; }
    public short? amount { get; set; }
    public Guid? discipline_condition_uid { get; set; }
    public Guid? discipline_punishment_uid { get; set; }
    public Guid? discipline_score_status_uid { get; set; }
    public  Guid? discipline_condition_year_uid { get; set; }
    public string discipline_condition_name_th { get; set; }
    public  Guid? discipline_score_status_year_uid { get; set; }
    public string discipline_score_status_name_th { get; set; }
    [NotMapped] public string search { get; set; }
  }

  public class student_discipline_report_request
  {
    public string type { get; set; }
    public Guid? college_faculty_uid { get; set; }
    // public Guid? faculty_curriculum_uid { get; set; }
    public DateTime? start_date { get; set; }
    public DateTime? end_date { get; set; }
    public Guid? discipline_condition_uid { get; set; }
    public Guid? discipline_year_uid { get; set; }
    public Guid? discipline_punishment_uid { get; set; }
    public Guid? discipline_score_status_uid { get; set; }
  }
}
