using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;
using rsu_common_api.models;
using SeventyOneDev.Utilities;
using SeventyOneDev.Utilities.Attributes;

namespace edu_api.Modules.Discipline.Databases.Models
{
  public class student_discipline:base_table
  {
    [Key]
    public Guid? student_discipline_uid { get; set; }
    public Guid? student_uid { get; set; }
    public string student_code { get; set; }
    public short? deduct_score { get; set; }
    public short? remain_score { get; set; }
    public Guid? discipline_score_status_uid { get; set; }
    public string action_by { get; set; }
  }

  // [GeneratedUidController("api/discipline/student_discipline")]
  public class t_student_discipline : student_discipline
  {
    [Include] 
    public List<t_student_discipline_detail> student_discipline_details { get; set; }
    [Include]  
    public List<t_student_discipline_exam> student_discipline_exams { get; set; }
    [Include] 
    public List<t_student_discipline_score> student_discipline_scores { get; set; }
    [Include] 
    public List<t_student_service_activity> student_service_activities { get; set; }
    // [JsonIgnore]
    // public t_discipline_score_status discipline_score_status { get; set; }

  }

  public class v_student_discipline : student_discipline
  {
    [Include] 
    public List<v_student_discipline_detail> student_discipline_details { get; set; }
    [Include] 
    public List<v_student_discipline_exam> student_discipline_exams { get; set; }
    [Include] 
    public List<v_student_discipline_score> student_discipline_scores { get; set; }
    [Include] 
    public List<v_student_service_activity> student_service_activities { get; set; }
    // [JsonIgnore]
    // public v_discipline_score_status discipline_score_status { get; set; }

    public string display_name_th { get; set; }
    public string display_name_en { get; set; }
    public string faculty_curriculum_name_th { get; set; }
    public string faculty_curriculum_name_en { get; set; }
    public string college_faculty_name_th { get; set; }
    public string college_faculty_name_en { get; set; }
    public string major_name_th { get; set; }
    public string major_name_en { get; set; }
    public string education_type_name_th { get; set; }
    public string education_type_name_en { get; set; }
    public string student_status_name_th { get; set; }
    public string discipline_display { get; set; }
    
    public string create_by_display_name { get; set; }

  }
}
