using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using SeventyOneDev.Utilities;
using SeventyOneDev.Utilities.Attributes;

namespace edu_api.Modules.Discipline.Databases.Models
{
    public class student_illness :base_table
    {
        [Key]
        public Guid? student_illness_uid { get; set; }
        // public Guid? student_uid { get; set; }
        public string student_code { get; set; }
        public string mobile_no { get; set; }
        public string illness_details { get; set; }
        public DateTime? illness_date { get; set; }
        public int? hospital_id { get; set; }
        public string teacher_duty_name { get; set; }
        public string recorder_name { get; set; }
    }
    public class t_student_illness :student_illness
    {
        [Include]
        public List<t_student_illness_file> student_illness_files { get; set; }
        [Include]
        public List<t_student_illness_photo> student_illness_photos { get; set; }
    }
    
    public class v_student_illness :student_illness
    {
        public string display_name_th { get; set; }
        public string faculty_curriculum_name_th { get; set; }
        public string college_faculty_name_th { get; set; }
        public string hospital_name_th { get; set; }
        public Guid? student_status_uid { get; set; }
        public string student_status_name_th { get; set; }
        
        [Include]
        public List<v_student_illness_file> student_illness_files { get; set; }
        [Include]
        public List<v_student_illness_photo> student_illness_photos { get; set; }
    }
}