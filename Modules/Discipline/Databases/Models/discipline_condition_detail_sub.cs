using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;
using SeventyOneDev.Utilities;

namespace edu_api.Modules.Discipline.Databases.Models
{
  public class discipline_condition_detail_sub:base_table
  {
    [Key,DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public Guid? discipline_condition_detail_sub_uid { get; set; }
    public Guid? discipline_condition_detail_uid { get; set; }
    public string detail_sub_section_code { get; set; }
    public string detail_sub_section_name_th { get; set; }
    public string detail_sub_section_name_en { get; set; }
    public int? row_order { get; set; }
  }

  public class t_discipline_condition_detail_sub : discipline_condition_detail_sub
  {
    [JsonIgnore]
    public t_discipline_condition_detail discipline_condition_detail { get; set; }
    public List<t_student_discipline_detail> student_discipline_details { get; set; }
  }

  public class v_discipline_condition_detail_sub : discipline_condition_detail_sub
  {
    [JsonIgnore]
    public v_discipline_condition_detail discipline_condition_detail { get; set; }
    public List<v_student_discipline_detail> student_discipline_details { get; set; }
    public string status_name_en { get; set; }
    public string status_name_th { get; set; }
  }
}
