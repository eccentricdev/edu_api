using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;
using SeventyOneDev.Utilities;
using SeventyOneDev.Utilities.Attributes;

namespace edu_api.Modules.Discipline.Databases.Models
{
  public class discipline_punishment:base_table
  {
    [Key]
    public Guid? discipline_punishment_uid { get; set; }
    public string discipline_punishment_code { get; set; }
    // [Unique]
    public string discipline_punishment_name_th { get; set; }
    public string discipline_punishment_name_en { get; set; }
  }
  
  // [GeneratedUidController("api/discipline/discipline_punishment")]
  public class t_discipline_punishment : discipline_punishment
  {
    [JsonIgnore]
    public List<t_student_discipline_detail> student_discipline_details { get; set; }
  }

  public class v_discipline_punishment : discipline_punishment
  {
    [JsonIgnore]
    public List<v_student_discipline_detail> student_discipline_details { get; set; }

    public string status_name_en { get; set; }
    public string status_name_th { get; set; }
  }

}
