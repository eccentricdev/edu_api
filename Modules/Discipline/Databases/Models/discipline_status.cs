using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using SeventyOneDev.Utilities;
using SeventyOneDev.Utilities.Attributes;

namespace edu_api.Modules.Discipline.Databases.Models
{
  public class discipline_status:base_table
  {
    [Key]
    public Guid? discipline_status_uid { get; set; }
    public string discipline_status_code { get; set; }
    public string discipline_status_name_th { get; set; }
    public string discipline_status_name_en { get; set; }
  }

  [GeneratedUidController("api/discipline/discipline_status")]
  public class t_discipline_status : discipline_status
  {
    // public t_discipline_status()
    // {
    // }
    //
    // public t_discipline_status(Guid? disciplineStatusId, string disciplineStatusCode, string disciplineStatusNameEn,
    //   string disciplineStatusNameTh)
    // {
    //   this.discipline_status_uid = disciplineStatusId;
    //   this.discipline_status_code = disciplineStatusCode;
    //   this.discipline_status_name_en = disciplineStatusNameEn;
    //   this.discipline_status_name_th = disciplineStatusNameTh;
    //   this.created_by = "SYSTEM";
    //   this.updated_by = "SYSTEM";
    //   this.created_datetime = DateTime.Now;
    //   this.updated_datetime = DateTime.Now;
    // }
    public List<t_student_discipline_detail> student_discipline_details { get; set; }
  }

  public class v_discipline_status : discipline_status
  {
    
  }

}
