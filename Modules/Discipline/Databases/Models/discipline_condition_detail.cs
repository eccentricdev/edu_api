using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;
using SeventyOneDev.Utilities;
using SeventyOneDev.Utilities.Attributes;

namespace edu_api.Modules.Discipline.Databases.Models
{
  public class discipline_condition_detail:base_table
  {
    [Key]
    public Guid? discipline_condition_detail_uid { get; set; }
    public Guid? discipline_condition_uid { get; set; }
    public string detail_section_code { get; set; }
    public string detail_section_name_th { get; set; }
    public string detail_section_name_en { get; set; }
    public int? row_order { get; set; }
  }

  [GeneratedUidController("api/discipline/discipline_condition_detail")]
  public class t_discipline_condition_detail : discipline_condition_detail
  {
    [JsonIgnore]
    public t_discipline_condition discipline_condition { get; set; }
    public List<t_student_discipline_detail> student_discipline_details { get; set; }
    public List<t_discipline_condition_detail_sub> discipline_condition_detail_subs { get; set; }
  }

  public class v_discipline_condition_detail : discipline_condition_detail
  {
    [JsonIgnore]
    public v_discipline_condition discipline_condition { get; set; }
    public List<v_student_discipline_detail> student_discipline_details { get; set; }
    public List<v_discipline_condition_detail_sub> discipline_condition_detail_subs { get; set; }
    public string status_name_en { get; set; }
    public string status_name_th { get; set; }
  }

}
