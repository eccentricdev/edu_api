using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;
using SeventyOneDev.Utilities;
using SeventyOneDev.Utilities.Attributes;

namespace edu_api.Modules.Discipline.Databases.Models
{
  public class student_service_activity:base_table
  {
    [Key]
    public Guid? student_service_activity_uid { get; set; }
    public Guid? student_discipline_uid { get; set; }
    public string activity_name { get; set; }
    public DateTime? activity_date { get; set; }
    public TimeSpan? activity_time { get; set; }
    [Column(TypeName="decimal(18,4)")]
    public decimal? score { get; set; }
    public string remark { get; set; }
    public string action_by { get; set; }
    public Guid? case_owner_teacher_uid { get; set; }
    
  }

  [GeneratedUidController("api/discipline/student_service_activity")]
  public class t_student_service_activity : student_service_activity
  {
    [JsonIgnore]
    public t_student_discipline student_discipline { get; set; }
  }

  public class v_student_service_activity : student_service_activity
  {
    [JsonIgnore]
    public v_student_discipline student_discipline { get; set; }
    public string case_owner_teacher_name { get; set; }
  }
}
