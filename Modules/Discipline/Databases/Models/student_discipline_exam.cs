using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;
using SeventyOneDev.Utilities;

namespace edu_api.Modules.Discipline.Databases.Models
{
  public class student_discipline_exam:base_table
  {
    [Key]
    public Guid? student_discipline_exam_uid { get; set; }
    public Guid? student_discipline_uid { get; set; }
    public Guid? discipline_exam_condition_uid { get; set; }

    public short? deduct_score { get; set; }
    public DateTime? action_date { get; set; }
    public TimeSpan? action_time { get; set; }
    public Guid? discipline_punishment_uid { get; set; }
    public string remark { get; set; }
    public string action_by { get; set; }
    public string description { get; set; }
     public Guid? case_owner_teacher_uid { get; set; }
  }

  public class t_student_discipline_exam : student_discipline_exam
  {
    [JsonIgnore]
    public t_student_discipline student_discipline { get; set; }
    [JsonIgnore]
    public t_discipline_punishment discipline_punishment { get; set; }
    
    [JsonIgnore]
    public t_discipline_exam_condition discipline_exam_condition { get; set; }
    // [JsonIgnore]
    // public t_discipline_exam_condition_detail discipline_exam_condition_detail { get; set; }
    // [JsonIgnore]
    // public t_discipline_exam_condition_detail_sub discipline_exam_condition_detail_sub { get; set; }
    //
  }

  public class v_student_discipline_exam : student_discipline_exam
  {
    [JsonIgnore]
    public v_student_discipline student_discipline { get; set; }
    [JsonIgnore]
    public v_discipline_punishment discipline_punishment { get; set; }
    [JsonIgnore]
    public v_discipline_exam_condition discipline_exam_condition { get; set; }
    // [JsonIgnore]
    // public v_discipline_exam_condition_detail discipline_exam_condition_detail { get; set; }
    //
    // [JsonIgnore]
    // public v_discipline_exam_condition_detail_sub discipline_exam_condition_detail_sub { get; set; }
    
    public string discipline_punishment_name_th { get; set; }
    public string discipline_punishment_code { get; set; }
    
    public string create_by_display_name { get; set; }
    
    public string section_code { get; set; }
    public string section_name_th { get; set; }
    public string case_owner_teacher_name { get; set; }
  }
}
