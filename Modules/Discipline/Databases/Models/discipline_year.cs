using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using SeventyOneDev.Utilities;
using SeventyOneDev.Utilities.Attributes;

namespace edu_api.Modules.Discipline.Databases.Models
{
  public class discipline_year:base_table
  {
    [Key]
    public Guid? discipline_year_uid { get; set; }
    public Guid? academic_year_uid { get; set; }
    public string discipline_year_code { get; set; }
    [Unique]
    public string discipline_year_name_th { get; set; }
    public string discipline_year_name_en { get; set; }
    public short? exam_status_id { get; set; }
  }

  // [GeneratedUidController("api/discipline/discipline_year")]
  public class t_discipline_year : discipline_year
  {
    [Include]
    public List<t_discipline_condition> discipline_conditions { get; set; }
    [Include]
    public List<t_discipline_score_status> discipline_score_statuses { get; set; }
    [Include]
    public List<t_discipline_exam_condition> discipline_exam_conditions { get; set; }
  }

  public class v_discipline_year : discipline_year
  {
    [Include]
    public List<v_discipline_condition> discipline_conditions { get; set; }
    [Include]
    public List<v_discipline_score_status> discipline_score_statuses { get; set; }
    [Include]
    public List<v_discipline_exam_condition> discipline_exam_conditions { get; set; }
  }
}
