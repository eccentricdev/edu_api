using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace edu_api.Modules.Discipline.Databases.Models
{
    public class v_student_illness_report
    {
        public string student_code {get;set;}
        public string display_name_th {get;set;}
        public Guid? faculty_curriculum_uid { get; set; }
        public string faculty_curriculum_name_th {get;set;}
        public Guid? college_faculty_uid { get; set; }
        public string college_faculty_name_th {get;set;}
        public string major_name_th {get;set;}
        public Guid? student_illness_uid {get;set;}
        public int? hospital_id { get; set; }
        public string hospital_name_th {get;set;}
        public string mobile_no {get;set;}
        public string illness_details {get;set;}
        public string teacher_duty_name {get;set;}
        public string recorder_name {get;set;}
        public DateTime? illness_date { get; set; }
        public string illness_date1 { get; set; }
        
        [NotMapped]
        public string search { get; set; }
        [NotMapped]
        public List<v_student_illness_file> student_illness_files { get; set; }
        [NotMapped]
        public List<v_student_illness_photo> student_illness_photos { get; set; }

    }

    public class student_illness_report_request
    {
        public string type { get; set; }
        public Guid? college_faculty_uid { get; set; }
        public DateTime? start_date { get; set; }
        public DateTime? end_date { get; set; }
        public int? hospital_id { get; set; }
    }
}