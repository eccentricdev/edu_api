using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;
using SeventyOneDev.Utilities;
using SeventyOneDev.Utilities.Attributes;

namespace edu_api.Modules.Discipline.Databases.Models
{
  public class discipline_condition:base_table
  {
    [Key]
    public Guid? discipline_condition_uid { get; set; }
    [Search]
    public string section_code { get; set; }
    [Search]
    public string section_name_th { get; set; }
    [Search]
    public string section_name_en { get; set; }
    public int? row_order { get; set; }
    public Guid? discipline_year_uid { get; set; }
  }
  
  // [GeneratedUidController("api/discipline/discipline_condition")]
  public class t_discipline_condition : discipline_condition
  {
    [JsonIgnore]
    public t_discipline_year discipline_year { get; set; }
    [Include] 
    public List<t_discipline_condition_detail> discipline_condition_details { get; set; }
    [Include] 
    public List<t_student_discipline_detail> student_discipline_details { get; set; }
  }

  public class v_discipline_condition : discipline_condition
  {
    [JsonIgnore]
    public v_discipline_year discipline_year { get; set; }
    [Include]  
    public List<v_discipline_condition_detail> discipline_condition_details { get; set; }
    [Include] 
    public List<v_student_discipline_detail> student_discipline_details { get; set; }
    public string status_name_en { get; set; }
    public string status_name_th { get; set; }
  }
}
