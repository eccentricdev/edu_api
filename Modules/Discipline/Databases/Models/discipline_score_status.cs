using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;
using SeventyOneDev.Utilities;
using SeventyOneDev.Utilities.Attributes;

namespace edu_api.Modules.Discipline.Databases.Models
{
  public class discipline_score_status:base_table
  {
    [Key]
    public Guid? discipline_score_status_uid { get; set; }
    public short? min_score { get; set; }
    public short? max_score { get; set; }
    public Guid? discipline_status_uid { get; set; }
    public short? amount { get; set; }
    public Guid? discipline_period_type_uid { get; set; }
    public Guid? discipline_year_uid { get; set; }

  }

  // [GeneratedUidController("api/discipline/discipline_score_status")]
  public class t_discipline_score_status : discipline_score_status
  {
    // [JsonIgnore]
    // public List<t_student_discipline> student_disciplines { get; set; }
    public t_discipline_period_type discipline_period_type { get; set; }
    [JsonIgnore]
    public t_discipline_year discipline_year { get; set; }
  }

  public class v_discipline_score_status : discipline_score_status
  {
    // [JsonIgnore]
    // public List<v_student_discipline> student_disciplines { get; set; }
    public v_discipline_period_type discipline_period_type { get; set; }
    [JsonIgnore]
    public v_discipline_year discipline_year { get; set; }
    [NotMapped]

    public string status_name_en { get; set; }
    public string status_name_th { get; set; }
    public string discipline_status_name_en { get; set; }
    public string discipline_status_name_th { get; set; }
    public string discipline_period_type_name_en { get; set; }
    public string discipline_period_type_name_th { get; set; }
    public string discipline_display { get; set; }
  }
}
