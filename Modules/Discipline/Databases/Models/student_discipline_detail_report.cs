using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace edu_api.Modules.Discipline.Databases.Models
{
    public class v_student_discipline_detail_report
    {
        public string student_code { get; set; }
        public string display_name_th { get; set; }
        public string faculty_curriculum_name_th { get; set; }
        public DateTime? action_date { get; set; }
        public string action_dates { get; set; }
        public string section_name_th { get; set; }
        public string detail_section_name_th { get; set; }
        public string detail_sub_section_name_th { get; set; }
        public string discipline_punishment_name_th { get; set; }
        public string discipline_year_name_th { get; set; }
        public int? amount {get;set;}
        public Guid? student_discipline_detail_uid { get; set; }
        public Guid? discipline_condition_uid { get; set; }
        public Guid? discipline_condition_detail_uid { get; set; }
        public Guid? discipline_condition_detail_sub_uid { get; set; }
        public Guid? discipline_punishment_uid { get; set; }
        public Guid? discipline_score_status_uid { get; set; }
        public Guid? discipline_year_uid { get; set; }
        
        [NotMapped]
        public string search { get; set; }
        [NotMapped]
        public List<v_student_discipline_detail_file> student_discipline_detail_files { get; set; }
        [NotMapped]
        public List<v_student_discipline_detail_photo> student_discipline_detail_photos { get; set; }
    }
}