using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;
using SeventyOneDev.Utilities;
using SeventyOneDev.Utilities.Attributes;

namespace edu_api.Modules.Discipline.Databases.Models
{
  public class discipline_period_type:base_table
  {
    [Key]
    public Guid? discipline_period_type_uid { get; set; }
    public string discipline_period_type_code { get; set; }
    public string discipline_period_type_name_th { get; set; }
    public string discipline_period_type_name_en { get; set; }
  }
  
  [GeneratedUidController("api/discipline/discipline_period_type")]
  public class t_discipline_period_type : discipline_period_type
  {
    [JsonIgnore]
    public List<t_discipline_score_status> student_score_statuses { get; set; }
  }

  public class v_discipline_period_type : discipline_period_type
  {
    [JsonIgnore]
    public List<v_discipline_score_status> student_score_statuses { get; set; }
  }
}
