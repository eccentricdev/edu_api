using System;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using SeventyOneDev.Utilities;
using SeventyOneDev.Utilities.Attributes;

namespace edu_api.Modules.Discipline.Databases.Models
{
    public class student_illness_photo : base_table
    {
        [Key]
        public Guid? student_illness_photo_uid { get; set; }
        public Guid? student_illness_uid { get; set; }
        public int? row_order { get; set; }
        public string photo_name { get; set; }
        [MaxLength(500)]
        public string photo_path { get; set; }
    }
    
    [GeneratedUidController("api/discipline/student_illness_photo")]
    public class t_student_illness_photo :student_illness_photo
    {
        [JsonIgnore]
        public t_student_illness student_illness { get; set; }
    }
    
    public class v_student_illness_photo :student_illness_photo
    {
        [JsonIgnore]
        public v_student_illness student_illness { get; set; }
    }
}