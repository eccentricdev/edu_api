using edu_api.Modules.Discipline.Databases.Models;
using Microsoft.EntityFrameworkCore;

namespace  SeventyOneDev.Utilities
{
    public partial class Db : DbContext
    {
        public DbSet<t_discipline_year> t_discipline_year { get; set; }
        public DbSet<v_discipline_year> v_discipline_year { get; set; }
        public DbSet<t_discipline_condition> t_discipline_condition { get; set; }
        public DbSet<v_discipline_condition> v_discipline_condition { get; set; }
        public DbSet<t_discipline_condition_detail> t_discipline_condition_detail { get; set; }
        public DbSet<v_discipline_condition_detail> v_discipline_condition_detail { get; set; }
        public DbSet<t_discipline_condition_detail_sub> t_discipline_condition_detail_sub { get; set; }
        public DbSet<v_discipline_condition_detail_sub> v_discipline_condition_detail_sub { get; set; }
        public DbSet<t_discipline_period_type> t_discipline_period_type { get; set; }
        public DbSet<v_discipline_period_type> v_discipline_period_type { get; set; }
        public DbSet<t_discipline_punishment> t_discipline_punishment { get; set; }
        public DbSet<v_discipline_punishment> v_discipline_punishment { get; set; }
        public DbSet<t_discipline_score_status> t_discipline_score_status { get; set; }
        public DbSet<v_discipline_score_status> v_discipline_score_status { get; set; }
        public DbSet<t_student_discipline> t_student_discipline { get; set; }
        public DbSet<v_student_discipline> v_student_discipline { get; set; }
        public DbSet<t_student_discipline_detail> t_student_discipline_detail { get; set; }
        public DbSet<v_student_discipline_detail> v_student_discipline_detail { get; set; }
        public DbSet<t_student_discipline_exam> t_student_discipline_exam { get; set; }
        public DbSet<v_student_discipline_exam> v_student_discipline_exam { get; set; }
        public DbSet<t_student_discipline_score> t_student_discipline_score { get; set; }
        public DbSet<v_student_discipline_score> v_student_discipline_score { get; set; }
        public DbSet<t_student_service_activity> t_student_service_activity { get; set; }
        public DbSet<v_student_service_activity> v_student_service_activity { get; set; }
        public DbSet<t_discipline_status> t_discipline_status { get; set; }
        public DbSet<v_discipline_status> v_discipline_status { get; set; } 
        public DbSet<t_student_illness> t_student_illness { get; set; }
        public DbSet<v_student_illness> v_student_illness { get; set; }
        public DbSet<v_student_discipline_report> v_student_discipline_report { get; set; }
        public DbSet<v_student_illness_report> v_student_illness_report { get; set; }
        
        public DbSet<t_student_illness_file> t_student_illness_file { get; set; }
        public DbSet<v_student_illness_file> v_student_illness_file { get; set; }
        
        public DbSet<t_student_illness_photo> t_student_illness_photo { get; set; }
        public DbSet<v_student_illness_photo> v_student_illness_photo { get; set; }
        
        public DbSet<t_student_discipline_detail_file> t_student_discipline_detail_file { get; set; }
        public DbSet<v_student_discipline_detail_file> v_student_discipline_detail_file { get; set; }
        
        public DbSet<t_student_discipline_detail_photo> t_student_discipline_detail_photo { get; set; }
        public DbSet<v_student_discipline_detail_photo> v_student_discipline_detail_photo { get; set; }
        public DbSet<v_student_discipline_detail_report> v_student_discipline_detail_report { get; set; }
        
        public DbSet<t_discipline_exam_condition> t_discipline_exam_condition { get; set; }
        public DbSet<v_discipline_exam_condition> v_discipline_exam_condition { get; set; }
        // public DbSet<t_discipline_exam_condition_detail> t_discipline_exam_condition_detail { get; set; }
        // public DbSet<v_discipline_exam_condition_detail> v_discipline_exam_condition_detail { get; set; }
        // public DbSet<t_discipline_exam_condition_detail_sub> t_discipline_exam_condition_detail_sub { get; set; }
        // public DbSet<v_discipline_exam_condition_detail_sub> v_discipline_exam_condition_detail_sub { get; set; }

        private void OnDisciplineModelCreating(ModelBuilder builder, string schema)
        {
          builder.Entity<t_discipline_year>().ToTable("discipline_year", schema);
          builder.Entity<v_discipline_year>().ToView("v_discipline_year", schema);

          builder.Entity<t_discipline_condition>().ToTable("discipline_condition", schema);
          builder.Entity<t_discipline_condition>().HasOne(d => d.discipline_year).WithMany(d => d.discipline_conditions)
            .HasForeignKey(d => d.discipline_year_uid).OnDelete(DeleteBehavior.Cascade);
          builder.Entity<v_discipline_condition>().ToView("v_discipline_condition", schema);
          builder.Entity<v_discipline_condition>().HasOne(d => d.discipline_year).WithMany(d => d.discipline_conditions)
            .HasForeignKey(d => d.discipline_year_uid).OnDelete(DeleteBehavior.Cascade);
          
          builder.Entity<t_discipline_condition_detail>().ToTable("discipline_condition_detail",schema);
          builder.Entity<t_discipline_condition_detail>().HasOne(d => d.discipline_condition)
            .WithMany(d => d.discipline_condition_details).HasForeignKey(d => d.discipline_condition_uid)
            .OnDelete(DeleteBehavior.Cascade);
          builder.Entity<v_discipline_condition_detail>().ToView("v_discipline_condition_detail", schema);
          builder.Entity<v_discipline_condition_detail>().HasOne(d => d.discipline_condition)
            .WithMany(d => d.discipline_condition_details).HasForeignKey(d => d.discipline_condition_uid);

          builder.Entity<t_discipline_condition_detail_sub>().ToTable("discipline_condition_detail_sub", schema);
          builder.Entity<t_discipline_condition_detail_sub>().HasOne(d => d.discipline_condition_detail)
            .WithMany(d => d.discipline_condition_detail_subs).HasForeignKey(d => d.discipline_condition_detail_uid)
            .OnDelete(DeleteBehavior.Cascade);
          builder.Entity<v_discipline_condition_detail_sub>().ToView("v_discipline_condition_detail_sub", schema);
          builder.Entity<v_discipline_condition_detail_sub>().HasOne(d => d.discipline_condition_detail)
            .WithMany(d => d.discipline_condition_detail_subs).HasForeignKey(d => d.discipline_condition_detail_uid);

          builder.Entity<t_discipline_period_type>().ToTable("discipline_period_type", schema);
          builder.Entity<v_discipline_period_type>().ToView("v_discipline_period_type", schema);

          builder.Entity<t_discipline_punishment>().ToTable("discipline_punishment", schema);
          builder.Entity<v_discipline_punishment>().ToView("v_discipline_punishment", schema);

          builder.Entity<t_discipline_score_status>().ToTable("discipline_score_status",schema);
          // builder.Entity<t_discipline_score_status>().HasMany(d => d.student_disciplines)
          //   .WithOne(d => d.discipline_score_status).HasForeignKey(d => d.discipline_score_status_uid)
          //   .OnDelete(DeleteBehavior.Restrict);
          builder.Entity<t_discipline_score_status>().HasOne(d => d.discipline_year)
            .WithMany(d => d.discipline_score_statuses).HasForeignKey(d => d.discipline_year_uid)
            .OnDelete(DeleteBehavior.Cascade);
          builder.Entity<t_discipline_score_status>().HasOne(d => d.discipline_period_type)
            .WithMany(d => d.student_score_statuses).HasForeignKey(d => d.discipline_period_type_uid)
            .OnDelete(DeleteBehavior.Restrict);

          builder.Entity<v_discipline_score_status>().ToView("v_discipline_score_status", schema);
          // builder.Entity<v_discipline_score_status>().HasMany(d => d.student_disciplines)
          //   .WithOne(d => d.discipline_score_status).HasForeignKey(d => d.discipline_score_status_uid)
          //   .OnDelete(DeleteBehavior.Restrict);
          builder.Entity<v_discipline_score_status>().HasOne(d => d.discipline_year)
            .WithMany(d => d.discipline_score_statuses).HasForeignKey(d => d.discipline_year_uid)
            .OnDelete(DeleteBehavior.Cascade);
          builder.Entity<v_discipline_score_status>().HasOne(d => d.discipline_period_type)
            .WithMany(d => d.student_score_statuses).HasForeignKey(d => d.discipline_period_type_uid)
            .OnDelete(DeleteBehavior.Restrict);

          builder.Entity<t_student_discipline>().ToTable("student_discipline", schema);
          
          // builder.Entity<t_student_discipline>().HasOne(d =>d.student)
          //   .WithMany(s =>s.student_disciplines).HasForeignKey(s =>s.student_uid)
          //   .OnDelete(DeleteBehavior.Cascade);

          builder.Entity<v_student_discipline>().ToView("v_student_discipline",schema);
          
          // builder.Entity<v_student_discipline>().HasOne(d =>d.student)
          //   .WithMany(s =>s.student_disciplines).HasForeignKey(s =>s.student_uid)
          //   .OnDelete(DeleteBehavior.Cascade);

          builder.Entity<t_student_discipline_detail>().ToTable("student_discipline_detail", schema);
          builder.Entity<t_student_discipline_detail>().HasOne(s => s.student_discipline)
            .WithMany(s => s.student_discipline_details).HasForeignKey(s => s.student_discipline_uid)
            .OnDelete(DeleteBehavior.Cascade);
          builder.Entity<t_student_discipline_detail>().HasOne(s => s.discipline_condition)
            .WithMany(s => s.student_discipline_details).HasForeignKey(s => s.discipline_condition_uid)
            .OnDelete(DeleteBehavior.Restrict);
          builder.Entity<t_student_discipline_detail>().HasOne(s => s.discipline_condition_detail)
            .WithMany(s => s.student_discipline_details).HasForeignKey(s => s.discipline_condition_detail_uid)
            .OnDelete(DeleteBehavior.Restrict);
          builder.Entity<t_student_discipline_detail>().HasOne(s => s.discipline_condition_detail_sub)
            .WithMany(s => s.student_discipline_details).HasForeignKey(s => s.discipline_condition_detail_sub_uid)
            .OnDelete(DeleteBehavior.Restrict);
          builder.Entity<t_student_discipline_detail>().HasOne(s => s.discipline_punishment)
            .WithMany(s => s.student_discipline_details).HasForeignKey(s => s.discipline_punishment_uid)
            .OnDelete(DeleteBehavior.Restrict);
          builder.Entity<v_student_discipline_detail>().ToView("v_student_discipline_detail", schema);
          builder.Entity<v_student_discipline_detail>().HasOne(s => s.student_discipline)
            .WithMany(s => s.student_discipline_details).HasForeignKey(d => d.student_discipline_uid)
            .OnDelete(DeleteBehavior.Cascade);
          builder.Entity<v_student_discipline_detail>().HasOne(s => s.discipline_condition)
            .WithMany(s => s.student_discipline_details).HasForeignKey(s => s.discipline_condition_uid)
            .OnDelete(DeleteBehavior.Restrict);
          builder.Entity<v_student_discipline_detail>().HasOne(s => s.discipline_condition_detail)
            .WithMany(s => s.student_discipline_details).HasForeignKey(s => s.discipline_condition_detail_uid)
            .OnDelete(DeleteBehavior.Restrict);
          builder.Entity<v_student_discipline_detail>().HasOne(s => s.discipline_condition_detail_sub)
            .WithMany(s => s.student_discipline_details).HasForeignKey(s => s.discipline_condition_detail_sub_uid)
            .OnDelete(DeleteBehavior.Restrict);
          builder.Entity<v_student_discipline_detail>().HasOne(s => s.discipline_punishment)
            .WithMany(s => s.student_discipline_details).HasForeignKey(s => s.discipline_punishment_uid)
            .OnDelete(DeleteBehavior.Restrict);

          builder.Entity<t_student_discipline_exam>().ToTable("student_discipline_exam", schema);
          builder.Entity<t_student_discipline_exam>().HasOne(s => s.student_discipline)
            .WithMany(s => s.student_discipline_exams).HasForeignKey(d => d.student_discipline_uid)
            .OnDelete(DeleteBehavior.Cascade);
          builder.Entity<t_student_discipline_exam>().HasOne(s => s.discipline_exam_condition)
            .WithMany(s => s.student_discipline_exams).HasForeignKey(s => s.discipline_exam_condition_uid)
            .OnDelete(DeleteBehavior.Restrict);
          // builder.Entity<t_student_discipline_exam>().HasOne(s => s.discipline_exam_condition_detail)
          //   .WithMany(s => s.student_discipline_exams).HasForeignKey(s => s.discipline_exam_condition_detail_uid)
          //   .OnDelete(DeleteBehavior.Restrict);
          // builder.Entity<t_student_discipline_exam>().HasOne(s => s.discipline_exam_condition_detail_sub)
          //   .WithMany(s => s.student_discipline_exams).HasForeignKey(s => s.discipline_exam_condition_detail_sub_uid)
          //   .OnDelete(DeleteBehavior.Restrict);

          builder.Entity<v_student_discipline_exam>().ToView("v_student_discipline_exam", schema);
          builder.Entity<v_student_discipline_exam>().HasOne(s => s.student_discipline)
            .WithMany(s => s.student_discipline_exams).HasForeignKey(d => d.student_discipline_uid)
            .OnDelete(DeleteBehavior.Cascade);
          builder.Entity<v_student_discipline_exam>().HasOne(s => s.discipline_exam_condition)
            .WithMany(s => s.student_discipline_exams).HasForeignKey(s => s.discipline_exam_condition_uid)
            .OnDelete(DeleteBehavior.Restrict);
          // builder.Entity<v_student_discipline_exam>().HasOne(s => s.discipline_exam_condition_detail)
          //   .WithMany(s => s.student_discipline_exams).HasForeignKey(s => s.discipline_exam_condition_detail_uid)
          //   .OnDelete(DeleteBehavior.Restrict);
          // builder.Entity<v_student_discipline_exam>().HasOne(s => s.discipline_exam_condition_detail_sub)
          //   .WithMany(s => s.student_discipline_exams).HasForeignKey(s => s.discipline_exam_condition_detail_sub_uid)
          //   .OnDelete(DeleteBehavior.Restrict);

          builder.Entity<t_student_discipline_score>().ToTable("student_discipline_score", schema);
          builder.Entity<t_student_discipline_score>().HasOne(s => s.student_discipline)
            .WithMany(s => s.student_discipline_scores).HasForeignKey(d => d.student_discipline_uid)
            .OnDelete(DeleteBehavior.Cascade);
          builder.Entity<v_student_discipline_score>().ToView("v_student_discipline_score", schema);
          builder.Entity<v_student_discipline_score>().HasOne(s => s.student_discipline)
            .WithMany(s => s.student_discipline_scores).HasForeignKey(d => d.student_discipline_uid)
            .OnDelete(DeleteBehavior.Cascade);
          builder.Entity<t_student_service_activity>().ToTable("student_service_activity", schema);
          builder.Entity<t_student_service_activity>().HasOne(s => s.student_discipline)
            .WithMany(s => s.student_service_activities).HasForeignKey(s => s.student_discipline_uid)
            .OnDelete(DeleteBehavior.Cascade);
          builder.Entity<v_student_service_activity>().ToView("v_student_service_activity", schema);
          builder.Entity<v_student_service_activity>().HasOne(s => s.student_discipline)
            .WithMany(s => s.student_service_activities).HasForeignKey(s => s.student_discipline_uid)
            .OnDelete(DeleteBehavior.Cascade);
          builder.Entity<t_discipline_status>().ToTable("discipline_status", schema);
          builder.Entity<v_discipline_status>().ToView("v_discipline_status", schema);

          builder.Entity<v_student_discipline_report>().ToView("v_student_discipline_report",schema).HasNoKey();
          builder.Entity<v_student_illness_report>().ToView("v_student_illness_report",schema).HasNoKey();
          builder.Entity<v_student_discipline_detail_report>().ToView("v_student_discipline_detail_report",schema).HasNoKey();
          
          builder.Entity<t_student_illness>().ToTable("student_illness", schema);
          builder.Entity<v_student_illness>().ToView("v_student_illness", schema);

          builder.Entity<t_student_illness_file>().ToTable("student_illness_file",schema);
          builder.Entity<t_student_illness_file>().HasOne(d => d.student_illness)
            .WithMany(d => d.student_illness_files).HasForeignKey(d => d.student_illness_uid)
            .OnDelete(DeleteBehavior.Cascade);
          builder.Entity<v_student_illness_file>().ToView("v_student_illness_file", schema);
          builder.Entity<v_student_illness_file>().HasOne(d => d.student_illness)
            .WithMany(d => d.student_illness_files).HasForeignKey(d => d.student_illness_uid)
            .OnDelete(DeleteBehavior.Cascade);
          
          builder.Entity<t_student_illness_photo>().ToTable("student_illness_photo",schema);
          builder.Entity<t_student_illness_photo>().HasOne(d => d.student_illness)
            .WithMany(d => d.student_illness_photos).HasForeignKey(d => d.student_illness_uid)
            .OnDelete(DeleteBehavior.Cascade);
          builder.Entity<v_student_illness_photo>().ToView("v_student_illness_photo", schema);
          builder.Entity<v_student_illness_photo>().HasOne(d => d.student_illness)
            .WithMany(d => d.student_illness_photos).HasForeignKey(d => d.student_illness_uid)
            .OnDelete(DeleteBehavior.Cascade);
          
          builder.Entity<t_student_discipline_detail_file>().ToTable("student_discipline_detail_file",schema);
          builder.Entity<t_student_discipline_detail_file>().HasOne(d => d.student_discipline_detail)
            .WithMany(d => d.student_discipline_detail_files).HasForeignKey(d => d.student_discipline_detail_uid)
            .OnDelete(DeleteBehavior.Cascade);
          builder.Entity<v_student_discipline_detail_file>().ToView("v_student_discipline_detail_file", schema);
          builder.Entity<v_student_discipline_detail_file>().HasOne(d => d.student_discipline_detail)
            .WithMany(d => d.student_discipline_detail_files).HasForeignKey(d => d.student_discipline_detail_uid)
            .OnDelete(DeleteBehavior.Cascade);
          
          builder.Entity<t_student_discipline_detail_photo>().ToTable("student_discipline_detail_photo",schema);
          builder.Entity<t_student_discipline_detail_photo>().HasOne(d => d.student_discipline_detail)
            .WithMany(d => d.student_discipline_detail_photos).HasForeignKey(d => d.student_discipline_detail_uid)
            .OnDelete(DeleteBehavior.Cascade);
          builder.Entity<v_student_discipline_detail_photo>().ToView("v_student_discipline_detail_photo", schema);
          builder.Entity<v_student_discipline_detail_photo>().HasOne(d => d.student_discipline_detail)
            .WithMany(d => d.student_discipline_detail_photos).HasForeignKey(d => d.student_discipline_detail_uid)
            .OnDelete(DeleteBehavior.Cascade);
          
          builder.Entity<t_discipline_exam_condition>().ToTable("discipline_exam_condition", schema);
          builder.Entity<t_discipline_exam_condition>().HasOne(d => d.discipline_year).WithMany(d => d.discipline_exam_conditions)
            .HasForeignKey(d => d.discipline_year_uid).OnDelete(DeleteBehavior.Cascade);
          builder.Entity<v_discipline_exam_condition>().ToView("v_discipline_exam_condition", schema);
          builder.Entity<v_discipline_exam_condition>().HasOne(d => d.discipline_year).WithMany(d => d.discipline_exam_conditions)
            .HasForeignKey(d => d.discipline_year_uid).OnDelete(DeleteBehavior.Cascade);
          
          // builder.Entity<t_discipline_exam_condition_detail>().ToTable("discipline_exam_condition_detail",schema);
          // builder.Entity<t_discipline_exam_condition_detail>().HasOne(d => d.discipline_exam_condition)
          //   .WithMany(d => d.discipline_exam_condition_details).HasForeignKey(d => d.discipline_exam_condition_uid)
          //   .OnDelete(DeleteBehavior.Cascade);
          // builder.Entity<v_discipline_exam_condition_detail>().ToView("v_discipline_exam_condition_detail", schema);
          // builder.Entity<v_discipline_exam_condition_detail>().HasOne(d => d.discipline_exam_condition)
          //   .WithMany(d => d.discipline_exam_condition_details).HasForeignKey(d => d.discipline_exam_condition_uid);
          //
          // builder.Entity<t_discipline_exam_condition_detail_sub>().ToTable("discipline_exam_condition_detail_sub", schema);
          // builder.Entity<t_discipline_exam_condition_detail_sub>().HasOne(d => d.discipline_exam_condition_detail)
          //   .WithMany(d => d.discipline_exam_condition_detail_subs).HasForeignKey(d => d.discipline_exam_condition_detail_uid)
          //   .OnDelete(DeleteBehavior.Cascade);
          // builder.Entity<v_discipline_exam_condition_detail_sub>().ToView("v_discipline_exam_condition_detail_sub", schema);
          // builder.Entity<v_discipline_exam_condition_detail_sub>().HasOne(d => d.discipline_exam_condition_detail)
          //   .WithMany(d => d.discipline_exam_condition_detail_subs).HasForeignKey(d => d.discipline_exam_condition_detail_uid);

        }
    }
}