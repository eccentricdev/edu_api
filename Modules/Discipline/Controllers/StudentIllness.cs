using System.Collections.Generic;
using System.Threading.Tasks;
using edu_api.Modules.Discipline.Databases.Models;
using edu_api.Modules.Discipline.Models;
using edu_api.Modules.Discipline.Services;
using Microsoft.AspNetCore.Mvc;
using SeventyOneDev.Utilities;

namespace edu_api.Modules.Discipline.Controllers
{
    [Route("api/discipline/student_illness", Name = "student_illness")]
    public class StudentIllnessController:BaseUidController<t_student_illness,v_student_illness>
    {
        private readonly StudentIllnessService _entityUidService;
        public StudentIllnessController(StudentIllnessService entityUidService) : base(entityUidService)
        {
            _entityUidService = entityUidService;
        }
        
        [HttpGet("get_by_date")]
        public async Task<ActionResult<List<v_student_illness>>> StudentIllnessByDate([FromQuery] student_illness_request request)
        {
            var studentLogResult= await _entityUidService.ListStudentIllnessByDate(request);
            return Ok(studentLogResult);
        }
        
        [HttpGet("{student_code}")]
        public async Task<ActionResult<List<v_student_illness>>> StudentIllnessByStudentCode([FromRoute] string student_code)
        {
            var studentLogResult= await _entityUidService.ListStudentIllnessByStudentCode(student_code);
            return Ok(studentLogResult);
        }
    }
}