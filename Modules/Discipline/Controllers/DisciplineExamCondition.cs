using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Security.Claims;
using System.Threading.Tasks;
using edu_api.Modules.Discipline.Databases.Models;
using edu_api.Modules.Discipline.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SeventyOneDev.Utilities;

namespace edu_api.Modules.Discipline.Controllers
{
    [Route("api/discipline/discipline_exam_condition", Name = "discipline_exam_condition")]
    public class DisciplineExamConditionController:BaseUidController<t_discipline_exam_condition,v_discipline_exam_condition>
    {
        private readonly DisciplineExamConditionService _entityUidService;
        public DisciplineExamConditionController(DisciplineExamConditionService entityUidService) : base(entityUidService)
        {
            _entityUidService = entityUidService;
        }
        
        [Authorize]
        [HttpPost("discipline_exam_condition_copy/{discipline_year_uid}/{new_discipline_year_uid}")]
        public async Task<ActionResult<List<t_discipline_exam_condition>>> DisciplineExamConditionCopy([FromRoute, Required] Guid discipline_year_uid,[FromRoute, Required] Guid new_discipline_year_uid)
        {
            var result = await _entityUidService.Copy(discipline_year_uid,new_discipline_year_uid, User.Identity as ClaimsIdentity);
            return result == null ? NotFound() : Ok(result);
        } 
    }
}