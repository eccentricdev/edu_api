using edu_api.Modules.Discipline.Databases.Models;
using edu_api.Modules.Discipline.Services;
using Microsoft.AspNetCore.Mvc;
using SeventyOneDev.Utilities;

namespace edu_api.Modules.Discipline.Controllers
{
    [Route("api/discipline/student_discipline_detail", Name = "student_discipline_detail")]
    public class StudentDisciplineDetailController:BaseUidController<t_student_discipline_detail,v_student_discipline_detail>
    {
        public StudentDisciplineDetailController(StudentDisciplineDetailService entityUidService) : base(entityUidService)
        {

        }
    }
}