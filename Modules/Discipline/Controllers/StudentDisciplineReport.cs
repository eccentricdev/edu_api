using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;
using edu_api.Modules.Discipline.Databases.Models;
using edu_api.Modules.Discipline.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SeventyOneDev.Utilities;

namespace edu_api.Modules.Discipline.Controllers
{
  [Route("api/discipline/student_discipline_report", Name = "student_discipline_report")]
  public class StudentDisciplineReportController : Controller
  {
    private readonly StudentDisciplineReportService _studentDisciplineReportService;
    private readonly IHttpClientFactory _httpClientFactory;

    public StudentDisciplineReportController(StudentDisciplineReportService studentDisciplineReportService,
      IHttpClientFactory httpClientFactory)
    {
      _studentDisciplineReportService = studentDisciplineReportService;
      _httpClientFactory = httpClientFactory;
    }


    [HttpGet("print")]
    public async Task<ActionResult> GetStudentDisciplineReports(
      [FromQuery] student_discipline_report_request request)
    {
      var listResult = await _studentDisciplineReportService.ListStudentDisciplineReport(request);

      if (request.type != null)
      {
        string type = request.type;
        var httpClient = _httpClientFactory.CreateClient("report");

        var content = new ByteArrayContent(JsonSerializer.SerializeToUtf8Bytes(listResult));
        content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");
        var res = await httpClient.PostAsync("reports/student_discipline_report/" + (type == "view" ? "pdf" : type),
          content);
        if (res.StatusCode == HttpStatusCode.OK)
        {
          var name = "student_discipline_" + DateTime.Now.ToString("MMddyyyy_HHmmss");
          var stream = await res.Content.ReadAsStreamAsync();
          switch (type)
          {
            case "view":
              return File(stream, "application/pdf");
            case "pdf":
              return File(stream, "application/pdf", name + ".pdf");
            case "xls":
              return File(stream, "application/vnd.ms-excel", name + ".xlsx");
            case "mht":
              return File(stream, "multipart/related", name + ".mht");
            case "csv":
              return File(stream, "text/csv", name + ".csv");

          }
          return NoContent();
        }
        else
        {
          return NotFound();
        }
      }
      return Ok(listResult);
    }

    [HttpGet("print/{student_code}/{type}")]
    public async Task<ActionResult> GetStudentDisciplineReports(
      [FromRoute, Required] string student_code,
      [FromRoute, Required] string type
    )
    {
      var listResult = await _studentDisciplineReportService.ListStudentDisciplineReport(student_code);

      var httpClient = _httpClientFactory.CreateClient("report");
      var content = new ByteArrayContent(JsonSerializer.SerializeToUtf8Bytes(listResult));
      content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");
      var res = await httpClient.PostAsync("reports/student_discipline_detail/" + (type == "view" ? "pdf" : type),
        content);
      if (res.StatusCode == HttpStatusCode.OK)
      {
        var name = "student_discipline_student_" + DateTime.Now.ToString("MMddyyyy_HHmmss");
        var stream = await res.Content.ReadAsStreamAsync();
        switch (type)
        {
          case "view":
            return File(stream, "application/pdf");
          case "pdf":
            return File(stream, "application/pdf", name + ".pdf");
          case "xls":
            return File(stream, "application/vnd.ms-excel", name + ".xlsx");
          case "mht":
            return File(stream, "multipart/related", name + ".mht");
          case "csv":
            return File(stream, "text/csv", name + ".csv");

        }
        return NoContent();
      }
      else 
      {
        return NotFound();
      } 
    }
    
    
    [HttpGet("print/statistics")]
    public async Task<ActionResult> GetStudentDisciplineStatisticReports(
      [FromQuery] student_discipline_report_request request)
    {
      var listResult = await _studentDisciplineReportService.ListStudentDisciplineStatisticReport(request);
    
      if (request.type != null)
      {
        string type = request.type;
        var httpClient = _httpClientFactory.CreateClient("report");
    
        var content = new ByteArrayContent(JsonSerializer.SerializeToUtf8Bytes(listResult));
        content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");
        var res = await httpClient.PostAsync("reports/discipline_statistics/" + (type == "view" ? "pdf" : type),
          content);
        if (res.StatusCode == HttpStatusCode.OK)
        {
          var name = "student_discipline_statistic_" + DateTime.Now.ToString("MMddyyyy_HHmmss");
          var stream = await res.Content.ReadAsStreamAsync();
          switch (type)
          {
            case "view":
              return File(stream, "application/pdf");
            case "pdf":
              return File(stream, "application/pdf", name + ".pdf");
            case "xls":
              return File(stream, "application/vnd.ms-excel", name + ".xlsx");
            case "mht":
              return File(stream, "multipart/related", name + ".mht");
            case "csv":
              return File(stream, "text/csv", name + ".csv");
    
          }
          return NoContent();
        }
        else
        {
          return NotFound();
        }
      }
      return Ok(listResult);
    }
  }
}

