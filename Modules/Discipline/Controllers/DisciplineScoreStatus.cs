using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Security.Claims;
using System.Threading.Tasks;
using edu_api.Modules.Discipline.Databases.Models;
using edu_api.Modules.Discipline.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SeventyOneDev.Utilities;

namespace edu_api.Modules.Discipline.Controllers
{
    [Route("api/discipline/discipline_score_status", Name = "discipline_score_status")]
    public class DisciplineScoreStatusController : BaseUidController<t_discipline_score_status, v_discipline_score_status>
    {
        private readonly DisciplineScoreStatusService _entityUidService;

        public DisciplineScoreStatusController(DisciplineScoreStatusService entityUidService) : base(entityUidService)
        {
            _entityUidService = entityUidService;
        }

        [Authorize]
        [HttpPost("discipline_score_status_copy/{discipline_year_uid}/{new_discipline_year_uid}")]
        public async Task<ActionResult<List<t_discipline_condition>>> DisciplineConditionCopy(
            [FromRoute, Required] Guid discipline_year_uid, [FromRoute, Required] Guid new_discipline_year_uid)
        {
            var result = await _entityUidService.Copy(discipline_year_uid, new_discipline_year_uid,
                User.Identity as ClaimsIdentity);
            return result == null ? NotFound() : Ok(result);
        }
    }
}