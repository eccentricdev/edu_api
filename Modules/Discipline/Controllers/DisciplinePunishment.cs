using edu_api.Modules.Discipline.Databases.Models;
using edu_api.Modules.Discipline.Services;
using Microsoft.AspNetCore.Mvc;
using SeventyOneDev.Utilities;

namespace edu_api.Modules.Discipline.Controllers
{
    [Route("api/discipline/discipline_punishment", Name = "discipline_punishment")]
    public class DisciplinePunishmentController:BaseUidController<t_discipline_punishment,v_discipline_punishment>
    {
        public DisciplinePunishmentController(DisciplinePunishmentService entityUidService) : base(entityUidService)
        {

        }
    }
}