using edu_api.Modules.Discipline.Databases.Models;
using edu_api.Modules.Discipline.Services;
using Microsoft.AspNetCore.Mvc;
using SeventyOneDev.Utilities;

namespace edu_api.Modules.Discipline.Controllers
{
    [Route("api/discipline/student_discipline", Name = "student_discipline")]
    public class StudentDisciplineController:BaseUidController<t_student_discipline,v_student_discipline>
    {
        public StudentDisciplineController(StudentDisciplineService entityUidService) : base(entityUidService)
        {

        }
    }
}