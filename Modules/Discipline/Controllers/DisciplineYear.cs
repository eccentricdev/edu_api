using System;
using System.ComponentModel.DataAnnotations;
using System.Security.Claims;
using System.Threading.Tasks;
using edu_api.Modules.Discipline.Databases.Models;
using edu_api.Modules.Discipline.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SeventyOneDev.Utilities;

namespace edu_api.Modules.Discipline.Controllers
{
    [Route("api/discipline/discipline_year", Name = "discipline_year")]
    public class DisciplineYearController:BaseUidController<t_discipline_year,v_discipline_year>
    {
        private readonly DisciplineYearService _entityUidService;
        
        public DisciplineYearController(DisciplineYearService entityUidService) : base(entityUidService)
        {
            _entityUidService = entityUidService;
        }
        
        // [Authorize]
        // [HttpPost("discipline_year_copy/{discipline_year_uid}/{new_discipline_year_uid}")]
        // public async Task<ActionResult<t_discipline_year>> GetAcademicSemesterNoFilters([FromRoute, Required] Guid discipline_year_uid,[FromRoute, Required] Guid new_discipline_year_uid)
        // {
        //     var result = await _entityUidService.Copy(discipline_year_uid,new_discipline_year_uid, User.Identity as ClaimsIdentity);
        //     return result == null ? NotFound() : Ok(result);
        // } 
    }
}