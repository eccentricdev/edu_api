using edu_api.Modules.Commons.Services;
using edu_api.Modules.Discipline.Controllers;
using edu_api.Modules.Discipline.Services;
using edu_api.Modules.Organizations.Services;
using Microsoft.Extensions.DependencyInjection;

namespace edu_api.Modules.Organizations.Configures
{
  public static class DisciplineCollectionExtension
  {
    public static IServiceCollection AddDisciplineServices(this IServiceCollection services)
    {
      services.AddScoped<DisciplineConditionService>();
      services.AddScoped<StudentDisciplineReportService>();
      services.AddScoped<StudentIllnessService>();
      services.AddScoped<StudentIllnessPeportService>();
      services.AddScoped<StudentDisciplineService>();
      services.AddScoped<StudentDisciplineDetailService>();
      services.AddScoped<DisciplineYearService>();
      services.AddScoped<DisciplineScoreStatusService>();
      services.AddScoped<DisciplinePunishmentService>();
      services.AddScoped<DisciplineExamConditionService>();

      return services;
    }
  }
}
