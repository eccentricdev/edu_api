using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using SeventyOneDev.Utilities;
using SeventyOneDev.Utilities.Attributes;

namespace edu_api.Modules.Curriculums.Databases.Models
{
  public class subject:base_table
  {
    [Key]
    public Guid? subject_uid {get;set;}
    public string subject_code { get; set; }
    public string subject_seq_code {get;set;}
    public string subject_name_th {get;set;}
    public string subject_name_en {get;set;}
    public string credit_text { get; set; }
    public string remark { get; set; }
    public short? lecture_hour { get; set; }
    public short? lab_hour { get; set; }
    public short? practice_hour { get; set; }
    public string min_grade { get; set; }
    public short? credit { get; set; }
    public bool? is_gpa_include { get; set; }
    public bool? has_prerequisite { get; set; }
    public bool? has_co_subject { get; set; }
    public bool? allow_scholarship { get; set; }
    public short? subject_type_id { get; set; }
    public short? study_type_id { get; set; }
  }
  [GeneratedUidController("api/curriculum/subject")]
  public class t_subject : subject
  {
    public List<t_subject_year> subject_years { get; set; }
    public List<t_subject_owner> subject_owners { get; set; }
    public List<t_subject_topic> subject_topics { get; set; }
  }

  public class v_subject : subject
  {
    public List<v_subject_year> subject_years { get; set; }
    public List<v_subject_owner> subject_owners { get; set; }

    public List<v_subject_topic> subject_topics { get; set; }
  }
}
