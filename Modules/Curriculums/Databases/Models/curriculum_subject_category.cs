using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;
using SeventyOneDev.Utilities;

namespace edu_api.Modules.Curriculums.Databases.Models
{
  public class curriculum_subject_category:base_table
  {
    [Key]
    public Guid? curriculum_subject_category_uid { get; set; }
    public Guid? curriculum_uid { get; set; }
    public string curriculum_subject_category_code { get; set; }
    public string curriculum_subject_category_name_th { get; set; }
    public string curriculum_subject_category_name_en { get; set; }
    public short? total_credit { get; set; }
    public short? total_subject { get; set; }
    public short? min_credit { get; set; }
    public short? max_credit { get; set; }
  }

  public class t_curriculum_subject_category : curriculum_subject_category
  {
    [JsonIgnore]
    public t_curriculum curriculum { get; set; }
    public List<t_curriculum_subject_group> curriculum_subject_groups { get; set; }
  }

  public class v_curriculum_subject_category : curriculum_subject_category
  {
    [JsonIgnore]
    public v_curriculum curriculum { get; set; }
    public List<v_curriculum_subject_group> curriculum_subject_groups { get; set; }
  }
}
