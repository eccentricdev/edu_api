using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;
using SeventyOneDev.Utilities;
using SeventyOneDev.Utilities.Attributes;

namespace edu_api.Modules.Curriculums.Databases.Models
{
  public class subject_year_topic:base_table
  {
    [Key]
    public Guid? subject_year_topic_uid {get;set;}
    public string topic_code {get;set;}
    public string topic_name_th {get;set;}
    public string topic_name_en {get;set;}
    [Column(TypeName="decimal(5,1)")]
    public decimal? topic_credit {get;set;}
    public Guid? subject_year_uid {get;set;}
    public string subject_seq_code { get; set; }

  }
  [GeneratedUidController("api/curriculum/subject_year_topic")]
  public class t_subject_year_topic : subject_year_topic
  {
    [JsonIgnore]
    public t_subject_year subject_year { get; set; }
  }

  public class v_subject_year_topic : subject_year_topic
  {
    [JsonIgnore]
    public v_subject_year subject_year { get; set; }
  }
}
