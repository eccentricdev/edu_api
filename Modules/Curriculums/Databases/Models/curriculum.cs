using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using SeventyOneDev.Utilities;
using SeventyOneDev.Utilities.Attributes;

namespace edu_api.Modules.Curriculums.Databases.Models
{
  public class curriculum:base_table
  {
		  [Key]
      public Guid? curriculum_uid { get; set; }
      public Guid? education_type_uid {get;set;}
      public Guid? college_faculty_uid { get; set; }
      public Guid? curriculum_year_uid { get; set; }
      public string curriculum_code {get;set;}
      public string education_type_code {get;set;}
      public string faculty_code {get;set;}
      //public Guid? college_uid { get; set; }
      //public Guid? faculty_uid { get; set; }
      //public Guid? department_uid { get; set; }
		  //public int? faculty_id {get;set;}

		  //public int? major_id {get;set;}

		  //public Guid? academic_year_uid {get;set;}
		  //public string academic_year_code {get;set;}
		  public short? total_semester {get;set;}
		  public short? max_year_study {get;set;}
		  public short? minimum_credits {get;set;}
		  public short? normal_year_study {get;set;}
		  public string curriculum_name_th {get;set;}
		  public string curriculum_name_en {get;set;}
		  public string full_degree_name_th {get;set;}
		  public string full_degree_name_en {get;set;}
		  public string short_degree_name_th {get;set;}
		  public string short_degree_name_en {get;set;}
		  public string curriculum_faculty_name_th {get;set;}
		  public string curriculum_faculty_name_en {get;set;}
		  public string curriculum_major_name_th {get;set;}
		  public string curriculum_major_name_en {get;set;}
		  public short? curriculum_type_id {get;set;}
		  public string full_degree_doc_name_th {get;set;}
		  public string full_degree_doc_name_en {get;set;}
		  public string curriculum_faculty_doc_name_th {get;set;}
		  public string curriculum_faculty_doc_name_en {get;set;}
		  public string curriculum_major_doc_name_th {get;set;}
		  public string curriculum_major_doc_name_en {get;set;}
      public string curriculum_year_code { get; set; }
      public bool? is_package { get; set; }
      public bool? is_group_semester { get; set; }
      public decimal? semester_1_amount { get; set; }
      public decimal? semester_2_amount { get; set; }
      public decimal? semester_3_amount { get; set; }
      public Guid? faculty_curriculum_uid { get; set; }
  }
  [GeneratedUidController("api/curriculum/curriculum")]
  public class t_curriculum : curriculum
  {
    public List<t_curriculum_subject_category> curriculum_subject_categories { get; set; }
  }

  public class v_curriculum : curriculum
  {
    public List<v_curriculum_subject_category> curriculum_subject_categories { get; set; }
  }
}
