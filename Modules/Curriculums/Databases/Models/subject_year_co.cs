using System;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;
using SeventyOneDev.Utilities;

namespace edu_api.Modules.Curriculums.Databases.Models
{
  public class subject_year_co:base_table
  {
    [Key]
    public Guid? subject_year_co_uid { get; set; }
    public Guid? subject_year_uid { get; set; }
    public Guid? subject_uid { get; set; }
  }

  public class t_subject_year_co : subject_year_co
  {
    [JsonIgnore]
    public t_subject_year subject_year { get; set; }
  }

  public class v_subject_year_co : subject_year_co
  {
    [JsonIgnore]
    public v_subject_year subject_year { get; set; }
  }
}
