using System;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;
using SeventyOneDev.Utilities;

namespace edu_api.Modules.Curriculums.Databases.Models
{
  public class subject_owner:base_table
  {
    [Key]
    public Guid? subject_owner_uid { get; set; }
    public Guid? subject_uid { get; set; }
    public Guid? academic_year_uid { get; set; }
    public Guid? college_faculty_uid { get; set; }
  }

  public class t_subject_owner : subject_owner
  {
    [JsonIgnore]
    public t_subject subject { get; set; }
  }

  public class v_subject_owner : subject_owner
  {
    [JsonIgnore]
    public v_subject subject { get; set; }
  }
}
