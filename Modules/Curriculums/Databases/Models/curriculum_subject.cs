using System;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;
using SeventyOneDev.Utilities;

namespace edu_api.Modules.Curriculums.Databases.Models
{
  public class curriculum_subject:base_table
  {
    [Key]
    public Guid? curriculum_subject_uid { get; set; }
    public Guid? curriculum_subject_group_uid { get; set; }
    public Guid? subject_uid { get; set; }
  }

  public class t_curriculum_subject : curriculum_subject
  {
    [JsonIgnore]
    public t_curriculum_subject_group curriculum_subject_group { get; set; }
  }

  public class v_curriculum_subject : curriculum_subject
  {
    [JsonIgnore]
    public v_curriculum_subject_group curriculum_subject_group { get; set; }
  }
}
