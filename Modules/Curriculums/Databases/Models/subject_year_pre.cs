using System;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;
using SeventyOneDev.Utilities;

namespace edu_api.Modules.Curriculums.Databases.Models
{
  public class subject_year_pre:base_table
  {
    [Key]
    public Guid? subject_year_pre_uid { get; set; }
    public Guid? subject_year_uid { get; set; }
    public Guid? subject_uid { get; set; }
  }

  public class t_subject_year_pre : subject_year_pre
  {
    [JsonIgnore]
    public t_subject_year subject_year { get; set; }
  }

  public class v_subject_year_pre : subject_year_pre
  {
    [JsonIgnore]
    public v_subject_year subject_year { get; set; }
  }
}
