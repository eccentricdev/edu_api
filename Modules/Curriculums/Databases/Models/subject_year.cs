using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;
using SeventyOneDev.Utilities;

namespace edu_api.Modules.Curriculums.Databases.Models
{
  public class subject_year:base_table
  {
    [Key]
    public Guid? subject_year_uid { get; set; }
    public Guid? subject_uid { get; set; }
    public Guid? curriculum_year_uid { get; set; }
    public string curriculum_year_code { get; set; }
    public string credit_text { get; set; }
    public string remark { get; set; }
    public short? lecture_hour { get; set; }
    public short? lab_hour { get; set; }
    public short? practice_hour { get; set; }
    public string min_grade { get; set; }
    public short? credit { get; set; }
    public bool? is_gpa_include { get; set; }
    //public List<string> pre_subject_codes { get; set; }
    //public List<string> co_subject_codes { get; set; }
    public bool? allow_scholarship { get; set; }
    public string subject_type_code { get; set; }
    // public Guid? education_type_uid { get; set; }
    // public Guid? curriculum_year_uid { get; set; }
    // public decimal? lecture_amount { get; set; }
    // public decimal? lab_amount { get; set; }
    // public decimal? practice_amount { get; set; }

    // public string credit_text { get; set; }
    // public string remark { get; set; }
    // public short? lecture_hour { get; set; }
    // public short? lab_hour { get; set; }
    // public short? practice_hour { get; set; }
    // public string min_grade { get; set; }
    // public short? credit { get; set; }
    // public bool? is_gpa_include { get; set; }
    // public bool? has_prerequisite { get; set; }
    // public bool? has_co_subject { get; set; }
    // public bool? allow_scholarship { get; set; }
    // public short? subject_type_id { get; set; }
    // public short? study_type_id { get; set; }

  }

  public class t_subject_year : subject_year
  {
    [JsonIgnore]
    public t_subject subject { get; set; }
    public List<t_subject_year_cost> subject_year_costs { get; set; }
    public List<t_subject_year_pre> subject_year_pres { get; set; }
    public List<t_subject_year_co> subject_year_cos { get; set; }
    public List<t_subject_year_topic> subject_year_topics { get; set; }
  }

  public class v_subject_year : subject_year
  {
    [JsonIgnore]
    public v_subject subject { get; set; }
    public List<v_subject_year_cost> subject_year_costs { get; set; }
    public List<v_subject_year_pre> subject_year_pres { get; set; }
    public List<v_subject_year_co> subject_year_cos { get; set; }
    public List<v_subject_year_topic> subject_year_topics { get; set; }
  }
}
