using System;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;
using SeventyOneDev.Utilities;

namespace edu_api.Modules.Curriculums.Databases.Models
{
  public class subject_year_cost:base_table
  {
    [Key]
    public Guid? subject_year_cost_uid { get; set; }
    public Guid? subject_year_uid { get; set; }
    public Guid? education_type_uid { get; set; }
    public decimal? lecture_amount { get; set; }
    public decimal? lab_amount { get; set; }
    public decimal? practice_amount { get; set; }
  }

  public class t_subject_year_cost : subject_year_cost
  {
    [JsonIgnore]
    public t_subject_year subject_year { get; set; }
  }

  public class v_subject_year_cost : subject_year_cost
  {
    public Guid? curriculum_year_uid { get; set; }
    public short? credit { get; set; }
    [JsonIgnore]
    public v_subject_year subject_year { get; set; }
  }
}
