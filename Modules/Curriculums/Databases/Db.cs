using edu_api.Modules.Curriculums.Databases.Models;
using Microsoft.EntityFrameworkCore;
using SeventyOneDev.Utilities;

namespace  SeventyOneDev.Utilities
{
  public partial class Db : DbContext
  {
    public DbSet<t_curriculum> t_curriculum { get; set; }
    public DbSet<v_curriculum> v_curriculum { get; set; }
    public DbSet<t_curriculum_subject_category> t_curriculum_subject_category { get; set; }
    public DbSet<v_curriculum_subject_category> v_curriculum_subject_category { get; set; }
    public DbSet<t_curriculum_subject_group> t_curriculum_subject_group { get; set; }
    public DbSet<v_curriculum_subject_group> v_curriculum_subject_group { get; set; }
    public DbSet<t_curriculum_subject> t_curriculum_subject { get; set; }
    public DbSet<v_curriculum_subject> v_curriculum_subject { get; set; }






    public DbSet<t_curriculum_year> t_curriculum_year { get; set; }
    public DbSet<v_curriculum_year> v_curriculum_year { get; set; }

    public DbSet<t_subject> t_subject { get; set; }
    public DbSet<v_subject> v_subject { get; set; }
    public DbSet<t_subject_owner> t_subject_owner { get; set; }
    public DbSet<v_subject_owner> v_subject_owner { get; set; }
    public DbSet<t_subject_year> t_subject_year { get; set; }
    public DbSet<v_subject_year> v_subject_year { get; set; }
    public DbSet<t_subject_year_cost> t_subject_year_cost { get; set; }
    public DbSet<v_subject_year_cost> v_subject_year_cost { get; set; }
    public DbSet<t_subject_year_topic> t_subject_year_topic { get; set; }
    public DbSet<v_subject_year_topic> v_subject_year_topic { get; set; }
    public DbSet<t_subject_year_pre> t_subject_year_pre { get; set; }
    public DbSet<v_subject_year_pre> v_subject_year_pre { get; set; }
    public DbSet<t_subject_year_co> t_subject_year_co { get; set; }
    public DbSet<v_subject_year_co> v_subject_year_co { get; set; }
    public DbSet<t_subject_topic> t_subject_topic { get; set; }
    public DbSet<v_subject_topic> v_subject_topic { get; set; }
    private void OnCurriculumModelCreating(ModelBuilder builder, string schema)
    {
      builder.Entity<t_curriculum>().ToTable("curriculum", schema);
      builder.Entity<v_curriculum>().ToView("v_curriculum", schema);
      builder.Entity<t_curriculum_subject_category>().ToTable("curriculum_subject_category", schema);
      builder.Entity<t_curriculum_subject_category>().HasOne(c => c.curriculum)
        .WithMany(c => c.curriculum_subject_categories).HasForeignKey(c => c.curriculum_uid)
        .OnDelete(DeleteBehavior.Cascade);
      builder.Entity<v_curriculum_subject_category>().ToView("v_curriculum_subject_category", schema);
      builder.Entity<v_curriculum_subject_category>().HasOne(c => c.curriculum)
        .WithMany(c => c.curriculum_subject_categories).HasForeignKey(c => c.curriculum_uid)
        .OnDelete(DeleteBehavior.Cascade);


      builder.Entity<t_curriculum_subject_group>().ToTable("curriculum_subject_group", schema);
      builder.Entity<t_curriculum_subject_group>().HasOne(c => c.curriculum_subject_category)
        .WithMany(c => c.curriculum_subject_groups).HasForeignKey(c => c.curriculum_subject_category_uid)
        .OnDelete(DeleteBehavior.Cascade);
      builder.Entity<v_curriculum_subject_group>().ToView("v_curriculum_subject_group", schema);
      builder.Entity<v_curriculum_subject_group>().HasOne(c => c.curriculum_subject_category)
        .WithMany(c => c.curriculum_subject_groups).HasForeignKey(c => c.curriculum_subject_category_uid)
        .OnDelete(DeleteBehavior.Cascade);


      builder.Entity<t_curriculum_subject>().ToTable("curriculum_subject", schema);
      builder.Entity<t_curriculum_subject>().HasOne(c => c.curriculum_subject_group)
        .WithMany(c => c.curriculum_subjects).HasForeignKey(c => c.curriculum_subject_group_uid)
        .OnDelete(DeleteBehavior.Cascade);
      builder.Entity<v_curriculum_subject>().ToView("v_curriculum_subject", schema);
      builder.Entity<v_curriculum_subject>().HasOne(c => c.curriculum_subject_group)
        .WithMany(c => c.curriculum_subjects).HasForeignKey(c => c.curriculum_subject_group_uid)
        .OnDelete(DeleteBehavior.Cascade);





      builder.Entity<t_curriculum_year>().ToTable("curriculum_year", schema);
      builder.Entity<v_curriculum_year>().ToView("v_curriculum_year", schema);
      builder.Entity<t_subject>().ToTable("subject", schema);
      builder.Entity<v_subject>().ToView("v_subject", schema);


      builder.Entity<t_subject_year>().ToTable("subject_year", schema);
      builder.Entity<t_subject_year>().HasOne(s => s.subject).WithMany(s => s.subject_years)
        .HasForeignKey(s => s.subject_uid).OnDelete(DeleteBehavior.Cascade);

      builder.Entity<v_subject_year>().ToView("v_subject_year", schema);
      builder.Entity<v_subject_year>().HasOne(s => s.subject).WithMany(s => s.subject_years)
        .HasForeignKey(s => s.subject_uid).OnDelete(DeleteBehavior.Cascade);

      builder.Entity<t_subject_owner>().ToTable("subject_owner", schema);
      builder.Entity<t_subject_owner>().HasOne(s => s.subject).WithMany(s => s.subject_owners)
        .HasForeignKey(s => s.subject_uid).OnDelete(DeleteBehavior.Cascade);

      builder.Entity<v_subject_owner>().ToView("v_subject_owner", schema);
      builder.Entity<v_subject_owner>().HasOne(s => s.subject).WithMany(s => s.subject_owners)
        .HasForeignKey(s => s.subject_uid).OnDelete(DeleteBehavior.Cascade);

      builder.Entity<t_subject_year_cost>().ToTable("subject_year_cost", schema);
      builder.Entity<t_subject_year_cost>().HasOne(s => s.subject_year).WithMany(s => s.subject_year_costs)
        .HasForeignKey(s => s.subject_year_uid).OnDelete(DeleteBehavior.Restrict);
      builder.Entity<v_subject_year_cost>().ToView("v_subject_year_cost", schema);
      builder.Entity<v_subject_year_cost>().HasOne(s => s.subject_year).WithMany(s => s.subject_year_costs)
        .HasForeignKey(s => s.subject_year_uid).OnDelete(DeleteBehavior.Restrict);

      builder.Entity<t_subject_year_pre>().ToTable("subject_year_pre", schema);
      builder.Entity<t_subject_year_pre>().HasOne(s => s.subject_year).WithMany(s => s.subject_year_pres)
        .HasForeignKey(s => s.subject_year_uid).OnDelete(DeleteBehavior.Restrict);
      builder.Entity<v_subject_year_pre>().ToView("v_subject_year_pre", schema);
      builder.Entity<v_subject_year_pre>().HasOne(s => s.subject_year).WithMany(s => s.subject_year_pres)
        .HasForeignKey(s => s.subject_year_uid).OnDelete(DeleteBehavior.Restrict);

      builder.Entity<t_subject_year_co>().ToTable("subject_year_co", schema);
      builder.Entity<t_subject_year_co>().HasOne(s => s.subject_year).WithMany(s => s.subject_year_cos)
        .HasForeignKey(s => s.subject_year_uid).OnDelete(DeleteBehavior.Restrict);
      builder.Entity<v_subject_year_co>().ToView("v_subject_year_co", schema);
      builder.Entity<v_subject_year_co>().HasOne(s => s.subject_year).WithMany(s => s.subject_year_cos)
        .HasForeignKey(s => s.subject_year_uid).OnDelete(DeleteBehavior.Restrict);

      builder.Entity<t_subject_year_topic>().ToTable("subject_year_topic", schema);
      builder.Entity<t_subject_year_topic>().HasOne(s => s.subject_year).WithMany(s => s.subject_year_topics)
        .HasForeignKey(s => s.subject_year_uid).OnDelete(DeleteBehavior.Restrict);
      builder.Entity<v_subject_year_topic>().ToView("v_subject_year_topic", schema);
      builder.Entity<v_subject_year_topic>().HasOne(s => s.subject_year).WithMany(s => s.subject_year_topics)
        .HasForeignKey(s => s.subject_year_uid).OnDelete(DeleteBehavior.Restrict);

      builder.Entity<t_subject_topic>().ToTable("subject_topic", schema);
      builder.Entity<t_subject_topic>().HasOne(s => s.subject).WithMany(s => s.subject_topics)
        .HasForeignKey(s => s.subject_uid).OnDelete(DeleteBehavior.Cascade);
      builder.Entity<v_subject_topic>().ToView("v_subject_topic", schema);
      builder.Entity<v_subject_topic>().HasOne(s => s.subject).WithMany(s => s.subject_topics)
        .HasForeignKey(s => s.subject_uid).OnDelete(DeleteBehavior.Cascade);

    }

  }
}
