using System;
using System.Collections.Generic;
using SeventyOneDev.Utilities;

namespace edu_api.Modules.Workflows.Databases.Models
{
    public class app_workflow:base_table
    {
        public Guid? workflow_uid { get; set; }
        public string workflow_code { get;set; }
        public string workflow_name { get; set; }
        public Guid? function_uid { get; set; }
        public Guid? start_workflow_step_uid { get; set; }
        public Guid? work_chart_uid { get; set; }
    }

    public class t_app_workflow : app_workflow
    {
        public List<t_app_workflow_step> workflow_steps { get; set; }
    }

    public class v_app_workflow : app_workflow
    {
        public List<v_app_workflow_step> workflow_steps { get; set; }
    }
    
}