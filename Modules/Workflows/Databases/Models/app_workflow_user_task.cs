using System;
using System.Text.Json.Serialization;
using SeventyOneDev.Utilities;

namespace edu_api.Modules.Workflows.Databases.Models
{
    
   
    //My task
    public class app_workflow_user_task:base_table
    {
        public Guid? workflow_user_task_uid { get; set; }
        public Guid? ref_uid { get; set; } //id of budget approve
        //public Guid? workflow_step_uid { get; set; } //Manager approce
        public Guid? user_uid { get; set; } //approve user
        public Guid? owner_uid { get; set; }
        //public Guid? workflow_status_uid { get; set; } //waiting,approve,reject,correction
        public Guid? workflow_step_action_uid { get; set; }
        //public Guid? workflow_process_type_uid { get; set; } //one,all
        public bool? is_readonly { get; set; }
        public bool? is_active { get; set; }
        //public bool? is_end { get; set; }
        public string remark { get; set; }
    }

    public class t_app_workflow_user_task : app_workflow_user_task
    {
        [JsonIgnore]public t_app_workflow_step_action app_workflow_step_action { get; set; }
    }

    public class v_app_workflow_user_task : app_workflow_user_task
    {
        [JsonIgnore]public v_app_workflow_step_action app_workflow_step_action { get; set; }
    }

    // public class app_workflow:base_table
    // {
    //     public Guid? workflow_uid { get; set; }
    //     public string workflow_code { get;set; }
    //     public string workflow_name { get; set; }
    //     public Guid? function_uid { get; set; }
    //     public Guid? start_workflow_step_uid { get; set; }
    //     public Guid? work_chart_uid { get; set; }
    // }

    

    
}