using System;
using System.Collections.Generic;
using System.Text.Json.Serialization;
using SeventyOneDev.Utilities;

namespace edu_api.Modules.Workflows.Databases.Models
{
    public class app_workflow_step:base_table
    {
        public Guid? workflow_step_uid { get; set; }
        public Guid? workflow_uid { get; set; }
        public string workflow_step_code { get; set; }
        public string workflow_step_name { get; set; }
        public string workflow_step_url { get; set; } //https://sssxxxx/workflow/{uid}
        public bool? is_readonly { get; set; }
        //public int? approve_level { get; set; }
        //public Guid? approve_position_uid { get; set; }
        //public Guid? approve_role_uid { get; set; }
        
        
        //public int? work_chart_level { get; set; }
    }

    public class t_app_workflow_step : app_workflow_step
    {
        [JsonIgnore]public t_app_workflow workflow { get; set; }
       public List<t_app_workflow_step_action>  app_workflow_step_actions { get; set; }
    }

    public class v_app_workflow_step : app_workflow_step
    {
        [JsonIgnore]public v_app_workflow workflow { get; set; }
        public List<v_app_workflow_step_action>  app_workflow_step_actions { get; set; }
    }
}