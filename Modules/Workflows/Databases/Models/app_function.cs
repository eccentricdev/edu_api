using System;
using SeventyOneDev.Utilities;

namespace edu_api.Modules.Workflows.Databases.Models
{
    public class app_function:base_table
    {
        public Guid? function_uid { get; set; }
        public string function_name { get; set; }
        //public string function_url { get; set; }
    }

    public class t_app_function : app_function
    {
        
    }

    public class v_app_function : app_function
    {
        
    }
}