using System;
using System.Collections.Generic;
using System.Text.Json.Serialization;
using SeventyOneDev.Utilities;

namespace edu_api.Modules.Workflows.Databases.Models
{

    public class app_workflow_step_action : base_table
    {
        public Guid? workflow_step_action_uid { get; set; }
        public Guid? workflow_step_uid { get; set; }
        public string workflow_step_action_code { get; set; }
        public string workflow_step_action_name { get; set; }
        public Guid? next_workflow_step { get; set; }
        public bool? is_end { get; set; }
    }

    public class t_app_workflow_step_action : app_workflow_step_action
    {
        [JsonIgnore]public t_app_workflow_step workflow_step { get; set; }
        public List<t_app_workflow_user_task> app_workflow_user_tasks { get; set; }
    }

    public class v_app_workflow_step_action : app_workflow_step_action
    {
        [JsonIgnore]public v_app_workflow_step workflow_step { get; set; }
        public List<v_app_workflow_user_task> app_workflow_user_tasks { get; set; }
    }

}