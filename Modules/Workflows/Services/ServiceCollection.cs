using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace edu_api.Modules.Workflows.Services
{
    public static class ServiceCollectionExtension
    {
        public static IServiceCollection AddWorkflow(this IServiceCollection services,IConfiguration configurationSection)
        {
            WorkflowJob.Configure(configurationSection);
            // services.AddScoped<UserService>();
            // services.AddScoped<RoleService>();
            // services.AddScoped<PermissionService>();
            // services.AddScoped<ObjectService>();
            // services.AddScoped<DataService>();
            // services.AddScoped<SecurityService>();
            // services.AddScoped<AppRoleTypeService>();
            // services.AddScoped<AppUserTypeService>();


            return services;
        }
       
    }
}