using System;
using edu_api.Modules.Register.Models;

namespace edu_api.Modules.Installment.Models
{
  public class pending_register
  {
      public student_installment student_installment { get; set; }
      public register_detail register_detail { get; set; }
      public installment_detail installment_detail { get; set; }
  }

  public class installment_detail
  {
    public decimal? minimum_installment_amount { get; set; }
    public decimal? pre_installment_amount { get; set; }
    public decimal? installment_amount { get; set; }
    public decimal? maximum_period { get; set; }
    public int? interest_charge_type_id { get; set; }
  }
  public class student_installment
  {
    public Guid? student_uid { get; set; }
    public string student_code { get; set; }
    public string display_name_th { get; set; }
    public string display_name_en { get; set; }
    public string college_faculty_name_th { get; set; }
    public string college_faculty_name_en { get; set; }
    public string faculty_curriculum_name_th { get; set; }
    public string faculty_curriculum_name_en { get; set; }
    public Guid? education_type_uid { get; set; }
    public Guid? college_faculty_uid { get; set; }
    public Guid? faculty_curriculum_uid { get; set; }
    public string rsu_email { get; set; }
    public Guid? education_type_level_uid { get; set; }

  }
}
