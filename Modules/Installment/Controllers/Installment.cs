using System;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using DocumentFormat.OpenXml.Bibliography;
using edu_api.Modules.Installment.Models;
using edu_api.Modules.Register.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using rsu_common_api.models;
using SeventyOneDev.Utilities;

namespace edu_api.Modules.Installment.Controllers
{
  [Route("api/installment/register", Name = "register")]
  public class InstallmentController:Controller
  {
    private readonly Db _db;
    private readonly CoreRegisterService _preRegisterService;
    public InstallmentController(Db db, CoreRegisterService preRegisterService)
    {
      _db = db;
      _preRegisterService = preRegisterService;
    }
    [Authorize]
    [HttpGet]
    public async Task<ActionResult<pending_register>> GetPendingRegister()
    {
      var pendingRegister = new pending_register(){};
      var claimsIdentity = User.Identity as ClaimsIdentity;
      var studentUid = claimsIdentity.FindFirst(s => s.Type == "user_uid").Value;
      if(studentUid!=null){
        pendingRegister.student_installment = await _db.v_student.AsNoTracking().Where(s => s.student_uid == Guid.Parse(studentUid))
          .ProjectTo<v_student, student_installment>().FirstOrDefaultAsync();
        if(pendingRegister.student_installment!=null){
          var registerToken = await _db.t_register_token.AsNoTracking()
            .FirstOrDefaultAsync(r => r.student_uid == Guid.Parse(studentUid));
          if(registerToken!=null){
            pendingRegister.register_detail = await _preRegisterService.PreRegisterCheck("PRE",registerToken.register_token_uid.Value);
            return pendingRegister;
          }
        }
      }
      return NotFound();
    }
  }
}
