using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;
using SeventyOneDev.Utilities;
using SeventyOneDev.Utilities.Attributes;

namespace edu_api.Modules.InstructorEvaluations.Databases.Models
{
  public class student_evaluation_question_result : base_table
  {
    [Key]
    public Guid? student_evaluation_question_result_uid { get; set; }
    public Guid? student_evaluation_question_set_uid { get; set; }
    //public Guid? question_set_uid { get; set; }
    public Guid? question_uid { get; set; }

    public short? selected_choice { get; set; }
    [MaxLength(500)]
    public string text_answer { get; set; }
  }
  [GeneratedUidController("api/instructor_evaluations/student_evaluation_question_result")]
  public class t_student_evaluation_question_result : student_evaluation_question_result
  {

    [JsonIgnore]
    public t_student_evaluation_question_set student_evaluation_question_set { get; set; }
  }


  public class v_student_evaluation_question_result : student_evaluation_question_result
  {
    public string question_name_th { get; set; }
    public string question_name_en { get; set; }
    public Guid? answer_type_uid { get; set; }
    public int? question_no { get; set; }
    public string answer_type_code { get; set; }
    public string answer_type_name_th { get; set; }
    public string answer_type_name_en { get; set; }
    [JsonIgnore]
    public v_student_evaluation_question_set student_evaluation_question_set { get; set; }

    public v_student_evaluation_question_result()
    {

    }

    public v_student_evaluation_question_result(v_question question)
    {
      this.question_name_th = question.question_name_th;
      this.question_name_en = question.question_name_en;
      this.question_uid = question.question_uid;
      this.answer_type_uid = question.answer_type_uid;
      this.question_no = question.question_no;
      answer_type_code = question.answer_type_code;
      answer_type_name_th = question.answer_type_name_th;
      answer_type_name_en = question.answer_type_name_en;
    }
  }
}
