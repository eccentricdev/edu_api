using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace edu_api.Modules.InstructorEvaluations.Databases.Models
{
  public class v_student_evaluation_subject
  {
    public Guid? college_faculty_uid { get; set; }
    public Guid? academic_year_uid {get;set;}
    public Guid? academic_semester_uid {get;set;}
    public Guid? year_subject_uid {get;set;}
    public Guid? year_subject_section_uid {get;set;}
    public string section_code {get;set;}
    public string year_subject_code { get; set; }
    public string year_subject_name_th {get;set;}
    public string year_subject_name_en {get;set;}
    public int? is_response {get;set;}
    [NotMapped]
    public int? assinged { get; set; }
    [NotMapped]
    public int? responsed { get; set; }
    [NotMapped]
    public string instructor_code { get; set; }
    [NotMapped]
    public string instructor_name { get; set; }
    // [NotMapped]
    // public List<v_evaluate_subject_section_instructor> instructors { get; set; }
    [NotMapped]
    public List<e_question_set_result> question_set_results { get; set; }
  }

  public class e_question_set_result
  {
    public string question_set_name_th { get; set; }
    public List<e_question_result> question_results { get; set; }
  }

  public class e_question_result
  {
    public int? question_no { get; set; }
    public string question_name_th { get; set; }
    public List<e_score_rate> score_rates { get; set; }
  }

  public class e_score_rate
  {
    public int? rate { get; set; }
    public int? score { get; set; }
    public decimal? percent { get; set; }
    public List<string> comments { get; set; }
  }
}
