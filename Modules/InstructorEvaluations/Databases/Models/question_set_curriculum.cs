using System;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;
using SeventyOneDev.Utilities;
using SeventyOneDev.Utilities.Attributes;

namespace edu_api.Modules.InstructorEvaluations.Databases.Models
{
  public class question_set_curriculum : base_table
  {
    [Key] public Guid? question_set_curriculum_uid { get; set; }
public Guid? question_set_faculty_uid { get; set; }
    public Guid? question_set_uid { get; set; }
    public Guid? academic_year_uid { get; set; }

    public Guid? academic_semester_uid { get; set; }

    //public int? major_id { get; set; }
    public Guid? faculty_curriculum_uid { get; set; }
    public Guid? college_faculty_uid { get; set; }
    public int? score_calculation_type_id { get; set; }
    public bool? is_approve { get; set; }
  }

  //[GeneratedUidController("api/instructor_evaluations/question_set_curriculum")]
  public class t_question_set_curriculum : question_set_curriculum
  {
    [JsonIgnore] public t_question_set question_set { get; set; }
  }

  public class v_question_set_curriculum : question_set_curriculum
  {
    public string academic_year_code { get; set; }
    public string academic_year_name_th { get; set; }
    public string academic_year_name_en { get; set; }

    public string academic_semester_code { get; set; }
    public string academic_semester_name_th { get; set; }
    public string academic_semester_name_en { get; set; }

    public string faculty_curriculum_code { get; set; }
    public string faculty_curriculum_name_th { get; set; }
    public string faculty_curriculum_name_en { get; set; }

    public int? total_questions { get; set; }
    
    public string question_group_name_th { get; set; }
    public string question_set_name_th { get; set; }
    
    public string education_type_code {get;set;}
    public string education_type_name_th {get;set;}
    public string education_type_name_en  {get;set;}

    //public int? total_subjects { get; set; }
    //public int? total_sets { get; set; }
    [JsonIgnore] public v_question_set question_set { get; set; }
  }

}
