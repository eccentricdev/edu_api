using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using SeventyOneDev.Utilities;
using SeventyOneDev.Utilities.Attributes;

namespace edu_api.Modules.InstructorEvaluations.Databases.Models
{
  public class student_evaluation : base_table
  {
    [Key] public Guid? student_evaluation_uid { get; set; }
    public Guid? student_uid { get; set; }
    public Guid? academic_year_uid { get; set; }
    public Guid? academic_semester_uid { get; set; }
    public Guid? year_subject_uid { get; set; }
    public string year_subject_code { get; set; }
    public string year_subject_name_th { get; set; }
    public string year_subject_name_en { get; set; }
    public Guid? year_subject_section_uid { get; set; }
    public Guid? student_evaluation_status_uid { get; set; }
    public Guid? college_faculty_uid { get; set; }
    public DateTime? evaluation_date { get; set; }
    public string section_code { get; set; }
    public string instructor_code { get; set; }

  }

  public class t_student_evaluation : student_evaluation
    {
      [Include]
      public List<t_student_evaluation_question_set> student_evaluation_question_sets { get; set; }

      public t_student_evaluation()
      {

      }

      public t_student_evaluation(v_student_evaluation_calendar studentEvaluationCalendar)
      {
        this.student_uid = studentEvaluationCalendar.student_uid;
        this.academic_year_uid = studentEvaluationCalendar.academic_year_uid;
        this.academic_semester_uid = studentEvaluationCalendar.academic_semester_uid;
        this.year_subject_uid = studentEvaluationCalendar.year_subject_uid;
        this.year_subject_code = studentEvaluationCalendar.year_subject_code;
        this.year_subject_name_th = studentEvaluationCalendar.year_subject_name_th;
        this.year_subject_name_en = studentEvaluationCalendar.year_subject_name_en;
        this.year_subject_section_uid = studentEvaluationCalendar.year_subject_section_uid;
        this.section_code = studentEvaluationCalendar.section_code;
        //this.st = "1";
        this.college_faculty_uid = studentEvaluationCalendar.college_faculty_uid;
      }
      public t_student_evaluation(v_student_evaluation_calendar_faculty studentEvaluationCalendarFaculty)
      {
        this.student_uid = studentEvaluationCalendarFaculty.student_uid;
        this.academic_year_uid = studentEvaluationCalendarFaculty.academic_year_uid;
        this.academic_semester_uid = studentEvaluationCalendarFaculty.academic_semester_uid;
        this.year_subject_uid = studentEvaluationCalendarFaculty.year_subject_uid;
        this.year_subject_code = studentEvaluationCalendarFaculty.year_subject_code;
        this.year_subject_name_th = studentEvaluationCalendarFaculty.year_subject_name_th;
        this.year_subject_name_en = studentEvaluationCalendarFaculty.year_subject_name_en;
        this.year_subject_section_uid = studentEvaluationCalendarFaculty.year_subject_section_uid;
        this.section_code = studentEvaluationCalendarFaculty.section_code;
        //this.st = "1";
        this.college_faculty_uid = studentEvaluationCalendarFaculty.college_faculty_uid;
      }
    }

  
  public class v_student_evaluation : student_evaluation
  {
    //public string student_evaluation_status_name_en { get; set; }
    //public string student_evaluation_status_name_th { get; set; }

    // public string study_type_name_th { get; set; }
    // public string study_type_name_en { get; set; }
    public short? section_type_id {get;set;}
    public string instructor_name_th { get; set; }

    [NotMapped]
    public string instructors { get; set; }

    [Include]
    public List<v_student_evaluation_question_set> student_evaluation_question_sets { get; set; }
  }
}
