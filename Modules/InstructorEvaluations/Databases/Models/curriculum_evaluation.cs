using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using SeventyOneDev.Utilities;
using SeventyOneDev.Utilities.Attributes;

namespace edu_api.Modules.InstructorEvaluations.Databases.Models
{
  [GeneratedUidController("api/instructor_evaluations/curriculum_evaluation")]
  public class v_curriculum_evaluation:base_table
  {
    [Key]
    public Guid? faculty_curriculum_uid { get; set; }
    public string faculty_curriculum_code { get; set; }
    public string faculty_curriculum_name_th { get; set; }
    public string faculty_curriculum_name_en { get; set; }
    public string education_type_code { get; set; }
    public string education_type_name_th { get; set; }
    public string education_type_name_en { get; set; }
    public string academic_year_code { get; set; }
    public string academic_year_name_th { get; set; }
    public string academic_year_name_en { get; set; }
    public string academic_semester_code { get; set; }
    public string academic_semester_name_th { get; set; }
    public string academic_semester_name_en { get; set; }
    public int? total_question_sets { get; set; }
    public Guid? college_faculty_uid { get; set; }
    public Guid? question_set_uid { get; set; }
    [NotMapped]
    public string search { get; set; }
  }
}
