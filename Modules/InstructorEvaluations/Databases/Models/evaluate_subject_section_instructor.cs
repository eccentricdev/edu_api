using System;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;
using SeventyOneDev.Utilities;

namespace edu_api.Modules.InstructorEvaluations.Databases.Models
{
  public class evaluate_subject_section_instructor :base_table
  {
    [Key]
    public Guid? evaluate_subject_section_instructor_uid {get;set;}
    public Guid? evaluate_subject_section_uid { get; set; }
    public string instructor_code { get; set; }
  }

  public class t_evaluate_subject_section_instructor : evaluate_subject_section_instructor
  {
    [JsonIgnore]
    public t_evaluate_subject_section evaluate_subject_section { get; set; }
  }

  public class v_evaluate_subject_section_instructor : evaluate_subject_section_instructor
  {
    public string display_name_th { get; set; }
    public string display_name_en { get; set; }
    
    [JsonIgnore]
    public v_evaluate_subject_section evaluate_subject_section { get; set; }

  }
}
