using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;
using SeventyOneDev.Utilities;
using SeventyOneDev.Utilities.Attributes;

namespace edu_api.Modules.InstructorEvaluations.Databases.Models
{
    public class question_set:base_table
      {
          [Key]
          public Guid? question_set_uid {get;set;}
          public Guid? question_group_uid { get; set; }
          [Unique]
          public string question_set_code {get;set;}
          [Unique]
          public string question_set_name_th {get;set;}
          [Unique]
          public string question_set_name_en {get;set;}
          public Guid? faculty_uid { get; set; }
          public int? total_questions { get; set; }
      }

      public class t_question_set : question_set
      {
        [Include]
        public List<t_question> questions { get; set; }
        [JsonIgnore]
        public List<t_question_set_faculty> question_set_faculties { get; set; }
        [JsonIgnore]
        public List<t_question_set_curriculum> question_set_curriculums { get; set; }
        [JsonIgnore]
        public List<t_question_set_subject> question_set_subjects { get; set; }
        [JsonIgnore]
        public t_question_group question_group { get; set; }

      }
      public class v_question_set:question_set
      {
          public string status_code { get; set; }
          public string status_name_th { get; set; }
          public string status_name_en { get; set; }
          public string question_group_code { get; set; }
          public string question_group_name_th { get; set; }
          public string question_group_name_en { get; set; }
          public string faculty_code { get; set; }
          public string faculty_name_th { get; set; }
          public string faculty_name_en { get; set; }
          [Include]
          public List<v_question> questions { get; set; }

          [JsonIgnore]
          public List<v_question_set_faculty> question_set_faculties { get; set; }
          [JsonIgnore]
          public List<v_question_set_curriculum> question_set_curriculums { get; set; }
          [JsonIgnore]
          public List<v_question_set_subject> question_set_subjects { get; set; }
          [JsonIgnore]
          public v_question_group question_group { get; set; }
      }
}
