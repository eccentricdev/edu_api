using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using SeventyOneDev.Utilities;
using SeventyOneDev.Utilities.Attributes;

namespace edu_api.Modules.InstructorEvaluations.Databases.Models
{
  [GeneratedUidController("api/instructor_evaluations/faculty_evaluation")]
  public class v_faculty_evaluation:base_table
  {
    [Key]
    public Guid? college_faculty_uid { get; set; }
    public string academic_year_code { get; set; }
    public string academic_year_name_th { get; set; }
    public string academic_year_name_en { get; set; }
    public string academic_semester_code { get; set; }
    public string academic_semester_name_th { get; set; }
    public string academic_semester_name_en { get; set; }
    public string college_faculty_code { get; set; }
    public string college_faculty_name_th { get; set; }
    public string college_faculty_name_en { get; set; }
    // ใน major faculty เป็น id
    public int? total_curriculums { get; set; }
    public int? total_question_sets { get; set; }
    [NotMapped]
    public string search { get; set; }
  }
}
