using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using SeventyOneDev.Utilities;
using SeventyOneDev.Utilities.Attributes;

namespace edu_api.Modules.InstructorEvaluations.Databases.Models
{
  public class evaluate_subject_section:base_table
  {
    [Key]
    public Guid? evaluate_subject_section_uid {get;set;}
    //public int? question_set_faculty_subject_id { get; set; }
    public Guid? subject_year_uid {get;set;}
    public Guid? academic_year_uid {get;set;}
    public Guid? academic_semester_uid {get;set;}
    public Guid? study_subject_section_uid {get;set;}
    public short? no_of_instructors { get; set; }

  }

  [GeneratedUidController("api/instructor_evaluations/evaluate_subject_section")]
  public class t_evaluate_subject_section : evaluate_subject_section
  {
    [Include]
    public List<t_evaluate_subject_section_instructor> evaluate_subject_section_instructors { get; set; }
  }

  public class v_evaluate_subject_section : evaluate_subject_section
  {
    public string section_code { get; set; }
    public short? section_type_id {get;set;}
    public string section_type_code { get; set; }
    public string section_type_name_th { get; set; }
    public string section_type_name_en { get; set; }
    public short? quota  { get; set; }
    public short? confirmed  { get; set; }
    
    [Include]
    public List<v_evaluate_subject_section_instructor> evaluate_subject_section_instructors { get; set; }

  }
}
