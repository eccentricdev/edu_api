using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;
using SeventyOneDev.Utilities;
using SeventyOneDev.Utilities.Attributes;

namespace edu_api.Modules.InstructorEvaluations.Databases.Models
{
      public class question_score_type:base_table
      {
          [Key]
          public Guid? question_score_type_uid {get;set;}
          public string question_score_type_code {get;set;}
          public string question_score_type_name_th {get;set;}
          public string question_score_type_name_en {get;set;}

      }
      
      [GeneratedUidController("api/instructor_evaluations/question_score_type")]
      public class t_question_score_type : question_score_type
      {
        // [JsonIgnore]
        // public List<t_question> questions { get; set; }
        
        public t_question_score_type()
        {
        }

        public t_question_score_type(Guid? questionScoreTypeId, string questionScoreTypeCode, string questionScoreTypeNameEn,
          string questionScoreTypeNameTh)
        {
          this.question_score_type_uid = questionScoreTypeId;
          this.question_score_type_code = questionScoreTypeCode;
          this.question_score_type_name_en = questionScoreTypeNameEn;
          this.question_score_type_name_th = questionScoreTypeNameTh;
          this.status_id = 1;
          this.created_by = "SYSTEM";
          this.created_datetime = DateTime.Now;
          this.updated_by = "SYSTEM";
          this.updated_datetime = DateTime.Now;
        }
      }
      
      public class v_question_score_type:question_score_type
      {
          // [JsonIgnore]
          // public List<v_question> questions { get; set; }
        

      }
}
