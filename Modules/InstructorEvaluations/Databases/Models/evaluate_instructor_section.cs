using System;

namespace edu_api.Modules.InstructorEvaluations.Databases.Models
{
  public class v_evaluate_instructor_section
  {
    public string instructor_code {get;set;}
    public string display_name_th {get;set;}
    public string display_name_en {get;set;}
    public Guid? subject_year_uid {get;set;}
    public Guid? study_subject_section_uid {get;set;}
    public Guid? academic_year_uid {get;set;}
    public Guid? academic_semester_uid {get;set;}
  }
}
