using System;

namespace edu_api.Modules.InstructorEvaluations.Databases.Models
{
  public class v_faculty_register_subject
  {
    public Guid? final_register_uid { get; set; }
    public Guid? academic_year_uid { get; set; }
    public Guid? academic_semester_uid { get; set; }
    public string student_code { get; set; }
    public Guid? subject_year_uid { get; set; }
    public Guid? study_subject_section_uid { get; set; }
    public Guid? faculty_uid { get; set; }
    public string section_code { get; set; }
  }
}
