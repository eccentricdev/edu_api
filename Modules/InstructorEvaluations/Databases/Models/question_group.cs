using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;
using SeventyOneDev.Utilities;
using SeventyOneDev.Utilities.Attributes;

namespace edu_api.Modules.InstructorEvaluations.Databases.Models
{
      public class question_group:base_table
      {
          [Key]
          public Guid? question_group_uid {get;set;}
          public string question_group_code {get;set;}
          public string question_group_name_th {get;set;}
          public string question_group_name_en {get;set;}
          public short? section_type_id { get; set; }


      }
      
      [GeneratedUidController("api/instructor_evaluations/question_group")]
      public class t_question_group : question_group
      {
        [JsonIgnore]
        public List<t_question_set> question_sets { get; set; }
        
        public t_question_group()
        {
        }

        public t_question_group(Guid? questionGroupId, string questionGroupCode, string questionGroupNameEn,
          string questionGroupNameTh)
        {
          this.question_group_uid = questionGroupId;
          this.question_group_code = questionGroupCode;
          this.question_group_name_en = questionGroupNameEn;
          this.question_group_name_th = questionGroupNameTh;
          this.status_id = 1;
          this.created_by = "SYSTEM";
          this.created_datetime = DateTime.Now;
          this.updated_by = "SYSTEM";
          this.updated_datetime = DateTime.Now;
        }
      }
      public class v_question_group: question_group
      {

          public string section_type_code { get; set; }
          public string section_type_name_th { get; set; }
          public string section_type_name_en { get; set; }
          [JsonIgnore]
          public List<v_question_set> question_sets { get; set; }
          

      }
}
