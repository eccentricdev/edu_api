using System;
using System.ComponentModel.DataAnnotations;
using SeventyOneDev.Utilities;
using SeventyOneDev.Utilities.Attributes;

namespace edu_api.Modules.InstructorEvaluations.Databases.Models
{
  public class evaluation_calendar_assignment:base_table
  {
    [Key]
    public Guid? evaluation_calendar_assignment_uid { get; set; }
    public Guid? evaluation_calendar_uid { get; set; }
    public Guid? college_faculty_uid { get; set; }
    public Guid? faculty_curriculum_uid { get; set; }
  }
  [GeneratedUidController("api/instructor_evaluations/evaluation_calendar_assignment")]
  public class t_evaluation_calendar_assignment : evaluation_calendar_assignment
  {

  }

  public class v_evaluation_calendar_assignment : evaluation_calendar_assignment
  {
    public string college_faculty_code { get; set; }
    public string college_faculty_name_th { get; set; }
    public string college_faculty_name_en { get; set; }
    public string faculty_curriculum_code { get; set; }
    public string faculty_curriculum_name_th { get; set; }
    public string faculty_curriculum_name_en { get; set; }
    public string evaluation_calendar_code { get; set; }
    public string evaluation_calendar_name_th { get; set; }
    public string evaluation_calendar_name_en { get; set; }

  }
}
