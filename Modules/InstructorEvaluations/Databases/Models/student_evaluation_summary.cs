using System;

namespace edu_api.Modules.InstructorEvaluations.Databases.Models
{
  public class v_student_evaluation_summary
  {
    public Guid? academic_year_uid { get; set; }
    public Guid? academic_semester_uid { get; set; }
    public Guid? year_subject_uid { get; set; }
    public Guid? year_subject_section_uid { get; set; }
    public Guid? question_set_faculty_uid { get; set; }
    public Guid? question_set_curriculum_uid { get; set; }
    public string year_subject_code { get; set; }
    public string year_subject_name_th { get; set; }
    public string year_subject_name_en { get; set; }
    public string section_code { get; set; }
    public string study_type_name_th { get; set; }
    public string study_type_name_en { get; set; }
    public Guid? college_faculty_uid { get; set; }
    public Guid? faculty_curriculum_uid { get; set; }
    public int? total_students { get; set; }
    public int? total_evaluates { get; set; }
  }
}
