using System;

namespace edu_api.Modules.InstructorEvaluations.Databases.Models
{
  public class v_student_evaluation_calendar_faculty
  {
    public Guid? academic_year_uid {get;set;}
    public Guid? academic_semester_uid {get;set;}
    public Guid? education_type_uid {get;set;}
    public Guid? college_faculty_uid {get;set;}
    public string evaluation_calendar_item_code {get;set;}
    public string evaluation_calendar_item_name_th {get;set;}
    public string evaluation_calendar_item_name_en {get;set;}
    public DateTime? start_date {get;set;}
    public DateTime? end_date {get;set;}
    public Guid? student_uid {get;set;}
    public Guid? year_subject_uid { get; set; }
    public string year_subject_code {get;set;}
    public string year_subject_name_th {get;set;}
    public string year_subject_name_en {get;set;}
    public Guid? year_subject_section_uid { get; set; }
    public string section_code {get;set;}
    public DateTime? primary_instructor_name_th {get;set;}
    public DateTime? primary_instructor_name_en { get; set; }
  }
}
