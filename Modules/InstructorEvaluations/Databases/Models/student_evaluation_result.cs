using System;

namespace edu_api.Modules.InstructorEvaluations.Databases.Models
{
  public class student_evaluation_result
  {
    public Guid? academic_year_uid {get;set;}
    public Guid? academic_semester_uid {get;set;}
    public Guid? year_subject_uid {get;set;}
    public Guid? year_subject_section_uid {get;set;}
    public Guid? question_set_uid {get;set;}
    public Guid? question_uid {get;set;}
    public int? selected_choice {get;set;}
    public string text_answer {get;set;}
  }
}
