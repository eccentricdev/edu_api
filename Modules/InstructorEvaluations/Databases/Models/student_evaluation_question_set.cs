using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;
using SeventyOneDev.Utilities;
using SeventyOneDev.Utilities.Attributes;

namespace edu_api.Modules.InstructorEvaluations.Databases.Models
{
  public class student_evaluation_question_set : base_table
  {
    [Key]
    public Guid? student_evaluation_question_set_uid { get; set; }
    public Guid? student_evaluation_uid { get; set; }
    public Guid? question_set_faculty_uid { get; set; }
    public Guid? question_set_curriculum_uid { get; set; }
    public Guid? question_set_uid { get; set; }
  }
  //[GeneratedUidController("api/instructor_evaluations/student_evaluation_question_set")]
  public class t_student_evaluation_question_set : student_evaluation_question_set
  {
    [JsonIgnore]
    public t_student_evaluation student_evaluation { get; set; }

    [Include]
    public List<t_student_evaluation_question_result> student_evaluation_question_results { get; set; }

    public t_student_evaluation_question_set()
    {

    }

    public t_student_evaluation_question_set(question_set_curriculum questionSetCurriculum)
    {
      question_set_faculty_uid = questionSetCurriculum.college_faculty_uid;
      question_set_curriculum_uid = questionSetCurriculum.faculty_curriculum_uid;
      question_set_uid = questionSetCurriculum.question_set_uid;

    }
    public t_student_evaluation_question_set(question_set_faculty questionSetCurriculumFaculty)
    {
      question_set_faculty_uid = questionSetCurriculumFaculty.college_faculty_uid;
      //question_set_curriculum_uid = questionSetCurriculumFaculty.faculty_curriculum_uid;
      question_set_uid = questionSetCurriculumFaculty.question_set_uid;

    }
  }

  public class v_student_evaluation_question_set  : student_evaluation_question_set
  {
    public string question_set_name_th { get; set; }
    public string question_set_name_en { get; set; }
    public string question_group_name_th { get; set; }
    //public string question_set_no { get; set; }

    [JsonIgnore]
    public v_student_evaluation student_evaluation { get; set; }

    [Include]
    public List<v_student_evaluation_question_result> student_evaluation_question_results { get; set; }
  }
}
