using System;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;
using SeventyOneDev.Utilities;

namespace edu_api.Modules.InstructorEvaluations.Databases.Models
{
  public class evaluation_calendar_schedule:base_table
  {
    [Key]
    public Guid? evaluation_calendar_schedule_uid { get; set; }
    public Guid? evaluation_calendar_uid { get; set; }
    public Guid? evaluation_calendar_item_uid { get; set; }
    public DateTime? start_date { get; set; }
    public DateTime? end_date { get; set; }


  }

  public class t_evaluation_calendar_schedule : evaluation_calendar_schedule
  {
    [JsonIgnore]
    public t_evaluation_calendar evaluation_calendar { get; set; }
    [JsonIgnore]
    public t_evaluation_calendar_item evaluation_calendar_item { get; set; }
  }
  public class v_evaluation_calendar_schedule : evaluation_calendar_schedule
  {

    public string evaluation_calendar_item_code { get; set; }
    public string evaluation_calendar_item_name_th { get; set; }
    public string evaluation_calendar_item_name_en { get; set; }
    [JsonIgnore]
    public v_evaluation_calendar evaluation_calendar { get; set; }
    [JsonIgnore]
    public v_evaluation_calendar_item evaluation_calendar_item { get; set; }
  }
}
