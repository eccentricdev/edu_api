using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;
using SeventyOneDev.Utilities;
using SeventyOneDev.Utilities.Attributes;

namespace edu_api.Modules.InstructorEvaluations.Databases.Models
{
    public class answer_type : base_table
    {
        [Key] 
        public Guid? answer_type_uid { get; set; }
        public string answer_type_code { get; set; }
        public string answer_type_name_th { get; set; }
        public string answer_type_name_en { get; set; }
        // public string answer_type_short_name_th {get;set;}
        // public string answer_type_short_name_en {get;set;}
        
    }
    
    [GeneratedUidController("api/instructor_evaluations/answer_type")]
    public class t_answer_type:answer_type
    { 
        [JsonIgnore]
        public List<t_question> questions { get; set; }
        
        public t_answer_type()
        {
            
        }

        public t_answer_type(Guid? answerTypeId, string answerTypeCode, string answerTypeNameEn,
          string answerTypeNameTh)
        {
          this.answer_type_uid = answerTypeId;
          this.answer_type_code = answerTypeCode;
          this.answer_type_name_en = answerTypeNameEn;
          this.answer_type_name_th = answerTypeNameTh;
          this.status_id = 1;
          this.created_by = "SYSTEM";
          this.created_datetime = DateTime.Now;
          this.updated_by = "SYSTEM";
          this.updated_datetime = DateTime.Now;
        }
        
      }
    
    public class v_answer_type:answer_type
      {
          [JsonIgnore]
          public List<v_question> questions { get; set; }

      }
}
