using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;
using SeventyOneDev.Utilities;
using SeventyOneDev.Utilities.Attributes;

namespace edu_api.Modules.InstructorEvaluations.Databases.Models
{
    public class question:base_table
      {
          [Key]
          public Guid? question_uid {get;set;}
          public Guid? question_set_uid { get; set; }
          //public int? question_set_faculty_id { get; set; }
          //public int? question_set_faculty_subject_id { get; set; }
          public Guid? answer_type_uid { get; set; }
          public int? question_no { get; set; }
          public string question_name_th {get;set;}
          public string question_name_en {get;set;}
          public bool? is_calculate { get; set; }
      }
    
    
      [GeneratedUidController("api/instructor_evaluations/question")]
      public class t_question : question
      {
        [JsonIgnore]
        public t_question_set question_set { get; set; }
        
        [JsonIgnore]
        public t_answer_type answer_type { get; set; }

        // [JsonIgnore]
        // public t_question_score_type question_score_type { get; set; }
      }
      public class v_question:question
      {
          // public string question_score_type_code { get; set; }
          // public string question_score_type_name_th { get; set; }
          // public string question_score_type_name_en { get; set; }
          public string answer_type_code { get; set; }
          public string answer_type_name_th { get; set; }
          public string answer_type_name_en { get; set; }
          
          [JsonIgnore]
          public v_question_set question_set { get; set; }
          [JsonIgnore]
          public v_answer_type answer_type { get; set; }
          // [JsonIgnore]
          // public v_question_score_type question_score_type { get; set; }
          
      }
}
