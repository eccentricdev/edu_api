using System;
using System.ComponentModel.DataAnnotations;
using SeventyOneDev.Utilities;
using SeventyOneDev.Utilities.Attributes;

namespace edu_api.Modules.InstructorEvaluations.Databases.Models
{
      public class student_evaluation_status:base_table
      {
          [Key]
          public Guid? student_evaluation_status_uid {get;set;}
          public string student_evaluation_status_code {get;set;}
          public string student_evaluation_status_name_th {get;set;}
          public string student_evaluation_status_name_en {get;set;}

      }
      
      [GeneratedUidController("api/instructor_evaluations/student_evaluation_status")]
      public class t_student_evaluation_status : student_evaluation_status
      {
        public t_student_evaluation_status()
        {
        }

        public t_student_evaluation_status(Guid? studentEvaluationStatusId, string studentEvaluationStatusCode, string studentEvaluationStatusNameEn,
          string studentEvaluationStatusNameTh)
        {
          this.student_evaluation_status_uid = studentEvaluationStatusId;
          this.student_evaluation_status_code = studentEvaluationStatusCode;
          this.student_evaluation_status_name_en = studentEvaluationStatusNameEn;
          this.student_evaluation_status_name_th = studentEvaluationStatusNameTh;
          this.status_id = 1;
          this.created_by = "SYSTEM";
          this.created_datetime = DateTime.Now;
          this.updated_by = "SYSTEM";
          this.updated_datetime = DateTime.Now;
        }
      }
      
      
      public class v_student_evaluation_status:student_evaluation_status
      {


      }
}
