using System;

namespace edu_api.Modules.InstructorEvaluations.Databases.Models
{
  public class v_student_evaluation_subject_result
  {
    public Guid? study_subject_section_uid {get;set;}
    public Guid? question_set_uid {get;set;}
    public string question_set_name_th {get;set;}
    public Guid? question_uid {get;set;}
    public string question_name_th {get;set;}
    public Guid? answer_type_uid { get; set; }
    public short? selected_choice {get;set;}
    public string text_answer {get;set;}
    public string instructor_code { get; set; }
  }
}
