using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;
using SeventyOneDev.Utilities;
using SeventyOneDev.Utilities.Attributes;

namespace edu_api.Modules.InstructorEvaluations.Databases.Models
{
  public class evaluation_calendar_item : base_table
  {
    [Key]
    public Guid? evaluation_calendar_item_uid { get; set; }
    public string evaluation_calendar_item_code { get; set; }
    public string evaluation_calendar_item_name_th { get; set; }
    public string evaluation_calendar_item_name_en { get; set; }
    public string evaluation_calendar_item_short_name_th { get; set; }
    public string evaluation_calendar_item_short_name_en { get; set; }
    public int? row_order { get; set; }



  }
  [GeneratedUidController("api/instructor_evaluations/evaluation_calendar_item")]
  public class t_evaluation_calendar_item : evaluation_calendar_item
  {
    public t_evaluation_calendar_item()
    {
    }

    public t_evaluation_calendar_item(Guid? evaluationCalendarItemId, string evaluationCalendarItemCode,
      string evaluationCalendarItemNameEn,
      string evaluationCalendarItemNameTh)
    {
      this.evaluation_calendar_item_uid = evaluationCalendarItemId;
      this.evaluation_calendar_item_code = evaluationCalendarItemCode;
      this.evaluation_calendar_item_name_en = evaluationCalendarItemNameEn;
      this.evaluation_calendar_item_name_th = evaluationCalendarItemNameTh;
      this.created_by = "SYSTEM";
      this.created_datetime = DateTime.Now;
      this.updated_by = "SYSTEM";
      this.updated_datetime = DateTime.Now;
    }
    [JsonIgnore]
    public List<t_evaluation_calendar_schedule> evaluation_calendar_schedules { get; set; }
  }

  public class v_evaluation_calendar_item : evaluation_calendar_item
  {
    [JsonIgnore]
    public List<v_evaluation_calendar_schedule> evaluation_calendar_schedules { get; set; }

  }
}
