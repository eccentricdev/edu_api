using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;
using SeventyOneDev.Utilities;
using SeventyOneDev.Utilities.Attributes;

namespace edu_api.Modules.InstructorEvaluations.Databases.Models
{
  public class evaluation_calendar:base_table
  {
    [Key]
    public Guid? evaluation_calendar_uid { get; set; }
    public Guid? academic_year_uid {get;set;}
    public Guid? academic_semester_uid {get;set;}
    public Guid? education_type_uid { get; set; }
    public string evaluation_calendar_code { get; set; }
    public string evaluation_calendar_name_th { get; set; }
    public string evaluation_calendar_name_en { get; set; }
  }
  [GeneratedUidController("api/instructor_evaluations/evaluation_calendar")]
  public class t_evaluation_calendar : evaluation_calendar
  {
    [Include]
    public List<t_evaluation_calendar_schedule> evaluation_calendar_schedules { get; set; }
    [JsonIgnore]
    public List<t_evaluation_calendar_faculty> evaluation_calendar_facultys { get; set; }
  }

  public class v_evaluation_calendar : evaluation_calendar
  {
    public string academic_year_code { get; set; }
    public string academic_year_name_th { get; set; }
    public string academic_year_name_en { get; set; }
    public string academic_semester_code { get; set; }
    public string academic_semester_name_th { get; set; }
    public string academic_semester_name_en { get; set; }
    public string education_type_code { get; set; }
    public string education_type_name_th { get; set; }
    public string education_type_name_en { get; set; }
    [Include]
    public List<v_evaluation_calendar_schedule> evaluation_calendar_schedules { get; set; }
    [JsonIgnore]
    public List<v_evaluation_calendar_faculty> evaluation_calendar_facultys { get; set; }
  }



}
