using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;
using SeventyOneDev.Utilities;
using SeventyOneDev.Utilities.Attributes;

namespace edu_api.Modules.InstructorEvaluations.Databases.Models
{
  public class evaluation_calendar_faculty_curriculum:base_table
  {
    [Key]
    public Guid? evaluation_calendar_faculty_curriculum_uid { get; set; }
    public Guid? evaluation_calendar_faculty_uid { get; set; }
    public Guid? faculty_curriculum_uid { get; set; }
    public bool? is_selected { get; set; }
  }
  //[GeneratedUidController("api/instructor_evaluations/evaluation_calendar_faculty_curriculum")]
  public class t_evaluation_calendar_faculty_curriculum : evaluation_calendar_faculty_curriculum
  {
    [JsonIgnore]
    public t_evaluation_calendar_faculty evaluation_calendar_faculty { get; set; }
  }

  public class v_evaluation_calendar_faculty_curriculum : evaluation_calendar_faculty_curriculum
  {
    public string faculty_curriculum_code { get; set; }
    public string faculty_curriculum_name_th { get; set; }
    public string faculty_curriculum_name_en { get; set; }
    public string college_faculty_code { get; set; }
    public string college_faculty_name_th { get; set; }
    public string college_faculty_name_en { get; set; }
    public string education_type_code { get; set; }
    public string education_type_name_th { get; set; }
    public string education_type_name_en { get; set; }
    public string academic_year_code { get; set; }
    public string academic_year_name_th { get; set; }
    public string academic_year_name_en { get; set; }
    public string semester_code { get; set; }
    public string semester_name_th { get; set; }
    public string semester_name_en { get; set; }
    public Guid? evaluation_calendar_uid { get; set; }

    [JsonIgnore]
    public v_evaluation_calendar_faculty evaluation_calendar_faculty { get; set; }
    [NotMapped]
    public List<v_evaluation_calendar_schedule> evaluation_calendar_schedules { get; set; }
  }
}
