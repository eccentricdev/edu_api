using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using SeventyOneDev.Utilities;
using SeventyOneDev.Utilities.Attributes;

namespace edu_api.Modules.InstructorEvaluations.Databases.Models
{
      public class score_calculation_type:base_table
      {
          [Key]
          public int? score_calculation_type_id {get;set;}
          public string score_calculation_type_code {get;set;}
          public string score_calculation_type_name_th {get;set;}
          public string score_calculation_type_name_en {get;set;}
      }
      
      
      [GeneratedIdController("api/instructor_evaluations/score_calculation_type")]
      public class t_score_calculation_type : score_calculation_type
      {
        public t_score_calculation_type()
        {
        }

        public t_score_calculation_type(short? scoreCalculationTypeId, string scoreCalculationTypeCode, string scoreCalculationTypeNameEn,
          string scoreCalculationTypeNameTh)
        {
          this.score_calculation_type_id = scoreCalculationTypeId;
          this.score_calculation_type_code = scoreCalculationTypeCode;
          this.score_calculation_type_name_en = scoreCalculationTypeNameEn;
          this.score_calculation_type_name_th = scoreCalculationTypeNameTh;
          this.status_id = 1;
          this.created_by = "SYSTEM";
          this.created_datetime = DateTime.Now;
          this.updated_by = "SYSTEM";
          this.updated_datetime = DateTime.Now;
        }
      }
      
      public class v_score_calculation_type:score_calculation_type
      {
        

      }
}
