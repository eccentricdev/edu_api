using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;
using SeventyOneDev.Utilities;
using SeventyOneDev.Utilities.Attributes;

namespace edu_api.Modules.InstructorEvaluations.Databases.Models
{
  public class question_set_subject:base_table
  {
    [Key]
    public Guid? question_set_subject_uid {get;set;}
    public Guid? question_set_uid { get; set; }
    public Guid? question_set_curriculum_uid { get; set; }
    //public Guid? question_set_faculty_uid { get; set; }
    public Guid? academic_year_uid {get;set;}
    public Guid? academic_semester_uid {get;set;}
    public Guid? year_subject_uid { get; set; }
    public int? score_calculation_type_id { get; set; }
    //public DateTime? update_datetime {get;set;}
    //public List<t_question> questions { get; set; }


    //public List<t_question_set_faculty_subject_section> question_set_faculty_subject_sections { get; set; }
  }
  
  [GeneratedUidController("api/instructor_evaluations/question_set_subject")]
  public class t_question_set_subject : question_set_subject
  {
    [JsonIgnore]
    public t_question_set question_set { get; set; }
  }
  
  public class v_question_set_subject:question_set_subject
  {
    public string academic_year_code { get; set; }
    public string academic_year_name_th { get; set; }
    public string academic_year_name_en { get; set; }

    public string academic_semester_code { get; set; }
    public string academic_semester_name_th { get; set; }
    public string academic_semester_name_en { get; set; }

    public string year_subject_code { get; set; }
    public string year_subject_name_th { get; set; }
    public string year_subject_name_en { get; set; }
    
    
    
    public Guid? question_set_faculty_uid { get; set; }

    [JsonIgnore]
    public v_question_set question_set { get; set; }
   

  }
}
