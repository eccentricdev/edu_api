using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using SeventyOneDev.Utilities;
using SeventyOneDev.Utilities.Attributes;

namespace edu_api.Modules.InstructorEvaluations.Databases.Models
{
  public class evaluation_calendar_faculty:base_table
  {
    [Key]
    public Guid? evaluation_calendar_faculty_uid { get; set; }
    public Guid? evaluation_calendar_uid { get; set; }
    public Guid? college_faculty_uid { get; set; }
    public bool? is_selected { get; set; }
  }


  [GeneratedUidController("api/instructor_evaluations/evaluation_calendar_faculty")]
  public class t_evaluation_calendar_faculty : evaluation_calendar_faculty
  {
    [JsonIgnore]
    public t_evaluation_calendar evaluation_calendar { get; set; }
    [Include]
    public List<t_evaluation_calendar_faculty_curriculum> evaluation_calendar_faculty_curriculums { get; set; }
  }

  public class v_evaluation_calendar_faculty : evaluation_calendar_faculty
  {
    public string college_faculty_code { get; set; }
    public string college_faculty_name_th { get; set; }
    public string college_faculty_name_en { get; set; }
    [JsonIgnore]
    public v_evaluation_calendar evaluation_calendar { get; set; }
    [Include]
    public List<v_evaluation_calendar_faculty_curriculum> evaluation_calendar_faculty_curriculums { get; set; }
  }
}
