using System;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;
using SeventyOneDev.Utilities;
using SeventyOneDev.Utilities.Attributes;

namespace edu_api.Modules.InstructorEvaluations.Databases.Models
{
      public class question_set_faculty:base_table
      {
          [Key]
          public Guid? question_set_faculty_uid {get;set;}
          public Guid? question_set_uid { get; set; }
          public Guid? academic_year_uid {get;set;}
          public Guid? academic_semester_uid {get;set;}
          public Guid? college_faculty_uid { get; set; }
          public int? score_calculation_type_id { get; set; }
      }

      //[GeneratedUidController("api/instructor_evaluations/question_set_faculty")]
      public class t_question_set_faculty : question_set_faculty
      {
        //public List<t_question_set_faculty_subject> question_set_faculty_subjects { get; set; }
        [JsonIgnore]
        public t_question_set question_set { get; set; }
      }

      public class v_question_set_faculty:question_set_faculty
      {

          public string academic_year_code { get; set; }
          public string academic_year_name_th { get; set; }
          public string academic_year_name_en { get; set; }

          public string academic_semester_code { get; set; }
          public string academic_semester_name_th { get; set; }
          public string academic_semester_name_en { get; set; }

          public string college_faculty_code { get; set; }
          public string college_faculty_name_th { get; set; }
          public string college_faculty_name_en { get; set; }
          //public int? total_curriculums { get; set; }
          //public int? total_sets { get; set; }
          [JsonIgnore]
          public v_question_set question_set { get; set; }


      }
}
