using edu_api.Modules.InstructorEvaluations.Databases.Models;
using Microsoft.EntityFrameworkCore;

namespace SeventyOneDev.Utilities
{
  public partial class Db : DbContext
  {

    public DbSet<t_answer_type> t_answer_type { get; set; }
    public DbSet<v_answer_type> v_answer_type { get; set; }
    public DbSet<t_question> t_question { get; set; }
    public DbSet<v_question> v_question { get; set; }
    public DbSet<t_question_group> t_question_group { get; set; }
    public DbSet<v_question_group> v_question_group { get; set; }
    public DbSet<t_question_set> t_question_set { get; set; }
    public DbSet<v_question_set> v_question_set { get; set; }
    public DbSet<t_question_score_type> t_question_score_type { get; set; }
    public DbSet<v_question_score_type> v_question_score_type { get; set; }
    public DbSet<t_question_set_faculty> t_question_set_faculty { get; set; }
    public DbSet<v_question_set_faculty> v_question_set_faculty { get; set; }
    public DbSet<t_question_set_curriculum> t_question_set_curriculum { get; set; }
    public DbSet<v_question_set_curriculum> v_question_set_curriculum { get; set; }
    public DbSet<t_question_set_subject> t_question_set_subject { get; set; }
    public DbSet<v_question_set_subject> v_question_set_subject { get; set; }
    public DbSet<t_evaluation_calendar> t_evaluation_calendar { get; set; }
    public DbSet<v_evaluation_calendar> v_evaluation_calendar { get; set; }
    public DbSet<t_evaluation_calendar_item> t_evaluation_calendar_item { get; set; }
    public DbSet<v_evaluation_calendar_item> v_evaluation_calendar_item { get; set; }
    public DbSet<t_evaluation_calendar_schedule> t_evaluation_calendar_schedule { get; set; }
    public DbSet<v_evaluation_calendar_schedule> v_evaluation_calendar_schedule { get; set; }
    public DbSet<t_evaluation_calendar_faculty> t_evaluation_calendar_faculty { get; set; }
    public DbSet<v_evaluation_calendar_faculty> v_evaluation_calendar_faculty { get; set; }
    public DbSet<t_evaluation_calendar_faculty_curriculum> t_evaluation_calendar_faculty_curriculum { get; set; }
    public DbSet<v_evaluation_calendar_faculty_curriculum> v_evaluation_calendar_faculty_curriculum { get; set; }
    public DbSet<v_faculty_evaluation> v_faculty_evaluation { get; set; }
    public DbSet<v_curriculum_evaluation> v_curriculum_evaluation { get; set; }
    public DbSet<t_score_calculation_type> t_score_calculation_type { get; set; }
    public DbSet<v_score_calculation_type> v_score_calculation_type { get; set; }
    public DbSet<t_student_evaluation_status> t_student_evaluation_status { get; set; }
    public DbSet<v_student_evaluation_status> v_student_evaluation_status { get; set; }
    public DbSet<t_evaluate_subject_section> t_evaluate_subject_section { get; set; }
    public DbSet<v_evaluate_subject_section> v_evaluate_subject_section { get; set; }
    public DbSet<t_evaluate_subject_section_instructor> t_evaluate_subject_section_instructor { get; set; }
    public DbSet<v_evaluate_subject_section_instructor> v_evaluate_subject_section_instructor { get; set; }
    public DbSet<v_student_evaluation_subject> v_student_evaluation_subject { get; set; }
    public DbSet<v_evaluate_instructor_section> v_evaluate_instructor_section { get; set; }
    public DbSet<v_student_evaluation_subject_result> v_student_evaluation_subject_result { get; set; }

    public DbSet<t_evaluation_calendar_assignment> t_evaluation_calendar_assignment { get; set; }
    public DbSet<v_evaluation_calendar_assignment> v_evaluation_calendar_assignment { get; set; }

    public DbSet<t_student_evaluation> t_student_evaluation { get; set; }
    public DbSet<v_student_evaluation> v_student_evaluation { get; set; }
    public DbSet<t_student_evaluation_question_set> t_student_evaluation_question_set { get; set; }
    public DbSet<v_student_evaluation_question_set> v_student_evaluation_question_set { get; set; }
    public DbSet<t_student_evaluation_question_result> t_student_evaluation_question_result { get; set; }
    public DbSet<v_student_evaluation_question_result> v_student_evaluation_question_result { get; set; }

    public DbSet<v_faculty_register_subject> v_faculty_register_subject { get; set; }
    public DbSet<v_student_evaluation_calendar> v_student_evaluation_calendar { get; set; }
    public DbSet<v_student_evaluation_calendar_faculty> v_student_evaluation_calendar_faculty { get; set; }
    public DbSet<v_student_evaluation_summary> v_student_evaluation_summary { get; set; }
    private void OnInstructorEvaluationsModelCreating(ModelBuilder builder, string schema)
    {
      builder.Entity<t_answer_type>().ToTable("answer_type", schema);
      builder.Entity<v_answer_type>().ToView("v_answer_type", schema);

      builder.Entity<t_question>().ToTable("question", schema);
      builder.Entity<t_question>().HasOne(c => c.answer_type).WithMany(c => c.questions)
        .HasForeignKey(c => c.answer_type_uid).OnDelete(DeleteBehavior.SetNull);
      // builder.Entity<t_question>().HasOne(c => c.question_score_type).WithMany(c => c.questions)
      //   .HasForeignKey(c => c.question_score_type_uid).OnDelete(DeleteBehavior.SetNull);
      builder.Entity<t_question>().HasOne(c => c.question_set).WithMany(c => c.questions)
        .HasForeignKey(c => c.question_set_uid).OnDelete(DeleteBehavior.Cascade);
      builder.Entity<v_question>().ToView("v_question", schema);
      builder.Entity<v_question>().HasOne(c => c.answer_type).WithMany(c => c.questions)
        .HasForeignKey(c => c.answer_type_uid).OnDelete(DeleteBehavior.SetNull);
      // builder.Entity<v_question>().HasOne(c => c.question_score_type).WithMany(c => c.questions)
      //   .HasForeignKey(c => c.question_score_type_uid).OnDelete(DeleteBehavior.SetNull);
      builder.Entity<v_question>().HasOne(c => c.question_set).WithMany(c => c.questions)
        .HasForeignKey(c => c.question_set_uid).OnDelete(DeleteBehavior.Cascade);

      builder.Entity<t_question_group>().ToTable("question_group", schema);
      builder.Entity<v_question_group>().ToView("v_question_group", schema);

      builder.Entity<t_question_set>().ToTable("question_set", schema);
      builder.Entity<t_question_set>().HasOne(c => c.question_group).WithMany(c => c.question_sets)
        .HasForeignKey(c => c.question_group_uid).OnDelete(DeleteBehavior.SetNull);
      builder.Entity<v_question_set>().ToView("v_question_set", schema);
      builder.Entity<v_question_set>().HasOne(c => c.question_group).WithMany(c => c.question_sets)
        .HasForeignKey(c => c.question_group_uid).OnDelete(DeleteBehavior.SetNull);

      builder.Entity<t_question_score_type>().ToTable("question_score_type", schema);
      builder.Entity<v_question_score_type>().ToView("v_question_score_type", schema);

      builder.Entity<t_question_set_faculty>().ToTable("question_set_faculty", schema);
      builder.Entity<t_question_set_faculty>().HasOne(q => q.question_set).WithMany(q => q.question_set_faculties)
        .HasForeignKey(q => q.question_set_uid).OnDelete(DeleteBehavior.Restrict);
      builder.Entity<v_question_set_faculty>().ToView("v_question_set_faculty", schema);
      builder.Entity<v_question_set_faculty>().HasOne(q => q.question_set).WithMany(q => q.question_set_faculties)
        .HasForeignKey(q => q.question_set_uid).OnDelete(DeleteBehavior.Restrict);

      builder.Entity<t_question_set_curriculum>().ToTable("question_set_curriculum", schema);
      builder.Entity<t_question_set_curriculum>().HasOne(q => q.question_set)
        .WithMany(q => q.question_set_curriculums).HasForeignKey(q => q.question_set_uid)
        .OnDelete(DeleteBehavior.Restrict);
      builder.Entity<v_question_set_curriculum>().ToView("v_question_set_curriculum", schema);
      builder.Entity<v_question_set_curriculum>().HasOne(q => q.question_set)
        .WithMany(q => q.question_set_curriculums).HasForeignKey(q => q.question_set_uid)
        .OnDelete(DeleteBehavior.Restrict);

      builder.Entity<t_question_set_subject>().ToTable("question_set_subject", schema);
      builder.Entity<t_question_set_subject>().HasOne(q => q.question_set)
        .WithMany(q => q.question_set_subjects).HasForeignKey(q => q.question_set_uid)
        .OnDelete(DeleteBehavior.Restrict);
      builder.Entity<v_question_set_subject>().ToView("v_question_set_subject", schema);
      builder.Entity<v_question_set_subject>().HasOne(q => q.question_set)
        .WithMany(q => q.question_set_subjects).HasForeignKey(q => q.question_set_uid)
        .OnDelete(DeleteBehavior.Restrict);

      builder.Entity<t_evaluation_calendar>().ToTable("evaluation_calendar", schema);
      builder.Entity<v_evaluation_calendar>().ToView("v_evaluation_calendar", schema);

      builder.Entity<t_evaluation_calendar_schedule>().ToTable("evaluation_calendar_schedule", schema);
      builder.Entity<t_evaluation_calendar_schedule>().HasOne(e => e.evaluation_calendar)
        .WithMany(e => e.evaluation_calendar_schedules).HasForeignKey(e => e.evaluation_calendar_uid)
        .OnDelete(DeleteBehavior.Cascade);
      builder.Entity<v_evaluation_calendar_schedule>().ToView("v_evaluation_calendar_schedule", schema);
      builder.Entity<v_evaluation_calendar_schedule>().HasOne(e => e.evaluation_calendar)
        .WithMany(e => e.evaluation_calendar_schedules).HasForeignKey(e => e.evaluation_calendar_uid)
        .OnDelete(DeleteBehavior.Cascade);

      builder.Entity<t_evaluation_calendar_item>().ToTable("evaluation_calendar_item", schema);
      builder.Entity<t_evaluation_calendar_item>().HasMany(e => e.evaluation_calendar_schedules)
        .WithOne(e => e.evaluation_calendar_item).HasForeignKey(e => e.evaluation_calendar_item_uid)
        .OnDelete(DeleteBehavior.Restrict);
      builder.Entity<v_evaluation_calendar_item>().ToView("v_evaluation_calendar_item", schema);
      builder.Entity<v_evaluation_calendar_item>().HasMany(e => e.evaluation_calendar_schedules)
        .WithOne(e => e.evaluation_calendar_item).HasForeignKey(e => e.evaluation_calendar_item_uid)
        .OnDelete(DeleteBehavior.Restrict);;

      builder.Entity<t_evaluation_calendar_faculty>().ToTable("evaluation_calendar_faculty", schema);
      builder.Entity<t_evaluation_calendar_faculty>().HasOne(e => e.evaluation_calendar)
        .WithMany(e => e.evaluation_calendar_facultys).HasForeignKey(e => e.evaluation_calendar_uid)
        .OnDelete(DeleteBehavior.Cascade);
      builder.Entity<v_evaluation_calendar_faculty>().ToView("v_evaluation_calendar_faculty", schema);
      builder.Entity<v_evaluation_calendar_faculty>().HasOne(e => e.evaluation_calendar)
        .WithMany(e => e.evaluation_calendar_facultys).HasForeignKey(e => e.evaluation_calendar_uid)
        .OnDelete(DeleteBehavior.Cascade);

      builder.Entity<t_evaluation_calendar_faculty_curriculum>().ToTable("evaluation_calendar_faculty_curriculum", schema);
      builder.Entity<t_evaluation_calendar_faculty_curriculum>().HasOne(e => e.evaluation_calendar_faculty)
        .WithMany(e => e.evaluation_calendar_faculty_curriculums).HasForeignKey(e => e.evaluation_calendar_faculty_uid)
        .OnDelete(DeleteBehavior.Cascade);
      builder.Entity<v_evaluation_calendar_faculty_curriculum>().ToView("v_evaluation_calendar_faculty_curriculum", schema);
      builder.Entity<v_evaluation_calendar_faculty_curriculum>().HasOne(e => e.evaluation_calendar_faculty)
        .WithMany(e => e.evaluation_calendar_faculty_curriculums).HasForeignKey(e => e.evaluation_calendar_faculty_uid)
        .OnDelete(DeleteBehavior.Cascade);

      builder.Entity<v_faculty_evaluation>().ToView("v_faculty_evaluation", schema).HasNoKey();
      builder.Entity<v_curriculum_evaluation>().ToView("v_curriculum_evaluation", schema).HasNoKey();

      builder.Entity<t_score_calculation_type>().ToTable("score_calculation_type", schema);
      builder.Entity<v_score_calculation_type>().ToView("v_score_calculation_type", schema);

      builder.Entity<t_student_evaluation_status>().ToTable("student_evaluation_status", schema);
      builder.Entity<v_student_evaluation_status>().ToView("v_student_evaluation_status", schema);

      builder.Entity<t_evaluate_subject_section>().ToTable("evaluate_subject_section", schema);
      builder.Entity<v_evaluate_subject_section>().ToView("v_evaluate_subject_section", schema);

      builder.Entity<t_evaluate_subject_section_instructor>().ToTable("evaluate_subject_section_instructor", schema);
      builder.Entity<t_evaluate_subject_section_instructor>().HasOne(c => c.evaluate_subject_section)
        .WithMany(c => c.evaluate_subject_section_instructors).HasForeignKey(c => c.evaluate_subject_section_uid)
        .OnDelete(DeleteBehavior.Cascade);
      builder.Entity<v_evaluate_subject_section_instructor>().ToView("v_evaluate_subject_section_instructor", schema);
      builder.Entity<v_evaluate_subject_section_instructor>().HasOne(c => c.evaluate_subject_section)
        .WithMany(c => c.evaluate_subject_section_instructors).HasForeignKey(c => c.evaluate_subject_section_uid)
        .OnDelete(DeleteBehavior.Cascade);

      builder.Entity<v_student_evaluation_subject>().ToView("v_student_evaluation_subject", schema).HasNoKey();

      builder.Entity<v_evaluate_instructor_section>().ToView("v_evaluate_instructor_section", schema).HasNoKey();

      builder.Entity<v_student_evaluation_subject_result>().ToView("v_student_evaluation_subject_result", schema).HasNoKey();

      builder.Entity<t_evaluation_calendar_assignment>().ToTable("evaluation_calendar_assignment", schema);
      builder.Entity<v_evaluation_calendar_assignment>().ToView("v_evaluation_calendar_assignment", schema);

      builder.Entity<t_student_evaluation>().ToTable("student_evaluation", schema);
      builder.Entity<v_student_evaluation>().ToView("v_student_evaluation", schema);

      builder.Entity<t_student_evaluation_question_set>().ToTable("student_evaluation_question_set", schema);
      builder.Entity<t_student_evaluation_question_set>().HasOne(c => c.student_evaluation)
        .WithMany(c => c.student_evaluation_question_sets).HasForeignKey(c => c.student_evaluation_uid)
        .OnDelete(DeleteBehavior.Cascade);
      builder.Entity<v_student_evaluation_question_set>().ToView("v_student_evaluation_question_set", schema);
      builder.Entity<v_student_evaluation_question_set>().HasOne(c => c.student_evaluation)
        .WithMany(c => c.student_evaluation_question_sets).HasForeignKey(c => c.student_evaluation_uid)
        .OnDelete(DeleteBehavior.Cascade);

      builder.Entity<t_student_evaluation_question_result>().ToTable("student_evaluation_question_result", schema);
      builder.Entity<t_student_evaluation_question_result>().HasOne(c => c.student_evaluation_question_set)
        .WithMany(c => c.student_evaluation_question_results).HasForeignKey(c => c.student_evaluation_question_set_uid)
        .OnDelete(DeleteBehavior.Cascade);
      builder.Entity<v_student_evaluation_question_result>().ToView("v_student_evaluation_question_result", schema);
      builder.Entity<v_student_evaluation_question_result>().HasOne(c => c.student_evaluation_question_set)
        .WithMany(c => c.student_evaluation_question_results).HasForeignKey(c => c.student_evaluation_question_set_uid)
        .OnDelete(DeleteBehavior.SetNull);

      builder.Entity<v_faculty_register_subject>().ToView("v_faculty_register_subject", schema).HasNoKey();
      builder.Entity<v_student_evaluation_calendar>().ToView("v_student_evaluation_calendar", schema).HasNoKey();
      builder.Entity<v_student_evaluation_calendar_faculty>().ToView("v_student_evaluation_calendar_faculty", schema).HasNoKey();
      builder.Entity<v_student_evaluation_summary>().ToView("v_student_evaluation_summary", schema).HasNoKey();

    }
  }
}
