using edu_api.Modules.InstructorEvaluations.Services;
using Microsoft.Extensions.DependencyInjection;

namespace edu_api.Modules.InstructorEvaluations.Configures
{
    public static class InstructorEvaluationsCollectionExtension
    {
        public static IServiceCollection AddInstructorEvaluationsServices(this IServiceCollection services)
        {
            services.AddScoped<QuestionSetService>();
            services.AddScoped<StudentEvaluationQuestionSetService>();
            //services.AddScoped<FacultyEvaluationService>();
            //services.AddScoped<MajorEvaluationService>();
            services.AddScoped<EvaluationSubjectSectionService>();
            services.AddScoped<EvaluationCalendarFacultyCurriculum>();
            services.AddScoped<EvaluationCalendarFacultyCurriculum>();
            services.AddScoped<StudentEvaluationSummaryService>();

            return services;
        }
    }
}
