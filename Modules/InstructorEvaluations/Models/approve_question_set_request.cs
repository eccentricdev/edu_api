using System;
using System.Collections.Generic;

namespace edu_api.Modules.InstructorEvaluations.Models
{
    public class approve_question_set_request
    {
        public List<Guid>  question_set_curriculum_uids {get;set;}
    }
}