using System;
using System.Collections.Generic;

namespace edu_api.Modules.InstructorEvaluations.Models
{
  public class question_set_curriculum_assign
  {
    public Guid academic_year_uid { get; set; }
    public Guid academic_semester_uid { get; set; }
    public List<Guid>  question_set_uids { get; set; }
    public List<Guid>  faculty_curriculum_uids { get; set; }
  }
}
