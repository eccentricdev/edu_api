using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DocumentFormat.OpenXml.Wordprocessing;
using edu_api.Modules.InstructorEvaluations.Databases.Models;
using Microsoft.EntityFrameworkCore;
using SeventyOneDev.Utilities;

namespace edu_api.Modules.InstructorEvaluations.Services
{
  public class StudentEvaluationSummaryService
  {
    private readonly Db _context;
    public StudentEvaluationSummaryService(Db context)
    {
      _context = context;
    }

    public async Task<List<v_student_evaluation_summary>> GetStudentEvaluationSummary(
      v_student_evaluation_summary studentEvaluationSummary)
    {
      var studentEvaluationSummaries = _context.v_student_evaluation_summary.AsNoTracking()
        .Where(s => s.academic_year_uid == studentEvaluationSummary.academic_year_uid &&
                    s.academic_semester_uid == studentEvaluationSummary.academic_semester_uid &&
                    s.college_faculty_uid == studentEvaluationSummary.college_faculty_uid);
      if (studentEvaluationSummary.faculty_curriculum_uid.HasValue)
      {
        studentEvaluationSummaries = studentEvaluationSummaries.Where(s =>
          s.faculty_curriculum_uid == studentEvaluationSummary.faculty_curriculum_uid);
      }

      return await studentEvaluationSummaries.ToListAsync();
    }

  }
}
