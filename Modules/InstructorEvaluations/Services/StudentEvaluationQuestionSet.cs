using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using edu_api.Modules.InstructorEvaluations.Databases.Models;
using Microsoft.EntityFrameworkCore;
using SeventyOneDev.Utilities;

namespace edu_api.Modules.InstructorEvaluations.Services
{
  public class StudentEvaluationQuestionSetService:EntityUidService<t_student_evaluation_question_set,v_student_evaluation_question_set>
  {
    private readonly Db _context;
    public StudentEvaluationQuestionSetService(Db context) : base(context)
    {
      _context = context;
    }

    public override async Task<v_student_evaluation_question_set> GetEntity(Guid uid, List<Guid?> agencies = null, string userName = null)
    {

      var result= await base.GetEntity(uid, agencies, userName);
      if (result.student_evaluation_question_results != null)
      {
        if (result.student_evaluation_question_results.Any())
        {
          return result;
        }
      }

      var questions = await _context.v_question.AsNoTracking().Where(q => q.question_set_uid == result.question_set_uid)
        .ToListAsync();

      result.student_evaluation_question_results =
        questions.Select(q => new v_student_evaluation_question_result(q)).ToList();
      return result;

    }
  }
}
