// using System.Collections.Generic;
// using System.Linq;
// using System.Threading.Tasks;
// using edu_api.Modules.InstructorEvaluations.Databases.Models;
// using Microsoft.EntityFrameworkCore;
// using SeventyOneDev.Utilities;
//
// namespace edu_api.Modules.InstructorEvaluations.Services
// {
//   public class MajorEvaluationService
//   {
//     readonly Db _context;
//     private DbSet<v_major_evaluation> readEntities;
//     public MajorEvaluationService(Db context)
//     {
//       _context = context;
//       readEntities = _context.Set<v_major_evaluation>();
//     }
//
//     public async Task<PagedResult<v_major_evaluation>> PageEntityId(v_major_evaluation entity,int page,int size)
//     {
//       var entitys = readEntities.AsNoTracking();
//       if (entity != null)
//       {
//         entitys = !string.IsNullOrEmpty(entity.search)? entitys.Search(entity.search) : entitys.Search(entity);
//       }
//       entitys = entitys.OrderBy(c => c.major_code);
//       var paged = await entitys.GetPaged<v_major_evaluation>(page, size);
//
//       return paged;
//     }
//
//     public async Task<List<v_major_evaluation>> ListEntityId(v_major_evaluation entity)
//     {
//       IQueryable <v_major_evaluation> entitys = readEntities.AsNoTracking();
//       if (entity != null)
//       {
//         entitys = !string.IsNullOrEmpty(entity.search)? entitys.Search(entity.search) : entitys.Search(entity);
//       }
//       entitys = entitys.OrderBy(c => c.major_code);
//       return await entitys.ToListAsync();
//     }
//
//     // public async Task<List<S>> ListEntityId<S>(v_major_evaluation entity)
//     // {
//     //   IQueryable<v_major_evaluation> entitys = readEntities.AsNoTracking();
//     //   if (entity != null)
//     //   {
//     //     entitys = !string.IsNullOrEmpty(entity.search)? entitys.Search(entity.search) : entitys.Search(entity);
//     //   }
//     //   var x = entitys.OrderBy(c => c.major_code).ProjectTo<v_major_evaluation,S>();
//     //   var result = await x.ToListAsync();
//     //   return result;
//     // }
//   }
// }
