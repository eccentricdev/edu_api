using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using edu_api.Modules.InstructorEvaluations.Databases.Models;
using Microsoft.EntityFrameworkCore;
using SeventyOneDev.Utilities;

namespace edu_api.Modules.InstructorEvaluations.Services
{
    public class EvaluationSubjectSectionService
    {
        readonly Db _context;

        public EvaluationSubjectSectionService(Db context)
        {
          this._context = context;
        }

        public async Task<List<v_student_evaluation_subject>> GetEvalutionSubject(v_student_evaluation_subject studentEvaluationSubject)
        {
          //List<v_evaluate_instructor_section> evaluateInstructorSections = null;
          // if (!String.IsNullOrEmpty(studentEvaluationSubject.instructor_code))
          // {
          //   evaluateInstructorSections = await _context.v_evaluate_instructor_section.AsNoTracking()
          //     .Where(s => s.instructor_code == studentEvaluationSubject.instructor_code &&
          //                 s.academic_year_uid == studentEvaluationSubject.academic_year_uid &&
          //                 s.academic_semester_uid == studentEvaluationSubject.academic_semester_uid).ToListAsync();
          // }

          studentEvaluationSubject.instructor_code = null;
          var evaluationSubjects = await _context.v_student_evaluation_subject.AsNoTracking()
            .Search(studentEvaluationSubject).ToListAsync();
          var studentEvaluationSubjects = evaluationSubjects.GroupBy(es => new
            {
              es.year_subject_code, es.year_subject_name_th,es.year_subject_section_uid,
              es.section_code
            })
            .Select(es => new v_student_evaluation_subject()
            {
              year_subject_code = es.Key.year_subject_code,
              year_subject_name_th = es.Key.year_subject_name_th,
              section_code = es.Key.section_code,
              year_subject_section_uid = es.Key.year_subject_section_uid,
              //study_type_name_th = es.Key.study_type_name_th,
              assinged = es.Count(),
              responsed = es.Sum(x => x.is_response)
            });
          var result = studentEvaluationSubjects.ToList();
          // if (evaluateInstructorSections != null)
          // {
          //   if (evaluateInstructorSections.Any())
          //   {
          //     result = result.Where(r =>
          //       evaluateInstructorSections.Any(e => e.year_subject_section_uid == r.year_subject_section_uid)).ToList();
          //   }
          // }

          return result;

        }

        public async Task<v_student_evaluation_subject> GetEvalutionSubjectResult(v_student_evaluation_subject studentEvaluationSubject)
        {
          studentEvaluationSubject = await _context.v_student_evaluation_subject.AsNoTracking()
            .FirstOrDefaultAsync(s => s.year_subject_section_uid == studentEvaluationSubject.year_subject_section_uid);
          if (studentEvaluationSubject != null)
          {
            if (!String.IsNullOrEmpty(studentEvaluationSubject.instructor_code))
            {
              var instructor = await _context.v_employee.AsNoTracking()
                .FirstOrDefaultAsync(e => e.employee_code == studentEvaluationSubject.instructor_code);
              if (instructor != null)
              {
                studentEvaluationSubject.instructor_name = instructor.display_name_th;
              }

              var studentEvaluationSubjectResults = await _context.v_student_evaluation_subject_result.AsNoTracking()
                .Where(
                  s =>
                    s.study_subject_section_uid == studentEvaluationSubject.year_subject_section_uid &&
                    s.instructor_code == studentEvaluationSubject.instructor_code).ToListAsync();
              if (!studentEvaluationSubjectResults.Any())
              {
                studentEvaluationSubjectResults = await _context.v_student_evaluation_subject_result.AsNoTracking()
                  .Where(
                    s =>
                      s.study_subject_section_uid == studentEvaluationSubject.year_subject_section_uid &&
                      s.instructor_code == null).ToListAsync();
              }

              var results = studentEvaluationSubjectResults
                .GroupBy(s => new { s.question_set_uid, s.question_set_name_th })
                .Select(s => new e_question_set_result()
                {
                  question_set_name_th = s.Key.question_set_name_th,
                  question_results = s.GroupBy(q => new { q.question_uid, q.question_name_th })
                    .Select(q => new e_question_result()
                    {
                      question_name_th = q.Key.question_name_th,
                      score_rates = q.GroupBy(r => r.selected_choice)
                        .Select(r =>
                          new e_score_rate()
                          {
                            rate = r.Key,
                            score = r.Count(),
                            comments = r.Where(c => c.text_answer != null).Select(c => c.text_answer).ToList()
                          }).ToList()
                    }).ToList()
                });
              studentEvaluationSubject.question_set_results = results.ToList();
            }
            else
            {

              var studentEvaluationSubjectResults = await _context.v_student_evaluation_subject_result.AsNoTracking()
                .Where(
                  s =>
                    s.study_subject_section_uid == studentEvaluationSubject.year_subject_section_uid &&
                    s.instructor_code == studentEvaluationSubject.instructor_code).ToListAsync();
              if (!studentEvaluationSubjectResults.Any())
              {
                studentEvaluationSubjectResults = await _context.v_student_evaluation_subject_result.AsNoTracking()
                  .Where(
                    s =>
                      s.study_subject_section_uid == studentEvaluationSubject.year_subject_section_uid &&
                      s.instructor_code == null).ToListAsync();
              }

              var results = studentEvaluationSubjectResults
                .GroupBy(s => new { s.question_set_uid, s.question_set_name_th })
                .Select(s => new e_question_set_result()
                {
                  question_set_name_th = s.Key.question_set_name_th,
                  question_results = s.GroupBy(q => new { q.question_uid, q.question_name_th })
                    .Select(q => new e_question_result()
                    {
                      question_name_th = q.Key.question_name_th,
                      score_rates = q.GroupBy(r => r.selected_choice)
                        .Select(r =>
                          new e_score_rate()
                          {
                            rate = r.Key,
                            score = r.Count(),
                            comments = r.Where(c => c.text_answer != null).Select(c => c.text_answer).ToList()
                          }).ToList()
                    }).ToList()
                });
              studentEvaluationSubject.question_set_results = results.ToList();
            }
            // if (studentEvaluationSubject.instructor_code == null)
            // {
            //   var instructors = await _context.v_evaluate_subject_section_instructor.AsNoTracking().Where(i =>
            //     i.evaluate_subject_section_id == studentEvaluationSubject.study_subject_section_id).ToListAsync();
            //   studentEvaluationSubject.instructors = instructors;
            // }
            // else
            // {
            //   var instructors = await _context.v_evaluate_subject_section_instructor.AsNoTracking().Where(i =>
            //     i.evaluate_subject_section_id == studentEvaluationSubject.study_subject_section_id && i.instructor_code==studentEvaluationSubject.instructor_code).ToListAsync();
            //   studentEvaluationSubject.instructors = instructors;
            // }
            //
            // if (instructors.Any())
            // {
            //    studentEvaluationSubject.instructors = String.Join("|",instructors.Select(i => i.display_name_th).ToList());
            // }
          }

          return studentEvaluationSubject;

        }

    }
}