using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using edu_api.Modules.InstructorEvaluations.Databases.Models;
using Microsoft.EntityFrameworkCore;
using SeventyOneDev.Utilities;

namespace edu_api.Modules.InstructorEvaluations.Services
{
  public class EvaluationCalendarFacultyCurriculum:EntityUidService<t_evaluation_calendar_faculty_curriculum,v_evaluation_calendar_faculty_curriculum>
  {
    private readonly Db _context;
    public EvaluationCalendarFacultyCurriculum(Db context) : base(context)
    {
      _context = context;
    }

    public override async Task<v_evaluation_calendar_faculty_curriculum> GetEntity(Guid uid, List<Guid?> agencies = null, string userName = null)
    {
      var evaluationCalendarFacultyCurriculum = await base.GetEntity(uid, agencies, userName);
      evaluationCalendarFacultyCurriculum.evaluation_calendar_schedules = await _context.v_evaluation_calendar_schedule
        .AsNoTracking()
        .Where(e => e.evaluation_calendar_uid == evaluationCalendarFacultyCurriculum.evaluation_calendar_uid)
        .ToListAsync();
      return evaluationCalendarFacultyCurriculum;

    }
  }
}
