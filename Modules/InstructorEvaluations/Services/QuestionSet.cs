using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using edu_api.Modules.InstructorEvaluations.Databases.Models;
using edu_api.Modules.InstructorEvaluations.Models;
using Microsoft.EntityFrameworkCore;
using SeventyOneDev.Utilities;

namespace edu_api.Modules.InstructorEvaluations.Services
{
    public class QuestionSetService:EntityUidService<t_question_set,v_question_set>
    {
        private readonly Db _context;
        public QuestionSetService(Db context) : base(context)
        {
            _context = context;
        }


        public async Task<bool> AssignQuestionSet(question_set_faculty_assign questionSetFacultyAssign,
          ClaimsIdentity claimsIdentity)
        {
          var questionSetFaculties = new List<t_question_set_faculty>();
          var questionSetCurriculums = new List<t_question_set_curriculum>();
          var questionSetSubjects = new List<t_question_set_subject>();
          var studentEvaluations = new List<t_student_evaluation>();
          var studentEvaluationQuestionSets = new List<t_student_evaluation_question_set>();
          //List<t_evaluate_subject_section> evaluateSubjectSections = new List<t_evaluate_subject_section>();
          foreach (var facultyUid in questionSetFacultyAssign.college_faculty_uids)
          {
            foreach (var questionSetUid in questionSetFacultyAssign.question_set_uids)
            {
              var questionSetFaculty = new t_question_set_faculty()
              {
                question_set_faculty_uid = Guid.NewGuid(),
                academic_year_uid = questionSetFacultyAssign.academic_year_uid,
                academic_semester_uid = questionSetFacultyAssign.academic_semester_uid,
                question_set_uid = questionSetUid,
                college_faculty_uid = facultyUid,
                // question_set_faculty_subjects =
                //   questionSetFacultySubjects
                //     .ToList(), //.Where(s=>s.question_set_faculty_subject_sections.Count>0).ToList(),
                status_id = 1,
                score_calculation_type_id = 1
              };
              questionSetFaculties.Add(questionSetFaculty);
              var facultyCurriculums = await _context.t_faculty_curriculum.AsNoTracking()
                .Where(f => f.college_faculty_uid == facultyUid).ToListAsync();
              foreach (var facultyCurriculum in facultyCurriculums)
              {
                var questionSetCurriculum = new t_question_set_curriculum()
                {
                  question_set_curriculum_uid = Guid.NewGuid(),
                  question_set_faculty_uid = questionSetFaculty.question_set_faculty_uid,
                  academic_year_uid = questionSetFacultyAssign.academic_year_uid,
                  academic_semester_uid = questionSetFacultyAssign.academic_semester_uid,
                  question_set_uid = questionSetUid,
                  college_faculty_uid = facultyUid,
                  faculty_curriculum_uid = facultyCurriculum.faculty_curriculum_uid,
                  status_id = 1,
                  score_calculation_type_id = 1
                };
                questionSetCurriculums.Add(questionSetCurriculum);
                var yearSubjects = await _context.t_year_subject.AsNoTracking()
                  .Where(s => s.owner_faculty_curriculum_uid == facultyCurriculum.faculty_curriculum_uid).ToListAsync();
                foreach (var yearSubject in yearSubjects)
                {
                  var questionSetSubject = new t_question_set_subject()
                  {
                    question_set_subject_uid = Guid.NewGuid(),
                    question_set_curriculum_uid = questionSetCurriculum.question_set_curriculum_uid,
                    academic_year_uid = questionSetFacultyAssign.academic_year_uid,
                    academic_semester_uid = questionSetFacultyAssign.academic_semester_uid,
                    question_set_uid = questionSetUid,
                    year_subject_uid = yearSubject.year_subject_uid,
                    status_id = 1,
                    score_calculation_type_id = 1
                  };
                  
                  questionSetSubjects.Add(questionSetSubject);
                  var finalRegisterSubjects = _context.v_final_register_subject.AsNoTracking()
                    .Where(s => s.year_subject_uid == yearSubject.year_subject_uid);
                  foreach (var finalRegisterSubject in finalRegisterSubjects)
                  {
                    var studentEvaluation = await _context.t_student_evaluation.AsNoTracking().FirstOrDefaultAsync(s =>
                      s.year_subject_uid == yearSubject.year_subject_uid &&
                      s.student_uid == finalRegisterSubject.student_uid);
                    
                    if (studentEvaluation is null)
                    {
                      studentEvaluation = studentEvaluations.FirstOrDefault(s =>
                        s.year_subject_uid == yearSubject.year_subject_uid &&
                        s.student_uid == finalRegisterSubject.student_uid);
                      if (studentEvaluation is null)
                      {


                        studentEvaluation = new t_student_evaluation()
                        {
                          student_evaluation_uid = Guid.NewGuid(),
                          student_uid = finalRegisterSubject.student_uid,
                          academic_year_uid = finalRegisterSubject.academic_year_uid,
                          academic_semester_uid = finalRegisterSubject.academic_semester_uid,
                          year_subject_uid = finalRegisterSubject.year_subject_uid,
                          year_subject_code = finalRegisterSubject.year_subject_code,
                          year_subject_name_th = finalRegisterSubject.year_subject_name_th,
                          year_subject_name_en = finalRegisterSubject.year_subject_name_en,
                          year_subject_section_uid = finalRegisterSubject.lecture_year_subject_section_uid,
                          student_evaluation_status_uid = null,
                          college_faculty_uid = null,
                          evaluation_date = null,
                          section_code = finalRegisterSubject.lecture_section_code,
                          instructor_code = null //finalRegisterSubject.instructor_code
                        };
                        studentEvaluations.Add(studentEvaluation);
                      }

                      // var studentEvaluationQuestionSet=  new t_student_evaluation_question_set()
                      // {
                      //   student_evaluation_uid = studentEvaluation.student_evaluation_uid,
                      //   question_set_faculty_uid =questionSetFaculty.question_set_faculty_uid,
                      //   question_set_curriculum_uid =questionSetCurriculum.question_set_curriculum_uid,
                      //   question_set_uid =questionSetUid
                      // };
                      // // await _context.t_student_evaluation.AddAsync(studentEvaluation);
                      // await _context.t_student_evaluation_question_set.AddAsync(studentEvaluationQuestionSet);
                    }
                    // else
                    // {
                      var studentEvaluationQuestionSet=  new t_student_evaluation_question_set()
                      {
                        student_evaluation_uid = studentEvaluation.student_evaluation_uid,
                        question_set_faculty_uid =questionSetFaculty.question_set_faculty_uid,
                        question_set_curriculum_uid =questionSetCurriculum.question_set_curriculum_uid,
                        question_set_uid =questionSetUid
                      };
                      studentEvaluationQuestionSets.Add(studentEvaluationQuestionSet);
                      //await _context.t_student_evaluation.AddAsync(studentEvaluation);
                      //await _context.t_student_evaluation_question_set.AddAsync(studentEvaluationQuestionSet);
                    //}
                    
                    //await _db.SaveChangesAsync();
                      
                    
                    

                  }
                
                }
              }
            }


          }

          await _context.t_student_evaluation.AddRangeAsync(studentEvaluations);
          await _context.t_student_evaluation_question_set.AddRangeAsync(studentEvaluationQuestionSets);
          await _context.t_question_set_faculty.AddRangeAsync(questionSetFaculties);
          await _context.t_question_set_curriculum.AddRangeAsync(questionSetCurriculums);
          await _context.t_question_set_subject.AddRangeAsync(questionSetSubjects);
          await _context.SaveChangesAsync();
          return true;
        }

        public async Task<bool> AssignQuestionSet(question_set_curriculum_assign questionSetMajorAssign,
          ClaimsIdentity claimsIdentity)
        {
          var questionSetMajors = new List<t_question_set_curriculum>();
          //List<t_evaluate_subject_section> evaluateSubjectSections = new List<t_evaluate_subject_section>();
          foreach (var facultyCurriculumUid in questionSetMajorAssign.faculty_curriculum_uids)
          {

            foreach (var questionSetUid in questionSetMajorAssign.question_set_uids)
            {
              questionSetMajors.Add(new t_question_set_curriculum()
              {
                academic_year_uid = questionSetMajorAssign.academic_year_uid,
                academic_semester_uid = questionSetMajorAssign.academic_semester_uid,
                question_set_uid = questionSetUid,
                faculty_curriculum_uid = facultyCurriculumUid,

                status_id = 1,
                score_calculation_type_id = 1
              });
            }

          }

          await _context.t_question_set_curriculum.AddRangeAsync(questionSetMajors);
          //await _context.t_evaluate_subject_section.AddRangeAsync(evaluateSubjectSections);
          await _context.SaveChangesAsync();
          return true;
        }

        public async Task<bool> AssignQuestionSet(question_set_subject_assign questionSetSubjectAssign,
          ClaimsIdentity claimsIdentity)
        {
          var questionSetSubjects = new List<t_question_set_subject>();
          //List<t_evaluate_subject_section> evaluateSubjectSections = new List<t_evaluate_subject_section>();
          foreach (var subjectYearId in questionSetSubjectAssign.subject_uids)
          {

            foreach (var questionSetUid in questionSetSubjectAssign.question_set_uids)
            {
              questionSetSubjects.Add(new t_question_set_subject()
              {
                academic_year_uid = questionSetSubjectAssign.academic_year_uid,
                academic_semester_uid = questionSetSubjectAssign.academic_semester_uid,
                question_set_uid = questionSetUid,
                year_subject_uid = subjectYearId,

                status_id = 1,
                score_calculation_type_id = 1
              });
            }


          }

          await _context.t_question_set_subject.AddRangeAsync(questionSetSubjects);
          //await _context.t_evaluate_subject_section.AddRangeAsync(evaluateSubjectSections);
          await _context.SaveChangesAsync();
          return true;
        }

        public override async Task<t_question_set> NewEntity(t_question_set entity, ClaimsIdentity claimsIdentity)
        {
          entity.total_questions = entity.questions.Count;
          return await base.NewEntity(entity, claimsIdentity);
        }

        public override async Task<int> UpdateEntity(t_question_set entity, ClaimsIdentity claimsIdentity)
        {
          entity.total_questions = entity.questions.Count;
          return await base.UpdateEntity(entity, claimsIdentity);
        }
    }
}
