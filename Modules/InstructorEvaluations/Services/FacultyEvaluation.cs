// using System.Collections.Generic;
// using System.Linq;
// using System.Threading.Tasks;
// using edu_api.Modules.InstructorEvaluations.Databases.Models;
// using Microsoft.EntityFrameworkCore;
// using SeventyOneDev.Utilities;
//
// namespace edu_api.Modules.InstructorEvaluations.Services
// {
//   public class FacultyEvaluationService
//   {
//     readonly Db _context;
//     private DbSet<v_faculty_evaluation> readEntities;
//     public FacultyEvaluationService(Db context)
//     {
//       _context = context;
//       readEntities = _context.Set<v_faculty_evaluation>();
//     }
//
//     public async Task<PagedResult<v_faculty_evaluation>> PageEntityId(v_faculty_evaluation entity,int page,int size)
//     {
//       var entitys = readEntities.AsNoTracking();
//       if (entity != null)
//       {
//         entitys = !string.IsNullOrEmpty(entity.search)? entitys.Search(entity.search) : entitys.Search(entity);
//       }
//       entitys = entitys.OrderBy(c => c.college_faculty_code);
//       var paged = await entitys.GetPaged<v_faculty_evaluation>(page, size);
//
//       return paged;
//     }
//
//     public async Task<List<v_faculty_evaluation>> ListEntityId(v_faculty_evaluation entity)
//     {
//       IQueryable <v_faculty_evaluation> entitys = readEntities.AsNoTracking();
//       if (entity != null)
//       {
//         entitys = !string.IsNullOrEmpty(entity.search)? entitys.Search(entity.search) : entitys.Search(entity);
//       }
//       entitys = entitys.OrderBy(c => c.college_faculty_code);
//       return await entitys.ToListAsync();
//     }
//
//
//     // public async Task<List<S>> ListEntityId<S>(v_faculty_evaluation entity)
//     // {
//     //   IQueryable<v_faculty_evaluation> entitys = readEntities.AsNoTracking();
//     //   if (entity != null)
//     //   {
//     //     entitys = !string.IsNullOrEmpty(entity.search)? entitys.Search(entity.search) : entitys.Search(entity);
//     //   }
//     //   var x = entitys.OrderBy(c => c.faculty_code).ProjectTo<v_faculty_evaluation,S>();
//     //   var result = await x.ToListAsync();
//     //   return result;
//     // }
//
//   }
// }
