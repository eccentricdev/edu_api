using System;
using System.Security.Claims;
using System.Threading.Tasks;
using edu_api.Modules.InstructorEvaluations.Databases.Models;
using edu_api.Modules.InstructorEvaluations.Models;
using edu_api.Modules.InstructorEvaluations.Services;
using Microsoft.AspNetCore.Mvc;
using SeventyOneDev.Utilities;

namespace edu_api.Modules.InstructorEvaluations.Controllers
{
    [Route("api/instructor_evaluations/question_set", Name = "question_set")]
    public class QuestionSetController:BaseUidController<t_question_set,v_question_set>
    {
        private readonly QuestionSetService _entityUidService;
        public QuestionSetController(QuestionSetService entityUidService) : base(entityUidService)
        {
            _entityUidService = entityUidService;
        }

        [HttpPost("assign/faculty")]
        public async Task<ActionResult> AssignQuestionSet([FromBody] question_set_faculty_assign questionSetFacultyAssign)
        {
          try
          {
            var newResult =
              await _entityUidService.AssignQuestionSet(questionSetFacultyAssign, User.Identity as ClaimsIdentity);
            return
              NoContent(); //Created("/v1/discipline/discipline_period_types/" + newResult.GetType()=====.ToString(),newResult);
          }
          catch (Exception ex)
          {
            Console.WriteLine(ex);
            return BadRequest(ErrorMessage.Get(ex.Message));
          }

        }

        [HttpPost("assign/curriculum")]
        public async Task<ActionResult> AssignQuestionSet([FromBody] question_set_curriculum_assign questionSetCurriculumAssign)
        {
          try
          {
            var newResult =
              await _entityUidService.AssignQuestionSet(questionSetCurriculumAssign, User.Identity as ClaimsIdentity);
            return
              NoContent(); //Created("/v1/discipline/discipline_period_types/" + newResult.GetType()=====.ToString(),newResult);
          }
          catch (Exception ex)
          {
            Console.WriteLine(ex);
            return BadRequest(ErrorMessage.Get(ex.Message));
          }

        }

        [HttpPost("assign/subject")]
        public async Task<ActionResult> AssignQuestionSet([FromBody] question_set_subject_assign questionSetSubjectAssign)
        {
          try
          {
            var newResult =
              await _entityUidService.AssignQuestionSet(questionSetSubjectAssign, User.Identity as ClaimsIdentity);
            return
              NoContent(); //Created("/v1/discipline/discipline_period_types/" + newResult.GetType()=====.ToString(),newResult);
          }
          catch (Exception ex)
          {
            Console.WriteLine(ex);
            return BadRequest(ErrorMessage.Get(ex.Message));
          }

        }
    }
}
