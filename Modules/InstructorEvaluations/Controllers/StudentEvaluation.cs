using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using edu_api.Modules.InstructorEvaluations.Databases.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using rsu_common_api.models;
using SeventyOneDev.Utilities;

namespace edu_api.Modules.InstructorEvaluations.Controllers
{
    [Route("api/instructor_evaluations/student_evaluation", Name = "student_evaluation")]
    public class StudentEvaluationController:BaseUidController<t_student_evaluation,v_student_evaluation>
    {
        private readonly Db _db;
        private readonly EntityUidService<t_student_evaluation, v_student_evaluation> _studentEvaluationService;
        public StudentEvaluationController(EntityUidService<t_student_evaluation, v_student_evaluation> entityUidService,Db db) : base(entityUidService)
        {
            _db = db;
            _studentEvaluationService = entityUidService;
        }
        [HttpGet("{uid}")]
        public override async Task<ActionResult<v_student_evaluation>> GetEntity(Guid uid)
        {
            var studentEvaluation = await _db.t_student_evaluation.AsNoTracking().FirstOrDefaultAsync(s => s.student_evaluation_uid == uid);
            if (studentEvaluation is null)
            {
                var studentEvaluationData = await _db.v_student_evaluation.AsNoTracking().FirstOrDefaultAsync(s => s.student_evaluation_uid == uid);
                
                if (studentEvaluationData is not null)
                {
                    
                    studentEvaluation = new t_student_evaluation()
                    {
                        student_evaluation_uid = uid,
                        student_uid = studentEvaluationData.student_uid,
                        academic_year_uid = studentEvaluationData.academic_year_uid,
                        academic_semester_uid = studentEvaluationData.academic_semester_uid,
                        year_subject_uid = studentEvaluationData.year_subject_uid,
                        year_subject_code = studentEvaluationData.year_subject_code,
                        year_subject_name_th = studentEvaluationData.year_subject_name_th,
                        year_subject_name_en = studentEvaluationData.year_subject_name_en,
                        year_subject_section_uid = studentEvaluationData.year_subject_section_uid,
                        student_evaluation_status_uid = null,
                        college_faculty_uid = studentEvaluationData.college_faculty_uid,
                        evaluation_date = studentEvaluationData.evaluation_date,
                        section_code = studentEvaluationData.section_code,
                        instructor_code = studentEvaluationData.instructor_code
                    };
                    var questionSetSubjects = await _db.v_question_set_subject.AsNoTracking()
                        .Where(q => q.year_subject_uid == studentEvaluationData.year_subject_uid).ToListAsync();
                    var studentEvaluationQuestionSets= questionSetSubjects.Select(q => new t_student_evaluation_question_set()
                    {
                        student_evaluation_uid = studentEvaluation.student_evaluation_uid,
                        question_set_faculty_uid =q.question_set_faculty_uid,
                        question_set_curriculum_uid =q.question_set_curriculum_uid,
                        question_set_uid =q.question_set_uid
                    });
                    await _db.t_student_evaluation.AddAsync(studentEvaluation);
                    await _db.t_student_evaluation_question_set.AddRangeAsync(studentEvaluationQuestionSets);
                    await _db.SaveChangesAsync();

                }

                var newStudentEvaluation = await _db.v_student_evaluation.AsNoTracking()
                    .Include(s => s.student_evaluation_question_sets)
                    .FirstOrDefaultAsync(s => s.student_evaluation_uid == uid);
                return Ok(newStudentEvaluation);


            }

            return await base.GetEntity(uid);
            //var studentEvaluations = await _studentEvaluationService.GetEntity(uid);
            //studentEvaluations.

        }

       
    }
}