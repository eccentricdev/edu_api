// using System.Collections.Generic;
// using System.Linq;
// using System.Threading.Tasks;
// using edu_api.Modules.InstructorEvaluations.Databases.Models;
// using edu_api.Modules.InstructorEvaluations.Services;
// using Microsoft.AspNetCore.Http;
// using Microsoft.AspNetCore.Mvc;
// using SeventyOneDev.Utilities;
//
// namespace edu_api.Modules.InstructorEvaluations.Controllers
// {
//   [Route("api/instructor_evaluations/faculty_evaluation",Name="faculty_evaluation")]
//   public class FacultyEvaluationController : Controller
//   {
//     readonly FacultyEvaluationService _facultyEvaluationService;
//
//     public FacultyEvaluationController(FacultyEvaluationService facultyEvaluationService)
//     {
//       _facultyEvaluationService = facultyEvaluationService;
//     }
//
//     [HttpGet]
//     public async Task<ActionResult<List<v_faculty_evaluation>>> GetEntityIds([FromQuery] v_faculty_evaluation _entityId = null)
//     {
//       if (EFExtension.IsNullOrEmpty(_entityId))
//       {
//         var listResult = await _facultyEvaluationService.ListEntityId(null);
//         return Ok(listResult);
//       }
//       else
//       {
//         var getResult = await _facultyEvaluationService.ListEntityId(_entityId);
//         if (!getResult.Any())
//         {
//           return Ok(new List<v_faculty_evaluation>());
//         }
//         else
//         {
//           return Ok(getResult.ToList());
//         }
//
//       }
//     }
//
//
//     [HttpGet("{page}/{size}")]
//     public async Task<ActionResult<PagedResult<v_faculty_evaluation>>> GetEntityPagedIds([FromQuery] v_faculty_evaluation _entity = null,[FromRoute]int page=1,[FromRoute]int size=10)
//     {
//       if (EFExtension.IsNullOrEmpty(_entity))
//       {
//         var listResult = await _facultyEvaluationService.PageEntityId(null,page,size);
//         return Ok(listResult);
//       }
//       else
//       {
//         var getResult = await _facultyEvaluationService.PageEntityId(_entity,page,size);
//         return Ok(getResult);
//       }
//     }
//
//   }
// }
