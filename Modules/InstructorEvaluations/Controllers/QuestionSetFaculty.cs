using System.Threading.Tasks;
using edu_api.Modules.InstructorEvaluations.Databases.Models;
using edu_api.Modules.InstructorEvaluations.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SeventyOneDev.Utilities;

namespace edu_api.Modules.InstructorEvaluations.Controllers
{
    [ApiController]
    [Route("api/instructor_evaluations/question_set_faculty", Name = "question_set_faculty")]
    public class QuestionSetFacultyController:BaseUidController<t_question_set_faculty,v_question_set_faculty>
    {
        private readonly Db _db;
        public QuestionSetFacultyController(EntityUidService<t_question_set_faculty, v_question_set_faculty> entityUidService,Db db) : base(entityUidService)
        {
            _db = db;
        }
        [HttpPost("approve")]
        public async Task<ActionResult> ApproveQuestionSet([FromBody]approve_question_set_request approveQuestionSetRequest)
        {
            foreach (var questionSetCurriculumUid in approveQuestionSetRequest.question_set_curriculum_uids)
            {
                var questionSetCurriculum =await 
                    _db.t_question_set_curriculum.FirstOrDefaultAsync(q =>
                        q.question_set_curriculum_uid == questionSetCurriculumUid);
                questionSetCurriculum.is_approve = true;
            }

            await _db.SaveChangesAsync();
            return Ok();
        }
    }
}