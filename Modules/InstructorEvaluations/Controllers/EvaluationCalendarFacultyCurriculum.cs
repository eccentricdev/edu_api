using edu_api.Modules.InstructorEvaluations.Databases.Models;
using Microsoft.AspNetCore.Mvc;
using SeventyOneDev.Utilities;

namespace edu_api.Modules.InstructorEvaluations.Controllers
{
  [Route("api/instructor_evaluations/evaluation_calendar_faculty_curriculum", Name = "evaluation_calendar_faculty_curriculum")]
  public class EvaluationCalendarFacultyCurriculum:BaseUidController<t_evaluation_calendar_faculty_curriculum,v_evaluation_calendar_faculty_curriculum>
  {
    public EvaluationCalendarFacultyCurriculum(EntityUidService<t_evaluation_calendar_faculty_curriculum, v_evaluation_calendar_faculty_curriculum> entityUidService) : base(entityUidService)
    {
    }
  }
}
