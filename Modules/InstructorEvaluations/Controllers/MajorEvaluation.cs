// using System.Collections.Generic;
// using System.Linq;
// using System.Threading.Tasks;
// using edu_api.Modules.InstructorEvaluations.Databases.Models;
// using edu_api.Modules.InstructorEvaluations.Services;
// using Microsoft.AspNetCore.Http;
// using Microsoft.AspNetCore.Mvc;
// using SeventyOneDev.Utilities;
//
// namespace edu_api.Modules.InstructorEvaluations.Controllers
// {
//   [Route("api/instructor_evaluations/major_evaluation",Name="major_evaluation")]
//   public class MajorEvaluationController : Controller
//   {
//     readonly MajorEvaluationService _majorEvaluationService;
//
//     public MajorEvaluationController(MajorEvaluationService majorEvaluationService)
//     {
//       _majorEvaluationService = majorEvaluationService;
//     }
//
//     [HttpGet]
//     public async Task<ActionResult<List<v_major_evaluation>>> GetEntityIds([FromQuery] v_major_evaluation _entityId = null)
//     {
//       if (EFExtension.IsNullOrEmpty(_entityId))
//       {
//         var listResult = await _majorEvaluationService.ListEntityId(null);
//         return Ok(listResult);
//       }
//       else
//       {
//         var getResult = await _majorEvaluationService.ListEntityId(_entityId);
//         if (!getResult.Any())
//         {
//           return Ok(new List<v_major_evaluation>());
//         }
//         else
//         {
//           return Ok(getResult.ToList());
//         }
//
//       }
//
//     }
//     [HttpGet("{page}/{size}")]
//     public async Task<ActionResult<PagedResult<v_major_evaluation>>> GetEntityPagedIds([FromQuery] v_major_evaluation _entity = null,[FromRoute]int page=1,[FromRoute]int size=10)
//     {
//       if (EFExtension.IsNullOrEmpty(_entity))
//       {
//         var listResult = await _majorEvaluationService.PageEntityId(null,page,size);
//         return Ok(listResult);
//       }
//       else
//       {
//         var getResult = await _majorEvaluationService.PageEntityId(_entity,page,size);
//
//         return Ok(getResult);
//       }
//
//     }
//
//   }
// }
