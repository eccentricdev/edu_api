using System;
using System.Linq;
using System.Threading.Tasks;
using edu_api.Modules.InstructorEvaluations.Databases.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SeventyOneDev.Utilities;

namespace edu_api.Modules.InstructorEvaluations.Controllers
{
  [Route("api/instructor_evaluations/student_evaluation_check", Name = "student_evaluation_check")]
  public class StudentEvaluationCheckController:Controller
  {
    private readonly Db _db;
    public StudentEvaluationCheckController(Db db)
    {
      _db = db;
    }
    [HttpGet("curriculum/{student_uid}")]
    public async Task GetEvaluation([FromRoute]Guid student_uid)
    {
      var studentEvaluationCalendars = await _db.v_student_evaluation_calendar.AsNoTracking()
        .Where(e => e.start_date <= DateTime.Today && e.end_date >= DateTime.Today &&
                    e.student_uid == student_uid).ToListAsync();

      foreach (var studentEvaluationCalendar in studentEvaluationCalendars)
      {
        //Get Evaluation Set
        var facultyCurriculumEvaluationSets = await _db.t_question_set_curriculum.AsNoTracking().Where(q =>
          q.academic_year_uid == studentEvaluationCalendar
            .academic_year_uid && q.academic_semester_uid == studentEvaluationCalendar.academic_semester_uid &&
          q.faculty_curriculum_uid == studentEvaluationCalendar.faculty_curriculum_uid).ToListAsync();
        var studentEvaluation = new t_student_evaluation(studentEvaluationCalendar);
        studentEvaluation.student_evaluation_question_sets = facultyCurriculumEvaluationSets
          .Select(f => new t_student_evaluation_question_set(f)).ToList();
        await _db.t_student_evaluation.AddAsync(studentEvaluation);


      }

      await _db.SaveChangesAsync();
    }
    
    [HttpGet("faculty/{student_uid}")]
    public async Task GetFacultyEvaluation([FromRoute]Guid student_uid)
    {
      var studentEvaluationCalendars = await _db.v_student_evaluation_calendar_faculty.AsNoTracking()
        .Where(e => e.start_date <= DateTime.Today && e.end_date >= DateTime.Today &&
                    e.student_uid == student_uid).ToListAsync();

      foreach (var studentEvaluationCalendar in studentEvaluationCalendars)
      {
        //Get Evaluation Set
        var facultyCurriculumEvaluationSets = await _db.t_question_set_faculty.AsNoTracking().Where(q =>
          q.academic_year_uid == studentEvaluationCalendar
            .academic_year_uid && q.academic_semester_uid == studentEvaluationCalendar.academic_semester_uid &&
          q.college_faculty_uid == studentEvaluationCalendar.college_faculty_uid).ToListAsync();
        var studentEvaluation = new t_student_evaluation(studentEvaluationCalendar);
        studentEvaluation.student_evaluation_question_sets = facultyCurriculumEvaluationSets
          .Select(f => new t_student_evaluation_question_set(f)).ToList();
        await _db.t_student_evaluation.AddAsync(studentEvaluation);
      }

      await _db.SaveChangesAsync();
    }
    
    

     


  }
}
