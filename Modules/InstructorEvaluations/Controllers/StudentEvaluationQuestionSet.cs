using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using edu_api.Modules.InstructorEvaluations.Databases.Models;
using edu_api.Modules.InstructorEvaluations.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SeventyOneDev.Utilities;

namespace edu_api.Modules.InstructorEvaluations.Controllers
{
  [Route("api/instructor_evaluations/student_evaluation_question_set", Name = "student_evaluation_question_set")]
  public class StudentEvaluationQuestionSetController:BaseUidController<t_student_evaluation_question_set,v_student_evaluation_question_set>
  {
    private readonly Db _db;
    public StudentEvaluationQuestionSetController(StudentEvaluationQuestionSetService entityUidService,Db db) : base(entityUidService)
    {
      _db = db;
    }

    public override async Task<ActionResult> UpdateEntityId(t_student_evaluation_question_set entity)
    {
      var studentEvaluation =
        await _db.t_student_evaluation.FirstOrDefaultAsync(s => s.student_evaluation_uid == entity.student_evaluation_uid);
      if (studentEvaluation is not null)
      {
        studentEvaluation.status_id = 1;
        studentEvaluation.evaluation_date = DateTime.Today;
        await _db.SaveChangesAsync();
      }
      return await base.UpdateEntityId(entity);
    }

    public override async Task<ActionResult> UpdateEntityId(List<t_student_evaluation_question_set> entities)
    {
      foreach (var entity in entities)
      {
        var studentEvaluation =
          await _db.t_student_evaluation.FirstOrDefaultAsync(s => s.student_evaluation_uid == entity.student_evaluation_uid);
        if (studentEvaluation is not null)
        {
          studentEvaluation.status_id = 1;
          studentEvaluation.evaluation_date = DateTime.Today;
          await _db.SaveChangesAsync();
        }
      }
      return await base.UpdateEntityId(entities);
    }
  }
}
