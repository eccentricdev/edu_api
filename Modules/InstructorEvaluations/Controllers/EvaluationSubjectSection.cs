using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using edu_api.Modules.InstructorEvaluations.Databases.Models;
using edu_api.Modules.InstructorEvaluations.Services;
using Microsoft.AspNetCore.Mvc;
using SeventyOneDev.Utilities;

namespace edu_api.Modules.InstructorEvaluations.Controllers
{
    [Route("api/instructor_evaluations/evaluation_subject_section",Name="evaluation_subject_section")]
    public class EvaluationSubjectSectionController:Controller
  {
    readonly EvaluationSubjectSectionService _evaluationSubjectSectionService;
    
    public EvaluationSubjectSectionController(EvaluationSubjectSectionService evaluationSubjectSectionService){
      _evaluationSubjectSectionService = evaluationSubjectSectionService;
    }

    [HttpGet]
    public async Task<ActionResult<List<v_student_evaluation_subject>>> GetEvaluationSubjectSections(
      [FromQuery] v_student_evaluation_subject _student_evaluation_subject = null)
    {
      if (EFExtension.IsNullOrEmpty(_student_evaluation_subject))
      {
        var listResult = await _evaluationSubjectSectionService.GetEvalutionSubject(null);
        return Ok(listResult);
      }
      else
      {
        var getResult = await _evaluationSubjectSectionService.GetEvalutionSubject(_student_evaluation_subject);
        if (!getResult.Any())
        {
          return Ok(new List<v_student_evaluation_subject>());
        }
        else
        {
          return getResult.ToList();
        }

      }

    }

    [HttpPost]
    public async Task<ActionResult<v_student_evaluation_subject>> GetEvaluationSubjectSectionResults(
      [FromBody] v_student_evaluation_subject _student_evaluation_subject)
    {
      // if (EFExtension.IsNullOrEmpty(_student_evaluation_subject))
      // {
      //   var listResult = await _evaluationResultService.GetEvalutionSubjectResult(_student_evaluation_subject);
      //   return Ok(listResult);
      // }
      // else
      // {
      var getResult = await _evaluationSubjectSectionService.GetEvalutionSubjectResult(_student_evaluation_subject);
      return Ok(getResult); //< getResult >;)
      // if ()
      // {
      return Ok(new List<v_student_evaluation_subject>());
      // }
      // else
      // {
      //   return getResult.ToList();
      // }

      //}

    }
  }
}