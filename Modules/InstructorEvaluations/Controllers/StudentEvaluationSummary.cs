using System.Collections.Generic;
using System.Threading.Tasks;
using edu_api.Modules.InstructorEvaluations.Databases.Models;
using edu_api.Modules.InstructorEvaluations.Services;
using Microsoft.AspNetCore.Mvc;

namespace edu_api.Modules.InstructorEvaluations.Controllers
{
  [Route("api/instructor_evaluations/student_evaluation_summary", Name = "student_evaluation_summary")]

  public class StudentEvaluationSummaryController:Controller
  {
    private readonly StudentEvaluationSummaryService _studentEvaluationSummaryService;
    public StudentEvaluationSummaryController(StudentEvaluationSummaryService studentEvaluationSummaryService)
    {
      _studentEvaluationSummaryService = studentEvaluationSummaryService;
    }
    [HttpGet]
    public async Task<ActionResult<List<v_student_evaluation_summary>>> GetStudentEvaluationSummary(
      [FromQuery] v_student_evaluation_summary studentEvaluationSummary)
    {
      var result = await _studentEvaluationSummaryService.GetStudentEvaluationSummary(studentEvaluationSummary);
      return Ok(result);
    }
  }
}
