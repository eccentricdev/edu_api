using edu_api.Modules.InstructorEvaluations.Databases.Models;
using Microsoft.AspNetCore.Mvc;
using SeventyOneDev.Utilities;

namespace edu_api.Modules.InstructorEvaluations.Controllers
{
    [ApiController]
    [Route("api/instructor_evaluations/question_set_curriculum", Name = "question_set_curriculum")]

    public class
        QuestionSetCurriculumController : BaseUidController<t_question_set_curriculum, v_question_set_curriculum>
    {
        private readonly Db _db;
        public QuestionSetCurriculumController(
            EntityUidService<t_question_set_curriculum, v_question_set_curriculum> entityUidService,Db db) : base(
            entityUidService)
        {
            _db = db;
        }
    }
}