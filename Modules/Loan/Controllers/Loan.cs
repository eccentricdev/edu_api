using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using edu_api.Modules.Register.Databases;
using edu_api.Modules.Register.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using SeventyOneDev.Utilities;

namespace edu_api.Modules.Loan.Controllers
{
  [Route("api/loan/loan", Name = "loan")]
  public class LoanController:Controller
  {
    private readonly Db _db;
    private readonly IHttpClientFactory _httpClientFactory;
    private readonly open_id_setting _openIdSetting;

    public LoanController(Db db,IHttpClientFactory httpClientFactory,IOptions<open_id_setting> openIdSetting)
    {
      _db = db;
      _httpClientFactory = httpClientFactory;
      _openIdSetting = openIdSetting.Value;
    }
    [Authorize]
    [HttpGet("{student_code}/{contract_no}/{year_code}/{semester_code}/{amount}")]
    public async Task<ActionResult> SetLoan([FromRoute]string student_code,[FromRoute]string contract_no,[FromRoute]string year_code,[FromRoute]string semester_code,  [FromRoute]decimal amount)
    {
      var studentUid = _db.t_student.FirstOrDefault(s => s.student_code == student_code)?.student_uid;
      if (!studentUid.HasValue) return BadRequest("NOT_FOUND_STUDENT");
      var yearUid = _db.t_academic_year.AsNoTracking().FirstOrDefault(y => y.academic_year_code == year_code)?
        .academic_year_uid;
      if (!yearUid.HasValue) return BadRequest("NOT_FOUND_YEAR");
      var semesterUid = _db.t_semester.AsNoTracking().FirstOrDefault(y => y.semester_code == semester_code)?
        .semester_uid;
      if (!semesterUid.HasValue) return BadRequest("NOT_FOUND_SEMESTER");
      var loan = await _db.t_loan.AsNoTracking().FirstOrDefaultAsync(l => l.student_uid == studentUid && l.academic_year_uid==yearUid);
      if (loan==null)
      {
        loan = new t_loan()
        {
          student_uid = studentUid,
          academic_year_uid = yearUid,
          contract_no = contract_no

        };
        await _db.t_loan.AddAsync(loan);
        await _db.SaveChangesAsync();
        var loanSemester = new t_loan_semester()
        {
          loan_uid = loan.loan_uid,
          semester_uid = semesterUid,
          loan_amount = amount,
          use_amount = 0
        };
        await _db.t_loan_semester.AddAsync(loanSemester);
        await _db.SaveChangesAsync();
        return Ok("Add new loan success");




      }
      else
      {
        var loanSemester =
          await _db.t_loan_semester.FirstOrDefaultAsync(s =>
            s.loan_uid == loan.loan_uid && s.semester_uid == semesterUid);
        if (loanSemester == null)
        {
          loanSemester = new t_loan_semester()
          {
            loan_uid = loan.loan_uid,
            semester_uid = semesterUid,
            loan_amount = amount,
            use_amount = 0
          };
          await _db.t_loan_semester.AddAsync(loanSemester);
          await _db.SaveChangesAsync();
          return Ok("Add new loan semester success");
        }
        else
        {
          loanSemester.loan_amount = amount;
          loanSemester.use_amount = 0;
          await _db.SaveChangesAsync();
          return Ok("Update loan success");
        }

      }






    }
  }
}
