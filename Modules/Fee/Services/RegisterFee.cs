using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using edu_api.Modules.Fee.Databases.Models;
using edu_api.Modules.Study.Databases.Models;
using Microsoft.EntityFrameworkCore;
using SeventyOneDev.Utilities;

namespace edu_api.Modules.Fee.Services
{
  public class RegisterFeeService:EntityUidService<t_register_fee,v_register_fee>
  {
    private readonly Db _context;
    public RegisterFeeService(Db context) : base(context)
    {
      _context = context;
    }

    public override async Task<v_register_fee> GetEntity(Guid uid, List<Guid?> agencies = null, string userName = null)
    {
      var yearSubject = await _context.v_register_fee.AsNoTracking()
        .Include(s => s.register_fee_filters).ThenInclude(c=>c.register_fee_filter_curriculums).Include(s => s.register_fee_filters).ThenInclude(c=>c.register_fee_details)
        .Include(s => s.register_fee_details)
        .FirstOrDefaultAsync(s => s.register_fee_uid == uid);

      return yearSubject;
    }
    
      public override async Task<int> UpdateEntity(t_register_fee entity, ClaimsIdentity claimsIdentity)
    {
      if (entity.register_fee_details != null)
      {
        var currentRegisterFeeDetails = await _context.t_register_fee_detail.AsNoTracking()
          .Where(a => a.register_fee_uid == entity.register_fee_uid).ToListAsync();
        var removeRegisterFeeDetails = currentRegisterFeeDetails.Where(c =>
          entity.register_fee_details.All((a => a.register_fee_detail_uid != c.register_fee_detail_uid))).ToList();
        if (removeRegisterFeeDetails.Any())
        {
          _context.t_register_fee_detail.RemoveRange(removeRegisterFeeDetails);
        }
      }

      if (entity.register_fee_filters != null)
      {
        var currentRegisterFeeFilters = await _context.t_register_fee_filter.AsNoTracking()
          .Where(a => a.register_fee_uid == entity.register_fee_uid).Include(c=>c.register_fee_filter_curriculums).Include(c=>c.register_fee_details).ToListAsync();
        var removeRegisterFeeFilters = currentRegisterFeeFilters.Where(c =>
          entity.register_fee_filters.All((a => a.register_fee_filter_uid != c.register_fee_filter_uid))).ToList();
        if (removeRegisterFeeFilters.Any())
        {
          _context.t_register_fee_filter.RemoveRange(removeRegisterFeeFilters);
        }
        
        
        foreach (var currentRegisterFeeFilter in currentRegisterFeeFilters)
        {
          foreach (var registerFeeFilter in entity.register_fee_filters)
          {
            var remove = currentRegisterFeeFilter.register_fee_filter_curriculums.Where(c =>
              registerFeeFilter.register_fee_filter_curriculums.All(a => a.register_fee_filter_curriculum_uid != c.register_fee_filter_curriculum_uid)).ToList();
            if (remove.Count>0)
            {
              _context.t_register_fee_filter_curriculum.RemoveRange(remove);
            }
            
            var remove2 = currentRegisterFeeFilter.register_fee_details.Where(c =>
              registerFeeFilter.register_fee_details.All(a => a.register_fee_detail_uid != c.register_fee_detail_uid)).ToList();
            if (remove.Count>0)
            {
              _context.t_register_fee_detail.RemoveRange(remove2);
            }
         
          }
        }
        

      }
      


      await _context.SaveChangesAsync();
      _context.ChangeTracker.Clear();
      return await base.UpdateEntity(entity, claimsIdentity);
    }


      public override async Task<t_register_fee> NewEntity(t_register_fee entity, ClaimsIdentity claimsIdentity)
      {
        var data = await _context.t_register_fee.AsNoTracking().Where(c =>
            c.education_type_uid == entity.education_type_uid &&
            c.entry_academic_year_uid == entity.entry_academic_year_uid &&
            c.register_fee_uid == entity.register_fee_uid && 
            c.register_type_uid == entity.register_type_uid)
          .ToListAsync();
        if (data.Count > 0)
        {
          throw new Exception("NEW_DUPLICATE");
        }
        else
        {
          return await base.NewEntity(entity, claimsIdentity);
         
        }

       
      }

    
  }
}
