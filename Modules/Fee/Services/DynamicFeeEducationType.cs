using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using edu_api.Modules.Fee.Databases.Models;
using edu_api.Modules.Study.Databases.Models;
using Microsoft.EntityFrameworkCore;
using SeventyOneDev.Utilities;

namespace edu_api.Modules.Fee.Services
{
  public class DynamicFeeEducationTypeService:EntityUidService<t_dynamic_fee_education_type,v_dynamic_fee_education_type>
  {
    private readonly Db _context;
    public DynamicFeeEducationTypeService(Db context) : base(context)
    {
      _context = context;
    }

    public override async Task<v_dynamic_fee_education_type> GetEntity(Guid uid, List<Guid?> agencies = null, string userName = null)
    {
      var DynamicFeeEducationType = await _context.v_dynamic_fee_education_type.AsNoTracking()
        .Include(s => s.education_type_lists)
        .FirstOrDefaultAsync(s => s.dynamic_fee_education_type_uid == uid);

      return DynamicFeeEducationType;
    }
    
    
  }
}
