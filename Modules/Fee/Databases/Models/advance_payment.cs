using System;
using System.ComponentModel.DataAnnotations;
using SeventyOneDev.Utilities;

namespace edu_api.Modules.Fee.Databases.Models
{
  public class advance_payment:base_table
  {
    [Key]
    public Guid? advance_payment_uid { get; set; }
    public Guid? student_uid { get; set; }
    public DateTime? advance_date { get; set; }
    public decimal? advance_amount { get; set; }
    public string advance_no { get; set; }
    public decimal? left_amount { get; set; }
    public bool? is_use { get; set; }
  }

  public class t_advance_payment : advance_payment
  {

  }

  public class v_advance_payment : advance_payment
  {

  }
}
