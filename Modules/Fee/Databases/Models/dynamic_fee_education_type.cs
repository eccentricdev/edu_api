using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;
using edu_api.Modules.Organizations.Databases.Models;
using SeventyOneDev.Utilities;
using SeventyOneDev.Utilities.Attributes;

namespace edu_api.Modules.Fee.Databases.Models
{
  public class dynamic_fee_education_type:base_table
  {
    [Key] public Guid? dynamic_fee_education_type_uid { get; set; }
    public Guid? dynamic_fee_uid { get; set; }
    public decimal fee_amount { get; set; }
    public Guid? dynamic_fee_apply_uid { get; set; }
    public Guid? education_type_uid { get; set; }
  }
  //[GeneratedUidController("api/fee/dynamic_fee_education_type")]
  public class t_dynamic_fee_education_type : dynamic_fee_education_type
  {
    [JsonIgnore]
    public t_dynamic_fee dynamic_fee { get; set; }
    [JsonIgnore]
    public t_dynamic_fee_apply dynamic_fee_apply { get; set; }
    
    [Include]
    public List<t_education_type_list> education_type_lists { get; set; }
  }

  public class v_dynamic_fee_education_type : dynamic_fee_education_type
  {
    [JsonIgnore]
    public v_dynamic_fee dynamic_fee { get; set; }
    [JsonIgnore]
    public v_dynamic_fee_apply dynamic_fee_apply { get; set; }
    [Include]
    public List<v_education_type_list> education_type_lists { get; set; }
  }
}
