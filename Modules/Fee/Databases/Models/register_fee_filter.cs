using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;
using SeventyOneDev.Utilities;
using SeventyOneDev.Utilities.Attributes;

namespace edu_api.Modules.Fee.Databases.Models
{
  public class register_fee_filter:base_table
  {
    [Key]
    public Guid? register_fee_filter_uid { get; set; }
    public Guid? register_fee_uid { get; set; }
    public string register_fee_filter_code { get; set; }
    public string register_fee_filter_name_th { get; set; }
    public string register_fee_filter_name_en { get; set; }
  }
  [GeneratedUidController("api/fee/register_fee_filter")]
  public class t_register_fee_filter : register_fee_filter
  {
    [JsonIgnore]
    public t_register_fee  register_fee { get; set; }
    [Include]
    public List<t_register_fee_filter_curriculum> register_fee_filter_curriculums { get; set; }
    [Include]
    public List<t_register_fee_detail> register_fee_details { get; set; }
  }

  public class v_register_fee_filter : register_fee_filter
  {
    [JsonIgnore]
    public v_register_fee  register_fee { get; set; }
    [Include]
    public List<v_register_fee_filter_curriculum> register_fee_filter_curriculums { get; set; }
    [Include]
    public List<v_register_fee_detail> register_fee_details { get; set; }
  }
}
