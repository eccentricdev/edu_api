using System;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;
using SeventyOneDev.Utilities;

namespace edu_api.Modules.Fee.Databases.Models
{
  public class education_type_fee_condition:base_table
  {
    [Key]
    public Guid? education_type_fee_condition_uid {get;set;}
    public Guid? education_type_fee_uid {get;set;}
    public Guid? academic_year_uid { get; set; }
  }

  public class t_education_type_fee_condition : education_type_fee_condition
  {
    [JsonIgnore]
    public t_education_type_fee education_type_fee { get; set; }
  }

  public class v_education_type_fee_condition : education_type_fee_condition
  {
    [JsonIgnore]
    public v_education_type_fee education_type_fee { get; set; }
  }
}
