using System;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;
using SeventyOneDev.Utilities;

namespace edu_api.Modules.Fee.Databases.Models
{
  public class register_fee_filter_curriculum:base_table
  {
    [Key]
    public Guid? register_fee_filter_curriculum_uid { get; set; }
    public Guid? register_fee_filter_uid { get; set; }
    public Guid? curriculum_uid { get; set; }
  }

  public class t_register_fee_filter_curriculum : register_fee_filter_curriculum
  {
    [JsonIgnore]
    public t_register_fee_filter register_fee_filter { get; set; }
  }

  public class v_register_fee_filter_curriculum : register_fee_filter_curriculum
  {
    public string curriculum_name_th {get;set;}
    [JsonIgnore]
    public v_register_fee_filter register_fee_filter { get; set; }
  }
}
