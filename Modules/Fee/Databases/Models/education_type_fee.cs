using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;
using SeventyOneDev.Utilities;
using SeventyOneDev.Utilities.Attributes;

namespace edu_api.Modules.Fee.Databases.Models
{
  public class education_type_fee:base_table
  {
    [Key]
    public Guid? education_type_fee_uid {get;set;}
    public Guid? fee_uid {get;set;}
    public Guid? education_type_uid {get;set;}
    public decimal? fee_amount {get;set;}
  }
  [GeneratedUidController("api/fee/education_type_fee")]
  public class t_education_type_fee : education_type_fee
  {
    [JsonIgnore]
    public t_fee fee { get; set; }
    [Include]
    public List<t_education_type_fee_condition> education_type_fee_conditions { get; set; }
  }

  public class v_education_type_fee : education_type_fee
  {
    public string education_type_name_th { get; set; }
    public string fee_name_th { get; set; }
    [JsonIgnore]
    public v_fee fee { get; set; }
    [Include]
    public List<v_education_type_fee_condition> education_type_fee_conditions { get; set; }
  }
}
