using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;
using SeventyOneDev.Utilities;
using SeventyOneDev.Utilities.Attributes;

namespace edu_api.Modules.Fee.Databases.Models
{
  public class register_fee:base_table
  {
    [Key]
    public Guid? register_fee_uid {get;set;}
    public Guid? fee_uid {get;set;}
    
    public Guid? register_type_uid {get;set;}
    // public int? curriculum_id {get;set;}
    // public int? academic_semester_id {get;set;}
    
    public Guid? education_type_uid { get; set; }
   
    public Guid? entry_academic_year_uid { get; set; }
   
    public Guid? semester_uid { get; set; }
    // public int? major_id { get; set; }
    // public decimal? amount {get;set;}
    // [MaxLength(10),JsonIgnore]
    // public string education_type_code {get;set;}
    // [MaxLength(10),JsonIgnore]
    // public string faculty_code {get;set;}
    // [MaxLength(10),JsonIgnore]
    // public string major_code {get;set;}
    // [MaxLength(10),JsonIgnore]
    // public string academic_year_code {get;set;}
    // [MaxLength(10),JsonIgnore]
    // public string academic_semester_code {get;set;}
  }
 // [GeneratedUidController("api/fee/register_fee")]
  public class t_register_fee : register_fee
  {
    [JsonIgnore]
    public t_register_type register_type { get; set; }
    [Include]
    public List<t_register_fee_detail> register_fee_details { get; set; }
    [Include]
    public List<t_register_fee_filter> register_fee_filters { get; set; }
  }

  public class v_register_fee : register_fee
  {
    public string fee_name_en { get; set; }
    public string fee_name_th { get; set; }
    [JsonIgnore]
    public v_register_type register_type { get; set; }
    [Include]
    public List<v_register_fee_detail> register_fee_details { get; set; }
    [Include]
    public List<v_register_fee_filter> register_fee_filters { get; set; }
  }
}
