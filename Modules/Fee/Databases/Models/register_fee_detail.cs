using System;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;
using SeventyOneDev.Utilities;

namespace edu_api.Modules.Fee.Databases.Models
{
  public class register_fee_detail:base_table
  {
    [Key]
    public Guid? register_fee_detail_uid {get;set;}
    public Guid? register_fee_uid { get; set; }
    public Guid? fee_uid { get; set; }
    public decimal? fee_amount { get; set; }
    public Guid? register_fee_filter_uid { get; set; }
  }

  public class t_register_fee_detail : register_fee_detail
  {
    [JsonIgnore]
    public t_register_fee register_fee { get; set; }
    [JsonIgnore]
    public t_register_fee_filter register_fee_filter { get; set; }
  }

  public class v_register_fee_detail : register_fee_detail
  {
    public string fee_code { get; set; }
    public string fee_name_th { get; set; }
    public string fee_name_en { get; set; }
    [JsonIgnore]
    public v_register_fee register_fee { get; set; }[JsonIgnore]
    public v_register_fee_filter register_fee_filter { get; set; }

  }
}
