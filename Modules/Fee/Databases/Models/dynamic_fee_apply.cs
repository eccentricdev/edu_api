using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;
using SeventyOneDev.Utilities;
using SeventyOneDev.Utilities.Attributes;

namespace edu_api.Modules.Fee.Databases.Models
{
  public class dynamic_fee_apply:base_table
  {
    [Key] public Guid? dynamic_fee_apply_uid { get; set; }
    public string dynamic_fee_apply_code { get; set; }
    public string dynamic_fee_apply_short_name_en { get; set; }
    public string dynamic_fee_apply_short_name_th { get; set; }
    public string dynamic_fee_apply_name_th { get; set; }
    public string dynamic_fee_apply_name_en { get; set; }
  }
  [GeneratedUidController("api/fee/dynamic_fee_apply")]
  public class t_dynamic_fee_apply : dynamic_fee_apply
  {
    [JsonIgnore]
    public List<t_dynamic_fee_education_type> dynamic_fee_education_types { get; set; }
  }

  public class v_dynamic_fee_apply : dynamic_fee_apply
  {
    [JsonIgnore]
    public List<v_dynamic_fee_education_type> dynamic_fee_education_types { get; set; }
  }
}
