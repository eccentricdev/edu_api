using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;
using SeventyOneDev.Utilities;
using SeventyOneDev.Utilities.Attributes;

namespace edu_api.Modules.Fee.Databases.Models
{
  public class register_type:base_table
  {
    [Key]
    public Guid? register_type_uid {get;set;}
    public string register_type_code {get;set;}
    public string register_type_short_name_en  {get;set;}
    public string register_type_short_name_th  {get;set;}
    public string register_type_name_en  {get;set;}
    public string register_type_name_th  {get;set;}
    public string remark {get;set;}
  }
  [GeneratedUidController("api/fee/register_type")]
  public class t_register_type : register_type
  {
    [JsonIgnore]
    public List<t_register_fee> register_fees { get; set; }
  }

  public class v_register_type : register_type
  {
    [JsonIgnore]
    public List<v_register_fee> register_fees { get; set; }
  }

}
