using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;
using SeventyOneDev.Utilities;
using SeventyOneDev.Utilities.Attributes;

namespace edu_api.Modules.Fee.Databases.Models
{
  public class fee:base_table
  {
    [Key] public Guid? fee_uid { get; set; }
    public string fee_code { get; set; }
    public string fee_short_name_en { get; set; }
    public string fee_short_name_th { get; set; }
    public string fee_name_th { get; set; }
    public string fee_name_en { get; set; }
  }
  [GeneratedUidController("api/fee/fee")]
  public class t_fee : fee
  {
    [JsonIgnore]
    public List<t_education_type_fee> education_type_fees { get; set; }
  }

  public class v_fee : fee
  {
    [JsonIgnore]
    public List<v_education_type_fee> education_type_fees { get; set; }
  }
}
