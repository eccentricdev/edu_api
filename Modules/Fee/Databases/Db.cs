using edu_api.Modules.Fee.Databases.Models;
using Microsoft.EntityFrameworkCore;

namespace SeventyOneDev.Utilities
{
  public partial class Db : DbContext
  {
    public DbSet<t_education_type_fee> t_education_type_fee {get;set;}
    public DbSet<v_education_type_fee> v_education_type_fee {get;set;}
    public DbSet<t_education_type_fee_condition> t_education_type_fee_condition {get;set;}
    public DbSet<v_education_type_fee_condition> v_education_type_fee_condition {get;set;}
    public DbSet<t_fee> t_fee {get;set;}
    public DbSet<v_fee> v_fee {get;set;}
    public DbSet<t_register_fee> t_register_fee {get;set;}
    public DbSet<v_register_fee> v_register_fee {get;set;}
    public DbSet<t_register_fee_detail> t_register_fee_detail {get;set;}
    public DbSet<v_register_fee_detail> v_register_fee_detail {get;set;}
    public DbSet<t_register_fee_filter> t_register_fee_filter {get;set;}
    public DbSet<v_register_fee_filter> v_register_fee_filter {get;set;}
    public DbSet<t_register_fee_filter_curriculum> t_register_fee_filter_curriculum {get;set;}
    public DbSet<v_register_fee_filter_curriculum> v_register_fee_filter_curriculum {get;set;}
    public DbSet<t_register_type> t_register_type {get;set;}
    public DbSet<v_register_type> v_register_type {get;set;}

    public DbSet<t_dynamic_fee> t_dynamic_fee {get;set;}
    public DbSet<v_dynamic_fee> v_dynamic_fee {get;set;}
    public DbSet<t_dynamic_fee_apply> t_dynamic_fee_apply {get;set;}
    public DbSet<v_dynamic_fee_apply> v_dynamic_fee_apply {get;set;}
    public DbSet<t_dynamic_fee_education_type> t_dynamic_fee_education_type {get;set;}
    public DbSet<v_dynamic_fee_education_type> v_dynamic_fee_education_type {get;set;}

    public DbSet<t_advance_payment> t_advance_payment {get;set;}
    public DbSet<v_advance_payment> v_advance_payment {get;set;}

    private void OnFeeModelCreating(ModelBuilder builder, string schema)
    {
      builder.Entity<t_education_type_fee>().ToTable("education_type_fee", schema);
      builder.Entity<t_education_type_fee>().HasOne(e => e.fee).WithMany(e => e.education_type_fees)
        .HasForeignKey(e => e.fee_uid).OnDelete(DeleteBehavior.Restrict);
      builder.Entity<v_education_type_fee>().ToView("v_education_type_fee", schema);
      builder.Entity<v_education_type_fee>().HasOne(e => e.fee).WithMany(e => e.education_type_fees)
        .HasForeignKey(e => e.fee_uid).OnDelete(DeleteBehavior.Restrict);





      builder.Entity<t_education_type_fee_condition>().ToTable("education_type_fee_condition", schema);
      builder.Entity<t_education_type_fee_condition>().HasOne(e => e.education_type_fee)
        .WithMany(e => e.education_type_fee_conditions).HasForeignKey(e => e.education_type_fee_uid)
        .OnDelete(DeleteBehavior.Cascade);
      builder.Entity<v_education_type_fee_condition>().ToView("v_education_type_fee_condition", schema);
      builder.Entity<v_education_type_fee_condition>().HasOne(e => e.education_type_fee)
        .WithMany(e => e.education_type_fee_conditions).HasForeignKey(e => e.education_type_fee_uid)
        .OnDelete(DeleteBehavior.Cascade);

      builder.Entity<t_fee>().ToTable("fee", schema);
      builder.Entity<v_fee>().ToView("v_fee", schema);
      builder.Entity<t_register_fee>().ToTable("register_fee", schema);
      builder.Entity<t_register_fee>().HasOne(r => r.register_type).WithMany(r => r.register_fees)
        .HasForeignKey(r => r.register_type_uid).OnDelete(DeleteBehavior.Restrict);
      builder.Entity<v_register_fee>().ToView("v_register_fee", schema);
      builder.Entity<v_register_fee>().HasOne(r => r.register_type).WithMany(r => r.register_fees)
        .HasForeignKey(r => r.register_type_uid).OnDelete(DeleteBehavior.Restrict);

      builder.Entity<t_register_fee_detail>().ToTable("register_fee_detail", schema);
      builder.Entity<t_register_fee_detail>().HasOne(r => r.register_fee).WithMany(r => r.register_fee_details)
        .HasForeignKey(r => r.register_fee_uid).OnDelete(DeleteBehavior.Cascade);
      builder.Entity<t_register_fee_detail>().HasOne(r => r.register_fee_filter).WithMany(r => r.register_fee_details)
        .HasForeignKey(r => r.register_fee_filter_uid).OnDelete(DeleteBehavior.Cascade);

      builder.Entity<v_register_fee_detail>().ToView("v_register_fee_detail", schema);
      builder.Entity<v_register_fee_detail>().HasOne(r => r.register_fee).WithMany(r => r.register_fee_details)
        .HasForeignKey(r => r.register_fee_uid).OnDelete(DeleteBehavior.Cascade);
      builder.Entity<v_register_fee_detail>().HasOne(r => r.register_fee_filter).WithMany(r => r.register_fee_details)
        .HasForeignKey(r => r.register_fee_filter_uid).OnDelete(DeleteBehavior.Cascade);

      builder.Entity<t_register_fee_filter>().ToTable("register_fee_filter", schema);
      builder.Entity<t_register_fee_filter>().HasOne(r => r.register_fee).WithMany(r => r.register_fee_filters)
        .HasForeignKey(r => r.register_fee_uid).OnDelete(DeleteBehavior.Cascade);
      builder.Entity<v_register_fee_filter>().ToView("v_register_fee_filter", schema);
      builder.Entity<v_register_fee_filter>().HasOne(r => r.register_fee).WithMany(r => r.register_fee_filters)
        .HasForeignKey(r => r.register_fee_uid).OnDelete(DeleteBehavior.Cascade);

      builder.Entity<t_register_fee_filter_curriculum>().ToTable("register_fee_filter_curriculum", schema);
      builder.Entity<t_register_fee_filter_curriculum>().HasOne(r => r.register_fee_filter)
        .WithMany(r => r.register_fee_filter_curriculums).HasForeignKey(r => r.register_fee_filter_uid)
        .OnDelete(DeleteBehavior.Cascade);
      builder.Entity<v_register_fee_filter_curriculum>().ToView("v_register_fee_filter_curriculum", schema);
      builder.Entity<v_register_fee_filter_curriculum>().HasOne(r => r.register_fee_filter)
        .WithMany(r => r.register_fee_filter_curriculums).HasForeignKey(r => r.register_fee_filter_uid)
        .OnDelete(DeleteBehavior.Cascade);
      builder.Entity<t_register_type>().ToTable("register_type", schema);
      builder.Entity<v_register_type>().ToView("v_register_type", schema);


      builder.Entity<t_dynamic_fee>().ToTable("dynamic_fee", schema);
      builder.Entity<v_dynamic_fee>().ToView("v_dynamic_fee", schema);
      builder.Entity<t_dynamic_fee_apply>().ToTable("dynamic_fee_apply", schema);
      builder.Entity<v_dynamic_fee_apply>().ToView("v_dynamic_fee_apply", schema);
      builder.Entity<t_dynamic_fee_education_type>().ToTable("dynamic_fee_education_type", schema);
      builder.Entity<t_dynamic_fee_education_type>().HasOne(d => d.dynamic_fee)
        .WithMany(d => d.dynamic_fee_education_types).HasForeignKey(d => d.dynamic_fee_uid)
        .OnDelete(DeleteBehavior.Restrict);
      builder.Entity<t_dynamic_fee_education_type>().HasOne(d => d.dynamic_fee_apply)
        .WithMany(d => d.dynamic_fee_education_types).HasForeignKey(d => d.dynamic_fee_apply_uid)
        .OnDelete(DeleteBehavior.Restrict);
      builder.Entity<v_dynamic_fee_education_type>().ToView("v_dynamic_fee_education_type", schema);
      builder.Entity<v_dynamic_fee_education_type>().HasOne(d => d.dynamic_fee)
        .WithMany(d => d.dynamic_fee_education_types).HasForeignKey(d => d.dynamic_fee_uid)
        .OnDelete(DeleteBehavior.Restrict);
      builder.Entity<v_dynamic_fee_education_type>().HasOne(d => d.dynamic_fee_apply)
        .WithMany(d => d.dynamic_fee_education_types).HasForeignKey(d => d.dynamic_fee_apply_uid)
        .OnDelete(DeleteBehavior.Restrict);

      builder.Entity<t_advance_payment>().ToTable("advance_payment", schema);
      builder.Entity<v_advance_payment>().ToView("v_advance_payment", schema);
    }
  }
}
