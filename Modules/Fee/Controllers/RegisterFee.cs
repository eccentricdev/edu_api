using edu_api.Modules.Calendars.Databases.Models;
using edu_api.Modules.Fee.Databases.Models;
using edu_api.Modules.Fee.Services;
using edu_api.Modules.Study.Databases.Models;
using edu_api.Modules.Study.Services;
using Microsoft.AspNetCore.Mvc;
using SeventyOneDev.Utilities;

namespace edu_api.Modules.Fee.Controllers
{
  [Route("api/fee/register_fee", Name = "register_fee")]
  public class RegisterFeeController:BaseUidController<t_register_fee,v_register_fee>
  {
    public RegisterFeeController(RegisterFeeService entityUidService) : base(entityUidService)
    {

    }
  }
}
