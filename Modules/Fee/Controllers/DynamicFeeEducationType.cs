using edu_api.Modules.Calendars.Databases.Models;
using edu_api.Modules.Fee.Databases.Models;
using edu_api.Modules.Fee.Services;
using edu_api.Modules.Study.Databases.Models;
using edu_api.Modules.Study.Services;
using Microsoft.AspNetCore.Mvc;
using SeventyOneDev.Utilities;

namespace edu_api.Modules.Fee.Controllers
{
  [Route("api/fee/dynamic_fee_education_type", Name = "dynamic_fee_education_type")]
  public class DynamicFeeEducationTypeController:BaseUidController<t_dynamic_fee_education_type,v_dynamic_fee_education_type>
  {
    public DynamicFeeEducationTypeController(DynamicFeeEducationTypeService entityUidService) : base(entityUidService)
    {

    }
  }
}
