using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using edu_api.Modules.Utilities.Models;
using Humanizer;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using SeventyOneDev.Core.Security.Models;

namespace SeventyOneDev.Utilities
{
    [Route("/api/utilities/upload", Name = "upload")]
    public class DocumentUploadController : Controller
    {
        private readonly application_setting _applicationSetting;

        public DocumentUploadController(IOptions<application_setting> applicationSetting)
        {
            _applicationSetting = applicationSetting.Value;
        }

        [HttpPost]
        [RequestSizeLimit(26214400)]
        public async Task<List<upload_document>> DocumentUpload()
        {
            var files = Request.Form.Files;
            if (!Directory.Exists(_applicationSetting.document_path))
            {
                Directory.CreateDirectory(_applicationSetting.document_path);
            }

            var uploads = Path.Combine(_applicationSetting.document_path);
            List<upload_document> fileNames = new List<upload_document>();
            foreach (var file in files)
            {
                
                if (file.Length > 0 )
                {
                    if (!file.FileName.Contains("."))
                    {
                        Console.WriteLine("File Name:" + file.FileName);
                    }

                    var newFileName = Guid.NewGuid().ToString() +
                                      file.FileName.Substring(file.FileName.LastIndexOf('.'), 4);
                    using (var fileStream = new FileStream(Path.Combine(uploads, newFileName), FileMode.Create))
                    {
                        await file.CopyToAsync(fileStream);
                    }

                    fileNames.Add(new upload_document()
                    {
                        file_name = _applicationSetting.document_url + newFileName, name = file.FileName,
                        mime_type = file.ContentType
                    });
                }
            }

            return fileNames;

        }
    }
}