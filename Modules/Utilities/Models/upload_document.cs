namespace edu_api.Modules.Utilities.Models
{
  public class upload_document
  {
      public string name { get; set; }
      public string path { get; set; }
      public string status {get;set;}
      public string file_name {get;set;}
      public string file_id {get;set;}
      public string url {get;set;}
      public string mime_type {get;set;}
  }
}
