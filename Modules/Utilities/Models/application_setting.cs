namespace edu_api.Modules.Utilities.Models
{
  public class application_setting
  {
      public string temporary_path {get;set;}
      public string document_path {get;set;}
      public string document_url {get;set;}

      public string elastic_search {get;set;}
      public string report_url {get;set;}
      //public string scholarship_url { get; set; }
      //public string scholarship_api_key { get; set; }
  }
}
