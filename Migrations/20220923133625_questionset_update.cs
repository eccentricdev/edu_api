﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace edu_api.Migrations
{
    public partial class questionset_update : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "subject_year_uid",
                schema: "EDU",
                table: "question_set_subject",
                newName: "year_subject_uid");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "year_subject_uid",
                schema: "EDU",
                table: "question_set_subject",
                newName: "subject_year_uid");
        }
    }
}
