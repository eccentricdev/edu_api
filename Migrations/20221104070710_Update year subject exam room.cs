﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace edu_api.Migrations
{
    public partial class Updateyearsubjectexamroom : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "end_datetime",
                schema: "EDU",
                table: "year_subject_exam_room",
                type: "TIMESTAMP(7)",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "start_datetime",
                schema: "EDU",
                table: "year_subject_exam_room",
                type: "TIMESTAMP(7)",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "end_datetime",
                schema: "EDU",
                table: "year_subject_exam_room");

            migrationBuilder.DropColumn(
                name: "start_datetime",
                schema: "EDU",
                table: "year_subject_exam_room");
        }
    }
}
