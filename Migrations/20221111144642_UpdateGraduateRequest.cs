﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace edu_api.Migrations
{
    public partial class UpdateGraduateRequest : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<decimal>(
                name: "total_amout",
                schema: "EDU",
                table: "student_request",
                type: "DECIMAL(18, 2)",
                nullable: true,
                oldClrType: typeof(decimal),
                oldType: "DECIMAL(18,2)",
                oldNullable: true);

            migrationBuilder.AddColumn<string>(
                name: "bill_no",
                schema: "EDU",
                table: "graduate_request",
                type: "NVARCHAR2(50)",
                maxLength: 50,
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "bill_uid",
                schema: "EDU",
                table: "graduate_request",
                type: "RAW(16)",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "step",
                schema: "EDU",
                table: "graduate_request",
                type: "NUMBER(10)",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "graduate_request_payment",
                schema: "EDU",
                columns: table => new
                {
                    graduate_request_payment_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
                    status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
                    created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
                    updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
                    owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
                    graduate_request_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
                    is_pick_up_by_yourself = table.Column<bool>(type: "NUMBER(1)", nullable: true),
                    total_amout = table.Column<decimal>(type: "DECIMAL(18, 2)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_graduate_request_payment", x => x.graduate_request_payment_uid);
                    table.ForeignKey(
                        name: "FK_graduate_request_payment_graduate_request_graduate_request_uid",
                        column: x => x.graduate_request_uid,
                        principalSchema: "EDU",
                        principalTable: "graduate_request",
                        principalColumn: "graduate_request_uid",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "graduate_request_profile",
                schema: "EDU",
                columns: table => new
                {
                    graduate_request_profile_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
                    status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
                    created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
                    updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
                    owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
                    graduate_request_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
                    is_wrong_name_th = table.Column<bool>(type: "NUMBER(1)", nullable: true),
                    correct_name_th = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    is_wrong_last_name_th = table.Column<bool>(type: "NUMBER(1)", nullable: true),
                    correct_last_name_th = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    is_wrong_name_en = table.Column<bool>(type: "NUMBER(1)", nullable: true),
                    correct_name_en = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    is_wrong_last_name_en = table.Column<bool>(type: "NUMBER(1)", nullable: true),
                    correct_last_name_en = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    is_wrong_date_of_birth = table.Column<bool>(type: "NUMBER(1)", nullable: true),
                    correct_date_of_birth = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
                    is_wrong_citizen_id = table.Column<bool>(type: "NUMBER(1)", nullable: true),
                    correct_citizen_id = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_graduate_request_profile", x => x.graduate_request_profile_uid);
                    table.ForeignKey(
                        name: "FK_graduate_request_profile_graduate_request_graduate_request_uid",
                        column: x => x.graduate_request_uid,
                        principalSchema: "EDU",
                        principalTable: "graduate_request",
                        principalColumn: "graduate_request_uid",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "graduate_request_payment_detail",
                schema: "EDU",
                columns: table => new
                {
                    graduate_request_payment_detail_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
                    status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
                    created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
                    updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
                    owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
                    graduate_request_payment_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
                    graduate_document_type_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
                    amout = table.Column<decimal>(type: "DECIMAL(18, 2)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_graduate_request_payment_detail", x => x.graduate_request_payment_detail_uid);
                    table.ForeignKey(
                        name: "FK_graduate_request_payment_detail_graduate_request_payment_graduate_request_payment_uid",
                        column: x => x.graduate_request_payment_uid,
                        principalSchema: "EDU",
                        principalTable: "graduate_request_payment",
                        principalColumn: "graduate_request_payment_uid",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "graduate_request_profile_address",
                schema: "EDU",
                columns: table => new
                {
                    graduate_request_profile_address_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
                    status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
                    created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
                    updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
                    owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
                    graduate_request_profile_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
                    address_type_id = table.Column<int>(type: "NUMBER(10)", nullable: true),
                    house_no = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
                    sub_district_id = table.Column<int>(type: "NUMBER(10)", nullable: true),
                    district_id = table.Column<int>(type: "NUMBER(10)", nullable: true),
                    province_id = table.Column<int>(type: "NUMBER(10)", nullable: true),
                    postal_code = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
                    telephone_no = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
                    email = table.Column<string>(type: "NVARCHAR2(2000)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_graduate_request_profile_address", x => x.graduate_request_profile_address_uid);
                    table.ForeignKey(
                        name: "FK_graduate_request_profile_address_graduate_request_profile_graduate_request_profile_uid",
                        column: x => x.graduate_request_profile_uid,
                        principalSchema: "EDU",
                        principalTable: "graduate_request_profile",
                        principalColumn: "graduate_request_profile_uid",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "graduate_request_profile_documentt",
                schema: "EDU",
                columns: table => new
                {
                    graduate_request_profile_document_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
                    status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
                    created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
                    updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
                    owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
                    graduate_request_profile_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
                    request_document_type_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
                    request_document_type_name = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    file_name = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    document_name = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    document_url = table.Column<string>(type: "NVARCHAR2(500)", maxLength: 500, nullable: true),
                    mime_type = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
                    document_status_id = table.Column<int>(type: "NUMBER(10)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_graduate_request_profile_documentt", x => x.graduate_request_profile_document_uid);
                    table.ForeignKey(
                        name: "FK_graduate_request_profile_documentt_graduate_request_profile_graduate_request_profile_uid",
                        column: x => x.graduate_request_profile_uid,
                        principalSchema: "EDU",
                        principalTable: "graduate_request_profile",
                        principalColumn: "graduate_request_profile_uid",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_graduate_request_payment_graduate_request_uid",
                schema: "EDU",
                table: "graduate_request_payment",
                column: "graduate_request_uid");

            migrationBuilder.CreateIndex(
                name: "IX_graduate_request_payment_detail_graduate_request_payment_uid",
                schema: "EDU",
                table: "graduate_request_payment_detail",
                column: "graduate_request_payment_uid");

            migrationBuilder.CreateIndex(
                name: "IX_graduate_request_profile_graduate_request_uid",
                schema: "EDU",
                table: "graduate_request_profile",
                column: "graduate_request_uid");

            migrationBuilder.CreateIndex(
                name: "IX_graduate_request_profile_address_graduate_request_profile_uid",
                schema: "EDU",
                table: "graduate_request_profile_address",
                column: "graduate_request_profile_uid");

            migrationBuilder.CreateIndex(
                name: "IX_graduate_request_profile_documentt_graduate_request_profile_uid",
                schema: "EDU",
                table: "graduate_request_profile_documentt",
                column: "graduate_request_profile_uid");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "graduate_request_payment_detail",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "graduate_request_profile_address",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "graduate_request_profile_documentt",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "graduate_request_payment",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "graduate_request_profile",
                schema: "EDU");

            migrationBuilder.DropColumn(
                name: "bill_no",
                schema: "EDU",
                table: "graduate_request");

            migrationBuilder.DropColumn(
                name: "bill_uid",
                schema: "EDU",
                table: "graduate_request");

            migrationBuilder.DropColumn(
                name: "step",
                schema: "EDU",
                table: "graduate_request");

            migrationBuilder.AlterColumn<decimal>(
                name: "total_amout",
                schema: "EDU",
                table: "student_request",
                type: "DECIMAL(18,2)",
                nullable: true,
                oldClrType: typeof(decimal),
                oldType: "DECIMAL(18, 2)",
                oldNullable: true);
        }
    }
}
