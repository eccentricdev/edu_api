﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace edu_api.Migrations
{
    public partial class Updatefinal_register_subject : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            // migrationBuilder.AlterColumn<decimal>(
            //     name: "max_re_grade_credit",
            //     schema: "EDU",
            //     table: "grade_calculation_setting",
            //     type: "decimal(5,2)",
            //     nullable: true,
            //     oldClrType: typeof(int),
            //     oldType: "NUMBER(10)",
            //     oldNullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "grade_criteria_uid",
                schema: "EDU",
                table: "final_register_subject",
                type: "RAW(16)",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "grade_criteria_uid",
                schema: "EDU",
                table: "final_register_subject");

            migrationBuilder.AlterColumn<int>(
                name: "max_re_grade_credit",
                schema: "EDU",
                table: "grade_calculation_setting",
                type: "NUMBER(10)",
                nullable: true,
                oldClrType: typeof(decimal),
                oldType: "decimal(5,2)",
                oldNullable: true);
        }
    }
}
