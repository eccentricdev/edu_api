﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace edu_api.Migrations
{
    public partial class AddRequestCost : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "from_date",
                schema: "EDU",
                table: "student_request");

            migrationBuilder.DropColumn(
                name: "to_date",
                schema: "EDU",
                table: "student_request");

            migrationBuilder.CreateTable(
                name: "request_cost",
                schema: "EDU",
                columns: table => new
                {
                    request_cost_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
                    status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
                    created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
                    updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
                    owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
                    request_type_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
                    request_cost_amount = table.Column<decimal>(type: "decimal(18,4)", precision: 18, scale: 4, nullable: true),
                    request_cost_type_uid = table.Column<Guid>(type: "RAW(16)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_request_cost", x => x.request_cost_uid);
                });

            migrationBuilder.CreateTable(
                name: "request_cost_type",
                schema: "EDU",
                columns: table => new
                {
                    request_cost_type_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
                    status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
                    created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
                    updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
                    owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
                    request_cost_type_code = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
                    request_cost_type_name_th = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    request_cost_type_name_en = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_request_cost_type", x => x.request_cost_type_uid);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "request_cost",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "request_cost_type",
                schema: "EDU");

            migrationBuilder.AddColumn<DateTime>(
                name: "from_date",
                schema: "EDU",
                table: "student_request",
                type: "date",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "to_date",
                schema: "EDU",
                table: "student_request",
                type: "date",
                nullable: true);
        }
    }
}
