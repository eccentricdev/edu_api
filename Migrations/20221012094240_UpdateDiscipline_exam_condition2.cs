﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace edu_api.Migrations
{
    public partial class UpdateDiscipline_exam_condition2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "discipline_exam_condition_detail_sub_uid",
                schema: "EDU",
                table: "student_discipline_exam");

            migrationBuilder.DropColumn(
                name: "discipline_exam_condition_detail_uid",
                schema: "EDU",
                table: "student_discipline_exam");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<Guid>(
                name: "discipline_exam_condition_detail_sub_uid",
                schema: "EDU",
                table: "student_discipline_exam",
                type: "RAW(16)",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "discipline_exam_condition_detail_uid",
                schema: "EDU",
                table: "student_discipline_exam",
                type: "RAW(16)",
                nullable: true);
        }
    }
}
