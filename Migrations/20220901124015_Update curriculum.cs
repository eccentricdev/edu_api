﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace edu_api.Migrations
{
    public partial class Updatecurriculum : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "curriculum_year_code",
                schema: "EDU",
                table: "curriculum_master");

            migrationBuilder.RenameColumn(
                name: "curriculum_year",
                schema: "EDU",
                table: "subject_master_year",
                newName: "register_year");

            migrationBuilder.RenameColumn(
                name: "entry_year",
                schema: "EDU",
                table: "curriculum_master",
                newName: "curriculum_year");

            migrationBuilder.AddColumn<string>(
                name: "subject_master_seq_code",
                schema: "EDU",
                table: "subject_master_owner",
                type: "NVARCHAR2(50)",
                maxLength: 50,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "subject_master_seq_code",
                schema: "EDU",
                table: "curriculum_subject_master",
                type: "NVARCHAR2(50)",
                maxLength: 50,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "academic_year",
                schema: "EDU",
                table: "curriculum_master",
                type: "NVARCHAR2(2000)",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "subject_master_seq_code",
                schema: "EDU",
                table: "subject_master_owner");

            migrationBuilder.DropColumn(
                name: "subject_master_seq_code",
                schema: "EDU",
                table: "curriculum_subject_master");

            migrationBuilder.DropColumn(
                name: "academic_year",
                schema: "EDU",
                table: "curriculum_master");

            migrationBuilder.RenameColumn(
                name: "register_year",
                schema: "EDU",
                table: "subject_master_year",
                newName: "curriculum_year");

            migrationBuilder.RenameColumn(
                name: "curriculum_year",
                schema: "EDU",
                table: "curriculum_master",
                newName: "entry_year");

            migrationBuilder.AddColumn<string>(
                name: "curriculum_year_code",
                schema: "EDU",
                table: "curriculum_master",
                type: "NVARCHAR2(50)",
                maxLength: 50,
                nullable: true);
        }
    }
}
