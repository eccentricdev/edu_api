﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace edu_api.Migrations
{
    public partial class UpdatedeStudent_discipline3 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "case_owner_teacher_code",
                schema: "EDU",
                table: "student_service_activity");

            migrationBuilder.DropColumn(
                name: "case_owner_teacher_code",
                schema: "EDU",
                table: "student_discipline_exam");

            migrationBuilder.DropColumn(
                name: "case_owner_teacher_code",
                schema: "EDU",
                table: "student_discipline_detail");

            migrationBuilder.AddColumn<Guid>(
                name: "case_owner_teacher_uid",
                schema: "EDU",
                table: "student_service_activity",
                type: "RAW(16)",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "case_owner_teacher_uid",
                schema: "EDU",
                table: "student_discipline_exam",
                type: "RAW(16)",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "case_owner_teacher_uid",
                schema: "EDU",
                table: "student_discipline_detail",
                type: "RAW(16)",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "case_owner_teacher_uid",
                schema: "EDU",
                table: "student_service_activity");

            migrationBuilder.DropColumn(
                name: "case_owner_teacher_uid",
                schema: "EDU",
                table: "student_discipline_exam");

            migrationBuilder.DropColumn(
                name: "case_owner_teacher_uid",
                schema: "EDU",
                table: "student_discipline_detail");

            migrationBuilder.AddColumn<string>(
                name: "case_owner_teacher_code",
                schema: "EDU",
                table: "student_service_activity",
                type: "NVARCHAR2(50)",
                maxLength: 50,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "case_owner_teacher_code",
                schema: "EDU",
                table: "student_discipline_exam",
                type: "NVARCHAR2(50)",
                maxLength: 50,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "case_owner_teacher_code",
                schema: "EDU",
                table: "student_discipline_detail",
                type: "NVARCHAR2(50)",
                maxLength: 50,
                nullable: true);
        }
    }
}
