﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace edu_api.Migrations
{
    public partial class Addinstructoravailability : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "instructor_availability",
                schema: "EDU",
                columns: table => new
                {
                    instructor_availability_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
                    status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
                    created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
                    updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
                    owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
                    instructor_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
                    start_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
                    end_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
                    next_available_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
                    previous_instructor_availability_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
                    year_subject_exam_room_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
                    is_busy = table.Column<bool>(type: "NUMBER(1)", nullable: true),
                    remark = table.Column<string>(type: "NVARCHAR2(1000)", maxLength: 1000, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_instructor_availability", x => x.instructor_availability_uid);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "instructor_availability",
                schema: "EDU");
        }
    }
}
