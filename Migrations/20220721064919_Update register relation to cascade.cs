﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace edu_api.Migrations
{
    public partial class Updateregisterrelationtocascade : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_final_register_subject_final_register_final_register_uid",
                schema: "EDU",
                table: "final_register_subject");

            migrationBuilder.AddForeignKey(
                name: "FK_final_register_subject_final_register_final_register_uid",
                schema: "EDU",
                table: "final_register_subject",
                column: "final_register_uid",
                principalSchema: "EDU",
                principalTable: "final_register",
                principalColumn: "final_register_uid",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_final_register_subject_final_register_final_register_uid",
                schema: "EDU",
                table: "final_register_subject");

            migrationBuilder.AddForeignKey(
                name: "FK_final_register_subject_final_register_final_register_uid",
                schema: "EDU",
                table: "final_register_subject",
                column: "final_register_uid",
                principalSchema: "EDU",
                principalTable: "final_register",
                principalColumn: "final_register_uid",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
