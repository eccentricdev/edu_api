﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace edu_api.Migrations
{
    public partial class Changeresponsibleperson2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<Guid>(
                name: "responsible_instructor_uid",
                schema: "EDU",
                table: "year_subject_section",
                type: "RAW(16)",
                nullable: true,
                oldClrType: typeof(bool),
                oldType: "NUMBER(1)",
                oldNullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<bool>(
                name: "responsible_instructor_uid",
                schema: "EDU",
                table: "year_subject_section",
                type: "NUMBER(1)",
                nullable: true,
                oldClrType: typeof(Guid),
                oldType: "RAW(16)",
                oldNullable: true);
        }
    }
}
