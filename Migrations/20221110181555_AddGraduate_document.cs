﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace edu_api.Migrations
{
    public partial class AddGraduate_document : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<decimal>(
                name: "total_amout",
                schema: "EDU",
                table: "student_request",
                type: "DECIMAL(18, 2)",
                nullable: true,
                oldClrType: typeof(decimal),
                oldType: "DECIMAL(18,2)",
                oldNullable: true);

            migrationBuilder.CreateTable(
                name: "graduate_document_cost",
                schema: "EDU",
                columns: table => new
                {
                    graduate_document_cost_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
                    status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
                    created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
                    updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
                    owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
                    graduate_document_type_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
                    graduate_document_cost_amount = table.Column<decimal>(type: "decimal(18,4)", precision: 18, scale: 4, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_graduate_document_cost", x => x.graduate_document_cost_uid);
                });

            migrationBuilder.CreateTable(
                name: "graduate_document_type",
                schema: "EDU",
                columns: table => new
                {
                    graduate_document_type_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
                    status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
                    created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
                    updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
                    owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
                    graduate_document_type_code = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
                    graduate_document_type_name_th = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    graduate_document_type_name_en = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    is_used = table.Column<bool>(type: "NUMBER(1)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_graduate_document_type", x => x.graduate_document_type_uid);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "graduate_document_cost",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "graduate_document_type",
                schema: "EDU");

            migrationBuilder.AlterColumn<decimal>(
                name: "total_amout",
                schema: "EDU",
                table: "student_request",
                type: "DECIMAL(18,2)",
                nullable: true,
                oldClrType: typeof(decimal),
                oldType: "DECIMAL(18, 2)",
                oldNullable: true);
        }
    }
}
