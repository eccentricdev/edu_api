using Microsoft.EntityFrameworkCore.Migrations;

namespace edu_api.Migrations
{
    public partial class Addstudytimetoyear_subject_secton : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "study_time_text_en",
                schema: "EDU",
                table: "year_subject_section",
                type: "NVARCHAR2(200)",
                maxLength: 200,
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "study_time_text_en",
                schema: "EDU",
                table: "year_subject_section");
        }
    }
}
