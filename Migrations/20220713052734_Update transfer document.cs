﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace edu_api.Migrations
{
    public partial class Updatetransferdocument : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "document_type_name",
                schema: "EDU",
                table: "transfer_leisure_document",
                newName: "request_document_type_name");

            migrationBuilder.RenameColumn(
                name: "document_type_name",
                schema: "EDU",
                table: "transfer_institute_document",
                newName: "request_document_type_name");

            migrationBuilder.RenameColumn(
                name: "document_type_name",
                schema: "EDU",
                table: "transfer_degree_document",
                newName: "request_document_type_name");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "request_document_type_name",
                schema: "EDU",
                table: "transfer_leisure_document",
                newName: "document_type_name");

            migrationBuilder.RenameColumn(
                name: "request_document_type_name",
                schema: "EDU",
                table: "transfer_institute_document",
                newName: "document_type_name");

            migrationBuilder.RenameColumn(
                name: "request_document_type_name",
                schema: "EDU",
                table: "transfer_degree_document",
                newName: "document_type_name");
        }
    }
}
