﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace edu_api.Migrations
{
    public partial class AddGraduateRegister : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            // migrationBuilder.AlterColumn<decimal>(
            //     name: "total_amout",
            //     schema: "EDU",
            //     table: "student_request",
            //     type: "DECIMAL(18, 2)",
            //     nullable: true,
            //     oldClrType: typeof(decimal),
            //     oldType: "DECIMAL(18,2)",
            //     oldNullable: true);
            //
            // migrationBuilder.AlterColumn<decimal>(
            //     name: "amout",
            //     schema: "EDU",
            //     table: "graduate_request_payment_detail",
            //     type: "DECIMAL(18, 2)",
            //     nullable: true,
            //     oldClrType: typeof(decimal),
            //     oldType: "DECIMAL(18,2)",
            //     oldNullable: true);
            //
            // migrationBuilder.AlterColumn<decimal>(
            //     name: "total_amout",
            //     schema: "EDU",
            //     table: "graduate_request_payment",
            //     type: "DECIMAL(18, 2)",
            //     nullable: true,
            //     oldClrType: typeof(decimal),
            //     oldType: "DECIMAL(18,2)",
            //     oldNullable: true);

            migrationBuilder.CreateTable(
                name: "graduate_register",
                schema: "EDU",
                columns: table => new
                {
                    graduate_register_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
                    status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
                    created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
                    updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
                    owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
                    graduate_register_code = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
                    student_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
                    request_date = table.Column<DateTime>(type: "date", nullable: true),
                    is_check = table.Column<bool>(type: "NUMBER(1)", nullable: true),
                    is_approve = table.Column<bool>(type: "NUMBER(1)", nullable: true),
                    bill_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
                    bill_no = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
                    is_get_degree = table.Column<bool>(type: "NUMBER(1)", nullable: true),
                    total_amout = table.Column<decimal>(type: "decimal(18,2)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_graduate_register", x => x.graduate_register_uid);
                });

            migrationBuilder.CreateTable(
                name: "graduate_register_payment",
                schema: "EDU",
                columns: table => new
                {
                    graduate_register_payment_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
                    status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
                    created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
                    updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
                    owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
                    graduate_register_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
                    graduate_document_type_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
                    amout = table.Column<decimal>(type: "DECIMAL(18, 2)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_graduate_register_payment", x => x.graduate_register_payment_uid);
                    table.ForeignKey(
                        name: "FK_graduate_register_payment_graduate_register_graduate_register_uid",
                        column: x => x.graduate_register_uid,
                        principalSchema: "EDU",
                        principalTable: "graduate_register",
                        principalColumn: "graduate_register_uid",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "graduate_register_question",
                schema: "EDU",
                columns: table => new
                {
                    graduate_register_question_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
                    status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
                    created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
                    updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
                    owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
                    graduate_register_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
                    question_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
                    answer_uid = table.Column<Guid>(type: "RAW(16)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_graduate_register_question", x => x.graduate_register_question_uid);
                    table.ForeignKey(
                        name: "FK_graduate_register_question_graduate_register_graduate_register_uid",
                        column: x => x.graduate_register_uid,
                        principalSchema: "EDU",
                        principalTable: "graduate_register",
                        principalColumn: "graduate_register_uid",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_graduate_register_payment_graduate_register_uid",
                schema: "EDU",
                table: "graduate_register_payment",
                column: "graduate_register_uid");

            migrationBuilder.CreateIndex(
                name: "IX_graduate_register_question_graduate_register_uid",
                schema: "EDU",
                table: "graduate_register_question",
                column: "graduate_register_uid");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "graduate_register_payment",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "graduate_register_question",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "graduate_register",
                schema: "EDU");

            migrationBuilder.AlterColumn<decimal>(
                name: "total_amout",
                schema: "EDU",
                table: "student_request",
                type: "DECIMAL(18,2)",
                nullable: true,
                oldClrType: typeof(decimal),
                oldType: "DECIMAL(18, 2)",
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "amout",
                schema: "EDU",
                table: "graduate_request_payment_detail",
                type: "DECIMAL(18,2)",
                nullable: true,
                oldClrType: typeof(decimal),
                oldType: "DECIMAL(18, 2)",
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "total_amout",
                schema: "EDU",
                table: "graduate_request_payment",
                type: "DECIMAL(18,2)",
                nullable: true,
                oldClrType: typeof(decimal),
                oldType: "DECIMAL(18, 2)",
                oldNullable: true);
        }
    }
}
