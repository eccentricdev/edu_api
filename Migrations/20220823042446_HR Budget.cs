﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace edu_api.Migrations
{
    public partial class HRBudget : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "created_datetime",
                schema: "EDU",
                table: "subject_master_year_topic",
                type: "TIMESTAMP(7)",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "created_datetime",
                schema: "EDU",
                table: "subject_master_year_cost",
                type: "TIMESTAMP(7)",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "created_datetime",
                schema: "EDU",
                table: "subject_master_year",
                type: "TIMESTAMP(7)",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "created_datetime",
                schema: "EDU",
                table: "subject_master_owner",
                type: "TIMESTAMP(7)",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "created_datetime",
                schema: "EDU",
                table: "subject_master",
                type: "TIMESTAMP(7)",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "created_datetime",
                schema: "EDU",
                table: "subject_master_year_topic");

            migrationBuilder.DropColumn(
                name: "created_datetime",
                schema: "EDU",
                table: "subject_master_year_cost");

            migrationBuilder.DropColumn(
                name: "created_datetime",
                schema: "EDU",
                table: "subject_master_year");

            migrationBuilder.DropColumn(
                name: "created_datetime",
                schema: "EDU",
                table: "subject_master_owner");

            migrationBuilder.DropColumn(
                name: "created_datetime",
                schema: "EDU",
                table: "subject_master");
        }
    }
}
