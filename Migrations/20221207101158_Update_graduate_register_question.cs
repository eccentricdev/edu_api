﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace edu_api.Migrations
{
    public partial class Update_graduate_register_question : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "answer_uid",
                schema: "EDU",
                table: "graduate_register_question");

            migrationBuilder.DropColumn(
                name: "question_uid",
                schema: "EDU",
                table: "graduate_register_question");

            // migrationBuilder.AlterColumn<decimal>(
            //     name: "total_amout",
            //     schema: "EDU",
            //     table: "student_request",
            //     type: "DECIMAL(18, 2)",
            //     nullable: true,
            //     oldClrType: typeof(decimal),
            //     oldType: "DECIMAL(18,2)",
            //     oldNullable: true);
            //
            // migrationBuilder.AlterColumn<decimal>(
            //     name: "amout",
            //     schema: "EDU",
            //     table: "graduate_request_payment_detail",
            //     type: "DECIMAL(18, 2)",
            //     nullable: true,
            //     oldClrType: typeof(decimal),
            //     oldType: "DECIMAL(18,2)",
            //     oldNullable: true);
            //
            // migrationBuilder.AlterColumn<decimal>(
            //     name: "total_amout",
            //     schema: "EDU",
            //     table: "graduate_request_payment",
            //     type: "DECIMAL(18, 2)",
            //     nullable: true,
            //     oldClrType: typeof(decimal),
            //     oldType: "DECIMAL(18,2)",
            //     oldNullable: true);

            migrationBuilder.AddColumn<string>(
                name: "answer",
                schema: "EDU",
                table: "graduate_register_question",
                type: "NVARCHAR2(2000)",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "answer_type",
                schema: "EDU",
                table: "graduate_register_question",
                type: "NUMBER(10)",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "question_id",
                schema: "EDU",
                table: "graduate_register_question",
                type: "NUMBER(10)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "question_title",
                schema: "EDU",
                table: "graduate_register_question",
                type: "NVARCHAR2(2000)",
                nullable: true);

            // migrationBuilder.AlterColumn<decimal>(
            //     name: "amout",
            //     schema: "EDU",
            //     table: "graduate_register_payment",
            //     type: "DECIMAL(18, 2)",
            //     nullable: true,
            //     oldClrType: typeof(decimal),
            //     oldType: "DECIMAL(18,2)",
            //     oldNullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "answer",
                schema: "EDU",
                table: "graduate_register_question");

            migrationBuilder.DropColumn(
                name: "answer_type",
                schema: "EDU",
                table: "graduate_register_question");

            migrationBuilder.DropColumn(
                name: "question_id",
                schema: "EDU",
                table: "graduate_register_question");

            migrationBuilder.DropColumn(
                name: "question_title",
                schema: "EDU",
                table: "graduate_register_question");

            migrationBuilder.AlterColumn<decimal>(
                name: "total_amout",
                schema: "EDU",
                table: "student_request",
                type: "DECIMAL(18,2)",
                nullable: true,
                oldClrType: typeof(decimal),
                oldType: "DECIMAL(18, 2)",
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "amout",
                schema: "EDU",
                table: "graduate_request_payment_detail",
                type: "DECIMAL(18,2)",
                nullable: true,
                oldClrType: typeof(decimal),
                oldType: "DECIMAL(18, 2)",
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "total_amout",
                schema: "EDU",
                table: "graduate_request_payment",
                type: "DECIMAL(18,2)",
                nullable: true,
                oldClrType: typeof(decimal),
                oldType: "DECIMAL(18, 2)",
                oldNullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "answer_uid",
                schema: "EDU",
                table: "graduate_register_question",
                type: "RAW(16)",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "question_uid",
                schema: "EDU",
                table: "graduate_register_question",
                type: "RAW(16)",
                nullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "amout",
                schema: "EDU",
                table: "graduate_register_payment",
                type: "DECIMAL(18,2)",
                nullable: true,
                oldClrType: typeof(decimal),
                oldType: "DECIMAL(18, 2)",
                oldNullable: true);
        }
    }
}
