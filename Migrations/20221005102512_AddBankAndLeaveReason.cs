﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace edu_api.Migrations
{
    public partial class AddBankAndLeaveReason : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "from_add_no",
                schema: "EDU",
                table: "student_request_name_and_address",
                type: "NVARCHAR2(50)",
                maxLength: 50,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "from_adg_no",
                schema: "EDU",
                table: "student_request_name_and_address",
                type: "NVARCHAR2(50)",
                maxLength: 50,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "from_adm_no",
                schema: "EDU",
                table: "student_request_name_and_address",
                type: "NVARCHAR2(50)",
                maxLength: 50,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "from_no",
                schema: "EDU",
                table: "student_request_name_and_address",
                type: "NVARCHAR2(50)",
                maxLength: 50,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "to_add_no",
                schema: "EDU",
                table: "student_request_name_and_address",
                type: "NVARCHAR2(50)",
                maxLength: 50,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "to_adg_no",
                schema: "EDU",
                table: "student_request_name_and_address",
                type: "NVARCHAR2(50)",
                maxLength: 50,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "to_adm_no",
                schema: "EDU",
                table: "student_request_name_and_address",
                type: "NVARCHAR2(50)",
                maxLength: 50,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "to_no",
                schema: "EDU",
                table: "student_request_name_and_address",
                type: "NVARCHAR2(50)",
                maxLength: 50,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "student_request_code",
                schema: "EDU",
                table: "student_request",
                type: "NVARCHAR2(50)",
                maxLength: 50,
                nullable: true);

            migrationBuilder.CreateTable(
                name: "bank",
                schema: "EDU",
                columns: table => new
                {
                    bank_id = table.Column<Guid>(type: "RAW(16)", nullable: false),
                    status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
                    created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
                    updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
                    owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
                    bank_code = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
                    bank_name_th = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    bank_name_en = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_bank", x => x.bank_id);
                });

            migrationBuilder.CreateTable(
                name: "leave_reason",
                schema: "EDU",
                columns: table => new
                {
                    leave_reason_id = table.Column<Guid>(type: "RAW(16)", nullable: false),
                    status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
                    created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
                    updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
                    owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
                    leave_reason_code = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
                    leave_reason_name_th = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    leave_reason_name_en = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_leave_reason", x => x.leave_reason_id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "bank",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "leave_reason",
                schema: "EDU");

            migrationBuilder.DropColumn(
                name: "from_add_no",
                schema: "EDU",
                table: "student_request_name_and_address");

            migrationBuilder.DropColumn(
                name: "from_adg_no",
                schema: "EDU",
                table: "student_request_name_and_address");

            migrationBuilder.DropColumn(
                name: "from_adm_no",
                schema: "EDU",
                table: "student_request_name_and_address");

            migrationBuilder.DropColumn(
                name: "from_no",
                schema: "EDU",
                table: "student_request_name_and_address");

            migrationBuilder.DropColumn(
                name: "to_add_no",
                schema: "EDU",
                table: "student_request_name_and_address");

            migrationBuilder.DropColumn(
                name: "to_adg_no",
                schema: "EDU",
                table: "student_request_name_and_address");

            migrationBuilder.DropColumn(
                name: "to_adm_no",
                schema: "EDU",
                table: "student_request_name_and_address");

            migrationBuilder.DropColumn(
                name: "to_no",
                schema: "EDU",
                table: "student_request_name_and_address");

            migrationBuilder.DropColumn(
                name: "student_request_code",
                schema: "EDU",
                table: "student_request");
        }
    }
}
