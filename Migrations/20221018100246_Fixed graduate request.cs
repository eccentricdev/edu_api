﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace edu_api.Migrations
{
    public partial class Fixedgraduaterequest : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "v_graduate_request",
                schema: "EDU");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "v_graduate_request",
                schema: "EDU",
                columns: table => new
                {
                    graduate_request_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
                    created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
                    is_approve = table.Column<bool>(type: "NUMBER(1)", nullable: true),
                    is_check = table.Column<bool>(type: "NUMBER(1)", nullable: true),
                    owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
                    request_date = table.Column<DateTime>(type: "date", nullable: true),
                    status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
                    student_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
                    updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_v_graduate_request", x => x.graduate_request_uid);
                });
        }
    }
}
