﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace edu_api.Migrations
{
    public partial class UpdateGraduateRequest2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_graduate_request_profile_documentt_graduate_request_profile_graduate_request_profile_uid",
                schema: "EDU",
                table: "graduate_request_profile_documentt");

            migrationBuilder.DropPrimaryKey(
                name: "PK_graduate_request_profile_documentt",
                schema: "EDU",
                table: "graduate_request_profile_documentt");

            migrationBuilder.RenameTable(
                name: "graduate_request_profile_documentt",
                schema: "EDU",
                newName: "graduate_request_profile_document",
                newSchema: "EDU");

            migrationBuilder.RenameIndex(
                name: "IX_graduate_request_profile_documentt_graduate_request_profile_uid",
                schema: "EDU",
                table: "graduate_request_profile_document",
                newName: "IX_graduate_request_profile_document_graduate_request_profile_uid");

            migrationBuilder.AlterColumn<decimal>(
                name: "total_amout",
                schema: "EDU",
                table: "student_request",
                type: "DECIMAL(18, 2)",
                nullable: true,
                oldClrType: typeof(decimal),
                oldType: "DECIMAL(18,2)",
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "amout",
                schema: "EDU",
                table: "graduate_request_payment_detail",
                type: "DECIMAL(18, 2)",
                nullable: true,
                oldClrType: typeof(decimal),
                oldType: "DECIMAL(18,2)",
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "total_amout",
                schema: "EDU",
                table: "graduate_request_payment",
                type: "DECIMAL(18, 2)",
                nullable: true,
                oldClrType: typeof(decimal),
                oldType: "DECIMAL(18,2)",
                oldNullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_graduate_request_profile_document",
                schema: "EDU",
                table: "graduate_request_profile_document",
                column: "graduate_request_profile_document_uid");

            migrationBuilder.AddForeignKey(
                name: "FK_graduate_request_profile_document_graduate_request_profile_graduate_request_profile_uid",
                schema: "EDU",
                table: "graduate_request_profile_document",
                column: "graduate_request_profile_uid",
                principalSchema: "EDU",
                principalTable: "graduate_request_profile",
                principalColumn: "graduate_request_profile_uid",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_graduate_request_profile_document_graduate_request_profile_graduate_request_profile_uid",
                schema: "EDU",
                table: "graduate_request_profile_document");

            migrationBuilder.DropPrimaryKey(
                name: "PK_graduate_request_profile_document",
                schema: "EDU",
                table: "graduate_request_profile_document");

            migrationBuilder.RenameTable(
                name: "graduate_request_profile_document",
                schema: "EDU",
                newName: "graduate_request_profile_documentt",
                newSchema: "EDU");

            migrationBuilder.RenameIndex(
                name: "IX_graduate_request_profile_document_graduate_request_profile_uid",
                schema: "EDU",
                table: "graduate_request_profile_documentt",
                newName: "IX_graduate_request_profile_documentt_graduate_request_profile_uid");

            migrationBuilder.AlterColumn<decimal>(
                name: "total_amout",
                schema: "EDU",
                table: "student_request",
                type: "DECIMAL(18,2)",
                nullable: true,
                oldClrType: typeof(decimal),
                oldType: "DECIMAL(18, 2)",
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "amout",
                schema: "EDU",
                table: "graduate_request_payment_detail",
                type: "DECIMAL(18,2)",
                nullable: true,
                oldClrType: typeof(decimal),
                oldType: "DECIMAL(18, 2)",
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "total_amout",
                schema: "EDU",
                table: "graduate_request_payment",
                type: "DECIMAL(18,2)",
                nullable: true,
                oldClrType: typeof(decimal),
                oldType: "DECIMAL(18, 2)",
                oldNullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_graduate_request_profile_documentt",
                schema: "EDU",
                table: "graduate_request_profile_documentt",
                column: "graduate_request_profile_document_uid");

            migrationBuilder.AddForeignKey(
                name: "FK_graduate_request_profile_documentt_graduate_request_profile_graduate_request_profile_uid",
                schema: "EDU",
                table: "graduate_request_profile_documentt",
                column: "graduate_request_profile_uid",
                principalSchema: "EDU",
                principalTable: "graduate_request_profile",
                principalColumn: "graduate_request_profile_uid",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
