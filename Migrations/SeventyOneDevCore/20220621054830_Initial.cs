﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace edu_api.Migrations.SeventyOneDevCore
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "EDU2");

            migrationBuilder.CreateTable(
                name: "app_notification",
                schema: "EDU2",
                columns: table => new
                {
                    notification_id = table.Column<long>(type: "NUMBER(19)", nullable: false)
                        .Annotation("Oracle:Identity", "1, 1"),
                    app_object_id = table.Column<int>(type: "NUMBER(10)", nullable: true),
                    department_id = table.Column<int>(type: "NUMBER(10)", nullable: true),
                    app_role_id = table.Column<int>(type: "NUMBER(10)", nullable: true),
                    user_id = table.Column<int>(type: "NUMBER(10)", nullable: true),
                    title = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    message = table.Column<string>(type: "NVARCHAR2(500)", maxLength: 500, nullable: true),
                    url = table.Column<string>(type: "NVARCHAR2(500)", maxLength: 500, nullable: true),
                    read_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
                    create_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
                    update_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_app_notification", x => x.notification_id);
                });

            migrationBuilder.CreateTable(
                name: "app_notification_template",
                schema: "EDU2",
                columns: table => new
                {
                    notification_template_id = table.Column<int>(type: "NUMBER(10)", nullable: false)
                        .Annotation("Oracle:Identity", "1, 1"),
                    app_object_id = table.Column<int>(type: "NUMBER(10)", nullable: true),
                    action_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
                    action = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
                    title_template = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    message_template = table.Column<string>(type: "NVARCHAR2(500)", maxLength: 500, nullable: true),
                    url_template = table.Column<string>(type: "NVARCHAR2(500)", maxLength: 500, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_app_notification_template", x => x.notification_template_id);
                });

            migrationBuilder.CreateTable(
                name: "app_object",
                schema: "EDU2",
                columns: table => new
                {
                    app_object_id = table.Column<int>(type: "NUMBER(10)", nullable: false)
                        .Annotation("Oracle:Identity", "1, 1"),
                    app_object_code = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
                    app_object_name_en = table.Column<string>(type: "NVARCHAR2(100)", maxLength: 100, nullable: true),
                    app_object_name_th = table.Column<string>(type: "NVARCHAR2(100)", maxLength: 100, nullable: true),
                    parent_app_object_id = table.Column<int>(type: "NUMBER(10)", nullable: true),
                    internal_object_name = table.Column<string>(type: "NVARCHAR2(100)", maxLength: 100, nullable: true),
                    status_id = table.Column<int>(type: "NUMBER(10)", nullable: true),
                    create_by = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
                    create_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
                    last_update_by = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
                    last_update_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_app_object", x => x.app_object_id);
                    table.ForeignKey(
                        name: "FK_app_object_app_object_parent_app_object_id",
                        column: x => x.parent_app_object_id,
                        principalSchema: "EDU2",
                        principalTable: "app_object",
                        principalColumn: "app_object_id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "app_permission",
                schema: "EDU2",
                columns: table => new
                {
                    app_permission_id = table.Column<int>(type: "NUMBER(10)", nullable: false)
                        .Annotation("Oracle:Identity", "1, 1"),
                    app_permission_code = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
                    app_permission_name_en = table.Column<string>(type: "NVARCHAR2(100)", maxLength: 100, nullable: true),
                    app_permission_name_th = table.Column<string>(type: "NVARCHAR2(100)", maxLength: 100, nullable: true),
                    status_id = table.Column<int>(type: "NUMBER(10)", nullable: true),
                    create_by = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
                    create_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
                    last_update_by = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
                    last_update_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_app_permission", x => x.app_permission_id);
                });

            migrationBuilder.CreateTable(
                name: "app_role_type",
                schema: "EDU2",
                columns: table => new
                {
                    app_role_type_id = table.Column<short>(type: "NUMBER(5)", nullable: false)
                        .Annotation("Oracle:Identity", "1, 1"),
                    app_role_type_code = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
                    app_role_type_name_en = table.Column<string>(type: "NVARCHAR2(100)", maxLength: 100, nullable: true),
                    app_role_type_name_th = table.Column<string>(type: "NVARCHAR2(100)", maxLength: 100, nullable: true),
                    status_id = table.Column<int>(type: "NUMBER(10)", nullable: true),
                    create_by = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
                    create_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
                    last_update_by = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
                    last_update_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_app_role_type", x => x.app_role_type_id);
                });

            migrationBuilder.CreateTable(
                name: "app_user_type",
                schema: "EDU2",
                columns: table => new
                {
                    app_user_type_id = table.Column<short>(type: "NUMBER(5)", nullable: false)
                        .Annotation("Oracle:Identity", "1, 1"),
                    app_user_type_code = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
                    app_user_type_name_en = table.Column<string>(type: "NVARCHAR2(100)", maxLength: 100, nullable: true),
                    app_user_type_name_th = table.Column<string>(type: "NVARCHAR2(100)", maxLength: 100, nullable: true),
                    jwt_claim_name = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    is_jwt_claim_array = table.Column<bool>(type: "NUMBER(1)", nullable: true),
                    status_id = table.Column<int>(type: "NUMBER(10)", nullable: true),
                    create_by = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
                    create_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
                    last_update_by = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
                    last_update_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_app_user_type", x => x.app_user_type_id);
                });

            migrationBuilder.CreateTable(
                name: "document_no_template",
                schema: "EDU2",
                columns: table => new
                {
                    document_no_template_id = table.Column<int>(type: "NUMBER(10)", nullable: false)
                        .Annotation("Oracle:Identity", "1, 1"),
                    document_code = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
                    current_no = table.Column<int>(type: "NUMBER(10)", nullable: true),
                    has_prefix = table.Column<bool>(type: "NUMBER(1)", nullable: true),
                    prefix_depends = table.Column<bool>(type: "NUMBER(1)", nullable: true),
                    prefix_template = table.Column<string>(type: "NVARCHAR2(500)", maxLength: 500, nullable: true),
                    has_suffix = table.Column<bool>(type: "NUMBER(1)", nullable: true),
                    suffix_depends = table.Column<bool>(type: "NUMBER(1)", nullable: true),
                    suffix_template = table.Column<string>(type: "NVARCHAR2(500)", maxLength: 500, nullable: true),
                    running_digit = table.Column<short>(type: "NUMBER(5)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_document_no_template", x => x.document_no_template_id);
                });

            migrationBuilder.CreateTable(
                name: "document_running_no",
                schema: "EDU2",
                columns: table => new
                {
                    document_running_no_id = table.Column<int>(type: "NUMBER(10)", nullable: false)
                        .Annotation("Oracle:Identity", "1, 1"),
                    document_code = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
                    document_key = table.Column<string>(type: "NVARCHAR2(500)", maxLength: 500, nullable: true),
                    current_no = table.Column<int>(type: "NUMBER(10)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_document_running_no", x => x.document_running_no_id);
                });

            migrationBuilder.CreateTable(
                name: "app_role",
                schema: "EDU2",
                columns: table => new
                {
                    app_role_id = table.Column<int>(type: "NUMBER(10)", nullable: false)
                        .Annotation("Oracle:Identity", "1, 1"),
                    app_role_code = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
                    app_role_name_en = table.Column<string>(type: "NVARCHAR2(100)", maxLength: 100, nullable: true),
                    app_role_name_th = table.Column<string>(type: "NVARCHAR2(100)", maxLength: 100, nullable: true),
                    app_role_type_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
                    status_id = table.Column<int>(type: "NUMBER(10)", nullable: true),
                    create_by = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
                    create_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
                    last_update_by = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
                    last_update_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_app_role", x => x.app_role_id);
                    table.ForeignKey(
                        name: "FK_app_role_app_role_type_app_role_type_id",
                        column: x => x.app_role_type_id,
                        principalSchema: "EDU2",
                        principalTable: "app_role_type",
                        principalColumn: "app_role_type_id",
                        onDelete: ReferentialAction.SetNull);
                });

            migrationBuilder.CreateTable(
                name: "app_user",
                schema: "EDU2",
                columns: table => new
                {
                    app_user_id = table.Column<int>(type: "NUMBER(10)", nullable: false)
                        .Annotation("Oracle:Identity", "1, 1"),
                    login_name = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
                    display_name = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    app_user_type_id = table.Column<short>(type: "NUMBER(5)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_app_user", x => x.app_user_id);
                    table.ForeignKey(
                        name: "FK_app_user_app_user_type_app_user_type_id",
                        column: x => x.app_user_type_id,
                        principalSchema: "EDU2",
                        principalTable: "app_user_type",
                        principalColumn: "app_user_type_id",
                        onDelete: ReferentialAction.SetNull);
                });

            migrationBuilder.CreateTable(
                name: "app_data",
                schema: "EDU2",
                columns: table => new
                {
                    app_data_id = table.Column<int>(type: "NUMBER(10)", nullable: false)
                        .Annotation("Oracle:Identity", "1, 1"),
                    app_role_id = table.Column<int>(type: "NUMBER(10)", nullable: true),
                    app_data_type_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
                    ref_data_id = table.Column<long>(type: "NUMBER(19)", nullable: true),
                    status_id = table.Column<int>(type: "NUMBER(10)", nullable: true),
                    create_by = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
                    create_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
                    last_update_by = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
                    last_update_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_app_data", x => x.app_data_id);
                    table.ForeignKey(
                        name: "FK_app_data_app_role_app_role_id",
                        column: x => x.app_role_id,
                        principalSchema: "EDU2",
                        principalTable: "app_role",
                        principalColumn: "app_role_id",
                        onDelete: ReferentialAction.SetNull);
                });

            migrationBuilder.CreateTable(
                name: "app_security",
                schema: "EDU2",
                columns: table => new
                {
                    app_security_id = table.Column<int>(type: "NUMBER(10)", nullable: false)
                        .Annotation("Oracle:Identity", "1, 1"),
                    app_role_id = table.Column<int>(type: "NUMBER(10)", nullable: true),
                    app_object_id = table.Column<int>(type: "NUMBER(10)", nullable: true),
                    app_permission_id = table.Column<int>(type: "NUMBER(10)", nullable: true),
                    restrict_user = table.Column<bool>(type: "NUMBER(1)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_app_security", x => x.app_security_id);
                    table.ForeignKey(
                        name: "FK_app_security_app_object_app_object_id",
                        column: x => x.app_object_id,
                        principalSchema: "EDU2",
                        principalTable: "app_object",
                        principalColumn: "app_object_id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_app_security_app_permission_app_permission_id",
                        column: x => x.app_permission_id,
                        principalSchema: "EDU2",
                        principalTable: "app_permission",
                        principalColumn: "app_permission_id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_app_security_app_role_app_role_id",
                        column: x => x.app_role_id,
                        principalSchema: "EDU2",
                        principalTable: "app_role",
                        principalColumn: "app_role_id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "app_user_role",
                schema: "EDU2",
                columns: table => new
                {
                    app_user_role_id = table.Column<int>(type: "NUMBER(10)", nullable: false)
                        .Annotation("Oracle:Identity", "1, 1"),
                    app_user_id = table.Column<int>(type: "NUMBER(10)", nullable: true),
                    app_role_id = table.Column<int>(type: "NUMBER(10)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_app_user_role", x => x.app_user_role_id);
                    table.ForeignKey(
                        name: "FK_app_user_role_app_role_app_role_id",
                        column: x => x.app_role_id,
                        principalSchema: "EDU2",
                        principalTable: "app_role",
                        principalColumn: "app_role_id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_app_user_role_app_user_app_user_id",
                        column: x => x.app_user_id,
                        principalSchema: "EDU2",
                        principalTable: "app_user",
                        principalColumn: "app_user_id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_app_data_app_role_id",
                schema: "EDU2",
                table: "app_data",
                column: "app_role_id");

            migrationBuilder.CreateIndex(
                name: "IX_app_object_parent_app_object_id",
                schema: "EDU2",
                table: "app_object",
                column: "parent_app_object_id");

            migrationBuilder.CreateIndex(
                name: "IX_app_role_app_role_type_id",
                schema: "EDU2",
                table: "app_role",
                column: "app_role_type_id");

            migrationBuilder.CreateIndex(
                name: "IX_app_security_app_object_id",
                schema: "EDU2",
                table: "app_security",
                column: "app_object_id");

            migrationBuilder.CreateIndex(
                name: "IX_app_security_app_permission_id",
                schema: "EDU2",
                table: "app_security",
                column: "app_permission_id");

            migrationBuilder.CreateIndex(
                name: "IX_app_security_app_role_id",
                schema: "EDU2",
                table: "app_security",
                column: "app_role_id");

            migrationBuilder.CreateIndex(
                name: "IX_app_user_app_user_type_id",
                schema: "EDU2",
                table: "app_user",
                column: "app_user_type_id");

            migrationBuilder.CreateIndex(
                name: "IX_app_user_role_app_role_id",
                schema: "EDU2",
                table: "app_user_role",
                column: "app_role_id");

            migrationBuilder.CreateIndex(
                name: "IX_app_user_role_app_user_id",
                schema: "EDU2",
                table: "app_user_role",
                column: "app_user_id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "app_data",
                schema: "EDU2");

            migrationBuilder.DropTable(
                name: "app_notification",
                schema: "EDU2");

            migrationBuilder.DropTable(
                name: "app_notification_template",
                schema: "EDU2");

            migrationBuilder.DropTable(
                name: "app_security",
                schema: "EDU2");

            migrationBuilder.DropTable(
                name: "app_user_role",
                schema: "EDU2");

            migrationBuilder.DropTable(
                name: "document_no_template",
                schema: "EDU2");

            migrationBuilder.DropTable(
                name: "document_running_no",
                schema: "EDU2");

            migrationBuilder.DropTable(
                name: "app_object",
                schema: "EDU2");

            migrationBuilder.DropTable(
                name: "app_permission",
                schema: "EDU2");

            migrationBuilder.DropTable(
                name: "app_role",
                schema: "EDU2");

            migrationBuilder.DropTable(
                name: "app_user",
                schema: "EDU2");

            migrationBuilder.DropTable(
                name: "app_role_type",
                schema: "EDU2");

            migrationBuilder.DropTable(
                name: "app_user_type",
                schema: "EDU2");
        }
    }
}
