﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace edu_api.Migrations
{
    public partial class Addrowordertofinalregister : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "row_order",
                schema: "EDU",
                table: "final_register_subject",
                type: "NUMBER(10)",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "row_order",
                schema: "EDU",
                table: "final_register",
                type: "NUMBER(10)",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "row_order",
                schema: "EDU",
                table: "final_register_subject");

            migrationBuilder.DropColumn(
                name: "row_order",
                schema: "EDU",
                table: "final_register");
        }
    }
}
