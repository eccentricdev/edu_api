﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace edu_api.Migrations
{
    public partial class Add_academic_year_document : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            // migrationBuilder.AlterColumn<decimal>(
            //     name: "total_amout",
            //     schema: "EDU",
            //     table: "student_request",
            //     type: "DECIMAL(18, 2)",
            //     nullable: true,
            //     oldClrType: typeof(decimal),
            //     oldType: "DECIMAL(18,2)",
            //     oldNullable: true);
            //
            // migrationBuilder.AlterColumn<decimal>(
            //     name: "amout",
            //     schema: "EDU",
            //     table: "graduate_request_payment_detail",
            //     type: "DECIMAL(18, 2)",
            //     nullable: true,
            //     oldClrType: typeof(decimal),
            //     oldType: "DECIMAL(18,2)",
            //     oldNullable: true);
            //
            // migrationBuilder.AlterColumn<decimal>(
            //     name: "total_amout",
            //     schema: "EDU",
            //     table: "graduate_request_payment",
            //     type: "DECIMAL(18, 2)",
            //     nullable: true,
            //     oldClrType: typeof(decimal),
            //     oldType: "DECIMAL(18,2)",
            //     oldNullable: true);
            //
            // migrationBuilder.AlterColumn<decimal>(
            //     name: "amout",
            //     schema: "EDU",
            //     table: "graduate_register_payment",
            //     type: "DECIMAL(18, 2)",
            //     nullable: true,
            //     oldClrType: typeof(decimal),
            //     oldType: "DECIMAL(18,2)",
            //     oldNullable: true);

            migrationBuilder.CreateTable(
                name: "academic_year_document",
                schema: "EDU",
                columns: table => new
                {
                    academic_year_document_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
                    status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
                    created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
                    updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
                    owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
                    from_academic_year_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
                    to_academic_year_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
                    is_not_specified_to_academic_year = table.Column<bool>(type: "NUMBER(1)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_academic_year_document", x => x.academic_year_document_uid);
                });

            migrationBuilder.CreateTable(
                name: "academic_year_document_type",
                schema: "EDU",
                columns: table => new
                {
                    academic_year_document_type_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
                    status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
                    created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
                    updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
                    owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
                    academic_year_document_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
                    document_type_id = table.Column<int>(type: "NUMBER(10)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_academic_year_document_type", x => x.academic_year_document_type_uid);
                    table.ForeignKey(
                        name: "FK_academic_year_document_type_academic_year_document_academic_year_document_uid",
                        column: x => x.academic_year_document_uid,
                        principalSchema: "EDU",
                        principalTable: "academic_year_document",
                        principalColumn: "academic_year_document_uid",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_academic_year_document_type_academic_year_document_uid",
                schema: "EDU",
                table: "academic_year_document_type",
                column: "academic_year_document_uid");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "academic_year_document_type",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "academic_year_document",
                schema: "EDU");

            migrationBuilder.AlterColumn<decimal>(
                name: "total_amout",
                schema: "EDU",
                table: "student_request",
                type: "DECIMAL(18,2)",
                nullable: true,
                oldClrType: typeof(decimal),
                oldType: "DECIMAL(18, 2)",
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "amout",
                schema: "EDU",
                table: "graduate_request_payment_detail",
                type: "DECIMAL(18,2)",
                nullable: true,
                oldClrType: typeof(decimal),
                oldType: "DECIMAL(18, 2)",
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "total_amout",
                schema: "EDU",
                table: "graduate_request_payment",
                type: "DECIMAL(18,2)",
                nullable: true,
                oldClrType: typeof(decimal),
                oldType: "DECIMAL(18, 2)",
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "amout",
                schema: "EDU",
                table: "graduate_register_payment",
                type: "DECIMAL(18,2)",
                nullable: true,
                oldClrType: typeof(decimal),
                oldType: "DECIMAL(18, 2)",
                oldNullable: true);
        }
    }
}
