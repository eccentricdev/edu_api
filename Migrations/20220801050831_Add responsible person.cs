﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace edu_api.Migrations
{
    public partial class Addresponsibleperson : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "is_responsible_instructor",
                schema: "EDU",
                table: "year_subject_section",
                type: "NUMBER(1)",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "responsible_person_uid",
                schema: "EDU",
                table: "year_subject_section",
                type: "RAW(16)",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "is_responsible_instructor",
                schema: "EDU",
                table: "year_subject_section");

            migrationBuilder.DropColumn(
                name: "responsible_person_uid",
                schema: "EDU",
                table: "year_subject_section");
        }
    }
}
