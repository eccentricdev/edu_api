﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace edu_api.Migrations
{
    public partial class Studentgrade : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "student_grade",
                schema: "EDU",
                columns: table => new
                {
                    student_grade_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
                    status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
                    created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
                    updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
                    owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
                    student_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
                    academic_year_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
                    academic_semester_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
                    register_credits = table.Column<decimal>(type: "decimal(5,1)", nullable: true),
                    earn_credits = table.Column<decimal>(type: "decimal(5,1)", nullable: true),
                    total_register_credits = table.Column<decimal>(type: "decimal(5,1)", nullable: true),
                    total_earn_credits = table.Column<decimal>(type: "decimal(5,1)", nullable: true),
                    gpa = table.Column<decimal>(type: "decimal(5,2)", nullable: true),
                    gpax = table.Column<decimal>(type: "decimal(5,2)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_student_grade", x => x.student_grade_uid);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "student_grade",
                schema: "EDU");
        }
    }
}
