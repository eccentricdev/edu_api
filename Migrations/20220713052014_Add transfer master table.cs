﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace edu_api.Migrations
{
    public partial class Addtransfermastertable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "document_type_uid",
                schema: "EDU",
                table: "transfer_leisure_document",
                newName: "request_document_type_uid");

            migrationBuilder.RenameColumn(
                name: "document_type_uid",
                schema: "EDU",
                table: "transfer_institute_document",
                newName: "request_document_type_uid");

            migrationBuilder.RenameColumn(
                name: "document_type_uid",
                schema: "EDU",
                table: "transfer_degree_document",
                newName: "request_document_type_uid");

            migrationBuilder.AddColumn<string>(
                name: "student_code",
                schema: "EDU",
                table: "transfer_new_from",
                type: "NVARCHAR2(50)",
                maxLength: 50,
                nullable: true);

            migrationBuilder.CreateTable(
                name: "request_document_type",
                schema: "EDU",
                columns: table => new
                {
                    request_document_type_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
                    status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
                    created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
                    updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
                    owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
                    request_document_type_code = table.Column<Guid>(type: "RAW(16)", nullable: true),
                    request_document_type_name_th = table.Column<Guid>(type: "RAW(16)", nullable: true),
                    request_document_type_name_en = table.Column<Guid>(type: "RAW(16)", nullable: true),
                    allow_multiple = table.Column<bool>(type: "NUMBER(1)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_request_document_type", x => x.request_document_type_uid);
                });

            migrationBuilder.CreateTable(
                name: "student_request_document_type",
                schema: "EDU",
                columns: table => new
                {
                    student_request_document_type_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
                    status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
                    created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
                    updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
                    owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
                    request_type_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
                    request_document_type_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
                    is_required = table.Column<bool>(type: "NUMBER(1)", nullable: true),
                    row_order = table.Column<int>(type: "NUMBER(10)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_student_request_document_type", x => x.student_request_document_type_uid);
                    table.ForeignKey(
                        name: "FK_student_request_document_type_request_document_type_request_document_type_uid",
                        column: x => x.request_document_type_uid,
                        principalSchema: "EDU",
                        principalTable: "request_document_type",
                        principalColumn: "request_document_type_uid",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_student_request_document_type_request_type_request_type_uid",
                        column: x => x.request_type_uid,
                        principalSchema: "EDU",
                        principalTable: "request_type",
                        principalColumn: "request_type_uid",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_transfer_leisure_document_request_document_type_uid",
                schema: "EDU",
                table: "transfer_leisure_document",
                column: "request_document_type_uid");

            migrationBuilder.CreateIndex(
                name: "IX_transfer_institute_document_request_document_type_uid",
                schema: "EDU",
                table: "transfer_institute_document",
                column: "request_document_type_uid");

            migrationBuilder.CreateIndex(
                name: "IX_transfer_degree_document_request_document_type_uid",
                schema: "EDU",
                table: "transfer_degree_document",
                column: "request_document_type_uid");

            migrationBuilder.CreateIndex(
                name: "IX_student_request_document_type_request_document_type_uid",
                schema: "EDU",
                table: "student_request_document_type",
                column: "request_document_type_uid");

            migrationBuilder.CreateIndex(
                name: "IX_student_request_document_type_request_type_uid",
                schema: "EDU",
                table: "student_request_document_type",
                column: "request_type_uid");

            migrationBuilder.AddForeignKey(
                name: "FK_transfer_degree_document_request_document_type_request_document_type_uid",
                schema: "EDU",
                table: "transfer_degree_document",
                column: "request_document_type_uid",
                principalSchema: "EDU",
                principalTable: "request_document_type",
                principalColumn: "request_document_type_uid",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_transfer_institute_document_request_document_type_request_document_type_uid",
                schema: "EDU",
                table: "transfer_institute_document",
                column: "request_document_type_uid",
                principalSchema: "EDU",
                principalTable: "request_document_type",
                principalColumn: "request_document_type_uid",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_transfer_leisure_document_request_document_type_request_document_type_uid",
                schema: "EDU",
                table: "transfer_leisure_document",
                column: "request_document_type_uid",
                principalSchema: "EDU",
                principalTable: "request_document_type",
                principalColumn: "request_document_type_uid",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_transfer_degree_document_request_document_type_request_document_type_uid",
                schema: "EDU",
                table: "transfer_degree_document");

            migrationBuilder.DropForeignKey(
                name: "FK_transfer_institute_document_request_document_type_request_document_type_uid",
                schema: "EDU",
                table: "transfer_institute_document");

            migrationBuilder.DropForeignKey(
                name: "FK_transfer_leisure_document_request_document_type_request_document_type_uid",
                schema: "EDU",
                table: "transfer_leisure_document");

            migrationBuilder.DropTable(
                name: "student_request_document_type",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "request_document_type",
                schema: "EDU");

            migrationBuilder.DropIndex(
                name: "IX_transfer_leisure_document_request_document_type_uid",
                schema: "EDU",
                table: "transfer_leisure_document");

            migrationBuilder.DropIndex(
                name: "IX_transfer_institute_document_request_document_type_uid",
                schema: "EDU",
                table: "transfer_institute_document");

            migrationBuilder.DropIndex(
                name: "IX_transfer_degree_document_request_document_type_uid",
                schema: "EDU",
                table: "transfer_degree_document");

            migrationBuilder.DropColumn(
                name: "student_code",
                schema: "EDU",
                table: "transfer_new_from");

            migrationBuilder.RenameColumn(
                name: "request_document_type_uid",
                schema: "EDU",
                table: "transfer_leisure_document",
                newName: "document_type_uid");

            migrationBuilder.RenameColumn(
                name: "request_document_type_uid",
                schema: "EDU",
                table: "transfer_institute_document",
                newName: "document_type_uid");

            migrationBuilder.RenameColumn(
                name: "request_document_type_uid",
                schema: "EDU",
                table: "transfer_degree_document",
                newName: "document_type_uid");
        }
    }
}
