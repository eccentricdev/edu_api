﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace edu_api.Migrations
{
    public partial class Graduatestudent : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<decimal>(
                name: "total_amout",
                schema: "EDU",
                table: "student_request",
                type: "DECIMAL(18, 2)",
                nullable: true,
                oldClrType: typeof(decimal),
                oldType: "DECIMAL(18,2)",
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "amout",
                schema: "EDU",
                table: "graduate_request_payment_detail",
                type: "DECIMAL(18, 2)",
                nullable: true,
                oldClrType: typeof(decimal),
                oldType: "DECIMAL(18,2)",
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "total_amout",
                schema: "EDU",
                table: "graduate_request_payment",
                type: "DECIMAL(18, 2)",
                nullable: true,
                oldClrType: typeof(decimal),
                oldType: "DECIMAL(18,2)",
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "amout",
                schema: "EDU",
                table: "graduate_register_payment",
                type: "DECIMAL(18, 2)",
                nullable: true,
                oldClrType: typeof(decimal),
                oldType: "DECIMAL(18,2)",
                oldNullable: true);

            migrationBuilder.CreateTable(
                name: "graduate_student",
                schema: "EDU",
                columns: table => new
                {
                    graduate_student_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
                    status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
                    created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
                    updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
                    owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
                    student_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
                    graduate_request_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
                    attended_credit = table.Column<decimal>(type: "decimal(5,2)", nullable: true),
                    earned_credit = table.Column<decimal>(type: "decimal(5,2)", nullable: true),
                    total_gpa = table.Column<decimal>(type: "decimal(5,2)", nullable: true),
                    gpa = table.Column<decimal>(type: "decimal(3,2)", nullable: true),
                    entry_date = table.Column<DateTime>(type: "date", nullable: true),
                    graduate_date = table.Column<DateTime>(type: "date", nullable: true),
                    year_study = table.Column<int>(type: "NUMBER(10)", nullable: true),
                    semester_study = table.Column<int>(type: "NUMBER(10)", nullable: true),
                    maximum_study_year = table.Column<int>(type: "NUMBER(10)", nullable: true),
                    curriculum_study_year = table.Column<int>(type: "NUMBER(10)", nullable: true),
                    is_study_period_pass = table.Column<bool>(type: "NUMBER(1)", nullable: true),
                    study_period_remark = table.Column<string>(type: "NVARCHAR2(1000)", maxLength: 1000, nullable: true),
                    curriculum_credit = table.Column<decimal>(type: "decimal(5,2)", nullable: true),
                    curriculum_earned_credit = table.Column<decimal>(type: "decimal(5,2)", nullable: true),
                    curriculum_attended_credit = table.Column<decimal>(type: "decimal(5,2)", nullable: true),
                    is_curriculum_credit_pass = table.Column<bool>(type: "NUMBER(1)", nullable: true),
                    curriculum_credit_remark = table.Column<string>(type: "NVARCHAR2(1000)", maxLength: 1000, nullable: true),
                    curriculum_gpa = table.Column<decimal>(type: "decimal(3,2)", nullable: true),
                    total_passed_gp = table.Column<decimal>(type: "decimal(5,2)", nullable: true),
                    passed_gpa = table.Column<decimal>(type: "decimal(3,2)", nullable: true),
                    is_curriculum_gpa_pass = table.Column<bool>(type: "NUMBER(1)", nullable: true),
                    curriculum_gpa_remark = table.Column<string>(type: "NVARCHAR2(1000)", maxLength: 1000, nullable: true),
                    facultry_gpa = table.Column<decimal>(type: "decimal(3,2)", nullable: true),
                    is_facultry_gpa_pass = table.Column<bool>(type: "NUMBER(1)", nullable: true),
                    facultry_gpa_remark = table.Column<string>(type: "NVARCHAR2(1000)", maxLength: 1000, nullable: true),
                    is_subject_score_pass = table.Column<bool>(type: "NUMBER(1)", nullable: true),
                    subject_score_remark = table.Column<string>(type: "NVARCHAR2(1000)", maxLength: 1000, nullable: true),
                    is_gradudate_check_pass = table.Column<bool>(type: "NUMBER(1)", nullable: true),
                    is_honor = table.Column<bool>(type: "NUMBER(1)", nullable: true),
                    honor_level_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
                    is_honor_pass = table.Column<bool>(type: "NUMBER(1)", nullable: true),
                    honor_remark = table.Column<string>(type: "NVARCHAR2(1000)", maxLength: 1000, nullable: true),
                    has_honor_re_grade = table.Column<bool>(type: "NUMBER(1)", nullable: true),
                    is_honor_re_grade_pass = table.Column<bool>(type: "NUMBER(1)", nullable: true),
                    honor_re_grade_remark = table.Column<string>(type: "NVARCHAR2(1000)", maxLength: 1000, nullable: true),
                    has_honor_education_leave = table.Column<bool>(type: "NUMBER(1)", nullable: true),
                    is_honor_education_leave_pass = table.Column<bool>(type: "NUMBER(1)", nullable: true),
                    honor_education_leave_remark = table.Column<string>(type: "NVARCHAR2(1000)", maxLength: 1000, nullable: true),
                    is_honor_credit_check_pass = table.Column<bool>(type: "NUMBER(1)", nullable: true),
                    honor_credit_check_remark = table.Column<string>(type: "NVARCHAR2(1000)", maxLength: 1000, nullable: true),
                    is_honor_subject_score_pass = table.Column<bool>(type: "NUMBER(1)", nullable: true),
                    honor_subject_score_remark = table.Column<string>(type: "NVARCHAR2(1000)", maxLength: 1000, nullable: true),
                    is_honor_gradudate_pass = table.Column<bool>(type: "NUMBER(1)", nullable: true),
                    is_honor_check_pass = table.Column<bool>(type: "NUMBER(1)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_graduate_student", x => x.graduate_student_uid);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "graduate_student",
                schema: "EDU");

            migrationBuilder.AlterColumn<decimal>(
                name: "total_amout",
                schema: "EDU",
                table: "student_request",
                type: "DECIMAL(18,2)",
                nullable: true,
                oldClrType: typeof(decimal),
                oldType: "DECIMAL(18, 2)",
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "amout",
                schema: "EDU",
                table: "graduate_request_payment_detail",
                type: "DECIMAL(18,2)",
                nullable: true,
                oldClrType: typeof(decimal),
                oldType: "DECIMAL(18, 2)",
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "total_amout",
                schema: "EDU",
                table: "graduate_request_payment",
                type: "DECIMAL(18,2)",
                nullable: true,
                oldClrType: typeof(decimal),
                oldType: "DECIMAL(18, 2)",
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "amout",
                schema: "EDU",
                table: "graduate_register_payment",
                type: "DECIMAL(18,2)",
                nullable: true,
                oldClrType: typeof(decimal),
                oldType: "DECIMAL(18, 2)",
                oldNullable: true);
        }
    }
}
