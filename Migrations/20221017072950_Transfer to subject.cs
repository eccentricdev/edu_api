﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace edu_api.Migrations
{
    public partial class Transfertosubject : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "remark",
                schema: "EDU",
                table: "transfer_partial_subject",
                type: "NVARCHAR2(1000)",
                maxLength: 1000,
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "to_grade_uid",
                schema: "EDU",
                table: "transfer_partial_subject",
                type: "RAW(16)",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "to_year_subject_uid",
                schema: "EDU",
                table: "transfer_partial_subject",
                type: "RAW(16)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "remark",
                schema: "EDU",
                table: "transfer_new_subject",
                type: "NVARCHAR2(1000)",
                maxLength: 1000,
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "to_grade_uid",
                schema: "EDU",
                table: "transfer_new_subject",
                type: "RAW(16)",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "to_year_subject_uid",
                schema: "EDU",
                table: "transfer_new_subject",
                type: "RAW(16)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "remark",
                schema: "EDU",
                table: "transfer_leisure_subject",
                type: "NVARCHAR2(1000)",
                maxLength: 1000,
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "to_grade_uid",
                schema: "EDU",
                table: "transfer_leisure_subject",
                type: "RAW(16)",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "to_year_subject_uid",
                schema: "EDU",
                table: "transfer_leisure_subject",
                type: "RAW(16)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "remark",
                schema: "EDU",
                table: "transfer_institute_subject",
                type: "NVARCHAR2(1000)",
                maxLength: 1000,
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "to_grade_uid",
                schema: "EDU",
                table: "transfer_institute_subject",
                type: "RAW(16)",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "to_year_subject_uid",
                schema: "EDU",
                table: "transfer_institute_subject",
                type: "RAW(16)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "remark",
                schema: "EDU",
                table: "transfer_degree_subject",
                type: "NVARCHAR2(1000)",
                maxLength: 1000,
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "to_grade_uid",
                schema: "EDU",
                table: "transfer_degree_subject",
                type: "RAW(16)",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "to_year_subject_uid",
                schema: "EDU",
                table: "transfer_degree_subject",
                type: "RAW(16)",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "remark",
                schema: "EDU",
                table: "transfer_partial_subject");

            migrationBuilder.DropColumn(
                name: "to_grade_uid",
                schema: "EDU",
                table: "transfer_partial_subject");

            migrationBuilder.DropColumn(
                name: "to_year_subject_uid",
                schema: "EDU",
                table: "transfer_partial_subject");

            migrationBuilder.DropColumn(
                name: "remark",
                schema: "EDU",
                table: "transfer_new_subject");

            migrationBuilder.DropColumn(
                name: "to_grade_uid",
                schema: "EDU",
                table: "transfer_new_subject");

            migrationBuilder.DropColumn(
                name: "to_year_subject_uid",
                schema: "EDU",
                table: "transfer_new_subject");

            migrationBuilder.DropColumn(
                name: "remark",
                schema: "EDU",
                table: "transfer_leisure_subject");

            migrationBuilder.DropColumn(
                name: "to_grade_uid",
                schema: "EDU",
                table: "transfer_leisure_subject");

            migrationBuilder.DropColumn(
                name: "to_year_subject_uid",
                schema: "EDU",
                table: "transfer_leisure_subject");

            migrationBuilder.DropColumn(
                name: "remark",
                schema: "EDU",
                table: "transfer_institute_subject");

            migrationBuilder.DropColumn(
                name: "to_grade_uid",
                schema: "EDU",
                table: "transfer_institute_subject");

            migrationBuilder.DropColumn(
                name: "to_year_subject_uid",
                schema: "EDU",
                table: "transfer_institute_subject");

            migrationBuilder.DropColumn(
                name: "remark",
                schema: "EDU",
                table: "transfer_degree_subject");

            migrationBuilder.DropColumn(
                name: "to_grade_uid",
                schema: "EDU",
                table: "transfer_degree_subject");

            migrationBuilder.DropColumn(
                name: "to_year_subject_uid",
                schema: "EDU",
                table: "transfer_degree_subject");
        }
    }
}
