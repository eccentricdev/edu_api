﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace edu_api.Migrations
{
    public partial class Add_graduate_student_final : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            // migrationBuilder.AlterColumn<decimal>(
            //     name: "total_amout",
            //     schema: "EDU",
            //     table: "student_request",
            //     type: "DECIMAL(18, 2)",
            //     nullable: true,
            //     oldClrType: typeof(decimal),
            //     oldType: "DECIMAL(18,2)",
            //     oldNullable: true);

            migrationBuilder.AddColumn<int>(
                name: "honor_status_id",
                schema: "EDU",
                table: "graduate_student",
                type: "NUMBER(10)",
                nullable: true);

            // migrationBuilder.AlterColumn<decimal>(
            //     name: "amout",
            //     schema: "EDU",
            //     table: "graduate_request_payment_detail",
            //     type: "DECIMAL(18, 2)",
            //     nullable: true,
            //     oldClrType: typeof(decimal),
            //     oldType: "DECIMAL(18,2)",
            //     oldNullable: true);

            // migrationBuilder.AlterColumn<decimal>(
            //     name: "total_amout",
            //     schema: "EDU",
            //     table: "graduate_request_payment",
            //     type: "DECIMAL(18, 2)",
            //     nullable: true,
            //     oldClrType: typeof(decimal),
            //     oldType: "DECIMAL(18,2)",
            //     oldNullable: true);

            // migrationBuilder.AlterColumn<decimal>(
            //     name: "amout",
            //     schema: "EDU",
            //     table: "graduate_register_payment",
            //     type: "DECIMAL(18, 2)",
            //     nullable: true,
            //     oldClrType: typeof(decimal),
            //     oldType: "DECIMAL(18,2)",
            //     oldNullable: true);

            migrationBuilder.CreateTable(
                name: "graduate_student_final",
                schema: "EDU",
                columns: table => new
                {
                    graduate_student_final_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
                    status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
                    created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
                    updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
                    owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
                    student_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
                    row_order = table.Column<int>(type: "NUMBER(10)", nullable: true),
                    graduate_faculty_curriculum_detail_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
                    is_rehearsal = table.Column<bool>(type: "NUMBER(1)", nullable: true),
                    remark_no_rehearsal = table.Column<int>(type: "NUMBER(10)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_graduate_student_final", x => x.graduate_student_final_uid);
                    table.ForeignKey(
                        name: "FK_graduate_student_final_graduate_faculty_curriculum_detail_graduate_faculty_curriculum_detail_uid",
                        column: x => x.graduate_faculty_curriculum_detail_uid,
                        principalSchema: "EDU",
                        principalTable: "graduate_faculty_curriculum_detail",
                        principalColumn: "graduate_faculty_curriculum_detail_uid",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_graduate_student_final_graduate_faculty_curriculum_detail_uid",
                schema: "EDU",
                table: "graduate_student_final",
                column: "graduate_faculty_curriculum_detail_uid");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "graduate_student_final",
                schema: "EDU");

            migrationBuilder.DropColumn(
                name: "honor_status_id",
                schema: "EDU",
                table: "graduate_student");

            migrationBuilder.AlterColumn<decimal>(
                name: "total_amout",
                schema: "EDU",
                table: "student_request",
                type: "DECIMAL(18,2)",
                nullable: true,
                oldClrType: typeof(decimal),
                oldType: "DECIMAL(18, 2)",
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "amout",
                schema: "EDU",
                table: "graduate_request_payment_detail",
                type: "DECIMAL(18,2)",
                nullable: true,
                oldClrType: typeof(decimal),
                oldType: "DECIMAL(18, 2)",
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "total_amout",
                schema: "EDU",
                table: "graduate_request_payment",
                type: "DECIMAL(18,2)",
                nullable: true,
                oldClrType: typeof(decimal),
                oldType: "DECIMAL(18, 2)",
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "amout",
                schema: "EDU",
                table: "graduate_register_payment",
                type: "DECIMAL(18,2)",
                nullable: true,
                oldClrType: typeof(decimal),
                oldType: "DECIMAL(18, 2)",
                oldNullable: true);
        }
    }
}
