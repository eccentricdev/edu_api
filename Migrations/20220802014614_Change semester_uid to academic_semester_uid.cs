﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace edu_api.Migrations
{
    public partial class Changesemester_uidtoacademic_semester_uid : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "semester_uid",
                schema: "EDU",
                table: "provision_register",
                newName: "academic_semester_uid");

            migrationBuilder.RenameColumn(
                name: "semester_uid",
                schema: "EDU",
                table: "final_register",
                newName: "academic_semester_uid");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "academic_semester_uid",
                schema: "EDU",
                table: "provision_register",
                newName: "semester_uid");

            migrationBuilder.RenameColumn(
                name: "academic_semester_uid",
                schema: "EDU",
                table: "final_register",
                newName: "semester_uid");
        }
    }
}
