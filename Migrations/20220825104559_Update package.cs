﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace edu_api.Migrations
{
    public partial class Updatepackage : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "entry_year",
                schema: "EDU",
                table: "curriculum_master",
                type: "NVARCHAR2(2000)",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "curriculum_package",
                schema: "EDU",
                columns: table => new
                {
                    curriculum_package_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
                    curriculum_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
                    register_year = table.Column<string>(type: "NVARCHAR2(2000)", nullable: true),
                    semester_1_amount = table.Column<decimal>(type: "DECIMAL(18,4)", precision: 18, scale: 4, nullable: true),
                    semester_2_amount = table.Column<decimal>(type: "DECIMAL(18,4)", precision: 18, scale: 4, nullable: true),
                    semester_3_amount = table.Column<decimal>(type: "DECIMAL(18,4)", precision: 18, scale: 4, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_curriculum_package", x => x.curriculum_package_uid);
                    table.ForeignKey(
                        name: "FK_curriculum_package_curriculum_master_curriculum_uid",
                        column: x => x.curriculum_uid,
                        principalSchema: "EDU",
                        principalTable: "curriculum_master",
                        principalColumn: "curriculum_uid",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_curriculum_package_curriculum_uid",
                schema: "EDU",
                table: "curriculum_package",
                column: "curriculum_uid");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "curriculum_package",
                schema: "EDU");

            migrationBuilder.DropColumn(
                name: "entry_year",
                schema: "EDU",
                table: "curriculum_master");
        }
    }
}
