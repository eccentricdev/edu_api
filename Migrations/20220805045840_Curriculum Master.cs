﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace edu_api.Migrations
{
    public partial class CurriculumMaster : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "curriculum_master",
                schema: "EDU",
                columns: table => new
                {
                    curriculum_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
                    curriculum_code = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
                    education_type_code = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
                    faculty_code = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
                    total_semester = table.Column<short>(type: "NUMBER(5)", nullable: true),
                    max_year_study = table.Column<short>(type: "NUMBER(5)", nullable: true),
                    minimum_credits = table.Column<short>(type: "NUMBER(5)", nullable: true),
                    normal_year_study = table.Column<short>(type: "NUMBER(5)", nullable: true),
                    curriculum_name_th = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    curriculum_name_en = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    full_degree_name_th = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    full_degree_name_en = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    short_degree_name_th = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    short_degree_name_en = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    curriculum_faculty_name_th = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    curriculum_faculty_name_en = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    curriculum_major_name_th = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    curriculum_major_name_en = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    curriculum_type_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
                    full_degree_doc_name_th = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    full_degree_doc_name_en = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    curriculum_faculty_doc_name_th = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    curriculum_faculty_doc_name_en = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    curriculum_major_doc_name_th = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    curriculum_major_doc_name_en = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    curriculum_year_code = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
                    is_package = table.Column<bool>(type: "NUMBER(1)", nullable: true),
                    is_group_semester = table.Column<bool>(type: "NUMBER(1)", nullable: true),
                    semester_1_amount = table.Column<decimal>(type: "DECIMAL(18,4)", precision: 18, scale: 4, nullable: true),
                    semester_2_amount = table.Column<decimal>(type: "DECIMAL(18,4)", precision: 18, scale: 4, nullable: true),
                    semester_3_amount = table.Column<decimal>(type: "DECIMAL(18,4)", precision: 18, scale: 4, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_curriculum_master", x => x.curriculum_uid);
                });

            migrationBuilder.CreateTable(
                name: "curriculum_instructor_master",
                schema: "EDU",
                columns: table => new
                {
                    curriculum_instructor_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
                    curriculum_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
                    personnel_no = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
                    firstname_th = table.Column<string>(type: "NVARCHAR2(2000)", nullable: true),
                    lastname_th = table.Column<string>(type: "NVARCHAR2(2000)", nullable: true),
                    firstname_en = table.Column<string>(type: "NVARCHAR2(2000)", nullable: true),
                    lastname_en = table.Column<string>(type: "NVARCHAR2(2000)", nullable: true),
                    id_card = table.Column<string>(type: "NVARCHAR2(2000)", nullable: true),
                    academic_position_name_th = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    academic_position_name_en = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    graduate = table.Column<string>(type: "NVARCHAR2(2000)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_curriculum_instructor_master", x => x.curriculum_instructor_uid);
                    table.ForeignKey(
                        name: "FK_curriculum_instructor_master_curriculum_master_curriculum_uid",
                        column: x => x.curriculum_uid,
                        principalSchema: "EDU",
                        principalTable: "curriculum_master",
                        principalColumn: "curriculum_uid",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "curriculum_subject_category_master",
                schema: "EDU",
                columns: table => new
                {
                    curriculum_subject_category_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
                    curriculum_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
                    curriculum_subject_category_code = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
                    curriculum_subject_category_name_th = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    curriculum_subject_category_name_en = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    total_credit = table.Column<short>(type: "NUMBER(5)", nullable: true),
                    total_subject = table.Column<short>(type: "NUMBER(5)", nullable: true),
                    min_credit = table.Column<short>(type: "NUMBER(5)", nullable: true),
                    max_credit = table.Column<short>(type: "NUMBER(5)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_curriculum_subject_category_master", x => x.curriculum_subject_category_uid);
                    table.ForeignKey(
                        name: "FK_curriculum_subject_category_master_curriculum_master_curriculum_uid",
                        column: x => x.curriculum_uid,
                        principalSchema: "EDU",
                        principalTable: "curriculum_master",
                        principalColumn: "curriculum_uid",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "education_plan_master",
                schema: "EDU",
                columns: table => new
                {
                    education_plan_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
                    curriculum_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
                    education_plan_name = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_education_plan_master", x => x.education_plan_uid);
                    table.ForeignKey(
                        name: "FK_education_plan_master_curriculum_master_curriculum_uid",
                        column: x => x.curriculum_uid,
                        principalSchema: "EDU",
                        principalTable: "curriculum_master",
                        principalColumn: "curriculum_uid",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "curriculum_subject_group_master",
                schema: "EDU",
                columns: table => new
                {
                    curriculum_subject_group_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
                    curriculum_subject_category_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
                    curriculum_subject_group_code = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
                    curriculum_subject_group_name_th = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    curriculum_subject_group_name_en = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    total_credit = table.Column<short>(type: "NUMBER(5)", nullable: true),
                    total_subject = table.Column<short>(type: "NUMBER(5)", nullable: true),
                    min_credit = table.Column<short>(type: "NUMBER(5)", nullable: true),
                    max_credit = table.Column<short>(type: "NUMBER(5)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_curriculum_subject_group_master", x => x.curriculum_subject_group_uid);
                    table.ForeignKey(
                        name: "FK_curriculum_subject_group_master_curriculum_subject_category_master_curriculum_subject_category_uid",
                        column: x => x.curriculum_subject_category_uid,
                        principalSchema: "EDU",
                        principalTable: "curriculum_subject_category_master",
                        principalColumn: "curriculum_subject_category_uid",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "class_unit_head_master",
                schema: "EDU",
                columns: table => new
                {
                    class_unit_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
                    education_plan_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
                    class_unit = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_class_unit_head_master", x => x.class_unit_uid);
                    table.ForeignKey(
                        name: "FK_class_unit_head_master_education_plan_master_education_plan_uid",
                        column: x => x.education_plan_uid,
                        principalSchema: "EDU",
                        principalTable: "education_plan_master",
                        principalColumn: "education_plan_uid",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "curriculum_subject_master",
                schema: "EDU",
                columns: table => new
                {
                    curriculum_subject_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
                    curriculum_subject_group_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
                    subject_code = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
                    curriculum_subject_group_code = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_curriculum_subject_master", x => x.curriculum_subject_uid);
                    table.ForeignKey(
                        name: "FK_curriculum_subject_master_curriculum_subject_group_master_curriculum_subject_group_uid",
                        column: x => x.curriculum_subject_group_uid,
                        principalSchema: "EDU",
                        principalTable: "curriculum_subject_group_master",
                        principalColumn: "curriculum_subject_group_uid",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "course_semester_head_master",
                schema: "EDU",
                columns: table => new
                {
                    course_semester_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
                    class_unit_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
                    course_semester = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_course_semester_head_master", x => x.course_semester_uid);
                    table.ForeignKey(
                        name: "FK_course_semester_head_master_class_unit_head_master_class_unit_uid",
                        column: x => x.class_unit_uid,
                        principalSchema: "EDU",
                        principalTable: "class_unit_head_master",
                        principalColumn: "class_unit_uid",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "education_plan_detail_master",
                schema: "EDU",
                columns: table => new
                {
                    education_plan_detail = table.Column<Guid>(type: "RAW(16)", nullable: false),
                    course_semester_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
                    education_plan_code = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
                    education_plan_subject = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    education_plan_credit = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_education_plan_detail_master", x => x.education_plan_detail);
                    table.ForeignKey(
                        name: "FK_education_plan_detail_master_course_semester_head_master_course_semester_uid",
                        column: x => x.course_semester_uid,
                        principalSchema: "EDU",
                        principalTable: "course_semester_head_master",
                        principalColumn: "course_semester_uid",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_class_unit_head_master_education_plan_uid",
                schema: "EDU",
                table: "class_unit_head_master",
                column: "education_plan_uid");

            migrationBuilder.CreateIndex(
                name: "IX_course_semester_head_master_class_unit_uid",
                schema: "EDU",
                table: "course_semester_head_master",
                column: "class_unit_uid");

            migrationBuilder.CreateIndex(
                name: "IX_curriculum_instructor_master_curriculum_uid",
                schema: "EDU",
                table: "curriculum_instructor_master",
                column: "curriculum_uid");

            migrationBuilder.CreateIndex(
                name: "IX_curriculum_subject_category_master_curriculum_uid",
                schema: "EDU",
                table: "curriculum_subject_category_master",
                column: "curriculum_uid");

            migrationBuilder.CreateIndex(
                name: "IX_curriculum_subject_group_master_curriculum_subject_category_uid",
                schema: "EDU",
                table: "curriculum_subject_group_master",
                column: "curriculum_subject_category_uid");

            migrationBuilder.CreateIndex(
                name: "IX_curriculum_subject_master_curriculum_subject_group_uid",
                schema: "EDU",
                table: "curriculum_subject_master",
                column: "curriculum_subject_group_uid");

            migrationBuilder.CreateIndex(
                name: "IX_education_plan_detail_master_course_semester_uid",
                schema: "EDU",
                table: "education_plan_detail_master",
                column: "course_semester_uid");

            migrationBuilder.CreateIndex(
                name: "IX_education_plan_master_curriculum_uid",
                schema: "EDU",
                table: "education_plan_master",
                column: "curriculum_uid");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "curriculum_instructor_master",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "curriculum_subject_master",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "education_plan_detail_master",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "curriculum_subject_group_master",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "course_semester_head_master",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "curriculum_subject_category_master",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "class_unit_head_master",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "education_plan_master",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "curriculum_master",
                schema: "EDU");
        }
    }
}
