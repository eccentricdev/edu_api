﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace edu_api.Migrations
{
    public partial class Transfer3types : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "transfer_degree_request",
                schema: "EDU",
                columns: table => new
                {
                    transfer_degree_request_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
                    status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
                    created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
                    updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
                    owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
                    student_request_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
                    mobile_no = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
                    email = table.Column<string>(type: "NVARCHAR2(2000)", nullable: true),
                    highest_education_level_name = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    institute_name = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    faculty_name = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    major_name = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    gpa = table.Column<decimal>(type: "decimal(5,2)", nullable: true),
                    start_year = table.Column<string>(type: "NVARCHAR2(4)", maxLength: 4, nullable: true),
                    last_year = table.Column<string>(type: "NVARCHAR2(4)", maxLength: 4, nullable: true),
                    leave_reason_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
                    other_reason_name = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    academic_year_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
                    academic_semester_uid = table.Column<Guid>(type: "RAW(16)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_transfer_degree_request", x => x.transfer_degree_request_uid);
                    table.ForeignKey(
                        name: "FK_transfer_degree_request_student_request_student_request_uid",
                        column: x => x.student_request_uid,
                        principalSchema: "EDU",
                        principalTable: "student_request",
                        principalColumn: "student_request_uid",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "transfer_institute_request",
                schema: "EDU",
                columns: table => new
                {
                    transfer_institute_request_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
                    status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
                    created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
                    updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
                    owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
                    student_request_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
                    mobile_no = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
                    email = table.Column<string>(type: "NVARCHAR2(2000)", nullable: true),
                    highest_education_level_name = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    institute_name = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    faculty_name = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    major_name = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    gpa = table.Column<decimal>(type: "decimal(5,2)", nullable: true),
                    start_year = table.Column<string>(type: "NVARCHAR2(4)", maxLength: 4, nullable: true),
                    last_year = table.Column<string>(type: "NVARCHAR2(4)", maxLength: 4, nullable: true),
                    leave_reason_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
                    other_reason_name = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    academic_year_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
                    academic_semester_uid = table.Column<Guid>(type: "RAW(16)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_transfer_institute_request", x => x.transfer_institute_request_uid);
                    table.ForeignKey(
                        name: "FK_transfer_institute_request_student_request_student_request_uid",
                        column: x => x.student_request_uid,
                        principalSchema: "EDU",
                        principalTable: "student_request",
                        principalColumn: "student_request_uid",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "transfer_leisure_request",
                schema: "EDU",
                columns: table => new
                {
                    transfer_leisure_request_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
                    status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
                    created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
                    updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
                    owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
                    student_request_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
                    mobile_no = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
                    email = table.Column<string>(type: "NVARCHAR2(2000)", nullable: true),
                    highest_education_level_name = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    institute_name = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    faculty_name = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    major_name = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    gpa = table.Column<decimal>(type: "decimal(5,2)", nullable: true),
                    start_year = table.Column<string>(type: "NVARCHAR2(4)", maxLength: 4, nullable: true),
                    last_year = table.Column<string>(type: "NVARCHAR2(4)", maxLength: 4, nullable: true),
                    leave_reason_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
                    other_reason_name = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    academic_year_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
                    academic_semester_uid = table.Column<Guid>(type: "RAW(16)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_transfer_leisure_request", x => x.transfer_leisure_request_uid);
                    table.ForeignKey(
                        name: "FK_transfer_leisure_request_student_request_student_request_uid",
                        column: x => x.student_request_uid,
                        principalSchema: "EDU",
                        principalTable: "student_request",
                        principalColumn: "student_request_uid",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "transfer_degree_document",
                schema: "EDU",
                columns: table => new
                {
                    transfer_degree_document_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
                    status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
                    created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
                    updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
                    owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
                    transfer_degree_request_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
                    document_type_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
                    document_type_name = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    file_name = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    document_name = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    document_url = table.Column<string>(type: "NVARCHAR2(500)", maxLength: 500, nullable: true),
                    admin_file_name = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    admin_document_url = table.Column<string>(type: "NVARCHAR2(500)", maxLength: 500, nullable: true),
                    mime_type = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
                    document_status_id = table.Column<int>(type: "NUMBER(10)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_transfer_degree_document", x => x.transfer_degree_document_uid);
                    table.ForeignKey(
                        name: "FK_transfer_degree_document_transfer_degree_request_transfer_degree_request_uid",
                        column: x => x.transfer_degree_request_uid,
                        principalSchema: "EDU",
                        principalTable: "transfer_degree_request",
                        principalColumn: "transfer_degree_request_uid",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "transfer_degree_subject",
                schema: "EDU",
                columns: table => new
                {
                    transfer_degree_subject_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
                    status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
                    created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
                    updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
                    owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
                    transfer_degree_request_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
                    transfer_subject_category_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
                    subject_code = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
                    subject_name = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    credits = table.Column<decimal>(type: "decimal(5,2)", nullable: true),
                    receive_grade = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_transfer_degree_subject", x => x.transfer_degree_subject_uid);
                    table.ForeignKey(
                        name: "FK_transfer_degree_subject_transfer_degree_request_transfer_degree_request_uid",
                        column: x => x.transfer_degree_request_uid,
                        principalSchema: "EDU",
                        principalTable: "transfer_degree_request",
                        principalColumn: "transfer_degree_request_uid",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "transfer_institute_document",
                schema: "EDU",
                columns: table => new
                {
                    transfer_institute_document_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
                    status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
                    created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
                    updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
                    owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
                    transfer_institute_request_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
                    document_type_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
                    document_type_name = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    file_name = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    document_name = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    document_url = table.Column<string>(type: "NVARCHAR2(500)", maxLength: 500, nullable: true),
                    admin_file_name = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    admin_document_url = table.Column<string>(type: "NVARCHAR2(500)", maxLength: 500, nullable: true),
                    mime_type = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
                    document_status_id = table.Column<int>(type: "NUMBER(10)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_transfer_institute_document", x => x.transfer_institute_document_uid);
                    table.ForeignKey(
                        name: "FK_transfer_institute_document_transfer_institute_request_transfer_institute_request_uid",
                        column: x => x.transfer_institute_request_uid,
                        principalSchema: "EDU",
                        principalTable: "transfer_institute_request",
                        principalColumn: "transfer_institute_request_uid",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "transfer_institute_subject",
                schema: "EDU",
                columns: table => new
                {
                    transfer_institute_subject_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
                    status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
                    created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
                    updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
                    owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
                    transfer_institute_request_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
                    transfer_subject_category_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
                    subject_code = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
                    subject_name = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    credits = table.Column<decimal>(type: "decimal(5,2)", nullable: true),
                    receive_grade = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_transfer_institute_subject", x => x.transfer_institute_subject_uid);
                    table.ForeignKey(
                        name: "FK_transfer_institute_subject_transfer_institute_request_transfer_institute_request_uid",
                        column: x => x.transfer_institute_request_uid,
                        principalSchema: "EDU",
                        principalTable: "transfer_institute_request",
                        principalColumn: "transfer_institute_request_uid",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "transfer_leisure_document",
                schema: "EDU",
                columns: table => new
                {
                    transfer_leisure_document_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
                    status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
                    created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
                    updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
                    owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
                    transfer_leisure_request_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
                    document_type_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
                    document_type_name = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    file_name = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    document_name = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    document_url = table.Column<string>(type: "NVARCHAR2(500)", maxLength: 500, nullable: true),
                    admin_file_name = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    admin_document_url = table.Column<string>(type: "NVARCHAR2(500)", maxLength: 500, nullable: true),
                    mime_type = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
                    document_status_id = table.Column<int>(type: "NUMBER(10)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_transfer_leisure_document", x => x.transfer_leisure_document_uid);
                    table.ForeignKey(
                        name: "FK_transfer_leisure_document_transfer_leisure_request_transfer_leisure_request_uid",
                        column: x => x.transfer_leisure_request_uid,
                        principalSchema: "EDU",
                        principalTable: "transfer_leisure_request",
                        principalColumn: "transfer_leisure_request_uid",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "transfer_leisure_subject",
                schema: "EDU",
                columns: table => new
                {
                    transfer_leisure_subject_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
                    status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
                    created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
                    updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
                    owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
                    transfer_leisure_request_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
                    transfer_subject_category_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
                    subject_code = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
                    subject_name = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    credits = table.Column<decimal>(type: "decimal(5,2)", nullable: true),
                    receive_grade = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_transfer_leisure_subject", x => x.transfer_leisure_subject_uid);
                    table.ForeignKey(
                        name: "FK_transfer_leisure_subject_transfer_leisure_request_transfer_leisure_request_uid",
                        column: x => x.transfer_leisure_request_uid,
                        principalSchema: "EDU",
                        principalTable: "transfer_leisure_request",
                        principalColumn: "transfer_leisure_request_uid",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_transfer_degree_document_transfer_degree_request_uid",
                schema: "EDU",
                table: "transfer_degree_document",
                column: "transfer_degree_request_uid");

            migrationBuilder.CreateIndex(
                name: "IX_transfer_degree_request_student_request_uid",
                schema: "EDU",
                table: "transfer_degree_request",
                column: "student_request_uid");

            migrationBuilder.CreateIndex(
                name: "IX_transfer_degree_subject_transfer_degree_request_uid",
                schema: "EDU",
                table: "transfer_degree_subject",
                column: "transfer_degree_request_uid");

            migrationBuilder.CreateIndex(
                name: "IX_transfer_institute_document_transfer_institute_request_uid",
                schema: "EDU",
                table: "transfer_institute_document",
                column: "transfer_institute_request_uid");

            migrationBuilder.CreateIndex(
                name: "IX_transfer_institute_request_student_request_uid",
                schema: "EDU",
                table: "transfer_institute_request",
                column: "student_request_uid");

            migrationBuilder.CreateIndex(
                name: "IX_transfer_institute_subject_transfer_institute_request_uid",
                schema: "EDU",
                table: "transfer_institute_subject",
                column: "transfer_institute_request_uid");

            migrationBuilder.CreateIndex(
                name: "IX_transfer_leisure_document_transfer_leisure_request_uid",
                schema: "EDU",
                table: "transfer_leisure_document",
                column: "transfer_leisure_request_uid");

            migrationBuilder.CreateIndex(
                name: "IX_transfer_leisure_request_student_request_uid",
                schema: "EDU",
                table: "transfer_leisure_request",
                column: "student_request_uid");

            migrationBuilder.CreateIndex(
                name: "IX_transfer_leisure_subject_transfer_leisure_request_uid",
                schema: "EDU",
                table: "transfer_leisure_subject",
                column: "transfer_leisure_request_uid");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "transfer_degree_document",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "transfer_degree_subject",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "transfer_institute_document",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "transfer_institute_subject",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "transfer_leisure_document",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "transfer_leisure_subject",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "transfer_degree_request",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "transfer_institute_request",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "transfer_leisure_request",
                schema: "EDU");
        }
    }
}
