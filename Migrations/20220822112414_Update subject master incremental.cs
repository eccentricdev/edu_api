﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace edu_api.Migrations
{
    public partial class Updatesubjectmasterincremental : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_subject_master_owner_subject_master_subject_master_uid",
                schema: "EDU",
                table: "subject_master_owner");

            migrationBuilder.DropForeignKey(
                name: "FK_subject_master_year_subject_master_subject_master_uid",
                schema: "EDU",
                table: "subject_master_year");

            migrationBuilder.DropForeignKey(
                name: "FK_subject_master_year_cost_subject_master_year_subject_master_year_uid",
                schema: "EDU",
                table: "subject_master_year_cost");

            migrationBuilder.DropForeignKey(
                name: "FK_subject_master_year_topic_subject_master_year_subject_master_year_uid",
                schema: "EDU",
                table: "subject_master_year_topic");

            migrationBuilder.AddForeignKey(
                name: "FK_subject_master_owner_subject_master_subject_master_uid",
                schema: "EDU",
                table: "subject_master_owner",
                column: "subject_master_uid",
                principalSchema: "EDU",
                principalTable: "subject_master",
                principalColumn: "subject_master_uid",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_subject_master_year_subject_master_subject_master_uid",
                schema: "EDU",
                table: "subject_master_year",
                column: "subject_master_uid",
                principalSchema: "EDU",
                principalTable: "subject_master",
                principalColumn: "subject_master_uid",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_subject_master_year_cost_subject_master_year_subject_master_year_uid",
                schema: "EDU",
                table: "subject_master_year_cost",
                column: "subject_master_year_uid",
                principalSchema: "EDU",
                principalTable: "subject_master_year",
                principalColumn: "subject_master_year_uid",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_subject_master_year_topic_subject_master_year_subject_master_year_uid",
                schema: "EDU",
                table: "subject_master_year_topic",
                column: "subject_master_year_uid",
                principalSchema: "EDU",
                principalTable: "subject_master_year",
                principalColumn: "subject_master_year_uid",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_subject_master_owner_subject_master_subject_master_uid",
                schema: "EDU",
                table: "subject_master_owner");

            migrationBuilder.DropForeignKey(
                name: "FK_subject_master_year_subject_master_subject_master_uid",
                schema: "EDU",
                table: "subject_master_year");

            migrationBuilder.DropForeignKey(
                name: "FK_subject_master_year_cost_subject_master_year_subject_master_year_uid",
                schema: "EDU",
                table: "subject_master_year_cost");

            migrationBuilder.DropForeignKey(
                name: "FK_subject_master_year_topic_subject_master_year_subject_master_year_uid",
                schema: "EDU",
                table: "subject_master_year_topic");

            migrationBuilder.AddForeignKey(
                name: "FK_subject_master_owner_subject_master_subject_master_uid",
                schema: "EDU",
                table: "subject_master_owner",
                column: "subject_master_uid",
                principalSchema: "EDU",
                principalTable: "subject_master",
                principalColumn: "subject_master_uid",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_subject_master_year_subject_master_subject_master_uid",
                schema: "EDU",
                table: "subject_master_year",
                column: "subject_master_uid",
                principalSchema: "EDU",
                principalTable: "subject_master",
                principalColumn: "subject_master_uid",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_subject_master_year_cost_subject_master_year_subject_master_year_uid",
                schema: "EDU",
                table: "subject_master_year_cost",
                column: "subject_master_year_uid",
                principalSchema: "EDU",
                principalTable: "subject_master_year",
                principalColumn: "subject_master_year_uid",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_subject_master_year_topic_subject_master_year_subject_master_year_uid",
                schema: "EDU",
                table: "subject_master_year_topic",
                column: "subject_master_year_uid",
                principalSchema: "EDU",
                principalTable: "subject_master_year",
                principalColumn: "subject_master_year_uid",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
