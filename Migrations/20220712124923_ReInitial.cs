﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace edu_api.Migrations
{
    public partial class ReInitial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            // migrationBuilder.EnsureSchema(
            //     name: "EDU");
            //
            // migrationBuilder.CreateTable(
            //     name: "academic_calendar_type",
            //     schema: "EDU",
            //     columns: table => new
            //     {
            //         academic_calendar_type_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
            //         status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         academic_calendar_type_code = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
            //         academic_calendar_type_name_th = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         academic_calendar_type_name_en = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         remark = table.Column<string>(type: "NVARCHAR2(1000)", maxLength: 1000, nullable: true)
            //     },
            //     constraints: table =>
            //     {
            //         table.PrimaryKey("PK_academic_calendar_type", x => x.academic_calendar_type_uid);
            //     });
            //
            // migrationBuilder.CreateTable(
            //     name: "academic_register_type",
            //     schema: "EDU",
            //     columns: table => new
            //     {
            //         academic_register_type_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
            //         status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         academic_register_type_code = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
            //         academic_register_type_name_th = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         academic_register_type_name_en = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         remark = table.Column<string>(type: "NVARCHAR2(1000)", maxLength: 1000, nullable: true),
            //         has_fine = table.Column<bool>(type: "NUMBER(1)", nullable: true),
            //         display_order = table.Column<int>(type: "NUMBER(10)", nullable: true)
            //     },
            //     constraints: table =>
            //     {
            //         table.PrimaryKey("PK_academic_register_type", x => x.academic_register_type_uid);
            //     });
            //
            // migrationBuilder.CreateTable(
            //     name: "academic_year",
            //     schema: "EDU",
            //     columns: table => new
            //     {
            //         academic_year_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
            //         status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         academic_year_code = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
            //         academic_year_name_en = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         academic_year_name_th = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         start_date = table.Column<DateTime>(type: "date", nullable: true),
            //         end_date = table.Column<DateTime>(type: "date", nullable: true),
            //         remark = table.Column<string>(type: "NVARCHAR2(1000)", maxLength: 1000, nullable: true),
            //         semester_type_uid = table.Column<Guid>(type: "RAW(16)", nullable: true)
            //     },
            //     constraints: table =>
            //     {
            //         table.PrimaryKey("PK_academic_year", x => x.academic_year_uid);
            //     });
            //
            // migrationBuilder.CreateTable(
            //     name: "address",
            //     schema: "EDU",
            //     columns: table => new
            //     {
            //         address_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
            //         status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         address_name = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         house_no = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
            //         room_no = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
            //         floor_no = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
            //         building_name = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         village_name = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         village_no = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
            //         alley_name = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         street_name = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         sub_district_id = table.Column<int>(type: "NUMBER(10)", nullable: true),
            //         other_sub_district_name_th = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         other_sub_district_name_en = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         other_district_name_th = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         other_district_name_en = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         other_province_name_th = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         other_province_name_en = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         postal_code = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
            //         country_id = table.Column<int>(type: "NUMBER(10)", nullable: true),
            //         telephone_no = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true)
            //     },
            //     constraints: table =>
            //     {
            //         table.PrimaryKey("PK_address", x => x.address_uid);
            //     });
            //
            // migrationBuilder.CreateTable(
            //     name: "address_type",
            //     schema: "EDU",
            //     columns: table => new
            //     {
            //         address_type_id = table.Column<int>(type: "NUMBER(10)", nullable: false)
            //             .Annotation("Oracle:Identity", "1, 1"),
            //         status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         address_type_code = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
            //         address_type_name_th = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         address_type_name_en = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true)
            //     },
            //     constraints: table =>
            //     {
            //         table.PrimaryKey("PK_address_type", x => x.address_type_id);
            //     });
            //
            // migrationBuilder.CreateTable(
            //     name: "advance_payment",
            //     schema: "EDU",
            //     columns: table => new
            //     {
            //         advance_payment_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
            //         status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         student_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         advance_date = table.Column<DateTime>(type: "date", nullable: true),
            //         advance_amount = table.Column<decimal>(type: "DECIMAL(18,4)", precision: 18, scale: 4, nullable: true),
            //         advance_no = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
            //         left_amount = table.Column<decimal>(type: "DECIMAL(18,4)", precision: 18, scale: 4, nullable: true),
            //         is_use = table.Column<bool>(type: "NUMBER(1)", nullable: true)
            //     },
            //     constraints: table =>
            //     {
            //         table.PrimaryKey("PK_advance_payment", x => x.advance_payment_uid);
            //     });
            //
            // migrationBuilder.CreateTable(
            //     name: "advance_status",
            //     schema: "EDU",
            //     columns: table => new
            //     {
            //         advance_status_id = table.Column<int>(type: "NUMBER(10)", nullable: false)
            //             .Annotation("Oracle:Identity", "1, 1"),
            //         status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         advance_status_code = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
            //         advance_status_name_th = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         advance_status_name_en = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true)
            //     },
            //     constraints: table =>
            //     {
            //         table.PrimaryKey("PK_advance_status", x => x.advance_status_id);
            //     });
            //
            // migrationBuilder.CreateTable(
            //     name: "advance_type",
            //     schema: "EDU",
            //     columns: table => new
            //     {
            //         advance_type_id = table.Column<int>(type: "NUMBER(10)", nullable: false)
            //             .Annotation("Oracle:Identity", "1, 1"),
            //         status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         advance_type_code = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
            //         advance_type_name_th = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         advance_type_name_en = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true)
            //     },
            //     constraints: table =>
            //     {
            //         table.PrimaryKey("PK_advance_type", x => x.advance_type_id);
            //     });
            //
            // migrationBuilder.CreateTable(
            //     name: "answer_type",
            //     schema: "EDU",
            //     columns: table => new
            //     {
            //         answer_type_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
            //         status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         answer_type_code = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
            //         answer_type_name_th = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         answer_type_name_en = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true)
            //     },
            //     constraints: table =>
            //     {
            //         table.PrimaryKey("PK_answer_type", x => x.answer_type_uid);
            //     });
            //
            // migrationBuilder.CreateTable(
            //     name: "attached_document",
            //     schema: "EDU",
            //     columns: table => new
            //     {
            //         attached_document_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
            //         status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         attached_document_code = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
            //         attached_document_name_th = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         attached_document_name_en = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true)
            //     },
            //     constraints: table =>
            //     {
            //         table.PrimaryKey("PK_attached_document", x => x.attached_document_uid);
            //     });
            //
            // migrationBuilder.CreateTable(
            //     name: "calendar",
            //     schema: "EDU",
            //     columns: table => new
            //     {
            //         calendar_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
            //         status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         calendar_code = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
            //         calendar_name_th = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         calendar_name_en = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         from_date = table.Column<DateTime>(type: "date", nullable: true),
            //         to_date = table.Column<DateTime>(type: "date", nullable: true)
            //     },
            //     constraints: table =>
            //     {
            //         table.PrimaryKey("PK_calendar", x => x.calendar_uid);
            //     });
            //
            // migrationBuilder.CreateTable(
            //     name: "cause_failed",
            //     schema: "EDU",
            //     columns: table => new
            //     {
            //         cause_failed_id = table.Column<int>(type: "NUMBER(10)", nullable: false)
            //             .Annotation("Oracle:Identity", "1, 1"),
            //         status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         cause_failed_code = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
            //         cause_failed_name_th = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         cause_failed_name_en = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true)
            //     },
            //     constraints: table =>
            //     {
            //         table.PrimaryKey("PK_cause_failed", x => x.cause_failed_id);
            //     });
            //
            // migrationBuilder.CreateTable(
            //     name: "college_faculty",
            //     schema: "EDU",
            //     columns: table => new
            //     {
            //         college_faculty_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
            //         status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         college_faculty_code = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
            //         college_faculty_name_th = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         college_faculty_name_en = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         remark = table.Column<string>(type: "NVARCHAR2(1000)", maxLength: 1000, nullable: true)
            //     },
            //     constraints: table =>
            //     {
            //         table.PrimaryKey("PK_college_faculty", x => x.college_faculty_uid);
            //     });
            //
            // migrationBuilder.CreateTable(
            //     name: "curriculum",
            //     schema: "EDU",
            //     columns: table => new
            //     {
            //         curriculum_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
            //         status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         education_type_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         college_faculty_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         curriculum_year_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         curriculum_code = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
            //         education_type_code = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
            //         faculty_code = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
            //         total_semester = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         max_year_study = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         minimum_credits = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         normal_year_study = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         curriculum_name_th = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         curriculum_name_en = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         full_degree_name_th = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         full_degree_name_en = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         short_degree_name_th = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         short_degree_name_en = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         curriculum_faculty_name_th = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         curriculum_faculty_name_en = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         curriculum_major_name_th = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         curriculum_major_name_en = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         curriculum_type_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         full_degree_doc_name_th = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         full_degree_doc_name_en = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         curriculum_faculty_doc_name_th = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         curriculum_faculty_doc_name_en = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         curriculum_major_doc_name_th = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         curriculum_major_doc_name_en = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         curriculum_year_code = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
            //         is_package = table.Column<bool>(type: "NUMBER(1)", nullable: true),
            //         is_group_semester = table.Column<bool>(type: "NUMBER(1)", nullable: true),
            //         semester_1_amount = table.Column<decimal>(type: "DECIMAL(18,4)", precision: 18, scale: 4, nullable: true),
            //         semester_2_amount = table.Column<decimal>(type: "DECIMAL(18,4)", precision: 18, scale: 4, nullable: true),
            //         semester_3_amount = table.Column<decimal>(type: "DECIMAL(18,4)", precision: 18, scale: 4, nullable: true),
            //         faculty_curriculum_uid = table.Column<Guid>(type: "RAW(16)", nullable: true)
            //     },
            //     constraints: table =>
            //     {
            //         table.PrimaryKey("PK_curriculum", x => x.curriculum_uid);
            //     });
            //
            // migrationBuilder.CreateTable(
            //     name: "curriculum_year",
            //     schema: "EDU",
            //     columns: table => new
            //     {
            //         curriculum_year_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
            //         status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         curriculum_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         academic_year_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         academic_year_code = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true)
            //     },
            //     constraints: table =>
            //     {
            //         table.PrimaryKey("PK_curriculum_year", x => x.curriculum_year_uid);
            //     });
            //
            // migrationBuilder.CreateTable(
            //     name: "discipline_period_type",
            //     schema: "EDU",
            //     columns: table => new
            //     {
            //         discipline_period_type_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
            //         status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         discipline_period_type_code = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
            //         discipline_period_type_name_th = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         discipline_period_type_name_en = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true)
            //     },
            //     constraints: table =>
            //     {
            //         table.PrimaryKey("PK_discipline_period_type", x => x.discipline_period_type_uid);
            //     });
            //
            // migrationBuilder.CreateTable(
            //     name: "discipline_punishment",
            //     schema: "EDU",
            //     columns: table => new
            //     {
            //         discipline_punishment_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
            //         status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         discipline_punishment_code = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
            //         discipline_punishment_name_th = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         discipline_punishment_name_en = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true)
            //     },
            //     constraints: table =>
            //     {
            //         table.PrimaryKey("PK_discipline_punishment", x => x.discipline_punishment_uid);
            //     });
            //
            // migrationBuilder.CreateTable(
            //     name: "discipline_status",
            //     schema: "EDU",
            //     columns: table => new
            //     {
            //         discipline_status_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
            //         status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         discipline_status_code = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
            //         discipline_status_name_th = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         discipline_status_name_en = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true)
            //     },
            //     constraints: table =>
            //     {
            //         table.PrimaryKey("PK_discipline_status", x => x.discipline_status_uid);
            //     });
            //
            // migrationBuilder.CreateTable(
            //     name: "discipline_year",
            //     schema: "EDU",
            //     columns: table => new
            //     {
            //         discipline_year_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
            //         status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         academic_year_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         discipline_year_code = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
            //         discipline_year_name_th = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         discipline_year_name_en = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true)
            //     },
            //     constraints: table =>
            //     {
            //         table.PrimaryKey("PK_discipline_year", x => x.discipline_year_uid);
            //     });
            //
            // migrationBuilder.CreateTable(
            //     name: "dynamic_fee",
            //     schema: "EDU",
            //     columns: table => new
            //     {
            //         dynamic_fee_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
            //         status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         dynamic_fee_code = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
            //         dynamic_fee_short_name_en = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         dynamic_fee_short_name_th = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         dynamic_fee_name_th = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         dynamic_fee_name_en = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true)
            //     },
            //     constraints: table =>
            //     {
            //         table.PrimaryKey("PK_dynamic_fee", x => x.dynamic_fee_uid);
            //     });
            //
            // migrationBuilder.CreateTable(
            //     name: "dynamic_fee_apply",
            //     schema: "EDU",
            //     columns: table => new
            //     {
            //         dynamic_fee_apply_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
            //         status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         dynamic_fee_apply_code = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
            //         dynamic_fee_apply_short_name_en = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         dynamic_fee_apply_short_name_th = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         dynamic_fee_apply_name_th = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         dynamic_fee_apply_name_en = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true)
            //     },
            //     constraints: table =>
            //     {
            //         table.PrimaryKey("PK_dynamic_fee_apply", x => x.dynamic_fee_apply_uid);
            //     });
            //
            // migrationBuilder.CreateTable(
            //     name: "education_check_document",
            //     schema: "EDU",
            //     columns: table => new
            //     {
            //         education_check_document_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
            //         status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         academic_year_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         academic_semester_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         school_id = table.Column<int>(type: "NUMBER(10)", nullable: true),
            //         document_date = table.Column<DateTime>(type: "date", nullable: true),
            //         document_no = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
            //         education_check_document_status_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         remark = table.Column<string>(type: "NVARCHAR2(500)", maxLength: 500, nullable: true),
            //         update_program = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
            //         no_of_students = table.Column<int>(type: "NUMBER(10)", nullable: true),
            //         no_of_students_correct = table.Column<int>(type: "NUMBER(10)", nullable: true),
            //         no_of_students_not_correct = table.Column<int>(type: "NUMBER(10)", nullable: true)
            //     },
            //     constraints: table =>
            //     {
            //         table.PrimaryKey("PK_education_check_document", x => x.education_check_document_uid);
            //     });
            //
            // migrationBuilder.CreateTable(
            //     name: "education_check_document_status",
            //     schema: "EDU",
            //     columns: table => new
            //     {
            //         education_check_document_status_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
            //         status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         education_check_document_status_code = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
            //         education_check_document_status_name_th = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         education_check_document_status_name_en = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         education_check_document_status_short_name_th = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         education_check_document_status_short_name_en = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true)
            //     },
            //     constraints: table =>
            //     {
            //         table.PrimaryKey("PK_education_check_document_status", x => x.education_check_document_status_uid);
            //     });
            //
            // migrationBuilder.CreateTable(
            //     name: "education_plan",
            //     schema: "EDU",
            //     columns: table => new
            //     {
            //         education_plan_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
            //         status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         faculty_curriculum_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         academic_year_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         academic_semester_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         education_plan_code = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
            //         education_plan_name_th = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         education_plan_name_en = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         advisor_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         description = table.Column<string>(type: "NVARCHAR2(500)", maxLength: 500, nullable: true),
            //         remark = table.Column<string>(type: "NVARCHAR2(1000)", maxLength: 1000, nullable: true),
            //         total_subject = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         total_credit = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         credit_amount = table.Column<decimal>(type: "DECIMAL(18,4)", precision: 18, scale: 4, nullable: true),
            //         fee_amount = table.Column<decimal>(type: "DECIMAL(18,4)", precision: 18, scale: 4, nullable: true),
            //         total_amount = table.Column<decimal>(type: "DECIMAL(18,4)", precision: 18, scale: 4, nullable: true),
            //         quota = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         reserve = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         confirm = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         education_type_uid = table.Column<Guid>(type: "RAW(16)", nullable: true)
            //     },
            //     constraints: table =>
            //     {
            //         table.PrimaryKey("PK_education_plan", x => x.education_plan_uid);
            //     });
            //
            // migrationBuilder.CreateTable(
            //     name: "education_type",
            //     schema: "EDU",
            //     columns: table => new
            //     {
            //         education_type_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
            //         status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         education_type_code = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
            //         education_type_name_th = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         education_type_name_en = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         remark = table.Column<string>(type: "NVARCHAR2(1000)", maxLength: 1000, nullable: true),
            //         education_type_level_code = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
            //         education_type_level_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         dynamic_fee_education_type_uid = table.Column<Guid>(type: "RAW(16)", nullable: true)
            //     },
            //     constraints: table =>
            //     {
            //         table.PrimaryKey("PK_education_type", x => x.education_type_uid);
            //     });
            //
            // migrationBuilder.CreateTable(
            //     name: "education_type_level",
            //     schema: "EDU",
            //     columns: table => new
            //     {
            //         education_type_level_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
            //         status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         education_type_level_code = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
            //         education_type_level_name_th = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         education_type_level_name_en = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true)
            //     },
            //     constraints: table =>
            //     {
            //         table.PrimaryKey("PK_education_type_level", x => x.education_type_level_uid);
            //     });
            //
            // migrationBuilder.CreateTable(
            //     name: "employee",
            //     schema: "EDU",
            //     columns: table => new
            //     {
            //         employee_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
            //         status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         employee_code = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
            //         prefix_name_th = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
            //         prefix_name_en = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
            //         first_name_th = table.Column<string>(type: "NVARCHAR2(255)", maxLength: 255, nullable: true),
            //         first_name_en = table.Column<string>(type: "NVARCHAR2(255)", maxLength: 255, nullable: true),
            //         last_name_th = table.Column<string>(type: "NVARCHAR2(255)", maxLength: 255, nullable: true),
            //         last_name_en = table.Column<string>(type: "NVARCHAR2(255)", maxLength: 255, nullable: true),
            //         display_name_th = table.Column<string>(type: "NVARCHAR2(500)", maxLength: 500, nullable: true),
            //         display_name_en = table.Column<string>(type: "NVARCHAR2(500)", maxLength: 500, nullable: true),
            //         faculty_code = table.Column<string>(type: "NVARCHAR2(10)", maxLength: 10, nullable: true),
            //         is_quit = table.Column<bool>(type: "NUMBER(1)", nullable: true)
            //     },
            //     constraints: table =>
            //     {
            //         table.PrimaryKey("PK_employee", x => x.employee_uid);
            //     });
            //
            // migrationBuilder.CreateTable(
            //     name: "entry_program",
            //     schema: "EDU",
            //     columns: table => new
            //     {
            //         entry_program_id = table.Column<int>(type: "NUMBER(10)", nullable: false)
            //             .Annotation("Oracle:Identity", "1, 1"),
            //         status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         entry_program_code = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
            //         entry_program_name_th = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         entry_program_name_en = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true)
            //     },
            //     constraints: table =>
            //     {
            //         table.PrimaryKey("PK_entry_program", x => x.entry_program_id);
            //     });
            //
            // migrationBuilder.CreateTable(
            //     name: "entry_type",
            //     schema: "EDU",
            //     columns: table => new
            //     {
            //         entry_type_id = table.Column<int>(type: "NUMBER(10)", nullable: false)
            //             .Annotation("Oracle:Identity", "1, 1"),
            //         status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         entry_type_code = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
            //         entry_type_name_th = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         entry_type_name_en = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true)
            //     },
            //     constraints: table =>
            //     {
            //         table.PrimaryKey("PK_entry_type", x => x.entry_type_id);
            //     });
            //
            // migrationBuilder.CreateTable(
            //     name: "evaluate_status",
            //     schema: "EDU",
            //     columns: table => new
            //     {
            //         evaluate_status_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
            //         status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         evaluate_status_code = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
            //         evaluate_status_name_th = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         evaluate_status_name_en = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true)
            //     },
            //     constraints: table =>
            //     {
            //         table.PrimaryKey("PK_evaluate_status", x => x.evaluate_status_uid);
            //     });
            //
            // migrationBuilder.CreateTable(
            //     name: "evaluate_subject_section",
            //     schema: "EDU",
            //     columns: table => new
            //     {
            //         evaluate_subject_section_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
            //         status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         subject_year_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         academic_year_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         academic_semester_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         study_subject_section_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         no_of_instructors = table.Column<short>(type: "NUMBER(5)", nullable: true)
            //     },
            //     constraints: table =>
            //     {
            //         table.PrimaryKey("PK_evaluate_subject_section", x => x.evaluate_subject_section_uid);
            //     });
            //
            // migrationBuilder.CreateTable(
            //     name: "evaluation_calendar",
            //     schema: "EDU",
            //     columns: table => new
            //     {
            //         evaluation_calendar_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
            //         status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         academic_year_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         academic_semester_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         education_type_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         evaluation_calendar_code = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
            //         evaluation_calendar_name_th = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         evaluation_calendar_name_en = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true)
            //     },
            //     constraints: table =>
            //     {
            //         table.PrimaryKey("PK_evaluation_calendar", x => x.evaluation_calendar_uid);
            //     });
            //
            // migrationBuilder.CreateTable(
            //     name: "evaluation_calendar_assignment",
            //     schema: "EDU",
            //     columns: table => new
            //     {
            //         evaluation_calendar_assignment_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
            //         status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         evaluation_calendar_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         college_faculty_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         faculty_curriculum_uid = table.Column<Guid>(type: "RAW(16)", nullable: true)
            //     },
            //     constraints: table =>
            //     {
            //         table.PrimaryKey("PK_evaluation_calendar_assignment", x => x.evaluation_calendar_assignment_uid);
            //     });
            //
            // migrationBuilder.CreateTable(
            //     name: "evaluation_calendar_item",
            //     schema: "EDU",
            //     columns: table => new
            //     {
            //         evaluation_calendar_item_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
            //         status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         evaluation_calendar_item_code = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
            //         evaluation_calendar_item_name_th = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         evaluation_calendar_item_name_en = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         evaluation_calendar_item_short_name_th = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         evaluation_calendar_item_short_name_en = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         row_order = table.Column<int>(type: "NUMBER(10)", nullable: true)
            //     },
            //     constraints: table =>
            //     {
            //         table.PrimaryKey("PK_evaluation_calendar_item", x => x.evaluation_calendar_item_uid);
            //     });
            //
            // migrationBuilder.CreateTable(
            //     name: "faculty_curriculum",
            //     schema: "EDU",
            //     columns: table => new
            //     {
            //         faculty_curriculum_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
            //         status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         education_type_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         college_faculty_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         faculty_curriculum_code = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
            //         faculty_curriculum_name_th = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         faculty_curriculum_name_en = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         remark = table.Column<string>(type: "NVARCHAR2(1000)", maxLength: 1000, nullable: true),
            //         education_type_code = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
            //         faculty_code = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true)
            //     },
            //     constraints: table =>
            //     {
            //         table.PrimaryKey("PK_faculty_curriculum", x => x.faculty_curriculum_uid);
            //     });
            //
            // migrationBuilder.CreateTable(
            //     name: "family_status",
            //     schema: "EDU",
            //     columns: table => new
            //     {
            //         family_status_id = table.Column<int>(type: "NUMBER(10)", nullable: false)
            //             .Annotation("Oracle:Identity", "1, 1"),
            //         status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         family_status_code = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
            //         family_status_name_th = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         family_status_name_en = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true)
            //     },
            //     constraints: table =>
            //     {
            //         table.PrimaryKey("PK_family_status", x => x.family_status_id);
            //     });
            //
            // migrationBuilder.CreateTable(
            //     name: "fee",
            //     schema: "EDU",
            //     columns: table => new
            //     {
            //         fee_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
            //         status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         fee_code = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
            //         fee_short_name_en = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         fee_short_name_th = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         fee_name_th = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         fee_name_en = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true)
            //     },
            //     constraints: table =>
            //     {
            //         table.PrimaryKey("PK_fee", x => x.fee_uid);
            //     });
            //
            // migrationBuilder.CreateTable(
            //     name: "final_register",
            //     schema: "EDU",
            //     columns: table => new
            //     {
            //         final_register_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
            //         status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         student_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         academic_year_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         semester_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         bill_no = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
            //         bill_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         register_date = table.Column<DateTime>(type: "date", nullable: true),
            //         is_package = table.Column<bool>(type: "NUMBER(1)", nullable: true),
            //         package_amount = table.Column<decimal>(type: "DECIMAL(18,4)", precision: 18, scale: 4, nullable: true),
            //         credit_amount = table.Column<decimal>(type: "DECIMAL(18,4)", precision: 18, scale: 4, nullable: true),
            //         fee_amount = table.Column<decimal>(type: "DECIMAL(18,4)", precision: 18, scale: 4, nullable: true),
            //         scholarship_amount = table.Column<decimal>(type: "DECIMAL(18,4)", precision: 18, scale: 4, nullable: true),
            //         loan_amount = table.Column<decimal>(type: "DECIMAL(18,4)", precision: 18, scale: 4, nullable: true),
            //         advanced_amount = table.Column<decimal>(type: "DECIMAL(18,4)", precision: 18, scale: 4, nullable: true),
            //         total_subject = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         total_credit = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         payment_amount = table.Column<decimal>(type: "DECIMAL(18,4)", precision: 18, scale: 4, nullable: true),
            //         register_type_code = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
            //         register_status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         paid_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         paid_amount = table.Column<decimal>(type: "DECIMAL(18,4)", precision: 18, scale: 4, nullable: true),
            //         receipt_no = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true)
            //     },
            //     constraints: table =>
            //     {
            //         table.PrimaryKey("PK_final_register", x => x.final_register_uid);
            //     });
            //
            // migrationBuilder.CreateTable(
            //     name: "grade",
            //     schema: "EDU",
            //     columns: table => new
            //     {
            //         grade_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
            //         status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         grade_code = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
            //         grade_name_th = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         grade_name_en = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         is_calculate_gpa = table.Column<bool>(type: "NUMBER(1)", nullable: true),
            //         display_order = table.Column<int>(type: "NUMBER(10)", nullable: true),
            //         description = table.Column<string>(type: "NVARCHAR2(500)", maxLength: 500, nullable: true),
            //         is_calculate_credit = table.Column<bool>(type: "NUMBER(1)", nullable: true),
            //         is_pass = table.Column<bool>(type: "NUMBER(1)", nullable: true),
            //         is_calculate_honor = table.Column<bool>(type: "NUMBER(1)", nullable: true),
            //         grade_point = table.Column<decimal>(type: "decimal(18,2)", nullable: true)
            //     },
            //     constraints: table =>
            //     {
            //         table.PrimaryKey("PK_grade", x => x.grade_uid);
            //     });
            //
            // migrationBuilder.CreateTable(
            //     name: "grade_calculation_method",
            //     schema: "EDU",
            //     columns: table => new
            //     {
            //         grade_calculation_method_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
            //         status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         grade_calculation_method_code = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         grade_calculation_method_name_th = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         grade_calculation_method_name_en = table.Column<Guid>(type: "RAW(16)", nullable: true)
            //     },
            //     constraints: table =>
            //     {
            //         table.PrimaryKey("PK_grade_calculation_method", x => x.grade_calculation_method_uid);
            //     });
            //
            // migrationBuilder.CreateTable(
            //     name: "grade_calculation_setting",
            //     schema: "EDU",
            //     columns: table => new
            //     {
            //         grade_calculation_setting_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
            //         status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         education_type_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         from_date = table.Column<DateTime>(type: "date", nullable: true),
            //         to_date = table.Column<DateTime>(type: "date", nullable: true),
            //         grade_calculation_method_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         max_re_grade_credit = table.Column<int>(type: "NUMBER(10)", nullable: true)
            //     },
            //     constraints: table =>
            //     {
            //         table.PrimaryKey("PK_grade_calculation_setting", x => x.grade_calculation_setting_uid);
            //     });
            //
            // migrationBuilder.CreateTable(
            //     name: "grade_calendar",
            //     schema: "EDU",
            //     columns: table => new
            //     {
            //         grade_calendar_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
            //         status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         academic_year_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         academic_semester_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         education_type_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         is_correct_calendar = table.Column<bool>(type: "NUMBER(1)", nullable: true),
            //         from_date = table.Column<DateTime>(type: "date", nullable: true),
            //         to_date = table.Column<DateTime>(type: "date", nullable: true)
            //     },
            //     constraints: table =>
            //     {
            //         table.PrimaryKey("PK_grade_calendar", x => x.grade_calendar_uid);
            //     });
            //
            // migrationBuilder.CreateTable(
            //     name: "grade_criteria",
            //     schema: "EDU",
            //     columns: table => new
            //     {
            //         grade_criteria_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
            //         status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         grade_criteria_code = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
            //         grade_criteria_name_th = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         grade_criteria_name_en = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         grade_criteria_description = table.Column<string>(type: "NVARCHAR2(500)", maxLength: 500, nullable: true)
            //     },
            //     constraints: table =>
            //     {
            //         table.PrimaryKey("PK_grade_criteria", x => x.grade_criteria_uid);
            //     });
            //
            // migrationBuilder.CreateTable(
            //     name: "grade_re_evaluation",
            //     schema: "EDU",
            //     columns: table => new
            //     {
            //         grade_re_evaluation_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
            //         status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         re_evaluation_type_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         education_type_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         academic_year_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         academic_semester_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         year_subject_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         study_type_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         year_subject_section_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         original_grade_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         to_grade_uid = table.Column<Guid>(type: "RAW(16)", nullable: true)
            //     },
            //     constraints: table =>
            //     {
            //         table.PrimaryKey("PK_grade_re_evaluation", x => x.grade_re_evaluation_uid);
            //     });
            //
            // migrationBuilder.CreateTable(
            //     name: "grade_reason",
            //     schema: "EDU",
            //     columns: table => new
            //     {
            //         grade_reason_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
            //         status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         grade_reason_code = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
            //         grade_reason_name_th = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         grade_reason_name_en = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         is_set_by_faculty = table.Column<bool>(type: "NUMBER(1)", nullable: true)
            //     },
            //     constraints: table =>
            //     {
            //         table.PrimaryKey("PK_grade_reason", x => x.grade_reason_uid);
            //     });
            //
            // migrationBuilder.CreateTable(
            //     name: "grade_recorder",
            //     schema: "EDU",
            //     columns: table => new
            //     {
            //         year_subject_section_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
            //         status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         recorder_uid = table.Column<Guid>(type: "RAW(16)", nullable: true)
            //     },
            //     constraints: table =>
            //     {
            //         table.PrimaryKey("PK_grade_recorder", x => x.year_subject_section_uid);
            //     });
            //
            // migrationBuilder.CreateTable(
            //     name: "group",
            //     schema: "EDU",
            //     columns: table => new
            //     {
            //         group_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
            //         status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         group_name = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         group_name_en = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         group_code = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
            //         group_image = table.Column<string>(type: "NVARCHAR2(2000)", nullable: true),
            //         group_prefix = table.Column<string>(type: "NVARCHAR2(2000)", nullable: true),
            //         group_suffix = table.Column<string>(type: "NVARCHAR2(2000)", nullable: true),
            //         instructor_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         faculty_curriculum_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         major_id = table.Column<int>(type: "NUMBER(10)", nullable: true),
            //         academic_year_uid = table.Column<Guid>(type: "RAW(16)", nullable: true)
            //     },
            //     constraints: table =>
            //     {
            //         table.PrimaryKey("PK_group", x => x.group_uid);
            //     });
            //
            // migrationBuilder.CreateTable(
            //     name: "honor_status",
            //     schema: "EDU",
            //     columns: table => new
            //     {
            //         honor_status_id = table.Column<int>(type: "NUMBER(10)", nullable: false)
            //             .Annotation("Oracle:Identity", "1, 1"),
            //         status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         honor_status_code = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
            //         honor_status_name_th = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         honor_status_name_en = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true)
            //     },
            //     constraints: table =>
            //     {
            //         table.PrimaryKey("PK_honor_status", x => x.honor_status_id);
            //     });
            //
            // migrationBuilder.CreateTable(
            //     name: "hospital",
            //     schema: "EDU",
            //     columns: table => new
            //     {
            //         hospital_id = table.Column<int>(type: "NUMBER(10)", nullable: false)
            //             .Annotation("Oracle:Identity", "1, 1"),
            //         status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         hospital_code = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
            //         hospital_name_th = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         hospital_name_en = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         province_id = table.Column<int>(type: "NUMBER(10)", nullable: true),
            //         district_id = table.Column<int>(type: "NUMBER(10)", nullable: true)
            //     },
            //     constraints: table =>
            //     {
            //         table.PrimaryKey("PK_hospital", x => x.hospital_id);
            //     });
            //
            // migrationBuilder.CreateTable(
            //     name: "income_level",
            //     schema: "EDU",
            //     columns: table => new
            //     {
            //         income_level_id = table.Column<int>(type: "NUMBER(10)", nullable: false)
            //             .Annotation("Oracle:Identity", "1, 1"),
            //         status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         income_level_code = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
            //         income_level_name_th = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         income_level_name_en = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true)
            //     },
            //     constraints: table =>
            //     {
            //         table.PrimaryKey("PK_income_level", x => x.income_level_id);
            //     });
            //
            // migrationBuilder.CreateTable(
            //     name: "loan",
            //     schema: "EDU",
            //     columns: table => new
            //     {
            //         loan_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
            //         status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         student_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         academic_year_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         contract_no = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true)
            //     },
            //     constraints: table =>
            //     {
            //         table.PrimaryKey("PK_loan", x => x.loan_uid);
            //     });
            //
            // migrationBuilder.CreateTable(
            //     name: "loan_semester",
            //     schema: "EDU",
            //     columns: table => new
            //     {
            //         loan_semester_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
            //         status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         semester_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         loan_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         loan_amount = table.Column<decimal>(type: "DECIMAL(18,4)", precision: 18, scale: 4, nullable: true),
            //         use_amount = table.Column<decimal>(type: "DECIMAL(18,4)", precision: 18, scale: 4, nullable: true)
            //     },
            //     constraints: table =>
            //     {
            //         table.PrimaryKey("PK_loan_semester", x => x.loan_semester_uid);
            //     });
            //
            // migrationBuilder.CreateTable(
            //     name: "major",
            //     schema: "EDU",
            //     columns: table => new
            //     {
            //         major_id = table.Column<int>(type: "NUMBER(10)", nullable: false)
            //             .Annotation("Oracle:Identity", "1, 1"),
            //         status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         faculty_id = table.Column<int>(type: "NUMBER(10)", nullable: true),
            //         major_code = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
            //         major_name_th = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         major_name_en = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         remark = table.Column<string>(type: "NVARCHAR2(1000)", maxLength: 1000, nullable: true),
            //         education_type_code = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true)
            //     },
            //     constraints: table =>
            //     {
            //         table.PrimaryKey("PK_major", x => x.major_id);
            //     });
            //
            // migrationBuilder.CreateTable(
            //     name: "parent_type",
            //     schema: "EDU",
            //     columns: table => new
            //     {
            //         parent_type_id = table.Column<int>(type: "NUMBER(10)", nullable: false)
            //             .Annotation("Oracle:Identity", "1, 1"),
            //         status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         parent_type_code = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
            //         parent_type_name_th = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         parent_type_name_en = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true)
            //     },
            //     constraints: table =>
            //     {
            //         table.PrimaryKey("PK_parent_type", x => x.parent_type_id);
            //     });
            //
            // migrationBuilder.CreateTable(
            //     name: "plan_register",
            //     schema: "EDU",
            //     columns: table => new
            //     {
            //         plan_register_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
            //         status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         applicant_apply_id = table.Column<int>(type: "NUMBER(10)", nullable: true),
            //         academic_year_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         semester_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         bill_no = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
            //         bill_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         register_date = table.Column<DateTime>(type: "date", nullable: true),
            //         is_package = table.Column<bool>(type: "NUMBER(1)", nullable: true),
            //         package_amount = table.Column<decimal>(type: "DECIMAL(18,4)", precision: 18, scale: 4, nullable: true),
            //         credit_amount = table.Column<decimal>(type: "DECIMAL(18,4)", precision: 18, scale: 4, nullable: true),
            //         fee_amount = table.Column<decimal>(type: "DECIMAL(18,4)", precision: 18, scale: 4, nullable: true),
            //         scholarship_amount = table.Column<decimal>(type: "DECIMAL(18,4)", precision: 18, scale: 4, nullable: true),
            //         loan_amount = table.Column<decimal>(type: "DECIMAL(18,4)", precision: 18, scale: 4, nullable: true),
            //         advanced_amount = table.Column<decimal>(type: "DECIMAL(18,4)", precision: 18, scale: 4, nullable: true),
            //         total_subject = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         total_credit = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         payment_amount = table.Column<decimal>(type: "DECIMAL(18,4)", precision: 18, scale: 4, nullable: true),
            //         register_type_code = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
            //         register_status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         is_paid = table.Column<bool>(type: "NUMBER(1)", nullable: true),
            //         paid_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         invoice_no = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
            //         invoice_due_date = table.Column<DateTime>(type: "date", nullable: true),
            //         paid_amount = table.Column<decimal>(type: "DECIMAL(18,4)", precision: 18, scale: 4, nullable: true),
            //         receipt_no = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true)
            //     },
            //     constraints: table =>
            //     {
            //         table.PrimaryKey("PK_plan_register", x => x.plan_register_uid);
            //     });
            //
            // migrationBuilder.CreateTable(
            //     name: "probation_retire_condition",
            //     schema: "EDU",
            //     columns: table => new
            //     {
            //         probation_retire_condition_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
            //         status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         education_type_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         start_date = table.Column<DateTime>(type: "date", nullable: true),
            //         end_date = table.Column<DateTime>(type: "date", nullable: true)
            //     },
            //     constraints: table =>
            //     {
            //         table.PrimaryKey("PK_probation_retire_condition", x => x.probation_retire_condition_uid);
            //     });
            //
            // migrationBuilder.CreateTable(
            //     name: "provision_register",
            //     schema: "EDU",
            //     columns: table => new
            //     {
            //         provision_register_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
            //         status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         student_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         academic_year_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         semester_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         bill_no = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
            //         bill_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         register_date = table.Column<DateTime>(type: "date", nullable: true),
            //         is_package = table.Column<bool>(type: "NUMBER(1)", nullable: true),
            //         package_amount = table.Column<decimal>(type: "DECIMAL(18,4)", precision: 18, scale: 4, nullable: true),
            //         credit_amount = table.Column<decimal>(type: "DECIMAL(18,4)", precision: 18, scale: 4, nullable: true),
            //         fee_amount = table.Column<decimal>(type: "DECIMAL(18,4)", precision: 18, scale: 4, nullable: true),
            //         scholarship_amount = table.Column<decimal>(type: "DECIMAL(18,4)", precision: 18, scale: 4, nullable: true),
            //         loan_amount = table.Column<decimal>(type: "DECIMAL(18,4)", precision: 18, scale: 4, nullable: true),
            //         advanced_amount = table.Column<decimal>(type: "DECIMAL(18,4)", precision: 18, scale: 4, nullable: true),
            //         total_subject = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         total_credit = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         payment_amount = table.Column<decimal>(type: "DECIMAL(18,4)", precision: 18, scale: 4, nullable: true),
            //         register_type_code = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
            //         register_status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         is_paid = table.Column<bool>(type: "NUMBER(1)", nullable: true),
            //         paid_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         invoice_no = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
            //         invoice_due_date = table.Column<DateTime>(type: "date", nullable: true),
            //         paid_amount = table.Column<decimal>(type: "DECIMAL(18,4)", precision: 18, scale: 4, nullable: true),
            //         receipt_no = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true)
            //     },
            //     constraints: table =>
            //     {
            //         table.PrimaryKey("PK_provision_register", x => x.provision_register_uid);
            //     });
            //
            // migrationBuilder.CreateTable(
            //     name: "question_group",
            //     schema: "EDU",
            //     columns: table => new
            //     {
            //         question_group_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
            //         status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         question_group_code = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
            //         question_group_name_th = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         question_group_name_en = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         section_type_id = table.Column<short>(type: "NUMBER(5)", nullable: true)
            //     },
            //     constraints: table =>
            //     {
            //         table.PrimaryKey("PK_question_group", x => x.question_group_uid);
            //     });
            //
            // migrationBuilder.CreateTable(
            //     name: "question_score_type",
            //     schema: "EDU",
            //     columns: table => new
            //     {
            //         question_score_type_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
            //         status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         question_score_type_code = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
            //         question_score_type_name_th = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         question_score_type_name_en = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true)
            //     },
            //     constraints: table =>
            //     {
            //         table.PrimaryKey("PK_question_score_type", x => x.question_score_type_uid);
            //     });
            //
            // migrationBuilder.CreateTable(
            //     name: "re_evaluation_type",
            //     schema: "EDU",
            //     columns: table => new
            //     {
            //         re_evaluation_type_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
            //         status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         re_evaluation_type_code = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
            //         re_evaluation_type_name_th = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         re_evaluation_type_name_en = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         row_order = table.Column<int>(type: "NUMBER(10)", nullable: true)
            //     },
            //     constraints: table =>
            //     {
            //         table.PrimaryKey("PK_re_evaluation_type", x => x.re_evaluation_type_uid);
            //     });
            //
            // migrationBuilder.CreateTable(
            //     name: "register_token",
            //     schema: "EDU",
            //     columns: table => new
            //     {
            //         register_token_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
            //         status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         student_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         education_type_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         college_faculty_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         faculty_curriculum_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         entry_academic_year_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         student_status_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         pre_academic_register_calendar_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         pre_register_academic_year_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         pre_register_academic_semester_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         late_academic_register_calendar_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         late_register_academic_year_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         late_register_academic_semester_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         add_drop_academic_register_calendar_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         add_drop_register_academic_year_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         add_drop_register_academic_semester_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         id_card_no = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
            //         register_academic_year_code = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
            //         register_semester_code = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
            //         register_semester_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         min_credit = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         max_credit = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         is_package = table.Column<bool>(type: "NUMBER(1)", nullable: true),
            //         package_amount = table.Column<decimal>(type: "DECIMAL(18,4)", precision: 18, scale: 4, nullable: true)
            //     },
            //     constraints: table =>
            //     {
            //         table.PrimaryKey("PK_register_token", x => x.register_token_uid);
            //     });
            //
            // migrationBuilder.CreateTable(
            //     name: "register_type",
            //     schema: "EDU",
            //     columns: table => new
            //     {
            //         register_type_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
            //         status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         register_type_code = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
            //         register_type_short_name_en = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         register_type_short_name_th = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         register_type_name_en = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         register_type_name_th = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         remark = table.Column<string>(type: "NVARCHAR2(1000)", maxLength: 1000, nullable: true)
            //     },
            //     constraints: table =>
            //     {
            //         table.PrimaryKey("PK_register_type", x => x.register_type_uid);
            //     });
            //
            // migrationBuilder.CreateTable(
            //     name: "required_document",
            //     schema: "EDU",
            //     columns: table => new
            //     {
            //         required_document_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
            //         status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         step_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         attached_document_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         is_multiple = table.Column<bool>(type: "NUMBER(1)", nullable: true)
            //     },
            //     constraints: table =>
            //     {
            //         table.PrimaryKey("PK_required_document", x => x.required_document_uid);
            //     });
            //
            // migrationBuilder.CreateTable(
            //     name: "school",
            //     schema: "EDU",
            //     columns: table => new
            //     {
            //         school_id = table.Column<int>(type: "NUMBER(10)", nullable: false)
            //             .Annotation("Oracle:Identity", "1, 1"),
            //         status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         school_code = table.Column<string>(type: "NVARCHAR2(10)", maxLength: 10, nullable: true),
            //         school_name_th = table.Column<string>(type: "NVARCHAR2(100)", maxLength: 100, nullable: true),
            //         school_name_en = table.Column<string>(type: "NVARCHAR2(100)", maxLength: 100, nullable: true),
            //         remark = table.Column<string>(type: "NVARCHAR2(500)", maxLength: 500, nullable: true),
            //         address1_th = table.Column<string>(type: "NVARCHAR2(500)", maxLength: 500, nullable: true),
            //         address2_th = table.Column<string>(type: "NVARCHAR2(500)", maxLength: 500, nullable: true),
            //         postal_code = table.Column<string>(type: "NVARCHAR2(30)", maxLength: 30, nullable: true),
            //         address1_en = table.Column<string>(type: "NVARCHAR2(500)", maxLength: 500, nullable: true),
            //         address2_en = table.Column<string>(type: "NVARCHAR2(500)", maxLength: 500, nullable: true),
            //         phone_no = table.Column<string>(type: "NVARCHAR2(100)", maxLength: 100, nullable: true),
            //         fax_no = table.Column<string>(type: "NVARCHAR2(100)", maxLength: 100, nullable: true),
            //         url = table.Column<string>(type: "NVARCHAR2(100)", maxLength: 100, nullable: true),
            //         country_id = table.Column<int>(type: "NUMBER(10)", nullable: true),
            //         contact = table.Column<string>(type: "NVARCHAR2(100)", maxLength: 100, nullable: true),
            //         province_id = table.Column<int>(type: "NUMBER(10)", nullable: true),
            //         district_id = table.Column<int>(type: "NUMBER(10)", nullable: true),
            //         sub_district_id = table.Column<int>(type: "NUMBER(10)", nullable: true),
            //         school_type = table.Column<string>(type: "NVARCHAR2(1)", maxLength: 1, nullable: true),
            //         school_flag = table.Column<string>(type: "NVARCHAR2(1)", maxLength: 1, nullable: true),
            //         email = table.Column<string>(type: "NVARCHAR2(100)", maxLength: 100, nullable: true),
            //         school_level_id = table.Column<short>(type: "NUMBER(5)", nullable: true)
            //     },
            //     constraints: table =>
            //     {
            //         table.PrimaryKey("PK_school", x => x.school_id);
            //     });
            //
            // migrationBuilder.CreateTable(
            //     name: "school_level",
            //     schema: "EDU",
            //     columns: table => new
            //     {
            //         school_level_id = table.Column<int>(type: "NUMBER(10)", nullable: false)
            //             .Annotation("Oracle:Identity", "1, 1"),
            //         status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         school_level_code = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
            //         school_level_name_th = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         school_level_name_en = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true)
            //     },
            //     constraints: table =>
            //     {
            //         table.PrimaryKey("PK_school_level", x => x.school_level_id);
            //     });
            //
            // migrationBuilder.CreateTable(
            //     name: "score_calculation_type",
            //     schema: "EDU",
            //     columns: table => new
            //     {
            //         score_calculation_type_id = table.Column<int>(type: "NUMBER(10)", nullable: false)
            //             .Annotation("Oracle:Identity", "1, 1"),
            //         status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         score_calculation_type_code = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
            //         score_calculation_type_name_th = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         score_calculation_type_name_en = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true)
            //     },
            //     constraints: table =>
            //     {
            //         table.PrimaryKey("PK_score_calculation_type", x => x.score_calculation_type_id);
            //     });
            //
            // migrationBuilder.CreateTable(
            //     name: "section_type",
            //     schema: "EDU",
            //     columns: table => new
            //     {
            //         section_type_id = table.Column<short>(type: "NUMBER(5)", nullable: false)
            //             .Annotation("Oracle:Identity", "1, 1"),
            //         status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         section_type_code = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
            //         section_type_name_th = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         section_type_name_en = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true)
            //     },
            //     constraints: table =>
            //     {
            //         table.PrimaryKey("PK_section_type", x => x.section_type_id);
            //     });
            //
            // migrationBuilder.CreateTable(
            //     name: "semester_type",
            //     schema: "EDU",
            //     columns: table => new
            //     {
            //         semester_type_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
            //         status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         semester_type_code = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
            //         semester_type_name_th = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         semester_type_name_en = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true)
            //     },
            //     constraints: table =>
            //     {
            //         table.PrimaryKey("PK_semester_type", x => x.semester_type_uid);
            //     });
            //
            // migrationBuilder.CreateTable(
            //     name: "status",
            //     schema: "EDU",
            //     columns: table => new
            //     {
            //         status_id = table.Column<short>(type: "NUMBER(5)", nullable: false)
            //             .Annotation("Oracle:Identity", "1, 1"),
            //         status_code = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
            //         status_name_th = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         status_name_en = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true)
            //     },
            //     constraints: table =>
            //     {
            //         table.PrimaryKey("PK_status", x => x.status_id);
            //     });
            //
            // migrationBuilder.CreateTable(
            //     name: "step",
            //     schema: "EDU",
            //     columns: table => new
            //     {
            //         step_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
            //         status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         step_code = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
            //         step_name_th = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         step_name_en = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true)
            //     },
            //     constraints: table =>
            //     {
            //         table.PrimaryKey("PK_step", x => x.step_uid);
            //     });
            //
            // migrationBuilder.CreateTable(
            //     name: "student",
            //     schema: "EDU",
            //     columns: table => new
            //     {
            //         student_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
            //         status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         student_code = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
            //         prefix_id = table.Column<int>(type: "NUMBER(10)", nullable: true),
            //         first_name_th = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         last_name_th = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         middle_name_th = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         first_name_en = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         last_name_en = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         middle_name_en = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         display_name_th = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         display_name_en = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         gender_id = table.Column<int>(type: "NUMBER(10)", nullable: true),
            //         marital_status_id = table.Column<int>(type: "NUMBER(10)", nullable: true),
            //         military_status_id = table.Column<int>(type: "NUMBER(10)", nullable: true),
            //         religion_id = table.Column<int>(type: "NUMBER(10)", nullable: true),
            //         nationality_id = table.Column<int>(type: "NUMBER(10)", nullable: true),
            //         blood_group_id = table.Column<int>(type: "NUMBER(10)", nullable: true),
            //         family_status_id = table.Column<int>(type: "NUMBER(10)", nullable: true),
            //         relatives_child = table.Column<int>(type: "NUMBER(10)", nullable: true),
            //         relatives_child_study = table.Column<int>(type: "NUMBER(10)", nullable: true),
            //         support_by_id = table.Column<int>(type: "NUMBER(10)", nullable: true),
            //         support_per_month_id = table.Column<int>(type: "NUMBER(10)", nullable: true),
            //         student_type_id = table.Column<int>(type: "NUMBER(10)", nullable: true),
            //         is_dorm_student = table.Column<bool>(type: "NUMBER(1)", nullable: true),
            //         citizen_id = table.Column<string>(type: "NVARCHAR2(2000)", nullable: true),
            //         citizen_id_card_issue_date = table.Column<DateTime>(type: "date", nullable: true),
            //         citizen_id_card_expire_date = table.Column<DateTime>(type: "date", nullable: true),
            //         passport_no = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
            //         passport_issue_date = table.Column<DateTime>(type: "date", nullable: true),
            //         passport_expire_date = table.Column<DateTime>(type: "date", nullable: true),
            //         passport_country_id = table.Column<int>(type: "NUMBER(10)", nullable: true),
            //         uid_no = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
            //         tax_id = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
            //         phone_no = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
            //         mobile_no = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
            //         fax_no = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
            //         personal_email = table.Column<string>(type: "NVARCHAR2(2000)", nullable: true),
            //         rsu_email = table.Column<string>(type: "NVARCHAR2(2000)", nullable: true),
            //         extraction = table.Column<string>(type: "NVARCHAR2(2000)", nullable: true),
            //         weight = table.Column<decimal>(type: "decimal(5,2)", nullable: true),
            //         height = table.Column<decimal>(type: "decimal(5,2)", nullable: true),
            //         birth_date = table.Column<DateTime>(type: "date", nullable: true),
            //         born_province_id = table.Column<int>(type: "NUMBER(10)", nullable: true),
            //         entry_academic_year_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         entry_semester_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         education_type_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         college_faculty_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         faculty_curriculum_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         student_status_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         major_id = table.Column<int>(type: "NUMBER(10)", nullable: true),
            //         is_rsu_student = table.Column<bool>(type: "NUMBER(1)", nullable: true),
            //         student_image_url = table.Column<string>(type: "NVARCHAR2(500)", maxLength: 500, nullable: true),
            //         entry_type_id = table.Column<int>(type: "NUMBER(10)", nullable: true),
            //         entry_program_id = table.Column<int>(type: "NUMBER(10)", nullable: true),
            //         application_id = table.Column<long>(type: "NUMBER(19)", nullable: true),
            //         entry_education_level_id = table.Column<int>(type: "NUMBER(10)", nullable: true),
            //         entry_program_type_id = table.Column<int>(type: "NUMBER(10)", nullable: true),
            //         school_id = table.Column<int>(type: "NUMBER(10)", nullable: true),
            //         entry_graduate_certificate_no = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
            //         entry_graduate_date = table.Column<DateTime>(type: "date", nullable: true),
            //         approve_certificate = table.Column<string>(type: "NVARCHAR2(1)", maxLength: 1, nullable: true),
            //         is_education_checked = table.Column<bool>(type: "NUMBER(1)", nullable: true),
            //         entry_graduate_year = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
            //         entry_gpa = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
            //         honor_status_id = table.Column<int>(type: "NUMBER(10)", nullable: true),
            //         graduate_academic_year_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         graduate_semester_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         graduate_date = table.Column<DateTime>(type: "date", nullable: true),
            //         graduate_document_no = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
            //         graduate_country_id = table.Column<int>(type: "NUMBER(10)", nullable: true),
            //         graduate_province_id = table.Column<int>(type: "NUMBER(10)", nullable: true),
            //         cumulative_gpa = table.Column<decimal>(type: "decimal(5,2)", nullable: true),
            //         total_credit = table.Column<decimal>(type: "decimal(5,1)", nullable: true),
            //         advisor_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         is_deformed = table.Column<bool>(type: "NUMBER(1)", nullable: true),
            //         deformed_no = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
            //         deformed_issue_date = table.Column<DateTime>(type: "date", nullable: true),
            //         deformed_expire_date = table.Column<DateTime>(type: "date", nullable: true),
            //         deformed_detail = table.Column<string>(type: "NVARCHAR2(2000)", nullable: true),
            //         visa_no = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
            //         visa_issue_date = table.Column<DateTime>(type: "date", nullable: true),
            //         visa_expire_date = table.Column<DateTime>(type: "date", nullable: true),
            //         old_student_code = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
            //         remark = table.Column<string>(type: "NVARCHAR2(1000)", maxLength: 1000, nullable: true),
            //         line_oa_id = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true)
            //     },
            //     constraints: table =>
            //     {
            //         table.PrimaryKey("PK_student", x => x.student_uid);
            //     });
            //
            // migrationBuilder.CreateTable(
            //     name: "student_card_record",
            //     schema: "EDU",
            //     columns: table => new
            //     {
            //         student_card_record_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
            //         status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         student_code = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
            //         digits_code = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
            //         times = table.Column<int>(type: "NUMBER(10)", nullable: true),
            //         student_card_issue_date = table.Column<DateTime>(type: "date", nullable: true),
            //         student_card_expire_date = table.Column<DateTime>(type: "date", nullable: true),
            //         is_long_name = table.Column<bool>(type: "NUMBER(1)", nullable: true),
            //         is_english = table.Column<bool>(type: "NUMBER(1)", nullable: true),
            //         is_display = table.Column<bool>(type: "NUMBER(1)", nullable: true),
            //         student_image_url = table.Column<string>(type: "NVARCHAR2(500)", maxLength: 500, nullable: true)
            //     },
            //     constraints: table =>
            //     {
            //         table.PrimaryKey("PK_student_card_record", x => x.student_card_record_uid);
            //     });
            //
            // migrationBuilder.CreateTable(
            //     name: "student_discipline",
            //     schema: "EDU",
            //     columns: table => new
            //     {
            //         student_discipline_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
            //         status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         student_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         student_code = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
            //         deduct_score = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         remain_score = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         discipline_score_status_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         action_by = table.Column<string>(type: "NVARCHAR2(2000)", nullable: true)
            //     },
            //     constraints: table =>
            //     {
            //         table.PrimaryKey("PK_student_discipline", x => x.student_discipline_uid);
            //     });
            //
            // migrationBuilder.CreateTable(
            //     name: "student_education_check",
            //     schema: "EDU",
            //     columns: table => new
            //     {
            //         student_education_check_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
            //         status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         student_code = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
            //         academic_year_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         academic_semester_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         major_id = table.Column<int>(type: "NUMBER(10)", nullable: true),
            //         school_id = table.Column<int>(type: "NUMBER(10)", nullable: true),
            //         education_level_id = table.Column<int>(type: "NUMBER(10)", nullable: true),
            //         program_type_id = table.Column<int>(type: "NUMBER(10)", nullable: true),
            //         graduate_year = table.Column<string>(type: "NVARCHAR2(4)", maxLength: 4, nullable: true),
            //         gpa = table.Column<string>(type: "NVARCHAR2(10)", maxLength: 10, nullable: true),
            //         student_education_check_status_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         is_correct = table.Column<bool>(type: "NUMBER(1)", nullable: true),
            //         education_check_document_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         update_program = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
            //         remark = table.Column<string>(type: "NVARCHAR2(500)", maxLength: 500, nullable: true),
            //         graduate_document_no = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         cause_failed_id = table.Column<int>(type: "NUMBER(10)", nullable: true)
            //     },
            //     constraints: table =>
            //     {
            //         table.PrimaryKey("PK_student_education_check", x => x.student_education_check_uid);
            //     });
            //
            // migrationBuilder.CreateTable(
            //     name: "student_education_check_status",
            //     schema: "EDU",
            //     columns: table => new
            //     {
            //         student_education_check_status_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
            //         status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         student_education_check_status_code = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
            //         student_education_check_status_name_th = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         student_education_check_status_name_en = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         student_education_check_status_short_name_th = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         student_education_check_status_short_name_en = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         is_checked = table.Column<bool>(type: "NUMBER(1)", nullable: true)
            //     },
            //     constraints: table =>
            //     {
            //         table.PrimaryKey("PK_student_education_check_status", x => x.student_education_check_status_uid);
            //     });
            //
            // migrationBuilder.CreateTable(
            //     name: "student_evaluation",
            //     schema: "EDU",
            //     columns: table => new
            //     {
            //         student_evaluation_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
            //         status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         student_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         academic_year_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         academic_semester_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         year_subject_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         year_subject_code = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
            //         year_subject_name_th = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         year_subject_name_en = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         year_subject_section_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         student_evaluation_status_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         college_faculty_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         evaluation_date = table.Column<DateTime>(type: "date", nullable: true),
            //         section_code = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
            //         instructor_code = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true)
            //     },
            //     constraints: table =>
            //     {
            //         table.PrimaryKey("PK_student_evaluation", x => x.student_evaluation_uid);
            //     });
            //
            // migrationBuilder.CreateTable(
            //     name: "student_evaluation_status",
            //     schema: "EDU",
            //     columns: table => new
            //     {
            //         student_evaluation_status_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
            //         status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         student_evaluation_status_code = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
            //         student_evaluation_status_name_th = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         student_evaluation_status_name_en = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true)
            //     },
            //     constraints: table =>
            //     {
            //         table.PrimaryKey("PK_student_evaluation_status", x => x.student_evaluation_status_uid);
            //     });
            //
            // migrationBuilder.CreateTable(
            //     name: "student_illness",
            //     schema: "EDU",
            //     columns: table => new
            //     {
            //         student_illness_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
            //         status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         student_code = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
            //         mobile_no = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
            //         illness_details = table.Column<string>(type: "NVARCHAR2(2000)", nullable: true),
            //         illness_date = table.Column<DateTime>(type: "date", nullable: true),
            //         hospital_id = table.Column<int>(type: "NUMBER(10)", nullable: true),
            //         teacher_duty_name = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         recorder_name = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true)
            //     },
            //     constraints: table =>
            //     {
            //         table.PrimaryKey("PK_student_illness", x => x.student_illness_uid);
            //     });
            //
            // migrationBuilder.CreateTable(
            //     name: "student_log",
            //     schema: "EDU",
            //     columns: table => new
            //     {
            //         student_log_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
            //         student_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         class_name = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         property_name = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         key = table.Column<Guid>(type: "RAW(16)", nullable: false),
            //         old_value = table.Column<string>(type: "NVARCHAR2(2000)", nullable: true),
            //         new_value = table.Column<string>(type: "NVARCHAR2(2000)", nullable: true),
            //         date_changed = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: false),
            //         changed_by = table.Column<string>(type: "NVARCHAR2(2000)", nullable: true)
            //     },
            //     constraints: table =>
            //     {
            //         table.PrimaryKey("PK_student_log", x => x.student_log_uid);
            //     });
            //
            // migrationBuilder.CreateTable(
            //     name: "student_restriction",
            //     schema: "EDU",
            //     columns: table => new
            //     {
            //         student_restriction_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
            //         status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         student_restriction_code = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
            //         student_restriction_name_th = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         student_restriction_name_en = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true)
            //     },
            //     constraints: table =>
            //     {
            //         table.PrimaryKey("PK_student_restriction", x => x.student_restriction_uid);
            //     });
            //
            // migrationBuilder.CreateTable(
            //     name: "student_status",
            //     schema: "EDU",
            //     columns: table => new
            //     {
            //         student_status_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
            //         status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         student_status_code = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
            //         student_status_name_th = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         student_status_name_en = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true)
            //     },
            //     constraints: table =>
            //     {
            //         table.PrimaryKey("PK_student_status", x => x.student_status_uid);
            //     });
            //
            // migrationBuilder.CreateTable(
            //     name: "student_type",
            //     schema: "EDU",
            //     columns: table => new
            //     {
            //         student_type_id = table.Column<int>(type: "NUMBER(10)", nullable: false)
            //             .Annotation("Oracle:Identity", "1, 1"),
            //         status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         student_type_code = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
            //         student_type_name_th = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         student_type_name_en = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true)
            //     },
            //     constraints: table =>
            //     {
            //         table.PrimaryKey("PK_student_type", x => x.student_type_id);
            //     });
            //
            // migrationBuilder.CreateTable(
            //     name: "study_day",
            //     schema: "EDU",
            //     columns: table => new
            //     {
            //         study_day_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
            //         status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         day_code = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
            //         day_short_name_th = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         day_short_name_en = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         day_name_th = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         day_name_en = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         row_order = table.Column<short>(type: "NUMBER(5)", nullable: true)
            //     },
            //     constraints: table =>
            //     {
            //         table.PrimaryKey("PK_study_day", x => x.study_day_uid);
            //     });
            //
            // migrationBuilder.CreateTable(
            //     name: "study_time",
            //     schema: "EDU",
            //     columns: table => new
            //     {
            //         study_time_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
            //         status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         study_time_code = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
            //         start_time = table.Column<TimeSpan>(type: "INTERVAL DAY(8) TO SECOND(7)", nullable: true),
            //         end_time = table.Column<TimeSpan>(type: "INTERVAL DAY(8) TO SECOND(7)", nullable: true),
            //         start_time_text = table.Column<string>(type: "NVARCHAR2(2000)", nullable: true),
            //         end_time_text = table.Column<string>(type: "NVARCHAR2(2000)", nullable: true),
            //         is_exam = table.Column<bool>(type: "NUMBER(1)", nullable: true)
            //     },
            //     constraints: table =>
            //     {
            //         table.PrimaryKey("PK_study_time", x => x.study_time_uid);
            //     });
            //
            // migrationBuilder.CreateTable(
            //     name: "study_type",
            //     schema: "EDU",
            //     columns: table => new
            //     {
            //         study_type_id = table.Column<int>(type: "NUMBER(10)", nullable: false)
            //             .Annotation("Oracle:Identity", "1, 1"),
            //         status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         study_type_code = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
            //         study_type_name_th = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         study_type_name_en = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true)
            //     },
            //     constraints: table =>
            //     {
            //         table.PrimaryKey("PK_study_type", x => x.study_type_id);
            //     });
            //
            // migrationBuilder.CreateTable(
            //     name: "subject",
            //     schema: "EDU",
            //     columns: table => new
            //     {
            //         subject_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
            //         status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         subject_code = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
            //         subject_seq_code = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
            //         subject_name_th = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         subject_name_en = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         credit_text = table.Column<string>(type: "NVARCHAR2(2000)", nullable: true),
            //         remark = table.Column<string>(type: "NVARCHAR2(1000)", maxLength: 1000, nullable: true),
            //         lecture_hour = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         lab_hour = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         practice_hour = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         min_grade = table.Column<string>(type: "NVARCHAR2(2000)", nullable: true),
            //         credit = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         is_gpa_include = table.Column<bool>(type: "NUMBER(1)", nullable: true),
            //         has_prerequisite = table.Column<bool>(type: "NUMBER(1)", nullable: true),
            //         has_co_subject = table.Column<bool>(type: "NUMBER(1)", nullable: true),
            //         allow_scholarship = table.Column<bool>(type: "NUMBER(1)", nullable: true),
            //         subject_type_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         study_type_id = table.Column<short>(type: "NUMBER(5)", nullable: true)
            //     },
            //     constraints: table =>
            //     {
            //         table.PrimaryKey("PK_subject", x => x.subject_uid);
            //     });
            //
            // migrationBuilder.CreateTable(
            //     name: "subject_master",
            //     schema: "EDU",
            //     columns: table => new
            //     {
            //         subject_master_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
            //         subject_code = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
            //         subject_name_th = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         subject_name_en = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true)
            //     },
            //     constraints: table =>
            //     {
            //         table.PrimaryKey("PK_subject_master", x => x.subject_master_uid);
            //     });
            //
            // migrationBuilder.CreateTable(
            //     name: "support_by",
            //     schema: "EDU",
            //     columns: table => new
            //     {
            //         support_by_id = table.Column<int>(type: "NUMBER(10)", nullable: false)
            //             .Annotation("Oracle:Identity", "1, 1"),
            //         status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         support_by_code = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
            //         support_by_name_th = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         support_by_name_en = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true)
            //     },
            //     constraints: table =>
            //     {
            //         table.PrimaryKey("PK_support_by", x => x.support_by_id);
            //     });
            //
            // migrationBuilder.CreateTable(
            //     name: "support_per_month",
            //     schema: "EDU",
            //     columns: table => new
            //     {
            //         support_per_month_id = table.Column<int>(type: "NUMBER(10)", nullable: false)
            //             .Annotation("Oracle:Identity", "1, 1"),
            //         status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         support_per_month_code = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
            //         support_per_month_name_th = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         support_per_month_name_en = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true)
            //     },
            //     constraints: table =>
            //     {
            //         table.PrimaryKey("PK_support_per_month", x => x.support_per_month_id);
            //     });
            //
            // migrationBuilder.CreateTable(
            //     name: "year_subject",
            //     schema: "EDU",
            //     columns: table => new
            //     {
            //         year_subject_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
            //         status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         subject_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         academic_year_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         academic_semester_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         education_type_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         subject_seq_code = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
            //         academic_year_code = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
            //         education_type_code = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
            //         year_subject_code = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
            //         year_subject_name_th = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         year_subject_name_en = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         credit_text = table.Column<string>(type: "NVARCHAR2(2000)", nullable: true),
            //         remark = table.Column<string>(type: "NVARCHAR2(1000)", maxLength: 1000, nullable: true),
            //         lecture_hour = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         lab_hour = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         practice_hour = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         min_grade = table.Column<string>(type: "NVARCHAR2(2000)", nullable: true),
            //         credit = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         is_gpa_include = table.Column<bool>(type: "NUMBER(1)", nullable: true),
            //         has_prerequisite = table.Column<bool>(type: "NUMBER(1)", nullable: true),
            //         has_co_subject = table.Column<bool>(type: "NUMBER(1)", nullable: true),
            //         allow_scholarship = table.Column<bool>(type: "NUMBER(1)", nullable: true),
            //         subject_type_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         study_type_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         owner_education_type_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         owner_college_faculty_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         owner_faculty_curriculum_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         lecture_amount = table.Column<decimal>(type: "DECIMAL(18,4)", precision: 18, scale: 4, nullable: true),
            //         lab_amount = table.Column<decimal>(type: "DECIMAL(18,4)", precision: 18, scale: 4, nullable: true),
            //         practice_amount = table.Column<decimal>(type: "DECIMAL(18,4)", precision: 18, scale: 4, nullable: true),
            //         created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true)
            //     },
            //     constraints: table =>
            //     {
            //         table.PrimaryKey("PK_year_subject", x => x.year_subject_uid);
            //     });
            //
            // migrationBuilder.CreateTable(
            //     name: "year_subject_grade_criteria_option",
            //     schema: "EDU",
            //     columns: table => new
            //     {
            //         year_grade_criteria_option_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
            //         status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         year_subject_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         min_score = table.Column<decimal>(type: "decimal(18,2)", nullable: true),
            //         max_score = table.Column<decimal>(type: "decimal(18,2)", nullable: true),
            //         grade_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         description = table.Column<string>(type: "NVARCHAR2(500)", maxLength: 500, nullable: true)
            //     },
            //     constraints: table =>
            //     {
            //         table.PrimaryKey("PK_year_subject_grade_criteria_option", x => x.year_grade_criteria_option_uid);
            //     });
            //
            // migrationBuilder.CreateTable(
            //     name: "year_subject_waiting_list",
            //     schema: "EDU",
            //     columns: table => new
            //     {
            //         subject_waiting_list_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
            //         status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         academic_year_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         academic_semester_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         student_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         provision_register_subject_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         year_subject_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         lecture_year_subject_section_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         lab_year_subject_section_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         topic_uid = table.Column<Guid>(type: "RAW(16)", nullable: true)
            //     },
            //     constraints: table =>
            //     {
            //         table.PrimaryKey("PK_year_subject_waiting_list", x => x.subject_waiting_list_uid);
            //     });
            //
            // migrationBuilder.CreateTable(
            //     name: "academic_year_filter",
            //     schema: "EDU",
            //     columns: table => new
            //     {
            //         academic_year_filter_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
            //         status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         academic_year_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         academic_year_filter_code = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
            //         academic_year_filter_name_th = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         academic_year_filter_name_en = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         start_date = table.Column<DateTime>(type: "date", nullable: true),
            //         end_date = table.Column<DateTime>(type: "date", nullable: true),
            //         remark = table.Column<string>(type: "NVARCHAR2(1000)", maxLength: 1000, nullable: true),
            //         semester_type_uid = table.Column<Guid>(type: "RAW(16)", nullable: true)
            //     },
            //     constraints: table =>
            //     {
            //         table.PrimaryKey("PK_academic_year_filter", x => x.academic_year_filter_uid);
            //         table.ForeignKey(
            //             name: "FK_academic_year_filter_academic_year_academic_year_uid",
            //             column: x => x.academic_year_uid,
            //             principalSchema: "EDU",
            //             principalTable: "academic_year",
            //             principalColumn: "academic_year_uid",
            //             onDelete: ReferentialAction.Cascade);
            //     });
            //
            // migrationBuilder.CreateTable(
            //     name: "curriculum_subject_category",
            //     schema: "EDU",
            //     columns: table => new
            //     {
            //         curriculum_subject_category_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
            //         status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         curriculum_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         curriculum_subject_category_code = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
            //         curriculum_subject_category_name_th = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         curriculum_subject_category_name_en = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         total_credit = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         total_subject = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         min_credit = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         max_credit = table.Column<short>(type: "NUMBER(5)", nullable: true)
            //     },
            //     constraints: table =>
            //     {
            //         table.PrimaryKey("PK_curriculum_subject_category", x => x.curriculum_subject_category_uid);
            //         table.ForeignKey(
            //             name: "FK_curriculum_subject_category_curriculum_curriculum_uid",
            //             column: x => x.curriculum_uid,
            //             principalSchema: "EDU",
            //             principalTable: "curriculum",
            //             principalColumn: "curriculum_uid",
            //             onDelete: ReferentialAction.Cascade);
            //     });
            //
            // migrationBuilder.CreateTable(
            //     name: "discipline_condition",
            //     schema: "EDU",
            //     columns: table => new
            //     {
            //         discipline_condition_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
            //         status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         section_code = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
            //         section_name_th = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         section_name_en = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         row_order = table.Column<int>(type: "NUMBER(10)", nullable: true),
            //         discipline_year_uid = table.Column<Guid>(type: "RAW(16)", nullable: true)
            //     },
            //     constraints: table =>
            //     {
            //         table.PrimaryKey("PK_discipline_condition", x => x.discipline_condition_uid);
            //         table.ForeignKey(
            //             name: "FK_discipline_condition_discipline_year_discipline_year_uid",
            //             column: x => x.discipline_year_uid,
            //             principalSchema: "EDU",
            //             principalTable: "discipline_year",
            //             principalColumn: "discipline_year_uid",
            //             onDelete: ReferentialAction.Cascade);
            //     });
            //
            // migrationBuilder.CreateTable(
            //     name: "discipline_score_status",
            //     schema: "EDU",
            //     columns: table => new
            //     {
            //         discipline_score_status_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
            //         status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         min_score = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         max_score = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         discipline_status_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         amount = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         discipline_period_type_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         discipline_year_uid = table.Column<Guid>(type: "RAW(16)", nullable: true)
            //     },
            //     constraints: table =>
            //     {
            //         table.PrimaryKey("PK_discipline_score_status", x => x.discipline_score_status_uid);
            //         table.ForeignKey(
            //             name: "FK_discipline_score_status_discipline_period_type_discipline_period_type_uid",
            //             column: x => x.discipline_period_type_uid,
            //             principalSchema: "EDU",
            //             principalTable: "discipline_period_type",
            //             principalColumn: "discipline_period_type_uid",
            //             onDelete: ReferentialAction.Restrict);
            //         table.ForeignKey(
            //             name: "FK_discipline_score_status_discipline_year_discipline_year_uid",
            //             column: x => x.discipline_year_uid,
            //             principalSchema: "EDU",
            //             principalTable: "discipline_year",
            //             principalColumn: "discipline_year_uid",
            //             onDelete: ReferentialAction.Cascade);
            //     });
            //
            // migrationBuilder.CreateTable(
            //     name: "dynamic_fee_education_type",
            //     schema: "EDU",
            //     columns: table => new
            //     {
            //         dynamic_fee_education_type_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
            //         status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         dynamic_fee_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         fee_amount = table.Column<decimal>(type: "DECIMAL(18,4)", precision: 18, scale: 4, nullable: false),
            //         dynamic_fee_apply_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         education_type_uid = table.Column<Guid>(type: "RAW(16)", nullable: true)
            //     },
            //     constraints: table =>
            //     {
            //         table.PrimaryKey("PK_dynamic_fee_education_type", x => x.dynamic_fee_education_type_uid);
            //         table.ForeignKey(
            //             name: "FK_dynamic_fee_education_type_dynamic_fee_apply_dynamic_fee_apply_uid",
            //             column: x => x.dynamic_fee_apply_uid,
            //             principalSchema: "EDU",
            //             principalTable: "dynamic_fee_apply",
            //             principalColumn: "dynamic_fee_apply_uid",
            //             onDelete: ReferentialAction.Restrict);
            //         table.ForeignKey(
            //             name: "FK_dynamic_fee_education_type_dynamic_fee_dynamic_fee_uid",
            //             column: x => x.dynamic_fee_uid,
            //             principalSchema: "EDU",
            //             principalTable: "dynamic_fee",
            //             principalColumn: "dynamic_fee_uid",
            //             onDelete: ReferentialAction.Restrict);
            //     });
            //
            // migrationBuilder.CreateTable(
            //     name: "education_plan_curriculum",
            //     schema: "EDU",
            //     columns: table => new
            //     {
            //         education_plan_curriculum_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
            //         education_plan_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         curriculum_uid = table.Column<Guid>(type: "RAW(16)", nullable: true)
            //     },
            //     constraints: table =>
            //     {
            //         table.PrimaryKey("PK_education_plan_curriculum", x => x.education_plan_curriculum_uid);
            //         table.ForeignKey(
            //             name: "FK_education_plan_curriculum_education_plan_education_plan_uid",
            //             column: x => x.education_plan_uid,
            //             principalSchema: "EDU",
            //             principalTable: "education_plan",
            //             principalColumn: "education_plan_uid",
            //             onDelete: ReferentialAction.Cascade);
            //     });
            //
            // migrationBuilder.CreateTable(
            //     name: "education_plan_fee",
            //     schema: "EDU",
            //     columns: table => new
            //     {
            //         education_plan_fee_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
            //         education_plan_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         register_fee_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         fee_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         fee_amount = table.Column<decimal>(type: "DECIMAL(18,4)", precision: 18, scale: 4, nullable: true)
            //     },
            //     constraints: table =>
            //     {
            //         table.PrimaryKey("PK_education_plan_fee", x => x.education_plan_fee_uid);
            //         table.ForeignKey(
            //             name: "FK_education_plan_fee_education_plan_education_plan_uid",
            //             column: x => x.education_plan_uid,
            //             principalSchema: "EDU",
            //             principalTable: "education_plan",
            //             principalColumn: "education_plan_uid",
            //             onDelete: ReferentialAction.Cascade);
            //     });
            //
            // migrationBuilder.CreateTable(
            //     name: "education_plan_subject",
            //     schema: "EDU",
            //     columns: table => new
            //     {
            //         education_plan_subject_id = table.Column<Guid>(type: "RAW(16)", nullable: false),
            //         status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         education_plan_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         subject_year_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         lecture_study_subject_section_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         lab_study_subject_section_uid = table.Column<Guid>(type: "RAW(16)", nullable: true)
            //     },
            //     constraints: table =>
            //     {
            //         table.PrimaryKey("PK_education_plan_subject", x => x.education_plan_subject_id);
            //         table.ForeignKey(
            //             name: "FK_education_plan_subject_education_plan_education_plan_uid",
            //             column: x => x.education_plan_uid,
            //             principalSchema: "EDU",
            //             principalTable: "education_plan",
            //             principalColumn: "education_plan_uid",
            //             onDelete: ReferentialAction.Cascade);
            //     });
            //
            // migrationBuilder.CreateTable(
            //     name: "evaluate_subject_section_instructor",
            //     schema: "EDU",
            //     columns: table => new
            //     {
            //         evaluate_subject_section_instructor_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
            //         status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         evaluate_subject_section_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         instructor_code = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true)
            //     },
            //     constraints: table =>
            //     {
            //         table.PrimaryKey("PK_evaluate_subject_section_instructor", x => x.evaluate_subject_section_instructor_uid);
            //         table.ForeignKey(
            //             name: "FK_evaluate_subject_section_instructor_evaluate_subject_section_evaluate_subject_section_uid",
            //             column: x => x.evaluate_subject_section_uid,
            //             principalSchema: "EDU",
            //             principalTable: "evaluate_subject_section",
            //             principalColumn: "evaluate_subject_section_uid",
            //             onDelete: ReferentialAction.Cascade);
            //     });
            //
            // migrationBuilder.CreateTable(
            //     name: "evaluation_calendar_faculty",
            //     schema: "EDU",
            //     columns: table => new
            //     {
            //         evaluation_calendar_faculty_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
            //         status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         evaluation_calendar_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         college_faculty_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         is_selected = table.Column<bool>(type: "NUMBER(1)", nullable: true)
            //     },
            //     constraints: table =>
            //     {
            //         table.PrimaryKey("PK_evaluation_calendar_faculty", x => x.evaluation_calendar_faculty_uid);
            //         table.ForeignKey(
            //             name: "FK_evaluation_calendar_faculty_evaluation_calendar_evaluation_calendar_uid",
            //             column: x => x.evaluation_calendar_uid,
            //             principalSchema: "EDU",
            //             principalTable: "evaluation_calendar",
            //             principalColumn: "evaluation_calendar_uid",
            //             onDelete: ReferentialAction.Cascade);
            //     });
            //
            // migrationBuilder.CreateTable(
            //     name: "evaluation_calendar_schedule",
            //     schema: "EDU",
            //     columns: table => new
            //     {
            //         evaluation_calendar_schedule_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
            //         status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         evaluation_calendar_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         evaluation_calendar_item_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         start_date = table.Column<DateTime>(type: "date", nullable: true),
            //         end_date = table.Column<DateTime>(type: "date", nullable: true)
            //     },
            //     constraints: table =>
            //     {
            //         table.PrimaryKey("PK_evaluation_calendar_schedule", x => x.evaluation_calendar_schedule_uid);
            //         table.ForeignKey(
            //             name: "FK_evaluation_calendar_schedule_evaluation_calendar_evaluation_calendar_uid",
            //             column: x => x.evaluation_calendar_uid,
            //             principalSchema: "EDU",
            //             principalTable: "evaluation_calendar",
            //             principalColumn: "evaluation_calendar_uid",
            //             onDelete: ReferentialAction.Cascade);
            //         table.ForeignKey(
            //             name: "FK_evaluation_calendar_schedule_evaluation_calendar_item_evaluation_calendar_item_uid",
            //             column: x => x.evaluation_calendar_item_uid,
            //             principalSchema: "EDU",
            //             principalTable: "evaluation_calendar_item",
            //             principalColumn: "evaluation_calendar_item_uid",
            //             onDelete: ReferentialAction.Restrict);
            //     });
            //
            // migrationBuilder.CreateTable(
            //     name: "education_type_fee",
            //     schema: "EDU",
            //     columns: table => new
            //     {
            //         education_type_fee_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
            //         status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         fee_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         education_type_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         fee_amount = table.Column<decimal>(type: "DECIMAL(18,4)", precision: 18, scale: 4, nullable: true)
            //     },
            //     constraints: table =>
            //     {
            //         table.PrimaryKey("PK_education_type_fee", x => x.education_type_fee_uid);
            //         table.ForeignKey(
            //             name: "FK_education_type_fee_fee_fee_uid",
            //             column: x => x.fee_uid,
            //             principalSchema: "EDU",
            //             principalTable: "fee",
            //             principalColumn: "fee_uid",
            //             onDelete: ReferentialAction.Restrict);
            //     });
            //
            // migrationBuilder.CreateTable(
            //     name: "final_register_subject",
            //     schema: "EDU",
            //     columns: table => new
            //     {
            //         final_register_subject_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
            //         status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         final_register_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         year_subject_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         year_subject_section_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         year_subject_code = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
            //         year_subject_name_th = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         year_subject_name_en = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         credit = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         lecture_year_subject_section_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         lecture_section_code = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
            //         lecture_study_time = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
            //         lecture_study_time_text = table.Column<string>(type: "NVARCHAR2(100)", maxLength: 100, nullable: true),
            //         topic_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         topic_code = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
            //         topic_name_th = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         topic_name_en = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         topic_credit = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         lab_year_subject_section_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         lab_section_code = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
            //         lab_study_time = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
            //         lab_study_time_text = table.Column<string>(type: "NVARCHAR2(1000)", maxLength: 1000, nullable: true),
            //         subject_type_id = table.Column<byte>(type: "NUMBER(3)", nullable: true),
            //         lecture_credit_amount = table.Column<decimal>(type: "DECIMAL(18,4)", precision: 18, scale: 4, nullable: true),
            //         total_lab_amount = table.Column<decimal>(type: "DECIMAL(18,4)", precision: 18, scale: 4, nullable: true),
            //         total_lecture_amount = table.Column<decimal>(type: "DECIMAL(18,4)", precision: 18, scale: 4, nullable: true),
            //         total_amount = table.Column<decimal>(type: "DECIMAL(18,4)", precision: 18, scale: 4, nullable: true),
            //         midterm_lecture_exam_from_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         midterm_lecture_exam_to_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         midterm_lab_exam_from_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         midterm_lab_exam_to_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         final_lecture_exam_from_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         final_lecture_exam_to_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         final_lab_exam_from_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         final_lab_exam_to_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         register_subject_status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         midterm_score = table.Column<decimal>(type: "decimal(18,2)", nullable: true),
            //         final_score = table.Column<decimal>(type: "decimal(18,2)", nullable: true),
            //         total_score = table.Column<decimal>(type: "decimal(18,2)", nullable: true),
            //         exam_score = table.Column<decimal>(type: "decimal(18,2)", nullable: true),
            //         grade_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         grade_reason_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         grade_remark = table.Column<string>(type: "NVARCHAR2(1000)", maxLength: 1000, nullable: true),
            //         primary_year_subject_section_uid = table.Column<Guid>(type: "RAW(16)", nullable: true)
            //     },
            //     constraints: table =>
            //     {
            //         table.PrimaryKey("PK_final_register_subject", x => x.final_register_subject_uid);
            //         table.ForeignKey(
            //             name: "FK_final_register_subject_final_register_final_register_uid",
            //             column: x => x.final_register_uid,
            //             principalSchema: "EDU",
            //             principalTable: "final_register",
            //             principalColumn: "final_register_uid",
            //             onDelete: ReferentialAction.Restrict);
            //     });
            //
            // migrationBuilder.CreateTable(
            //     name: "grade_criteria_option",
            //     schema: "EDU",
            //     columns: table => new
            //     {
            //         grade_criteria_option_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
            //         status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         grade_criteria_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         min_score = table.Column<decimal>(type: "decimal(18,2)", nullable: true),
            //         max_score = table.Column<decimal>(type: "decimal(18,2)", nullable: true),
            //         grade_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         description = table.Column<string>(type: "NVARCHAR2(500)", maxLength: 500, nullable: true)
            //     },
            //     constraints: table =>
            //     {
            //         table.PrimaryKey("PK_grade_criteria_option", x => x.grade_criteria_option_uid);
            //         table.ForeignKey(
            //             name: "FK_grade_criteria_option_grade_criteria_grade_criteria_uid",
            //             column: x => x.grade_criteria_uid,
            //             principalSchema: "EDU",
            //             principalTable: "grade_criteria",
            //             principalColumn: "grade_criteria_uid",
            //             onDelete: ReferentialAction.Cascade);
            //     });
            //
            // migrationBuilder.CreateTable(
            //     name: "student_group",
            //     schema: "EDU",
            //     columns: table => new
            //     {
            //         student_group_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
            //         group_uid1 = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         t_groupgroup_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         student_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         group_uid = table.Column<Guid>(type: "RAW(16)", nullable: true)
            //     },
            //     constraints: table =>
            //     {
            //         table.PrimaryKey("PK_student_group", x => x.student_group_uid);
            //         table.ForeignKey(
            //             name: "FK_student_group_group_t_groupgroup_uid",
            //             column: x => x.t_groupgroup_uid,
            //             principalSchema: "EDU",
            //             principalTable: "group",
            //             principalColumn: "group_uid",
            //             onDelete: ReferentialAction.Restrict);
            //     });
            //
            // migrationBuilder.CreateTable(
            //     name: "plan_register_fee",
            //     schema: "EDU",
            //     columns: table => new
            //     {
            //         plan_register_fee_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
            //         status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         plan_register_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         register_fee_detail_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         register_fee_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         fee_name_th = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         fee_name_en = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         fee_amount = table.Column<decimal>(type: "DECIMAL(18,4)", precision: 18, scale: 4, nullable: true)
            //     },
            //     constraints: table =>
            //     {
            //         table.PrimaryKey("PK_plan_register_fee", x => x.plan_register_fee_uid);
            //         table.ForeignKey(
            //             name: "FK_plan_register_fee_plan_register_plan_register_uid",
            //             column: x => x.plan_register_uid,
            //             principalSchema: "EDU",
            //             principalTable: "plan_register",
            //             principalColumn: "plan_register_uid",
            //             onDelete: ReferentialAction.Cascade);
            //     });
            //
            // migrationBuilder.CreateTable(
            //     name: "plan_register_subject",
            //     schema: "EDU",
            //     columns: table => new
            //     {
            //         plan_register_subject_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
            //         status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         plan_register_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         year_subject_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         year_subject_section_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         year_subject_code = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
            //         year_subject_name_th = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         year_subject_name_en = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         credit = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         lecture_year_subject_section_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         lecture_section_code = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
            //         lecture_study_time = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
            //         lecture_study_time_text = table.Column<string>(type: "NVARCHAR2(100)", maxLength: 100, nullable: true),
            //         topic_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         topic_code = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
            //         topic_name_th = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         topic_name_en = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         topic_credit = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         lab_year_subject_section_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         lab_section_code = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
            //         lab_study_time = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
            //         lab_study_time_text = table.Column<string>(type: "NVARCHAR2(1000)", maxLength: 1000, nullable: true),
            //         subject_type_id = table.Column<byte>(type: "NUMBER(3)", nullable: true),
            //         lecture_credit_amount = table.Column<decimal>(type: "DECIMAL(18,4)", precision: 18, scale: 4, nullable: true),
            //         total_lab_amount = table.Column<decimal>(type: "DECIMAL(18,4)", precision: 18, scale: 4, nullable: true),
            //         total_lecture_amount = table.Column<decimal>(type: "DECIMAL(18,4)", precision: 18, scale: 4, nullable: true),
            //         total_amount = table.Column<decimal>(type: "DECIMAL(18,4)", precision: 18, scale: 4, nullable: true),
            //         midterm_lecture_exam_from_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         midterm_lecture_exam_to_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         midterm_lab_exam_from_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         midterm_lab_exam_to_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         final_lecture_exam_from_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         final_lecture_exam_to_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         final_lab_exam_from_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         final_lab_exam_to_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         register_subject_status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         register_subject_flag_id = table.Column<short>(type: "NUMBER(5)", nullable: true)
            //     },
            //     constraints: table =>
            //     {
            //         table.PrimaryKey("PK_plan_register_subject", x => x.plan_register_subject_uid);
            //         table.ForeignKey(
            //             name: "FK_plan_register_subject_plan_register_plan_register_uid",
            //             column: x => x.plan_register_uid,
            //             principalSchema: "EDU",
            //             principalTable: "plan_register",
            //             principalColumn: "plan_register_uid",
            //             onDelete: ReferentialAction.Cascade);
            //     });
            //
            // migrationBuilder.CreateTable(
            //     name: "probation_retire_condition_detail",
            //     schema: "EDU",
            //     columns: table => new
            //     {
            //         probation_retire_condition_detail_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
            //         status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         probation_retire_condition_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         from_grade = table.Column<decimal>(type: "DECIMAL(18, 2)", nullable: true),
            //         to_grade = table.Column<decimal>(type: "DECIMAL(18, 2)", nullable: true),
            //         from_credit = table.Column<decimal>(type: "DECIMAL(18, 2)", nullable: true),
            //         to_credit = table.Column<decimal>(type: "DECIMAL(18, 2)", nullable: true),
            //         start_semester_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         previous_probations = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         pre_student_status_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         student_status_uid = table.Column<Guid>(type: "RAW(16)", nullable: true)
            //     },
            //     constraints: table =>
            //     {
            //         table.PrimaryKey("PK_probation_retire_condition_detail", x => x.probation_retire_condition_detail_uid);
            //         table.ForeignKey(
            //             name: "FK_probation_retire_condition_detail_probation_retire_condition_probation_retire_condition_uid",
            //             column: x => x.probation_retire_condition_uid,
            //             principalSchema: "EDU",
            //             principalTable: "probation_retire_condition",
            //             principalColumn: "probation_retire_condition_uid",
            //             onDelete: ReferentialAction.Cascade);
            //     });
            //
            // migrationBuilder.CreateTable(
            //     name: "provision_register_advance",
            //     schema: "EDU",
            //     columns: table => new
            //     {
            //         provision_register_advance_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
            //         status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         provision_register_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         advance_payment_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         used_amount = table.Column<decimal>(type: "DECIMAL(18,4)", precision: 18, scale: 4, nullable: true),
            //         row_order = table.Column<int>(type: "NUMBER(10)", nullable: true)
            //     },
            //     constraints: table =>
            //     {
            //         table.PrimaryKey("PK_provision_register_advance", x => x.provision_register_advance_uid);
            //         table.ForeignKey(
            //             name: "FK_provision_register_advance_provision_register_provision_register_uid",
            //             column: x => x.provision_register_uid,
            //             principalSchema: "EDU",
            //             principalTable: "provision_register",
            //             principalColumn: "provision_register_uid",
            //             onDelete: ReferentialAction.Cascade);
            //     });
            //
            // migrationBuilder.CreateTable(
            //     name: "provision_register_fee",
            //     schema: "EDU",
            //     columns: table => new
            //     {
            //         provision_register_fee_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
            //         status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         provision_register_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         register_fee_detail_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         register_fee_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         fee_name_th = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         fee_name_en = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         fee_amount = table.Column<decimal>(type: "DECIMAL(18,4)", precision: 18, scale: 4, nullable: true)
            //     },
            //     constraints: table =>
            //     {
            //         table.PrimaryKey("PK_provision_register_fee", x => x.provision_register_fee_uid);
            //         table.ForeignKey(
            //             name: "FK_provision_register_fee_provision_register_provision_register_uid",
            //             column: x => x.provision_register_uid,
            //             principalSchema: "EDU",
            //             principalTable: "provision_register",
            //             principalColumn: "provision_register_uid",
            //             onDelete: ReferentialAction.Cascade);
            //     });
            //
            // migrationBuilder.CreateTable(
            //     name: "provision_register_scholarship",
            //     schema: "EDU",
            //     columns: table => new
            //     {
            //         provision_register_scholarship_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
            //         status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         provision_register_uid = table.Column<Guid>(type: "RAW(16)", nullable: true)
            //     },
            //     constraints: table =>
            //     {
            //         table.PrimaryKey("PK_provision_register_scholarship", x => x.provision_register_scholarship_uid);
            //         table.ForeignKey(
            //             name: "FK_provision_register_scholarship_provision_register_provision_register_uid",
            //             column: x => x.provision_register_uid,
            //             principalSchema: "EDU",
            //             principalTable: "provision_register",
            //             principalColumn: "provision_register_uid",
            //             onDelete: ReferentialAction.Cascade);
            //     });
            //
            // migrationBuilder.CreateTable(
            //     name: "provision_register_subject",
            //     schema: "EDU",
            //     columns: table => new
            //     {
            //         provision_register_subject_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
            //         status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         provision_register_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         year_subject_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         year_subject_section_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         year_subject_code = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
            //         year_subject_name_th = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         year_subject_name_en = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         credit = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         lecture_year_subject_section_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         lecture_section_code = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
            //         lecture_study_time = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
            //         lecture_study_time_text = table.Column<string>(type: "NVARCHAR2(100)", maxLength: 100, nullable: true),
            //         topic_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         topic_code = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
            //         topic_name_th = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         topic_name_en = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         topic_credit = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         lab_year_subject_section_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         lab_section_code = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
            //         lab_study_time = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
            //         lab_study_time_text = table.Column<string>(type: "NVARCHAR2(1000)", maxLength: 1000, nullable: true),
            //         subject_type_id = table.Column<byte>(type: "NUMBER(3)", nullable: true),
            //         lecture_credit_amount = table.Column<decimal>(type: "DECIMAL(18,4)", precision: 18, scale: 4, nullable: true),
            //         total_lab_amount = table.Column<decimal>(type: "DECIMAL(18,4)", precision: 18, scale: 4, nullable: true),
            //         total_lecture_amount = table.Column<decimal>(type: "DECIMAL(18,4)", precision: 18, scale: 4, nullable: true),
            //         total_amount = table.Column<decimal>(type: "DECIMAL(18,4)", precision: 18, scale: 4, nullable: true),
            //         midterm_lecture_exam_from_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         midterm_lecture_exam_to_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         midterm_lab_exam_from_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         midterm_lab_exam_to_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         final_lecture_exam_from_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         final_lecture_exam_to_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         final_lab_exam_from_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         final_lab_exam_to_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         register_subject_status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         register_subject_flag_id = table.Column<short>(type: "NUMBER(5)", nullable: true)
            //     },
            //     constraints: table =>
            //     {
            //         table.PrimaryKey("PK_provision_register_subject", x => x.provision_register_subject_uid);
            //         table.ForeignKey(
            //             name: "FK_provision_register_subject_provision_register_provision_register_uid",
            //             column: x => x.provision_register_uid,
            //             principalSchema: "EDU",
            //             principalTable: "provision_register",
            //             principalColumn: "provision_register_uid",
            //             onDelete: ReferentialAction.Cascade);
            //     });
            //
            // migrationBuilder.CreateTable(
            //     name: "question_set",
            //     schema: "EDU",
            //     columns: table => new
            //     {
            //         question_set_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
            //         status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         question_group_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         question_set_code = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
            //         question_set_name_th = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         question_set_name_en = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         faculty_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         total_questions = table.Column<int>(type: "NUMBER(10)", nullable: true)
            //     },
            //     constraints: table =>
            //     {
            //         table.PrimaryKey("PK_question_set", x => x.question_set_uid);
            //         table.ForeignKey(
            //             name: "FK_question_set_question_group_question_group_uid",
            //             column: x => x.question_group_uid,
            //             principalSchema: "EDU",
            //             principalTable: "question_group",
            //             principalColumn: "question_group_uid",
            //             onDelete: ReferentialAction.SetNull);
            //     });
            //
            // migrationBuilder.CreateTable(
            //     name: "register_fee",
            //     schema: "EDU",
            //     columns: table => new
            //     {
            //         register_fee_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
            //         status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         fee_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         register_type_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         education_type_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         entry_academic_year_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         semester_uid = table.Column<Guid>(type: "RAW(16)", nullable: true)
            //     },
            //     constraints: table =>
            //     {
            //         table.PrimaryKey("PK_register_fee", x => x.register_fee_uid);
            //         table.ForeignKey(
            //             name: "FK_register_fee_register_type_register_type_uid",
            //             column: x => x.register_type_uid,
            //             principalSchema: "EDU",
            //             principalTable: "register_type",
            //             principalColumn: "register_type_uid",
            //             onDelete: ReferentialAction.Restrict);
            //     });
            //
            // migrationBuilder.CreateTable(
            //     name: "semester",
            //     schema: "EDU",
            //     columns: table => new
            //     {
            //         semester_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
            //         status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         semester_code = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
            //         semester_name_th = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         semester_name_en = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         semester_type_uid = table.Column<Guid>(type: "RAW(16)", nullable: true)
            //     },
            //     constraints: table =>
            //     {
            //         table.PrimaryKey("PK_semester", x => x.semester_uid);
            //         table.ForeignKey(
            //             name: "FK_semester_semester_type_semester_type_uid",
            //             column: x => x.semester_type_uid,
            //             principalSchema: "EDU",
            //             principalTable: "semester_type",
            //             principalColumn: "semester_type_uid",
            //             onDelete: ReferentialAction.Cascade);
            //     });
            //
            // migrationBuilder.CreateTable(
            //     name: "student_address",
            //     schema: "EDU",
            //     columns: table => new
            //     {
            //         student_address_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
            //         status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         student_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         address_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         address_type_id = table.Column<int>(type: "NUMBER(10)", nullable: true)
            //     },
            //     constraints: table =>
            //     {
            //         table.PrimaryKey("PK_student_address", x => x.student_address_uid);
            //         table.ForeignKey(
            //             name: "FK_student_address_student_student_uid",
            //             column: x => x.student_uid,
            //             principalSchema: "EDU",
            //             principalTable: "student",
            //             principalColumn: "student_uid",
            //             onDelete: ReferentialAction.Cascade);
            //     });
            //
            // migrationBuilder.CreateTable(
            //     name: "student_document",
            //     schema: "EDU",
            //     columns: table => new
            //     {
            //         student_document_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
            //         status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         student_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         attached_document_id = table.Column<int>(type: "NUMBER(10)", nullable: true),
            //         required_document_id = table.Column<int>(type: "NUMBER(10)", nullable: true),
            //         original_file_name = table.Column<string>(type: "NVARCHAR2(500)", maxLength: 500, nullable: true),
            //         file_id = table.Column<string>(type: "NVARCHAR2(500)", maxLength: 500, nullable: true),
            //         file_name = table.Column<string>(type: "NVARCHAR2(500)", maxLength: 500, nullable: true),
            //         file_path = table.Column<string>(type: "NVARCHAR2(500)", maxLength: 500, nullable: true),
            //         is_approve = table.Column<bool>(type: "NUMBER(1)", nullable: true),
            //         is_request_edit = table.Column<bool>(type: "NUMBER(1)", nullable: true),
            //         remark = table.Column<string>(type: "NVARCHAR2(1000)", maxLength: 1000, nullable: true)
            //     },
            //     constraints: table =>
            //     {
            //         table.PrimaryKey("PK_student_document", x => x.student_document_uid);
            //         table.ForeignKey(
            //             name: "FK_student_document_student_student_uid",
            //             column: x => x.student_uid,
            //             principalSchema: "EDU",
            //             principalTable: "student",
            //             principalColumn: "student_uid",
            //             onDelete: ReferentialAction.Cascade);
            //     });
            //
            // migrationBuilder.CreateTable(
            //     name: "student_parent",
            //     schema: "EDU",
            //     columns: table => new
            //     {
            //         student_parent_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
            //         status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         student_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         parent_type_id = table.Column<int>(type: "NUMBER(10)", nullable: true),
            //         prefix_id = table.Column<int>(type: "NUMBER(10)", nullable: true),
            //         first_name_th = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         last_name_th = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         middle_name_th = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         first_name_en = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         last_name_en = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         middle_name_en = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         display_name_th = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         display_name_en = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         marital_status_id = table.Column<int>(type: "NUMBER(10)", nullable: true),
            //         income_level_id = table.Column<int>(type: "NUMBER(10)", nullable: true),
            //         id_card_no = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
            //         id_card_issue_date = table.Column<DateTime>(type: "date", nullable: true),
            //         id_card_expire_date = table.Column<DateTime>(type: "date", nullable: true),
            //         address_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         personal_email = table.Column<string>(type: "NVARCHAR2(2000)", nullable: true),
            //         occupation_id = table.Column<int>(type: "NUMBER(10)", nullable: true),
            //         home_phone = table.Column<string>(type: "NVARCHAR2(2000)", nullable: true),
            //         mobile_phone = table.Column<string>(type: "NVARCHAR2(2000)", nullable: true),
            //         company_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         company_contact_email = table.Column<string>(type: "NVARCHAR2(2000)", nullable: true),
            //         company_contact_phone = table.Column<string>(type: "NVARCHAR2(2000)", nullable: true),
            //         is_deformed = table.Column<bool>(type: "NUMBER(1)", nullable: true),
            //         deformed_no = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
            //         deformed_issue_date = table.Column<DateTime>(type: "date", nullable: true),
            //         deformed_expire_date = table.Column<DateTime>(type: "date", nullable: true),
            //         deformed_detail = table.Column<string>(type: "NVARCHAR2(2000)", nullable: true),
            //         company_name = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         company_province_id = table.Column<string>(type: "NVARCHAR2(2000)", nullable: true),
            //         relationship_with_student = table.Column<string>(type: "NVARCHAR2(2000)", nullable: true)
            //     },
            //     constraints: table =>
            //     {
            //         table.PrimaryKey("PK_student_parent", x => x.student_parent_uid);
            //         table.ForeignKey(
            //             name: "FK_student_parent_student_student_uid",
            //             column: x => x.student_uid,
            //             principalSchema: "EDU",
            //             principalTable: "student",
            //             principalColumn: "student_uid",
            //             onDelete: ReferentialAction.Cascade);
            //     });
            //
            // migrationBuilder.CreateTable(
            //     name: "student_discipline_exam",
            //     schema: "EDU",
            //     columns: table => new
            //     {
            //         student_discipline_exam_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
            //         discipline_punishment_uid1 = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         student_discipline_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         deduct_score = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         action_date = table.Column<DateTime>(type: "date", nullable: true),
            //         action_time = table.Column<TimeSpan>(type: "INTERVAL DAY(8) TO SECOND(7)", nullable: true),
            //         discipline_punishment_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         remark = table.Column<string>(type: "NVARCHAR2(1000)", maxLength: 1000, nullable: true),
            //         action_by = table.Column<string>(type: "NVARCHAR2(2000)", nullable: true)
            //     },
            //     constraints: table =>
            //     {
            //         table.PrimaryKey("PK_student_discipline_exam", x => x.student_discipline_exam_uid);
            //         table.ForeignKey(
            //             name: "FK_student_discipline_exam_discipline_punishment_discipline_punishment_uid1",
            //             column: x => x.discipline_punishment_uid1,
            //             principalSchema: "EDU",
            //             principalTable: "discipline_punishment",
            //             principalColumn: "discipline_punishment_uid",
            //             onDelete: ReferentialAction.Restrict);
            //         table.ForeignKey(
            //             name: "FK_student_discipline_exam_student_discipline_student_discipline_uid",
            //             column: x => x.student_discipline_uid,
            //             principalSchema: "EDU",
            //             principalTable: "student_discipline",
            //             principalColumn: "student_discipline_uid",
            //             onDelete: ReferentialAction.Cascade);
            //     });
            //
            // migrationBuilder.CreateTable(
            //     name: "student_discipline_score",
            //     schema: "EDU",
            //     columns: table => new
            //     {
            //         student_discipline_score_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
            //         status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         student_discipline_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         discipline_score_status_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         is_approve = table.Column<bool>(type: "NUMBER(1)", nullable: true),
            //         discipline_score_status_date = table.Column<DateTime>(type: "date", nullable: true),
            //         action_by = table.Column<string>(type: "NVARCHAR2(2000)", nullable: true)
            //     },
            //     constraints: table =>
            //     {
            //         table.PrimaryKey("PK_student_discipline_score", x => x.student_discipline_score_uid);
            //         table.ForeignKey(
            //             name: "FK_student_discipline_score_student_discipline_student_discipline_uid",
            //             column: x => x.student_discipline_uid,
            //             principalSchema: "EDU",
            //             principalTable: "student_discipline",
            //             principalColumn: "student_discipline_uid",
            //             onDelete: ReferentialAction.Cascade);
            //     });
            //
            // migrationBuilder.CreateTable(
            //     name: "student_service_activity",
            //     schema: "EDU",
            //     columns: table => new
            //     {
            //         student_service_activity_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
            //         status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         student_discipline_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         activity_name = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         activity_date = table.Column<DateTime>(type: "date", nullable: true),
            //         activity_time = table.Column<TimeSpan>(type: "INTERVAL DAY(8) TO SECOND(7)", nullable: true),
            //         score = table.Column<decimal>(type: "decimal(18,4)", nullable: true),
            //         remark = table.Column<string>(type: "NVARCHAR2(1000)", maxLength: 1000, nullable: true),
            //         action_by = table.Column<string>(type: "NVARCHAR2(2000)", nullable: true)
            //     },
            //     constraints: table =>
            //     {
            //         table.PrimaryKey("PK_student_service_activity", x => x.student_service_activity_uid);
            //         table.ForeignKey(
            //             name: "FK_student_service_activity_student_discipline_student_discipline_uid",
            //             column: x => x.student_discipline_uid,
            //             principalSchema: "EDU",
            //             principalTable: "student_discipline",
            //             principalColumn: "student_discipline_uid",
            //             onDelete: ReferentialAction.Cascade);
            //     });
            //
            // migrationBuilder.CreateTable(
            //     name: "student_evaluation_question_set",
            //     schema: "EDU",
            //     columns: table => new
            //     {
            //         student_evaluation_question_set_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
            //         status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         student_evaluation_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         question_set_faculty_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         question_set_curriculum_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         question_set_uid = table.Column<Guid>(type: "RAW(16)", nullable: true)
            //     },
            //     constraints: table =>
            //     {
            //         table.PrimaryKey("PK_student_evaluation_question_set", x => x.student_evaluation_question_set_uid);
            //         table.ForeignKey(
            //             name: "FK_student_evaluation_question_set_student_evaluation_student_evaluation_uid",
            //             column: x => x.student_evaluation_uid,
            //             principalSchema: "EDU",
            //             principalTable: "student_evaluation",
            //             principalColumn: "student_evaluation_uid",
            //             onDelete: ReferentialAction.Cascade);
            //     });
            //
            // migrationBuilder.CreateTable(
            //     name: "student_illness_file",
            //     schema: "EDU",
            //     columns: table => new
            //     {
            //         student_illness_file_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
            //         status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         student_illness_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         row_order = table.Column<int>(type: "NUMBER(10)", nullable: true),
            //         file_name = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         file_path = table.Column<string>(type: "NVARCHAR2(500)", maxLength: 500, nullable: true)
            //     },
            //     constraints: table =>
            //     {
            //         table.PrimaryKey("PK_student_illness_file", x => x.student_illness_file_uid);
            //         table.ForeignKey(
            //             name: "FK_student_illness_file_student_illness_student_illness_uid",
            //             column: x => x.student_illness_uid,
            //             principalSchema: "EDU",
            //             principalTable: "student_illness",
            //             principalColumn: "student_illness_uid",
            //             onDelete: ReferentialAction.Cascade);
            //     });
            //
            // migrationBuilder.CreateTable(
            //     name: "student_illness_photo",
            //     schema: "EDU",
            //     columns: table => new
            //     {
            //         student_illness_photo_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
            //         status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         student_illness_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         row_order = table.Column<int>(type: "NUMBER(10)", nullable: true),
            //         photo_name = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         photo_path = table.Column<string>(type: "NVARCHAR2(500)", maxLength: 500, nullable: true)
            //     },
            //     constraints: table =>
            //     {
            //         table.PrimaryKey("PK_student_illness_photo", x => x.student_illness_photo_uid);
            //         table.ForeignKey(
            //             name: "FK_student_illness_photo_student_illness_student_illness_uid",
            //             column: x => x.student_illness_uid,
            //             principalSchema: "EDU",
            //             principalTable: "student_illness",
            //             principalColumn: "student_illness_uid",
            //             onDelete: ReferentialAction.Cascade);
            //     });
            //
            // migrationBuilder.CreateTable(
            //     name: "student_status_restriction",
            //     schema: "EDU",
            //     columns: table => new
            //     {
            //         student_status_restriction_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
            //         status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         student_status_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         student_restriction_uid = table.Column<Guid>(type: "RAW(16)", nullable: true)
            //     },
            //     constraints: table =>
            //     {
            //         table.PrimaryKey("PK_student_status_restriction", x => x.student_status_restriction_uid);
            //         table.ForeignKey(
            //             name: "FK_student_status_restriction_student_restriction_student_restriction_uid",
            //             column: x => x.student_restriction_uid,
            //             principalSchema: "EDU",
            //             principalTable: "student_restriction",
            //             principalColumn: "student_restriction_uid",
            //             onDelete: ReferentialAction.SetNull);
            //         table.ForeignKey(
            //             name: "FK_student_status_restriction_student_status_student_status_uid",
            //             column: x => x.student_status_uid,
            //             principalSchema: "EDU",
            //             principalTable: "student_status",
            //             principalColumn: "student_status_uid",
            //             onDelete: ReferentialAction.SetNull);
            //     });
            //
            // migrationBuilder.CreateTable(
            //     name: "subject_owner",
            //     schema: "EDU",
            //     columns: table => new
            //     {
            //         subject_owner_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
            //         status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         subject_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         academic_year_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         college_faculty_uid = table.Column<Guid>(type: "RAW(16)", nullable: true)
            //     },
            //     constraints: table =>
            //     {
            //         table.PrimaryKey("PK_subject_owner", x => x.subject_owner_uid);
            //         table.ForeignKey(
            //             name: "FK_subject_owner_subject_subject_uid",
            //             column: x => x.subject_uid,
            //             principalSchema: "EDU",
            //             principalTable: "subject",
            //             principalColumn: "subject_uid",
            //             onDelete: ReferentialAction.Cascade);
            //     });
            //
            // migrationBuilder.CreateTable(
            //     name: "subject_topic",
            //     schema: "EDU",
            //     columns: table => new
            //     {
            //         subject_topic_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
            //         status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         academic_year_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         academic_year_code = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
            //         subject_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         topic_code = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
            //         topic_name_th = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         topic_name_en = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         topic_credit = table.Column<decimal>(type: "decimal(5,1)", nullable: true),
            //         subject_code = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
            //         subject_seq_code = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true)
            //     },
            //     constraints: table =>
            //     {
            //         table.PrimaryKey("PK_subject_topic", x => x.subject_topic_uid);
            //         table.ForeignKey(
            //             name: "FK_subject_topic_subject_subject_uid",
            //             column: x => x.subject_uid,
            //             principalSchema: "EDU",
            //             principalTable: "subject",
            //             principalColumn: "subject_uid",
            //             onDelete: ReferentialAction.Cascade);
            //     });
            //
            // migrationBuilder.CreateTable(
            //     name: "subject_year",
            //     schema: "EDU",
            //     columns: table => new
            //     {
            //         subject_year_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
            //         status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         subject_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         curriculum_year_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         curriculum_year_code = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
            //         credit_text = table.Column<string>(type: "NVARCHAR2(2000)", nullable: true),
            //         remark = table.Column<string>(type: "NVARCHAR2(1000)", maxLength: 1000, nullable: true),
            //         lecture_hour = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         lab_hour = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         practice_hour = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         min_grade = table.Column<string>(type: "NVARCHAR2(2000)", nullable: true),
            //         credit = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         is_gpa_include = table.Column<bool>(type: "NUMBER(1)", nullable: true),
            //         allow_scholarship = table.Column<bool>(type: "NUMBER(1)", nullable: true),
            //         subject_type_code = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true)
            //     },
            //     constraints: table =>
            //     {
            //         table.PrimaryKey("PK_subject_year", x => x.subject_year_uid);
            //         table.ForeignKey(
            //             name: "FK_subject_year_subject_subject_uid",
            //             column: x => x.subject_uid,
            //             principalSchema: "EDU",
            //             principalTable: "subject",
            //             principalColumn: "subject_uid",
            //             onDelete: ReferentialAction.Cascade);
            //     });
            //
            // migrationBuilder.CreateTable(
            //     name: "subject_master_owner",
            //     schema: "EDU",
            //     columns: table => new
            //     {
            //         subject_master_owner_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
            //         subject_master_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         register_year = table.Column<string>(type: "NVARCHAR2(2000)", nullable: true),
            //         faculty_code = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
            //         curriculum_code = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
            //         owner_faculty_percent = table.Column<decimal>(type: "DECIMAL(5,2)", precision: 5, scale: 2, nullable: true),
            //         student_faculty_percent = table.Column<decimal>(type: "DECIMAL(5,2)", precision: 5, scale: 2, nullable: true)
            //     },
            //     constraints: table =>
            //     {
            //         table.PrimaryKey("PK_subject_master_owner", x => x.subject_master_owner_uid);
            //         table.ForeignKey(
            //             name: "FK_subject_master_owner_subject_master_subject_master_uid",
            //             column: x => x.subject_master_uid,
            //             principalSchema: "EDU",
            //             principalTable: "subject_master",
            //             principalColumn: "subject_master_uid",
            //             onDelete: ReferentialAction.Restrict);
            //     });
            //
            // migrationBuilder.CreateTable(
            //     name: "subject_master_year",
            //     schema: "EDU",
            //     columns: table => new
            //     {
            //         subject_master_year_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
            //         subject_master_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         curriculum_year = table.Column<string>(type: "NVARCHAR2(2000)", nullable: true),
            //         credit_text = table.Column<string>(type: "NVARCHAR2(2000)", nullable: true),
            //         remark = table.Column<string>(type: "NVARCHAR2(1000)", maxLength: 1000, nullable: true),
            //         lecture_hour = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         lab_hour = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         practice_hour = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         min_grade = table.Column<string>(type: "NVARCHAR2(2000)", nullable: true),
            //         credit = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         is_gpa_include = table.Column<bool>(type: "NUMBER(1)", nullable: true),
            //         pre_subject_codes = table.Column<string>(type: "NVARCHAR2(2000)", nullable: true),
            //         co_subject_codes = table.Column<string>(type: "NVARCHAR2(2000)", nullable: true),
            //         allow_scholarship = table.Column<bool>(type: "NUMBER(1)", nullable: true),
            //         subject_type_code = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true)
            //     },
            //     constraints: table =>
            //     {
            //         table.PrimaryKey("PK_subject_master_year", x => x.subject_master_year_uid);
            //         table.ForeignKey(
            //             name: "FK_subject_master_year_subject_master_subject_master_uid",
            //             column: x => x.subject_master_uid,
            //             principalSchema: "EDU",
            //             principalTable: "subject_master",
            //             principalColumn: "subject_master_uid",
            //             onDelete: ReferentialAction.Restrict);
            //     });
            //
            // migrationBuilder.CreateTable(
            //     name: "year_subject_exam",
            //     schema: "EDU",
            //     columns: table => new
            //     {
            //         year_subject_exam_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
            //         status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         year_subject_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         midterm_lecture_exam_date = table.Column<DateTime>(type: "date", nullable: true),
            //         midterm_lecture_exam_time_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         midterm_lecture_exam_from_time = table.Column<TimeSpan>(type: "INTERVAL DAY(8) TO SECOND(7)", nullable: true),
            //         midterm_lecture_exam_to_time = table.Column<TimeSpan>(type: "INTERVAL DAY(8) TO SECOND(7)", nullable: true),
            //         midterm_lab_exam_date = table.Column<DateTime>(type: "date", nullable: true),
            //         midterm_lab_exam_time_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         midterm_lab_exam_from_time = table.Column<TimeSpan>(type: "INTERVAL DAY(8) TO SECOND(7)", nullable: true),
            //         midterm_lab_exam_to_time = table.Column<TimeSpan>(type: "INTERVAL DAY(8) TO SECOND(7)", nullable: true),
            //         final_lecture_exam_date = table.Column<DateTime>(type: "date", nullable: true),
            //         final_lecture_exam_time_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         final_lecture_exam_from_time = table.Column<TimeSpan>(type: "INTERVAL DAY(8) TO SECOND(7)", nullable: true),
            //         final_lecture_exam_to_time = table.Column<TimeSpan>(type: "INTERVAL DAY(8) TO SECOND(7)", nullable: true),
            //         final_lab_exam_date = table.Column<DateTime>(type: "date", nullable: true),
            //         final_lab_exam_time_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         final_lab_exam_from_time = table.Column<TimeSpan>(type: "INTERVAL DAY(8) TO SECOND(7)", nullable: true),
            //         final_lab_exam_to_time = table.Column<TimeSpan>(type: "INTERVAL DAY(8) TO SECOND(7)", nullable: true)
            //     },
            //     constraints: table =>
            //     {
            //         table.PrimaryKey("PK_year_subject_exam", x => x.year_subject_exam_uid);
            //         table.ForeignKey(
            //             name: "FK_year_subject_exam_year_subject_year_subject_uid",
            //             column: x => x.year_subject_uid,
            //             principalSchema: "EDU",
            //             principalTable: "year_subject",
            //             principalColumn: "year_subject_uid",
            //             onDelete: ReferentialAction.Cascade);
            //     });
            //
            // migrationBuilder.CreateTable(
            //     name: "year_subject_section",
            //     schema: "EDU",
            //     columns: table => new
            //     {
            //         year_subject_section_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
            //         status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         year_subject_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         section_code = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
            //         section_type_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         section_type_code = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
            //         web_quota = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         package_quota = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         quota = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         reserved = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         web_reserved = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         package_reserved = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         confirmed = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         web_confirmed = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         package_confirmed = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         remark = table.Column<string>(type: "NVARCHAR2(1000)", maxLength: 1000, nullable: true),
            //         is_approved = table.Column<bool>(type: "NUMBER(1)", nullable: true),
            //         college_faculty_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         subject_topic_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         topic_code = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
            //         topic_name_th = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         topic_name_en = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         topic_credit = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         primary_instructor_name_th = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         primary_instructor_name_en = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         evaluate_status_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         grade_criteria_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         is_primary = table.Column<bool>(type: "NUMBER(1)", nullable: true)
            //     },
            //     constraints: table =>
            //     {
            //         table.PrimaryKey("PK_year_subject_section", x => x.year_subject_section_uid);
            //         table.ForeignKey(
            //             name: "FK_year_subject_section_year_subject_year_subject_uid",
            //             column: x => x.year_subject_uid,
            //             principalSchema: "EDU",
            //             principalTable: "year_subject",
            //             principalColumn: "year_subject_uid",
            //             onDelete: ReferentialAction.Restrict);
            //     });
            //
            // migrationBuilder.CreateTable(
            //     name: "academic_year_filter_curriculum",
            //     schema: "EDU",
            //     columns: table => new
            //     {
            //         academic_year_filter_curriculum_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
            //         status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         academic_year_filter_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         curriculum_uid = table.Column<Guid>(type: "RAW(16)", nullable: true)
            //     },
            //     constraints: table =>
            //     {
            //         table.PrimaryKey("PK_academic_year_filter_curriculum", x => x.academic_year_filter_curriculum_uid);
            //         table.ForeignKey(
            //             name: "FK_academic_year_filter_curriculum_academic_year_filter_academic_year_filter_uid",
            //             column: x => x.academic_year_filter_uid,
            //             principalSchema: "EDU",
            //             principalTable: "academic_year_filter",
            //             principalColumn: "academic_year_filter_uid",
            //             onDelete: ReferentialAction.Cascade);
            //     });
            //
            // migrationBuilder.CreateTable(
            //     name: "academic_year_filter_education_type",
            //     schema: "EDU",
            //     columns: table => new
            //     {
            //         academic_year_filter_education_type_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
            //         status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         academic_year_filter_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         education_type_uid = table.Column<Guid>(type: "RAW(16)", nullable: true)
            //     },
            //     constraints: table =>
            //     {
            //         table.PrimaryKey("PK_academic_year_filter_education_type", x => x.academic_year_filter_education_type_uid);
            //         table.ForeignKey(
            //             name: "FK_academic_year_filter_education_type_academic_year_filter_academic_year_filter_uid",
            //             column: x => x.academic_year_filter_uid,
            //             principalSchema: "EDU",
            //             principalTable: "academic_year_filter",
            //             principalColumn: "academic_year_filter_uid",
            //             onDelete: ReferentialAction.Cascade);
            //     });
            //
            // migrationBuilder.CreateTable(
            //     name: "academic_year_filter_faculty",
            //     schema: "EDU",
            //     columns: table => new
            //     {
            //         academic_year_filter_faculty_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
            //         status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         academic_year_filter_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         faculty_uid = table.Column<Guid>(type: "RAW(16)", nullable: true)
            //     },
            //     constraints: table =>
            //     {
            //         table.PrimaryKey("PK_academic_year_filter_faculty", x => x.academic_year_filter_faculty_uid);
            //         table.ForeignKey(
            //             name: "FK_academic_year_filter_faculty_academic_year_filter_academic_year_filter_uid",
            //             column: x => x.academic_year_filter_uid,
            //             principalSchema: "EDU",
            //             principalTable: "academic_year_filter",
            //             principalColumn: "academic_year_filter_uid",
            //             onDelete: ReferentialAction.Cascade);
            //     });
            //
            // migrationBuilder.CreateTable(
            //     name: "curriculum_subject_group",
            //     schema: "EDU",
            //     columns: table => new
            //     {
            //         curriculum_subject_group_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
            //         status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         curriculum_subject_category_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         curriculum_subject_group_code = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
            //         curriculum_subject_group_name_th = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         curriculum_subject_group_name_en = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         total_credit = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         total_subject = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         min_credit = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         max_credit = table.Column<short>(type: "NUMBER(5)", nullable: true)
            //     },
            //     constraints: table =>
            //     {
            //         table.PrimaryKey("PK_curriculum_subject_group", x => x.curriculum_subject_group_uid);
            //         table.ForeignKey(
            //             name: "FK_curriculum_subject_group_curriculum_subject_category_curriculum_subject_category_uid",
            //             column: x => x.curriculum_subject_category_uid,
            //             principalSchema: "EDU",
            //             principalTable: "curriculum_subject_category",
            //             principalColumn: "curriculum_subject_category_uid",
            //             onDelete: ReferentialAction.Cascade);
            //     });
            //
            // migrationBuilder.CreateTable(
            //     name: "discipline_condition_detail",
            //     schema: "EDU",
            //     columns: table => new
            //     {
            //         discipline_condition_detail_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
            //         status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         discipline_condition_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         detail_section_code = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
            //         detail_section_name_th = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         detail_section_name_en = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         row_order = table.Column<int>(type: "NUMBER(10)", nullable: true)
            //     },
            //     constraints: table =>
            //     {
            //         table.PrimaryKey("PK_discipline_condition_detail", x => x.discipline_condition_detail_uid);
            //         table.ForeignKey(
            //             name: "FK_discipline_condition_detail_discipline_condition_discipline_condition_uid",
            //             column: x => x.discipline_condition_uid,
            //             principalSchema: "EDU",
            //             principalTable: "discipline_condition",
            //             principalColumn: "discipline_condition_uid",
            //             onDelete: ReferentialAction.Cascade);
            //     });
            //
            // migrationBuilder.CreateTable(
            //     name: "education_type_list",
            //     schema: "EDU",
            //     columns: table => new
            //     {
            //         education_type_list_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
            //         status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         education_type_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         education_type_code = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
            //         education_type_name_th = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         education_type_name_en = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         remark = table.Column<string>(type: "NVARCHAR2(1000)", maxLength: 1000, nullable: true),
            //         education_type_level_code = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
            //         dynamic_fee_education_type_uid = table.Column<Guid>(type: "RAW(16)", nullable: true)
            //     },
            //     constraints: table =>
            //     {
            //         table.PrimaryKey("PK_education_type_list", x => x.education_type_list_uid);
            //         table.ForeignKey(
            //             name: "FK_education_type_list_dynamic_fee_education_type_dynamic_fee_education_type_uid",
            //             column: x => x.dynamic_fee_education_type_uid,
            //             principalSchema: "EDU",
            //             principalTable: "dynamic_fee_education_type",
            //             principalColumn: "dynamic_fee_education_type_uid",
            //             onDelete: ReferentialAction.Cascade);
            //     });
            //
            // migrationBuilder.CreateTable(
            //     name: "evaluation_calendar_faculty_curriculum",
            //     schema: "EDU",
            //     columns: table => new
            //     {
            //         evaluation_calendar_faculty_curriculum_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
            //         status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         evaluation_calendar_faculty_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         faculty_curriculum_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         is_selected = table.Column<bool>(type: "NUMBER(1)", nullable: true)
            //     },
            //     constraints: table =>
            //     {
            //         table.PrimaryKey("PK_evaluation_calendar_faculty_curriculum", x => x.evaluation_calendar_faculty_curriculum_uid);
            //         table.ForeignKey(
            //             name: "FK_evaluation_calendar_faculty_curriculum_evaluation_calendar_faculty_evaluation_calendar_faculty_uid",
            //             column: x => x.evaluation_calendar_faculty_uid,
            //             principalSchema: "EDU",
            //             principalTable: "evaluation_calendar_faculty",
            //             principalColumn: "evaluation_calendar_faculty_uid",
            //             onDelete: ReferentialAction.Cascade);
            //     });
            //
            // migrationBuilder.CreateTable(
            //     name: "education_type_fee_condition",
            //     schema: "EDU",
            //     columns: table => new
            //     {
            //         education_type_fee_condition_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
            //         status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         education_type_fee_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         academic_year_uid = table.Column<Guid>(type: "RAW(16)", nullable: true)
            //     },
            //     constraints: table =>
            //     {
            //         table.PrimaryKey("PK_education_type_fee_condition", x => x.education_type_fee_condition_uid);
            //         table.ForeignKey(
            //             name: "FK_education_type_fee_condition_education_type_fee_education_type_fee_uid",
            //             column: x => x.education_type_fee_uid,
            //             principalSchema: "EDU",
            //             principalTable: "education_type_fee",
            //             principalColumn: "education_type_fee_uid",
            //             onDelete: ReferentialAction.Cascade);
            //     });
            //
            // migrationBuilder.CreateTable(
            //     name: "question",
            //     schema: "EDU",
            //     columns: table => new
            //     {
            //         question_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
            //         status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         question_set_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         answer_type_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         question_no = table.Column<int>(type: "NUMBER(10)", nullable: true),
            //         question_name_th = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         question_name_en = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         is_calculate = table.Column<bool>(type: "NUMBER(1)", nullable: true)
            //     },
            //     constraints: table =>
            //     {
            //         table.PrimaryKey("PK_question", x => x.question_uid);
            //         table.ForeignKey(
            //             name: "FK_question_answer_type_answer_type_uid",
            //             column: x => x.answer_type_uid,
            //             principalSchema: "EDU",
            //             principalTable: "answer_type",
            //             principalColumn: "answer_type_uid",
            //             onDelete: ReferentialAction.SetNull);
            //         table.ForeignKey(
            //             name: "FK_question_question_set_question_set_uid",
            //             column: x => x.question_set_uid,
            //             principalSchema: "EDU",
            //             principalTable: "question_set",
            //             principalColumn: "question_set_uid",
            //             onDelete: ReferentialAction.Cascade);
            //     });
            //
            // migrationBuilder.CreateTable(
            //     name: "question_set_curriculum",
            //     schema: "EDU",
            //     columns: table => new
            //     {
            //         question_set_curriculum_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
            //         status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         question_set_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         academic_year_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         academic_semester_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         faculty_curriculum_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         college_faculty_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         score_calculation_type_id = table.Column<int>(type: "NUMBER(10)", nullable: true)
            //     },
            //     constraints: table =>
            //     {
            //         table.PrimaryKey("PK_question_set_curriculum", x => x.question_set_curriculum_uid);
            //         table.ForeignKey(
            //             name: "FK_question_set_curriculum_question_set_question_set_uid",
            //             column: x => x.question_set_uid,
            //             principalSchema: "EDU",
            //             principalTable: "question_set",
            //             principalColumn: "question_set_uid",
            //             onDelete: ReferentialAction.Restrict);
            //     });
            //
            // migrationBuilder.CreateTable(
            //     name: "question_set_faculty",
            //     schema: "EDU",
            //     columns: table => new
            //     {
            //         question_set_faculty_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
            //         status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         question_set_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         academic_year_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         academic_semester_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         college_faculty_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         score_calculation_type_id = table.Column<int>(type: "NUMBER(10)", nullable: true)
            //     },
            //     constraints: table =>
            //     {
            //         table.PrimaryKey("PK_question_set_faculty", x => x.question_set_faculty_uid);
            //         table.ForeignKey(
            //             name: "FK_question_set_faculty_question_set_question_set_uid",
            //             column: x => x.question_set_uid,
            //             principalSchema: "EDU",
            //             principalTable: "question_set",
            //             principalColumn: "question_set_uid",
            //             onDelete: ReferentialAction.Restrict);
            //     });
            //
            // migrationBuilder.CreateTable(
            //     name: "question_set_subject",
            //     schema: "EDU",
            //     columns: table => new
            //     {
            //         question_set_subject_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
            //         status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         question_set_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         academic_year_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         academic_semester_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         subject_year_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         score_calculation_type_id = table.Column<int>(type: "NUMBER(10)", nullable: true)
            //     },
            //     constraints: table =>
            //     {
            //         table.PrimaryKey("PK_question_set_subject", x => x.question_set_subject_uid);
            //         table.ForeignKey(
            //             name: "FK_question_set_subject_question_set_question_set_uid",
            //             column: x => x.question_set_uid,
            //             principalSchema: "EDU",
            //             principalTable: "question_set",
            //             principalColumn: "question_set_uid",
            //             onDelete: ReferentialAction.Restrict);
            //     });
            //
            // migrationBuilder.CreateTable(
            //     name: "register_fee_filter",
            //     schema: "EDU",
            //     columns: table => new
            //     {
            //         register_fee_filter_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
            //         status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         register_fee_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         register_fee_filter_code = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
            //         register_fee_filter_name_th = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         register_fee_filter_name_en = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true)
            //     },
            //     constraints: table =>
            //     {
            //         table.PrimaryKey("PK_register_fee_filter", x => x.register_fee_filter_uid);
            //         table.ForeignKey(
            //             name: "FK_register_fee_filter_register_fee_register_fee_uid",
            //             column: x => x.register_fee_uid,
            //             principalSchema: "EDU",
            //             principalTable: "register_fee",
            //             principalColumn: "register_fee_uid",
            //             onDelete: ReferentialAction.Cascade);
            //     });
            //
            // migrationBuilder.CreateTable(
            //     name: "academic_semester",
            //     schema: "EDU",
            //     columns: table => new
            //     {
            //         academic_semester_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
            //         status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         academic_year_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         semester_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         semester_name_th = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         semester_name_en = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         academic_semester_code = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
            //         semester_start_date = table.Column<DateTime>(type: "date", nullable: true),
            //         semester_end_date = table.Column<DateTime>(type: "date", nullable: true),
            //         is_student_status_update = table.Column<bool>(type: "NUMBER(1)", nullable: true),
            //         semester_order = table.Column<int>(type: "NUMBER(10)", nullable: true),
            //         academic_year_filter_uid = table.Column<Guid>(type: "RAW(16)", nullable: true)
            //     },
            //     constraints: table =>
            //     {
            //         table.PrimaryKey("PK_academic_semester", x => x.academic_semester_uid);
            //         table.ForeignKey(
            //             name: "FK_academic_semester_academic_year_academic_year_uid",
            //             column: x => x.academic_year_uid,
            //             principalSchema: "EDU",
            //             principalTable: "academic_year",
            //             principalColumn: "academic_year_uid",
            //             onDelete: ReferentialAction.Cascade);
            //         table.ForeignKey(
            //             name: "FK_academic_semester_academic_year_filter_academic_year_filter_uid",
            //             column: x => x.academic_year_filter_uid,
            //             principalSchema: "EDU",
            //             principalTable: "academic_year_filter",
            //             principalColumn: "academic_year_filter_uid",
            //             onDelete: ReferentialAction.Cascade);
            //         table.ForeignKey(
            //             name: "FK_academic_semester_semester_semester_uid",
            //             column: x => x.semester_uid,
            //             principalSchema: "EDU",
            //             principalTable: "semester",
            //             principalColumn: "semester_uid",
            //             onDelete: ReferentialAction.Restrict);
            //     });
            //
            // migrationBuilder.CreateTable(
            //     name: "company",
            //     schema: "EDU",
            //     columns: table => new
            //     {
            //         company_id = table.Column<Guid>(type: "RAW(16)", nullable: false),
            //         student_parent_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         address_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         company_name = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         company_address_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         company_email = table.Column<string>(type: "NVARCHAR2(2000)", nullable: true),
            //         company_phone = table.Column<string>(type: "NVARCHAR2(2000)", nullable: true)
            //     },
            //     constraints: table =>
            //     {
            //         table.PrimaryKey("PK_company", x => x.company_id);
            //         table.ForeignKey(
            //             name: "FK_company_address_address_uid",
            //             column: x => x.address_uid,
            //             principalSchema: "EDU",
            //             principalTable: "address",
            //             principalColumn: "address_uid",
            //             onDelete: ReferentialAction.Restrict);
            //         table.ForeignKey(
            //             name: "FK_company_student_parent_student_parent_uid",
            //             column: x => x.student_parent_uid,
            //             principalSchema: "EDU",
            //             principalTable: "student_parent",
            //             principalColumn: "student_parent_uid",
            //             onDelete: ReferentialAction.Restrict);
            //     });
            //
            // migrationBuilder.CreateTable(
            //     name: "student_evaluation_question_result",
            //     schema: "EDU",
            //     columns: table => new
            //     {
            //         student_evaluation_question_result_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
            //         status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         student_evaluation_question_set_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         question_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         selected_choice = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         text_answer = table.Column<string>(type: "NVARCHAR2(500)", maxLength: 500, nullable: true)
            //     },
            //     constraints: table =>
            //     {
            //         table.PrimaryKey("PK_student_evaluation_question_result", x => x.student_evaluation_question_result_uid);
            //         table.ForeignKey(
            //             name: "FK_student_evaluation_question_result_student_evaluation_question_set_student_evaluation_question_set_uid",
            //             column: x => x.student_evaluation_question_set_uid,
            //             principalSchema: "EDU",
            //             principalTable: "student_evaluation_question_set",
            //             principalColumn: "student_evaluation_question_set_uid",
            //             onDelete: ReferentialAction.Cascade);
            //     });
            //
            // migrationBuilder.CreateTable(
            //     name: "subject_year_co",
            //     schema: "EDU",
            //     columns: table => new
            //     {
            //         subject_year_co_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
            //         status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         subject_year_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         subject_uid = table.Column<Guid>(type: "RAW(16)", nullable: true)
            //     },
            //     constraints: table =>
            //     {
            //         table.PrimaryKey("PK_subject_year_co", x => x.subject_year_co_uid);
            //         table.ForeignKey(
            //             name: "FK_subject_year_co_subject_year_subject_year_uid",
            //             column: x => x.subject_year_uid,
            //             principalSchema: "EDU",
            //             principalTable: "subject_year",
            //             principalColumn: "subject_year_uid",
            //             onDelete: ReferentialAction.Restrict);
            //     });
            //
            // migrationBuilder.CreateTable(
            //     name: "subject_year_cost",
            //     schema: "EDU",
            //     columns: table => new
            //     {
            //         subject_year_cost_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
            //         status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         subject_year_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         education_type_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         lecture_amount = table.Column<decimal>(type: "DECIMAL(18,4)", precision: 18, scale: 4, nullable: true),
            //         lab_amount = table.Column<decimal>(type: "DECIMAL(18,4)", precision: 18, scale: 4, nullable: true),
            //         practice_amount = table.Column<decimal>(type: "DECIMAL(18,4)", precision: 18, scale: 4, nullable: true)
            //     },
            //     constraints: table =>
            //     {
            //         table.PrimaryKey("PK_subject_year_cost", x => x.subject_year_cost_uid);
            //         table.ForeignKey(
            //             name: "FK_subject_year_cost_subject_year_subject_year_uid",
            //             column: x => x.subject_year_uid,
            //             principalSchema: "EDU",
            //             principalTable: "subject_year",
            //             principalColumn: "subject_year_uid",
            //             onDelete: ReferentialAction.Restrict);
            //     });
            //
            // migrationBuilder.CreateTable(
            //     name: "subject_year_pre",
            //     schema: "EDU",
            //     columns: table => new
            //     {
            //         subject_year_pre_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
            //         status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         subject_year_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         subject_uid = table.Column<Guid>(type: "RAW(16)", nullable: true)
            //     },
            //     constraints: table =>
            //     {
            //         table.PrimaryKey("PK_subject_year_pre", x => x.subject_year_pre_uid);
            //         table.ForeignKey(
            //             name: "FK_subject_year_pre_subject_year_subject_year_uid",
            //             column: x => x.subject_year_uid,
            //             principalSchema: "EDU",
            //             principalTable: "subject_year",
            //             principalColumn: "subject_year_uid",
            //             onDelete: ReferentialAction.Restrict);
            //     });
            //
            // migrationBuilder.CreateTable(
            //     name: "subject_year_topic",
            //     schema: "EDU",
            //     columns: table => new
            //     {
            //         subject_year_topic_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
            //         status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         topic_code = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
            //         topic_name_th = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         topic_name_en = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         topic_credit = table.Column<decimal>(type: "decimal(5,1)", nullable: true),
            //         subject_year_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         subject_seq_code = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true)
            //     },
            //     constraints: table =>
            //     {
            //         table.PrimaryKey("PK_subject_year_topic", x => x.subject_year_topic_uid);
            //         table.ForeignKey(
            //             name: "FK_subject_year_topic_subject_year_subject_year_uid",
            //             column: x => x.subject_year_uid,
            //             principalSchema: "EDU",
            //             principalTable: "subject_year",
            //             principalColumn: "subject_year_uid",
            //             onDelete: ReferentialAction.Restrict);
            //     });
            //
            // migrationBuilder.CreateTable(
            //     name: "subject_master_year_cost",
            //     schema: "EDU",
            //     columns: table => new
            //     {
            //         subject_master_year_cost_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
            //         subject_master_year_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         education_type_code = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
            //         lecture_amount = table.Column<decimal>(type: "DECIMAL(18,4)", precision: 18, scale: 4, nullable: true),
            //         lab_amount = table.Column<decimal>(type: "DECIMAL(18,4)", precision: 18, scale: 4, nullable: true),
            //         practice_amount = table.Column<decimal>(type: "DECIMAL(18,4)", precision: 18, scale: 4, nullable: true)
            //     },
            //     constraints: table =>
            //     {
            //         table.PrimaryKey("PK_subject_master_year_cost", x => x.subject_master_year_cost_uid);
            //         table.ForeignKey(
            //             name: "FK_subject_master_year_cost_subject_master_year_subject_master_year_uid",
            //             column: x => x.subject_master_year_uid,
            //             principalSchema: "EDU",
            //             principalTable: "subject_master_year",
            //             principalColumn: "subject_master_year_uid",
            //             onDelete: ReferentialAction.Restrict);
            //     });
            //
            // migrationBuilder.CreateTable(
            //     name: "subject_master_year_topic",
            //     schema: "EDU",
            //     columns: table => new
            //     {
            //         subject_master_year_topic_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
            //         subject_master_year_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         topic_code = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
            //         topic_name_th = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         topic_name_en = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         topic_credit = table.Column<short>(type: "NUMBER(5)", nullable: true)
            //     },
            //     constraints: table =>
            //     {
            //         table.PrimaryKey("PK_subject_master_year_topic", x => x.subject_master_year_topic_uid);
            //         table.ForeignKey(
            //             name: "FK_subject_master_year_topic_subject_master_year_subject_master_year_uid",
            //             column: x => x.subject_master_year_uid,
            //             principalSchema: "EDU",
            //             principalTable: "subject_master_year",
            //             principalColumn: "subject_master_year_uid",
            //             onDelete: ReferentialAction.Restrict);
            //     });
            //
            // migrationBuilder.CreateTable(
            //     name: "year_subject_section_day",
            //     schema: "EDU",
            //     columns: table => new
            //     {
            //         year_subject_section_day_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
            //         status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         year_subject_section_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         study_day_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         study_time_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         room_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         study_from_time = table.Column<TimeSpan>(type: "INTERVAL DAY(8) TO SECOND(7)", nullable: true),
            //         study_to_time = table.Column<TimeSpan>(type: "INTERVAL DAY(8) TO SECOND(7)", nullable: true),
            //         room_code = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
            //         study_time_text_en = table.Column<string>(type: "NVARCHAR2(2000)", nullable: true)
            //     },
            //     constraints: table =>
            //     {
            //         table.PrimaryKey("PK_year_subject_section_day", x => x.year_subject_section_day_uid);
            //         table.ForeignKey(
            //             name: "FK_year_subject_section_day_study_day_study_day_uid",
            //             column: x => x.study_day_uid,
            //             principalSchema: "EDU",
            //             principalTable: "study_day",
            //             principalColumn: "study_day_uid",
            //             onDelete: ReferentialAction.Restrict);
            //         table.ForeignKey(
            //             name: "FK_year_subject_section_day_study_time_study_time_uid",
            //             column: x => x.study_time_uid,
            //             principalSchema: "EDU",
            //             principalTable: "study_time",
            //             principalColumn: "study_time_uid",
            //             onDelete: ReferentialAction.Restrict);
            //         table.ForeignKey(
            //             name: "FK_year_subject_section_day_year_subject_section_year_subject_section_uid",
            //             column: x => x.year_subject_section_uid,
            //             principalSchema: "EDU",
            //             principalTable: "year_subject_section",
            //             principalColumn: "year_subject_section_uid",
            //             onDelete: ReferentialAction.Restrict);
            //     });
            //
            // migrationBuilder.CreateTable(
            //     name: "year_subject_section_faculty",
            //     schema: "EDU",
            //     columns: table => new
            //     {
            //         year_subject_section_faculty_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
            //         status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         year_subject_section_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         college_faculty_uid = table.Column<Guid>(type: "RAW(16)", nullable: true)
            //     },
            //     constraints: table =>
            //     {
            //         table.PrimaryKey("PK_year_subject_section_faculty", x => x.year_subject_section_faculty_uid);
            //         table.ForeignKey(
            //             name: "FK_year_subject_section_faculty_year_subject_section_year_subject_section_uid",
            //             column: x => x.year_subject_section_uid,
            //             principalSchema: "EDU",
            //             principalTable: "year_subject_section",
            //             principalColumn: "year_subject_section_uid",
            //             onDelete: ReferentialAction.Restrict);
            //     });
            //
            // migrationBuilder.CreateTable(
            //     name: "year_subject_section_instructor",
            //     schema: "EDU",
            //     columns: table => new
            //     {
            //         year_subject_section_instructor_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
            //         status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         year_subject_section_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         instructor_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         responsible_percent = table.Column<decimal>(type: "DECIMAL(5,2)", precision: 5, scale: 2, nullable: true),
            //         remark = table.Column<string>(type: "NVARCHAR2(1000)", maxLength: 1000, nullable: true)
            //     },
            //     constraints: table =>
            //     {
            //         table.PrimaryKey("PK_year_subject_section_instructor", x => x.year_subject_section_instructor_uid);
            //         table.ForeignKey(
            //             name: "FK_year_subject_section_instructor_year_subject_section_year_subject_section_uid",
            //             column: x => x.year_subject_section_uid,
            //             principalSchema: "EDU",
            //             principalTable: "year_subject_section",
            //             principalColumn: "year_subject_section_uid",
            //             onDelete: ReferentialAction.Restrict);
            //     });
            //
            // migrationBuilder.CreateTable(
            //     name: "year_subject_section_year",
            //     schema: "EDU",
            //     columns: table => new
            //     {
            //         year_subject_section_year_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
            //         status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         year_subject_section_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         entry_academic_year_uid = table.Column<Guid>(type: "RAW(16)", nullable: true)
            //     },
            //     constraints: table =>
            //     {
            //         table.PrimaryKey("PK_year_subject_section_year", x => x.year_subject_section_year_uid);
            //         table.ForeignKey(
            //             name: "FK_year_subject_section_year_year_subject_section_year_subject_section_uid",
            //             column: x => x.year_subject_section_uid,
            //             principalSchema: "EDU",
            //             principalTable: "year_subject_section",
            //             principalColumn: "year_subject_section_uid",
            //             onDelete: ReferentialAction.Restrict);
            //     });
            //
            // migrationBuilder.CreateTable(
            //     name: "curriculum_subject",
            //     schema: "EDU",
            //     columns: table => new
            //     {
            //         curriculum_subject_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
            //         status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         curriculum_subject_group_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         subject_uid = table.Column<Guid>(type: "RAW(16)", nullable: true)
            //     },
            //     constraints: table =>
            //     {
            //         table.PrimaryKey("PK_curriculum_subject", x => x.curriculum_subject_uid);
            //         table.ForeignKey(
            //             name: "FK_curriculum_subject_curriculum_subject_group_curriculum_subject_group_uid",
            //             column: x => x.curriculum_subject_group_uid,
            //             principalSchema: "EDU",
            //             principalTable: "curriculum_subject_group",
            //             principalColumn: "curriculum_subject_group_uid",
            //             onDelete: ReferentialAction.Cascade);
            //     });
            //
            // migrationBuilder.CreateTable(
            //     name: "discipline_condition_detail_sub",
            //     schema: "EDU",
            //     columns: table => new
            //     {
            //         discipline_condition_detail_sub_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
            //         status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         discipline_condition_detail_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         detail_sub_section_code = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
            //         detail_sub_section_name_th = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         detail_sub_section_name_en = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         row_order = table.Column<int>(type: "NUMBER(10)", nullable: true)
            //     },
            //     constraints: table =>
            //     {
            //         table.PrimaryKey("PK_discipline_condition_detail_sub", x => x.discipline_condition_detail_sub_uid);
            //         table.ForeignKey(
            //             name: "FK_discipline_condition_detail_sub_discipline_condition_detail_discipline_condition_detail_uid",
            //             column: x => x.discipline_condition_detail_uid,
            //             principalSchema: "EDU",
            //             principalTable: "discipline_condition_detail",
            //             principalColumn: "discipline_condition_detail_uid",
            //             onDelete: ReferentialAction.Cascade);
            //     });
            //
            // migrationBuilder.CreateTable(
            //     name: "register_fee_detail",
            //     schema: "EDU",
            //     columns: table => new
            //     {
            //         register_fee_detail_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
            //         status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         register_fee_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         fee_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         fee_amount = table.Column<decimal>(type: "DECIMAL(18,4)", precision: 18, scale: 4, nullable: true),
            //         register_fee_filter_uid = table.Column<Guid>(type: "RAW(16)", nullable: true)
            //     },
            //     constraints: table =>
            //     {
            //         table.PrimaryKey("PK_register_fee_detail", x => x.register_fee_detail_uid);
            //         table.ForeignKey(
            //             name: "FK_register_fee_detail_register_fee_filter_register_fee_filter_uid",
            //             column: x => x.register_fee_filter_uid,
            //             principalSchema: "EDU",
            //             principalTable: "register_fee_filter",
            //             principalColumn: "register_fee_filter_uid",
            //             onDelete: ReferentialAction.Cascade);
            //         table.ForeignKey(
            //             name: "FK_register_fee_detail_register_fee_register_fee_uid",
            //             column: x => x.register_fee_uid,
            //             principalSchema: "EDU",
            //             principalTable: "register_fee",
            //             principalColumn: "register_fee_uid",
            //             onDelete: ReferentialAction.Cascade);
            //     });
            //
            // migrationBuilder.CreateTable(
            //     name: "register_fee_filter_curriculum",
            //     schema: "EDU",
            //     columns: table => new
            //     {
            //         register_fee_filter_curriculum_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
            //         status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         register_fee_filter_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         curriculum_uid = table.Column<Guid>(type: "RAW(16)", nullable: true)
            //     },
            //     constraints: table =>
            //     {
            //         table.PrimaryKey("PK_register_fee_filter_curriculum", x => x.register_fee_filter_curriculum_uid);
            //         table.ForeignKey(
            //             name: "FK_register_fee_filter_curriculum_register_fee_filter_register_fee_filter_uid",
            //             column: x => x.register_fee_filter_uid,
            //             principalSchema: "EDU",
            //             principalTable: "register_fee_filter",
            //             principalColumn: "register_fee_filter_uid",
            //             onDelete: ReferentialAction.Cascade);
            //     });
            //
            // migrationBuilder.CreateTable(
            //     name: "academic_semester_filter",
            //     schema: "EDU",
            //     columns: table => new
            //     {
            //         academic_semester_filter_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
            //         status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         academic_semester_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         academic_semester_filter_code = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
            //         academic_semester_filter_name_th = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         academic_semester_filter_name_en = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         academic_calendar_type_uid = table.Column<Guid>(type: "RAW(16)", nullable: true)
            //     },
            //     constraints: table =>
            //     {
            //         table.PrimaryKey("PK_academic_semester_filter", x => x.academic_semester_filter_uid);
            //         table.ForeignKey(
            //             name: "FK_academic_semester_filter_academic_semester_academic_semester_uid",
            //             column: x => x.academic_semester_uid,
            //             principalSchema: "EDU",
            //             principalTable: "academic_semester",
            //             principalColumn: "academic_semester_uid",
            //             onDelete: ReferentialAction.Cascade);
            //     });
            //
            // migrationBuilder.CreateTable(
            //     name: "student_discipline_detail",
            //     schema: "EDU",
            //     columns: table => new
            //     {
            //         student_discipline_detail_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
            //         t_discipline_statusdiscipline_status_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         student_discipline_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         discipline_condition_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         discipline_condition_detail_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         discipline_condition_detail_sub_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         deduct_score = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         action_date = table.Column<DateTime>(type: "date", nullable: true),
            //         action_time = table.Column<TimeSpan>(type: "INTERVAL DAY(8) TO SECOND(7)", nullable: true),
            //         discipline_punishment_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         remark = table.Column<string>(type: "NVARCHAR2(1000)", maxLength: 1000, nullable: true),
            //         action_by = table.Column<string>(type: "NVARCHAR2(2000)", nullable: true)
            //     },
            //     constraints: table =>
            //     {
            //         table.PrimaryKey("PK_student_discipline_detail", x => x.student_discipline_detail_uid);
            //         table.ForeignKey(
            //             name: "FK_student_discipline_detail_discipline_condition_detail_discipline_condition_detail_uid",
            //             column: x => x.discipline_condition_detail_uid,
            //             principalSchema: "EDU",
            //             principalTable: "discipline_condition_detail",
            //             principalColumn: "discipline_condition_detail_uid",
            //             onDelete: ReferentialAction.Restrict);
            //         table.ForeignKey(
            //             name: "FK_student_discipline_detail_discipline_condition_detail_sub_discipline_condition_detail_sub_uid",
            //             column: x => x.discipline_condition_detail_sub_uid,
            //             principalSchema: "EDU",
            //             principalTable: "discipline_condition_detail_sub",
            //             principalColumn: "discipline_condition_detail_sub_uid",
            //             onDelete: ReferentialAction.Restrict);
            //         table.ForeignKey(
            //             name: "FK_student_discipline_detail_discipline_condition_discipline_condition_uid",
            //             column: x => x.discipline_condition_uid,
            //             principalSchema: "EDU",
            //             principalTable: "discipline_condition",
            //             principalColumn: "discipline_condition_uid",
            //             onDelete: ReferentialAction.Restrict);
            //         table.ForeignKey(
            //             name: "FK_student_discipline_detail_discipline_punishment_discipline_punishment_uid",
            //             column: x => x.discipline_punishment_uid,
            //             principalSchema: "EDU",
            //             principalTable: "discipline_punishment",
            //             principalColumn: "discipline_punishment_uid",
            //             onDelete: ReferentialAction.Restrict);
            //         table.ForeignKey(
            //             name: "FK_student_discipline_detail_discipline_status_t_discipline_statusdiscipline_status_uid",
            //             column: x => x.t_discipline_statusdiscipline_status_uid,
            //             principalSchema: "EDU",
            //             principalTable: "discipline_status",
            //             principalColumn: "discipline_status_uid",
            //             onDelete: ReferentialAction.Restrict);
            //         table.ForeignKey(
            //             name: "FK_student_discipline_detail_student_discipline_student_discipline_uid",
            //             column: x => x.student_discipline_uid,
            //             principalSchema: "EDU",
            //             principalTable: "student_discipline",
            //             principalColumn: "student_discipline_uid",
            //             onDelete: ReferentialAction.Cascade);
            //     });
            //
            // migrationBuilder.CreateTable(
            //     name: "academic_calendar",
            //     schema: "EDU",
            //     columns: table => new
            //     {
            //         academic_calendar_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
            //         status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         academic_year_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         academic_semester_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         from_date = table.Column<DateTime>(type: "date", nullable: true),
            //         to_date = table.Column<DateTime>(type: "date", nullable: true),
            //         description = table.Column<string>(type: "NVARCHAR2(500)", maxLength: 500, nullable: true),
            //         remark = table.Column<string>(type: "NVARCHAR2(1000)", maxLength: 1000, nullable: true),
            //         academic_calendar_type_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         academic_semester_filter_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         education_type_uid = table.Column<Guid>(type: "RAW(16)", nullable: true)
            //     },
            //     constraints: table =>
            //     {
            //         table.PrimaryKey("PK_academic_calendar", x => x.academic_calendar_uid);
            //         table.ForeignKey(
            //             name: "FK_academic_calendar_academic_semester_academic_semester_uid",
            //             column: x => x.academic_semester_uid,
            //             principalSchema: "EDU",
            //             principalTable: "academic_semester",
            //             principalColumn: "academic_semester_uid",
            //             onDelete: ReferentialAction.Restrict);
            //         table.ForeignKey(
            //             name: "FK_academic_calendar_academic_semester_filter_academic_semester_filter_uid",
            //             column: x => x.academic_semester_filter_uid,
            //             principalSchema: "EDU",
            //             principalTable: "academic_semester_filter",
            //             principalColumn: "academic_semester_filter_uid",
            //             onDelete: ReferentialAction.Restrict);
            //     });
            //
            // migrationBuilder.CreateTable(
            //     name: "academic_register_calendar",
            //     schema: "EDU",
            //     columns: table => new
            //     {
            //         academic_register_calendar_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
            //         status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         academic_year_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         academic_semester_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         education_type_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         from_date = table.Column<DateTime>(type: "date", nullable: true),
            //         to_date = table.Column<DateTime>(type: "date", nullable: true),
            //         student_status_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         last_payment_date = table.Column<DateTime>(type: "date", nullable: true),
            //         description = table.Column<string>(type: "NVARCHAR2(500)", maxLength: 500, nullable: true),
            //         fine_rate_amount = table.Column<decimal>(type: "DECIMAL(18,4)", precision: 18, scale: 4, nullable: true),
            //         max_fine_amount = table.Column<decimal>(type: "DECIMAL(18,4)", precision: 18, scale: 4, nullable: true),
            //         min_credit = table.Column<decimal>(type: "decimal(5,2)", nullable: true),
            //         max_credit = table.Column<decimal>(type: "decimal(5,2)", nullable: true),
            //         academic_register_type_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         remark = table.Column<string>(type: "NVARCHAR2(1000)", maxLength: 1000, nullable: true),
            //         academic_semester_filter_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         cash_back = table.Column<decimal>(type: "decimal(5,2)", nullable: true),
            //         after_withdraw_status = table.Column<bool>(type: "NUMBER(1)", nullable: true),
            //         semester_uid = table.Column<Guid>(type: "RAW(16)", nullable: true)
            //     },
            //     constraints: table =>
            //     {
            //         table.PrimaryKey("PK_academic_register_calendar", x => x.academic_register_calendar_uid);
            //         table.ForeignKey(
            //             name: "FK_academic_register_calendar_academic_register_type_academic_register_type_uid",
            //             column: x => x.academic_register_type_uid,
            //             principalSchema: "EDU",
            //             principalTable: "academic_register_type",
            //             principalColumn: "academic_register_type_uid",
            //             onDelete: ReferentialAction.Restrict);
            //         table.ForeignKey(
            //             name: "FK_academic_register_calendar_academic_semester_academic_semester_uid",
            //             column: x => x.academic_semester_uid,
            //             principalSchema: "EDU",
            //             principalTable: "academic_semester",
            //             principalColumn: "academic_semester_uid",
            //             onDelete: ReferentialAction.Restrict);
            //         table.ForeignKey(
            //             name: "FK_academic_register_calendar_academic_semester_filter_academic_semester_filter_uid",
            //             column: x => x.academic_semester_filter_uid,
            //             principalSchema: "EDU",
            //             principalTable: "academic_semester_filter",
            //             principalColumn: "academic_semester_filter_uid",
            //             onDelete: ReferentialAction.Restrict);
            //     });
            //
            // migrationBuilder.CreateTable(
            //     name: "academic_semester_filter_curriculum",
            //     schema: "EDU",
            //     columns: table => new
            //     {
            //         academic_semester_filter_curriculum_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
            //         status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         academic_semester_filter_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         curriculum_uid = table.Column<Guid>(type: "RAW(16)", nullable: true)
            //     },
            //     constraints: table =>
            //     {
            //         table.PrimaryKey("PK_academic_semester_filter_curriculum", x => x.academic_semester_filter_curriculum_uid);
            //         table.ForeignKey(
            //             name: "FK_academic_semester_filter_curriculum_academic_semester_filter_academic_semester_filter_uid",
            //             column: x => x.academic_semester_filter_uid,
            //             principalSchema: "EDU",
            //             principalTable: "academic_semester_filter",
            //             principalColumn: "academic_semester_filter_uid",
            //             onDelete: ReferentialAction.Cascade);
            //     });
            //
            // migrationBuilder.CreateTable(
            //     name: "academic_semester_filter_education_type",
            //     schema: "EDU",
            //     columns: table => new
            //     {
            //         academic_semester_filter_education_type_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
            //         status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         academic_semester_filter_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         education_type_uid = table.Column<Guid>(type: "RAW(16)", nullable: true)
            //     },
            //     constraints: table =>
            //     {
            //         table.PrimaryKey("PK_academic_semester_filter_education_type", x => x.academic_semester_filter_education_type_uid);
            //         table.ForeignKey(
            //             name: "FK_academic_semester_filter_education_type_academic_semester_filter_academic_semester_filter_uid",
            //             column: x => x.academic_semester_filter_uid,
            //             principalSchema: "EDU",
            //             principalTable: "academic_semester_filter",
            //             principalColumn: "academic_semester_filter_uid",
            //             onDelete: ReferentialAction.Cascade);
            //     });
            //
            // migrationBuilder.CreateTable(
            //     name: "academic_semester_filter_faculty",
            //     schema: "EDU",
            //     columns: table => new
            //     {
            //         academic_semester_filter_faculty_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
            //         status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         academic_semester_filter_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         faculty_uid = table.Column<Guid>(type: "RAW(16)", nullable: true)
            //     },
            //     constraints: table =>
            //     {
            //         table.PrimaryKey("PK_academic_semester_filter_faculty", x => x.academic_semester_filter_faculty_uid);
            //         table.ForeignKey(
            //             name: "FK_academic_semester_filter_faculty_academic_semester_filter_academic_semester_filter_uid",
            //             column: x => x.academic_semester_filter_uid,
            //             principalSchema: "EDU",
            //             principalTable: "academic_semester_filter",
            //             principalColumn: "academic_semester_filter_uid",
            //             onDelete: ReferentialAction.Cascade);
            //     });
            //
            // migrationBuilder.CreateTable(
            //     name: "student_discipline_detail_file",
            //     schema: "EDU",
            //     columns: table => new
            //     {
            //         student_discipline_detail_file_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
            //         status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         student_discipline_detail_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         row_order = table.Column<int>(type: "NUMBER(10)", nullable: true),
            //         file_name = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         file_path = table.Column<string>(type: "NVARCHAR2(500)", maxLength: 500, nullable: true),
            //         action_by = table.Column<string>(type: "NVARCHAR2(2000)", nullable: true)
            //     },
            //     constraints: table =>
            //     {
            //         table.PrimaryKey("PK_student_discipline_detail_file", x => x.student_discipline_detail_file_uid);
            //         table.ForeignKey(
            //             name: "FK_student_discipline_detail_file_student_discipline_detail_student_discipline_detail_uid",
            //             column: x => x.student_discipline_detail_uid,
            //             principalSchema: "EDU",
            //             principalTable: "student_discipline_detail",
            //             principalColumn: "student_discipline_detail_uid",
            //             onDelete: ReferentialAction.Cascade);
            //     });
            //
            // migrationBuilder.CreateTable(
            //     name: "student_discipline_detail_photo",
            //     schema: "EDU",
            //     columns: table => new
            //     {
            //         student_discipline_detail_photo_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
            //         status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         student_discipline_detail_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         row_order = table.Column<int>(type: "NUMBER(10)", nullable: true),
            //         photo_name = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         photo_path = table.Column<string>(type: "NVARCHAR2(500)", maxLength: 500, nullable: true),
            //         action_by = table.Column<string>(type: "NVARCHAR2(2000)", nullable: true)
            //     },
            //     constraints: table =>
            //     {
            //         table.PrimaryKey("PK_student_discipline_detail_photo", x => x.student_discipline_detail_photo_uid);
            //         table.ForeignKey(
            //             name: "FK_student_discipline_detail_photo_student_discipline_detail_student_discipline_detail_uid",
            //             column: x => x.student_discipline_detail_uid,
            //             principalSchema: "EDU",
            //             principalTable: "student_discipline_detail",
            //             principalColumn: "student_discipline_detail_uid",
            //             onDelete: ReferentialAction.Cascade);
            //     });
            //
            // migrationBuilder.CreateIndex(
            //     name: "IX_academic_calendar_academic_semester_filter_uid",
            //     schema: "EDU",
            //     table: "academic_calendar",
            //     column: "academic_semester_filter_uid");
            //
            // migrationBuilder.CreateIndex(
            //     name: "IX_academic_calendar_academic_semester_uid",
            //     schema: "EDU",
            //     table: "academic_calendar",
            //     column: "academic_semester_uid");
            //
            // migrationBuilder.CreateIndex(
            //     name: "IX_academic_register_calendar_academic_register_type_uid",
            //     schema: "EDU",
            //     table: "academic_register_calendar",
            //     column: "academic_register_type_uid");
            //
            // migrationBuilder.CreateIndex(
            //     name: "IX_academic_register_calendar_academic_semester_filter_uid",
            //     schema: "EDU",
            //     table: "academic_register_calendar",
            //     column: "academic_semester_filter_uid");
            //
            // migrationBuilder.CreateIndex(
            //     name: "IX_academic_register_calendar_academic_semester_uid",
            //     schema: "EDU",
            //     table: "academic_register_calendar",
            //     column: "academic_semester_uid");
            //
            // migrationBuilder.CreateIndex(
            //     name: "IX_academic_semester_academic_year_filter_uid",
            //     schema: "EDU",
            //     table: "academic_semester",
            //     column: "academic_year_filter_uid");
            //
            // migrationBuilder.CreateIndex(
            //     name: "IX_academic_semester_academic_year_uid",
            //     schema: "EDU",
            //     table: "academic_semester",
            //     column: "academic_year_uid");
            //
            // migrationBuilder.CreateIndex(
            //     name: "IX_academic_semester_semester_uid",
            //     schema: "EDU",
            //     table: "academic_semester",
            //     column: "semester_uid");
            //
            // migrationBuilder.CreateIndex(
            //     name: "IX_academic_semester_filter_academic_semester_uid",
            //     schema: "EDU",
            //     table: "academic_semester_filter",
            //     column: "academic_semester_uid");
            //
            // migrationBuilder.CreateIndex(
            //     name: "IX_academic_semester_filter_curriculum_academic_semester_filter_uid",
            //     schema: "EDU",
            //     table: "academic_semester_filter_curriculum",
            //     column: "academic_semester_filter_uid");
            //
            // migrationBuilder.CreateIndex(
            //     name: "IX_academic_semester_filter_education_type_academic_semester_filter_uid",
            //     schema: "EDU",
            //     table: "academic_semester_filter_education_type",
            //     column: "academic_semester_filter_uid");
            //
            // migrationBuilder.CreateIndex(
            //     name: "IX_academic_semester_filter_faculty_academic_semester_filter_uid",
            //     schema: "EDU",
            //     table: "academic_semester_filter_faculty",
            //     column: "academic_semester_filter_uid");
            //
            // migrationBuilder.CreateIndex(
            //     name: "IX_academic_year_filter_academic_year_uid",
            //     schema: "EDU",
            //     table: "academic_year_filter",
            //     column: "academic_year_uid");
            //
            // migrationBuilder.CreateIndex(
            //     name: "IX_academic_year_filter_curriculum_academic_year_filter_uid",
            //     schema: "EDU",
            //     table: "academic_year_filter_curriculum",
            //     column: "academic_year_filter_uid");
            //
            // migrationBuilder.CreateIndex(
            //     name: "IX_academic_year_filter_education_type_academic_year_filter_uid",
            //     schema: "EDU",
            //     table: "academic_year_filter_education_type",
            //     column: "academic_year_filter_uid");
            //
            // migrationBuilder.CreateIndex(
            //     name: "IX_academic_year_filter_faculty_academic_year_filter_uid",
            //     schema: "EDU",
            //     table: "academic_year_filter_faculty",
            //     column: "academic_year_filter_uid");
            //
            // migrationBuilder.CreateIndex(
            //     name: "IX_company_address_uid",
            //     schema: "EDU",
            //     table: "company",
            //     column: "address_uid");
            //
            // migrationBuilder.CreateIndex(
            //     name: "IX_company_student_parent_uid",
            //     schema: "EDU",
            //     table: "company",
            //     column: "student_parent_uid");
            //
            // migrationBuilder.CreateIndex(
            //     name: "IX_curriculum_subject_curriculum_subject_group_uid",
            //     schema: "EDU",
            //     table: "curriculum_subject",
            //     column: "curriculum_subject_group_uid");
            //
            // migrationBuilder.CreateIndex(
            //     name: "IX_curriculum_subject_category_curriculum_uid",
            //     schema: "EDU",
            //     table: "curriculum_subject_category",
            //     column: "curriculum_uid");
            //
            // migrationBuilder.CreateIndex(
            //     name: "IX_curriculum_subject_group_curriculum_subject_category_uid",
            //     schema: "EDU",
            //     table: "curriculum_subject_group",
            //     column: "curriculum_subject_category_uid");
            //
            // migrationBuilder.CreateIndex(
            //     name: "IX_discipline_condition_discipline_year_uid",
            //     schema: "EDU",
            //     table: "discipline_condition",
            //     column: "discipline_year_uid");
            //
            // migrationBuilder.CreateIndex(
            //     name: "IX_discipline_condition_detail_discipline_condition_uid",
            //     schema: "EDU",
            //     table: "discipline_condition_detail",
            //     column: "discipline_condition_uid");
            //
            // migrationBuilder.CreateIndex(
            //     name: "IX_discipline_condition_detail_sub_discipline_condition_detail_uid",
            //     schema: "EDU",
            //     table: "discipline_condition_detail_sub",
            //     column: "discipline_condition_detail_uid");
            //
            // migrationBuilder.CreateIndex(
            //     name: "IX_discipline_score_status_discipline_period_type_uid",
            //     schema: "EDU",
            //     table: "discipline_score_status",
            //     column: "discipline_period_type_uid");
            //
            // migrationBuilder.CreateIndex(
            //     name: "IX_discipline_score_status_discipline_year_uid",
            //     schema: "EDU",
            //     table: "discipline_score_status",
            //     column: "discipline_year_uid");
            //
            // migrationBuilder.CreateIndex(
            //     name: "IX_dynamic_fee_education_type_dynamic_fee_apply_uid",
            //     schema: "EDU",
            //     table: "dynamic_fee_education_type",
            //     column: "dynamic_fee_apply_uid");
            //
            // migrationBuilder.CreateIndex(
            //     name: "IX_dynamic_fee_education_type_dynamic_fee_uid",
            //     schema: "EDU",
            //     table: "dynamic_fee_education_type",
            //     column: "dynamic_fee_uid");
            //
            // migrationBuilder.CreateIndex(
            //     name: "IX_education_plan_curriculum_education_plan_uid",
            //     schema: "EDU",
            //     table: "education_plan_curriculum",
            //     column: "education_plan_uid");
            //
            // migrationBuilder.CreateIndex(
            //     name: "IX_education_plan_fee_education_plan_uid",
            //     schema: "EDU",
            //     table: "education_plan_fee",
            //     column: "education_plan_uid");
            //
            // migrationBuilder.CreateIndex(
            //     name: "IX_education_plan_subject_education_plan_uid",
            //     schema: "EDU",
            //     table: "education_plan_subject",
            //     column: "education_plan_uid");
            //
            // migrationBuilder.CreateIndex(
            //     name: "IX_education_type_fee_fee_uid",
            //     schema: "EDU",
            //     table: "education_type_fee",
            //     column: "fee_uid");
            //
            // migrationBuilder.CreateIndex(
            //     name: "IX_education_type_fee_condition_education_type_fee_uid",
            //     schema: "EDU",
            //     table: "education_type_fee_condition",
            //     column: "education_type_fee_uid");
            //
            // migrationBuilder.CreateIndex(
            //     name: "IX_education_type_list_dynamic_fee_education_type_uid",
            //     schema: "EDU",
            //     table: "education_type_list",
            //     column: "dynamic_fee_education_type_uid");
            //
            // migrationBuilder.CreateIndex(
            //     name: "IX_evaluate_subject_section_instructor_evaluate_subject_section_uid",
            //     schema: "EDU",
            //     table: "evaluate_subject_section_instructor",
            //     column: "evaluate_subject_section_uid");
            //
            // migrationBuilder.CreateIndex(
            //     name: "IX_evaluation_calendar_faculty_evaluation_calendar_uid",
            //     schema: "EDU",
            //     table: "evaluation_calendar_faculty",
            //     column: "evaluation_calendar_uid");
            //
            // migrationBuilder.CreateIndex(
            //     name: "IX_evaluation_calendar_faculty_curriculum_evaluation_calendar_faculty_uid",
            //     schema: "EDU",
            //     table: "evaluation_calendar_faculty_curriculum",
            //     column: "evaluation_calendar_faculty_uid");
            //
            // migrationBuilder.CreateIndex(
            //     name: "IX_evaluation_calendar_schedule_evaluation_calendar_item_uid",
            //     schema: "EDU",
            //     table: "evaluation_calendar_schedule",
            //     column: "evaluation_calendar_item_uid");
            //
            // migrationBuilder.CreateIndex(
            //     name: "IX_evaluation_calendar_schedule_evaluation_calendar_uid",
            //     schema: "EDU",
            //     table: "evaluation_calendar_schedule",
            //     column: "evaluation_calendar_uid");
            //
            // migrationBuilder.CreateIndex(
            //     name: "IX_final_register_subject_final_register_uid",
            //     schema: "EDU",
            //     table: "final_register_subject",
            //     column: "final_register_uid");
            //
            // migrationBuilder.CreateIndex(
            //     name: "IX_grade_criteria_option_grade_criteria_uid",
            //     schema: "EDU",
            //     table: "grade_criteria_option",
            //     column: "grade_criteria_uid");
            //
            // migrationBuilder.CreateIndex(
            //     name: "IX_plan_register_fee_plan_register_uid",
            //     schema: "EDU",
            //     table: "plan_register_fee",
            //     column: "plan_register_uid");
            //
            // migrationBuilder.CreateIndex(
            //     name: "IX_plan_register_subject_plan_register_uid",
            //     schema: "EDU",
            //     table: "plan_register_subject",
            //     column: "plan_register_uid");
            //
            // migrationBuilder.CreateIndex(
            //     name: "IX_probation_retire_condition_detail_probation_retire_condition_uid",
            //     schema: "EDU",
            //     table: "probation_retire_condition_detail",
            //     column: "probation_retire_condition_uid");
            //
            // migrationBuilder.CreateIndex(
            //     name: "IX_provision_register_advance_provision_register_uid",
            //     schema: "EDU",
            //     table: "provision_register_advance",
            //     column: "provision_register_uid");
            //
            // migrationBuilder.CreateIndex(
            //     name: "IX_provision_register_fee_provision_register_uid",
            //     schema: "EDU",
            //     table: "provision_register_fee",
            //     column: "provision_register_uid");
            //
            // migrationBuilder.CreateIndex(
            //     name: "IX_provision_register_scholarship_provision_register_uid",
            //     schema: "EDU",
            //     table: "provision_register_scholarship",
            //     column: "provision_register_uid");
            //
            // migrationBuilder.CreateIndex(
            //     name: "IX_provision_register_subject_provision_register_uid",
            //     schema: "EDU",
            //     table: "provision_register_subject",
            //     column: "provision_register_uid");
            //
            // migrationBuilder.CreateIndex(
            //     name: "IX_question_answer_type_uid",
            //     schema: "EDU",
            //     table: "question",
            //     column: "answer_type_uid");
            //
            // migrationBuilder.CreateIndex(
            //     name: "IX_question_question_set_uid",
            //     schema: "EDU",
            //     table: "question",
            //     column: "question_set_uid");
            //
            // migrationBuilder.CreateIndex(
            //     name: "IX_question_set_question_group_uid",
            //     schema: "EDU",
            //     table: "question_set",
            //     column: "question_group_uid");
            //
            // migrationBuilder.CreateIndex(
            //     name: "IX_question_set_curriculum_question_set_uid",
            //     schema: "EDU",
            //     table: "question_set_curriculum",
            //     column: "question_set_uid");
            //
            // migrationBuilder.CreateIndex(
            //     name: "IX_question_set_faculty_question_set_uid",
            //     schema: "EDU",
            //     table: "question_set_faculty",
            //     column: "question_set_uid");
            //
            // migrationBuilder.CreateIndex(
            //     name: "IX_question_set_subject_question_set_uid",
            //     schema: "EDU",
            //     table: "question_set_subject",
            //     column: "question_set_uid");
            //
            // migrationBuilder.CreateIndex(
            //     name: "IX_register_fee_register_type_uid",
            //     schema: "EDU",
            //     table: "register_fee",
            //     column: "register_type_uid");
            //
            // migrationBuilder.CreateIndex(
            //     name: "IX_register_fee_detail_register_fee_filter_uid",
            //     schema: "EDU",
            //     table: "register_fee_detail",
            //     column: "register_fee_filter_uid");
            //
            // migrationBuilder.CreateIndex(
            //     name: "IX_register_fee_detail_register_fee_uid",
            //     schema: "EDU",
            //     table: "register_fee_detail",
            //     column: "register_fee_uid");
            //
            // migrationBuilder.CreateIndex(
            //     name: "IX_register_fee_filter_register_fee_uid",
            //     schema: "EDU",
            //     table: "register_fee_filter",
            //     column: "register_fee_uid");
            //
            // migrationBuilder.CreateIndex(
            //     name: "IX_register_fee_filter_curriculum_register_fee_filter_uid",
            //     schema: "EDU",
            //     table: "register_fee_filter_curriculum",
            //     column: "register_fee_filter_uid");
            //
            // migrationBuilder.CreateIndex(
            //     name: "IX_semester_semester_type_uid",
            //     schema: "EDU",
            //     table: "semester",
            //     column: "semester_type_uid");
            //
            // migrationBuilder.CreateIndex(
            //     name: "IX_student_address_student_uid",
            //     schema: "EDU",
            //     table: "student_address",
            //     column: "student_uid");
            //
            // migrationBuilder.CreateIndex(
            //     name: "IX_student_discipline_detail_discipline_condition_detail_sub_uid",
            //     schema: "EDU",
            //     table: "student_discipline_detail",
            //     column: "discipline_condition_detail_sub_uid");
            //
            // migrationBuilder.CreateIndex(
            //     name: "IX_student_discipline_detail_discipline_condition_detail_uid",
            //     schema: "EDU",
            //     table: "student_discipline_detail",
            //     column: "discipline_condition_detail_uid");
            //
            // migrationBuilder.CreateIndex(
            //     name: "IX_student_discipline_detail_discipline_condition_uid",
            //     schema: "EDU",
            //     table: "student_discipline_detail",
            //     column: "discipline_condition_uid");
            //
            // migrationBuilder.CreateIndex(
            //     name: "IX_student_discipline_detail_discipline_punishment_uid",
            //     schema: "EDU",
            //     table: "student_discipline_detail",
            //     column: "discipline_punishment_uid");
            //
            // migrationBuilder.CreateIndex(
            //     name: "IX_student_discipline_detail_student_discipline_uid",
            //     schema: "EDU",
            //     table: "student_discipline_detail",
            //     column: "student_discipline_uid");
            //
            // migrationBuilder.CreateIndex(
            //     name: "IX_student_discipline_detail_t_discipline_statusdiscipline_status_uid",
            //     schema: "EDU",
            //     table: "student_discipline_detail",
            //     column: "t_discipline_statusdiscipline_status_uid");
            //
            // migrationBuilder.CreateIndex(
            //     name: "IX_student_discipline_detail_file_student_discipline_detail_uid",
            //     schema: "EDU",
            //     table: "student_discipline_detail_file",
            //     column: "student_discipline_detail_uid");
            //
            // migrationBuilder.CreateIndex(
            //     name: "IX_student_discipline_detail_photo_student_discipline_detail_uid",
            //     schema: "EDU",
            //     table: "student_discipline_detail_photo",
            //     column: "student_discipline_detail_uid");
            //
            // migrationBuilder.CreateIndex(
            //     name: "IX_student_discipline_exam_discipline_punishment_uid1",
            //     schema: "EDU",
            //     table: "student_discipline_exam",
            //     column: "discipline_punishment_uid1");
            //
            // migrationBuilder.CreateIndex(
            //     name: "IX_student_discipline_exam_student_discipline_uid",
            //     schema: "EDU",
            //     table: "student_discipline_exam",
            //     column: "student_discipline_uid");
            //
            // migrationBuilder.CreateIndex(
            //     name: "IX_student_discipline_score_student_discipline_uid",
            //     schema: "EDU",
            //     table: "student_discipline_score",
            //     column: "student_discipline_uid");
            //
            // migrationBuilder.CreateIndex(
            //     name: "IX_student_document_student_uid",
            //     schema: "EDU",
            //     table: "student_document",
            //     column: "student_uid");
            //
            // migrationBuilder.CreateIndex(
            //     name: "IX_student_evaluation_question_result_student_evaluation_question_set_uid",
            //     schema: "EDU",
            //     table: "student_evaluation_question_result",
            //     column: "student_evaluation_question_set_uid");
            //
            // migrationBuilder.CreateIndex(
            //     name: "IX_student_evaluation_question_set_student_evaluation_uid",
            //     schema: "EDU",
            //     table: "student_evaluation_question_set",
            //     column: "student_evaluation_uid");
            //
            // migrationBuilder.CreateIndex(
            //     name: "IX_student_group_group_uid1",
            //     schema: "EDU",
            //     table: "student_group",
            //     column: "group_uid1");
            //
            // migrationBuilder.CreateIndex(
            //     name: "IX_student_group_t_groupgroup_uid",
            //     schema: "EDU",
            //     table: "student_group",
            //     column: "t_groupgroup_uid");
            //
            // migrationBuilder.CreateIndex(
            //     name: "IX_student_illness_file_student_illness_uid",
            //     schema: "EDU",
            //     table: "student_illness_file",
            //     column: "student_illness_uid");
            //
            // migrationBuilder.CreateIndex(
            //     name: "IX_student_illness_photo_student_illness_uid",
            //     schema: "EDU",
            //     table: "student_illness_photo",
            //     column: "student_illness_uid");
            //
            // migrationBuilder.CreateIndex(
            //     name: "IX_student_parent_student_uid",
            //     schema: "EDU",
            //     table: "student_parent",
            //     column: "student_uid");
            //
            // migrationBuilder.CreateIndex(
            //     name: "IX_student_service_activity_student_discipline_uid",
            //     schema: "EDU",
            //     table: "student_service_activity",
            //     column: "student_discipline_uid");
            //
            // migrationBuilder.CreateIndex(
            //     name: "IX_student_status_restriction_student_restriction_uid",
            //     schema: "EDU",
            //     table: "student_status_restriction",
            //     column: "student_restriction_uid");
            //
            // migrationBuilder.CreateIndex(
            //     name: "IX_student_status_restriction_student_status_uid",
            //     schema: "EDU",
            //     table: "student_status_restriction",
            //     column: "student_status_uid");
            //
            // migrationBuilder.CreateIndex(
            //     name: "IX_subject_master_owner_subject_master_uid",
            //     schema: "EDU",
            //     table: "subject_master_owner",
            //     column: "subject_master_uid");
            //
            // migrationBuilder.CreateIndex(
            //     name: "IX_subject_master_year_subject_master_uid",
            //     schema: "EDU",
            //     table: "subject_master_year",
            //     column: "subject_master_uid");
            //
            // migrationBuilder.CreateIndex(
            //     name: "IX_subject_master_year_cost_subject_master_year_uid",
            //     schema: "EDU",
            //     table: "subject_master_year_cost",
            //     column: "subject_master_year_uid");
            //
            // migrationBuilder.CreateIndex(
            //     name: "IX_subject_master_year_topic_subject_master_year_uid",
            //     schema: "EDU",
            //     table: "subject_master_year_topic",
            //     column: "subject_master_year_uid");
            //
            // migrationBuilder.CreateIndex(
            //     name: "IX_subject_owner_subject_uid",
            //     schema: "EDU",
            //     table: "subject_owner",
            //     column: "subject_uid");
            //
            // migrationBuilder.CreateIndex(
            //     name: "IX_subject_topic_subject_uid",
            //     schema: "EDU",
            //     table: "subject_topic",
            //     column: "subject_uid");
            //
            // migrationBuilder.CreateIndex(
            //     name: "IX_subject_year_subject_uid",
            //     schema: "EDU",
            //     table: "subject_year",
            //     column: "subject_uid");
            //
            // migrationBuilder.CreateIndex(
            //     name: "IX_subject_year_co_subject_year_uid",
            //     schema: "EDU",
            //     table: "subject_year_co",
            //     column: "subject_year_uid");
            //
            // migrationBuilder.CreateIndex(
            //     name: "IX_subject_year_cost_subject_year_uid",
            //     schema: "EDU",
            //     table: "subject_year_cost",
            //     column: "subject_year_uid");
            //
            // migrationBuilder.CreateIndex(
            //     name: "IX_subject_year_pre_subject_year_uid",
            //     schema: "EDU",
            //     table: "subject_year_pre",
            //     column: "subject_year_uid");
            //
            // migrationBuilder.CreateIndex(
            //     name: "IX_subject_year_topic_subject_year_uid",
            //     schema: "EDU",
            //     table: "subject_year_topic",
            //     column: "subject_year_uid");
            //
            // migrationBuilder.CreateIndex(
            //     name: "IX_year_subject_exam_year_subject_uid",
            //     schema: "EDU",
            //     table: "year_subject_exam",
            //     column: "year_subject_uid");
            //
            // migrationBuilder.CreateIndex(
            //     name: "IX_year_subject_section_year_subject_uid",
            //     schema: "EDU",
            //     table: "year_subject_section",
            //     column: "year_subject_uid");
            //
            // migrationBuilder.CreateIndex(
            //     name: "IX_year_subject_section_day_study_day_uid",
            //     schema: "EDU",
            //     table: "year_subject_section_day",
            //     column: "study_day_uid");
            //
            // migrationBuilder.CreateIndex(
            //     name: "IX_year_subject_section_day_study_time_uid",
            //     schema: "EDU",
            //     table: "year_subject_section_day",
            //     column: "study_time_uid");
            //
            // migrationBuilder.CreateIndex(
            //     name: "IX_year_subject_section_day_year_subject_section_uid",
            //     schema: "EDU",
            //     table: "year_subject_section_day",
            //     column: "year_subject_section_uid");
            //
            // migrationBuilder.CreateIndex(
            //     name: "IX_year_subject_section_faculty_year_subject_section_uid",
            //     schema: "EDU",
            //     table: "year_subject_section_faculty",
            //     column: "year_subject_section_uid");
            //
            // migrationBuilder.CreateIndex(
            //     name: "IX_year_subject_section_instructor_year_subject_section_uid",
            //     schema: "EDU",
            //     table: "year_subject_section_instructor",
            //     column: "year_subject_section_uid");
            //
            // migrationBuilder.CreateIndex(
            //     name: "IX_year_subject_section_year_year_subject_section_uid",
            //     schema: "EDU",
            //     table: "year_subject_section_year",
            //     column: "year_subject_section_uid");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "academic_calendar",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "academic_calendar_type",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "academic_register_calendar",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "academic_semester_filter_curriculum",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "academic_semester_filter_education_type",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "academic_semester_filter_faculty",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "academic_year_filter_curriculum",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "academic_year_filter_education_type",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "academic_year_filter_faculty",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "address_type",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "advance_payment",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "advance_status",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "advance_type",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "attached_document",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "calendar",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "cause_failed",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "college_faculty",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "company",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "curriculum_subject",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "curriculum_year",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "discipline_score_status",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "education_check_document",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "education_check_document_status",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "education_plan_curriculum",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "education_plan_fee",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "education_plan_subject",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "education_type",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "education_type_fee_condition",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "education_type_level",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "education_type_list",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "employee",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "entry_program",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "entry_type",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "evaluate_status",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "evaluate_subject_section_instructor",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "evaluation_calendar_assignment",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "evaluation_calendar_faculty_curriculum",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "evaluation_calendar_schedule",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "faculty_curriculum",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "family_status",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "final_register_subject",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "grade",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "grade_calculation_method",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "grade_calculation_setting",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "grade_calendar",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "grade_criteria_option",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "grade_re_evaluation",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "grade_reason",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "grade_recorder",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "honor_status",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "hospital",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "income_level",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "loan",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "loan_semester",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "major",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "parent_type",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "plan_register_fee",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "plan_register_subject",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "probation_retire_condition_detail",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "provision_register_advance",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "provision_register_fee",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "provision_register_scholarship",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "provision_register_subject",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "question",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "question_score_type",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "question_set_curriculum",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "question_set_faculty",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "question_set_subject",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "re_evaluation_type",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "register_fee_detail",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "register_fee_filter_curriculum",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "register_token",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "required_document",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "school",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "school_level",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "score_calculation_type",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "section_type",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "status",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "step",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "student_address",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "student_card_record",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "student_discipline_detail_file",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "student_discipline_detail_photo",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "student_discipline_exam",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "student_discipline_score",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "student_document",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "student_education_check",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "student_education_check_status",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "student_evaluation_question_result",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "student_evaluation_status",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "student_group",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "student_illness_file",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "student_illness_photo",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "student_log",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "student_service_activity",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "student_status_restriction",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "student_type",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "study_type",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "subject_master_owner",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "subject_master_year_cost",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "subject_master_year_topic",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "subject_owner",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "subject_topic",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "subject_year_co",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "subject_year_cost",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "subject_year_pre",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "subject_year_topic",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "support_by",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "support_per_month",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "year_subject_exam",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "year_subject_grade_criteria_option",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "year_subject_section_day",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "year_subject_section_faculty",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "year_subject_section_instructor",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "year_subject_section_year",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "year_subject_waiting_list",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "academic_register_type",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "academic_semester_filter",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "address",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "student_parent",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "curriculum_subject_group",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "discipline_period_type",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "education_plan",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "education_type_fee",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "dynamic_fee_education_type",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "evaluate_subject_section",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "evaluation_calendar_faculty",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "evaluation_calendar_item",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "final_register",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "grade_criteria",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "plan_register",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "probation_retire_condition",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "provision_register",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "answer_type",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "question_set",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "register_fee_filter",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "student_discipline_detail",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "student_evaluation_question_set",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "group",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "student_illness",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "student_restriction",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "student_status",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "subject_master_year",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "subject_year",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "study_day",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "study_time",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "year_subject_section",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "academic_semester",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "student",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "curriculum_subject_category",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "fee",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "dynamic_fee_apply",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "dynamic_fee",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "evaluation_calendar",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "question_group",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "register_fee",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "discipline_condition_detail_sub",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "discipline_punishment",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "discipline_status",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "student_discipline",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "student_evaluation",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "subject_master",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "subject",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "year_subject",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "academic_year_filter",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "semester",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "curriculum",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "register_type",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "discipline_condition_detail",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "academic_year",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "semester_type",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "discipline_condition",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "discipline_year",
                schema: "EDU");
        }
    }
}
