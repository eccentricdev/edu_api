﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace edu_api.Migrations
{
    public partial class UpdateStudentFlag : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "student_status_name_th",
                schema: "EDU",
                table: "student_flag_type",
                newName: "student_flag_name_th");

            migrationBuilder.RenameColumn(
                name: "student_status_name_en",
                schema: "EDU",
                table: "student_flag_type",
                newName: "student_flag_name_en");

            migrationBuilder.RenameColumn(
                name: "student_status_code",
                schema: "EDU",
                table: "student_flag_type",
                newName: "student_flag_code");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "student_flag_name_th",
                schema: "EDU",
                table: "student_flag_type",
                newName: "student_status_name_th");

            migrationBuilder.RenameColumn(
                name: "student_flag_name_en",
                schema: "EDU",
                table: "student_flag_type",
                newName: "student_status_name_en");

            migrationBuilder.RenameColumn(
                name: "student_flag_code",
                schema: "EDU",
                table: "student_flag_type",
                newName: "student_status_code");
        }
    }
}
