﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace edu_api.Migrations
{
    public partial class Addagency_uidtocollege_faculty : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<Guid>(
                name: "agency_uid",
                schema: "EDU",
                table: "college_faculty",
                type: "RAW(16)",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "agency_uid",
                schema: "EDU",
                table: "college_faculty");
        }
    }
}
