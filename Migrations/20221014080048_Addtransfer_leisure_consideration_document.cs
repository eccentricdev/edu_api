﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace edu_api.Migrations
{
    public partial class Addtransfer_leisure_consideration_document : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "transfer_leisure_consideration_document",
                schema: "EDU",
                columns: table => new
                {
                    transfer_leisure_consideration_document_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
                    status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
                    created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
                    updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
                    owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
                    transfer_leisure_request_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
                    request_document_type_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
                    request_document_type_name = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    file_name = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    document_name = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    document_url = table.Column<string>(type: "NVARCHAR2(500)", maxLength: 500, nullable: true),
                    admin_file_name = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    admin_document_url = table.Column<string>(type: "NVARCHAR2(500)", maxLength: 500, nullable: true),
                    mime_type = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
                    document_status_id = table.Column<int>(type: "NUMBER(10)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_transfer_leisure_consideration_document", x => x.transfer_leisure_consideration_document_uid);
                    table.ForeignKey(
                        name: "FK_transfer_leisure_consideration_document_request_document_type_request_document_type_uid",
                        column: x => x.request_document_type_uid,
                        principalSchema: "EDU",
                        principalTable: "request_document_type",
                        principalColumn: "request_document_type_uid",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_transfer_leisure_consideration_document_transfer_leisure_request_transfer_leisure_request_uid",
                        column: x => x.transfer_leisure_request_uid,
                        principalSchema: "EDU",
                        principalTable: "transfer_leisure_request",
                        principalColumn: "transfer_leisure_request_uid",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_transfer_leisure_consideration_document_request_document_type_uid",
                schema: "EDU",
                table: "transfer_leisure_consideration_document",
                column: "request_document_type_uid");

            migrationBuilder.CreateIndex(
                name: "IX_transfer_leisure_consideration_document_transfer_leisure_request_uid",
                schema: "EDU",
                table: "transfer_leisure_consideration_document",
                column: "transfer_leisure_request_uid");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "transfer_leisure_consideration_document",
                schema: "EDU");
        }
    }
}
