﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace edu_api.Migrations
{
    public partial class Changeresponsibleperson : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "is_responsible_instructor",
                schema: "EDU",
                table: "year_subject_section",
                newName: "responsible_instructor_uid");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "responsible_instructor_uid",
                schema: "EDU",
                table: "year_subject_section",
                newName: "is_responsible_instructor");
        }
    }
}
