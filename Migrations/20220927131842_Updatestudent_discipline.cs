﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace edu_api.Migrations
{
    public partial class Updatestudent_discipline : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "case_owner_teacher_name",
                schema: "EDU",
                table: "student_service_activity",
                type: "NVARCHAR2(200)",
                maxLength: 200,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "case_owner_teacher_name",
                schema: "EDU",
                table: "student_discipline_exam",
                type: "NVARCHAR2(200)",
                maxLength: 200,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "case_owner_teacher_name",
                schema: "EDU",
                table: "student_discipline_detail",
                type: "NVARCHAR2(200)",
                maxLength: 200,
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "case_owner_teacher_name",
                schema: "EDU",
                table: "student_service_activity");

            migrationBuilder.DropColumn(
                name: "case_owner_teacher_name",
                schema: "EDU",
                table: "student_discipline_exam");

            migrationBuilder.DropColumn(
                name: "case_owner_teacher_name",
                schema: "EDU",
                table: "student_discipline_detail");
        }
    }
}
