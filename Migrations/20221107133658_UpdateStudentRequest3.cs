﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace edu_api.Migrations
{
    public partial class UpdateStudentRequest3 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "bill_no",
                schema: "EDU",
                table: "student_request",
                type: "NVARCHAR2(50)",
                maxLength: 50,
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "bill_uid",
                schema: "EDU",
                table: "student_request",
                type: "RAW(16)",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "total_amout",
                schema: "EDU",
                table: "student_request",
                type: "DECIMAL(18, 2)",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "bill_no",
                schema: "EDU",
                table: "student_request");

            migrationBuilder.DropColumn(
                name: "bill_uid",
                schema: "EDU",
                table: "student_request");

            migrationBuilder.DropColumn(
                name: "total_amout",
                schema: "EDU",
                table: "student_request");
        }
    }
}
