﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace edu_api.Migrations
{
    public partial class studen_request_url_name : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "name_url",
                schema: "EDU",
                table: "student_request_name_and_address",
                type: "NVARCHAR2(500)",
                maxLength: 500,
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "name_url",
                schema: "EDU",
                table: "student_request_name_and_address");
        }
    }
}
