using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace edu_api.Migrations
{
    public partial class Updateparentchildtosubjectgroup : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<Guid>(
                name: "parent_curriculum_subject_group_uid",
                schema: "EDU",
                table: "curriculum_subject_group_master",
                type: "RAW(16)",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_curriculum_subject_group_master_parent_curriculum_subject_group_uid",
                schema: "EDU",
                table: "curriculum_subject_group_master",
                column: "parent_curriculum_subject_group_uid");

            migrationBuilder.AddForeignKey(
                name: "FK_curriculum_subject_group_master_curriculum_subject_group_master_parent_curriculum_subject_group_uid",
                schema: "EDU",
                table: "curriculum_subject_group_master",
                column: "parent_curriculum_subject_group_uid",
                principalSchema: "EDU",
                principalTable: "curriculum_subject_group_master",
                principalColumn: "curriculum_subject_group_uid",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_curriculum_subject_group_master_curriculum_subject_group_master_parent_curriculum_subject_group_uid",
                schema: "EDU",
                table: "curriculum_subject_group_master");

            migrationBuilder.DropIndex(
                name: "IX_curriculum_subject_group_master_parent_curriculum_subject_group_uid",
                schema: "EDU",
                table: "curriculum_subject_group_master");

            migrationBuilder.DropColumn(
                name: "parent_curriculum_subject_group_uid",
                schema: "EDU",
                table: "curriculum_subject_group_master");
        }
    }
}
