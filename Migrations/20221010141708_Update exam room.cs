﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace edu_api.Migrations
{
    public partial class Updateexamroom : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "year_subject_exam_room",
                schema: "EDU",
                columns: table => new
                {
                    year_subject_exam_room_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
                    status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
                    created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
                    updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
                    owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
                    year_subject_exam_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
                    room_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
                    exam_seats = table.Column<int>(type: "NUMBER(10)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_year_subject_exam_room", x => x.year_subject_exam_room_uid);
                });

            migrationBuilder.CreateTable(
                name: "year_subject_exam_room_proctor",
                schema: "EDU",
                columns: table => new
                {
                    year_subject_exam_room_proctor_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
                    status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
                    created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
                    updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
                    owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
                    year_subject_exam_room_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
                    instructor_uid = table.Column<Guid>(type: "RAW(16)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_year_subject_exam_room_proctor", x => x.year_subject_exam_room_proctor_uid);
                    table.ForeignKey(
                        name: "FK_year_subject_exam_room_proctor_year_subject_exam_room_year_subject_exam_room_uid",
                        column: x => x.year_subject_exam_room_uid,
                        principalSchema: "EDU",
                        principalTable: "year_subject_exam_room",
                        principalColumn: "year_subject_exam_room_uid",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_year_subject_exam_room_proctor_year_subject_exam_room_uid",
                schema: "EDU",
                table: "year_subject_exam_room_proctor",
                column: "year_subject_exam_room_uid");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "year_subject_exam_room_proctor",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "year_subject_exam_room",
                schema: "EDU");
        }
    }
}
