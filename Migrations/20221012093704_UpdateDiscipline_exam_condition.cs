﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace edu_api.Migrations
{
    public partial class UpdateDiscipline_exam_condition : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_student_discipline_exam_discipline_exam_condition_detail_discipline_exam_condition_detail_uid",
                schema: "EDU",
                table: "student_discipline_exam");

            migrationBuilder.DropForeignKey(
                name: "FK_student_discipline_exam_discipline_exam_condition_detail_sub_discipline_exam_condition_detail_sub_uid",
                schema: "EDU",
                table: "student_discipline_exam");

            migrationBuilder.DropTable(
                name: "discipline_exam_condition_detail_sub",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "discipline_exam_condition_detail",
                schema: "EDU");

            migrationBuilder.DropIndex(
                name: "IX_student_discipline_exam_discipline_exam_condition_detail_sub_uid",
                schema: "EDU",
                table: "student_discipline_exam");

            migrationBuilder.DropIndex(
                name: "IX_student_discipline_exam_discipline_exam_condition_detail_uid",
                schema: "EDU",
                table: "student_discipline_exam");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "discipline_exam_condition_detail",
                schema: "EDU",
                columns: table => new
                {
                    discipline_exam_condition_detail_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
                    created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
                    detail_section_code = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
                    detail_section_name_en = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    detail_section_name_th = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    discipline_exam_condition_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
                    owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
                    row_order = table.Column<int>(type: "NUMBER(10)", nullable: true),
                    status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
                    updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_discipline_exam_condition_detail", x => x.discipline_exam_condition_detail_uid);
                    table.ForeignKey(
                        name: "FK_discipline_exam_condition_detail_discipline_exam_condition_discipline_exam_condition_uid",
                        column: x => x.discipline_exam_condition_uid,
                        principalSchema: "EDU",
                        principalTable: "discipline_exam_condition",
                        principalColumn: "discipline_exam_condition_uid",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "discipline_exam_condition_detail_sub",
                schema: "EDU",
                columns: table => new
                {
                    discipline_exam_condition_detail_sub_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
                    created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
                    detail_sub_section_code = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
                    detail_sub_section_name_en = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    detail_sub_section_name_th = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    discipline_exam_condition_detail_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
                    owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
                    row_order = table.Column<int>(type: "NUMBER(10)", nullable: true),
                    status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
                    updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_discipline_exam_condition_detail_sub", x => x.discipline_exam_condition_detail_sub_uid);
                    table.ForeignKey(
                        name: "FK_discipline_exam_condition_detail_sub_discipline_exam_condition_detail_discipline_exam_condition_detail_uid",
                        column: x => x.discipline_exam_condition_detail_uid,
                        principalSchema: "EDU",
                        principalTable: "discipline_exam_condition_detail",
                        principalColumn: "discipline_exam_condition_detail_uid",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_student_discipline_exam_discipline_exam_condition_detail_sub_uid",
                schema: "EDU",
                table: "student_discipline_exam",
                column: "discipline_exam_condition_detail_sub_uid");

            migrationBuilder.CreateIndex(
                name: "IX_student_discipline_exam_discipline_exam_condition_detail_uid",
                schema: "EDU",
                table: "student_discipline_exam",
                column: "discipline_exam_condition_detail_uid");

            migrationBuilder.CreateIndex(
                name: "IX_discipline_exam_condition_detail_discipline_exam_condition_uid",
                schema: "EDU",
                table: "discipline_exam_condition_detail",
                column: "discipline_exam_condition_uid");

            migrationBuilder.CreateIndex(
                name: "IX_discipline_exam_condition_detail_sub_discipline_exam_condition_detail_uid",
                schema: "EDU",
                table: "discipline_exam_condition_detail_sub",
                column: "discipline_exam_condition_detail_uid");

            migrationBuilder.AddForeignKey(
                name: "FK_student_discipline_exam_discipline_exam_condition_detail_discipline_exam_condition_detail_uid",
                schema: "EDU",
                table: "student_discipline_exam",
                column: "discipline_exam_condition_detail_uid",
                principalSchema: "EDU",
                principalTable: "discipline_exam_condition_detail",
                principalColumn: "discipline_exam_condition_detail_uid",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_student_discipline_exam_discipline_exam_condition_detail_sub_discipline_exam_condition_detail_sub_uid",
                schema: "EDU",
                table: "student_discipline_exam",
                column: "discipline_exam_condition_detail_sub_uid",
                principalSchema: "EDU",
                principalTable: "discipline_exam_condition_detail_sub",
                principalColumn: "discipline_exam_condition_detail_sub_uid",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
