﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace edu_api.Migrations
{
    public partial class UpdateGrade : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "is_score",
                schema: "EDU",
                table: "grade",
                type: "NUMBER(1)",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "is_score",
                schema: "EDU",
                table: "grade");
        }
    }
}
