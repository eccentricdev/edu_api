﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace edu_api.Migrations
{
    public partial class Updaterequestdocumenttype : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "request_document_type_name_th",
                schema: "EDU",
                table: "request_document_type",
                type: "NVARCHAR2(200)",
                maxLength: 200,
                nullable: true,
                oldClrType: typeof(Guid),
                oldType: "RAW(16)",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "request_document_type_name_en",
                schema: "EDU",
                table: "request_document_type",
                type: "NVARCHAR2(200)",
                maxLength: 200,
                nullable: true,
                oldClrType: typeof(Guid),
                oldType: "RAW(16)",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "request_document_type_code",
                schema: "EDU",
                table: "request_document_type",
                type: "NVARCHAR2(50)",
                maxLength: 50,
                nullable: true,
                oldClrType: typeof(Guid),
                oldType: "RAW(16)",
                oldNullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<Guid>(
                name: "request_document_type_name_th",
                schema: "EDU",
                table: "request_document_type",
                type: "RAW(16)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "NVARCHAR2(200)",
                oldMaxLength: 200,
                oldNullable: true);

            migrationBuilder.AlterColumn<Guid>(
                name: "request_document_type_name_en",
                schema: "EDU",
                table: "request_document_type",
                type: "RAW(16)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "NVARCHAR2(200)",
                oldMaxLength: 200,
                oldNullable: true);

            migrationBuilder.AlterColumn<Guid>(
                name: "request_document_type_code",
                schema: "EDU",
                table: "request_document_type",
                type: "RAW(16)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "NVARCHAR2(50)",
                oldMaxLength: 50,
                oldNullable: true);
        }
    }
}
