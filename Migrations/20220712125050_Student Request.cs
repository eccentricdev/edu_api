﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace edu_api.Migrations
{
    public partial class StudentRequest : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<decimal>(
                name: "to_grade",
                schema: "EDU",
                table: "probation_retire_condition_detail",
                type: "DECIMAL(18, 2)",
                nullable: true,
                oldClrType: typeof(decimal),
                oldType: "DECIMAL(18,2)",
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "to_credit",
                schema: "EDU",
                table: "probation_retire_condition_detail",
                type: "DECIMAL(18, 2)",
                nullable: true,
                oldClrType: typeof(decimal),
                oldType: "DECIMAL(18,2)",
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "from_grade",
                schema: "EDU",
                table: "probation_retire_condition_detail",
                type: "DECIMAL(18, 2)",
                nullable: true,
                oldClrType: typeof(decimal),
                oldType: "DECIMAL(18,2)",
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "from_credit",
                schema: "EDU",
                table: "probation_retire_condition_detail",
                type: "DECIMAL(18, 2)",
                nullable: true,
                oldClrType: typeof(decimal),
                oldType: "DECIMAL(18,2)",
                oldNullable: true);

            migrationBuilder.CreateTable(
                name: "request_type",
                schema: "EDU",
                columns: table => new
                {
                    request_type_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
                    status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
                    created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
                    updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
                    owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
                    request_type_code = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
                    request_type_name_th = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    request_type_name_en = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_request_type", x => x.request_type_uid);
                });

            migrationBuilder.CreateTable(
                name: "student_request",
                schema: "EDU",
                columns: table => new
                {
                    student_request_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
                    status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
                    created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
                    updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
                    owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
                    student_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
                    request_type_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
                    is_consent = table.Column<bool>(type: "NUMBER(1)", nullable: true),
                    request_date = table.Column<DateTime>(type: "date", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_student_request", x => x.student_request_uid);
                    table.ForeignKey(
                        name: "FK_student_request_request_type_request_type_uid",
                        column: x => x.request_type_uid,
                        principalSchema: "EDU",
                        principalTable: "request_type",
                        principalColumn: "request_type_uid",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "transfer_all_request",
                schema: "EDU",
                columns: table => new
                {
                    transfer_all_request_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
                    status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
                    created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
                    updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
                    owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
                    student_request_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
                    mobile_no = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
                    email = table.Column<string>(type: "NVARCHAR2(2000)", nullable: true),
                    education_type_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
                    college_faculty_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
                    faculty_curriculum_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
                    academic_year_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
                    academic_semester_uid = table.Column<Guid>(type: "RAW(16)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_transfer_all_request", x => x.transfer_all_request_uid);
                    table.ForeignKey(
                        name: "FK_transfer_all_request_student_request_student_request_uid",
                        column: x => x.student_request_uid,
                        principalSchema: "EDU",
                        principalTable: "student_request",
                        principalColumn: "student_request_uid",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_student_request_request_type_uid",
                schema: "EDU",
                table: "student_request",
                column: "request_type_uid");

            migrationBuilder.CreateIndex(
                name: "IX_transfer_all_request_student_request_uid",
                schema: "EDU",
                table: "transfer_all_request",
                column: "student_request_uid");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "transfer_all_request",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "student_request",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "request_type",
                schema: "EDU");

            migrationBuilder.AlterColumn<decimal>(
                name: "to_grade",
                schema: "EDU",
                table: "probation_retire_condition_detail",
                type: "DECIMAL(18,2)",
                nullable: true,
                oldClrType: typeof(decimal),
                oldType: "DECIMAL(18, 2)",
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "to_credit",
                schema: "EDU",
                table: "probation_retire_condition_detail",
                type: "DECIMAL(18,2)",
                nullable: true,
                oldClrType: typeof(decimal),
                oldType: "DECIMAL(18, 2)",
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "from_grade",
                schema: "EDU",
                table: "probation_retire_condition_detail",
                type: "DECIMAL(18,2)",
                nullable: true,
                oldClrType: typeof(decimal),
                oldType: "DECIMAL(18, 2)",
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "from_credit",
                schema: "EDU",
                table: "probation_retire_condition_detail",
                type: "DECIMAL(18,2)",
                nullable: true,
                oldClrType: typeof(decimal),
                oldType: "DECIMAL(18, 2)",
                oldNullable: true);
        }
    }
}
