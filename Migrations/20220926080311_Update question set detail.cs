﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace edu_api.Migrations
{
    public partial class Updatequestionsetdetail : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<Guid>(
                name: "question_set_curriculum_uid",
                schema: "EDU",
                table: "question_set_subject",
                type: "RAW(16)",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "question_set_faculty_uid",
                schema: "EDU",
                table: "question_set_curriculum",
                type: "RAW(16)",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "question_set_curriculum_uid",
                schema: "EDU",
                table: "question_set_subject");

            migrationBuilder.DropColumn(
                name: "question_set_faculty_uid",
                schema: "EDU",
                table: "question_set_curriculum");
        }
    }
}
