﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace edu_api.Migrations
{
    public partial class Add_graduate_faculty_curriculum : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            // migrationBuilder.AlterColumn<decimal>(
            //     name: "total_amout",
            //     schema: "EDU",
            //     table: "student_request",
            //     type: "DECIMAL(18, 2)",
            //     nullable: true,
            //     oldClrType: typeof(decimal),
            //     oldType: "DECIMAL(18,2)",
            //     oldNullable: true);

            migrationBuilder.AddColumn<int>(
                name: "generation",
                schema: "EDU",
                table: "graduate_student",
                type: "NUMBER(10)",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "graduate_academic_semester_uid",
                schema: "EDU",
                table: "graduate_student",
                type: "RAW(16)",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "graduate_academic_year_uid",
                schema: "EDU",
                table: "graduate_student",
                type: "RAW(16)",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "is_record_graduate",
                schema: "EDU",
                table: "graduate_student",
                type: "NUMBER(1)",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "round",
                schema: "EDU",
                table: "graduate_student",
                type: "NUMBER(10)",
                nullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "amout",
                schema: "EDU",
                table: "graduate_request_payment_detail",
                type: "DECIMAL(18, 2)",
                nullable: true,
                oldClrType: typeof(decimal),
                oldType: "DECIMAL(18,2)",
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "total_amout",
                schema: "EDU",
                table: "graduate_request_payment",
                type: "DECIMAL(18, 2)",
                nullable: true,
                oldClrType: typeof(decimal),
                oldType: "DECIMAL(18,2)",
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "amout",
                schema: "EDU",
                table: "graduate_register_payment",
                type: "DECIMAL(18, 2)",
                nullable: true,
                oldClrType: typeof(decimal),
                oldType: "DECIMAL(18,2)",
                oldNullable: true);

            migrationBuilder.CreateTable(
                name: "graduate_faculty_curriculum",
                schema: "EDU",
                columns: table => new
                {
                    graduate_faculty_curriculum_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
                    status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
                    created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
                    updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
                    owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
                    graduate_academic_year_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
                    round = table.Column<int>(type: "NUMBER(10)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_graduate_faculty_curriculum", x => x.graduate_faculty_curriculum_uid);
                });

            migrationBuilder.CreateTable(
                name: "graduate_faculty_curriculum_detail",
                schema: "EDU",
                columns: table => new
                {
                    graduate_faculty_curriculum_detail_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
                    status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
                    created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
                    updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
                    owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
                    graduate_faculty_curriculum_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
                    faculty_curriculum_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
                    row_order = table.Column<int>(type: "NUMBER(10)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_graduate_faculty_curriculum_detail", x => x.graduate_faculty_curriculum_detail_uid);
                    table.ForeignKey(
                        name: "FK_graduate_faculty_curriculum_detail_graduate_faculty_curriculum_graduate_faculty_curriculum_uid",
                        column: x => x.graduate_faculty_curriculum_uid,
                        principalSchema: "EDU",
                        principalTable: "graduate_faculty_curriculum",
                        principalColumn: "graduate_faculty_curriculum_uid",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_graduate_faculty_curriculum_detail_graduate_faculty_curriculum_uid",
                schema: "EDU",
                table: "graduate_faculty_curriculum_detail",
                column: "graduate_faculty_curriculum_uid");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "graduate_faculty_curriculum_detail",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "graduate_faculty_curriculum",
                schema: "EDU");

            migrationBuilder.DropColumn(
                name: "generation",
                schema: "EDU",
                table: "graduate_student");

            migrationBuilder.DropColumn(
                name: "graduate_academic_semester_uid",
                schema: "EDU",
                table: "graduate_student");

            migrationBuilder.DropColumn(
                name: "graduate_academic_year_uid",
                schema: "EDU",
                table: "graduate_student");

            migrationBuilder.DropColumn(
                name: "is_record_graduate",
                schema: "EDU",
                table: "graduate_student");

            migrationBuilder.DropColumn(
                name: "round",
                schema: "EDU",
                table: "graduate_student");

            migrationBuilder.AlterColumn<decimal>(
                name: "total_amout",
                schema: "EDU",
                table: "student_request",
                type: "DECIMAL(18,2)",
                nullable: true,
                oldClrType: typeof(decimal),
                oldType: "DECIMAL(18, 2)",
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "amout",
                schema: "EDU",
                table: "graduate_request_payment_detail",
                type: "DECIMAL(18,2)",
                nullable: true,
                oldClrType: typeof(decimal),
                oldType: "DECIMAL(18, 2)",
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "total_amout",
                schema: "EDU",
                table: "graduate_request_payment",
                type: "DECIMAL(18,2)",
                nullable: true,
                oldClrType: typeof(decimal),
                oldType: "DECIMAL(18, 2)",
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "amout",
                schema: "EDU",
                table: "graduate_register_payment",
                type: "DECIMAL(18,2)",
                nullable: true,
                oldClrType: typeof(decimal),
                oldType: "DECIMAL(18, 2)",
                oldNullable: true);
        }
    }
}
