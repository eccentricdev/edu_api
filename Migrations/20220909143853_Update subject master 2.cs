﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace edu_api.Migrations
{
    public partial class Updatesubjectmaster2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "subject_co_operative_edu",
                schema: "EDU",
                table: "subject_master",
                type: "NUMBER(1)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "subject_type",
                schema: "EDU",
                table: "subject_master",
                type: "NVARCHAR2(2000)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "subject_type_code",
                schema: "EDU",
                table: "subject_master",
                type: "NVARCHAR2(50)",
                maxLength: 50,
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "subject_co_operative_edu",
                schema: "EDU",
                table: "subject_master");

            migrationBuilder.DropColumn(
                name: "subject_type",
                schema: "EDU",
                table: "subject_master");

            migrationBuilder.DropColumn(
                name: "subject_type_code",
                schema: "EDU",
                table: "subject_master");
        }
    }
}
