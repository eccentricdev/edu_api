﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace edu_api.Migrations
{
    public partial class StudentFlag : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "student_flag_type",
                schema: "EDU",
                columns: table => new
                {
                    student_flag_type_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
                    status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
                    created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
                    updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
                    owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
                    student_status_code = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
                    student_status_name_th = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    student_status_name_en = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_student_flag_type", x => x.student_flag_type_uid);
                });

            migrationBuilder.CreateTable(
                name: "student_flag",
                schema: "EDU",
                columns: table => new
                {
                    student_flag_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
                    status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
                    created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
                    updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
                    owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
                    student_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
                    academic_year_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
                    academic_semester_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
                    flag_date = table.Column<DateTime>(type: "date", nullable: true),
                    student_flag_type_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
                    agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
                    description_th = table.Column<string>(type: "NVARCHAR2(500)", maxLength: 500, nullable: true),
                    description_en = table.Column<string>(type: "NVARCHAR2(500)", maxLength: 500, nullable: true),
                    is_active = table.Column<bool>(type: "NUMBER(1)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_student_flag", x => x.student_flag_uid);
                    table.ForeignKey(
                        name: "FK_student_flag_student_flag_type_student_flag_type_uid",
                        column: x => x.student_flag_type_uid,
                        principalSchema: "EDU",
                        principalTable: "student_flag_type",
                        principalColumn: "student_flag_type_uid",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_student_flag_student_flag_type_uid",
                schema: "EDU",
                table: "student_flag",
                column: "student_flag_type_uid");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "student_flag",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "student_flag_type",
                schema: "EDU");
        }
    }
}
