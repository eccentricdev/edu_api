﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace edu_api.Migrations
{
    public partial class Addleavestatusandstudentcardrequest : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "leave_status_request",
                schema: "EDU",
                columns: table => new
                {
                    leave_status_request_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
                    status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
                    created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
                    updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
                    owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
                    student_request_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
                    is_intermission_leave = table.Column<bool>(type: "NUMBER(1)", nullable: true),
                    intermission_leave_semester = table.Column<string>(type: "NVARCHAR2(2000)", nullable: true),
                    intermission_leave_reason = table.Column<string>(type: "NVARCHAR2(2000)", nullable: true),
                    is_leave = table.Column<bool>(type: "NUMBER(1)", nullable: true),
                    leave_semester = table.Column<string>(type: "NVARCHAR2(2000)", nullable: true),
                    leave_year = table.Column<string>(type: "NVARCHAR2(2000)", nullable: true),
                    leave_reason_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
                    leave_reason_other_remark = table.Column<string>(type: "NVARCHAR2(1000)", maxLength: 1000, nullable: true),
                    is_receive_insurance = table.Column<bool>(type: "NUMBER(1)", nullable: true),
                    insurance_amount = table.Column<decimal>(type: "DECIMAL(18,4)", precision: 18, scale: 4, nullable: true),
                    is_leave_receive_advance_payment = table.Column<bool>(type: "NUMBER(1)", nullable: true),
                    leave_advanced_payment_amount = table.Column<decimal>(type: "DECIMAL(18,4)", precision: 18, scale: 4, nullable: true),
                    is_leave_receive_other = table.Column<bool>(type: "NUMBER(1)", nullable: true),
                    leave_receive_other_remark = table.Column<string>(type: "NVARCHAR2(1000)", maxLength: 1000, nullable: true),
                    leave_other_amount = table.Column<string>(type: "NVARCHAR2(2000)", nullable: true),
                    is_return = table.Column<bool>(type: "NUMBER(1)", nullable: true),
                    is_receive_loan = table.Column<bool>(type: "NUMBER(1)", nullable: true),
                    loan_amount = table.Column<decimal>(type: "DECIMAL(18,4)", precision: 18, scale: 4, nullable: true),
                    is_other_receive = table.Column<bool>(type: "NUMBER(1)", nullable: true),
                    other_receive_reason = table.Column<string>(type: "NVARCHAR2(2000)", nullable: true),
                    other_receive_amount = table.Column<decimal>(type: "DECIMAL(18,4)", precision: 18, scale: 4, nullable: true),
                    receive_account_no = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
                    receive_bank_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
                    receive_bank_branch = table.Column<string>(type: "NVARCHAR2(2000)", nullable: true),
                    receive_account_owner = table.Column<string>(type: "NVARCHAR2(2000)", nullable: true),
                    receive_phone_no = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_leave_status_request", x => x.leave_status_request_uid);
                    table.ForeignKey(
                        name: "FK_leave_status_request_student_request_student_request_uid",
                        column: x => x.student_request_uid,
                        principalSchema: "EDU",
                        principalTable: "student_request",
                        principalColumn: "student_request_uid",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "student_card_request",
                schema: "EDU",
                columns: table => new
                {
                    student_card_request_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
                    status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
                    created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
                    updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
                    owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
                    student_request_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
                    is_lost = table.Column<bool>(type: "NUMBER(1)", nullable: true),
                    is_expire = table.Column<bool>(type: "NUMBER(1)", nullable: true),
                    graduate_semester = table.Column<string>(type: "NVARCHAR2(2000)", nullable: true),
                    graduate_year = table.Column<string>(type: "NVARCHAR2(2000)", nullable: true),
                    is_faculty_change = table.Column<bool>(type: "NUMBER(1)", nullable: true),
                    from_college_faculty_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
                    to_college_faculty_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
                    is_name_change = table.Column<bool>(type: "NUMBER(1)", nullable: true),
                    from_title_id = table.Column<int>(type: "NUMBER(10)", nullable: true),
                    from_first_name_th = table.Column<int>(type: "NUMBER(10)", nullable: true),
                    from_middle_name_th = table.Column<int>(type: "NUMBER(10)", nullable: true),
                    from_last_name_th = table.Column<int>(type: "NUMBER(10)", nullable: true),
                    from_first_name_en = table.Column<int>(type: "NUMBER(10)", nullable: true),
                    from_middle_name_en = table.Column<int>(type: "NUMBER(10)", nullable: true),
                    from_last_name_en = table.Column<int>(type: "NUMBER(10)", nullable: true),
                    to_title_id = table.Column<int>(type: "NUMBER(10)", nullable: true),
                    to_first_name_th = table.Column<int>(type: "NUMBER(10)", nullable: true),
                    to_middle_name_th = table.Column<int>(type: "NUMBER(10)", nullable: true),
                    to_last_name_th = table.Column<int>(type: "NUMBER(10)", nullable: true),
                    to_first_name_en = table.Column<int>(type: "NUMBER(10)", nullable: true),
                    to_middle_name_en = table.Column<int>(type: "NUMBER(10)", nullable: true),
                    to_last_name_en = table.Column<int>(type: "NUMBER(10)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_student_card_request", x => x.student_card_request_uid);
                    table.ForeignKey(
                        name: "FK_student_card_request_student_request_student_request_uid",
                        column: x => x.student_request_uid,
                        principalSchema: "EDU",
                        principalTable: "student_request",
                        principalColumn: "student_request_uid",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "leave_status_document",
                schema: "EDU",
                columns: table => new
                {
                    leave_status_document_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
                    status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
                    created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
                    updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
                    owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
                    leave_status_request_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
                    request_document_type_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
                    request_document_type_name = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    file_name = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    document_name = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    document_url = table.Column<string>(type: "NVARCHAR2(500)", maxLength: 500, nullable: true),
                    admin_file_name = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    admin_document_url = table.Column<string>(type: "NVARCHAR2(500)", maxLength: 500, nullable: true),
                    mime_type = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
                    document_status_id = table.Column<int>(type: "NUMBER(10)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_leave_status_document", x => x.leave_status_document_uid);
                    table.ForeignKey(
                        name: "FK_leave_status_document_leave_status_request_leave_status_request_uid",
                        column: x => x.leave_status_request_uid,
                        principalSchema: "EDU",
                        principalTable: "leave_status_request",
                        principalColumn: "leave_status_request_uid",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "student_card_document",
                schema: "EDU",
                columns: table => new
                {
                    student_card_document_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
                    status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
                    created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
                    updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
                    owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
                    student_card_request_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
                    request_document_type_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
                    request_document_type_name = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    file_name = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    document_name = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    document_url = table.Column<string>(type: "NVARCHAR2(500)", maxLength: 500, nullable: true),
                    admin_file_name = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    admin_document_url = table.Column<string>(type: "NVARCHAR2(500)", maxLength: 500, nullable: true),
                    mime_type = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
                    document_status_id = table.Column<int>(type: "NUMBER(10)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_student_card_document", x => x.student_card_document_uid);
                    table.ForeignKey(
                        name: "FK_student_card_document_student_card_request_student_card_request_uid",
                        column: x => x.student_card_request_uid,
                        principalSchema: "EDU",
                        principalTable: "student_card_request",
                        principalColumn: "student_card_request_uid",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_leave_status_document_leave_status_request_uid",
                schema: "EDU",
                table: "leave_status_document",
                column: "leave_status_request_uid");

            migrationBuilder.CreateIndex(
                name: "IX_leave_status_request_student_request_uid",
                schema: "EDU",
                table: "leave_status_request",
                column: "student_request_uid");

            migrationBuilder.CreateIndex(
                name: "IX_student_card_document_student_card_request_uid",
                schema: "EDU",
                table: "student_card_document",
                column: "student_card_request_uid");

            migrationBuilder.CreateIndex(
                name: "IX_student_card_request_student_request_uid",
                schema: "EDU",
                table: "student_card_request",
                column: "student_request_uid");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "leave_status_document",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "student_card_document",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "leave_status_request",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "student_card_request",
                schema: "EDU");
        }
    }
}
