﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace edu_api.Migrations
{
    public partial class updatesubjectmaster : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            // migrationBuilder.EnsureSchema(
            //     name: "RSUAPP");

            // migrationBuilder.AddColumn<string>(
            //     name: "subject_master_seq_code",
            //     schema: "EDU",
            //     table: "subject_master_year",
            //     type: "NVARCHAR2(50)",
            //     maxLength: 50,
            //     nullable: true);
            //
            // migrationBuilder.AddColumn<string>(
            //     name: "year_subject_name_en",
            //     schema: "EDU",
            //     table: "subject_master_year",
            //     type: "NVARCHAR2(200)",
            //     maxLength: 200,
            //     nullable: true);
            //
            // migrationBuilder.AddColumn<string>(
            //     name: "year_subject_name_th",
            //     schema: "EDU",
            //     table: "subject_master_year",
            //     type: "NVARCHAR2(200)",
            //     maxLength: 200,
            //     nullable: true);
            //
            // migrationBuilder.AddColumn<string>(
            //     name: "education_type_code",
            //     schema: "EDU",
            //     table: "subject_master_owner",
            //     type: "NVARCHAR2(50)",
            //     maxLength: 50,
            //     nullable: true);

            // migrationBuilder.CreateTable(
            //     name: "applicant_apply",
            //     schema: "RSUAPP",
            //     columns: table => new
            //     {
            //         applicant_apply_id = table.Column<int>(type: "NUMBER(10)", nullable: false)
            //             .Annotation("Oracle:Identity", "1, 1"),
            //         academic_year_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         academic_semester_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         applicant_id = table.Column<int>(type: "NUMBER(10)", nullable: true),
            //         program_id = table.Column<int>(type: "NUMBER(10)", nullable: true),
            //         program_schedule_id = table.Column<int>(type: "NUMBER(10)", nullable: true),
            //         course_schedule_id = table.Column<int>(type: "NUMBER(10)", nullable: true),
            //         application_id = table.Column<long>(type: "NUMBER(19)", nullable: true),
            //         application_invoice_no = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
            //         application_invoice_due_date = table.Column<DateTime>(type: "date", nullable: true),
            //         payin_id = table.Column<int>(type: "NUMBER(10)", nullable: true),
            //         payin_no = table.Column<long>(type: "NUMBER(19)", nullable: true),
            //         application_amount = table.Column<decimal>(type: "decimal(18,4)", precision: 18, scale: 4, nullable: true),
            //         exam_amount = table.Column<decimal>(type: "decimal(18,4)", precision: 18, scale: 4, nullable: true),
            //         coupon_amount = table.Column<decimal>(type: "decimal(18,4)", precision: 18, scale: 4, nullable: true),
            //         coupon_id = table.Column<int>(type: "NUMBER(10)", nullable: true),
            //         payment_amount = table.Column<decimal>(type: "decimal(18,4)", precision: 18, scale: 4, nullable: true),
            //         payment_flag = table.Column<bool>(type: "NUMBER(1)", nullable: true),
            //         document_flag = table.Column<bool>(type: "NUMBER(1)", nullable: true),
            //         is_document_completed = table.Column<bool>(type: "NUMBER(1)", nullable: true),
            //         payment_type_code = table.Column<string>(type: "NVARCHAR2(10)", maxLength: 10, nullable: true),
            //         status_id = table.Column<int>(type: "NUMBER(10)", nullable: true),
            //         create_by = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
            //         create_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         last_update_by = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
            //         last_update_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //         application_status_id = table.Column<int>(type: "NUMBER(10)", nullable: true),
            //         apply_date = table.Column<DateTime>(type: "date", nullable: true),
            //         plan_register_pay_in_no = table.Column<long>(type: "NUMBER(19)", nullable: true),
            //         plan_register_invoice_no = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
            //         plan_register_payment_amount = table.Column<decimal>(type: "decimal(18,4)", precision: 18, scale: 4, nullable: true),
            //         plan_register_payment_due_date = table.Column<DateTime>(type: "date", nullable: true),
            //         plan_register_payment_flag = table.Column<bool>(type: "NUMBER(1)", nullable: true),
            //         plan_register_id = table.Column<int>(type: "NUMBER(10)", nullable: true),
            //         education_type_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         college_faculty_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         faculty_curriculum_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         student_code = table.Column<string>(type: "NVARCHAR2(10)", maxLength: 10, nullable: true),
            //         rsu_email = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
            //         validate_by_id = table.Column<int>(type: "NUMBER(10)", nullable: true),
            //         apply_type_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
            //         bill_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         plan_register_bill_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         plan_register_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //         plan_register_bill_no = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
            //         application_no = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
            //         written_exam_room_id = table.Column<int>(type: "NUMBER(10)", nullable: true),
            //         seat_no = table.Column<int>(type: "NUMBER(10)", nullable: true),
            //         exam_seat_no = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
            //         is_written_exam_passed = table.Column<bool>(type: "NUMBER(1)", nullable: true),
            //         is_interview_exam_passed = table.Column<bool>(type: "NUMBER(1)", nullable: true)
            //     },
            //     constraints: table =>
            //     {
            //         table.PrimaryKey("PK_applicant_apply", x => x.applicant_apply_id);
            //     });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            // migrationBuilder.DropTable(
            //     name: "applicant_apply",
            //     schema: "RSUAPP");

            migrationBuilder.DropColumn(
                name: "subject_master_seq_code",
                schema: "EDU",
                table: "subject_master_year");

            migrationBuilder.DropColumn(
                name: "year_subject_name_en",
                schema: "EDU",
                table: "subject_master_year");

            migrationBuilder.DropColumn(
                name: "year_subject_name_th",
                schema: "EDU",
                table: "subject_master_year");

            migrationBuilder.DropColumn(
                name: "education_type_code",
                schema: "EDU",
                table: "subject_master_owner");
        }
    }
}
