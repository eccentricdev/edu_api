﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace edu_api.Migrations
{
    public partial class updatestudent_request_name_and_address : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "IX_student_request_name_and_address_student_request_uid",
                schema: "EDU",
                table: "student_request_name_and_address",
                column: "student_request_uid");

            migrationBuilder.AddForeignKey(
                name: "FK_student_request_name_and_address_student_request_student_request_uid",
                schema: "EDU",
                table: "student_request_name_and_address",
                column: "student_request_uid",
                principalSchema: "EDU",
                principalTable: "student_request",
                principalColumn: "student_request_uid",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_student_request_name_and_address_student_request_student_request_uid",
                schema: "EDU",
                table: "student_request_name_and_address");

            migrationBuilder.DropIndex(
                name: "IX_student_request_name_and_address_student_request_uid",
                schema: "EDU",
                table: "student_request_name_and_address");
        }
    }
}
