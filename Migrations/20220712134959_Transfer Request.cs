﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace edu_api.Migrations
{
    public partial class TransferRequest : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<decimal>(
                name: "to_grade",
                schema: "EDU",
                table: "probation_retire_condition_detail",
                type: "decimal(5,2)",
                nullable: true,
                oldClrType: typeof(decimal),
                oldType: "DECIMAL(18,2)",
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "to_credit",
                schema: "EDU",
                table: "probation_retire_condition_detail",
                type: "decimal(5,2)",
                nullable: true,
                oldClrType: typeof(decimal),
                oldType: "DECIMAL(18,2)",
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "from_grade",
                schema: "EDU",
                table: "probation_retire_condition_detail",
                type: "decimal(5,2)",
                nullable: true,
                oldClrType: typeof(decimal),
                oldType: "DECIMAL(18,2)",
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "from_credit",
                schema: "EDU",
                table: "probation_retire_condition_detail",
                type: "decimal(5,2)",
                nullable: true,
                oldClrType: typeof(decimal),
                oldType: "DECIMAL(18,2)",
                oldNullable: true);

            migrationBuilder.CreateTable(
                name: "transfer_new_request",
                schema: "EDU",
                columns: table => new
                {
                    transfer_new_request_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
                    status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
                    created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
                    updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
                    owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
                    student_request_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
                    mobile_no = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
                    email = table.Column<string>(type: "NVARCHAR2(2000)", nullable: true),
                    education_type_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
                    college_faculty_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
                    faculty_curriculum_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
                    academic_year_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
                    academic_semester_uid = table.Column<Guid>(type: "RAW(16)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_transfer_new_request", x => x.transfer_new_request_uid);
                    table.ForeignKey(
                        name: "FK_transfer_new_request_student_request_student_request_uid",
                        column: x => x.student_request_uid,
                        principalSchema: "EDU",
                        principalTable: "student_request",
                        principalColumn: "student_request_uid",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "transfer_partial_request",
                schema: "EDU",
                columns: table => new
                {
                    transfer_partial_request_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
                    status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
                    created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
                    updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
                    owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
                    student_request_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
                    mobile_no = table.Column<string>(type: "NVARCHAR2(50)", maxLength: 50, nullable: true),
                    email = table.Column<string>(type: "NVARCHAR2(2000)", nullable: true),
                    education_type_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
                    college_faculty_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
                    faculty_curriculum_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
                    academic_year_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
                    academic_semester_uid = table.Column<Guid>(type: "RAW(16)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_transfer_partial_request", x => x.transfer_partial_request_uid);
                    table.ForeignKey(
                        name: "FK_transfer_partial_request_student_request_student_request_uid",
                        column: x => x.student_request_uid,
                        principalSchema: "EDU",
                        principalTable: "student_request",
                        principalColumn: "student_request_uid",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "transfer_new_from",
                schema: "EDU",
                columns: table => new
                {
                    transfer_new_from_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
                    status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
                    created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
                    updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
                    owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
                    transfer_new_request_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
                    student_uid = table.Column<Guid>(type: "RAW(16)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_transfer_new_from", x => x.transfer_new_from_uid);
                    table.ForeignKey(
                        name: "FK_transfer_new_from_transfer_new_request_transfer_new_request_uid",
                        column: x => x.transfer_new_request_uid,
                        principalSchema: "EDU",
                        principalTable: "transfer_new_request",
                        principalColumn: "transfer_new_request_uid",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "transfer_partial_subject",
                schema: "EDU",
                columns: table => new
                {
                    transfer_partial_subject_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
                    status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
                    created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
                    updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
                    owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
                    transfer_partial_request_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
                    final_register_subject_uid = table.Column<Guid>(type: "RAW(16)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_transfer_partial_subject", x => x.transfer_partial_subject_uid);
                    table.ForeignKey(
                        name: "FK_transfer_partial_subject_transfer_partial_request_transfer_partial_request_uid",
                        column: x => x.transfer_partial_request_uid,
                        principalSchema: "EDU",
                        principalTable: "transfer_partial_request",
                        principalColumn: "transfer_partial_request_uid",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "transfer_new_subject",
                schema: "EDU",
                columns: table => new
                {
                    transfer_new_subject_uid = table.Column<Guid>(type: "RAW(16)", nullable: false),
                    status_id = table.Column<short>(type: "NUMBER(5)", nullable: true),
                    created_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    created_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
                    updated_by = table.Column<string>(type: "NVARCHAR2(200)", maxLength: 200, nullable: true),
                    updated_datetime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
                    owner_agency_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
                    transfer_new_from_uid = table.Column<Guid>(type: "RAW(16)", nullable: true),
                    final_register_subject_uid = table.Column<Guid>(type: "RAW(16)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_transfer_new_subject", x => x.transfer_new_subject_uid);
                    table.ForeignKey(
                        name: "FK_transfer_new_subject_transfer_new_from_transfer_new_from_uid",
                        column: x => x.transfer_new_from_uid,
                        principalSchema: "EDU",
                        principalTable: "transfer_new_from",
                        principalColumn: "transfer_new_from_uid",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_transfer_new_from_transfer_new_request_uid",
                schema: "EDU",
                table: "transfer_new_from",
                column: "transfer_new_request_uid");

            migrationBuilder.CreateIndex(
                name: "IX_transfer_new_request_student_request_uid",
                schema: "EDU",
                table: "transfer_new_request",
                column: "student_request_uid");

            migrationBuilder.CreateIndex(
                name: "IX_transfer_new_subject_transfer_new_from_uid",
                schema: "EDU",
                table: "transfer_new_subject",
                column: "transfer_new_from_uid");

            migrationBuilder.CreateIndex(
                name: "IX_transfer_partial_request_student_request_uid",
                schema: "EDU",
                table: "transfer_partial_request",
                column: "student_request_uid");

            migrationBuilder.CreateIndex(
                name: "IX_transfer_partial_subject_transfer_partial_request_uid",
                schema: "EDU",
                table: "transfer_partial_subject",
                column: "transfer_partial_request_uid");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "transfer_new_subject",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "transfer_partial_subject",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "transfer_new_from",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "transfer_partial_request",
                schema: "EDU");

            migrationBuilder.DropTable(
                name: "transfer_new_request",
                schema: "EDU");

            migrationBuilder.AlterColumn<decimal>(
                name: "to_grade",
                schema: "EDU",
                table: "probation_retire_condition_detail",
                type: "DECIMAL(18,2)",
                nullable: true,
                oldClrType: typeof(decimal),
                oldType: "decimal(5,2)",
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "to_credit",
                schema: "EDU",
                table: "probation_retire_condition_detail",
                type: "DECIMAL(18,2)",
                nullable: true,
                oldClrType: typeof(decimal),
                oldType: "decimal(5,2)",
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "from_grade",
                schema: "EDU",
                table: "probation_retire_condition_detail",
                type: "DECIMAL(18,2)",
                nullable: true,
                oldClrType: typeof(decimal),
                oldType: "decimal(5,2)",
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "from_credit",
                schema: "EDU",
                table: "probation_retire_condition_detail",
                type: "DECIMAL(18,2)",
                nullable: true,
                oldClrType: typeof(decimal),
                oldType: "decimal(5,2)",
                oldNullable: true);
        }
    }
}
