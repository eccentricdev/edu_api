using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using edu_api.Modules.Calendars.Configures;
using edu_api.Modules.Commons.Configures;
using edu_api.Modules.Commons.Initials;
using edu_api.Modules.EducationChecks.Configures;
using edu_api.Modules.EducationPlan.Configures;
using edu_api.Modules.Fee.Configures;
using edu_api.Modules.Graduate.Configures;
using edu_api.Modules.InstructorEvaluations.Configures;
using edu_api.Modules.Organizations.Configures;
using edu_api.Modules.Payment.Services;
using edu_api.Modules.Register.Configures;
using edu_api.Modules.Register.Models;
using edu_api.Modules.StudentEvaluations.Configures;
using edu_api.Modules.Study.Configures;
using edu_api.Modules.Utilities.Models;
using edu_api.Modules.Workflows.Services;
using Hangfire;
using Hangfire.Oracle.Core;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Any;
using Microsoft.OpenApi.Models;
using Oracle.ManagedDataAccess.Client;
using rsu_common_api.Modules.Profiles.Configures;
using SeventyOneDev.Core;
using SeventyOneDev.Core.Context;
using SeventyOneDev.Core.Security.Models;
using SeventyOneDev.Utilities;
using Swashbuckle.AspNetCore.SwaggerUI;

namespace edu_api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
          services.AddDbContext<Db>(options =>
            {
              options.UseOracle(Configuration["databases:oracle"]);
            },
            ServiceLifetime.Transient
          );
          services.AddDbContext<SeventyOneDevCoreContext>(
            options =>
            {
              options.UseOracle(Configuration["databases:oracle"], b => b.MigrationsAssembly("edu_api"));
            }, ServiceLifetime.Transient);

          services.AddHttpClient("open_id", client =>
          {
            client.BaseAddress = new Uri(Configuration["OpenID:authority"]);
            client.DefaultRequestHeaders.Add("Accept", "application/json");
            client.Timeout=TimeSpan.FromSeconds(10);

          });
          services.AddHttpClient("iam_api", client =>
          {
            client.BaseAddress = new Uri(Configuration["OpenID:api"]);
            client.DefaultRequestHeaders.Add("Accept", "application/json");
            client.Timeout = TimeSpan.FromSeconds(10);

          });
          services.AddHttpClient("ar_payment", client =>
          {
            client.BaseAddress = new Uri(Configuration["Settings:payment_url"]);
            client.DefaultRequestHeaders.Add("Accept", "application/json");
            client.Timeout=TimeSpan.FromSeconds(30);

          });
          var spArPayment = ServicePointManager.FindServicePoint(new Uri(Configuration["Settings:payment_url"]));
          spArPayment.ConnectionLimit = 200;

          var iamApi = ServicePointManager.FindServicePoint(new Uri(Configuration["OpenID:api"]));
          iamApi.ConnectionLimit = 200;

            services.AddControllers(o =>{
              o.Conventions.Add(
                new GenericControllerRouteConvention()
              );
            })
            .
            ConfigureApplicationPartManager(m =>
              {

                m.FeatureProviders.Add(new GenericTypeControllerFeatureProvider());
                m.FeatureProviders.Add(new GenericUidTypeControllerFeatureProvider());
                // m.FeatureProviders.Add(new GenericTestUidTypeControllerFeatureProvider());

              }
            ).AddJsonOptions(options=>
              options.JsonSerializerOptions.Converters.Add(new TimeSpanToStringConverter()));

            services.AddCors();

            services.AddCommonServices();
            services.AddOrganizationServices();
            services.AddRegisterServices();
            services.AddCalendarServices();
            services.AddStudyServices();
            services.AddFeeServices();
            services.AddProfileServices();
            services.AddDisciplineServices();
            services.AddEducationCheckServices();
            services.AddInstructorEvaluationsServices();
            services.AddStudentEvaluationServices();
            services.AddEducationPlanServices();
            services.AddGradeServices();
            services.AddStudentRequestServices();
            services.AddGraduateServices();
            services.AddScoped(typeof(EntityIdService<,>));
            services.AddScoped(typeof(EntityUidService<,>));
            services.AddScoped(typeof(ReadOnlyEntityUidService<>));

            services.AddScoped<PaymentService>();
            services.Configure<application_setting>(Configuration.GetSection("Settings"));
            services.Configure<Setting>(Configuration.GetSection("SeventyOneDev.Core"));
            services.Configure<jwt_config>(Configuration.GetSection("jwt_config"));
            services.Configure<open_id_setting>(Configuration.GetSection("PaymentOpenID"));
            services.AddSecurities();
            services.AddDocuments();
            services.AddNotifications();
            services.AddSingleton(new AutomaticRetryAttribute {Attempts = 3});
            services.AddWorkflow(Configuration.GetSection("PaymentOpenID"));
            services.AddControllersWithViews()
              .AddNewtonsoftJson(options =>
                options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
              );

            services.AddHangfire((provider, configuration) =>
            {
              // var filter = provider.GetRequiredService<AutomaticRetryAttribute>();
              // filter.Attempts = 5;
              configuration.UseStorage(new OracleStorage(
                  () => new OracleConnection(Configuration["databases:oracle"]),
                  new OracleStorageOptions
                  {
                    SchemaName = "EDU"
                  }));
                //.UseOr(Configuration["databases:postgres"]);
              //configuration.UseFilter(filter);
              configuration.UseFilter(provider.GetRequiredService<AutomaticRetryAttribute>());
            });

            services.AddHangfireServer(options =>
            {
              options.WorkerCount = 10;
            });
            services.AddSwaggerGen(c =>
            {
              c.DocumentFilter<CustomSwaggerFilter>();
                c.SwaggerDoc("calendar", new OpenApiInfo { Title = "Calendar API", Version = "v1" });
                c.SwaggerDoc("common", new OpenApiInfo { Title = "Common API", Version = "v1" });
                c.SwaggerDoc("curriculum", new OpenApiInfo { Title = "Curriculum API", Version = "v1" });
                c.SwaggerDoc("education_plan", new OpenApiInfo { Title = "Education Plan API", Version = "v1" });
                c.SwaggerDoc("exam_plan", new OpenApiInfo { Title = "Exam Plan API", Version = "v1" });
                c.SwaggerDoc("external", new OpenApiInfo { Title = "External API", Version = "v1" });
                c.SwaggerDoc("fee", new OpenApiInfo { Title = "Fee API", Version = "v1" });
                c.SwaggerDoc("grade", new OpenApiInfo { Title = "Grade API", Version = "v1" });
                c.SwaggerDoc("graduate", new OpenApiInfo { Title = "Graduate API", Version = "v1" });
                c.SwaggerDoc("installment", new OpenApiInfo { Title = "Installment API", Version = "v1" });
                c.SwaggerDoc("loan", new OpenApiInfo { Title = "Loan API", Version = "v1" });
                c.SwaggerDoc("organization", new OpenApiInfo { Title = "Organization API", Version = "v1" });
                c.SwaggerDoc("profile", new OpenApiInfo { Title = "Profile API", Version = "v1" });
                c.SwaggerDoc("register", new OpenApiInfo { Title = "Register API", Version = "v1" });
                c.SwaggerDoc("security", new OpenApiInfo { Title = "Security API", Version = "v1" });
                c.SwaggerDoc("setup", new OpenApiInfo { Title = "Setup API", Version = "v1" });
                c.SwaggerDoc("study", new OpenApiInfo { Title = "Study API", Version = "v1" });
                c.SwaggerDoc("discipline", new OpenApiInfo { Title = "Discipline API", Version = "v1" });
                c.SwaggerDoc("education_check", new OpenApiInfo { Title = "Education Check API", Version = "v1" });
                c.SwaggerDoc("utilities", new OpenApiInfo { Title = "Utilities API", Version = "v1" });
                c.SwaggerDoc("instructor_evaluations", new OpenApiInfo { Title = "Instructor Evaluations API", Version = "v1" });
                c.SwaggerDoc("student_evaluation", new OpenApiInfo { Title = "Student Evaluation API", Version = "v1" });
                c.SwaggerDoc("student_request", new OpenApiInfo { Title = "Student Request API", Version = "v1" });
                c.SwaggerDoc("payment", new OpenApiInfo { Title = "Payment API", Version = "v1" });
                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                c.IncludeXmlComments(xmlPath, includeControllerXmlComments: true);
                c.MapType(typeof(TimeSpan?), () => new OpenApiSchema
                {
                  Type = "string",
                  Example = new OpenApiString("09:30:00")
                });
                c.MapType(typeof(TimeSpan), () => new OpenApiSchema
                {
                  Type = "string",
                  Example = new OpenApiString("09:30:00")
                });
                c.EnableAnnotations();
                c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
                {
                  Type = SecuritySchemeType.Http,
                  BearerFormat = "JWT",
                  In = ParameterLocation.Header,
                  Scheme = "bearer",
                  Description = "Please insert JWT token into field"
                });

                c.AddSecurityRequirement(new OpenApiSecurityRequirement
                {
                  {
                    new OpenApiSecurityScheme
                    {
                      Reference = new OpenApiReference
                      {
                        Type = ReferenceType.SecurityScheme,
                        Id = "Bearer"
                      }
                    },
                    new string[] { }
                  }
                });
            });
            services.AddAuthentication("Zerpa")//JwtBearerDefaults.AuthenticationScheme)

              .AddJwtBearer("IAM",options =>
              {
                options.RequireHttpsMetadata = false;
                options.Authority = Configuration["OpenID:Authority"];
                options.TokenValidationParameters = new Microsoft.IdentityModel.Tokens.TokenValidationParameters
                {
                  ValidateIssuer = true,
                  ValidateAudience = false

                };
                options.Events = new JwtBearerEvents()
                {
                  OnAuthenticationFailed = context => context.HttpContext.User.Identity!.IsAuthenticated ? Task.CompletedTask : Task.FromResult(0)
                };

              })
              .AddJwtBearer("Zerpa", options =>
              {
                options.RequireHttpsMetadata = false;
                options.SaveToken = true;
                options.TokenValidationParameters = new TokenValidationParameters
                {
                  ValidateIssuer = true,
                  ValidIssuer = Configuration["jwt_config:issuer"],
                  ValidateIssuerSigningKey = true,
                  IssuerSigningKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(Configuration["jwt_config:secret"])),
                  ValidAudience = Configuration["jwt_config:audience"],
                  ValidateAudience = true,
                  ValidateLifetime = true,
                  ClockSkew = TimeSpan.FromMinutes(5)
                };
                options.Events = new JwtBearerEvents()
                {
                  OnAuthenticationFailed = context => context.HttpContext.User.Identity!.IsAuthenticated ? Task.CompletedTask : Task.FromResult(0)
                };
              });
            services.AddAuthorization(options =>
            {
              options.DefaultPolicy = new AuthorizationPolicyBuilder().RequireAuthenticatedUser()
                .AddAuthenticationSchemes("Zerpa", "IAM").Build();
              options.AddPolicy("STUDENT", policy =>
              {
                policy.RequireAuthenticatedUser();
                policy.AddAuthenticationSchemes("IAM");
                policy.RequireClaim("scope", "student");
              });
              options.AddPolicy("CURRICULUM", policy =>
              {
                policy.RequireAuthenticatedUser();
                policy.AddAuthenticationSchemes("IAM");
                policy.RequireClaim("scope", "curriculum");
              });
            });

            services.AddHttpClient("report", client =>
            {
              client.BaseAddress = new Uri(Configuration["Settings:report_url"]);
              client.DefaultRequestHeaders.Add("Accept", "application/json");
              client.Timeout=TimeSpan.FromSeconds(10);

            });

            var spReport = ServicePointManager.FindServicePoint(new Uri(Configuration["Settings:report_url"]));
            spReport.ConnectionLimit = 100;

            services.AddHttpClient("scholarship", client =>
            {
              client.BaseAddress = new Uri(Configuration["Settings:scholarship_url"]);
              client.DefaultRequestHeaders.Add("Accept", "application/json");
              client.DefaultRequestHeaders.Add("x-api-key", Configuration["Settings:scholarship_api_key"]);
              client.Timeout=TimeSpan.FromSeconds(10);

            });

            var spScholarship = ServicePointManager.FindServicePoint(new Uri(Configuration["Settings:scholarship_url"]));
            spReport.ConnectionLimit = 100;
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
          InitializeDatabase(app);
          app.UseForwardedHeaders(new ForwardedHeadersOptions
          {
            ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto,
            KnownNetworks =
            {
              new IPNetwork(IPAddress.Any, 0)
            }
          });
          app.UseCors(builder => builder
            .AllowAnyOrigin()
            .AllowAnyMethod()
            .AllowAnyHeader());


                app.UseDeveloperExceptionPage();
                app.UseSwagger(c => {
                  c.RouteTemplate = "swagger/{documentName}/swagger.json";
                  c.PreSerializeFilters.Add((swaggerDoc, httpReq) =>
                  {
                    if (!httpReq.Headers.ContainsKey("X-Forwarded-Host")) return;

                    var serverUrl = $"{httpReq.Headers["X-Scheme"]}://" +
                                    $"{httpReq.Headers["X-Forwarded-Host"]}" +
                                    $"{httpReq.Headers["X-Forwarded-Prefix"]}";
                    swaggerDoc.Servers = new List<OpenApiServer> { new OpenApiServer { Url =serverUrl}};// $"{httpReq.Scheme}://{httpReq.Host.Value}{swaggerBasePath}" } };
                  });
                });
                app.UseSwaggerUI(c =>
                {

                  c.SwaggerEndpoint("calendar/swagger.json", "Calendar API v1");
                  c.SwaggerEndpoint("common/swagger.json", "Common API v1");
                  c.SwaggerEndpoint("curriculum/swagger.json", "Curriculum API v1");
                  c.SwaggerEndpoint("education_plan/swagger.json", "Education Plan API v1");
                  c.SwaggerEndpoint("exam_plan/swagger.json", "Exam Plan API v1");
                  c.SwaggerEndpoint("external/swagger.json", "External API v1");
                  c.SwaggerEndpoint("fee/swagger.json", "Fee API v1");
                  c.SwaggerEndpoint("grade/swagger.json", "Grade API v1");
                  c.SwaggerEndpoint("graduate/swagger.json", "Graduate API v1");
                  c.SwaggerEndpoint("installment/swagger.json", "Installment API v1");
                  c.SwaggerEndpoint("loan/swagger.json", "Loan API v1");
                  c.SwaggerEndpoint("organization/swagger.json", "Organization API v1");
                  c.SwaggerEndpoint("profile/swagger.json", "Profile API v1");
                  c.SwaggerEndpoint("register/swagger.json", "Register API v1");
                  c.SwaggerEndpoint("security/swagger.json", "Security API v1");
                  c.SwaggerEndpoint("setup/swagger.json", "Setup API v1");
                  c.SwaggerEndpoint("study/swagger.json", "Study API v1");
                  c.SwaggerEndpoint("discipline/swagger.json", "Discipline API v1");
                  c.SwaggerEndpoint("education_check/swagger.json", "Education Check API v1");
                  c.SwaggerEndpoint("utilities/swagger.json", "Utilities API v1");
                  c.SwaggerEndpoint("instructor_evaluations/swagger.json", "Instructor Evaluations API v1");
                  c.SwaggerEndpoint("student_evaluation/swagger.json", "Student Evaluation API v1");
                  c.SwaggerEndpoint("student_request/swagger.json", "Student Request API v1");
                  c.SwaggerEndpoint("payment/swagger.json", "Payment API v1");
                  c.DefaultModelExpandDepth(0);
                  c.DefaultModelsExpandDepth(-1);
                  c.DocExpansion(DocExpansion.None);

                });

                app.UseHttpsRedirection();
                // app.Use(async (context, next) =>
                // {
                //   var referer = context.Request.Headers["Referer"].FirstOrDefault();
                //   if (!string.IsNullOrWhiteSpace(referer))
                //   {
                //     if (referer.StartsWith("https://rsu.71dev.com"))
                //     {
                //       await next();
                //     }
                //     else if ( referer.StartsWith("https://mcru.71dev.com"))
                //
                //     {
                //       await next();
                //     }
                //     else if ( referer.StartsWith("https://localhost:4200"))
                //
                //     {
                //       await next();
                //     }
                //     else if ( referer.StartsWith("https://localhost:5001"))
                //     {
                //       await next();
                //     }
                //   }
                //   context.Response.StatusCode = 401;
                //   await context.Response.WriteAsync("Access Denied");
                // });

                app.UseRouting();
                app.UseAuthentication();

                app.UseAuthorization();
                app.UseStaticFiles();

                app.UseEndpoints(endpoints =>
                {
                  endpoints.MapControllers();
                });
        }
        private void InitializeDatabase(IApplicationBuilder app)
        {
          using var serviceScope = app.ApplicationServices.GetService<IServiceScopeFactory>()?.CreateScope();
          if (serviceScope == null) throw new Exception("Cannot create IServiceScopeFactory");
          var coreContext = serviceScope.ServiceProvider.GetRequiredService<SeventyOneDevCoreContext>();
          var dbContext = serviceScope.ServiceProvider.GetRequiredService<Db>();
          var setting = serviceScope.ServiceProvider.GetRequiredService<IOptions<Setting>>();
          CommonInit.Run(dbContext);
          CreateViews(coreContext.Database,setting.Value.default_schema);
        }

        private static void CreateViews(DatabaseFacade db, string schema)
        {
          //InjectView(db, "app_data.sql", "v_app_data",schema);
          var assembly = Assembly.GetAssembly(typeof(SeventyOneDev.Core.ServiceCollectionExtension));
          string[] names = assembly.GetManifestResourceNames();
          foreach (var name in names)
          {
            var resource = assembly.GetManifestResourceStream(name);
            var sqlQuery = new StreamReader(resource).ReadToEnd();
            //we always delete the old view, in case the sql query has changed
            //db.ExecuteSqlRaw($"IF OBJECT_ID('{viewName}') IS NOT NULL BEGIN DROP VIEW {viewName} END");
            //creating a view based on the sql query
            db.ExecuteSqlRaw(string.Format(sqlQuery, schema));
          }
        }
    }
}
