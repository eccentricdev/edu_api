using System;
using System.Security.Claims;

namespace SeventyOneDev.Utilities
{
  public static class Token
  {
    public static Guid GetUserUid(ClaimsIdentity claimsIdentity)
    {
      var userUidString = claimsIdentity.FindFirst(c => c.Type == "user_uid")?.Value;
      if (string.IsNullOrEmpty(userUidString)) throw new Exception("MISSING_USER_UID");
      return Guid.Parse(userUidString);
    }
  }
}
