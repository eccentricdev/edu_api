# Build Stage
FROM registry.71dev.com/dotnetsdk:5.0 as build-env
WORKDIR /source
COPY . .
RUN git clone https://chokchai_j:jcuJe8JgCUVG8YNYSbAk@bitbucket.org/eccentricdev/core_api.git
RUN git clone https://chokchai_j:jcuJe8JgCUVG8YNYSbAk@bitbucket.org/eccentricdev/master_core.git
RUN git clone https://chokchai_j:jcuJe8JgCUVG8YNYSbAk@bitbucket.org/eccentricdev/payment_core.git
RUN dotnet restore --configfile nuget.config
# RUN dotnet build
RUN dotnet publish -o /publish --configuration Release;
# Publish Stage
FROM registry.71dev.com/dotnetaspnet:5.0
WORKDIR /app
COPY --from=build-env /publish .
ENV ASPNETCORE_URLS http://*:8080
ENTRYPOINT ["dotnet", "edu_api.dll"]
